from robot.api import logger
import hlk_pcta_traffic
import cli_command
import tl1_command
import hlk_cli_connection
import hlk_tl1_connection

def pcta_check_traffic_debug(*args):
    """
    call pcta_check_traffic. If failed ,re-check again. 
    example: pcta_check_traffic_debug capt_options='ta_protocol=ip,
    side=user, mac_dest=000000000011, packet_count=5' 
    send_options='ta_protocol=IP, lif=2, vlan1_id=100, vlan1_user_prior=2,
    mac_dest=000000000022, mac_src=000000000011, ip_dest=2.2.2.2,
    ip_src=1.1.1.1, packet_count=5' delay_time='3' exp_result='pass'
    """
    try:
        hlk_pcta_traffic.pcta_check_traffic_bidirect(*args)
    except:
        #if ('first_cli_session' in cli_command.CLI_SESSION) :
        #    cli_command.send_cli_command('show vlan bridge-port-fdb')
        #    cli_command.send_cli_command('show service fdb-mac')
        #    cli_command.send_cli_command('info configure bridge port')
        #    cli_command.send_cli_command('show port')
        #    cli_command.send_cli_command('show interface port')
        #    hlk_cli_connection.disconnect_isam()
        #    #hlk_tl1_connection.connect_isam_tl1()
        #    #htl1_command.send_tl1_command('rtrv-ontenet::all;')
        #    #htl1_command.send_tl1_command('rtrv-ont::all;')
        #    #htl1_command.send_tl1_command('rtrv-eqpt::all;')
        #    #htl1_command.send_tl1_command('rtrv-alm-all;')
        #    #htl1_command.send_tl1_command('rtrv-cond-all;')
        #    #hhlk_tl1_connection.disconnect_isam_tl1()
        #    hlk_cli_connection.connect_isam()
        #elif ('first_tl1_session' in tl1_command.TL1_SESSION) :
        #    tl1_command.send_tl1_command('rtrv-ontenet::all;')
        #    tl1_command.send_tl1_command('rtrv-ont::all;')
        #    tl1_command.send_tl1_command('rtrv-eqpt::all;')
        #    tl1_command.send_tl1_command('rtrv-alm-all;')
        #    tl1_command.send_tl1_command('rtrv-cond-all;')
        #    hlk_tl1_connection.disconnect_isam_tl1()
        #    hlk_cli_connection.connect_isam()
        #    cli_command.send_cli_command('show vlan bridge-port-fdb')
        #    cli_command.send_cli_command('show service fdb-mac')
        #    cli_command.send_cli_command('info configure bridge port')
        #    cli_command.send_cli_command('show port')
        #    cli_command.send_cli_command('show interface port')
        #    hlk_cli_connection.disconnect_isam()
        #    hlk_tl1_connection.connect_isam_tl1()
        #else:
        #    hlk_cli_connection.connect_isam()
        #    cli_command.send_cli_command('show vlan bridge-port-fdb')
        #    cli_command.send_cli_command('show service fdb-mac')
        #    cli_command.send_cli_command('info configure bridge port')
        #    cli_command.send_cli_command('show port')
        #    cli_command.send_cli_command('show interface port')
        #    hlk_cli_connection.disconnect_isam()
        #    hlk_tl1_connection.connect_isam_tl1()
        #    tl1_command.send_tl1_command('rtrv-ontenet::all;')
        #    tl1_command.send_tl1_command('rtrv-ont::all;')
        #    tl1_command.send_tl1_command('rtrv-eqpt::all;')
        #    tl1_command.send_tl1_command('rtrv-alm-all;')
        #    tl1_command.send_tl1_command('rtrv-cond-all;')
        #    hlk_tl1_connection.disconnect_isam_tl1()
        logger.trace('open trace')
        hlk_pcta_traffic.pcta_check_traffic_bidirect(*args)
        logger.trace('close trace')
    
def pcta_check_traffic_double(*args):
    """
    call pcta_check_traffic. If failed ,re-check again. 
    example: pcta_check_traffic_debug capt_options='ta_protocol=ip,
    side=user, mac_dest=000000000011, packet_count=5' 
    send_options='ta_protocol=IP, lif=2, vlan1_id=100, vlan1_user_prior=2,
    mac_dest=000000000022, mac_src=000000000011, ip_dest=2.2.2.2,
    ip_src=1.1.1.1, packet_count=5' delay_time='3' exp_result='pass'
    """
    try:
        hlk_pcta_traffic.pcta_check_traffic_bidirect(*args)
    except:
        logger.trace('start second time to check the traffic')
        hlk_pcta_traffic.pcta_check_traffic_bidirect(*args)
        logger.trace('end second time to check the traffic')

