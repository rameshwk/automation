*** Settings ***
Library           %{RFPATH}/Python-2.7/lib/python2.7/site-packages/SSHLibrary/
Library           %{RFPATH}/Python-2.7/lib/python2.7/site-packages/robot/libraries/String.py
Library           %{RFPATH}/Python-2.7/lib/python2.7/site-packages/robot/libraries/Collections.py
Library           %{ROBOTREPO}/HLK/lib/l1-string.py
Library           %{ROBOTREPO}/HLK/lib/l1-pcta.py

*** Keywords ***
CloseAllPCTA
    SSHLibrary.Close All Connections

ClosePCTA
    SSHLibrary.write    exit
    SSHLibrary.Read Until    ]\#
    SSHLibrary.Close Connection

IPPlusOne
    [Arguments]    ${ip_addr}
    @{addr_list}=    Split String    ${ip_addr}    .
    ${result}=    Evaluate    @{addr_list}[3]+1
    Remove From List    ${addr_list}    -1
    Append To List    ${addr_list}    ${result}
    : FOR    ${p1}    ${p2}    ${p3}    ${p4}    IN    @{addr_list}
    \    ${new_addr}=    Set Variable    ${p1}.${p2}.${p3}.${p4}
    Return From Keyword    ${new_addr}

IPToHex
    [Arguments]    ${ip_address}
    @{list}=    Split String    ${ip_address}    .
    ${result}=    Set Variable
    : FOR    ${element}    IN    @{list}
    \    ${element}=    Convert To Hex    ${element}    length=2
    \    ${result}=    Set Variable    ${result}${element}
    Return From Keyword    ${result}

MACConvertToLinkLayer
    [Arguments]    ${mac_addr}
    ${mac_p1}=    Get Substring    ${mac_addr}    0    2    #split mac address
    ${mac_p2}=    Get Substring    ${mac_addr}    2    4
    ${mac_p3}=    Get Substring    ${mac_addr}    4    6
    ${mac_p4}=    Get Substring    ${mac_addr}    6    8
    ${mac_p5}=    Get Substring    ${mac_addr}    8    10
    ${mac_p6}=    Get Substring    ${mac_addr}    10    12
    ${mac_p1}=    Convert To Integer    ${mac_p1}    16    #convert part1 to 10
    ${mac_p1}=    Convert To Binary    ${mac_p1}    #convert part1 to binary
    ${mac_p1_length}=    Get Length    ${mac_p1}
    log    ${mac_p1}
    ${mac_p1}=    Set Variable If    ${mac_p1_length} == 1    0000000${mac_p1}    ${mac_p1_length} == 2    000000${mac_p1}    ${mac_p1_length} == 3
    ...    00000${mac_p1}    ${mac_p1_length} == 4    0000${mac_p1}    ${mac_p1_length} == 5    000${mac_p1}    ${mac_p1_length} == 6
    ...    00${mac_p1}    ${mac_p1_length} == 7    0${mac_p1}    ${mac_p1_length} == 8    ${mac_p1}
    log    ${mac_p1}
    ${mac_p1_tmp1}=    Get Substring    ${mac_p1}    0    1
    ${mac_p1_tmp2}=    Get Substring    ${mac_p1}    1    2
    ${mac_p1_tmp3}=    Get Substring    ${mac_p1}    2    3
    ${mac_p1_tmp4}=    Get Substring    ${mac_p1}    3    4
    ${mac_p1_tmp5}=    Get Substring    ${mac_p1}    4    5
    ${mac_p1_tmp6}=    Get Substring    ${mac_p1}    5    6
    ${mac_p1_tmp7}=    Get Substring    ${mac_p1}    6    7
    ${mac_p1_tmp8}=    Get Substring    ${mac_p1}    7    8
    ${mac_p1_tmp7}=    Set Variable If    ${mac_p1_tmp7} == 0    1    ${mac_p1_tmp7} == 1    0
    ${mac_p1}=    Set Variable    ${mac_p1_tmp1}${mac_p1_tmp2}${mac_p1_tmp3}${mac_p1_tmp4}${mac_p1_tmp5}${mac_p1_tmp6}${mac_p1_tmp7}${mac_p1_tmp8}
    ${mac_p1}=    Convert To Integer    ${mac_p1}    2    #convert part1 to 10
    ${mac_p1}=    Convert To Hex    ${mac_p1}
    ${link_layer_addr}=    Set Variable    fe80:0:0:0:${mac_p1}${mac_p2}:${mac_p3}ff:fe${mac_p4}:${mac_p5}${mac_p6}
    Return From Keyword    ${link_layer_addr}

StartPCTA
    [Arguments]    ${pcta_host}    ${pcta_host_username}    ${pcta_host_password}    ${pcta_path}
    [Documentation]    Open SSH connection
    ...
    ...    move to PCTA directory, and execute ./pcta.exe
    ...
    ...    return connection index
    Set Default Configuration    timeout=60s
    ${index}=    SSHLibrary.Open Connection    ${pcta_host}
    SSHLibrary.login    ${pcta_host_username}    ${pcta_host_password}
    SSHLibrary.write    cd ${pcta_path}
    SSHLibrary.write    ./pcta.exe
    ${stdout}=    SSHLibrary.Read Until    >
    Should Contain    ${stdout}    Entering interactive mode. Exit with Control-C
    Return From Keyword    ${index}

ta_check_capt_bootp_dhcp_message_success
    [Arguments]    ${interface}    ${options}    ${expect}
    ${expect}    Convert to Uppercase    ${expect}
    SSHLibrary.Write    ta_stop_capt_bootp_dhcp_message -lif ${interface} ${options}
    ${output}=    SSHLibrary.read until    >
    log    ${output}
    Should Not Contain    ${output}    no bootp/dhcp capture proces
    ${length}=    Get Length    ${output}
    log    ${length}
    Run Keyword If    '${expect}' == 'TRUE'    Should Be True    ${length} > 51
    ...    ELSE IF    '${expect}' == 'FALSE'    Should Be True    ${length} <= 51
    ...    ELSE    Fatal Error    MSG="Please Check Expect Result!"

ta_send_arp_rarp_packet
    [Arguments]    ${interface}    ${options}=
    SSHLibrary.write    ta_send_arp_rarp_packet -lif ${interface} ${options}
    ${out_put}=    SSHLibrary.read until    >
    Should Not Contain    ${out_put}    Illegal
    Should Not Contain    ${out_put}    is not a correct value
    Should Not Contain    ${out_put}    invalid command

ta_send_bootp_dhcp_message
    [Arguments]    ${interface}    ${options}=
    SSHLibrary.write    ta_send_bootp_dhcp_message -lif ${interface} ${options}
    ${out_put}=    SSHLibrary.read until    >
    Should Not Contain    ${out_put}    Illegal
    Should Not Contain    ${out_put}    is not a correct value
    Should Not Contain    ${out_put}    invalid command

ta_send_bootp_dhcpv6_message
    [Arguments]    ${interface}    ${options}=
    SSHLibrary.write    ta_send_bootp_dhcpv6_message -lif ${interface} ${options}
    sleep    5s
    ${out_put}=    SSHLibrary.read until    >
    Should Not Contain    ${out_put}    Illegal
    Should Not Contain    ${out_put}    is not a correct value
    Should Not Contain    ${out_put}    invalid command

ta_send_icmp_message
    [Arguments]    ${interface}    ${options}=
    SSHLibrary.write    ta_send_icmp_message -lif ${interface} ${options}
    sleep    5s
    ${out_put}=    SSHLibrary.read until    >
    Should Not Contain    ${out_put}    Illegal
    Should Not Contain    ${out_put}    is not a correct value
    Should Not Contain    ${out_put}    invalid command

ta_send_icmpv6_message
    [Arguments]    ${interface}    ${options}=
    SSHLibrary.write    ta_send_icmpv6_message -lif ${interface} ${options}
    sleep    5s
    ${out_put}=    SSHLibrary.read until    >
    Should Not Contain    ${out_put}    Illegal
    Should Not Contain    ${out_put}    is not a correct value
    Should Not Contain    ${out_put}    invalid command

ta_send_igmp_message
    [Arguments]    ${interface}    ${options}=
    SSHLibrary.write    ta_send_igmp_message -lif ${interface} ${options}
    sleep    5s
    ${out_put}=    SSHLibrary.read until    >
    Should Not Contain    ${out_put}    Illegal
    Should Not Contain    ${out_put}    is not a correct value
    Should Not Contain    ${out_put}    invalid command

ta_send_ip_packet
    [Arguments]    ${interface}    ${options}=
    SSHLibrary.write    ta_send_ip_packet -lif ${interface} ${options}
    sleep    5s
    ${out_put}=    SSHLibrary.read until    >
    Should Not Contain    ${out_put}    Illegal
    Should Not Contain    ${out_put}    is not a correct value
    Should Not Contain    ${out_put}    invalid command

ta_send_sntp_message
    [Arguments]    ${interface}    ${options}=
    SSHLibrary.write    ta_send_sntp_message -lif ${interface} ${options}
    Comment    sleep    5s
    ${out_put}=    SSHLibrary.read until    >
    Should Not Contain    ${out_put}    Illegal
    Should Not Contain    ${out_put}    is not a correct value
    Should Not Contain    ${out_put}    invalid command

ta_send_tcp_packet
    [Arguments]    ${interface}    ${options}=
    SSHLibrary.write    ta_send_tcp_packet -lif ${interface} ${options}
    sleep    5s
    ${out_put}=    SSHLibrary.read until    >
    Should Not Contain    ${out_put}    Illegal
    Should Not Contain    ${out_put}    is not a correct value
    Should Not Contain    ${out_put}    invalid command

ta_send_udp_packet
    [Arguments]    ${interface}    ${options}=
    SSHLibrary.write    ta_send_udp_packet -lif ${interface} ${options}
    sleep    5s
    ${out_put}=    SSHLibrary.read until    >
    Should Not Contain    ${out_put}    Illegal
    Should Not Contain    ${out_put}    is not a correct value
    Should Not Contain    ${out_put}    invalid command

ta_start_capt_arp_rarp_packet
    [Arguments]    ${interface}    ${options}=
    SSHLibrary.write    ta_start_capt_arp_rarp_packet -lif ${interface} ${options}
    ${output}=    SSHLibrary.read until    >
    ${result}=    Fetch From Right    ${output}    -dist_id${SPACE}
    ${dist_id}=    Get Substring    ${result}    0    1
    Return From Keyword    ${dist_id}

ta_start_capt_bootp_dhcp_message
    [Arguments]    ${interface}    ${options}=
    SSHLibrary.write    ta_start_capt_bootp_dhcp_message -lif ${interface} ${options}
    ${output}=    SSHLibrary.read until    >
    ${result}=    Fetch From Right    ${output}    -dist_id${SPACE}
    ${dist_id}=    Get Substring    ${result}    0    1
    Return From Keyword    ${dist_id}

ta_start_capt_bootp_dhcpv6_message
    [Arguments]    ${interface}    ${options}=
    SSHLibrary.write    ta_start_capt_bootp_dhcpv6_message -lif ${interface} ${options}
    ${output}=    SSHLibrary.read until    >
    ${result}=    Fetch From Right    ${output}    -dist_id${SPACE}
    ${dist_id}=    Get Substring    ${result}    0    1
    Return From Keyword    ${dist_id}

ta_start_capt_icmp_message
    [Arguments]    ${interface}    ${options}=
    SSHLibrary.write    ta_start_capt_icmp_message -lif ${interface} ${options}
    ${output}=    SSHLibrary.read until    >
    Should Not Contain    ${output}    unknown
    ${result}=    Fetch From Right    ${output}    -dist_id${SPACE}
    ${dist_id}=    Get Substring    ${result}    0    1
    Return From Keyword    ${dist_id}

ta_start_capt_igmp_message
    [Arguments]    ${interface}    ${options}=
    SSHLibrary.write    ta_start_capt_igmp_message -lif ${interface} ${options}
    ${output}=    SSHLibrary.read until    >
    Should Not Contain    ${output}    unknown
    ${result}=    Fetch From Right    ${output}    -dist_id${SPACE}
    ${dist_id}=    Get Substring    ${result}    0    1
    Return From Keyword    ${dist_id}

ta_start_capt_ip_packet
    [Arguments]    ${interface}    ${options}=
    SSHLibrary.write    ta_start_capt_ip_packet -lif ${interface} ${options}
    ${output}=    SSHLibrary.read until    >
    Should Not Contain    ${output}    unknown
    ${result}=    Fetch From Right    ${output}    -dist_id${SPACE}
    ${dist_id}=    Get Substring    ${result}    0    1
    Return From Keyword    ${dist_id}

ta_rtrv_capt_ip_packet
    [Arguments]    ${interface}    ${options}=
    [Documentation]    This keyword is used to retrive all ip packets that recived by our PC_TA capture before _ta_stop_..._ executed. It will return an list, of wich each element in this list is an dictionary structure like ta_stop_pcta... got.
    SSHLibrary.write    ta_rtrv_capt_ip_packet -lif ${interface} ${options}
    ${output}=    SSHLibrary.read until    >
    Should Not Contain    ${output}    not open
    ${length}=    Get Length    ${output}
    Return From Keyword If    ${length}<300    NONE
    ${cap_list}    l1-pcta.parse_pcta_result    ${output}
    Return From Keyword    ${cap_list}

ta_start_capt_sntp_message
    [Arguments]    ${interface}    ${options}=
    SSHLibrary.write    ta_start_capt_sntp_message -lif ${interface} ${options}
    ${output}=    SSHLibrary.read until    >
    Should Not Contain    ${output}    unknown
    ${result}=    Fetch From Right    ${output}    -dist_id${SPACE}
    ${dist_id}=    Get Substring    ${result}    0    1
    Return From Keyword    ${dist_id}

ta_start_capt_tcp_packet
    [Arguments]    ${interface}    ${options}=
    SSHLibrary.write    ta_start_capt_tcp_packet -lif ${interface} ${options}
    ${output}=    SSHLibrary.read until    >
    Should Not Contain    ${output}    unknown
    ${result}=    Fetch From Right    ${output}    -dist_id${SPACE}
    ${dist_id}=    Get Substring    ${result}    0    1
    Return From Keyword    ${dist_id}

ta_start_capt_udp_packet
    [Arguments]    ${interface}    ${options}=
    SSHLibrary.write    ta_start_capt_udp_packet -lif ${interface} ${options}
    ${output}=    SSHLibrary.read until    >
    Should Not Contain    ${output}    unknown
    ${result}=    Fetch From Right    ${output}    -dist_id${SPACE}
    ${dist_id}=    Get Substring    ${result}    0    1
    Return From Keyword    ${dist_id}

ta_stop_capt_arp_rarp_packet
    [Arguments]    ${interface}    ${options}=
    SSHLibrary.write    ta_stop_capt_arp_rarp_packet -lif ${interface} ${options}
    ${output}=    SSHLibrary.read until    >
    Should Not Contain    ${output}    not open
    ${length}=    Get Length    ${output}
    Return From Keyword If    ${length}<50    NONE
    ${result}=    Fetch From Right    ${output}    {{
    ${result}=    Fetch From Left    ${result}    }${SPACE}}
    ${result}=    Remove String Using Regexp    ${result}    -packet ".*"${SPACE}
    ${result}=    Replace String Using Regexp    ${result}    ${SPACE}${SPACE}    ${SPACE}
    @{result}=    Split String    ${result}    ${SPACE}
    ${cap_dict}=    Create Dictionary
    : FOR    ${key}    ${value}    IN    @{result}
    \    Set To Dictionary    ${cap_dict}    ${key}    ${value}
    Return From Keyword    ${cap_dict}

ta_stop_capt_bootp_dhcp_message
    [Arguments]    ${interface}    ${options}=
    SSHLibrary.write    ta_stop_capt_bootp_dhcp_message -lif ${interface} ${options}
    ${output}=    SSHLibrary.read until    >
    Should Not Contain    ${output}    no bootp/dhcp capture process
    ${length}=    Get Length    ${output}
    Return From Keyword If    ${length}<50    NONE
    ${result}=    Fetch From Right    ${output}    {{
    ${result}=    Fetch From Left    ${result}    }}
    ${result}=    Remove String Using Regexp    ${result}    -message ".*"${SPACE}
    log    ${result}
    ${result}=    Replace String Using Regexp    ${result}    ${SPACE}+    ${SPACE}
    @{result}=    Split String    ${result}    ${SPACE}
    log    ${result}
    ${cap_dict}=    Create Dictionary
    : FOR    ${key}    ${value}    IN    @{result}
    \    Set To Dictionary    ${cap_dict}    ${key}    ${value}
    Return From Keyword    ${cap_dict}

ta_stop_capt_bootp_dhcpv6_message
    [Arguments]    ${interface}    ${options}=
    SSHLibrary.write    ta_stop_capt_bootp_dhcpv6_message -lif ${interface} ${options}
    ${output}=    SSHLibrary.read until    >
    Should Not Contain    ${output}    no bootp/dhcp capture process
    ${length}=    Get Length    ${output}
    Return From Keyword If    ${length}<100    NONE
    ${result}=    Fetch From Right    ${output}    {{
    ${result}=    Fetch From Left    ${result}    }}
    ${result}=    Remove String Using Regexp    ${result}    -message ".*"${SPACE}
    ${result}=    Replace String Using Regexp    ${result}    ${SPACE}${SPACE}    ${SPACE}
    @{result}=    Split String    ${result}    ${SPACE}
    ${cap_dict}=    Create Dictionary
    : FOR    ${key}    ${value}    IN    @{result}
    \    Set To Dictionary    ${cap_dict}    ${key}    ${value}
    Return From Keyword    ${cap_dict}

ta_stop_capt_icmp_message
    [Arguments]    ${interface}    ${options}=
    SSHLibrary.write    ta_stop_capt_icmp_message -lif ${interface} ${options}
    ${output}=    SSHLibrary.read until    >
    Should Not Contain    ${output}    not open
    ${length}=    Get Length    ${output}
    Return From Keyword If    ${length}<100    NONE
    ${result}=    Fetch From Right    ${output}    {{
    log    ${result}
    ${result}=    Fetch From Left    ${result}    }${SPACE}{
    log    ${result}
    ${message}=    Get String By Regexp    ${result}    -message {(.*)}
    ${message}=    Remove String    ${message}    ${SPACE}
    log    ${message}
    ${result1}=    Remove String Using Regexp    ${result}    -message ".*"${SPACE}
    log    ${result1}
    ${result1}=    Replace String Using Regexp    ${result1}    ${SPACE}${SPACE}    ${SPACE}
    log    ${result1}
    @{result1}=    Split String    ${result1}    ${SPACE}
    ${cap_dict}=    Create Dictionary
    : FOR    ${key}    ${value}    IN    @{result1}
    \    Set To Dictionary    ${cap_dict}    ${key}    ${value}
    Set To Dictionary    ${cap_dict}    -message    ${message}
    Return From Keyword    ${cap_dict}

ta_stop_capt_igmp_message
    [Arguments]    ${interface}    ${options}=
    SSHLibrary.write    ta_stop_capt_igmp_message -lif ${interface} ${options}
    ${output}=    SSHLibrary.Read Until Regexp    PC_TA_.*>
    log many    ${output}
    Should Not Contain    ${output}    not open
    ${length}=    Get Length    ${output}
    Return From Keyword If    ${length}<500    NONE
    ${result}=    Fetch From Right    ${output}    {{
    log    ${result}
    ${result}=    Fetch From Left    ${result}    }${SPACE}{
    log    ${result}
    ${result}=    Fetch From Left    ${result}    }${SPACE}}
    log    ${result}
    ${message}=    Get String By Regexp    ${result}    -message {(.*)}
    ${message}=    Remove String    ${message}    ${SPACE}
    log    ${message}
    ${result1}=    Remove String Using Regexp    ${result}    -message {(.*)}
    log    ${result1}
    ${result1}=    Replace String Using Regexp    ${result1}    ${SPACE}${SPACE}    ${SPACE}
    log    ${result1}
    @{result1}=    Split String    ${result1}    ${SPACE}
    ${cap_dict}=    Create Dictionary
    : FOR    ${key}    ${value}    IN    @{result1}
    \    Set To Dictionary    ${cap_dict}    ${key}    ${value}
    Set To Dictionary    ${cap_dict}    -message    ${message}
    Return From Keyword    ${cap_dict}

ta_stop_capt_ip_packet
    [Arguments]    ${interface}    ${options}=
    SSHLibrary.write    ta_stop_capt_ip_packet -lif ${interface} ${options}
    ${output}=    SSHLibrary.read until    >
    Should Not Contain    ${output}    not open
    ${length}=    Get Length    ${output}
    Return From Keyword If    ${length}<300    NONE
    ${result}=    Fetch From Right    ${output}    {{
    log    ${result}
    Comment    ${result}=    Fetch From Left    ${result}    }
    ${result}    Split To Lines    ${result}
    log    ${result}
    ${result}=    Replace String Using Regexp    @{result}[0]    -packet .*    ${SPACE}
    log    ${result}
    Comment    ${message}=    Get String By Regexp    ${result}    -message    \".*\"
    ${message}=    Remove String    ${result}    ${EMPTY}
    log    ${message}
    ${result1}=    Remove String Using Regexp    ${result}    -message    ".*"${SPACE}
    log    ${result1}
    ${result1}=    Replace String Using Regexp    ${result1}    ${SPACE}+    ${SPACE}
    log    ${result1}
    @{result1}=    Split String    ${result1}
    Log Many    @{result1}
    ${cap_dict}=    Create Dictionary
    : FOR    ${key}    ${value}    IN    @{result1}
    \    Set To Dictionary    ${cap_dict}    ${key}    ${value}
    Set To Dictionary    ${cap_dict}    -message    ${message}
    Return From Keyword    ${cap_dict}

ta_stop_capt_sntp_message
    [Arguments]    ${interface}    ${options}=
    SSHLibrary.write    ta_stop_capt_sntp_message -lif ${interface} ${options}
    ${output}=    SSHLibrary.Read Until Regexp    PC_TA_.*>
    log many    ${output}
    Should Not Contain    ${output}    not open
    ${length}=    Get Length    ${output}
    Return From Keyword If    ${length}<500    NONE
    ${result}=    Fetch From Right    ${output}    {{
    log    ${result}
    ${result}=    Fetch From Left    ${result}    }${SPACE}{
    log    ${result}
    ${result}=    Fetch From Left    ${result}    }${SPACE}}
    log    ${result}
    Comment    ${message}=    Get String By Regexp    ${result}    -message    {(.*)}
    ${message}=    Remove String    ${result}    ${SPACE}
    log    ${message}
    ${result1}=    Remove String Using Regexp    ${result}    -message {(.*)}
    log    ${result1}
    ${result1}=    Replace String Using Regexp    ${result1}    ${SPACE}+    ${SPACE}
    log    ${result1}
    @{result1}=    Split String    ${result1}    ${SPACE}
    ${cap_dict}=    Create Dictionary
    : FOR    ${key}    ${value}    IN    @{result1}
    \    Set To Dictionary    ${cap_dict}    ${key}    ${value}
    Set To Dictionary    ${cap_dict}    -message    ${message}
    Return From Keyword    ${cap_dict}

ta_stop_capt_tcp_packet
    [Arguments]    ${interface}    ${options}=
    SSHLibrary.write    ta_stop_capt_tcp_packet -lif ${interface} ${options}
    ${output}=    SSHLibrary.read until    >
    Should Not Contain    ${output}    not open
    ${length}=    Get Length    ${output}
    Return From Keyword If    ${length}<100    NONE
    ${result}=    Fetch From Right    ${output}    {{
    log    ${result}
    ${result}=    Fetch From Left    ${result}    }
    log    ${result}
    ${message}=    Get String By Regexp    ${result}    -packet \"(.*)\"
    ${message}=    Remove String    ${message}    ${SPACE}
    log    ${message}
    ${result1}=    Remove String Using Regexp    ${result}    -packet ".*"${SPACE}
    log    ${result1}
    ${result1}=    Replace String Using Regexp    ${result1}    ${SPACE}${SPACE}    ${SPACE}
    log    ${result1}
    @{result1}=    Split String    ${result1}    ${SPACE}
    ${cap_dict}=    Create Dictionary
    : FOR    ${key}    ${value}    IN    @{result1}
    \    Set To Dictionary    ${cap_dict}    ${key}    ${value}
    Set To Dictionary    ${cap_dict}    -message    ${message}
    Return From Keyword    ${cap_dict}

ta_stop_capt_udp_packet
    [Arguments]    ${interface}    ${options}=
    SSHLibrary.write    ta_stop_capt_udp_packet -lif ${interface} ${options}
    ${output}=    SSHLibrary.read until    >
    Should Not Contain    ${output}    not open
    ${length}=    Get Length    ${output}
    Return From Keyword If    ${length}<300    NONE
    ${result}=    Fetch From Right    ${output}    {{
    log    ${result}
    ${result}=    Fetch From Left    ${result}    }
    log    ${result}
    ${message}=    Get String By Regexp    ${result}    -message \"(.*)\"
    ${message}=    Remove String    ${message}    ${SPACE}
    log    ${message}
    ${result1}=    Remove String Using Regexp    ${result}    -message ".*"${SPACE}
    log    ${result1}
    ${result1}=    Replace String Using Regexp    ${result1}    ${SPACE}${SPACE}    ${SPACE}
    log    ${result1}
    @{result1}=    Split String    ${result1}    ${SPACE}
    ${cap_dict}=    Create Dictionary
    : FOR    ${key}    ${value}    IN    @{result1}
    \    Set To Dictionary    ${cap_dict}    ${key}    ${value}
    Set To Dictionary    ${cap_dict}    -message    ${message}
    Return From Keyword    ${cap_dict}

ta_traffic_start_arp_rarp
    [Arguments]    ${interface}    ${options}=
    SSHLibrary.write    ta_traffic_start_arp_rarp -lif ${interface} ${options}
    ${output}=    SSHLibrary.read until    >
    ${result}=    Fetch From Right    ${output}    -dist_id${SPACE}
    ${dist_id}=    Get Substring    ${result}    0    1
    Return From Keyword    ${dist_id}

ta_traffic_stop_arp_rarp
    [Arguments]    ${interface}    ${options}=
    SSHLibrary.write    ta_traffic_stop_arp_rarp -lif ${interface} ${options}
    sleep    5s
    ${output}=    SSHLibrary.read until    >
    Should Not Contain    ${output}    no bootp/dhcp capture process
