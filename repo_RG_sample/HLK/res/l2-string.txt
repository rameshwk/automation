*** Settings ***
Library           %{RFPATH}/Python-2.7/lib/python2.7/site-packages/robot/libraries/String.py
Library           %{RFPATH}/Python-2.7/lib/python2.7/site-packages/robot/libraries/Collections.py

*** Keywords ***
ConvertToValidTR069String
    [Arguments]    ${string}
    [Documentation]    Convert a string to TR069 valid parameter value form.
    ...
    ...    author: Cai Jinglu
    ${string}    Replace String    ${string}    .    \.
    ${space_status}    Run Keyword and return status    Should Contain    ${string}    ${SPACE}
    ${string1}    Set Variable if    ${space_status}    {{${string}}}    ${string}
    ${string2}    Set Variable if    '${string1}'=='${EMPTY}'    {}    ${string1}
    Return From Keyword    ${string2}

IGMPv3RecordGenerator
    [Arguments]    ${mcast_address}    ${type}
    [Documentation]    Generate group record list with type of igmp v3 asm/ssm-join/leave
    ...
    ...    type: v3asm-join, v3asm-leave, v3ssm-join, v3ssm-leave
    ...
    ...    Example:
    ...
    ...    GroupRecordGenerator '225.1.1.1' 'v3ssm-join'
    ...
    ...    Return String :05000001e1010101
    ${mcast_address}    l2-string.IPToHex    ${mcast_address}
    ${group_record_list}=    Set Variable If    '${type}'=='v3asm-join'    04000000${mcast_address}    '${type}'=='v3asm-leave'    03000000${mcast_address}    '${type}'=='v3ssm-join'
    ...    05000000${mcast_address}    '${type}'=='v3ssm-leave'    06000000${mcast_address}
    Return From Keyword    ${group_record_list}

IPPlusOne
    [Arguments]    ${ip_addr}
    @{addr_list}=    Split String    ${ip_addr}    .
    ${result}=    Evaluate    @{addr_list}[3]+1
    Remove From List    ${addr_list}    -1
    Append To List    ${addr_list}    ${result}
    : FOR    ${p1}    ${p2}    ${p3}    ${p4}    IN    @{addr_list}
    \    ${new_addr}=    Set Variable    ${p1}.${p2}.${p3}.${p4}
    Return From Keyword    ${new_addr}

IPToHex
    [Arguments]    ${ip_address}
    @{list}=    Split String    ${ip_address}    .
    ${result}=    Set Variable
    : FOR    ${element}    IN    @{list}
    \    ${element}=    Convert To Hex    ${element}    length=2
    \    ${result}=    Set Variable    ${result}${element}
    Return From Keyword    ${result}

MACConvertToLinkLayer
    [Arguments]    ${mac_addr}
    ${mac_p1}=    Get Substring    ${mac_addr}    0    2    #split mac address
    ${mac_p2}=    Get Substring    ${mac_addr}    2    4
    ${mac_p3}=    Get Substring    ${mac_addr}    4    6
    ${mac_p4}=    Get Substring    ${mac_addr}    6    8
    ${mac_p5}=    Get Substring    ${mac_addr}    8    10
    ${mac_p6}=    Get Substring    ${mac_addr}    10    12
    ${mac_p1}=    Convert To Integer    ${mac_p1}    16    #convert part1 to 10
    ${mac_p1}=    Convert To Binary    ${mac_p1}    #convert part1 to binary
    ${mac_p1_length}=    Get Length    ${mac_p1}
    log    ${mac_p1}
    ${mac_p1}=    Set Variable If    ${mac_p1_length} == 1    0000000${mac_p1}    ${mac_p1_length} == 2    000000${mac_p1}    ${mac_p1_length} == 3
    ...    00000${mac_p1}    ${mac_p1_length} == 4    0000${mac_p1}    ${mac_p1_length} == 5    000${mac_p1}    ${mac_p1_length} == 6
    ...    00${mac_p1}    ${mac_p1_length} == 7    0${mac_p1}    ${mac_p1_length} == 8    ${mac_p1}
    log    ${mac_p1}
    ${mac_p1_tmp1}=    Get Substring    ${mac_p1}    0    1
    ${mac_p1_tmp2}=    Get Substring    ${mac_p1}    1    2
    ${mac_p1_tmp3}=    Get Substring    ${mac_p1}    2    3
    ${mac_p1_tmp4}=    Get Substring    ${mac_p1}    3    4
    ${mac_p1_tmp5}=    Get Substring    ${mac_p1}    4    5
    ${mac_p1_tmp6}=    Get Substring    ${mac_p1}    5    6
    ${mac_p1_tmp7}=    Get Substring    ${mac_p1}    6    7
    ${mac_p1_tmp8}=    Get Substring    ${mac_p1}    7    8
    ${mac_p1_tmp7}=    Set Variable If    ${mac_p1_tmp7} == 0    1    ${mac_p1_tmp7} == 1    0
    ${mac_p1}=    Set Variable    ${mac_p1_tmp1}${mac_p1_tmp2}${mac_p1_tmp3}${mac_p1_tmp4}${mac_p1_tmp5}${mac_p1_tmp6}${mac_p1_tmp7}${mac_p1_tmp8}
    ${mac_p1}=    Convert To Integer    ${mac_p1}    2    #convert part1 to 10
    ${mac_p1}=    Convert To Hex    ${mac_p1}
    ${link_layer_addr}=    Set Variable    fe80:0:0:0:${mac_p1}${mac_p2}:${mac_p3}ff:fe${mac_p4}:${mac_p5}${mac_p6}
    Return From Keyword    ${link_layer_addr}
