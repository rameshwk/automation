*** Settings ***
Library           %{RFPATH}/Python-2.7/lib/python2.7/site-packages/robot/libraries/Telnet.py
Library           %{RFPATH}/Python-2.7/lib/python2.7/site-packages/robot/libraries/Process.py
Library           %{RFPATH}/Python-2.7/lib/python2.7/site-packages/robot/libraries/OperatingSystem.py
Resource          %{ROBOTREPO}/HLK/res/l2-telnet.txt
Resource          %{ROBOTREPO}/HLK/res/l2-common.txt

*** Keywords ***
EXECommand
    [Arguments]    ${command}    ${ip}=${ont['ip']}    ${port}=${ont['telnet_port']}    ${username}=${ont['telnet_username']}    ${password}=${ont['telnet_password']}    ${vtysh}=${ont['telnet_vtysh']}
    @{counter}=    evaluate    range(3)
    : FOR    ${a}    IN    @{counter}
    \    ${passed}    ${stdout}=    Run Keyword And Ignore Error    l2-telnet.SendCommand    ${command}
    \    Sleep    2s
    \    log    ${stdout}
    \    log    ${passed}
    \    Run Keyword If    '${passed}' == 'FAIL'    l2-telnet.OpenConnection    ${ip}    ${port}    ${username}
    \    ...    ${password}
    \    Exit For Loop If    '${passed}' == 'PASS'
    \    Sleep    10s
    Return From Keyword    ${stdout}

IsTelnetEnabled
    [Documentation]    This Keyword return ${True} if the DUT Telnet is enabled, or ${False} when disabled. It use NMAP to detect the telnet service. ---Alfred 20170623
    ${rc}    ${o}    ${e}    Exec With Log    nmap -p 23 ${ont['ip']} | awk '$0 ~ /23.tcp +(\\w+) +telnet.*/ {print $2}'
    Should Be equal    "${rc}"    "0"
    ${res}    Evaluate    "${o}"=="open"
    Return From Keyword    ${res}

LoginTelnet
    [Arguments]    ${ip}    ${port}    ${username}    ${password}    ${times}=3    ${timeout}=5
    ...    ${vtysh}=${ont['telnet_vtysh']}    ${pmt_login}=AONT${SPACE}login:    ${pmt_pwd}=assword:
    [Documentation]    open telnet connection, try 3 times.
    log    ${username} \ ${password} ${pmt_login} ${pmt_pwd}
    @{counter}=    evaluate    range(${times})
    : FOR    ${a}    IN    @{counter}
    \    ${passed}    ${telnet_index}=    Run Keyword And Ignore Error    l2-telnet.OpenConnection    ${ip}    ${port}
    \    ...    ${username}    ${password}    timeout=${timeout}    pmt_login=${pmt_login}    pmt_pwd=${pmt_pwd}
    \    Exit For Loop If    '${passed}' == 'PASS'
    \    Sleep    5s
    #If telnet fail, return the message
    Should Be True    '${passed}' == 'PASS'
    [Teardown]    Run Keywords    Set Global Variable    ${telnetresult}    ${telnet_index}
    ...    AND    Log    ${telnetresult}

SetPcIpv4
    [Arguments]    ${ip}    ${nic}='eth2'
    [Documentation]    == Discription ==
    ...    ---
    ...    *Author :* Alfred <Xin.Q.Zhang@alcatel-sbell.com.cn>
    ...
    ...    *Date :* 2017-04-27
    ...
    ...    This keyword used to set IPv4 address for a specifid NIC, The default NIC is _*eth2*_ . It *return* the _*bakup_file_name*_ of ifcfg
    ...
    ...    ---
    ...    == Examples ==
    ...    For example, if you want to set eth1 to ip 10.18.90.90, then you should use it like:
    ...    | *SetPcIpv4* | 10.18.90.90 | eth1 |
    ...
    ...    This will cause the /etc/sysconfig/network-scripts/ifcfg-eth1 be modified, and reboot the NIC eth1. So, please do not forget to restore your setting if this modification is only for this test. if you want do restore . Just like this is ok
    ...
    ...    | ${bakup} | *SetPcIpv4* | 10.18.90.90 | eth1 |
    ...    | ... | ;# No meter how much job has been done here... |
    ...    | *SetPcIpv4* | ${bakup} |
    ...
    ...    Do you get it ?
    ...    ---
    ...    == Customers ==
    ...
    ...    GOMMON
    ...
    ...    ---
    ...    == History ==
    ...
    ...    *Rev 0:* 2017-04-27 _Alfred Zhang_ Create this keyword. For fix [http://135.251.200.225/project/issues/382 | BUG 303]
    ${cmd}=    Set Variable    %{ROBOTREPO}${/}HLK${/}lib${/}SetPcIP
    Run Process    test -x ${cmd}    shell=True    alias=test
    ${result}    Get Process Result    test
    Run Keyword If    ${result.rc} != ${0}    Fail    ${result.stderr}
    Run Process    ${cmd} ${ip} ${nic}    shell=True    alias=setlanip
    ${result}    Get Process Result    setlanip
    log Many    ${result.rc}
    log Many    ${result.stdout}
    log Many    ${result.stderr}
    Should Be Equal    ${result.rc}    ${0}

SetMirrorEnable
    [Arguments]    ${SwitchBit}=1    ${SourcePort}=5    ${DestPort}=${None}
    [Documentation]    This keyword is used to mirror all WAN Connection to LAN Port
    ...
    ...    Author: Zhang Yaping
    ...
    ...    == History ==
    ...    _*Rev 0:*_ 5. Octomber, 2017 --- Zhang Yaping \ <yaping.zhang@nokia-sbell.com>: Create this keyword.
    ${SwitchBit}    Evaluate    bool(${SwitchBit})
    ${SwitchBit}    SetVariableIf    ${SwitchBit}    1    0
    ${DestPort}    SetVariableIf    ${DestPort}==${None}    ${ont['pc_port']}    ${DestPort}
    #BroadComm cmd is \ "halmgr lp set_lp_mirror 1 5 ${DestPort}"
    #BroadLight cmd is "mirror -i wan_0 -d lan_0 -t both"
    ${isBCM}=    Run Keyword And Return Status    Should Match Regexp    ${ont['hardware']}    G240WB|G240WA|G240WC
    ${cmd}    SetVariableIf    ${isBCM}    halmgr lp set_lp_mirror \ ${SwitchBit} ${SourcePort} ${DestPort}    mirror -i wan_0 -d lan_0 -t both
    log    ${DestPort}
    L3-Selenium2.LoginGUI    ${ont['gui_type']}    ${ont['url_protocol']}://${ont['ip']}    ${ont['gui_username']}    ${ont['gui_password']}
    L3-Selenium2.SetTelnet    enable
    L3-Selenium2.LogoutGUI    ${ont['gui_type']}    ${ont['url_protocol']}://${ont['ip']}
    L3-Telnet.EXECommand    ${cmd}
