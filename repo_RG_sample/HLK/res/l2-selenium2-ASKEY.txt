*** Settings ***
Resource          %{ROBOTREPO}/HLK/res/l1-selenium2.txt
Library           %{RFPATH}/Python-2.7/lib/python2.7/site-packages/robot/libraries/Collections.py
Library           %{RFPATH}/Python-2.7/lib/python2.7/site-packages/robot/libraries/String.py

*** Keywords ***
LoginGUI
    [Arguments]    ${base_url}    ${gui_username}    ${gui_password}
    ${ip}    GetSubstring    ${base_url}    7
    ${base_url}    SetVariable    http://${gui_username}:${gui_password}@${ip}
    log    ${base_url}
    L1-Selenium2.OpenBrowser    ${base_url}
    sleep    1s

LogoutGUI
    [Arguments]    ${base_url}
    L1-Selenium2.GoTo    ${base_url}/logout.cgi?isLogout=1
    sleep    1s
    L1-Selenium2.CloseBrowser
    [Teardown]

ConfigTR069
    [Arguments]    ${base_url}    ${acs_url}    ${acs_username}    ${acs_password}    ${connection_request_username}    ${connection_request_password}
    Comment    L1-Selenium2.GoTo    ${base_url}/tr69.cgi
    UnselectFrame
    SelectFrame    name=mainMenuFrm
    L1-Selenium2.ClickLink    //a[@href="managementViewSystem.cmd?action=view"]
    sleep    0.3s
    UnselectFrame
    SelectFrame    name=basefrm
    sleep    0.3s
    L1-Selenium2.ClickLink    //a[@href="managementViewSystem.cmd?acion=view"]
    ClickElement    //input[@name="inform"][2]
    L1-Selenium2.InputText    name=informInterval    86400
    L1-Selenium2.InputText    name=tr069AcsUrl    ${acs_url}
    L1-Selenium2.InputText    name=tr069AcsUsername    ${acs_username}
    L1-Selenium2.InputText    name=tr069AcsPassword    ${acs_password}
    L1-Selenium2.SelectCheckbox    name=connReqAuth
    sleep    0.3s
    L1-Selenium2.InputText    name=connReqUser    ${connection_request_username}
    L1-Selenium2.InputText    name=connReqPwd    ${connection_request_password}
    Choose Ok On Next Confirmation
    L1-Selenium2.ClickButton    xpath=//input[@value="Apply/Save"]
    sleep    3s
    Confirm Action
    sleep    3s

DeleteAllConnection
    [Arguments]    ${base_url}
    UnselectFrame
    SelectFrame    name=mainMenuFrm
    L1-Selenium2.ClickLink    //a[@href="wancfg.cmd"]
    sleep    0.3s
    UnselectFrame
    SelectFrame    name=basefrm
    sleep    0.3s
    L1-Selenium2.ClickLink    //a[@href="wancfg.cmd"]
    Sleep    0.3s
    ${status}    ${connection_list}    Run Keyword And Ignore Error    GetWebElements    name=rml
    log    status is ${status}
    log    out put result\n @{connection_list}
    ReturnFromKeywordIf    '${status}'=='FAIL'    OK
    : FOR    ${e}    IN    @{connection_list}
    \    sleep    1s
    \    Comment    ${e}    GetFromList    ${e}    0
    \    Selenium2Library.Select Checkbox    ${e}
    \    Sleep    0.4s
    L1-Selenium2.CLickButton    //input[@value="Remove"]

CreateDHCPConnection
    [Arguments]    ${base_url}    ${vlan_id}    @{wan_services}
    [Documentation]    2015-10-23 :
    ...    Modifier : Alfred Zhang CSL: \ xqzhang \ , ASB
    ...
    ...    I reuse ${base_url} as a type multiplexing argument. Because for TELMEX GUI, the WAN connection can not be delete, but you can modify it. I think may be it's a good idea to reuse ${base_url} to resolve it.
    ...
    ...    e.g : If you want to create an DHCP connection as the second wan connection, then you shuold :
    ...    | _${base_url}_ | =SetVariable= | _M2_ |
    ...    | _@{services}_ | =CreateList= | _TR069_ | _INTERNET_ |
    ...    | =CreateDHCPConnection= | _${baseurl}_ | _102_ | _@{service}_ |
    ...
    ...    This will create an DHCP WAN connection with index 2, vlan 102, for service TR069 and service INTERNET.
    UnselectFrame
    SelectFrame    name=mainMenuFrm
    L1-Selenium2.ClickLink    //a[@href="wancfg.cmd"]
    sleep    0.3s
    UnselectFrame
    SelectFrame    name=basefrm
    sleep    0.3s
    L1-Selenium2.ClickLink    //a[@href="wancfg.cmd"]
    Sleep    0.3s
    ${type}    Get_Regexp_Matches    ${base_url}    M(.+)    1
    ${res}    RunKeywordAndReturnStatus    ShouldNotBeEmpty    ${type}
    ${idx}    Run_Keyword_If    ${res}==True    Collections.GetFromList    ${type}    0
    ...    ELSE    SetVariable    NEW
    RunKeywordIf    '${idx}'!='NEW'    L1-Selenium2.ClickButton    /html/body/blockquote/form/table/tbody/tr[${idx}]/td[-2]    ELSE    L1-Selenium2.ClickButton   //input[@value="Add"] 
    Sleep    1s
    ${WanModes}    GetWebElements    //input[@name="ntwkPrtcl"]
    ${DHCPMode}    GetFromList    ${WANModes}    1
    ClickElement    ${DHCPMode}
    Sleep    1s
    # @{ServiceDict}    Create List    OTHER    VOIP    TR069    TR069,VOIP    INTERNET
    # ...    VOIP,INTERNET    TR069,INTERNET    TR069,VOIP,INTERNET
    # ${key}=    Evaluate    int(0)+0
    Run_Keyword_If    'VOIP' in @{wan_services}    SelectCheckbox    //input[@name="seviceTypeVoIP"]
    ...    ELSE    UnselectCheckbox    //input[@name="seviceTypeVoIP"]
    Run_Keyword_If    'TR069' in @{wan_services}    SelectCheckBox    //input[@name="seviceTypeMgmt"]
    ...    ELSE    UnselectCheckbox    //input[@name="seviceTypeMgmt"]
    Run_Keyword_If    'INTERNET' in @{wan_services}    SelectCheckBox    //input[@name="seviceTypeHSI"]
    ...    ELSE    UnselectCheckBox    //input[@name="seviceTypeHSI"]
    Run_Keyword_If    'IPTV' in @{wan_services}    SelectCheckBox    //input[@name="seviceTypeIPTV"]
    ...    ELSE    UnselectCheckBox    //input[@name="seviceTypeIPTV"]
    UnselectCheckBox    name=enblDscpToPbit
    ${status}    RunKeywordAndReturnStatus    Element Should Not Be Visible    name=enblv6
    RunKeywordIf    ${status}=='False'    UnselectCheckBox    name=enblv6
    SelectCheckBox    name=enblv4
    SelectCheckBox    name=DefaultConectionService
    L1-Selenium2.Input Text    xpath=//input[@name="vlanMuxPr"]    1
    L1-Selenium2.Input Text    xpath=//input[@name="vlanMuxId"]    ${vlan_id}
    Sleep    1s
    L1-Selenium2.Click Button    xpath=//input[@value="Next"]
    Sleep    0.3s
    L1-Selenium2.Click Button    xpath=//input[@value="Next"]
    Sleep    0.3s
    SelectCheckBox    name=enblNat
    UnSelectCheckBox    name=enblFullcone
    SelectCheckBox    name=enblFirewall
    L1-Selenium2.Click Button    xpath=//input[@value="Next"]
    Sleep    0.3s
    SelectCheckBox    name=dnsOverrideAllowed
    L1-Selenium2.Click Button    xpath=//input[@value="Next"]
    Sleep    0.3s
    L1-Selenium2.Click Button    xpath=//input[@value="Apply/Save"]
    Sleep    3s
	
CreatePPPoEConnection
    [Arguments]    ${base_url}    ${vlan_id}    ${pppoe_username}    ${pppoe_password}    @{wan_services}
    [Documentation]    2015-10-23 :
    ...    Modifier : Alfred Zhang CSL: \ xqzhang \ , ASB
    ...
    ...    I reuse ${base_url} as a type multiplexing argument. Because for TELMEX GUI, the WAN connection can not be delete, but you can modify it. I think may be it's a good idea to reuse ${base_url} to resolve it.
    ...
    ...    e.g : If you want to create an DHCP connection as the second wan connection, then you shuold :
    ...    | _${base_url}_ | =SetVariable= | _M2_ |
    ...    | _@{services}_ | =CreateList= | _TR069_ | _INTERNET_ |
    ...    | =CreateDHCPConnection= | _${baseurl}_ | _102_ | _@{service}_ |
    ...
    ...    This will create an DHCP WAN connection with index 2, vlan 102, for service TR069 and service INTERNET.
    UnselectFrame
    SelectFrame    name=mainMenuFrm
    L1-Selenium2.ClickLink    //a[@href="wancfg.cmd"]
    sleep    0.3s
    UnselectFrame
    SelectFrame    name=basefrm
    sleep    0.3s
    L1-Selenium2.ClickLink    //a[@href="wancfg.cmd"]
    Sleep    0.3s
    ${type}    Get_Regexp_Matches    ${base_url}    M(.+)    1
    ${res}    RunKeywordAndReturnStatus    ShouldNotBeEmpty    ${type}
    ${idx}    Run_Keyword_If    ${res}==True    Collections.GetFromList    ${type}    0
    ...    ELSE    SetVariable    NEW
    RunKeywordIf    '${idx}'!='NEW'    L1-Selenium2.ClickButton    /html/body/blockquote/form/table/tbody/tr[${idx}]/td[-2]    ELSE    L1-Selenium2.ClickButton   //input[@value="Add"] 
    Sleep    1s
    ${WanModes}    GetWebElements    //input[@name="ntwkPrtcl"]
    ${PPPoEMode}    GetFromList    ${WANModes}    0
    ClickElement    ${PPPoEMode}
    Sleep    1s
    Run_Keyword_If    'VOIP' in @{wan_services}    SelectCheckbox    //input[@name="seviceTypeVoIP"]
    ...    ELSE    UnselectCheckbox    //input[@name="seviceTypeVoIP"]
    Run_Keyword_If    'TR069' in @{wan_services}    SelectCheckBox    //input[@name="seviceTypeMgmt"]
    ...    ELSE    UnselectCheckbox    //input[@name="seviceTypeMgmt"]
    Run_Keyword_If    'INTERNET' in @{wan_services}    SelectCheckBox    //input[@name="seviceTypeHSI"]
    ...    ELSE    UnselectCheckBox    //input[@name="seviceTypeHSI"]
    UnselectCheckBox    name=enblDscpToPbit
    ${status}    RunKeywordAndReturnStatus    Element Should Not Be Visible    name=enblv6
    log    Status is ${status}
    RunKeywordIf    ${status}=='False'    UnselectCheckBox    name=enblv6
    SelectCheckBox    name=enblv4
    SelectCheckBox    name=DefaultConectionService
    L1-Selenium2.Input Text    xpath=//input[@name="vlanMuxPr"]    0
    L1-Selenium2.Input Text    xpath=//input[@name="vlanMuxId"]    ${vlan_id}
    Sleep    1s
    L1-Selenium2.Click Button    xpath=//input[@value="Next"]
    Sleep    0.3s
    L1-Selenium2.Input Text    xpath=//input[@name='pppUserName']    ${pppoe_username}
    L1-Selenium2.Input Text    xpath=//input[@name='pppPassword']    ${pppoe_password}
    SelectCheckbox    name=enblNat
    UnselectCheckbox    name=enblFullcone
    UnselectCheckbox    name=enblFirewall
    UnselectCheckbox    name=enblOnDemand
    UnselectCheckbox    name=enblPppIpAddress
    UnselectCheckbox    name=enblPppDebug
    UnselectCheckbox    name=pppToBridge
    L1-Selenium2.Click Button    xpath=//input[@value="Next"]
    Sleep    0.3s
    SelectCheckbox    name=dnsOverrideAllowed
    L1-Selenium2.Click Button    xpath=//input[@value="Next"]
    Sleep    0.3s	
    L1-Selenium2.Click Button    xpath=//input[@name="btnSave"]
    Sleep    3s

GetWANStatus
    [Arguments]    ${base_url}    ${service_name}
    Comment    L1-Selenium2.GoTo    ${base_url}/wan_config.cgi?status
    UnselectFrame
    SelectFrame    name=mainMenuFrm
    L1-Selenium2.ClickLink    //a[@href="wancfg.cmd"]
    sleep    0.3s
    UnselectFrame
    SelectFrame    name=basefrm
    sleep    0.3s
    L1-Selenium2.ClickLink    //a[@href="wancfg.cmd"]
    Sleep    0.3s
    @{ConnElements}=    GetWebElements    //html/body/blockquote/form/table/tbody/tr
    ${NumOfConns}=    GetLength    ${ConnElements}
    @{NumOfConns}=    Evaluate    range(1,${NumOfConns}+1)
    ${RowIndex}=    SetVariable    0
    :For    ${i}    IN    @{NumOfConns}
    \    ${ConnName}    GetText   //html/body/blockquote/form/table/tbody/tr[${i}]/td[4]
    \    ${status}    Run Keyword And Return Status    Should Contain    ${ConnName}    ${service_name}
    \    ${RowIndex}    SetVariable    ${i} 
    \    ExitForLoopIf    ${status}==True  
    Should Not Be Equal As Strings    ${RowIndex}    NONE    not find list item with internet service name
    ${vlan}=    GetText    //html/body/blockquote/form/table/tbody/tr[${i}]/td[6]
    ${wan_link_status}=    GetText    //html/body/blockquote/form/table/tbody/tr[${i}]/td[13]
    ${ip}=    GetText    //html/body/blockquote/form/table/tbody/tr[${i}]/td[14]
    ${gateway}=    GetText    //html/body/blockquote/form/table/tbody/tr[${i}]/td[15]
    ${primary_dns}=    GetText    //html/body/blockquote/form/table/tbody/tr[${i}]/td[17]
    ${second_dns}=    GetText    //html/body/blockquote/form/table/tbody/tr[${i}]/td[17]
    UnselectFrame
    SelectFrame    name=mainMenuFrm
    L1-Selenium2.ClickLink    //a[@href="ontViewGpon.cmd?action=view"]
    sleep    0.3s
    UnselectFrame
    SelectFrame    name=basefrm
    sleep    0.3s
    ${pon_link_status}=    GetText    //blockquote/form/b/table[3]/tbody/tr/td/b/table/tbody/tr[2]/td[1]
    ${pDict}=    Create Dictionary    vlan=${vlan}    wan_link_status=${wan_link_status}    ip=${ip}    gateway=${gateway}    primary_dns=${primary_dns}
    ...    second_dns=${second_dns}    pon_link_status=${pon_link_status}
    Return From Keyword    ${pDict}

UpgradeFirmware
    [Arguments]    ${base_url}    ${file_path}
    L1-Selenium2.Go To    ${base_url}upgrade.cgi
    L1-Selenium2.ChooseFile    name=filename    ${file_path}
    sleep    1s
    L1-Selenium2.Click Button    xpath=//input[@type='submit']
    Sleep    180s

AddDomainResolution
    [Arguments]    ${base_url}    ${domain_name}    ${ip}
    Comment    L1-Selenium2.Go To    ${base_url}/dns.cgi
    Comment    Sleep    2s
    Comment    L1-Selenium2.Input Text    xpath=//input[@name='domainName']    ${domain_name}
    Comment    Sleep    1s
    Comment    L1-Selenium2.Input Text    xpath=//input[@name='domainAddr']    ${ip}
    Comment    Sleep    1s
    Comment    L1-Selenium2.ClickButton    xpath=//input[@id='add1']
    Comment    Sleep    3s
    Comment    L1-Selenium2.TableColumnShouldContain    list1    1    ${domain_name}
    Comment    L1-Selenium2.TableColumnShouldContain    list1    3    ${ip}
    Log    "This Keyword can not run in this DUT"    WARN

AddDomainMapping
    [Arguments]    ${base_url}    ${origin_domain}    ${new_domain}
    Comment    L1-Selenium2.Go To    ${base_url}/dns.cgi
    Comment    Sleep    2s
    Comment    L1-Selenium2.Input Text    xpath=//input[@name='origindomain']    ${origin_domain}
    Comment    Sleep    1s
    Comment    L1-Selenium2.Input Text    xpath=//input[@name='newdomain']    ${new_domain}
    Comment    Sleep    1s
    Comment    L1-Selenium2.ClickButton    xpath=//input[@id='add2']
    Comment    Sleep    3s
    Comment    L1-Selenium2.TableColumnShouldContain    list2    1    ${origin_domain}
    Comment    L1-Selenium2.TableColumnShouldContain    list2    2    ${new_domain}
    Log    "This Keyword can not run in this DUT"    WARN

ClearDomainResolution
    [Arguments]    ${base_url}
    [Documentation]    this is stupid keyword, but no better method now.
    ...    Delete Domain Resolution id 0-7.
    Comment    L1-Selenium2.Go To    ${base_url}/dns.cgi
    Comment    Sleep    2s
    Comment    : FOR    ${index}    IN RANGE    5
    Comment    \    L1-Selenium2.Go To    ${base_url}/dns.cgi?act=del&oid=${index}
    Comment    \    sleep    2s
    Log    "This Keyword can not run in this DUT"    WARN

ClearDomainMapping
    [Arguments]    ${base_url}
    [Documentation]    this is stupid keyword, but no better method now.
    ...    Delete Domain Mapping id 0-7.
    Comment    L1-Selenium2.Go To    ${base_url}/dns.cgi
    Comment    Sleep    2s
    Comment    : FOR    ${index}    IN RANGE    5
    Comment    \    L1-Selenium2.Go To    ${base_url}/dns.cgi?act=dell&oidd=${index}
    Comment    \    sleep    2s
    Log    "This Keyword can not run in this DUT"    WARN
	
ConfigFirewall
    [Arguments]    ${base_url}    ${status}    ${level}
    [Documentation]   ${level} has three opthions : low, medium, high
    Log    "This Keyword can not run in this DUT"    WARN


RebootDut
    [Arguments]    ${mode}
    UnselectFrame
    SelectFrame    
    sleep    0.3s
    l2-Selenium2.ClickLink    //a[@href="managementViewSystem.cmd?action=view"]
    sleep    0.3s
    UnselectFrame
    SelectFrame    name=mainMenuFrm
    sleep    0.3s
    RunKeywordIf    '${mode}'=='RESET'        l2-Selenium2.ClickLink    //a[@href="managementViewSetting.cmd?acion=view"]
    ...    ELSE    l1-selenium2.ClickLink    //a[@href="managementViewReboot.cmd?acion=view"]
    RunKeywordIf    '${mode}'=='RESET'        l2-Selenium2.ClickButton    //input[@value="Restore Default Settings"]
    ...    ELSE    l1-selenium2.ClickButton    //input[@value="Reboot"]
    Sleep    20s
    CloseBrowser