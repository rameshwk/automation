import time, datetime, os, sys
import subprocess

def getTime():
    return "[%s]: " % datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')

def PrintResult(result):
    print "%s ========================" % getTime()
    print "%s RESULT:%s" % (getTime(), result.upper())
    print "%s ========================" % getTime()
    
def TraceError():
    info = sys.exc_info()
    print "%s ***************Traceback: Error Info*******************" % getTime()
    for opt in info:
        print "%s      %s" % (getTime(), str(opt))
    print "%s *******************************************************" % getTime()

def ExecuteShell(lCmd, sDir="", timeout="yes"):
    if "" != sDir:
        os.chdir("%s" % sDir)
    lResult = []
    for sCmd in lCmd:
        if "yes" == timeout:
            sResult = "".join(os.popen(sCmd).readlines())
        else:
            os.popen(sCmd)
            sResult = ""
        lResult.append(sCmd)
        lResult.append(sResult)
    print "\n".join(lResult)

def VconfigCard(basic_ip, sCard, iVlan):
    lCmdConfig = []
    ipH = basic_ip[ 0 : basic_ip.rfind(".")+1 ]
    ipL = basic_ip[ basic_ip.rfind(".")+1 :]
    ipLn = abs(int(ipL) - 100) + 5
    card_ip = "%s%s" % (ipH, ipLn)
    lCmdConfig.append("modprobe 8021q")
    lCmdConfig.append("vconfig add %s %s" % (sCard, iVlan))
    lCmdConfig.append("/sbin/ifconfig %s 0.0.0.0" % sCard)
    lCmdConfig.append("/sbin/ifconfig %s.%s %s netmask 255.255.255.0 up" % (sCard, iVlan, card_ip))
    ExecuteShell(lCmdConfig)

def CleanVconfigCard(sCard, iVlan):
    lCmdClean = []
    lCmdClean.append("vconfig rem %s.%s" % (sCard, iVlan))
    lCmdClean.append("/sbin/ifup %s" % sCard)
    ExecuteShell(lCmdClean)

def TIMEOUT_COMMAND(command, timeout=5):  
    """
    call shell-command and either return its output or kill it
    if it doesn't normally exit within timeout seconds and return None
    """
    import subprocess, datetime, os, time, signal
    cmd = command.split(" ")
    start = datetime.datetime.now()
    process = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    while process.poll() is None:
        time.sleep(0.2)
        now = datetime.datetime.now()
        if (now - start).seconds> timeout:
            os.kill(process.pid, signal.SIGKILL)
            os.waitpid(-1, os.WNOHANG)
            return None
    return process.stdout.readlines()

