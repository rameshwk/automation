
import psutil
import sys
import os

h = open("process_listing.pid", 'w')
h.write(str(os.getpid()))
h.close()

try:
    run_count = 0
    while (run_count < 20000):
        run_count = run_count + 1
        sys.stdout.write("\r\r%s" % run_count)
        sys.stdout.flush()

        try:
            cmdline = psutil.Process(0).cmdline
        except Exception, e:
            print "\n"
            print "Exception for PID 0: %s" % e
            continue

        PID = -1
        for i in psutil.get_process_list():
            PID = i.pid
            try:
                cmdline = i.cmdline
            except psutil.NoSuchProcess:
                continue
            except Exception, e:
                print "\n"
                print "Exception for PID %s: %s" % (PID, e)
                continue

except:
    os.remove("process_listing.pid")
