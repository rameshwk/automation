#!/usr/bin/env python

import os
import gc
import unittest

import psutil

LOOPS = 250


class TestCase(unittest.TestCase):

    def test_memory_leak(self):
        # step 1
        for x in xrange(LOOPS):
            p = psutil.Process(os.getpid())
            str(p)
            p.create_time
            p.get_cpu_times()
            p.get_cpu_percent()
            p.get_memory_info()
            p.get_memory_percent()
        del p
        gc.collect()
        rss1, vms1 = psutil.Process(os.getpid()).get_memory_info()

        # step 2
        for x in xrange(LOOPS):
            p = psutil.Process(os.getpid())
            str(p)
            p.create_time
            p.get_cpu_times()
            p.get_cpu_percent()
            p.get_memory_info()
            p.get_memory_percent()
        del p
        gc.collect()
        rss2, vms2 = psutil.Process(os.getpid()).get_memory_info()

        # result
        print
        print rss1, vms1
        print rss2, vms2
        self.assertEqual(rss1, rss2)
        self.assertEqual(vms1, vms2)


def test_main():
    test_suite = unittest.TestSuite()
    test_suite.addTest(unittest.makeSuite(TestCase))
    unittest.TextTestRunner(verbosity=2).run(test_suite)

if __name__ == '__main__':
    test_main()
