#!/usr/bin/env python

import psutil
import os
import subprocess

DEVNULL = open(os.devnull, 'r+')
proc = subprocess.Popen("/bin/ls", stdout=DEVNULL, stderr=DEVNULL)
currp = psutil.Process(proc.pid)
proc.wait()

try:
    str(currp.get_cpu_times())
except psutil.NoSuchProcess:
    print "OK"
else:
    print "FAILED"
