#-*- coding:utf-8 -*-

"""
the template script only works for sepcified HGU, including all kinds of function for webpage configration,
each time we add new feacture to script, we should do as the belows:
    1). update parameters to '[BLOCK: INIT]'
    2). update feature calling order of the script to '[BLOCK: MAIN]'
    3). update sepcific feature function to '[BLOCK: FUNCTION]'

we should follow and keep the same format whatever we add or update any features to script.
"""
# import build-in models
import os, time, sys
import getopt, inspect

# import third-part models
from selenium import webdriver
from selenium.webdriver.support.ui import Select

# import custom models
sys.path.append(os.path.dirname(sys.path[0]))
from LIB.common import getTime,PrintResult,TraceError

reload(sys)
sys.setdefaultencoding('utf-8')

'''
*********************************************************************************************************************************
[BLOCK: FUNCTION]
All related feature functions which operate webpage will be defined as belows, we should keep the same format when we create them
*********************************************************************************************************************************
'''

def login():
    """
    name: login
    desc: login to webpage using correct account
    """
    
    global driver, base_url, web_username, web_password
    
    print "%s %s START" % (getTime(), sys._getframe().f_code.co_name)
    driver.get(base_url + "?lang=eng")
    time.sleep(1)
    driver.refresh()
    time.sleep(1)
    driver.find_element_by_id("username").clear()
    driver.find_element_by_id("username").send_keys(web_username)
    driver.find_element_by_id("password").clear()
    driver.find_element_by_id("password").send_keys(web_password)
    driver.find_element_by_xpath("//input[@value='Login']").click()
    try:
        driver.find_element_by_xpath("/html/body/form/font")
    except:
        print "%s login: Successed to login web" % getTime()
        time.sleep(3)
    else:
        print "%s login: Failed to login web" % getTime()
        raise Exception, driver.find_element_by_xpath("/html/body/form/font").text

def logout():
    """
    name: logout
    desc: logout webpage
    """
    
    global driver, base_url

    print "%s %s START" % (getTime(), sys._getframe().f_code.co_name)
    driver.get(base_url + "login.cgi?out")
    print "%s logout: Successed to logout" % getTime()

def create_wan():
    """
    name: create_wan
    desc: create all related wan connection, totally we will 
          create 3 kinds of service wan connection as belows:
             INTERNET, TR069, IPTV/OTHER
          all wan conections ip mode will be DHCP and use different
          vlan id to seperate each other.
    """
    
    global driver, base_url, tr069_vlan, internet_vlan, igmp_vlan
    
    print "%s %s START" % (getTime(), sys._getframe().f_code.co_name)
    driver.get(base_url + "wan_config.cgi")
    time.sleep(1)
    driver.refresh()
    time.sleep(1)
    
    serviceText = driver.find_element_by_name("conn_id").text
    serviceList = serviceText.strip().split("\n")
    for ser in serviceList:
        if "Create One New Connection" != ser:
            Select(driver.find_element_by_name("conn_id")).select_by_visible_text(ser)
            driver.find_element_by_id("do_del").click()
            time.sleep(2)
    
    # voip tr069 internet iptv
    # <-->
    # ty_ser1 ty_ser2 ty_ser3 ty_ser4
    service_tags = ["ty_ser2", "ty_ser3", "ty_ser4"]
    vlans = [tr069_vlan, internet_vlan, igmp_vlan]
    service_names = ["TR069", "INTERNET", "IPTV"]
    for vlan, service in zip(vlans, service_tags):
        Select(driver.find_element_by_name("conn_id")).select_by_visible_text("Create One New Connection")
        time.sleep(1)
        if not driver.find_element_by_name(service).is_selected():
            driver.find_element_by_name(service).click()
        driver.find_element_by_name("vlanId").clear()
        driver.find_element_by_name("vlanId").send_keys(vlan)
        if not driver.find_element_by_name("conn_en").is_selected():
            driver.find_element_by_name("conn_en").click()
        if not driver.find_element_by_name("natSw").is_selected():
            driver.find_element_by_name("natSw").click()
        if not driver.find_element_by_name("vlanSw").is_selected():
            driver.find_element_by_name("vlanSw").click()
        driver.find_element_by_name("m8021p").clear()
        driver.find_element_by_name("m8021p").send_keys("0")
        Select(driver.find_element_by_name("ip_mode")).select_by_visible_text("DHCP")
        driver.find_element_by_id("do_edit").click()
        time.sleep(5)

    print "%s finished to creat all wan connections with its details as format of (service name, vlan) %s" % \
                (getTime(), zip(service_names, vlans))

def config_loid():
    """
    name: modify_loid
    desc: configure loid on webpage, if webpage does not support
          we should do nothing and return directly
    """
    
    global driver, base_url, log_auth_id, log_auth_pwd
    
    print "%s %s START" % (getTime(), sys._getframe().f_code.co_name)
    driver.get(base_url + "reg.cgi?loidcfg")
    time.sleep(1)
    driver.refresh()
    time.sleep(1)
    driver.find_element_by_name("loid").clear()
    driver.find_element_by_name("loid").send_keys(log_auth_id)
    driver.find_element_by_name("pswd").clear()
    driver.find_element_by_name("pswd").send_keys(log_auth_pwd)
    driver.find_element_by_xpath("//input[@value='Save/Apply']").click()
    time.sleep(2)
    print "%s finished to config loid and password: %s, %s" % \
                (getTime(), log_auth_id, log_auth_pwd)
    
def config_slid():
    """
    name: modify_slid
    desc: configure slid on webpage, if webpage does not support
          we should do nothing and return directly
    """

    global driver, base_url, subslocid
    
    print "%s %s START" % (getTime(), sys._getframe().f_code.co_name)
    driver.get(base_url + "gpon_config.cgi")
    time.sleep(1)
    driver.refresh()
    time.sleep(1)
    driver.find_element_by_name("pswd_new").clear()
    driver.find_element_by_name("pswd_new").send_keys(subslocid)
    Select(driver.find_element_by_name("pswd_mode")).select_by_value("1"); # 1:ASCII Mode, 0:HEX Mode
    driver.find_element_by_xpath("//input[@value='Save']").click()
    time.sleep(2)
    print "%s finished to config slid: %s" % \
                (getTime(), subslocid)

def config_igmp_vlan_version():
    """
    name: config_igmp_vlan_version
    desc: check if we need to config igmp vlan and its version,
          since some Telecom ONT need to set igmp vlan and specify
          igmp version on webpage, but for Telmex ONT don't need to
          do as this, so if don't need, we should do nothing and 
          return directly
    """

    global driver, base_url, igmp_vlan, igmp_version
    
    print "%s %s START" % (getTime(), sys._getframe().f_code.co_name)
    print "%s this ONT don't need to config igmp vlan and version on webpage" % getTime()

def config_tr069():
    """
    name: config_tr069
    desc: configure tr069 on webpage, if webpage does not support
          we should do nothing and return directly
    """

    global driver, base_url, acs_url, acs_user, acs_pswd, conn_req_user, conn_req_pswd

    print "%s %s START" % (getTime(), sys._getframe().f_code.co_name)
    driver.get(base_url + "tr69.cgi")
    time.sleep(1)
    driver.refresh()
    time.sleep(1)

    if not driver.find_element_by_name("inform_enable").is_selected():
        driver.find_element_by_name("inform_enable").click()

    driver.find_element_by_name("informInterval").clear()
    driver.find_element_by_name("informInterval").send_keys(43200)

    driver.find_element_by_name("acsURL").clear()
    driver.find_element_by_name("acsURL").send_keys(acs_url)

    driver.find_element_by_name("acsUser").clear()
    driver.find_element_by_name("acsUser").send_keys(acs_user)

    driver.find_element_by_name("acsPswd").clear()
    driver.find_element_by_name("acsPswd").send_keys(acs_pswd)

    driver.find_element_by_name("connReqUser").clear()
    driver.find_element_by_name("connReqUser").send_keys(conn_req_user)

    driver.find_element_by_name("connReqPswd").clear()
    driver.find_element_by_name("connReqPswd").send_keys(conn_req_pswd)

    driver.find_element_by_xpath("//input[@value='Save']").click()
    time.sleep(1)
    print '''%s finished to config tr069: Periodic Inform Enable => Enable,
                        Periodic Inform Interval(s) =>43200,URL => %s, Username => %s, Password => %s,
                        Connect Request Username => %s ,Connect Request Password => %s''' % \
                            (getTime(), acs_url, acs_user, acs_pswd, conn_req_user, conn_req_pswd)

'''
*********************************************************************************************************************************
[BLOCK: INIT]
read input script parameters and initial them
*********************************************************************************************************************************
'''
param = {}
lInput = [
            "web_ip=",
            "web_username=",
            "web_password=",
            "log_auth_id=",
            "log_auth_pwd=",
            "subslocid=",
            "tr069_vlan=",
            "internet_vlan=",
            "igmp_vlan=",
            "igmp_version=",
            "acs_url=",
            "acs_user=",
            "acs_pswd=",
            "conn_req_user=",
            "conn_req_pswd=",
          ]
try:
    opts, args = getopt.getopt(sys.argv[1:], "hw:", lInput)
except Exception as msg:
    print "%s error message: %s" % (getTime(), msg)
    print "%s illegal option: %s" % (getTime(), str(sys.argv[1:]))
    PrintResult("FAIL")
    sys.exit("FAIL")

for opt, value in opts:
    for item in lInput:
        tag = item.replace("=", "")
        if "--%s" % tag == opt:
            param[tag] = value
            exec("%s='%s'" % (tag, value))

for item in lInput:
    tag = item.replace("=", "")
    if not param.has_key(tag):
        param[tag] = "x"
        exec("%s='%s'" % (tag, "x"))

'''
*********************************************************************************************************************************
[BLOCK: MAIN]
access of calling feature
*********************************************************************************************************************************
'''
print "%s input params for script: %s" % (getTime(), param)
if "x" == web_ip or "x" == web_username or "x" == web_password:
    print "%s havent provided all of params: web_ip & web_username & web_password, will skip running and exit script" % getTime()
    PrintResult("FAIL")
    sys.exit("FAIL")

try:
    driver = webdriver.Remote("http://localhost:4444/wd/hub", webdriver.DesiredCapabilities.FIREFOX)
    driver.implicitly_wait(5)
    base_url = "http://%s/" % web_ip
    
    # login to webpage
    login()
    
    # create wan connection
    if "x" != tr069_vlan and "x" != internet_vlan and "x" != igmp_vlan:
        create_wan()
    else:
        print "%s ignore create wan connection, due to havent provided all of params: tr069_vlan & internet_vlan & igmp_vlan" % getTime()
    
    # config loid and password
    if "x" != log_auth_id and "x" != log_auth_pwd:
        config_loid()
    else:
        print "%s ignore config loid and password, due to havent provided all of params: log_auth_id&log_auth_pwd" % getTime()
    
    # config slid
    if "x" != subslocid:
        config_slid()
    else:
        print "%s ignore config subslocid, due to havent provided all of params: subslocid" % getTime()

    # config igmp vlan and igmp version
    if "x" != igmp_vlan and "x" != igmp_version:
        config_igmp_vlan_version()
    else:
        print "%s ignore config igmp vlan version, due to havent provided all of params: igmp_vlan&igmp_version" % getTime()

    # config tr069
    if "x" != acs_url and "x" != acs_user and "x" != acs_pswd and "x" != conn_req_user and "x" != conn_req_pswd:
        config_tr069()
    else:
        print "%s ignore config tr069, due to havent provided all of params: acs_url or&acs_user&acs_pswd&conn_req_user&conn_req_pswd" % getTime()

    result = "PASS"
    PrintResult("PASS")
except:
    TraceError()
    result = "FAIL"
    PrintResult("FAIL")
finally:
    # logout webpage
    try:logout()
    except:pass
        
    # quit selenium
    try:driver.quit()
    except:pass

# exit script and return result
sys.exit(result)

