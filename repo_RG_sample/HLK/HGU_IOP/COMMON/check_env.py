# -*- coding: utf-8 -*-

import re, os, sys, time

sys.path.append(os.path.dirname(sys.path[0]))
from LIB.common import ExecuteShell

sCurDir = os.path.abspath('.')
sREPO = os.environ["ROBOTREPO"]

#==================================================================
# get process id
#==================================================================
def processinfo(x):
    p = psutil.get_process_list()
    for r in p:
        aa = str(r)
        f = re.compile(x,re.I)
        if f.search(aa):
            return aa.split(';')[0].split(':')[1]

#==================================================================
# check psutil
#==================================================================
try:
    import psutil
except:
    lCmdPsu = []
    sDir = "%s/HLK/HGU_IOP/Software_Install/psutil-0.1.2" % sREPO
    lCmdPsu.append("python setup.py install")
    ExecuteShell(lCmdPsu, sDir)
    time.sleep(5)
    
try:
    import psutil
except Exception as msg:
    print "error msg: %s" % msg
    print "========================"
    print "RESULT:FAIL"
    print "========================"
    sys.exit("FAIL")

#==================================================================
# check setuptools & selenium
#==================================================================
try:
    from selenium import webdriver
except:
    sDir = "%s/HLK/HGU_IOP/Software_Install/setuptools-0.6c11" % sREPO
    lCmdTool = []
    lCmdTool.append("python setup.py install")
    ExecuteShell(lCmdTool, sDir)
    sDir = "%s/HLK/HGU_IOP/Software_Install/selenium-2.25.0" % sREPO
    lCmdSelenium = []
    lCmdSelenium.append("python setup.py install")
    ExecuteShell(lCmdPsu, sDir)
    time.sleep(5)
    
#==================================================================
# check selenium-server-standalone-.*.jar
#==================================================================
pid = processinfo("selenium-server-standalone-.*\.jar")
print "selenium-server pid: %s" % pid
if not pid:
    print "========================"
    print "RESULT:FAIL"
    print "========================"
    sys.exit("FAIL")
else:
    print "========================"
    print "RESULT:PASS"
    print "========================"
    sys.exit("PASS")
