import getopt, os, sys
from StringIO import StringIO
sys.path.append(os.path.dirname(sys.path[0]))
from LIB.common import *

sCurDir = os.path.abspath('.')
#==================================================================
# read script input parameters
#==================================================================
if __name__ == "__main__":
    try:
        opts, args = getopt.getopt(sys.argv[1:], "hb:", [
                          "basic_ip=",
                          "sCard=",
                          "iVlan=",
                          "help",
                     ])
    except getopt.GetoptError:
        print "illegal option: %s" % str(sys.argv[1:])
        sys.exit("FAIL")

    param = {}
    for opt, value in opts:
        if "--basic_ip" == opt:
            param["basic_ip"] = value
        if "--sCard" == opt:
            param["sCard"] = value
        if "--iVlan" == opt:
            param["iVlan"] = value

reload(sys)
sys.setdefaultencoding('utf-8')
buffer = StringIO()
temp = sys.stdout
sys.stdout = buffer

basic_ip = param["basic_ip"]
sCard = param["sCard"]
iVlan = param["iVlan"]

ipH = basic_ip[ 0 : basic_ip.rfind(".")+1 ]
ipL = basic_ip[ basic_ip.rfind(".")+1 : ]
ipLn = abs(int(ipL) - 100) + 5
card_ip = "%s%s" % (ipH, ipLn)

VconfigCard(card_ip, sCard, iVlan)

sys.stdout = temp
print buffer.getvalue()

print "========================"
print "RESULT:PASS"
print "========================"

