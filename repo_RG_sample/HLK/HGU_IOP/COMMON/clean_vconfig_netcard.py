import getopt, os, sys
from StringIO import StringIO
sys.path.append(os.path.dirname(sys.path[0]))
from LIB.common import *

sCurDir = os.path.abspath('.')
#==================================================================
# read script input parameters
#==================================================================
if __name__ == "__main__":
    try:
        opts, args = getopt.getopt(sys.argv[1:], "hb:", [
                          "sCard=",
                          "iVlan=",
                          "help",
                     ])
    except getopt.GetoptError:
        print "illegal option: %s" % str(sys.argv[1:])
        sys.exit("FAIL")

    param = {}
    for opt, value in opts:
        if "--sCard" == opt:
            param["sCard"] = value
        if "--iVlan" == opt:
            param["iVlan"] = value

reload(sys)
sys.setdefaultencoding('utf-8')
buffer = StringIO()
temp = sys.stdout
sys.stdout = buffer

sCard = param["sCard"]
iVlan = param["iVlan"]

CleanVconfigCard(sCard, iVlan)

sys.stdout = temp
print buffer.getvalue()

print "========================"
print "RESULT:PASS"
print "========================"

