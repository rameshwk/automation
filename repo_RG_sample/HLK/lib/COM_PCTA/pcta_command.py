import re,time,sys
from robot.api import logger
from copy import deepcopy
import com_pcta  
from robot.libraries.BuiltIn import BuiltIn
import logging

PCTA_SESSION = ""
network_side_traffic = []
user_side_traffic = []
network_side_traffic2 = []
network_side_trafficlag = []
PCTA_STATUS = ""

PCTA_NETWORK_NIC = ''
PCTA_CLIENT_NIC = ''

# Protocols Mapping Commands
Protocols = {

    'IP' : { 'start':'ta_traffic_start_ip', 
             'bursty':'ta_bursty_traffic_start_ip',
             'stop':'ta_traffic_stop_ip', 
             'send':'ta_send_ip_packet',
             'start_capture':'ta_start_capt_ip_packet', 
             'rtrv_capture':'ta_rtrv_capt_ip_packet',
             'stop_capture':'ta_stop_capt_ip_packet', 
             'start_measure':'ta_start_measurement_ip',
             'stop_measure':'ta_stop_measurement_ip' },

    'ETH' : { 'start':'ta_traffic_start_eth', 'bursty':'ta_bursty_traffic_start_eth', \
              'stop':'ta_traffic_stop_eth', 'send':'ta_send_eth_frame', \
              'start_capture':'ta_start_capt_eth_frame', 'rtrv_capture':'ta_rtrv_capt_eth_frame', \
              'stop_capture':'ta_stop_capt_eth_frame', 'start_measure':'ta_start_measurement_eth', \
              'stop_measure':'ta_stop_measurement_eth' },

    'BOOTP' : { 'start':'ta_traffic_start_bootp_dhcp', 'bursty':'x', \
                'stop':'ta_traffic_stop_bootp_dhcp', 'send':'ta_send_bootp_dhcp_message', \
                'start_capture':'ta_start_capt_bootp_dhcp_message', 'rtrv_capture':'ta_rtrv_capt_bootp_dhcp_message', \
                'stop_capture':'ta_stop_capt_bootp_dhcp_message', 'start_measure':'ta_start_measurement_bootp_dhcp', \
                'stop_measure':'ta_stop_measurement_bootp_dhcp' },

    'DHCP' : { 'start':'ta_traffic_start_bootp_dhcp', 'bursty':'x',
               'stop':'ta_traffic_stop_bootp_dhcp', 'send':'ta_send_bootp_dhcp_message',
               'start_capture':'ta_start_capt_bootp_dhcp_message',
               'rtrv_capture':'ta_rtrv_capt_bootp_dhcp_message',
               'stop_capture':'ta_stop_capt_bootp_dhcp_message',
               'start_measure':'ta_start_measurement_bootp_dhcp',
               'stop_measure':'ta_stop_measurement_bootp_dhcp',
               'packet_count':'message_count' 
              },

    'ARP' : { 'start':'ta_traffic_start_arp_rarp', 'bursty':'x', 'stop':'ta_traffic_stop_arp_rarp', 'send':'ta_send_arp_rarp_packet', 'start_capture':'ta_start_capt_arp_rarp_packet', 'rtrv_capture':'ta_rtrv_capt_arp_rarp_packet', 'stop_capture':'ta_stop_capt_arp_rarp_packet', 'start_measure':'ta_start_measurement_arp_rarp', 'stop_measure':'ta_stop_measurement_arp_rarp' },

    'RARP' : { 'start':'ta_traffic_start_arp_rarp', 'bursty':'x', 'stop':'ta_traffic_stop_arp_rarp', 'send':'ta_send_arp_rarp_packet', 'start_capture':'ta_start_capt_arp_rarp_packet', 'rtrv_capture':'ta_rtrv_capt_arp_rarp_packet', 'stop_capture':'ta_stop_capt_arp_rarp_packet', 'start_measure':'ta_start_measurement_arp_rarp', 'stop_measure':'ta_stop_measurement_arp_rarp' },

    'IGMP' : { 'start':'ta_traffic_start_igmp', 'bursty':'x', 'stop':'ta_traffic_stop_igmp', 'send':'ta_send_igmp_message', 'start_capture':'ta_start_capt_igmp_message', 'rtrv_capture':'ta_rtrv_capt_igmp_message', 'stop_capture':'ta_stop_capt_igmp_message', 'start_measure':'ta_start_measurement_igmp', 'stop_measure':'ta_stop_measurement_igmp','packet_count':'message_count' },

    'ICMP' : { 'start':'ta_traffic_start_icmp', 'bursty':'x', 'stop':'ta_traffic_stop_icmp', 'send':'ta_send_icmp_message', 'start_capture':'ta_start_capt_icmp_message', 'rtrv_capture':'ta_rtrv_capt_icmp_message', 'stop_capture':'ta_stop_capt_icmp_message', 'start_measure':'ta_start_measurement_icmp', 'stop_measure':'ta_stop_measurement_icmp','packet_count':'message_count' },

    'PPPOE' : { 'start':'ta_traffic_start_pppoe_discovery', 'bursty':'x', 'stop':'ta_traffic_stop_pppoe_discovery', 'send':'ta_send_pppoe_discovery_message', 'start_capture':'ta_start_capt_pppoe_discovery_message', 'rtrv_capture':'ta_rtrv_capt_pppoe_discovery_message', 'stop_capture':'ta_stop_capt_pppoe_discovery_message', 'start_measure':'ta_start_measurement_pppoe_discovery', 'stop_measure':'ta_stop_measurement_pppoe_discovery','packet_count':'message_count' },

    'PPP' : { 'start':'ta_traffic_start_ppp', 'bursty':'x', 'stop':'ta_traffic_stop_ppp', 'send':'ta_send_ppp_message', 'start_capture':'ta_start_capt_ppp_message', 'rtrv_capture':'ta_rtrv_capt_ppp_message', 'stop_capture':'ta_stop_capt_ppp_message', 'start_measure':'ta_start_measurement_ppp', 'stop_measure':'ta_stop_measurement_ppp','packet_count':'message_count'  },

    'PPP_LCP' : { 'start':'ta_traffic_start_ppp_lcp', 'bursty':'x', 'stop':'ta_traffic_stop_ppp_lcp_ipcp', 'send':'ta_send_ppp_lcp_packet', 'start_capture':'ta_start_capt_ppp_lcp_ipcp_packet', 'rtrv_capture':'ta_rtrv_capt_ppp_lcp_ipcp_packet', 'stop_capture':'ta_stop_capt_ppp_lcp_ipcp_packet', 'start_measure':'ta_start_measurement_ppp_lcp_ipcp', 'stop_measure':'ta_stop_measurement_ppp_lcp_ipcp' },

    'PPP_IPCP' : { 'start':'ta_traffic_start_ppp_ipcp', 'bursty':'x', 'stop':'ta_traffic_stop_ppp_lcp_ipcp', 'send':'ta_send_ppp_ipcp_packet', 'start_capture':'ta_start_capt_ppp_lcp_ipcp_packet', 'rtrv_capture':'ta_rtrv_capt_ppp_lcp_ipcp_packet', 'stop_capture':'ta_stop_capt_ppp_lcp_ipcp_packet', 'start_measure':'ta_start_measurement_ppp_lcp_ipcp', 'stop_measure':'ta_stop_measurement_ppp_lcp_ipcp' },

    'RADIUS' : { 'start':'x', 'bursty':'x', 'stop':'x', 'send':'ta_send_radius_message', 'start_capture':'ta_start_capt_radius_message', 'rtrv_capture':'ta_rtrv_capt_radius_message', 'stop_capture':'ta_stop_capt_radius_message', 'start_measure':'ta_start_measurement_radius', 'stop_measure':'ta_stop_measurement_radius' },

    '8021X' : { 'start':'x', 'bursty':'x', 'stop':'x', 'send':'ta_send_8021x_frame', 'start_capture':'ta_start_capt_8021x_frame', 'rtrv_capture':'ta_rtrv_capt_8021x_frame', 'stop_capture':'ta_stop_capt_8021x_frame', 'start_measure':'ta_start_measurement_8021x', 'stop_measure':'ta_stop_measurement_8021x' },

    'VBAS' : { 'start':'ta_traffic_start_vbas', 'bursty':'x', 'stop':'ta_traffic_stop_vbas', 'send':'ta_send_vbas_message', 'start_capture':'ta_start_capt_vbas_message', 'rtrv_capture':'ta_rtrv_capt_vbas_message', 'stop_capture':'ta_stop_capt_vbas_message', 'start_measure':'ta_start_measurement_vbas', 'stop_measure':'ta_stop_measurement_vbas' },

    'L2CP_ADJACENCY' : { 'start':'ta_traffic_start_l2cp_adjacency', 'bursty':'x', 'stop':'ta_traffic_stop_l2cp_adjacency', 'send':'ta_send_l2cp_adjacency_message', 'start_capture':'ta_start_capt_l2cp_adjacency_message', 'rtrv_capture':'ta_rtrv_capt_l2cp_adjacency_message', 'stop_capture':'ta_stop_capt_l2cp_adjacency_message', 'start_measure':'ta_start_measurement_l2cp_adjacency', 'stop_measure':'ta_stop_measurement_l2cp_adjacency' },

    'L2CP_EVENT' : { 'start':'ta_traffic_start_l2cp_event', 'bursty':'x', 'stop':'ta_traffic_stop_l2cp_event', 'send':'ta_send_l2cp_event_message', 'start_capture':'ta_start_capt_l2cp_event_message', 'rtrv_capture':'ta_rtrv_capt_l2cp_event_message', 'stop_capture':'ta_stop_capt_l2cp_event_message', 'start_measure':'ta_start_measurement_l2cp_event', 'stop_measure':'ta_stop_measurement_l2cp_event' },

    'L2CP_ADJACENCY_TCP' : { 'start':'ta_traffic_start_l2cp_adjacency_tcp', 'bursty':'x', 'stop':'ta_traffic_stop_l2cp_adjacency_tcp', 'send':'ta_send_l2cp_adjacency_message_tcp', 'start_capture':'x', 'rtrv_capture':'x', 'stop_capture':'x', 'start_measure':'x', 'stop_measure':'x' },

    'L2CP_PORT_MGMT' : { 'start':'ta_traffic_start_l2cp_port_management_tcp', 'bursty':'x', 'stop':'ta_traffic_stop_l2cp_port_management_tcp', 'send':'ta_send_l2cp_port_management_message_tcp', 'start_capture':'ta_start_capt_l2cp_port_management_message', 'rtrv_capture':'ta_rtrv_capt_l2cp_port_management_message', 'stop_capture':'ta_stop_capt_l2cp_port_management_message', 'start_measure':'ta_start_measurement_l2cp_port_management', 'stop_measure':'ta_stop_measurement_l2cp_port_management' },

    'RIP' : { 'start':'ta_traffic_start_rip', 'bursty':'x', 'stop':'ta_traffic_stop_rip', 'send':'ta_send_rip_message', 'start_capture':'ta_start_capt_rip_message', 'rtrv_capture':'ta_rtrv_capt_rip_message', 'stop_capture':'ta_stop_capt_rip_message', 'start_measure':'ta_start_measurement_rip', 'stop_measure':'ta_stop_measurement_rip' ,'packet_count':'message_count'},

    'CFM' : { 'start':'x', 'bursty' : 'x','stop' : 'x','send' : 'ta_send_8021ag_frame', 'start_capture' : 'ta_start_capt_8021ag_frame','rtrv_capture' : 'x','stop_capture' : 'ta_stop_capt_8021ag_frame', 'start_measure' : 'x','stop_measure' : 'x'},

    'Y1731' : { 'start':'x', 'bursty' : 'x', 'stop' : 'x', 'send' : 'ta_send_y1731_cfm_frame', 'start_capture' : 'ta_start_capt_y1731_cfm_frame', 'rtrv_capture': 'x', 'stop_capture' : 'ta_stop_capt_y1731_cfm_frame', 'start_measure' : 'x', 'stop_measure' : 'x' },

    'UDP' : { 'start':'ta_traffic_start_udp', 'stop':'ta_traffic_stop_udp', 'send':'ta_send_udp_packet', 'start_capture':'ta_start_capt_udp_packet', 'stop_capture':'ta_stop_capt_udp_packet','packet_count':'message_count'},
    
    'UDPV6' : { 'start':'ta_traffic_start_udpv6', 'stop':'ta_traffic_stop_udpv6', 'send':'ta_send_udpv6_packet', 'start_capture':'ta_start_capt_udpv6_packet', 'stop_capture':'ta_stop_capt_udpv6_packet' },
    
    'TCP' : { 'start':'ta_traffic_start_tcp', 'stop':'ta_traffic_stop_tcp', 'send':'ta_send_tcp_packet', 'start_capture':'ta_start_capt_tcp_packet',
'stop_capture':'ta_stop_capt_tcp_packet' },

    'IPV6' : { 'start':'ta_traffic_start_ipv6', 'stop':'ta_traffic_stop_ipv6', 'send':'ta_send_ipv6_packet', 'start_capture':'ta_start_capt_ipv6_packet', 'stop_capture':'ta_stop_capt_ipv6_packet' },

    'DHCPV6' : { 'start':'ta_traffic_start_bootp_dhcpv6', 'stop':'ta_traffic_stop_bootp_dhcpv6_message', 'send':'ta_send_bootp_dhcpv6_message', 'start_capture':'ta_start_capt_bootp_dhcpv6_message', 'stop_capture':'ta_stop_capt_bootp_dhcpv6_message','packet_count':'message_count' },

    'ICMPV6' : { 'start':'ta_traffic_start_icmpv6', 'stop':'ta_traffic_stop_icmpv6', 'send':'ta_send_icmpv6_message', 'start_capture':'ta_start_capt_icmpv6_message', 'stop_capture':'ta_stop_capt_icmpv6_message' },

    'MPLS_ATM' : { 'send':'ta_send_mpls_atm_packet', 'start':'ta_traffic_start_mpls_atm', 'stop':'ta_traffic_stop_mpls_atm', 'start_capture':'ta_start_capt_mpls_atm_packet', 'stop_capture':'ta_stop_capt_mpls_atm_packet' },

    'MPLS_IP' : { 'send':'ta_send_mpls_ip_packet', 'start':'ta_traffic_start_mpls_ip', 'stop':'ta_traffic_stop_mpls_ip', 'start_capture':'ta_start_capt_mpls_ip_packet', 'stop_capture':'ta_stop_capt_mpls_ip_packet' },

    'MPLS_ETH' : { 'send':'ta_send_mpls_eth_packet', 'start':'ta_traffic_start_mpls_eth', 'stop':'ta_traffic_stop_mpls_eth', 'start_capture':'ta_start_capt_mpls_eth_packet', 'stop_capture':'ta_stop_capt_mpls_eth_packet' },

    'MPLS_IP_CW' : { 'send':'ta_send_mpls_ip_cw_packet', 'start':'ta_traffic_start_mpls_ip_cw', 'stop':'ta_traffic_stop_mpls_ip_cw', 'start_capture':'ta_start_capt_mpls_ip_cw_packet', 'stop_capture':'ta_stop_capt_mpls_ip_cw_packet' },

    'MPLS_ETH_CW' : { 'send':'ta_send_mpls_eth_cw_packet', 'start':'ta_traffic_start_mpls_eth_cw', 'stop':'ta_traffic_stop_mpls_eth_cw', 'start_capture':'ta_start_capt_mpls_eth_cw_packet', 'stop_capture':'ta_stop_capt_mpls_eth_cw_packet' },

    'GET_IP' : { 'get':'ta_get_ip_addr' }

}

def pcta_get_command (prot,cmd_type) :
    cmd = ''
    try:
        cmd = _get_pcta_command (prot,cmd_type)
    except Exception as inst:
	logger.warn("Failed to get %s type command for protocol %s" % (cmd_type,prot))

    return cmd

def _get_pcta_command (prot,cmd_type) :
    """
    this keyword look up dictionary 'Protocols' 
    to find out the defined pcta traffic command
    
    """
    global Protocols
    try :
        cmd = Protocols[prot][cmd_type]
    except Exception as inst :
        raise AssertionError("fail to get pcta command. protocols: %s, command type: %s" % (prot,cmd_type))
    logger.debug ("The required pcta command: %s" % cmd)
    return cmd
    
def _pcta_command_options (*args):
    """
      translate user input options to the format of pcta command required
    """
    out_options = " "
    for arg in args:
        arg = arg.encode("ascii")
        index = arg.index("=") # first "=" is the seperation sign
        key = arg[0:index].strip().strip('\'').strip('"')
        value = arg[1+index:].strip('\'')
        if re.search(" ",value) and not re.search("{",value) :
            value = "\""+value+"\""
	if value != 'x' :
            out_options = out_options + "-" + key + " " + value + " "
        
    return out_options
    
def get_pcta_info (sessionObject=None) :
    """
    get pcta info
    
    return an array
    """
    
    global PCTA_SESSION
    global PCTA_NETWORK_NIC
    global PCTA_CLIENT_NIC

    if not sessionObject :
        sessionObject = PCTA_SESSION

    pcta_arr = {}
    pcta_arr['ip'] = sessionObject.ip
    pcta_arr['port'] = sessionObject.port 
    pcta_arr['username'] = sessionObject.username
    pcta_arr['password'] = sessionObject.password      
    pcta_arr['exec_file'] = sessionObject.exec_file
    pcta_arr['client_slot'] = sessionObject.client_slot 
    pcta_arr['network_slot'] = sessionObject.network_slot
    pcta_arr['client2_slot'] = sessionObject.client2_slot
    pcta_arr['network_slot2'] = sessionObject.network_slot2
    pcta_arr['network_slotlag'] = sessionObject.network_slotlag
    pcta_arr['prompt'] = sessionObject.prompt
    pcta_arr['available'] = sessionObject.available
    pcta_arr['network_nic'] = PCTA_NETWORK_NIC
    pcta_arr['client_nic']  = PCTA_CLIENT_NIC

    return pcta_arr
    
def send_pcta_command (command,prompt=None):
    """
    send original pcta command via pcta connection session
    
    """
    # open ta_traffic_cmd log file
    logger_cmd_ta = logging.getLogger("ta_traffic_cmd")

    # if PCTA is not aviable, exit from this function
    global PCTA_STATUS
    if PCTA_STATUS.lower() == 'false' :
        logger.info("PCTA is not available, exit from this function!")
        return 'PASS'

    global PCTA_SESSION
    logger_cmd_ta.debug("PCTA CMD >> %s" % command)

    logger.info("<span style=\"color: #006400\">"+"PCTA CMD >> %s" % command+"</span>",html=True)
    m = re.search(" (-vlan\d{1})_id +65535", command)
    if m :
        vlan_str = m.group(1)
        command = re.sub(" "+vlan_str+"_id +65535","",command)
        command = re.sub(" "+vlan_str+" +true","",command)
        
    m = re.search(" (-(s|c)_vlan)_id +65535", command)
    if m :  
        vlan_str = m.group(1)      
        command = re.sub(" "+vlan_str+"_id +65535","",command)
        command = re.sub(" "+vlan_str+" +true","",command)
    try :
        res = PCTA_SESSION.send_command(command,prompt)
    except Exception as inst :
        raise AssertionError("fail to send pcta command -> exception: %s" % inst)           
    else :
        logger.info("PCTA REPLY << %s" % res)
        logger_cmd_ta.debug("PCTA REPLY << %s\r\n" % res)

    if re.search('unknown\s*option|extra option|error|\
    invalid token|is not complete', res.lower()):
        return ("fail", res)
    else:
        return ("pass", res)

def connect_pcta (**pcta_info) :
    """ 
    Build up the session to pcta
    - *ip:* ip address of the pcta
    - *port:* port number of pcta 
    - *username:* username of login pcta
    - *password:* password of login user
    - *exec_file:* pcta executable file    
    - *prompt:* expected prompt for executing pcta process  
    - *RETURN VALUE: None or raise error
    
    Usage Example:
    | connect pcta | &session_info_dict |
    | connect pcta | ip= 2.3.5.55 | port=22 | username=auto | password=auto123 |
    |   | exec_file=/home/auto/pcta/pcta.exe | client_slot=1 | network_slot=2 |  
    """
    global PCTA_NETWORK_NIC
    global PCTA_CLIENT_NIC

    keyword = "connect_pcta"
    ip        = pcta_info.setdefault('ip',None)
    port      = pcta_info.setdefault('port',None)
    username  = pcta_info.setdefault('username',None)
    password  = pcta_info.setdefault('password',None)
    exec_file = pcta_info.setdefault('exec_file',None)
    prompt    = pcta_info.setdefault('prompt',None)
    client    = pcta_info['client_slot']
    network   = pcta_info['network_slot']
    client2   = pcta_info.setdefault('client2_slot','3')
    available = pcta_info.setdefault('available','True')
    network_slot2 = pcta_info.setdefault('network_slot2','3')
    network_slotlag = pcta_info.setdefault('network_slotlag','4')
    pcta_status = pcta_info.setdefault('available','True')
    PCTA_NETWORK_NIC = pcta_info.setdefault('network_nic','eth1') 
    PCTA_CLIENT_NIC  = pcta_info.setdefault('client_nic','eth2')

    global PCTA_SESSION
    global PCTA_STATUS

    PCTA_STATUS = pcta_status
    # if PCTA is not aviable, exit from this function
    if PCTA_STATUS.lower() == 'false' :
        logger.info("PCTA is not available, exit from this function!")
        return 'PASS'

    try:
        PCTA_SESSION = com_pcta.com_pcta \
        (ip,port,username,password,exec_file,client,network,client2,prompt,available,\
        network_slot2,network_slotlag)

    except Exception as inst:
        raise AssertionError("%s:%s-> fail to init com_pcta, exception: %s" \
        % (__name__,keyword,str(inst))) 
        
    try:       
        PCTA_SESSION.open_pcta()
    except Exception as inst:
        raise AssertionError("%s:%s-> fail to open_pcta, exception: %s" \
        % (__name__,keyword,str(inst)))           
    else :
        logger.info("connect pcta success ") 
        
def disconnect_pcta () :
    """
       disconnect pcta by close pcta session
    """
    # if PCTA is not aviable, exit from this function 
    global PCTA_STATUS
    if PCTA_STATUS.lower() == 'false' :
        logger.info("PCTA is not available, exit from this function!")
        return 'PASS'

    global PCTA_SESSION
    keyword_name = "disconnect_pcta"

    try:
        PCTA_SESSION.close_pcta()
        del PCTA_SESSION
    except Exception as inst:
        raise AssertionError("%s:%s-> fail to close pcta session, exception: %s" \
        % (__name__,keyword_name, str(inst)))      
    else:
        logger.info("disconnect pcta connection success " )
  
def pcta_start_capture (**args) :
    """
    starting the capture process of pcta
    
    - *side:* 'user' or 'network'
    - *ta_protocol: IP or DHCP or ICMP, etc * 
    - *RETURN:* 1~8,dist_id for pcta_receive_traffic

    usage example :
    | pcta_start_capture | side=user | ta_protocol=IP | 
    | pcta_start_capture | side=user | ta_protocol=ETH | type=8809 |

    """
    # if PCTA is not aviable, exit from this function 
    global PCTA_STATUS
    if PCTA_STATUS.lower() == 'false' :
        logger.info("PCTA is not available, exit from this function!")
        return 'PASS'

    keyword_name = "pcta_start_capture"
    aArgs = args
    logger.debug("%s-> %s" % (keyword_name,aArgs))
 
    # Get the capture & send type commands for the mentioned protocol
    if aArgs.has_key('ta_protocol') and aArgs['ta_protocol']!= "x" :
        captcommand = _get_pcta_command(aArgs['ta_protocol'].upper(),'start_capture')
    else :
        raise PctaContinueError("ta_protocol value not exists\n '%s'" % str(args))
    captSideOpt = ""  
    # Set the lif side for the user/network in both capt side
    if aArgs.has_key('lif') and aArgs['lif'] != "x" :
        logger.debug("%s:%s -> lif exists in input" % (__name__,keyword_name))
        captSideOpt = "lif=" + aArgs['lif']
    elif aArgs.has_key ('side') :
        pcta_info = get_pcta_info()  
        if 'user' in aArgs['side'] :
            captSideOpt = "lif="+pcta_info['client_slot']
        elif 'network2' in aArgs['side'] :
            captSideOpt = "lif="+pcta_info['network_slot2'] 
        elif 'networklag' in aArgs['side'] :
            captSideOpt = "lif="+pcta_info['network_slotlag'] 
        elif 'network' in aArgs['side'] :
            captSideOpt = "lif="+pcta_info['network_slot'] 
            
        else :
            raise PctaContinueError("unexpected 'side' option '%s' " % str(aArgs['side']))           
    else :
          raise PctaContinueError("'side' option not exists \n'%s' " % str(args))

    options = []
    options.append(captSideOpt)
    for key,value in aArgs.items() :
        if key not in ['lif','side','ta_protocol'] :
            options.append (key+"="+value)

    # Send Start capture command
    try :
        command_options = _pcta_command_options(*options)
        full_command = captcommand +" " + command_options
        (status,return_info) = send_pcta_command(full_command)
    except Exception as inst :
         raise AssertionError("failed to create/send command \n exception: %s" % inst)
                
    if "pass" == status.lower() :
        chkdist = re.search('-dist_id\s*(\d*)', return_info)
        dist_id = chkdist.group(1)  
        if  1 < int(dist_id) :
            msg = "abnormal dist_id '%s', it should be '1'" % str(dist_id)
            logger.info("<span style=\"color: #FF8C00\">"+msg+"</span>",html=True) 
        logger.debug ("capture started with dist_id '%s'" % str(dist_id))
        return dist_id
    else :
         raise AssertionError("failed command: %s" % full_command)

def pcta_start_measure (**args) :
    """
    starting the measure process of pcta
    
    - *side:* 'user' or 'network'
    - *ta_protocol: IP or DHCP or ICMP, etc * 
    - *RETURN:* 1~8,dist_id for pcta_receive_traffic

    usage example :
    | pcta_start_measure | side=user | ta_protocol=IP | 
    | pcta_start_measure | side=user | ta_protocol=ETH | type=8809 |

    """
    # if PCTA is not aviable, exit from this function 
    global PCTA_STATUS
    if PCTA_STATUS.lower() == 'false' :
        logger.info("PCTA is not available, exit from this function!")
        return 'PASS'

    keyword_name = "pcta_start_measure"
    aArgs = args
    logger.debug("%s-> %s" % (keyword_name,aArgs))
 
    # Get the capture & send type commands for the mentioned protocol
    if aArgs.has_key('ta_protocol') and aArgs['ta_protocol']!= "x" :
        try : 
            captcommand = _get_pcta_command(aArgs['ta_protocol'].upper(),'start_measure')
        except Exception as inst :
            
            raise PctaContinueError("Can't measure ta_protocol %s" % str(args))
    else :
        raise PctaContinueError("ta_protocol value not exists\n '%s'" % str(args))
    captSideOpt = ""  
    # Set the lif side for the user/network in both capt side
    if aArgs.has_key('lif') and aArgs['lif'] != "x" :
        logger.debug("%s:%s -> lif exists in input" % (__name__,keyword_name))
        captSideOpt = "lif=" + aArgs['lif']
    elif aArgs.has_key ('side') :
        pcta_info = get_pcta_info()  
        if 'user' in aArgs['side'] :
            captSideOpt = "lif="+pcta_info['client_slot']
        elif 'network2' in aArgs['side'] :
            captSideOpt = "lif="+pcta_info['network_slot2'] 
        elif 'networklag' in aArgs['side'] :
            captSideOpt = "lif="+pcta_info['network_slotlag'] 
        elif 'network' in aArgs['side'] :
            captSideOpt = "lif="+pcta_info['network_slot']             
        else :
            raise PctaContinueError("unexpected 'side' option '%s' " % str(aArgs['side']))         
    else :
          raise PctaContinueError("'side' option not exists \n'%s' " % str(args))

    options = []
    options.append(captSideOpt)
    for key,value in aArgs.items() :
        if key not in ['lif','side','ta_protocol'] :
            options.append (key+"="+value)

    # Send Start measure command
    try :
        command_options = _pcta_command_options(*options)
        full_command = captcommand +" " + command_options
        (status,return_info) = send_pcta_command(full_command)
    except Exception as inst :
         raise AssertionError("failed to create/send command \n exception: %s" % inst)
                
    if "pass" == status.lower() :
        chkdist = re.search('-dist_id\s*(\d*)', return_info)
        dist_id = chkdist.group(1)  
        if  1 < int(dist_id) :
            msg = "abnormal dist_id '%s', it should be '1'" % str(dist_id)
            logger.info("<span style=\"color: #FF8C00\">"+msg+"</span>",html=True) 
        logger.debug ("measure started with dist_id '%s'" % str(dist_id))
        return dist_id
    else :
         raise AssertionError("failed command: %s" % full_command)


def pcta_stop_cmd (**args) :
    """
    stop the measure,traffic or capture process of pcta
    
    - *side:* 'user' or 'network'
    - *ta_protocol: IP or DHCP or ICMP, etc * 
    - *RETURN:* 1~8,dist_id for pcta_stop_cmd

    usage example :
    | pcta_stop_cmd | side=user | ta_protocol=IP | stop_type = traffic | dist_id = 1 | 
    | pcta_stop_cmd | side=user | ta_protocol=ETH | stop_type = capture | 
    | pcta_stop_cmd | side=user | ta_protocol=ETH | stop_type = measure | 

    """
    # if PCTA is not aviable, exit from this function 
    global PCTA_STATUS
    if PCTA_STATUS.lower() == 'false' :
        logger.info("PCTA is not available, exit from this function!")
        return 'PASS'

    keyword_name = "pcta_stop_cmd"
    aArgs = args
    logger.debug("%s-> %s" % (keyword_name,aArgs))

    dStopCmd = {'traffic':'stop','capture':'stop_capture','measure':'stop_measure'}
    sStopType = ''
    if aArgs.has_key('stop_type') and aArgs['stop_type']!= "x" :
        sStopType = dStopCmd[aArgs.pop('stop_type').lower()]
    else:
        raise PctaContinueError("stop_type value is abnormal!\n '%s'" % str(args))

    # Get the capture & send type commands for the mentioned protocol
    if aArgs.has_key('ta_protocol') and aArgs['ta_protocol']!= "x" :
        captcommand = _get_pcta_command(aArgs['ta_protocol'].upper(),sStopType)
    else :
        raise PctaContinueError("ta_protocol value not exists\n '%s'" % str(args))

    if aArgs.has_key('packet_count') and aArgs['packet_count']!='x' and Protocols[aArgs['ta_protocol']].has_key('packet_count') :
       sPacketKey = Protocols[aArgs['ta_protocol']]['packet_count']
       aArgs[sPacketKey] = aArgs['packet_count']
       aArgs.pop('packet_count')

    captSideOpt = ""  
    # Set the lif side for the user/network in both capt side
    if aArgs.has_key('lif') and aArgs['lif'] != "x" :
        logger.debug("%s:%s -> lif exists in input" % (__name__,keyword_name))
        captSideOpt = "lif=" + aArgs['lif']
    elif aArgs.has_key ('side') :
        pcta_info = get_pcta_info()  
        if 'user' in aArgs['side'] :
            captSideOpt = "lif="+pcta_info['client_slot']
        elif 'network2' in aArgs['side'] :
            captSideOpt = "lif="+pcta_info['network_slot2'] 
        elif 'networklag' in aArgs['side'] :
            captSideOpt = "lif="+pcta_info['network_slotlag'] 
        elif 'network' in aArgs['side'] :
            captSideOpt = "lif="+pcta_info['network_slot']             
        else :
            raise PctaContinueError("unexpected 'side' option '%s' " % str(aArgs['side']))         
    else :
          raise PctaContinueError("'side' option not exists \n'%s' " % str(args))

    options = []
    options.append(captSideOpt)
    for key,value in aArgs.items() :
        if key not in ['lif','side','ta_protocol'] :
            options.append (key+"="+value)

    # Send stop command
    try :
        command_options = _pcta_command_options(*options)
        full_command = captcommand +" " + command_options
        (status,return_info) = send_pcta_command(full_command)
    except Exception as inst :
         raise AssertionError("failed to create/send command \n exception: %s" % inst)

    dResultVerify = {'stop':'_send_l','stop_capture':'_received_l','stop_measure':'_result_l'}                
    if dResultVerify[sStopType] not in return_info :
        raise PctaContinueError("pcta return abnormal value\n %s" % return_info)

    if aArgs.has_key('capt_reserve') and "true" == aArgs['capt_reserve'] :
        global user_side_traffic, network_side_traffic, network_side_traffic2,network_side_trafficlag

    if dResultVerify[sStopType] + ' {}' in return_info :
        msg = "no packets found!"
        packets = []        
    try :
        packets = _packet_convertor (return_info) 
        
    except Exception as inst :
        raise PctaContinueError("failed to tide up packets\n exception:" +str(inst))

    return packets


def pcta_send_traffic (*arg_list,**args) :
    """
    send packets on pcta

    - *ta_protocol:* IP,UDP, DHCP,etc
    - *side:* user or network
    - .* :* any param to define a protocol packet in pcta

    usage example :
    | pcta_send_traffic | side=user | ta_protocol=IP | vlan1_id=500 | mac_src=000000000011 | 

    """
    # if PCTA is not aviable, exit from this function 
    global PCTA_STATUS
    if PCTA_STATUS.lower() == 'false' :
        logger.info("PCTA is not available, exit from this function!")
        return 'PASS'

    keyword_name = "pcta_send_traffic"    
    if arg_list :   
        aArgs = _list_to_dict(arg_list)      
    else :
        aArgs = args
    logger.debug("%s-> %s" % (keyword_name,aArgs))
 
    # Get the send type command for the mentioned protocol
    if not aArgs.has_key('ta_protocol') or aArgs['ta_protocol']== "x" :
        raise PctaContinueError("fail to get 'ta_protocol' in input")

    # convert packet_count to the related frame_count or message_count according to protocol
    if aArgs.has_key('packet_count') and aArgs['packet_count']!='x' and Protocols[aArgs['ta_protocol']].has_key('packet_count'):
       sPacketKey = Protocols[aArgs['ta_protocol']]['packet_count']
       aArgs[sPacketKey] = aArgs['packet_count']
       aArgs.pop('packet_count')
    
    if aArgs.has_key('send_type') and aArgs['send_type']!='x' :
        send_type = aArgs['send_type']   
    else :
        send_type = 'send'
    try :    
        command_name = _get_pcta_command(aArgs['ta_protocol'],send_type)
    except Exception as inst :
        raise PctaContinueError("fail to get pcta command for %s\nexception: %s" \
        % (aArgs['ta_protocol'],inst))  
            
    # Set the lif side for the user/network in both capt & send sides
    if aArgs.has_key('lif') and aArgs['lif'] != "x" :
        sendSideOpt = "lif=" + aArgs['lif']  
    elif aArgs.has_key('side') :      
        pcta_info =  get_pcta_info()      
        if 'user' in aArgs['side'] :
            sendSideOpt = "lif="+pcta_info['client_slot']
        elif 'network2' in aArgs['side'] :
            sendSideOpt = "lif="+pcta_info['network_slot2']
        elif 'networklag' in aArgs['side'] :
            sendSideOpt = "lif="+pcta_info['network_slotlag']
        elif 'network' in aArgs['side'] :
            sendSideOpt = "lif="+pcta_info['network_slot']
    else :
        raise PctaContinueError("fail to get 'side' in input \n'%s'" % str(args)) 
  
    # PCTA command to send traffic
    options = []
    options.append(sendSideOpt)
    for key,value in aArgs.items() :

        if key in ['ta_protocol','side','send_type'] :
            continue
        else :
            m = re.search('((c|s)?_?vlan[1-4]?)_id',key)
            if m :
                if '65535' not in value :
                    options.append(key+"="+value)
                    options.append(m.group(1)+'=true')           
                else :
                    continue
            else :
                #value = value.strip ("{").strip("}")
                options.append(key+"="+value)
         
    try :
        command_options = _pcta_command_options(*options)
        full_command = command_name +" " + command_options 
    except Exception as inst :
        raise PctaContinueError("fail to create pcta command") 
    
    (status,return_info) = send_pcta_command(full_command)       
    if "pass" != status.lower() :
        raise PctaContinueError("fail to send pcta command '%s'" % full_command)
    else :
        dist_id = ''
        chkdist = re.search('-dist_id\s*(\d*)', return_info)
        if chkdist : 
            dist_id = chkdist.group(1)  
            #if  1 < int(dist_id) :
            #    msg = "abnormal dist_id '%s', it should be '1'" % str(dist_id)
            #    logger.info("<span style=\"color: #FF8C00\">"+msg+"</span>",html=True) 
            logger.debug ("send traffic with dist_id '%s'" % str(dist_id))
        return "pass", dist_id

def pcta_receive_traffic(**args) :
    """
    stop the capture process of pcta and validate the captured packets
    
    - *side:* 'user' or 'network'
    - *ta_protocol: IP or DHCP or ICMP, etc * 
    - *dist_id:*  1~8, the return value of pcta_start_capture

    usage example :
    | pcta_receive_traffic | side=user | mac_dest=000000000011 | ta_protocol=IP | dist_id=1 |  

    """
    # if PCTA is not aviable, exit from this function
    logger.info("pcta_receive_traffic->%s" % args) 
    global PCTA_STATUS
    if PCTA_STATUS.lower() == 'false' :
        logger.info("PCTA is not available, exit from this function!")
        return 'PASS'

    keyword_name = "pcta_receive_traffic"
    aArgs = args
    # Set the lif side for the user/network in both capt side
    if aArgs.has_key('lif') and aArgs['lif'] != "x" :
        captSideOpt = "lif=" + aArgs['lif']
    elif aArgs.has_key ('side') :
        try :
            pcta_info = get_pcta_info()   
        except Exception as inst :
            raise AssertionError("fail to get pcta info\n exception: %s" % inst) 

        if 'user' in aArgs['side'] :
            captSideOpt = "lif="+pcta_info['client_slot']
            aArgs['lif'] = pcta_info['client_slot']
        elif 'network2' in aArgs['side'] :
            captSideOpt = "lif="+pcta_info['network_slot2'] 
            aArgs['lif'] = pcta_info['network_slot2'] 
        elif 'networklag' in aArgs['side'] :
            captSideOpt = "lif="+pcta_info['network_slotlag'] 
            aArgs['lif'] = pcta_info['network_slotlag'] 
        elif 'network' in aArgs['side'] :
            captSideOpt = "lif="+pcta_info['network_slot'] 
            aArgs['lif'] = pcta_info['network_slot'] 
        else :
            raise PctaContinueError("unexpected 'side' option:'%s' " % str(aArgs['side']))        
    else :
        raise PctaContinueError("'side' option not exists in input \n '%s' " % str(args))  

    if aArgs.has_key('dist_id') and int(aArgs['dist_id']) <= 8 :  
        distOpt = "dist_id=" + str(aArgs['dist_id'])
    else :
        raise APctaContinueError("gotten unexpected dist_id from '%s'" % str(args))

    if aArgs.has_key('receive_type') and aArgs['receive_type']!='x' :
        receive_type = aArgs['receive_type']   
    else :
        receive_type = 'stop_capture'    
    # Get the capture & send type commands for the mentioned protocol
    try:
        captcommand = _get_pcta_command \
        (aArgs['ta_protocol'],receive_type)
    except Exception :
        raise PctaContinueError("%s:%s-> no capture stop command for ta_protocol'%s'" % aArgs['ta_protocol'])
  
    if aArgs.has_key('exp_result'):
        exp_result = aArgs['exp_result'].strip('"')
    elif aArgs.has_key('exp_res'):
        exp_result = aArgs['exp_res'].strip('"')
    else :
        exp_result = "pass"
    #Input Dict to compare received packet values
    inputDict = deepcopy(aArgs)
    del inputDict['lif']
    del inputDict['ta_protocol']
    if aArgs.has_key('side'):
        del inputDict['side']
    if aArgs.has_key('exp_result'):
        del inputDict['exp_result']
    if aArgs.has_key('dist_id'):
        del inputDict['dist_id']
    if aArgs.has_key('capt_reserve'):
        del inputDict['capt_reserve']
    logger.info("Conditions To Filter Received Packets:\n %s" % inputDict)
    # Send Stop capture command
    captOptList = []  
    captOptList.append(captSideOpt)
    captOptList.append(distOpt)

    command_options = _pcta_command_options(*captOptList)
    full_command = captcommand +" " + command_options 
  
    try:   
        (status,return_info) = send_pcta_command(full_command)
    except Exception as inst:
        raise PctaContinueError("fail to stop capture\n exception:%s" % inst)
 
    if '_received_l' not in return_info :
        raise PctaContinueError("pcta return abnormal value\n %s" % return_info)

    if aArgs.has_key('capt_reserve') and "true" == aArgs['capt_reserve'] :
        global user_side_traffic, network_side_traffic, network_side_traffic2,network_side_trafficlag

    if '_received_l {}' in return_info :
        msg = "no packets found!"
        packets = []
        if aArgs.has_key('capt_reserve') and "true" == aArgs['capt_reserve'] :
            if 'user' in aArgs['side'] :
                user_side_traffic = packets
            if 'network' in aArgs['side'] :
                network_side_traffic = packets 
        if exp_result.lower() == "fail" :
            logger.info ("<i>"+msg+'</i>',html=True)
        else :          
            raise PctaContinueError(msg)
    try :
        packets = _packet_convertor (return_info) 
        
    except Exception as inst :
        raise PctaContinueError("failed to tide up packets\n exception:" +str(inst))

    if aArgs.has_key('capt_reserve') and "true" == aArgs['capt_reserve'] :
        if 'user' in aArgs['side'] :
            user_side_traffic = packets
        elif 'network2' in aArgs['side'] :
            network_side_traffic2 = packets
        elif 'networklag' in aArgs['side'] :
            network_side_trafficlag = packets
        elif 'network' in aArgs['side'] :
            network_side_traffic = packets
    try :
        packet_verify = _validate_packets(inputDict,packets)
    except Exception as inst :
        raise PctaContinueError("failed to verify packets \n exception:" +str(inst))
    
    if packet_verify != exp_result.lower() :
        raise PctaContinueError("expect/verified is '%s'/'%s'" % (exp_result,packet_verify))
    logger.info ("gotten expected traffic")

def pcta_check_traffic(*args) :
    """
    single direction traffic check, the traffic can be expected as pass or fail
  
    | pcta_check_traffic | capt_options='ta_protocol=ip, side=user, mac_dest=000000000011,packet_count=5' |
    |   | send_options='ta_protocol=IP, side=network, vlan1_id=100, vlan1_user_prior=2,
          mac_dest=000000000022, mac_src=000000000011,ip_dest=2.2.2.2, ip_src=1.1.1.1,packet_count=5' |
    |   | delay_time='3' | exp_result=PASS |

    """
    # if PCTA is not aviable, exit from this function 
    global PCTA_STATUS
    if PCTA_STATUS.lower() == 'false' :
        logger.info("PCTA is not available, exit from this function!")
        return 'PASS'

    keyword_name = "pcta_check_traffic"
    logger.debug("%s-> %s" % (keyword_name, str(args))) 
    aArgs = _list_to_dict(args)

    if aArgs.has_key('exp_result') and aArgs['exp_result']!= "x" :
        pass
    else:
        aArgs['exp_result']="pass"   

    result = "fail"
    
    # 1 start capture packets
    if aArgs.has_key('capt_options') and aArgs['capt_options']!= "x" :
        dict_capt_options = {}
        for item in aArgs['capt_options'].lstrip('[').strip(']').split(',') :
            try :
                m = re.search("\s*(\S+)\s*=\s*(.*)", item)
                key,value = m.groups()
                key = re.sub("\'|\"|u\'","",key)
            except Exception as inst :
                pass
            finally :    
                dict_capt_options[key]=value.strip("'").strip('"')
        try :
            dist_id = pcta_start_capture(side=dict_capt_options['side'],ta_protocol=dict_capt_options['ta_protocol'])
            dict_capt_options['dist_id'] = str(dist_id)
        except Exception as inst :
            raise PctaContinueError("failed to start capture\n exception: %s" \
            % (inst))

    # add delay between start capture and send packets
    delay_before_send =  0;      
    if aArgs.has_key('send_delay') :
        delay_before_send = aArgs['send_delay'].strip('s')
    if delay_before_send > 0 :
        time.sleep(int(delay_before_send))
    logger.debug("sleep %s seconds before send packets" % delay_before_send )


    # 2 send packets
    if aArgs.has_key('send_options') and aArgs['send_options']!= "x" :
        dict_send_options = {}
        for item in aArgs['send_options'].lstrip('[').strip(']').split(',') :
            try :
                m = re.search("\s*(\S+)\s*=\s*(.*)", item)
                key,value = m.groups()
                key = re.sub("\'|\"|u\'","",key)
            except Exception as inst :
                pass
            finally :    
                dict_send_options[key] = value.strip("'").strip('"')
        
        try:
            result = pcta_send_traffic(**dict_send_options)
        except Exception as inst:
            msg = "failed to send traffic"
            logger.info("<span style=\"color: #FF8C00\">"+msg+"</span>",html=True)
            msg = "exception:"+ str(inst)
            logger.debug("<span style=\"color: #FF8C00\">"+msg+"</span>",html=True)            
  
    # 3 stop capture and check packets
    delay_before_stop_capt =  1      
    if aArgs.has_key('delay_time') :
        delay_before_stop_capt = aArgs['delay_time'].strip('s')
    logger.debug("sleep %s seconds before stop capture" % delay_before_stop_capt )
    time.sleep(int(delay_before_stop_capt))
  
    if aArgs.has_key('capt_options') and aArgs['capt_options']!= "x" : 
        # let stop capture process know the distance id of start capture and expect result
        
        dict_capt_options['exp_result'] = aArgs['exp_result']
        if aArgs.has_key('capt_reserve') and 'true' == aArgs['capt_reserve'] :
            dict_capt_options['capt_reserve']= "true"
        try: 
            pcta_receive_traffic(**dict_capt_options)
        except Exception as inst:
            raise PctaContinueError("traffic check failure\n exception:%s " % str(inst))   
    return "pass"

def pcta_capture_traffic(*args) :
 
    """ 
    return all captured packet saved in dist_id

    - *side:* 'user' or 'network'
    - *ta_protocol: IP or DHCP or ICMP, etc * 
    - *dist_id:*  1~8, the return value of pcta_start_capture

    usage example :
    | pcta_capture_traffic | side=user | ta_protocol=IP | dist_id=1 | mac_dest=000000000011 |

    """
    # if PCTA is not aviable, exit from this function 
    global PCTA_STATUS
    if PCTA_STATUS.lower() == 'false' :
        logger.info("PCTA is not available, exit from this function!")
        return 'PASS'

    packets =[]
    keyword_name = "pcta_capture_traffic"
    aArgs = _list_to_dict(args)
    logger.debug("%s-> %s" % (keyword_name,aArgs))
    
    # Set the lif side for the user/network in both capt side
    if aArgs.has_key('lif') and aArgs['lif'] != "x" :
        logger.debug("%s:%s -> lif exists in input" % (__name__,keyword_name))
        captSideOpt = "lif=" + aArgs['lif']
    elif aArgs.has_key ('side') and aArgs['side'] in ["user","network"] :
        pcta_info = get_pcta_info()      
        if 'user' in aArgs['side'] :
            captSideOpt = "lif="+pcta_info['client_slot']
            aArgs['lif'] = pcta_info['client_slot']
        elif 'network2' in aArgs['side'] :
            captSideOpt = "lif="+pcta_info['network_slot2'] 
            aArgs['lif'] = pcta_info['network_slot2'] 
        elif 'networklag' in aArgs['side'] :
            captSideOpt = "lif="+pcta_info['network_slotlag'] 
            aArgs['lif'] = pcta_info['network_slotlag'] 
        elif 'network' in aArgs['side'] :
            captSideOpt = "lif="+pcta_info['network_slot'] 
            aArgs['lif'] = pcta_info['network_slot'] 
        else :
            raise PctaContinueError("unexpected 'side' option:'%s' " \
            % (str(aArgs['side'])))            
    else :
          raise PctaContinueError("'side' option not exists \nin '%s' " % str(args))

    if aArgs.has_key('dist_id') and int(aArgs['dist_id']) <= 8 :  
        distOpt = "dist_id=" + str(aArgs['dist_id'])
    else :
        raise PctaContinueError("gotten abnormal dist_id \n '%s'" \
        % (str(args)))    
    # Get the capture & send type commands for the mentioned protocol
    try:
        captcommand = _get_pcta_command(aArgs['ta_protocol'],'stop_capture')
    except Exception :
        raise PctaContinueError("not found capture stop command for %s" \
        % aArgs['ta_protocol'])
  
    if aArgs.has_key('exp_result'):
        exp_res = aArgs['exp_result']
    else :
        exp_res = "pass"

    #Input Dict to compare received packet values
    inputDict = aArgs
    if aArgs.has_key('side'):
        del inputDict['side']
    del inputDict['lif']
    del inputDict['ta_protocol']
    if aArgs.has_key('exp_res'):
        del inputDict['exp_res']
    if aArgs.has_key('dist_id'):
        del inputDict['dist_id']
    logger.debug ("input to compare : %s" % inputDict)

    # Send Stop capture command
    captOptList = []  
    captOptList.append(captSideOpt)
    captOptList.append(distOpt)

    time.sleep(1.5)
    command_options = _pcta_command_options(*captOptList)
    full_command = captcommand +" " + command_options
    try:   
        (status,return_info) = send_pcta_command(full_command)
    except Exception as inst:
        raise PctaContinueError("fail to stop capture\n exception:%s" \
        % (inst))
 
    if '_received_l' not in return_info :
        raise PctaContinueError("pcta return abnormal value\n %s" % return_info)
        
    # Validation part
    if '_received_l {}' in return_info :
        msg = "no packets found"
        if exp_res.lower() == "fail" :
            logger.info (msg + " and expect 'fail'")
            return []
        else :
            raise PctaContinueError(msg)
    else :
        packets = _packet_convertor (return_info)
    if len(inputDict) < 1 :
       logger.debug ("There is no filter,return all packets: \n %s" % str(packets))
       return packets
    try :
        matched_packets = _capture_validate_packets(inputDict,packets)
        logger.debug ("matched packets : \n %s" % (str(matched_packets)))
        return matched_packets
    except Exception as inst :
        raise PctaContinueError("not gotten expect packets \n exception:" +str(inst))       
  

def get_traffic_capture(**args):
    
    """
    return specified field's value from captured packets on user side or network side

    - *side:* 'user' or 'network'
    - *get_field: field expected to retrieve 
    - * :*  any fields to filter a packet  

    usage example: 
    | &{packet1}= | get_traffic_capture | side=user | ip_src=192.168.1.254 |
    |   | get_field=mac_src |# will get target field from user side packet | 
    | &{packet2}= | get_traffic_capture | side=network | ip_src=10.18.81.186 |
    |   | get_field=mac_dest,vlan1_id | 
    | &{whole_packet}= | get_traffic_capture | side=network | ip_src=10.18.81.186 |
    |   | # used to check if relative packet was captured |
    """
    # if PCTA is not aviable, exit from this function 
    global PCTA_STATUS
    if PCTA_STATUS.lower() == 'false' :
        logger.info("PCTA is not available, exit from this function!")
        return 'PASS'

    global user_side_traffic, network_side_traffic
    keyword_name = "get_traffic_capture"
    logger.debug("%s:%s ->  %s" % (__name__,keyword_name, str(args)))
 
    in_option = deepcopy(args)
    result = {}
    packets = []

    if args.has_key('exp_result'):
        exp_res = args['exp_result'].lower()
    else :
        exp_res = "pass"

    if not args.has_key('side') :
        raise PctaContinueError("no 'side=user' or 'side=network' in input")
    
    if 'user' == args['side'] : 
        packets = user_side_traffic
    elif 'network' == args['side'] : 
        packets = network_side_traffic 
    del args['side']  
    
    if args.has_key('get_field') :
        del args['get_field'] 

    if args.has_key('exp_result') :
        del args['exp_result'] 
             
    if len(args) < 1 :
        logger.debug ("There is no filter,capture packets : \n %s" % (str(packets[0])))
        return packets[0]

    try:
        fields = in_option['get_field'].split(",")
    except Exception as inst:
        fields = []   
    
    try:
        del in_option['side']
        del in_option['get_field']
    except:
        pass
    if not in_option: 
        return result

    packets_str = ""
    for packet in packets :
        packets_str = packets_str + "\n" + str(packet)
    logger.debug ("captured packets: %s" % packets_str)
    
    logger.debug("filter conditions: %s" % in_option.items())
    for packet in packets :
        #each captured packet
        bMatch = True       
        for option in in_option.keys() :
            #each input option
            if option != "exp_result" :
                temp = packet.has_key(option) and packet[option] or "nonExist"
                if temp != in_option[option] :
                    bMatch = False
        if bMatch :
            if not fields:
                result = deepcopy(packet)
            for field in fields :
                #each field which should be gotten
                result[field] = packet.has_key(field) and packet[field] or ""
            logger.debug ("return : \n %s" % (str(result)))
            return result
        if bMatch != "True" :  
            if exp_res != "pass" :
                result = deepcopy(packet)
                return result
    
    raise PctaContinueError("Fail to capture expected traffic: %s" % str(in_option))


def pcta_check_traffic_bidirect(*args) :
    """
    bi direction traffic check, the traffic can be expected as pass or fail

    Example 
    | pcta_check_traffic_bidirect | us_send_options='side=user,ta_protocol=CFM,...' | us_capt_options='ta_protocol=ip,side=network,...' |
    | ds_send_options='side=network,ta_protocol=IP,...' | ds_capt_options='side=user,...' | exp_result="us_pass_ds_pass"  |
    
    """

    # if PCTA is not aviable, exit from this function 
    global PCTA_STATUS
    if PCTA_STATUS.lower() == 'false' :
        logger.info("PCTA is not available, exit from this function!")
        return 'PASS'

    keyword_name = "pcta_check_traffic_bidirect"
    logger.debug("%s:%s ->  %s" \
    % (__name__,keyword_name, str(args)))
 
    aArgs = _list_to_dict(args)

    if aArgs.has_key('exp_result') and aArgs['exp_result']!= "x" :
        pass
    else:
        aArgs['exp_result']="us_pass"

    exp_result_us="pass"
    exp_result_ds="pass"
    exp_result_old="pass"
    if "us" in aArgs['exp_result'] or "ds" in aArgs['exp_result'] :
        exp_tmp_l=aArgs['exp_result'].split("_")
        for i,exp in enumerate(exp_tmp_l) :
            tmp_len=len(exp_tmp_l)
            if exp == "us" and i != tmp_len :
                exp_result_us = exp_tmp_l[i+1]
            elif exp == "ds" and i != tmp_len :
                exp_result_ds = exp_tmp_l[i+1]
            else :
                continue
    else :
        exp_result_old = aArgs['exp_result']
    
    result = "fail"
    
    # 1 start capture packets
    dict_capt_old = {}
    dict_capt_us = {}
    dict_capt_ds = []
    if aArgs.has_key('capt_options') and aArgs['capt_options']!= "x" :
        dict_capt_old=_start_capt_option(aArgs['capt_options'])
        
    if aArgs.has_key('us_capt_options') and aArgs['us_capt_options']!= "x" :
        dict_capt_us=_start_capt_option(aArgs['us_capt_options'])

    if aArgs.has_key('ds_capt_options') and aArgs['ds_capt_options']!= "x" :
        dict_capt_ds=_start_capt_option(aArgs['ds_capt_options'])

    if aArgs.has_key('capt_reserve') and 'true' == aArgs['capt_reserve'] :
        if aArgs.has_key('capt_options') and aArgs['capt_options']!= "x" :
            dict_capt_old['capt_reserve']='true'

        if aArgs.has_key('us_capt_options') and aArgs['us_capt_options']!= "x" :
            dict_capt_old['capt_reserve']='true'

        if aArgs.has_key('ds_capt_options') and aArgs['ds_capt_options']!= "x" :
            dict_capt_old['capt_reserve']='true'
    
    # 2 send packets
    if aArgs.has_key('send_options') and aArgs['send_options']!= "x" :
        _send_traffic(aArgs['send_options'])
        
    if aArgs.has_key('us_send_options') and aArgs['us_send_options']!= "x" :
        _send_traffic(aArgs['us_send_options'])    
    
    if aArgs.has_key('ds_send_options') and aArgs['ds_send_options']!= "x" :
        _send_traffic(aArgs['ds_send_options'])

  
    # 3 stop capture and check packets
    if aArgs.has_key('delay_time') :
        time.sleep(int(aArgs['delay_time'].strip('s')))

    result_old=""
    result_us=""
    result_ds=""
    if aArgs.has_key('capt_options') and aArgs['capt_options']!= "x" :
        dict_capt_old['exp_result']=exp_result_old
        try: 
            #result_old = pcta_receive_traffic(dict_capt_options)
            result_old = pcta_receive_traffic(**dict_capt_old)
        except Exception as inst:
            raise PctaContinueError("failed to receive traffic\nexception: %s"  % inst)
    if aArgs.has_key('us_capt_options') and aArgs['us_capt_options']!= "x" :
        dict_capt_us['exp_result'] = exp_result_us
        try: 
            result_us = pcta_receive_traffic(**dict_capt_us)
        except Exception as inst:
            raise PctaContinueError("failed to receive traffic\n %s"  % inst)
    if aArgs.has_key('ds_capt_options') and aArgs['ds_capt_options']!= "x" :   
        dict_capt_ds['exp_result'] = exp_result_ds
        try: 
            result_ds = pcta_receive_traffic(**dict_capt_ds)
        except Exception as inst:
            raise PctaContinueError("failed to receive traffic\n %s"  % inst)

    if result_us == "fail" or result_ds == "fail" or result_old == "fail" :
        result="fail"
        raise PctaContinueError("could not get expect traffic" ) 
    else :
        result="pass"              
    return result

def _start_capt_option(capt_info):

    keyword_name="_start_capt_option"
    
    dict_capt_options = {}
    for item in capt_info.lstrip('[').strip(']').split(',') :
        try :
            m = re.search("\s*(\S+)\s*=\s*(.*)'", item)
            key,value = m.groups()
            key = re.sub("\'|\"|u\'","",key)
        except Exception as inst :
            pass
        finally :    
            dict_capt_options[key]=value

    try :
        dist_id = pcta_start_capture(**dict_capt_options)
        dict_capt_options['dist_id']=str(dist_id)
    except Exception as inst :
        raise PctaContinueError("failed to start capture \nexception: %s" % inst)
    return dict_capt_options


def _send_traffic(send_info):

    keyword_name="_send_traffic"
    dict_send_options={}
    for item in send_info.lstrip('[').strip(']').split(',') :
        try :
            m = re.search("\s*(\S+)\s*=\s*(.*)'", item)
            key,value = m.groups()
            key = re.sub("\'|\"|u\'","",key)
        except Exception as inst :
            pass
        finally :    
            dict_send_options[key]=value.strip("'").strip('"')
    try:
        result = pcta_send_traffic(**dict_send_options)
    except Exception as inst:
        raise PctaContinueError("fail to send traffic\n exception: %s" % inst)


def _filter_packets(compdict={},packets={}) :
    """
    comparing packets content with condition in comdict
    """
    keyword_name = "_filter_packets"
    recv_packet_len = len(packets)
    msg = str(recv_packet_len)+ " Packet(s) Received"
    logger.info(msg)
    matched_packet_num = []

    if compdict.has_key('packet_count'):
        dontcare_scenario = 0
        expect_packets = int(compdict['packet_count'])
        del compdict['packet_count']
    else :
        expect_packets = 1
        dontcare_scenario = 1
    
    for packet in packets :
        flag = 1
        if compdict.has_key('opt82_agent_remote_id_hex') :
            if packet.has_key('dhcp_options') :
                if packet['dhcp_options'].find(compdict['opt82_agent_remote_id_hex']) >= 0:
                    packet['opt82_agent_remote_id_hex'] = \
                    compdict['opt82_agent_remote_id_hex']
                else:
                    continue
            else:
                continue
        for key in compdict.keys() : 
            if packet.has_key(key) :
                if key == 'cfm_flags' :
                    packet[key] = str(int(packet[key],16))
                    compdict[key] = str(int(compdict[key]))

                if type(packet[key]) is list :
                    if compdict[key] in packet[key] :
                        continue
                    else :
                        flag = 0
                        break

                elif packet[key] == compdict[key] :
                    continue
                else :
                    flag = 0
                    break
                      
            else :
                flag = 0
                break
        
        if flag == 1 :
            matched_packet_num.append(packet)
        else :
            continue
    match_packet_len = len(matched_packet_num)
    msg1 = str(match_packet_len)+ " Packet(s) Matched"
    logger.info(msg1)
    return matched_packet_num

def _list_to_dict(args):
    """
        convert a list like ["'a'='aaa'", "'b'='bbb'"] to a dictionary
    """
    x = []
    y = []
    for arg in args:
        arg = arg.encode("ascii")
        arg_list = arg.split('=',1) # first "=" is the seperation sign       
        key=arg_list[0].strip().strip('\'').strip('"')
        try:
            val=arg_list[1].strip('\'').strip()
        except IndexError:
            val = None
        x.append(key)
        y.append(val)

    outdict = dict(zip(x, y)) 
    return outdict 

def _packet_convertor (input='') :
    """
    convert packet content from string to dictionary list
    """
    try :
        packetschk = re.search('{(.*)}' , input)
        matchedcont = packetschk.group(1)
    except Exception as inst :
        raise PctaContinueError("packets string do not match format'{(.*)}'\n\
        exception: %s" % inst)

    matchedcont = matchedcont.lstrip('{').rstrip('}')
    matchedcont = matchedcont.strip(' ')
    matchedcont = re.sub('\s+',' ',matchedcont) 

    try :
        packetlst = matchedcont.split('} {')
        packetscount = len(packetlst)
    except Exception as inst :
        raise PctaContinueError("packets string could not split by '} {''\n\
        exception: %s" % inst)

    packets = []
    packet_num = 0
    for pkt in packetlst :
        packet_num = packet_num + 1
        pkt=" "+pkt

        #add for dealing with negative value. replace "-" with "####".
        patten_num="-\d+"
        patten_char="[a-zA-Z]"
        flag_ne=0

        if re.search(patten_num,pkt) :
            list_1=pkt.split(" ")
            for i,item in enumerate(list_1) :
                if re.match(patten_num,item) and not re.match(patten_char,item) :
                    flag_ne=1 
                    list_1[i]=re.sub("-","####",item)
            pkt=" ".join(list_1)

        splitlst = pkt.split(' -')
        del splitlst[0]
        packet_dict = {}
        for itemInPkt in splitlst :
            itemInPkt = itemInPkt.strip(' ');
            #add for dealing with negative value. recover "-".
            if flag_ne == 1 and "####" in item :
                itemInPkt=itemInPkt.replace("####","-")

            if "\"" in itemInPkt :
                itemInPkt = itemInPkt.replace("\s*"," ")
                packetsplit = itemInPkt.split(' "')
                key = packetsplit[0]
                value = packetsplit[1].strip("\"")
                packet_dict[key] = value
            else :
                mobj = re.match(r'(\w+)\ (.*)',itemInPkt)
                if not mobj :
                    msg = "abnormal item '%s' in packet No.%s" % (itemInPkt,str(packet_num))
                    logger.info("<span style=\"color: #FF8C00\">"+msg+"</span>",html=True) 
                    continue
                key = mobj.group(1)
                value = mobj.group(2)
                if "{" in value and "}" in value :
                    value = re.sub('{','',value)
                    value = re.sub('}','',value)
                    value = value.strip()
                    if key != 'message' and key != 'frame':
                        value = value.split(' ')
                if key == "time_stamp" :
                    value = re.sub('}','',value)
                    
                packet_dict[key] = value
	
        packets.append(packet_dict)

    return packets


def _validate_packets (compdict={},packets={}) :
    """
    comparing packets content with condition in comdict
    """
    keyword_name = "_validate_packets"

    logger.debug("compdict:%s" % compdict)
    logger.debug("packets:%s" % packets)

    msg = "Received packet(s) : "+ str(len(packets))
    logger.info(msg)

    if compdict.has_key('packet_count'):
        check_packet_count = True
        expect_packets = int(compdict['packet_count'])
        del compdict['packet_count']
    else :
        check_packet_count = False

    matched_packet_num = 0
    current_packet_num = 0
    for packet in packets :
        current_packet_num = current_packet_num + 1

        # we hope to check hex mode opt82_agent_remote_id
        # but it only exist in dhcp options as a part
        # if pcta support it by hex mode, we can remove this 'if'
        if compdict.has_key('opt82_agent_remote_id_hex') :
            if packet.has_key('dhcp_options') :
                if packet['dhcp_options'].find(compdict['opt82_agent_remote_id_hex']) >= 0:
                    packet['opt82_agent_remote_id_hex'] = \
                    compdict['opt82_agent_remote_id_hex']
                else:
                    logger.debug("no opt82_agent_remote_id_hex in captured packet: %s" 
                    % (str(current_packet_num)))
            else:
                logger.debug("no dhcp_options in captured packet: %s" % str(current_packet_num))
        if compdict.has_key('cfm_flags') :
            packet['cfm_flags'] = str(int(packet['cfm_flags'],16))
            compdict['cfm_flags'] = str(int(compdict['cfm_flags']))

        packet_matched = True
        for key in compdict.keys() : 
            if packet.has_key(key) :
                if (type(packet[key]) is list and compdict[key] not in packet[key]) \
                or (type(packet[key]) is not list and str(compdict[key]) not in str(packet[key])) :
                    packet_matched = False
                    msg = "'%s' is not expected '%s' for option '%s' in packet No.%s" \
                    % (str(packet[key]),compdict[key],key,str(current_packet_num))
                    logger.info ("<i>"+msg+'</i>',html=True)                 
            else :
                packet_matched = False
                msg = "No value for option '"+key+"' in packet No."+ str(current_packet_num)
                logger.info("<span style=\"color: #FF8C00\">"+msg+"</span>",html=True)               
                break

        if packet_matched :
            matched_packet_num = matched_packet_num + 1
            logger.info ("Packet No." + str(current_packet_num) + " Matched")
        else :
            logger.info ("Packet No." + str(current_packet_num) + " Not Matched")
        
    logger.info ("Matched packet(s) : "+str(matched_packet_num))

    if not check_packet_count :
        logger.info ("Expected packet(s): >= 1")
        if matched_packet_num >= 1 :
            return "pass"
        else :
            return "fail" 
    else :
        logger.info ("Expected packet(s): "+str(expect_packets))
        if matched_packet_num == expect_packets :
            return "pass"
        elif matched_packet_num > expect_packets :
            msg = "Matched packet(s) '%s' is more than Expected packet(s) '%s'" \
            % (str(matched_packet_num),str(expect_packets))
            logger.debug("<span style=\"color: #FF8C00\">"+msg+"</span>",html=True)
            return "fail"       
        else :
            logger.debug("Matched packet(s) '%s' is less than Expected packet(s) '%s'" \
            % (str(matched_packet_num),str(expect_packets)))
            return "fail" 


def _capture_validate_packets (compdict={},packets=[]) :
    """
    comparing packets content with condition in comdict,and return the matched packets.
    """
    keyword_name = "_capture_validate_packets"
    pkts = []
    matched = True
    for packet in packets :
        for option in compdict.keys() :
            #each input option
            temp = packet.has_key(option) and packet[option] or "nonExist"
            if temp != compdict[option] :
                matched = False
        if not matched :
            matched = True
            continue
        else :
            pkts.append(packet)
    return pkts 


class PctaContinueError (AssertionError) :
    ROBOT_CONTINUE_ON_FAILURE = True
    

##############################################
# following keywords going to be obsoleted
##############################################
import os
lib_path = os.environ['ROBOTREPO'] +'/LIBS/DATA_TRANSLATION'
if lib_path not in sys.path:
    sys.path.append(lib_path)
import data_translation  

def pcta_connect (*args):
    """ 
    *Obsoleted*   please use connect_pcta
    """
    keyword_name = "pcta_connect"
    tablename = "TrafficGenData"
    tabledata = data_translation.get_global_table(tablename)
    for entry in tabledata:
        if entry['Type'].encode("ascii") == 'PCTA':
            break
 
    logger.debug("%s:%s -> pcta info:%s " \
    % (__name__,keyword_name,str(entry)))
    
    ipAdress = entry['IPAddress']
    try:
        username = entry['Username']
    except Exception:
        username = entry['UserName']
    password = entry['Password']
    execFile = entry['ExePath']
    
    try :
        clientSlot = entry['ClientSlot']
    except Exception:
        clientSlot = "1"
    else :
        if clientSlot == "x" :
            clientSlot = "1"
            
    try :    
        networkSlot = entry['NetworkSlot']
    except Exception:  
        networkSlot =  "2"
    else :
        if networkSlot == "x" :
            networkSlot = "2"  

    try :    
        client2 = entry['client2']
    except Exception:  
        client2 = None

    try :    
        prompt = entry['prompt']
    except Exception:  
        prompt = "PC_TA_.*>".encode("ascii")
    
    try :    
        available = entry['available']
    except Exception:  
        available =  "True"

    try :    
        networkSlot2 = entry['NetworkSlot2']
    except Exception:  
        networkSlot2 =  "3"
    else :
        if networkSlot2 == "x" :
            networkSlot2 = "3" 

    try :    
        networkSlotlag = entry['NetworkSlotlag']
    except Exception:  
        networkSlotlag =  "4"
    else :
        if networkSlotlag == "x" :
            networkSlotlag = "4"

    global PCTA_SESSION
    try:
        PCTA_SESSION = com_pcta.com_pcta \
        (ipAdress,"22",username,password,execFile,clientSlot,networkSlot,client2,prompt,available,networkSlot2,networkSlotlag)

    except Exception as inst:
        raise AssertionError("%s:%s-> fail to init com_pcta, exception: %s" \
        % (__name__,keyword_name,str(inst))) 
        
    try:       
        PCTA_SESSION.open_pcta()
    except Exception as inst:
        raise AssertionError("%s:%s-> fail to open_pcta, exception: %s" \
        % (__name__,keyword,str(inst)))           
    else :
        logger.info("connect pcta success ") 

   
def pcta_disconnect () :
    """
     *Obsoleted* 
     please user disconnect_pcta
    """
    global PCTA_SESSION   
    keyword_name = "pcta_disconnect"

    try:
        PCTA_SESSION.close_pcta()
        del PCTA_SESSION
    except Exception as inst:
        raise AssertionError("%s:%s-> fail to close pcta session, exception: %s" \
        % (__name__,keyword_name, str(inst)))      
    else:
        logger.info("disconnect pcta connection success " )
 


