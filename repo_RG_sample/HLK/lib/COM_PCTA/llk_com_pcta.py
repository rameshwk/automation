import re,time,sys
from robot.api import logger
from copy import deepcopy
import pcta_command 

sINFO_LOG = ''

def llk_traffic_pcta_get_cmd (prot,cmd_type) :
    sCmd = pcta_command.pcta_get_command (prot,cmd_type)
    return sCmd 

def llk_traffic_pcta_connect (**pcta_info) :
    logger.info("pcta CONNECT")
    pcta_command.connect_pcta(**pcta_info)

def llk_traffic_pcta_disconnect () :
    logger.info("pcta DISCONNECT")
    pcta_command.disconnect_pcta()

def llk_traffic_pcta_issue_cmd (command,prompt=None):
    pcta_command.send_pcta_command(command,prompt)

def llk_traffic_pcta_start_capture (**args) :
    pcta_command.pcta_start_capture (**args)

def llk_traffic_pcta_start_measure (**args) :
    pcta_command.pcta_start_measure (**args)

def llk_traffic_pcta_stop_cmd (**args) :
    packets = []
    packets = pcta_command.pcta_stop_cmd (**args)
    return packets

def llk_traffic_pcta_send_traffic (**args) :
    status,dist_id = pcta_command.pcta_send_traffic (**args)
    return dist_id

def _append_to_info_log(msg) : 
    global sINFO_LOG 
    logger.debug(msg)
    sINFO_LOG = sINFO_LOG + msg + '\n'

def llk_traffic_pcta_verify_measure (streamId,compdict={},results={}) :
    """
    comparing measure result content with condition in comdict
    """
    keyword_name = "llk_traffic_pcta_verify_measure"
    global sINFO_LOG
    compdict = dict(map(lambda (k,v): (str(k).lower(), v), compdict.iteritems()))

    sINFO_LOG = ''
    sExpResult = 'pass'
    try :
        for each in ['exp_result','expect_result'] :
            if compdict.has_key(each) : 
                sExpResult = compdict.pop(each).lower()

        msg = '\nVerify Streamblock: %s\n' % streamId
        _append_to_info_log(msg)
        msg = "Verify Condition: %s\n" % compdict
        _append_to_info_log(msg)
        logger.trace("packets:%s" % results)
    
        msg = "Received Result(s) : "+ str(len(results))
        _append_to_info_log(msg)
    
        result_matched = False
        current_result_num = 0
        fDefaultTolerance = 0
        for result in results :
            current_result_num = current_result_num + 1
            result_matched = True
            sResult = ''
            if compdict.has_key('default_tol') : 
                sTol = compdict.pop('default_tol')
                if '%' in str(sTol) : 
                    sTol = sTol.strip('%')
                try : 
                    fDefaultTolerance = float(sTol)/100.0
                except Exception as inst:
                    fDefaultTolerance = float(5/100.0)
    
            for key in compdict.keys() :
    
                # skip the single _tol key
                if '_tol' in key :
                    continue
    
                if result.has_key(key) :

                    if '<' in str(compdict[key]) or '>' in str(compdict[key]) or '!' in str(compdict[key]) :
                        if eval(str(result[key])+str(compdict[key])) : 
                            #result_matched = True
                            msg = "***Matched %s***: Expected:%s, Obtained:%s " % (key,compdict[key],result[key])
                            _append_to_info_log(msg)
                        else :  
                            result_matched = False
                            msg = "Not Matched %s: Expected:%s, Obtained:%s " % (key,compdict[key],result[key])
                            _append_to_info_log(msg) 

                    elif '(' in str(compdict[key]) and ')' in str(compdict[key]):
                        sMin,sMax = compdict[key].strip('\(\)').split(',')
                        if sMin and sMax : 
                            sValidateCmd = "float(sMin) <= float(result[key]) <= float(sMax)"
                        elif sMin :
                            sValidateCmd = "float(sMin) <= float(result[key])"
                        elif sMax :
                            sValidateCmd = "float(result[key]) <= float(sMax)"
                        else :
                            raise AssertionError("Invalid param format: %s" % compdict[key])

                        if eval(sValidateCmd):
                            #result_matched = True
                            sResult = 'PASS'
                        else:
                            result_matched = False
                            sResult = 'FAIL'

                        msg = "\nMeasure parameter : %-15s" % str(key) + "\n" \
                            + "Expected value : %-15s" % compdict[key] + "\t" + "Obtained value : %-15s" % result[key] + "\n" \
                            + "Result : %-5s" % sResult
                        _append_to_info_log(msg)

                    else :

                        try :
                            # it's a float value
                            fExpectedValue = float(compdict[key])
                            fObtainedValue = float(result[key])

                            # calculate fAllowedTolerance
                            fAllowedTolerance = 0
                            sToleranceKey = key + '_tol'
                            sCriteria = ''
                            if compdict.has_key(sToleranceKey):
                                fAllowedTolerance = float(compdict[sToleranceKey].strip('%'))/100.0
                                sCriteria = compdict[sToleranceKey]
                            else :
                                fAllowedTolerance = fDefaultTolerance
                                sCriteria = fDefaultTolerance

                            fAllowedTolerance = float(fAllowedTolerance*fExpectedValue)

                            if float(fExpectedValue-fAllowedTolerance) <= float(fObtainedValue) <= float(fExpectedValue+fAllowedTolerance):
                                #result_matched = True
                                sResult = 'PASS'
                            else:
                                result_matched = False
                                sResult = 'FAIL'

                            msg = "\nMeasure parameter : %-15s" % str(key) + "\n" \
                            + "Expected value : %-15s" % compdict[key] + "\t" + "Obtained value : %-15s" % result[key] + "\n" \
                            + "Criteria : %-20s" % ("+/-"+str(sCriteria)) + "\t" + "        Result : %-5s" % sResult
                        
                        except Exception as inst:
                            # not a float value,just a str value
                            if compdict[key] != result[key] :
                                result_matched = False
                                msg = "\nNot Matched %s***: Expected:%s, Obtained:%s " % (key,compdict[key],result[key])
                            else :
                                msg = "\n***Matched %s***: Expected:%s, Obtained:%s " % (key,compdict[key],result[key])

                        _append_to_info_log(msg)

                else :
                    result_matched = False
                    msg = "No value for option '"+key+"' in result No."+ str(current_result_num)
                    _append_to_info_log(msg)
                    break
    
            # all compdict checked and got the matched result, quit of checking result list
            if result_matched == True : 
                break
   
        sRealResult = '' 
        if result_matched :
            sRealResult = 'pass'
        else :
            sRealResult = 'fail'

        if sRealResult == sExpResult : 
            sResult =  "pass"
        else :
            sResult =  "fail"

        msg = "\r\nVerify %s : expect_result '%s', real_result '%s'" % (sResult.upper(),sExpResult,sRealResult)
        _append_to_info_log(msg)
        logger.info("<span style=\"color: #0000FF\">"+sINFO_LOG+"</span>",html=True)

        return sResult

    except Exception as inst:
        s = sys.exc_info()
        raise AssertionError('llk_traffic_pcta_verify_measure has invalid syntax,lineno:%s\nException:%s' % (s[2].tb_lineno,inst))

def llk_traffic_pcta_verify_capture (streamId,compdict={},packets={}) :
    """
    comparing packets content with condition in comdict
    """
    keyword_name = "llk_traffic_pcta_verify_capture"
    global sINFO_LOG
    sINFO_LOG = ''
    sExpResult = 'pass'

    compdict = dict(map(lambda (k,v): (str(k).lower(), v), compdict.iteritems()))

    for each in ['exp_result','expect_result'] :
        if compdict.has_key(each) : 
            sExpResult = compdict.pop(each).lower()

    msg = '\nVerify Streamblock: %s\n' % streamId
    _append_to_info_log(msg)
    msg = "Verify Condition: %s\n" % compdict
    _append_to_info_log(msg)

    logger.trace("packets:%s" % packets)

    msg = "Received packet(s) : "+ str(len(packets))
    _append_to_info_log(msg)

    if compdict.has_key('packet_count'):
        check_packet_count = True
        expect_packets = compdict['packet_count']
        del compdict['packet_count']
    else :
        check_packet_count = False

    matched_packet_num = 0
    current_packet_num = 0
    for packet in packets :
        current_packet_num = current_packet_num + 1

        # we hope to check hex mode opt82_agent_remote_id
        # but it only exist in dhcp options as a part
        # if pcta support it by hex mode, we can remove this 'if'
        if compdict.has_key('opt82_agent_remote_id_hex') :
            if packet.has_key('dhcp_options') :
                if packet['dhcp_options'].find(compdict['opt82_agent_remote_id_hex']) >= 0:
                    packet['opt82_agent_remote_id_hex'] = \
                    compdict['opt82_agent_remote_id_hex']
                else:
                    logger.debug("no opt82_agent_remote_id_hex in captured packet: %s" 
                    % (str(current_packet_num)))
            else:
                logger.debug("no dhcp_options in captured packet: %s" % str(current_packet_num))
        if compdict.has_key('cfm_flags') :
            packet['cfm_flags'] = str(int(packet['cfm_flags'],16))
            compdict['cfm_flags'] = str(int(compdict['cfm_flags']))

        packet_matched = True
        for key in compdict.keys() : 
            if packet.has_key(key) :
                if (type(packet[key]) is list and compdict[key] not in packet[key]) \
                or (type(packet[key]) is not list and packet[key] != compdict[key]) :
                    packet_matched = False
                    msg = "'%s' is not expected '%s' for option '%s' in packet No.%s" \
                    % (str(packet[key]),compdict[key],key,str(current_packet_num))
                    _append_to_info_log(msg)           
            else :
                packet_matched = False
                msg = "No value for option '"+key+"' in packet No."+ str(current_packet_num)
                _append_to_info_log(msg)
                break

        if packet_matched :
            matched_packet_num = matched_packet_num + 1
            msg = "Packet No." + str(current_packet_num) + " Matched"
            _append_to_info_log(msg)
        else :
            msg = "Packet No." + str(current_packet_num) + " Not Matched"
            _append_to_info_log(msg)

    msg = "Matched packet(s) : "+str(matched_packet_num)
    _append_to_info_log(msg)

    sRealResult = ''
    if not check_packet_count :
        msg = "Expected packet(s): >= 1"
        _append_to_info_log(msg)
        if matched_packet_num >= 1 :
            sRealResult = "pass"
        else :
            sRealResult = "fail" 
    else :
        msg = "Expected packet(s): "+str(expect_packets)
        _append_to_info_log(msg)

        cmd_validate_pkt_cnt = ''
        try:
            cmd_validate_pkt_cnt = matched_packet_num + '==' + int(expect_packets)
        except Exception as inst:
            # expect_packets has char '<','>','!','='
            cmd_validate_pkt_cnt = str(matched_packet_num) + str(expect_packets)

        if eval(cmd_validate_pkt_cnt) :
            sRealResult =  "pass"
        else :
            msg = "Matched packet(s) '%s' is not equal Expected packet(s) '%s'" \
            % (str(matched_packet_num),str(expect_packets))
            _append_to_info_log(msg)
            sRealResult =  "fail" 

    if sRealResult == sExpResult :
        sResult =  "pass"
    else :
        sResult =  "fail"

    msg = "\r\nVerify %s : expect_result '%s', real_result '%s'" % (sResult.upper(),sExpResult,sRealResult)
    _append_to_info_log(msg)
    logger.info("<span style=\"color: #0000FF\">"+sINFO_LOG+"</span>",html=True)

    return sResult
