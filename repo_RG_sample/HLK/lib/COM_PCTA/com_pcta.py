import os,sys,time,re
import socket,paramiko
from robot.api import logger


try:	
    lib_path =  os.environ['ROBOTREPO'] +'/LIBS/COM_PCTA'
    if lib_path not in sys.path:
        sys.path.append(lib_path)
    
except Exception:
    raise AssertionError("Module: %s -> Fail to set sys.path" % __name__)
    
""" 
    To login the pcta server and launch the pcta 
    
    Its done with the help of paramiko module using ssh protocol

Author:  
Mahalakshmi Venkatraman 	Developed	 
"""
class com_pcta(object) :
    """ 
    com_pcta is the class will have ip, port, username, password and exec_file arguments.
        
    Author:  
    Mahalakshmi Venkatraman 	Developed	 
    """
    def __init__(self,ip,port,username,password,exec_file,client_slot,network_slot,client2_slot,prompt,available,network_slot2,network_slotlag) :
    
        self.exec_file = exec_file.encode("ascii")
        if exec_file :
            self.exec_file = str(exec_file.encode("ascii"))
        else :
            self.exec_file = '/root/PCTA/pcta.exe'

        self.ip = ip.encode("ascii")
        if port :
            self.port = int(port.encode("ascii"))
        else :
            self.port = 22
        self.username = username.encode("ascii")
        if not password or password.lower() == 'x' :
            self.password = None
        else :
            self.password = password.encode("ascii")
        self.exec_file = exec_file.encode("ascii")
        self.client_slot = client_slot.encode("ascii")
        self.network_slot = network_slot.encode("ascii")
        if client2_slot :
            self.client2_slot = client2_slot.encode("ascii")
        else :
            self.client2_slot = None
        if network_slot2 :
            self.network_slot2 = network_slot2.encode("ascii")
        else :
            self.network_slot2 = '3'
        if network_slotlag :
            self.network_slotlag = network_slotlag.encode("ascii")
        else :
            self.network_slotlag = '4'           
        if prompt :
            self.prompt = prompt.encode("ascii")
        else :
            self.prompt = "PC_TA_.*>".encode("ascii")  
        if str(available).lower() == 'false' :
            self.available = 'False'
        else :
            self.available = 'True'          
        #transport and channel are the object variables which will have the transport & channel objects.
        self.transport = ""
        self.channel = ""
        if not self.available :
            msg = "PCTA is not available. all relative check will be skiped"
            logger.info("<span style=\"color: #FF8C00\">"+msg+"</span>",html=True) 
                              	
    def open_pcta(self):
        """ 
        open_pcta will establish the ssh connection using paramiko and create a transport & channel objects for the
          ssh session & invoke the shell. Set the pty terminal session to run the sudo commands. Set default timeout
          as '3'. The channel will wait upto 3 seconds to get the data from the socket, else return.
            
        Author:  
        Mahalakshmi Venkatraman 	Developed	 
        """
        if not self.available or self.available.lower() == "false":
            return  "pass"
        keyword_name = "open_pcta"
        logger.debug("%s:%s " % (__name__,keyword_name))
        retry = 1
        while retry < 11:
            try:                                
                self.transport = paramiko.Transport((self.ip, self.port))
            except Exception as inst:
                if retry == 10:
                    raise AssertionError("%s:%s-> transport error for ip='%s' port='%s', exception:%s" \
                    % (__name__,keyword_name,self.ip,str(self.port),inst) )
                logger.debug ( str(retry) + " : " + "transport failure,retry..." )
                retry += 1
                time.sleep(2)
                continue

            try :  
                if self.password :                    
                    self.transport.connect(username=self.username, password=self.password)
                else :
                    private_key_file = os.path.expanduser('~/.ssh/id_rsa')
                    my_key = paramiko.RSAKey.from_private_key_file (private_key_file)
                    self.transport.connect(username=self.username,pkey=my_key)
                    
                self.channel = self.transport.open_session()
                self.channel.settimeout(3)
                self.channel.get_pty('vt100')
                self.channel.invoke_shell()
                logger.debug("SSH session established successfully...")
            except Exception as inst:
                if retry == 10:
                    print AssertionError("fail to do password validation, exception:\n%s" % (inst))
                try :
                    self.channel.close()
                    self.transport.close()
                except Exception as inst :
                    logger.debug ( "close channel or transport failure for exception: % ,retry..." % inst )

                logger.debug ( str(retry) + " : " + "fail to open session,retry..." )
                retry += 1
                time.sleep(3)
                continue
            else:
                break

        pcta_path = os.path.dirname(self.exec_file)
        dir_prompt = "\$|\#|\%"
        switch_dir =  "cd "+ pcta_path 

        # Switch directory to given 'exec_dir'
        return_info = self._exec_command(switch_dir,dir_prompt,"switch directory",10)     
        return_info = self._exec_command('pwd',pcta_path,"check directory",10)
        if not re.search(pcta_path,return_info):
            return_info = self._exec_command(switch_dir,dir_prompt,"switch directory again",20)
            return_info = self._exec_command('pwd',pcta_path,"check directory",10)
        if not re.search(pcta_path,return_info):    
            raise AssertionError("%s:%s -> fail to switch directory" \
            % (__name__,keyword_name))
                    
        # launch the pcta with the sudo permission   
        run_pcta = "sudo " + pcta_path + "/" + os.path.basename(self.exec_file)
        return_info = self._exec_command(run_pcta,self.prompt,"execute pcta process",10)
        if not re.search(self.prompt,return_info) :		    
            self.channel.close()
            self.transport.close()
            raise AssertionError("%s:%s -> fail to execute pcta process : %s" \
            %(__name__,keyword_name,return_info))
  
        self.channel.settimeout(60)
        self.session_alive = True     
        time.sleep(5)
        return "pass"
	
    def reopen_pcta(self):
        if not self.available :
            return "pass"
        keyword_name = "reopen_pcta"
        logger.debug("%s:%s " % (__name__,keyword_name))
        iretry_count = 10
        i = 0
        while ( i <= iretry_count ) :
            i += 1
            msg = "try to reopen pcta for the %s(th) time" % str(i)
            logger.debug("%s:%s -> %s" % (__name__,'reopen_pcta',msg))
            try:
                self.open_pcta()
            except Exception as inst:
                pass
            if self.session_alive :
                return "pass"
            else :
                time.sleep(1)
                continue

        raise AssertionError("%s:%s ->Can't open PCTA session, exception:%s" \
                    % (__name__,keyword_name,inst))	
      
    def close_pcta(self):
        """ 
        close_pcta will close the ssh connection which is established using paramiko module
          
        Author:  
        Mahalakshmi Venkatraman 	Developed	 
        """
        if not self.available :
            return "pass"
                    
        keyword_name = "close_pcta"
        resultFlag = "OK"
        self.channel.settimeout(3)
        try:
            command = "exit\n"
            expectPrompt = "$|#|%"
            msg = "exit command execution"
            self._exec_command(command,expectPrompt,msg,5)
            self.channel.close()
            self.transport.close()
        except Exception as inst:
            msg = "PCTA can't been closed"
            raise AssertionError("%s:%s -> %s, exception: %s" \
            % (__name__,keyword_name,msg, inst))
             
        return "pass"         
    
    def send_command(self,pctaCmd,prompt=None):
        if not self.available :
            msg = "PCTA is not available. all relative check will be skiped"
            logger.info("<span style=\"color: #FF8C00\">"+msg+"</span>",html=True)
            return  "pass"
                
        keyword_name = "send_command"
        pctaCmd = pctaCmd + "\n"
        if not prompt :
            prompt = 'PC_TA_.*'    
        i = 1
        while (i < 3) :
            i += 1
            result = self._ssh_sendcmd(pctaCmd,prompt)
            if re.search( prompt,result) :
                return result
            else :
                logger.debug("not gotten '%s'\n PCTA response: %s" % (prompt,result))
        
        #logger.debug("re-open pcta for next command sending")
        logger.debug("re-open pcta to try to re-run current failed command: %s" % pctaCmd)
        try :
            self.close_pcta()
            self.session_alive = False
            time.sleep(2)
            self.reopen_pcta()

            i = 1
            while (i < 3):
                i += 1
                result = self._ssh_sendcmd(pctaCmd, prompt)
                if re.search(prompt, result):
                    return result
                else:
                    logger.debug("After reopen PCTA, not gotten '%s'\n PCTA response: %s" % (prompt, result))
                    raise AssertionError("fail to get prompt for pcta command:" + pctaCmd)
        except Exception as inst :
            raise AssertionError("fail to reopen_pcta for exception: %s" % inst)
        #else :
            #import traceback
            #logger.debug("Failed to get prompt for exception:\n %s" % traceback.print_exc())
        #    raise AssertionError("fail to get prompt for pcta command:" +pctaCmd)


    def _exec_command(self,command,expectPrompt,message="execute command",timeout=5):
        keyword_name = "_exec_command"
        returnTmp = ""
        try:
            returnTmp = self._ssh_sendcmd(command+"\n")
            logger.debug("write:'%s', expect:'%s'" % (command,expectPrompt))
            logger.debug("get return:\n%s\n" % returnTmp)
        except Exception as inst:
            msg = "fail to " + message
            raise AssertionError("%s:%s-> %s,exception:%s" % (__name__,keyword_name,msg,inst))
        return returnTmp

  
    def _ssh_sendcmd(self,cmd,prompt=None):
        result = ""
        data = ""	
        if not prompt :
            prompt = "PC_TA_.*>"	
        self.channel.send(cmd)
        time.sleep(0.1)
        # read the socket buffer until the channel exit_status_ready state
        while not self.channel.exit_status_ready():
            if self.channel.recv_ready() :
                # read the socket buffer
                data = self.channel.recv(2048)
                result = data
                while data :
                    try :
                        if re.search(prompt,result) :
                            return result
                        else :
                            data = self.channel.recv(1024)
                            result += data
                            continue
                    except socket.timeout:
                        return result
                    except Exception as inst :
                        logger.debug("Error : %s" % str(inst))
                        return result
            if self.channel.recv_stderr_ready():
                error_buf = self.channel.recv_stderr(1024)
                errinfo = ""
                while error_buf :
                    errinfo += error_buf
                    try :
                        error_buf = self.channel.recv_stderr(1024)
                    except Exception as inst :
                        logger.debug("Error : %s" % str(inst))
                        return errinfo  
			           
        return result
 

