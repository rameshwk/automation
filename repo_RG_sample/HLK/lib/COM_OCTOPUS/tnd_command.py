import os,sys,time,re, pexpect
from robot.api import logger

class octopus_object (object) :
    def __init__(self,ip,port='23',username='root',password='2x2=4',prompt=None,timeout=60,waittime=600):
        self.ip = ip.encode("ascii")
        self.port = port.encode("ascii")
        self.username = username.encode("ascii")
        self.password = password.encode("ascii")
        if prompt :
            self.prompt = prompt.encode("ascii")
        else :
            self.prompt = '.*>'.encode("ascii")
            
        self.timeout = int(timeout)
        self.waittime = int(waittime)
        self.session_alive = False


    def _init_octopus_udp(self):
        #octopus_cmd = os.environ['ROBOTREPO']+"/PACKAGES/bin/octopus STDIO " \
        octopus_cmd = os.environ['ROBOTREPO']+"/../../../TOOLS/octopus STDIO " \
                      + self.ip + ":udp:" + self.port + "\n"
        try:
            self.interface = pexpect.spawn(octopus_cmd,timeout=self.timeout)
        except Exception :
            raise AssertionError("%s -> fail to initial octopus by command:%s" % (__name__,octopus_cmd))
        else :
            logger.debug (octopus_cmd)
        
        max_retry = 20
        i = 1       
        self.interface.sendline("\n")  # GPON octopus need another \\n
        while (i < max_retry) :              
            try :              
                i += 1
                return_val = self.interface.expect(['Login: *$', \
                'Password:.*',\
                self.prompt,\
                'TASKS BUSY.*',\
                pexpect.EOF, pexpect.TIMEOUT])
            except Exception as inst :
                logger.debug ("%s -> gotten return: '%s'" % (__name__, str(self.interface.after)))
                logger.debug ("%s -> try again" % __name__)
                self.interface.close()
                return "fail"
            else :
                logger.debug ("gotten: "+ str(self.interface.after))
                   
            if return_val == 0 :
                logger.debug("%s-> send: %s" % (__name__,self.username))
                self.interface.sendline(self.username)
                continue
            elif return_val == 1 :
                logger.debug("%s-> send: %s" % (__name__,self.password))
                self.interface.sendline(self.password)
                continue
            elif return_val == 2 :
                logger.debug ("success to open an octopus session")
                self.session_alive = True
                return "pass"
            elif return_val == 3 :
                logger.debug("Fail to login NT board by TASKS BUSY:\n %s" % octopus_cmd)
                time.sleep(self.timeout)
                logger.info("Will wait %d seconds to end the tasks busy status and try to reconnect it again" % self.waittime)
                return "fail"

            elif return_val == 5 :
                logger.debug("%s-> send: \\n" % __name__)
                self.interface.sendline("\n")
                continue                
            else :
                self.interface.close ()
                time.sleep(5)
                logger.debug ("send again:" + octopus_cmd)
                self.interface = pexpect.spawn(octopus_cmd,timeout=self.timeout)
                self.interface.sendline("\r")

        if i == max_retry :
            self.interface.close ()
            logger.info("%s -> fail to open octopus connection after %s times try" % (__name__,str(i)))
            return "fail"


    def open_octopus_session (self):
        res = 'fail'
        keyword_name = "open_octopus_session"
        re_connectTime = 1
        currentTime = time.mktime(time.localtime())
        endTime = currentTime + int(self.waittime)
        while currentTime <= endTime :
            currentTime = time.mktime(time.localtime())
            try:
                
                res = self._init_octopus_udp()
                if self.session_alive:
                    logger.info("%s:%s-> open tnd session success" % (__name__,keyword_name))
                    return res
                else:
                    time.sleep(5)
                    re_connectTime += 1                
            except Exception as inst:
                logger.debug ("%s:%s-> gotten exception for tnd connection : %s" % (__name__,keyword_name,inst))
                re_connectTime += 1
                logger.debug ("tnd connect failed, try it the %d(th) time"  % re_connectTime)
                time.sleep(5)
        logger.debug("%s:%s-> failed to connect tnd in the %d seconds." % (__name__,keyword_name,self.waittime))
        return res
    

    def send_command(self,cmd,timeout=0,prompt=None):
        """
        This keyword is used to Send the T&D Command through Octopus 
        
        """
	
        keyword_name = 'send_command'
        if  not prompt :
            prompt = self.prompt
            
        if timeout != 0 :
            timeout = int(timeout)
        else:
            timeout = 60
        i = 0
        while (i < 3) :
            i = i + 1
            # Use a loop to clean up long buffer  
            #while True:
            #    clean_up = self.interface.expect([prompt,pexpect.EOF,pexpect.TIMEOUT],timeout=0.5) 
            #    output = str(self.interface.after)
            #    logger.debug ("clean up output:\n" + output)
            #    if 'pexpect.TIMEOUT' in output or output == '':
            #        break
          
            try :
                self.interface.sendline(cmd)
            except Exception as inst:
                self.open_octopus_session()
                self.interface.sendline(cmd)
                continue
            else:
                try :
                    cmdout = "" 
                    rep = self.interface.expect([prompt ,\
                    #rep = self.interface.expect([cmd ,\
                        'Inactivity timeout',\
                        'err_code.*',\
                        'T&D console.*login',\
                        pexpect.TIMEOUT],timeout=timeout) 
                except Exception as inst:
                    logger.debug ("gotten exception:" + str(inst))            
                    continue
                                   
                if rep == 0 or rep == 4 or rep ==3 and "login board" in cmd:
                    return self.interface.before + self.interface.after 
                else  :
                    continue
        if i == 4 :
             raise AssertionError ("%s:%s -> failed cmd : %s" \
             % (__name__,keyword_name,cmd))
        

    def close_octopus_session(self):
        """
        This keyword is used to Logout the created T&D Session	
        """
        keyword_name = 'close_tnd'
        try :
            self.interface.sendline('exit')
            self.interface.expect(['Logout .* console.*'])
            self.interface.sendline('\003')
            self.interface.expect(['.*octopus.*'])
            self.interface.sendline('q')
            self.interface.expect(['quit'])
            self.interface.expect([pexpect.EOF])
            self.interface.close()	
        except Exception as inst :
            raise AssertionError("%s:%s -> fail to close tnd: %s" \
             % (__name__,keyword_name,inst)) 
   

TND_SESSION = {}

def connect_tnd (ip='0.0.0.0',tnd_port='23',tnd_username='shell',tnd_password='nt', \
tnd_session_name="first_tnd_session",prompt=None,timeout=10,waittime=660) :
    """ 
        build up the trace & debug session for sending TnD command
    usage example:
    | connect tnd | 135.251.197.239 | 23 | shell | nt | 
    | connect tnd | 135.251.197.239 | 23 | shell | nt |
    |   |  tnd_session_name=this_tnd |
    | connect tnd | 135.251.197.239 | 23 | shell | nt |
    |   | tnd_session_name=this_tnd | prompt=.*> |
    """
    keyword_name = 'connect_tnd'
    logger.debug ( "input: %s, %s, %s,%s,%s,%s,%s : " \
    % (ip,tnd_port,tnd_username,tnd_password,tnd_session_name,str(prompt),str(timeout)))

    try:
        TND_SESSION[tnd_session_name] = octopus_object \
            (ip,port=tnd_port,username=tnd_username,password=tnd_password,prompt=prompt,timeout=timeout,waittime=waittime)
    except Exception as inst:
        raise AssertionError("%s:%s -> fail to init tnd: %s" \
        % (__name__,keyword_name,inst)) 
        
    try:
        TND_SESSION[tnd_session_name].open_octopus_session()
    except Exception as inst:
        raise AssertionError("%s:%s -> fail to open tnd: %s" \
        % (__name__,keyword_name,inst))        
       
    return TND_SESSION[tnd_session_name]
    

def disconnect_tnd (tnd_session_name="first_tnd_session") :
    """
        deleted the active trace & debug session
    usage example :
    | disconnect tnd |
    | disconnect tnd | tnd_session_name=this_tnd |

    """
    keyword_name = 'disconnect_tnd'
    try:
        TND_SESSION[tnd_session_name].close_octopus_session()
    except Exception as inst:
        raise AssertionError("%s:%s -> fail to close tnd session, exception:'%s'" \
        % (__name__,keyword_name,inst))         
    
    TND_SESSION.pop(tnd_session_name)


def send_tnd_command(command,timeout=0,prompt=None,tnd_session_name="first_tnd_session"):
    """
        send single tnd command 
    usage example :
    | send tnd command | gpon ONT ontSessionClientIpAddr  135 251 29 24 |
    | send tnd command | gpon ONT ontSessionClientIpAddr  135 251 29 24 | prompt=.*> |
    |   | tnd_session_name=this_tnd |   
    """

    try:
        if prompt :
            res = TND_SESSION[tnd_session_name].send_command \
            (command,timeout=int(timeout),prompt=prompt)
        else :
            res = TND_SESSION[tnd_session_name].send_command \
            (command,timeout=int(timeout))
        logger.info("<span style=\"color: #00008B\">TND CMD >> "+command+"</span>",html=True)
    except Exception as inst:
        raise AssertionError("%s-> fail to send command: %s: %s" % (__name__,command,str(inst)))        
    else :
        logger.info ( "TND REPLY << %s" % res )
        return res


def get_tnd_output (command,timeout="5",prompt=None,tnd_session_name="first_tnd_session"):

    """
        return the response of trace debug command

    usage example :
    | get tnd output | gpon ONT getCfg 1103 0 0 | timeout=10 |
    | get tnd output | gpon ONT getCfg 1103 0 0 | prompt=.*> |
    |   | tnd_session_name=this_tnd |
    """
    keyword_name = 'get_tnd_output'
    timeout = int(timeout.encode("ascii"))
    
    try:  
        logger.info("<span style=\"color: #00008B\">"+"TND CMD >> %s" % command+"</span>",html=True)
        res = TND_SESSION[tnd_session_name].send_command \
        (command,timeout=timeout,prompt=prompt)
        logger.info ( "TND REPLY << %s" % res )
    except Exception as inst:
        raise AssertionError("%s -> fail to send command: %s: %s" \
        % (__name__,command,inst))        
    else :
        return res
    
def login_tnd_lt(lt_slot="1103", timeout="20", prompt=None, tnd_session_name="first_tnd_session"):
    """
        return 
    usage:
    | login_tnd_lt | 1103 | timeout=8 |
    """
    keyword_name = 'login_tnd_lt'
    timeout = int(timeout.encode("ascii"))
    busy_tag = "TASKS BUSY"
    error_record = "ERROR RECORD"
    not_login = True
    count = 0

    try:
        logger.info("<span style=\"color: #00008B\">"+"Login LT board >> %s" % lt_slot +"</span>",html=True)
        while not_login and count < 3 :
            res = TND_SESSION[tnd_session_name].send_command("login board %s" % lt_slot, prompt=prompt)
            logger.info("TND REPLY << %s" % res)
            not_login = _match_in_multi_line(busy_tag, res)
            if not_login:
                TND_SESSION[tnd_session_name].send_command("rcom exec -b %s -c login kill 0" % lt_slot, prompt=prompt)
                time.sleep(300)
                count = count + 1
            else:
                logger.info("TND REPLY << %s" % res)
                logger.info("Success to login LT board: %s , will check for error record" % lt_slot)
                break
        else:
            logger.info("Fail to login LT board: %s" % lt_slot)
            raise AssertionError("Blocked by task busy, failed to login lt: %s" % lt_slot)

        while timeout > 0:
            res = TND_SESSION[tnd_session_name].send_command("\n", prompt=prompt)
            if _match_in_multi_line(error_record, res):
                time.sleep(.5)
                timeout = timeout - 1        
                # logger.info("Returned res: %s " % res )
            else:
                logger.info("No more error record found, lt board is ready now.")
                break
        else:
                logger.info("More error record found, lt board is not ready.")
                return False

    except Exception as inst:
        raise AssertionError("%s _> fail to login LT board by exception: %s" % inst)

def check_tnd_output (command,search_pattern=None,\
    expect_result="PASS",prompt=None,tnd_session_name="first_tnd_session",timeout=5) :
    """
        check if the output of trace debug command match expect search pattern
        
    usage example:
        
    | memm_delta = | check_tnd_output | memm free_memm | 
    |   | search_pattern=Delta:\s+(\d+) |
    | get tnd output | gpon ONT getCfg 1103 0 0 | prompt=.*> |
    |   | tnd_session_name=this_tnd |
        
    """
    keyword_name = 'check_tnd_output'

    try:
        #logger.debug ("TND CMD >> " + command ) 
        logger.info ("<span style=\"color: #00008B\">"+"TND CMD >> %s" % command+"</span>",html=True)  
        res = TND_SESSION[tnd_session_name].send_command \
        (command,timeout=timeout,prompt=prompt)
        logger.info ( "TND REPLY << %s" % res )
    except Exception as inst:
        raise AssertionError("%s -> fail to send command %s, exception: %s" \
        % (__name__,command, str(inst)))  
    if not search_pattern :
        return           
    m = re.search (search_pattern,res,re.M) 
    is_match = _match_in_multi_line (search_pattern,res)
    if is_match :
        exec_result = "PASS"
    else :
        exec_result = "FAIL"
    if exec_result == expect_result.upper() :
        return is_match
    else :
        raise AssertionError ("result '%s' was not expected '%s' for output : %s" %
         (exec_result,expect_result,str(res)))
    
     
def _match_in_multi_line (pattern_string, lines) :

    found = True  
    command,lines = lines.split("\n",1)

    res = re.search(pattern_string,lines,re.DOTALL) 
    if not res :
        logger.debug("not found : %s" % pattern_string)
        found = False
    else :
        logger.debug("found : %s" % pattern_string)   
                 
    if found :
        matched = res.groups()       
        if  len(matched) == 0 :
            matched = res.group(0)
        elif len(matched) == 1 :
            matched = matched[0]
        else :
            matched = list(matched)
    else :
        matched = False      
        
    #logger.debug("all items to find in output: %s" % item_list) 
    return matched     
    
