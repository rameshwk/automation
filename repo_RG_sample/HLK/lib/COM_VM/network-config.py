__switch_code__ = "Miguel Cabrerizo - ArtemIT Labs for Open vSwitch"
__edited_by__ = "Abhijit Sipani--To be used for Nokia internal use"

# Common libraries
import socket
import os
import socket
import fcntl
import struct
import subprocess
import time


''' This script will create network configuration scripts and load them for CentOS.'''


def __get_ip_address(ifname):

    '''
    This function get_ip_address() takes an interface name as the input and returns the
    ip_address for that interface by using the socket library.
    '''

    try:
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        return socket.inet_ntoa(fcntl.ioctl(
               s.fileno(),
               0x8915,  # SIOCGIFADDR
               struct.pack('256s', ifname[:15])
               )[20:24])
    except IOError as inst:
                        raise IOError ("\n\nPlease make sure that your interface is enabled" \
                                       "\nYour IP Addressing is correct and also make sure" \
                                       "\nthat your DHCP server is up and running\t%s"%inst)


def __get_static_ip_and_mask(static_ip_address, static_ip_gateway, static_ip_netmask, dns_flag, dns_server):

    '''
    This function takes the static IP parameters provided by the calling function and
    checks if the IP address formatting is correct by using the socket library. If
    found ok, it returns all the parameters in a list. If not it will throw an
    exception for the wrong IP address
    '''

    ip = ''
    while not ip:
            ip = static_ip_address
            try:
                '''This step checks if the static IP address was provided in the correct format'''
                socket.inet_aton(ip)
            except socket.error:
                print 'Wrong format for the IP ADDRESS. Please, try again'
                ip = ''

    mask = ''
    while not mask:
            mask = static_ip_netmask
            try:
                '''This step checks if the static IP mask was provided in the correct format'''
                socket.inet_aton(mask)
            except socket.error:
                print 'Wrong format for the IP NETMASK. Please, try again'
                mask = ''

    gateway = ''
    while not gateway:
            gateway = static_ip_gateway
            try:
                '''This step checks if the static IP gateway was provided in the correct format'''
                socket.inet_aton(gateway)
            except socket.error:
                print 'Wrong IP format for gateway. Please, try again'
                gateway = ''


    dns = ''
    option_dns = dns_flag
    if not option_dns or option_dns == 'n':
       dns = ''
    if option_dns == 'y':
       dns = ''
       while not dns:
             dns = dns_server
             try:
                '''This step checks if the static DNS address  was provided in the correct format'''
                socket.inet_aton(dns)
             except socket.error:
                    print 'Wrong format for DNS server. Please, try again'
                    dns = ''

    return ip, mask, gateway, dns


def hlk_generate_port_config(arg_nic_name, dynamic_flag, dns_flag=None, static_ip_address=None, static_ip_netmask=None, static_ip_gateway=None, dns_server=None):

    '''
    This is the function that is called directly from ROBOT as a keyword. It checks what type of
    configuration is needed from the arguments by checking the dynamic_flag argument. If it is set then
    it registers the interface as DHCP enabled. if not then it registers it as a static
    interface with all IP-addresses
    '''
    if dynamic_flag == 'y':
       print 'The specified NICs will be configured for dhcp%s\n\n' %arg_nic_name
       llk_generate_config(arg_nic_name,dynamic_flag)

    if dynamic_flag == 'n':
       address = __get_static_ip_and_mask(static_ip_address, static_ip_gateway, static_ip_netmask, dns_flag, dns_server)
       llk_generate_config(arg_nic_name,arg_ip_address=address)


def llk_generate_config(arg_name,dynamic_flag=None,arg_ip_address=None):

    '''
    This is the main lower layer function that is called from the HLK generate-port_config. It gets all
    the info required to generate the configuration file from the HLK. It then writes the file to disk with the
    required info and restarts the network for the IP addressing to take effect. It then checks the
    interface to see if it got the correct IP or not.
    '''

    print '\nFinished!'
    print 'The below content has been copied in a file named ifcfg-Auto_' + arg_name
    print 'inside the /etc/sysconfig/network-scripts directory.'
    print '\n'+'='*20

    '''This opens the file descriptor (fd) where all the networking config is to be written to'''

    filename = str('/etc/sysconfig/network-scripts/ifcfg-Auto_' + arg_name)
    fd = open(filename, 'w')

    interface_name = str('DEVICE='+arg_name + '\n')
    boot_result = str('ONBOOT=yes' + '\n')
    print 'DEVICE='+arg_name
    print 'ONBOOT=yes'
    fd.write(interface_name)
    fd.write(boot_result)

    if arg_ip_address is not None:
        print 'BOOTPROTO=static'
        boot_proto = str('BOOTPROTO=static' + '\n')
        print 'DEFROUTE=no'
        def_route_option = str('DEFROUTE=no'+ '\n')
        print 'IPADDR='+arg_ip_address[0]
        ip_addr = str('IPADDR='+arg_ip_address[0] + '\n')
        print 'NETMASK='+arg_ip_address[1]
        ip_netmask = str('NETMASK='+arg_ip_address[1] + '\n')
        if arg_ip_address[2]:
            print 'GATEWAY='+arg_ip_address[2]
            ip_gateway = str('GATEWAY='+arg_ip_address[2] + '\n')
        if arg_ip_address[3]:
            print 'DNS1='+arg_ip_address[3]
            ip_dns = str('DNS1='+arg_ip_address[3] + '\n')
            seq_of_params = str(boot_proto + def_route_option + ip_addr + ip_netmask + ip_gateway + ip_dns)
            fd.writelines(seq_of_params)
        else:
             seq_of_params = str(boot_proto + def_route_option + ip_addr + ip_netmask + ip_gateway)
             fd.writelines(seq_of_params)

    if dynamic_flag is not None:
        print 'BOOTPROTO="dhcp"'
        print 'DEFROUTE=no'
        def_route_option = str('DEFROUTE=no' + '\n')
        boot_proto = str('BOOTPROTO="dhcp"' +'\n')
        if dynamic_flag:
            print 'DHCPINTERFACES="'+arg_name+'"'
            dhcp_interfaces = str('DHCPINTERFACES="'+arg_name+'"' + '\n')
            seq_of_params = str(boot_proto +  def_route_option + dhcp_interfaces)
            fd.writelines(seq_of_params)

    if dynamic_flag is None and arg_ip_address is None:
        print 'BOOTPROTO=none'
        boot_proto = str('BOOTPROTO=none' + '\n')
        fd.write(boot_proto)

    fd.close()

    print '='*20+'\n'

    """The below lines are code to make sure that interface is enabled after file has been written"""

    ifconfig_up_msg = 'ifconfig ' + str(arg_name) + ' up'
    os_status = os.popen(ifconfig_up_msg)
    os.popen('service network restart')


    ip_addr_after_config = __get_ip_address(str(arg_name))

    if ip_addr_after_config :
       print "\nOperation completed successfully."
       print "\nThe interface is configured with the provided parameters."
       print "\nHere is the ifconfig output from the machine."

       output = subprocess.Popen(['ifconfig', arg_name], stdout=subprocess.PIPE).communicate()[0]
       print "\n\n %s\n\n" %output

    else:
       print "Something has gone wrong! The interface could not be restarted"

