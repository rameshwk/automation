import os,sys,time,re,telnetlib,multiprocessing
from robot.api import logger
import socket

ROBOTREPO = os.environ.get('ROBOTREPO')
try :
    import pexpect
except ImportError :
    pexpect_path = ROBOTREPO+'/TOOLS/pexpect-2.3_1'
    sys.path.append(pexpect_path)
    import pexpect

class com_tl1(object):

    def __init__(self,ip="127.0.0.1",port="1023",username="SUPERUSER",password="ANS#150",timeout=30,transProtocol="telnet"):
        self.hostIp=ip.encode("ascii")
        self.tl1Port=port.encode("ascii")
        self.tl1User=username.encode("ascii")
        self.tl1Password=password.encode("ascii")
        self.tl1Timeout=int(timeout)
        #self.interface=""
        self.tl1Client=""
        self.session_alive = False
        self.protocol=transProtocol.encode("ascii")
        keywordName = "__init__"
        if self.hostIp == "127.0.0.1" :
            raise AssertionError("%s:%s-> please give correct ip" %  (__name__,keywordName))

        connect_info = {}
        connect_info['hostIp'] = self.hostIp
        connect_info['tl1User'] = self.tl1User
        connect_info['tl1Password'] = self.tl1Password
        connect_info['tl1Timeout'] = self.tl1Timeout
        connect_info['sessionAlive'] = self.session_alive

        if self.protocol == "ssh":
            self.tl1Port = "1022"
            self.tl1Client = tl1SSHClient(port=self.tl1Port,**connect_info)
        elif self.protocol == "telnet":
            self.tl1Port = "1023"
            self.tl1Client = tl1TelnetClient(port=self.tl1Port,**connect_info)
        elif self.protocol == "udp":
            self.tl1Port = "13001"
            self.tl1Client = tl1UDPClient(port=self.tl1Port,**connect_info)
        else:
            raise AssertionError("%s:%s-> %s is not supported, please give correct protocol" % (__name__,keywordName,self.protocol))

    def open_tl1(self):
        res = self.tl1Client.open()
        if res == "pass":
            self.session_alive = self.tl1Client.session_alive
        return res

    def close_tl1(self):
        return self.tl1Client.close()

    def send_command(self,command,timeout=0,reboottime=None):
        res = self.tl1Client.send_command(command,timeout,reboottime)
        self.session_alive = self.tl1Client.session_alive
        return res

    def reboot_connect_back (self,max_wait_time):
        return self.tl1Client.reboot_connect_back(max_wait_time)

    def connect_status_check(self,timeout=3):
        return self.tl1Client.connect_status_check(timeout)


class tl1SSHClient():
    def __init__(self,port="1022",**connection):
        self.port = port
        self.ip = connection['hostIp']
        self.username = connection['tl1User']
        self.password = connection['tl1Password']
        self.timeout = connection['tl1Timeout']
        self.session_alive = connection['sessionAlive']
        self.login_command = "ssh -l "+self.username+" -p "+self.port+" "+self.ip
        self.interface = ""
        self.expectedList = ['.*password:',\
                '.*Are you sure you want to continue connecting.*',\
                '.*Connection Refused!.*',\
                '.*Unable to connect.*',\
                '.*closed by remote host.*',\
                pexpect.EOF,pexpect.TIMEOUT]        

    def _init_tl1_expect(self):
        """
          intialize a TL1 session through SSH 
        """

        logger.info("TL1 CMD >> %s" % self.login_command)
        try :
            self.interface = pexpect.spawn(self.login_command)
        except Exception as inst :
            raise AssertionError("%s-> fail to exec 'pexpect.spawn(%s)', exception: %s" % (__name__,self.login_command, inst))
        try:    
            # pexpect had a bug: 
            # if buffer is empty and the regular expression is used in re.compile
            # search buffer throw exception "internal error in regular expression engine"
            # the solution : using string in re.compile
            expect_value = self.interface.expect(self.expectedList)                         
        except Exception as inst :  
            logger.debug ("%s->pexpect can not deal with '.*' search, exception:%s" % (__name__,inst))
            try:
                expect_value = self.interface.expect(self.expectedList)
            except Exception as inst :                     
                raise AssertionError ("%s-> fail to 'expect passowrd': %s" % (__name__,inst))          

        logger.debug("%s-> get return: %s, %s" % (__name__,str(expect_value),self.interface.after))
        
        if int(expect_value) == 0 :
            self.interface.sendline(self.password) 
            logger.debug("%s-> send: '%s'" % (__name__,self.password))             
        elif int(expect_value) > 1 :
            self.session_alive = False
            self.interface.terminate() # to avoid gui pop up when close()
            self.interface.close()
            return "fail" 
        elif int(expect_value) == 1 :
            self.interface.sendline("yes")
            logger.debug("%s-> send: 'yes'" % (__name__))
            try :
                expect_value = self.interface.expect(
                ['.*password:',\
                pexpect.EOF,pexpect.TIMEOUT])
            except Exception as inst:
                raise AssertionError ("%s-> fail to 'expect passowrd': %s" % (__name__,inst))
                 
            logger.debug("%s-> get return: %s, %s" % (__name__,str(expect_value),self.interface.after))
            
            if int(expect_value) == 0 :
                self.interface.sendline(self.password) 
                logger.debug("%s-> send: '%s'" % (__name__,self.password))
            else :
                self.session_alive = False
                self.interface.terminate()
                self.interface.close()
                return "fail"
            
        try :
            expect_value = self.interface.expect(\
            ['.*HELP MENU.*<DEL>.*input.*(\s)+<',\
            '.*;(\s)+>.*',\
            'Connection Refused!.*',\
            '.*DENY.*',\
            '.*Connection closed by foreign host.*',\
            pexpect.EOF, \
            pexpect.TIMEOUT],\
            timeout=self.timeout)
        except Exception as inst :
            raise AssertionError ("%s-> fail to exec 'expect', exception: %s" % (__name__,inst))
            
        logger.debug("%s-> get return: %s,%s" % (__name__,str(expect_value),self.interface.after))
        if expect_value == 0 or expect_value == 1 :
            self.session_alive = True 
            return "pass"
        else :
            self.session_alive = False
            logger.info("%s-> get return: %s" \
            % (__name__,self.interface.after))
            self.interface.close()
            return "fail"

    def open(self):
        keyword_name = 'open'
        iretry_count = 10
        try :
            result = self._init_tl1_expect()
        except Exception as inst :
            raise AssertionError ("%s:%s-> open tl1 failed, exception:%s" % (__name__,keyword_name,inst))
        if self.session_alive :            
            return "pass"
            
        i = 0
        while (i <= iretry_count) :
            i += 1
            logger.debug("%s-> open tl1 for the %sth time, try again" \
            % (__name__,str(i)))
            self._init_tl1_expect()           
            if self.session_alive :                
                return "pass"
            else:
                time.sleep(10)

    def write(self,cmd):
        self.interface.sendline(cmd)

    def close(self):
        """
            logout the created tl1 session
        """

        if self.session_alive : 
            try :
                self.write('LOGOFF;')
                logger.debug("%s-> send: '%s'" % (__name__,'LOGOFF;')) 
            except Exception as inst :
                raise AssertionError("%s-> failed to logoff tl1 , exception:%s" \
                % (__name__,inst))                
        try :  
            self.session_alive = False
            self.interface.terminate() # to avoid gui pop up
            self.interface.close()
        except Exception as inst :
            raise AssertionError("%s-> failed to close tl1 session, exception:%s" \
            % (__name__,inst))            
         
        return "pass"

    def send_command(self,command,timeout=0,reboottime=None):  
        """ 
            Send the TL1 Command by SSH session 
        """

        keyword_name='send_command'
        if timeout != 0 :
            timeout = int(timeout)
        else:
            timeout = 60

        if not self.session_alive : 
            self.open()
        i = 0
        retval = ''
        while (i < 3) :
            try :
                self.interface.sendline(command+"\n")
                logger.debug("%s:%s-> send command '%s'" % (__name__,keyword_name, command))
                # expect_value = self.interface.expect(['.*M  \d .*;\s+<','.*M  \d .*','^< ',\
                expect_value = self.interface.expect(['.*M  \d .*;','^< ',\
                '.*closed by remote host.*',\
                '.*Status, MIB Locked by Different Manager.*',\
                '.*Connection reset by peer.*',\
                pexpect.EOF,pexpect.TIMEOUT],timeout=timeout)
            except Exception as inst :
                expect_value = 4
                logger.debug("%s:%s-> fail to send '%s', exception: %s" % (__name__,keyword_name,command,inst))
            if expect_value == 0 and reboottime is None:
                # logger.debug("%s-> get return: %s" % (__name__,self.interface.after))
                return self.interface.after
            else :
                logger.debug("%s-> returned error info:\n %s" % (__name__,self.interface.after))
                self.interface.close()
                time.sleep(30)
                self.session_alive = False
                try :                
                    if reboottime is not None:
                        retval = self.reboot_connect_back(int(reboottime))
                        if retval == "pass":
                            return retval
                    else:
                        retval = self.open()
                        if retval == "pass":
                            continue
                except Exception as inst :
                    raise AssertionError ("%s:%s-> failed to open tl1, exception: %s" \
                    % (__name__,keyword_name,inst))                             
            i += 1

    def reboot_connect_back (self,max_wait_time):        
        currentTime = time.mktime(time.localtime())
        endTime = currentTime + float(max_wait_time)
        expect_value = 0
        res = None
        ssh_command = "ssh -l "+self.username+" -p "+self.port+" "+self.ip

        while currentTime <= endTime and expect_value == 0:         
            time.sleep(5)
            currentTime = time.mktime(time.localtime())
            try:
                self.interface = pexpect.spawn(ssh_command)
                self.interface.sendline("\r\n")     
                #expect_value = self.interface.expect(['please try again.',pexpect.EOF,pexpect.TIMEOUT])
                expect_value = self.interface.expect(pexpect.EOF)
                logger.debug("while expect 'break':\n write '\\r\\n', gotten '%s'" % str(self.interface.after)) 
                self.interface.terminate() # to avoid gui pop up when close()
                self.interface.close()
            except Exception as inst:                                                        
                self.interface.terminate() # to avoid gui pop up when close()
                self.interface.close()
                logger.debug ("gotten expected exception for reset\n '%s'" % inst)
                time.sleep(10)
                logger.debug ("to open tl1 session after reboot")
                res = self.open() 
                if res == "pass":
                    break
        if currentTime >= endTime :
            raise AssertionError("SUT didn't reset in %s seconds" % str(max_wait_time) )
        return res

    def connect_status_check(self,timeout=3):
        currentTime = time.mktime(time.localtime())
        expect_value = 0
        endTime = currentTime + timeout
        while currentTime <= endTime and expect_value == 0:
            time.sleep(.1)
            currentTime = time.mktime(time.localtime())
            try:
                self.interface.sendline("\r\n")
                expect_value = self.interface.expect(pexpect.EOF,timeout=3)
                if expect_value != 0:
                    return True
            except Exception as inst:
                logger.debug ("gotten expected exception for connect status check\n '%s'" % inst)
                return True
        else:
             return False


class tl1TelnetClient():
    def __init__(self,port="1023",**connection):
        self.port = port
        self.hostIp = connection['hostIp']
        self.username = connection['tl1User']
        self.password = connection['tl1Password']
        self.timeout = connection['tl1Timeout']
        self.session_alive = connection['sessionAlive']
        self.client = ""

    def _write_to_session(self,message,timeout=None) :
        logger.debug("write:"+message+"\n")
        self.write(message.encode("ascii"))
        if not timeout :
           timeout = 2
        returnMsg = self.client.read_until(".*",timeout)
        logger.debug("gotten return:"+returnMsg)
        return returnMsg

    def _any_item_matched(self,item_list,target_string):
        for item in item_list :
            if item in target_string :
                return True
        else :
            return False

    def _init_tl1_telnet(self):
        ## intialize a TL1 session through Telnet 
        non_recover_error = ["Connection Refused!","Unable to connect to remote host",
        "Internal processing error","This terminal is locked for ",
        "Connection reset by peer"]
        currentTime = time.mktime(time.localtime())
        endTime = currentTime + int(self.timeout)
        try :
            self.client = telnetlib.Telnet(self.hostIp,self.port,6)
            logger.info("create telnet: self client:%s" % self.client )
        except Exception as inst :
            raise AssertionError("gotten exception for telnetlib.telnet : %s" % inst) 
        returnTmp = " "
        try :
            returnTmp = self.client.read_until(".*",5)
            # print "Returned value: %s" % returnTmp
            # self.session_alive = True
        except Exception as inst :
            logger.debug("first read failed for this telnet to %s \n exception:%s" % (self.hostIp,inst))
            if int(self.timeout) < 30:
                time.sleep(int(self.timeout))
            else:
                time.sleep(30)
        logger.debug("get return:%s\n" % returnTmp)
        while currentTime <= endTime:
            currentTime = time.mktime(time.localtime())
            if "<" in returnTmp and "M  \d COMPLD" not in  returnTmp:
                time.sleep(2)
                returnTmp = self._write_to_session("\r\n",3)
                continue
            elif "Would you like a TL1 login(T)" in returnTmp:
                returnTmp = self._write_to_session("T\r\n",3)
                continue
            elif "login:" in returnTmp or "Enter Username" in returnTmp :
                returnTmp = self._write_to_session(self.username+"\r",2)
            elif "Enter Password" in returnTmp :
                returnTmp = self._write_to_session(self.password+"\r",2)
                if "M  \d COMPLD" in returnTmp:
                    time.sleep(2)
                    self.session_alive = True
                    logger.info("Login TL1 with telnet successful!")
                    break
                else:
                    continue
            elif "No command code entered" in returnTmp:
                self.session_alive = True
                break
            elif "Login incorrect" in returnTmp and "Connection failed" in returnTmp :
                logger.debug("get return:%s\n" % returnTmp)
                self.client.close()
                time.sleep(1)
                break
            elif self._any_item_matched(non_recover_error,returnTmp) :
                logger.debug("get return:%s\n" % returnTmp)
                self.client.close()
                raise AssertionError ("fatal to telnet TL1\n get error:%s" % returnTmp)
            else:
                logger.debug("get return:%s\n" % returnTmp)
                logger.info("<span style=\"color: #FFFF00\">gotten unexpected return</span>",html=True)
                self.client.close()
                time.sleep(1)
                break
        logger.info("TL1 REPLY << %s" % returnTmp)
        if self.session_alive:
            return "pass"
        else:
            return "fail"

    def open(self):
        keywordName = "open_tl1"
        iretry_count = 10
        terminal_set = 'SET-NE-ALL::COM::::CMDSSNTMO=0;'
        try :
            result = self._init_tl1_telnet()
        except Exception as inst :
            raise AssertionError ("%s:%s-> open tl1 failed, exception:%s" % (__name__,keywordName,inst))
        if self.session_alive :            
            # - Set the terminal-timeout as '0' to avoid the terminal timeout
            self._write_to_session(terminal_set,2)
            return "pass"
        i = 0
        while (i <= iretry_count) :
            i += 1
            logger.debug("%s-> open tl1 for the %sth time, try again" % (__name__,str(i)))
            self._init_tl1_telnet()           
            if self.session_alive :                
                logger.info(self.session_alive)
                self._write_to_session(terminal_set,2)
                return "pass"
            else:
                time.sleep(10)

    def write(self,cmd):
        self.client.write(cmd) 

    def close(self):
        """
            logout the created tl1 session
        """
        if self.session_alive :
            try :
                self.write('LOGOFF;')
                logger.debug("%s-> send: '%s'" % (__name__,'LOGOFF;'))
            except Exception as inst :
                raise AssertionError("%s-> failed to logoff tl1 , exception:%s" % (__name__,inst))
        try :
            self.client.close()
        except Exception as inst :
            raise AssertionError("%s-> failed to close tl1 session, exception:%s" % (__name__,inst))

        return "pass"

    def send_command(self,command,timeout=0,reboottime=None):  
        """ 
            Send the TL1 Command by TELNET session 
        """

        keyword_name='send_command'
        if timeout != 0 :
            timeout = int(timeout)
        else:
            timeout = 3

        if not self.session_alive : 
            self.open()

        #Cleanup the buffer in try block
        returnTmp = ""
        try:
            returnTmp = self.client.read_until(".*",timeout*1.0/1000) 
        except Exception as inst:
            logger.debug("%s:%s-> need re-open tl1 \n expection: %s" \
            % (__name__,keyword_name,inst))
            self.open()
        logger.debug ("info in buffer before send cli:\n %s" % (returnTmp))

        i = 0
        res = ''
        while (i < 3) :
            try :
                res = self._write_to_session(command,timeout)
                logger.debug("%s:%s-> send command '%s'" % (__name__,keyword_name, command))
                stat = True
            except Exception as inst :
                logger.debug("%s:%s-> fail to send '%s', exception: %s" % (__name__,keyword_name,command,inst))
                stat = False
                res = inst
            unexpect_list = ['.*M  \d .*;\s+<', '^< ', '.*closed by remote host.*',\
            '.*Status, MIB Locked by Different Manager.*', '.*Connection reset by peer.*']
            if res:
                for item in unexpect_list:
                    if item in res:
                        stat = False
            if stat == True and reboottime is None:
                # logger.debug("%s-> get return: %s" % (__name__,res))
                return res
            else:
                self.client.close()
                time.sleep(30)
                self.session_alive = False
                try :        
                    if reboottime is not None:
                        retval = self.reboot_connect_back(int(reboottime))
                        if retval == "pass":
                            return retval
                    else:
                        self.open()  
                except Exception as inst :
                    raise AssertionError ("%s:%s-> failed to open tl1, exception: %s" \
                    % (__name__,keyword_name,inst))                             
            i += 1

    def connect_status_check(self,timeout=10):
        currentTime = time.mktime(time.localtime())
        connection = False
        endTime = currentTime + timeout
        while currentTime <= endTime:
            time.sleep(.5)
            currentTime = time.mktime(time.localtime())
            try:
                res = self.client.read_until(".*",timeout*1.0/5)
                if res != None:
                    connection = True
                    break
            except Exception as inst:
                logger.debug ("gotten expected exception for connect status check\n '%s'" % inst)
                connection = False 
        return connection

    def reboot_connect_back (self,max_wait_time):        
        res = None
        currentTime = time.mktime(time.localtime())
        endTime = currentTime + int(max_wait_time)

        while currentTime <= endTime :         
            time.sleep(5)
            currentTime = time.mktime(time.localtime())
            try:
                res = telnetlib.Telnet(self.hostIp,self.port,float(max_wait_time)*1.0/5)
                break
            except Exception as inst:                                                        
                logger.debug ("Board is rebooting, please wait .... Fail to login for exception: \n '%s'" % inst)

        if res != None:
            res.close()
            time.sleep(3)
            logger.debug ("to open tl1 session after reboot")
            res = self.open()
        if currentTime >= endTime :
            raise AssertionError("SUT didn't reset in %s seconds" % str(max_wait_time) )
        return res


class tl1UDPClient():
    def __init__(self,port="13001",**connection):
        self.port = int(port)
        self.ip = connection['hostIp']
        self.address = (self.ip, int(self.port))
        self.username = connection['tl1User']
        self.password = connection['tl1Password']
        self.timeout = connection['tl1Timeout']
        self.session_alive = connection['sessionAlive']
        self.login_command = 'ACT-USER::'+self.username+':1::'+self.password+';'
        self.interface = ""
        
    def read(self, keyword='.*M  \d .*', buffer_len=4096):
        res = ''
        try:
            output_list = []
            pattern = re.compile(r'%s' % keyword)
            while True:
                buf, address = self.interface.recvfrom(buffer_len)
                output_list.append(buf)
                if buf.endswith(';') and pattern.search(buf):
                   break
            res = ''.join(output_list)
        except Exception as inst:
            logger.debug("%s->Couldn't get expected output: %s , exception:%s" % (__name__,keyword,inst))
        return res

    def _write(self, data):
        print "send str[%s] to %s:%d" % (data, self.address[0], self.address[1])
        self.interface.sendto(data, self.address)

    def _init_tl1_udp(self):
        """
          intialize a TL1 session through UDP 
        """

        logger.info("TL1 CMD >> %s" % self.login_command)
        try :
            self.interface = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            self.interface.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            self.interface.settimeout(int(self.timeout))
            # hostip='', which means the ip address of localhost will be used
            # port=0, which means the available port will be selected automatically.
            self.interface.bind(('', 0))
            self.interface.sendto(self.login_command, self.address)
        except Exception as inst :
            logger.debug("%s-> fail to login with cmd: %s, exception: %s" % (__name__,self.login_command, inst))
        
        res = ''
        try:    
            res = self.read(keyword='', buffer_len=1024)
        except Exception as inst :  
            logger.debug ("%s->pexpect can not deal with '.*' search, exception:%s" % (__name__,inst))        

        
        if 'User(s) Logged On' in res:
            logger.debug("%s-> send: '%s'" % (__name__,self.login_command))             
            self.session_alive = True
            logger.info("%s-> log in TL1 with account %s by UDP" % (__name__,self.username))
            return 'pass'
        elif 'DENY' in res:
            logger.debug("%s-> log in TL1 failed with account %s by UDP" % (__name__,self.username))
            logoutCmd = 'CANC-USER::'+self.username+':1;'
            self._write(logoutCmd)
            logger.debug("%s-> send: '%s'" % (__name__,logoutCmd))
            self.session_alive = False
            self.interface.close()
            return "fail" 
        else:
            self.session_alive = False
            self.interface.close()
            return "fail" 
            
    def open(self):
        keyword_name = 'open'
        iretry_count = 10
        i = 0
        try:
            while (i < iretry_count) :
                i += 1
                logger.debug("%s-> Try to open tl1 for the %sth time." % (__name__,str(i)))
                result = self._init_tl1_udp()
                if self.session_alive :
                    return "pass"
                else:
                    time.sleep(10)
            if result == "fail":
                raise AssertionError("%s:%s-> Try to connect TL1 failed for %d times, please check the connection" % (__name__,keyword_name,iretry_count))
        except Exception as inst :
            raise AssertionError ("%s:%s-> open tl1 failed, exception:%s" % (__name__,keyword_name,inst))

    def close(self):
        """
            logout the created tl1 session
        """
        res = ''
        if self.session_alive : 
            try :
                #logoutCmd = 'CANC-USER::'+self.username+':1;'
                logoutCmd = 'LOGOFF;'
                self._write(logoutCmd)
                logger.debug("%s-> send: '%s'" % (__name__,logoutCmd)) 
            except Exception as inst :
                raise AssertionError("%s-> failed to logoff tl1 , exception:%s" % (__name__,inst))                
        try :  
            res = self.read(keyword='')
            logger.debug("%s-> TL1 REPLY: %s " % (__name__, res))
            if 'COMPLD' in res:
                self.session_alive = False
                self.interface.close()
            else:
                logger.info("%s->Fail to disconnect TL1 session with command: %s." % (__name__,logoutCmd))
                return "fail"
        except Exception as inst :
            raise AssertionError("%s-> failed to close tl1 session, exception:%s" % (__name__,inst))            
        return "pass"

    def send_command(self,command,timeout=0,reboottime=None):  
        """ 
            Send the TL1 Command by UDP session 
        """

        keyword_name='send_command'
        if timeout != 0 :
            timeout = int(timeout)
        else:
            timeout = 3

        if not self.session_alive : 
            self.open()

        i = 0
        while (i < 3) :
            try :
                self._write(command)
                logger.debug("%s:%s-> send command '%s'" % (__name__,keyword_name, command))
                res = self.read(keyword='')
                stat = True
            except Exception as inst :
                logger.debug("%s:%s-> fail to send '%s', exception: %s" % (__name__,keyword_name,command,inst))
                stat = False
            unexpect_list = ['.*M  \d .*;\s+<', '.*closed by remote host.*',\
            '.*Status, MIB Locked by Different Manager.*', '.*Connection reset by peer.*']
            for item in unexpect_list:
                if item in res:
                    stat = False
            if stat == True and reboottime is None:
                return res
            else:
                self.interface.close()
                time.sleep(30)
                self.session_alive = False
                try :
                    self.open()
                except Exception as inst :
                    raise AssertionError ("%s:%s-> failed to open tl1, exception: %s" % (__name__,keyword_name,inst))                             
            i += 1

    def connect_status_check(self,timeout=60):
        return True

    def reboot_connect_back (self,max_wait_time=60):
        return 'pass' 
        

class TL1FatalError (RuntimeError) :
    ROBOT_EXIT_ON_FAILURE = True
