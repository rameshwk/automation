import os
import re
import sys
import time
import com_tl1
from robot.api import logger

TL1_SESSION = {}

def connect_tl1 (ip,port='1023',username="SUPERUSER",password="ANS#150", \
session_name="first_tl1_session",protocol="telnet") :
    """ 
    build up the tl1 session for sending tl1 command
        
    this Keyword is based class com_tl1, it provide \
    a global dict TL1_SESSION to save com_tl1 objects

    - *ip:* ip address of connect tl1 based on ssh protocol. 
    - *port:*  port, default is 22
    - *username:* username of the ssh user
    - *password:* password of the ssh user
    - *session_name:* session name of the connection, default value 'first_tl1_session'
    - *protocol:* translation protocol includes ssh, telnet and udp, default value is 'ssh'
    - *RETURN_VALUE:* SSH_SESSION[session_name] or raise error

    Usage Example:
    | connect_tl1 | 135.251.200.170 | 22 | SUPERUSER | ANS#150 | 
    | connect_tl1 | 135.251.200.170 | 22 | second_tl1_session | protocol |   
    """
    logger.debug("%s -> ip=%s,port=%s,username=%s,password=%s, session_name=%s, protocol=%s" \
    % (__name__,ip,port,username,password,session_name,protocol))
  
    #Globals.ISAM_CLI_Details = entry
    ip = ip.encode("ascii")
    port = port.encode("ascii")
    username = username.encode("ascii")
    password = password.encode("ascii")
    Protocol = protocol.encode("ascii")
    
    try:
        TL1_SESSION[session_name] = com_tl1.com_tl1\
        (ip,port=port,username=username,password=password,transProtocol=Protocol)
    except Exception as inst:
        raise AssertionError("%s-> fail to init tl1,exception: %s" \
        % (__name__,inst)) 

    try:
        TL1_SESSION[session_name].open_tl1()
    except Exception as inst:
        raise AssertionError("%s-> fail to open tl1,exception: %s" \
        % (__name__,inst))        
     
    logger.debug("%s -> tl1 session created: %s of %s " \
    % (__name__,session_name,str(TL1_SESSION)))
    
    return TL1_SESSION[session_name]


def disconnect_tl1 (session_name="first_tl1_session") :
    """
    disconnect tl1 session
    - *session_name:* session name of the connection, default value 'first_tl1_session'
    
    Usage Example:
    | disconnect tl1 |
    | disconnect tl1 | second_tl1_session |

    """
    logger.info("%s -> session_name=%s" \
    % (__name__,session_name))
    try:
        if TL1_SESSION == {} or TL1_SESSION[session_name].session_alive == False or TL1_SESSION[session_name].connect_status_check() == False:
            logger.info("%s -> No TL1 connected before disconnected it. " % __name__)
        else:
            TL1_SESSION[session_name].close_tl1()
            TL1_SESSION.pop(session_name)
    except Exception as inst :
        raise AssertionError("%s -> fail to close tl1 session, exception:" \
        % (__name__,inst))         

    logger.debug("%s -> tl1 session '%s' closed " \
        % (__name__,session_name)) 
           
    return "pass"


def send_tl1_command(command,session_name="first_tl1_session",timeout=0,expect_result="pass"):
    """
    send single tl1 command
    
    - *command:* tl1 command to be executed
    - *session_name:* session name of the connection, default value 'first_tl1_session'
    - *timeout:* waiting time (seconds) before check return prompt, default value is 0
    - *expect_result* expect result, pass or fail.
    - *RETURN_VALUE:* (status,result) or raise error

    Usage Example:
    | send_tl1_command | rtrv-cond-all::all | session_name=second_session | timeout=3 |
    """
    
    if int(timeout) != 0 :
        timeout = int(timeout)
    else:
        timeout = 3
    
    logger.debug("%s -> command='%s',session_name='%s' " \
    % (__name__,command,session_name))
    
    command = command.strip('\n')
    if not re.search(";$", command):
        command = command + ";"
			
    try:
        logger.info("TL1 CMD >> %s" % (command))
        res = TL1_SESSION[session_name].send_command(command,timeout=timeout)
    except Exception as inst:
        raise AssertionError("%s-> fail to send command '%s', exception: %s" \
        % (__name__,command,str(inst)))        
    else :
        logger.info("TL1 REPLY << %s " % (res))
    status = _check_error(res)
    
    if status != expect_result :
        tl1_command_failure_check(command,res,session_name)                   
        raise AssertionError("%s-> failed command '%s'" \
        % (__name__,command))
     
    return "pass",res

'''
Sample Output :
02:47:25.744 	ERROR 	Auto TI Analysis Report Start...
02:47:26.094 	INFO 	PROMPT :0
02:47:26.098 	INFO 	Module:tl1_command, Keyword:send_tl1_command -> TL1 REPLY: rtrv-cond-all::all; IP 0 < <PLEASE-SET-SID> 13-08-05 18:46:55 M 0 COMPLD /* rtrv-cond-all::all */ "SECACCESS-135-249-30-6,SECACCESS:MJ,SECUMGTACCESS,NSA,,,,,, \"Successful Management login\"" "SECACCESS-135-249-30-6,SECACCESS:MJ,SECUMGTACCESS,NSA,,,,,, \"Successful Management login\"" "SECACCESS-135-249-26-74,SECACCESS:MJ,SECUMGTACCESS,NSA,,,,,, \"Successful Management login\"" "SECACCESS-0-0-0-0,SECACCESS:MJ,SECUMGTACCESSTCA,NSA,,,,,, \"Failed Management login\"" "LT-1-1-4,EQPT:MJ,NTRBLOS,SA,,,,,,\"the NTR-B signal is not available\"" "LT-1-1-5,EQPT:MJ,PROGVER,NSA,,,,,, \"Failed to load or find requested software\"" "SHELF-1-1,EQPT:MJ,FANALM-1,NSA,,,,,,\"Fan alarm for fan #1\"" ; <
02:47:26.380 	INFO 	PROMPT :0
02:47:26.384 	INFO 	Module:tl1_command, Keyword:send_tl1_command -> TL1 REPLY: <PLEASE-SET-SID> 13-08-05 18:46:55 ** 132 REPT ALM SECACCESS "SECACCESS-135-249-30-6:MJ,SECUMGTACCESS,NSA,8-5,18-46-55,,: \"Successful Management login\", \"TimeStamp : 05/08/13 18:46:41 , Username : isadmin , Type : CLI\"" ; < rtrv-errorlog:::1::3; IP 1 < <PLEASE-SET-SID> 13-08-05 18:46:55 M 1 COMPLD /* rtrv-errorlog:::1::3 */ "127.0.0.1,.1.3.6.1.4.1.637.61.1.23.3.1.2.4356,,458791, EQPT MGT error 40 : The board is already planned and needs to be unplanned first,, \"13-08-05 12:22:31\"" "127.0.0.1,.1.3.6.1.4.1.637.61.1.23.3.1.2.4356,,458791, EQPT MGT error 40 : The board is already planned and needs to be unplanned first,, \"13-08-05 11:30:35\"" "127.0.0.1,.1.3.6.1.4.1.637.61.1.23.5.3.1.4.4354,,4063239, REDCY MGT error 8 : The protection group is locked,,\"13-08-02 20:15:52\"" ; <
02:47:26.385 	ERROR 	Auto TI Analysis Report End...
'''
def tl1_command_failure_check(command,reply,session_name):

    """
    Print the reason for the tl1_command_failure
    - *command:* failed tl1 command need to be checked
    - *reply:* failed tl1 reply need to be checked.
    - *session_name:* the name of tl1 session
    - *RETURN_VALUE:* NONE
    
    Usage Example:
    | tl1_command_failure_check | ${tl1_command} | ${tl1_error_reply} |  

    """ 

    TL1_ANALYSIS_REPORT = ""
    TL1_ANALYSIS_REPORT = "CONFIGURATION FAILED"
    logger.error("<font color='blue'>#################################################################</font>" ,html=True)
    logger.error("<font color='blue'>######Auto TI Analysis Report Start######...</font>" ,html=True)
    logger.error("<font color='blue'>=================================================================</font>" ,html=True)
    logger.error("<b><font color='blue'>######Auto TI Analysis Failure Type : CONFIGURATION FAILED :#####</font></b>" ,html=True)
    logger.error("<font color='blue'>=================================================================</font>" ,html=True)
    logger.error("<font color='blue'>Failed Command : %s </font>" % (command),html=True)
    logger.error("<font color='blue'>Failed Command output : %s </font>" % (reply),html=True)
    if not command.startswith('rtrv') :
        try :            
            ret = send_tl1_command("rtrv-cond-all::all;",session_name=session_name,timeout=3)
            ret = send_tl1_command("rtrv-errorlog:::1::3;",session_name=session_name,timeout=3)			    
        except Exception as inst:	   
            logger.error("<font color='red'>Debug commands rtrv-cond-all & rtrv-errorlog executions failed... exception:%s </font>" % (inst))            
    logger.error("<font color='blue'>=================================================================</font>" ,html=True)
    logger.error("<b><font color='blue'>#####Auto TI Analysis Report : Type = CONFIGURATION : %s#####</font></b>" % (TL1_ANALYSIS_REPORT),html=True)
    logger.error("<font color='blue'>=================================================================</font>" ,html=True)
    logger.error("<font color='blue'>#####Auto TI Analysis Report End#####...</font>",html=True)
    logger.error("<font color='blue'>#################################################################</font>" ,html=True)


def get_tl1_output (command,timeout=5,session_name="first_tl1_session"):

    """
    get the output of tl1 command
    - *command:* tl1 command need to be executed
    - *timeout:* waiting time (seconds) before check return prompt, default value is 5
    - *session_name:* the name of tl1 session
    - *RETURN_VALUE:* tl1 command output result or raise error
    
    Usage Example:
    | ${eont_info} | get tl1 output | rtrv-eont::ONT-${eont_id} | 

    """

    logger.debug("%s -> command=%s,timeout=%s,session_name= %s" \
    % (__name__,command,str(timeout),session_name))

    command = command.strip('\n')
    if not re.search(";$", command):
        command = command + ";"
    try:
        cliobj = TL1_SESSION[session_name]
        res = cliobj.send_command(command,timeout=timeout)
        logger.info("TL1 CMD >> %s" % command)
    except Exception as inst:
        raise AssertionError("%s-> fail to send command '%s', exception: %s" \
        % (__name__,command,inst))        
    else :
        logger.info("TL1 REPLY << %s " % (res))
        
    status = _check_error(res)
    if status == "fail" :
        raise AssertionError("%s ->failed command: %s" \
        % (__name__,command))

    return _parse_tl1_response(res)

def get_tl1_output_raw (command,timeout=5,session_name="first_tl1_session"):

    """
    get raw output of tli command without error check
    - *command:* tl1 command need to be executed
    - *timeout:* waiting time (seconds) before check return prompt, default value is 5
    - *session_name:* the name of tl1 session
    - *RETURN_VALUE:* tl1 command output result or raise error
    
    Usage Example:
    | ${eont_info} | get tl1 output raw | rtrv-eont::ONT-${eont_id} | 

    """

    logger.debug("%s -> command=%s,timeout=%s,session_name= %s" \
    % (__name__,command,str(timeout),session_name))
    res = ""
    command = command.strip('\n')
    if not re.search(";$", command):
        command = command + ";"
    try:
        tl1obj = TL1_SESSION[session_name]
        res = tl1obj.send_command(command,timeout=timeout)
        logger.info("TL1 CMD >> %s" % command)
    except Exception as inst:
        raise AssertionError("%s-> fail to send command '%s', exception: %s" \
        % (__name__,command,inst))
    else :
        logger.info("TL1 REPLY << %s " % (res))

    return res

def send_tl1_reboot_command (command,timeout=0, session_name="first_tl1_session",\
    reboot_num=3,reboot_time=900):
    """
    send tl1 command which will cause SUT reboot
    
    usage example :
    | send tl1 reboot command | **************** | reboot_time=30000 |
    
    - *command:* the command to be executed
    - *timeout:* the max waiting time if the command did not return normally
    - *session_name:* the name of target tl1 session
    - *reboot_num:*  the max trying number when send reboot command encounter error
    - *reboot_time:* time to keep trying for DUT connection come back after reboot
             
    """

    keyword_name = "send_tl1_reboot_command"  
    logger.debug("%s:%s-> command=%s,session_name=%s,timeout=%s" \
    % (__name__,keyword_name,command,session_name,str(timeout)))
        
    for i in range (int(reboot_num)) :   
        try:
            logger.info("<span style=\"color: #00008B\">"+"TL1 CMD >> %s" % command+"</span>",html=True)
            res = TL1_SESSION[session_name].send_command(command,timeout=timeout,reboottime=reboot_time)
        except AssertionError as inst:
            print AssertionError("failed tl1 '%s'\n exception:%s" \
            % (command,inst))              
        time.sleep (60)
        logger.info("TL1 REPLY << %s " % (res)) 
        
    logger.debug("expect SUT reset by '%s'" % command)   
    return res

def check_tl1_command_regexp (command,*args) :
    """
    to check if specific contents in the command output, the contents should keep in same line
   
    usage example :
    | check tl1 command regexp | rtrv-cond-all::all | 19-5-35 | check_time=300 |
    | ${modem_ip}= | check tl1 command regexp | rtrv-cond-all::all | \\d+-\\d+-\\d+ |    
    
    - *command:*     the retrieve command to be executed
    - * .* :*        regular expression to as search partern in command response
    - *check_time:*   duration time to check output every 5 seconds | 
    - *expect_result:*  "PASS" or "FAIL" 
    - *session_name:*   the name of target tl1 session 
    - *RETURN_VALUE:*  if matched and expect 'PASS', return the match result;
                if not matched and expect 'FAIL', return 'False';
                else,raise error 
                 
    chunyagu developed at Oct 21th 2013
    """
    keyword_name = 'check_tl1_command_regexp'
    logger.debug("command=%s, args=%s" % (command, str(args)))
    input_dict = {}
    input_dict['check_time'] = 0
    input_dict['timeout'] = 0
    input_dict['expect_result'] = "PASS"
    input_dict['session_name'] = "first_tl1_session"
    
    search_list = []
    for elem in args:
        try : 
            key_word,expect_value = elem.split("=") 
        except Exception as inst :
            search_list.append(elem)
        else :
            input_dict[key_word] = expect_value
            
    check_time = int(input_dict['check_time'])        
    if check_time > 5 :
        interval_time = int(check_time/5)
    else :
        interval_time = 1   

    current_time = time.mktime(time.localtime())
    end_time = current_time + check_time

    while current_time <= end_time :
        #try:
        info = get_tl1_output_raw(command,timeout=int(input_dict['timeout']),session_name=input_dict['session_name'])
        #except AssertionError as inst:    
        #    raise AssertionError("failed tl1 '%s'\n exception: %s" % (command, inst)) 
        info = info.replace(command,"")        
        search_result = "PASS"
        found = _all_items_in_one_line (search_list, info)
        logger.debug("found: %s" % (found))
        if not found :
            search_result = "FAIL"
            logger.debug("not gotten '%s' in output" % str(search_list))
            logger.info("expect_result:%s, search_result:%s" \
            % (input_dict['expect_result'], search_result)) 
            
        if search_result == input_dict['expect_result'].upper():
            return found
        else :
            time.sleep(interval_time)
            current_time = time.mktime(time.localtime()) 
            continue
    else :        
        raise AssertionError("'%s' not in tl1 '%s' output" % (search_list,command))   	

def _parse_tl1_response(response) :  

    """
      parse tl1 response 
      
      tl1 response have format like:
      IP 0 < <PLEASE-SET-SID> 70-01-14 01:59:35 
      M 0 COMPLD /* rtrv-eqpt::lt-1-1-7 */ 
      "LT-1-1-7:FPLT-A,FPLT-A:SWOVERRULE=NOOVERRULE,LSMPWR=UP, 
      EQPTPROFNAME=DPOE_LT,,RSTCAUSE=WARM_RESET,RSTTIME=70-01-13,08-05-10,    
      RSTNUM=18,OAMIPADDR=0-0-0-0,PAIRNUMBER=0,DUALHOSTIPADDR=0-0-0-0, 
      DUALHOSTLSMLOC=0-0:IS-NR," 
      ;
    """
    next_is_command = False
    next_check = False
    response_string = ""
    lData = str(response).split('\n')
    for lLine in lData :
        if re.search("M  0 COMPLD",lLine) :
            next_is_command = True
            next_check = True
            continue
	if next_check :
                if  re.search('IP\s\d|PF\s\d|OK\s\d|NA\s\d|NG\s\d|RL\s\d',lLine) :
                    next_is_command = True
                    continue			      
        if next_is_command :
            response_string = response_string + lLine
    if not next_is_command or response_string == "" :
        logger.debug("%s-> wrong response format: %s " \
        % (__name__,response))
                         
    #get command itself
    d = {}
    try:
    	
        m = re.search("/\*(.*)\*/",response)
	if m:
	    if m.group():
                    d['COMMAND'] = m.group(1).strip()
	else:
	   raise AssertionError ("FAILED PARSE TL1 RESPONSE")
    except Exception as inst :
        raise AssertionError ("can not get input command: %s:%s" % (response,inst))

    #get command parameters      
    try:
        m = re.search("\"(.*:)(.*=.*?)(\"\s*;){1}",response_string)
	if m :
	    if m.group():
        	d['INDEX']=m.group(1)
        	command_parameters = m.group(2)
	else:
	   raise AssertionError ("FAILED PARSE TL1 BLOCK VALUES")
    except Exception as inst :
        raise AssertionError ("can not get command index string: %s:%s" \
        % (response_string,inst))    

    # parse command parameters
    try :    
        (left_str, right_str) = command_parameters.rsplit(":",1)      
    except Exception as inst :
        left_str = command_parameters
        right_str = None
        
    if right_str :
        d['ADMIN_OPER'] = right_str
        
    for item in re.findall("([^,]*?)=([^=]*)(,|$)",left_str) :
        key,value,x = item
        key = key.strip("\r").strip("\r").strip()
        d[key] = value

    logger.debug("%s-> parse result: %s "  % (__name__,str(d)))
    return d

def _parse_tl1_ouput(response) :  

    """
      parse tl1 response 
      
      tl1 response have format like:
      IP 0 < <PLEASE-SET-SID> 70-01-14 01:59:35 
      M 0 COMPLD /* rtrv-eqpt::lt-1-1-7 */ 
      "LT-1-1-7:FPLT-A,FPLT-A:SWOVERRULE=NOOVERRULE,LSMPWR=UP, 
      EQPTPROFNAME=DPOE_LT,,RSTCAUSE=WARM_RESET,RSTTIME=70-01-13,08-05-10,    
      RSTNUM=18,OAMIPADDR=0-0-0-0,PAIRNUMBER=0,DUALHOSTIPADDR=0-0-0-0, 
      DUALHOSTLSMLOC=0-0:IS-NR," 
      ;
     #NOW INCLUDED SINGLE ELEMENT PARSING 
     #TO ADD MULTIPLE DATA PARSING AS A SINGLE MODULE 
    """
    result_list = []
    next_is_command = False
    next_check = False
    response_string = ""
    lData = str(response).split('\n')
    for lLine in lData :
        if re.search("M  0 COMPLD",lLine) :
            next_is_command = True
	    next_check = True
            continue
	if next_check :
            if  re.search('IP\s\d|PF\s\d|OK\s\d|NA\s\d|NG\s\d|RL\s\d',lLine) :
                next_is_command = True
		continue			      
        if next_is_command :
            response_string = response_string + lLine
    #logger.info("Next is command : %s" % (next_is_command))  
    if not next_is_command or response_string == "" :
        logger.debug("%s -> wrong response format: %s " \
        % (__name__,response))
                         
    #get command itself
    d = {}
    #logger.info(" RESPONSE STRING %s" %(response_string))
    try:
    	
        m = re.search("/\*(.*)\*/",response_string)
	if m:
	    if m.group():
       		d['COMMAND'] = m.group(1).strip()
	else:
	   raise AssertionError ("FAILED PARSE TL1 RESPONSE")
    except Exception as inst :
        raise AssertionError ("can not get input command: %s:%s" % (response,inst))

    #get command parameters  
    #| \"(.*:.*),(.*)(\"\s*;){1}    
    #try:
   #     m = re.search('\"(.*:)(.*=.*?)(\"\s*;){1} | \"(.*:.*),(.*)(\"\s*;){1}',response_string)
    #except Exception as err :
   #     raise AssertionError ("IN TEST : %s  "%err )
    try:
        m = re.search('\"(.*:)(.*=.*?)(\"\s*;){1}',response_string)
        if m :
            if m.group():
                d['INDEX']=m.group(1)
                command_parameters = m.group(2)
        else:
            m = re.search('\"(.*:.*),(.*)(\"\s*;){1}',response_string)
            if m :
                if m.group():
                    d['INDEX']=m.group(1)
                    command_parameters = m.group(2)
                    print " COMMAND PARAM ",command_parameters
                    port_value = re.search('([A-Z]+)-(.*)',m.group(2))
                    if port_value:
                        if port_value.group():
                            if port_value.group(1):
                                if port_value.group(2):
                                    d[port_value.group(1)] = port_value.group(2)
            else:
                raise AssertionError ("FAILED PARSE TL1 BLOCK VALUES")
    except Exception as inst :
        raise AssertionError ("can not get command index string: %s:%s" \
        % (response_string,inst))    
    print " D  VAL",d
    # parse command parameters
    try :    
        (left_str, right_str) = command_parameters.rsplit(":",1)      
    except Exception as inst :
        left_str = command_parameters
        right_str = None
        
    if right_str :
        d['ADMIN_OPER'] = right_str.rstrip(",").strip("\r").strip()\
   
    for item in re.findall("([^,]*?)=([^=]*)(,|$)",left_str) :
        key,value,x = item
        key = key.strip("\r").strip("\r").strip()
        d[key] = value

    logger.info("%s -> parse result: %s " \
        % (__name__,str(d)))
    result_list.append(d)
    #print "PRINT PARSED RESULT : ",result_list
    return result_list

def parse_multiple_tl1_output(response):

    """
    parse tl1 response to list
    - *response:* the string of tl1 repsonse
    - *RETURN_VALUE:* the list of response  

    Usage Example:
    | parse_multiple_tl1_output | ${str_tl1_response} |

    """
    parsed_list =[] 
    parsed_dict = {} 
    for line in response.split('\n'):
        line =line.strip()
        if line:
            #print "Lines ",line
            pat_val = re.search('\"(.*)\"',line)
            if pat_val:
                if pat_val.group():
                    if pat_val.group(1):
                        split_buffer = pat_val.group(1).split(',')
                        key = split_buffer[0]
                        value = split_buffer[1]
                        #print "KEY ",key
                        #print "VALUE",value
                        parsed_dict[key] = value
    
    parsed_list.append(parsed_dict)
    return parsed_list

def _check_error(res):

    return_result = "pass"
    try :
        if not re.search('M  \d+ COMPLD', res):
            logger.debug("%s-> can not get success prompt: 'M  0 COMPLD'" \
            % (__name__))
            return_result = "fail"
    except Exception as inst:
        raise AssertionError("%s-> search success prompt,gotten exception:%s" \
        % (__name__,inst))

    try:       
        m = re.search('.*M  0 DENY.*|.*\* Input, Data, Not Valid \*.*\
        |.* Error at.*',res)
        if m :
            val = m.group(0).strip()
            return_val = "fail"
            logger.debug("%s->get failure matched: %s" \
            %(__name__,val))  
            return_result = "fail"  
    except  Exception as inst:
        raise AssertionError("%s-> search failure prompt, gotten exception:%s" \
        % (__name__,inst))
	     
    return  return_result
    

def _retrive_parsedtl1_output(*resp):	
	for val in resp:
		print " RET ",val

def _validate_parsedtl1_data(*args):
    """
    Usage Example:
    | validate_parsedtl1_data | aArgs['VALIDATE'] | 
    """
    global parsed_out
    input_dict = _tuple_to_dict(args)
    #print " CONVERTED OUT ",input_dict
    inp_key =  input_dict.keys()
    keyunavail = 0 
    validate_flag = 0
    status = "pass"
    for key_in in inp_key :
        for val in parsed_out:
	    if val.has_key(key_in) :
                if input_dict[key_in] == val[key_in]:
                    logger.info("Expected val : %s : %s >>> Retrieved val : %s : %s <<< is matched" % (key_in,input_dict[key_in],key_in,val[key_in]))
                else:
                    logger.error("Expected val :>>> %s : %s Retrieved val :>>> %s : %s is not matched" % (key_in,input_dict[key_in],key_in,val[key_in]))
                    validate_flag = 1
            else:
                keyunavail =1 
                logger.error("Invalid Key value:%s"%(key_in))
    if keyunavail ==1 :
        status = "fail"
        logger.error("Input argument supplied to validate is incorrect ")
    if validate_flag ==1 :
        status = "fail"
        logger.error("Expected value is not matched with the retrieved val ")
    return status
	
def _update_parsedtl1_data(*args):        
    """
    set data to global variable parsed_out
    -*args:* data need to be updated to parsed_out
   
    Usage Example:
    | update_parsedtl1_data | ${parsed_out} |
 
    """	
    global parsed_out
    #print " UPDATE DATA " ,args
    parsed_out = args
	
def _tuple_to_dict(args):
	import re
	list_options = []
	#print " TUPLE DATA",args
	tempdata = str(args)
	sub_val = re.sub('\[','',tempdata.strip())
	sub_val = re.sub('\]','',sub_val)
	sub_val = re.sub('\)','',sub_val)
	converted_val  = re.sub('\(','',sub_val)
	#print " CONVERTED DATA",converted_val
	for item in converted_val.split(',') :
            try :
	    	item = item.strip()
	    	matched = re.search("\s*u'(.*)'", item)
		if matched:
			if matched.group():
                		item = matched.group(1)
            except Exception as inst :
               	pass
            finally :
	    	if item:    
               		list_options.append(item)    
	dict_output = _list_to_dict(list_options)
	#print " List output",list_options
	#print " DICT OUT ",dict_output
    	return dict_output 
    	
def _list_to_dict(args):
    """
        convert a list like ["'a'='aaa'", "'b'='bbb'"] to a dictionary
    """
    x = []
    y = []
    for arg in args:
        arg = arg.encode("ascii")
        arg_list = arg.split('=',1) # first "=" is the seperation sign       
        key=arg_list[0].strip().strip('\'').strip('"')
        try:
            val=arg_list[1].strip('\'').strip('"').strip()
        except IndexError:
            val = "NONE"
        x.append(key)
        y.append(val)

    outdict = dict(zip(x, y)) 
    return outdict        	

def _all_items_in_one_line (item_list, lines) :
    
    line_list = lines.split('\n') 

    for line in line_list[1:] :
        found = True 
        for item in item_list :
            res = re.search(item,line,re.DOTALL)
            if not res :
                found = False
                break
        if found :
            break           
    if found :
        matched = res.groups()       
        if   len(matched) == 0 :
            matched = res.group(0)
        elif len(matched) == 1 :
            matched = matched[0]
        else :
            matched = list(matched)
    else :
        matched = False
    
    return matched
