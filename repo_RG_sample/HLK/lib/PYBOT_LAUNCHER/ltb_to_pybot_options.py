#!/usr/bin/env python

"""
   launched from LTB, it reads the LTB command and writes it in a suitable format \
   for triggering the pybot run.

   INPUT FROMAT: ltb_to_pybot_options.py <filePath> <ltbCommand>
     filePath -- the directory where the formated robot_variable.txt was created
     ltbCommand -- the full command with arguments from apme LaunchTestBatch
     
   Example:
     ltb_to_pybot_options.py /tftpboot/atx/atxuser/SB_Logs_06152016-122629/ROBOT \
     "/tftpboot/atx/atxuser/.launchTestBatchATX.06152016-122629 -v -k \
     -D /tftpboot/atx/atxuser/SB_Logs_06152016-122629
     -f -r CFASB -N SRNTD -T smoke -R 5.5 -G 135.249.25.175 -d ROBOT:mef,ossi,vmgmt \
     -V NC_DPU:NC_DPU_SETUP_01.csv -P PCTA:135.249.26.175 -a --framework ROBOT -e MERC"
      
   pybot is the first interface of automation test based on robot framework
   the created robot_variable.txt looks like:
   
     SHELF = CFASB
     NT = SRNTD
     ISAMIP = 135.249.25.175
     
     RELEASE = 5.5     
     PRODUCT = NC_DPU
     COVERAGE = smoke    
     SETUPFILENAME = NC_DPU_SETUP_01.csv

"""

################################
# functions
################################
def config_qemu_ip (setupFilename,directory,qemu_ip):
    setupfile = ''
    cp_cmd="cp $ROBOTREPO/SETUPS/" +setupFilename + " " + directory

    if not os.system(cp_cmd) :
        setupfile = directory + "/" + setupFilename
        try :
            lines = open(setupfile,'r').readlines()
            flen = len(lines) - 1
            for i in range(flen) :
                if "HOST_IP" in lines[i] :
                    lines[i] = lines[i].replace("HOST_IP",qemu_ip)
            file_object = open(setupfile,'w')
            file_object.writelines(lines)
            file_object.close( )
            # options = options.replace(setupFilename,setupfile)
        except Exception as inst:
            print ("reset HOST ip FAIL,get:%s" % inst)
            sys.exit(1)

    return setupfile

################################
# functions
################################
def  map_domain_to_tag_suite(domain_string):
    """
    map domain to --include, --exclude (tag), --suite, --prerunmodifier, --listener, --rerunfailed and --variable
    """
    include_string = "--include = "
    new_domain_string = ""
    domain_string = domain_string.split(":",1)[-1].strip()
    domain_list = domain_string.split(",")
    for item in domain_list:
        m = re.search("(\w+)\-(\S+)",item)
        if m:
            key=m.group(1)
            value=m.group(2)
            if key.lower() not in ["argumentfile","variable","listener","prerunmodifier","test","exclude","suite","debugfile","rerunfailed"]:
                print "  is it a wrong robot parameter? '--"+key+"'"
            if re.search ("--"+key,new_domain_string):
                new_domain_string = re.sub("--"+key+" = ", "--"+key+" = "+value+",", new_domain_string)
            else:
                new_domain_string = new_domain_string + "--" + key + " = " + value +"\n"
            #matchType = re.search("LOOPTYPE:\s?([^\,]+)",item)
        else:
            item=item.lower()
            include_string = include_string + item +","
            
    if "," in include_string:
        new_domain_string = new_domain_string + include_string.strip(",")
    return new_domain_string
    
################################

ltbPybotArgumentMap = {
  "T" : "COVERAGE",
  "b" : "BUILD",
  "r" : "SHELF",
  "N" : "NT",
  "G" : "ISAMIP",
  "V" : "PRODUCT",
  "R" : "RELEASE",
  "d" : "DOMAIN"
} 
  
#./ltb_to_pybot_options.py /home/auto/ROBOT_LOG "/tftpboot/atx/atxuser/.launchTestBatchATX.06012015-152031 -v -k -D /tftpboot/atx/atxuser/SB_Logs_06012015-152031 -f -r NFXSE -N FANTF -T daily -R 5.3 -u sivasubs -b 53.880 -A legacy -G 135.249.25.22 -x -V EPON_DPOE:/tftpboot/atx/configs/FX8_EPON_DPOE_Daily_04.txt --ontbuild SIP -P PCTA:135.249.26.22 -e MERC -d ROBOT:mef,iphsd,exclude-stablility,suite-TRAF"

import os,sys,re

dirOrFile = sys.argv[1]
# /home/auto/ROBOT_LOG
if os.path.isdir ( dirOrFile ) :
    directory = dirOrFile
    fileName = directory +"/robot_variable.txt"
if os.path.isfile(dirOrFile) :
    fileName = dirOrFile
    directory = dirOrFile.rsplit("/",1)[0]
setupFilename = "" 
sLTBCmd  = sys.argv[2]
# /tftpboot/atx/atxuser/.launchTestBatchATX.06012015-152031 -v -k -D /tftpboot/atx/atxuser/SB_Logs_06012015-152031 -f -r NFXSE -N FANTF -T daily -R 5.3 -u sivasubs -b 53.880 -A legacy -G 135.249.25.22 -x -V EPON_DPOE:/tftpboot/atx/configs/FX8_EPON_DPOE_Daily_04.txt --ontbuild SIP -P PCTA:135.249.26.22 -e MERC -d ROBOT:mef,iphsd,exclude-stability

basicCommand = re.search("(.*?)\-",sLTBCmd).group(1)
# /tftpboot/atx/atxuser/.launchTestBatchATX.06012015-152031 

commandOptions = re.sub(basicCommand,"",sLTBCmd)
commandOptions = re.sub("--","-",commandOptions)

optionsList = commandOptions.lstrip('-').split(" -")

# write info to file according to dictionary ltbPybotArgumentMap
options = ""
qemu_ip = ""
newSetupFile  = ''
setupFilename = ''
batchType     = ''

for specOption in optionsList:
    try:       
        specOption = specOption.strip()
        specKey, specVal = specOption.split(" ")
    except:
        pass
    else:
        if specKey == "V": 
            # only get "EPON_DPOE" field for specific "-V EPON_DPOE:/repo/chunyagu/EPON_DPOE_FX16.txt"
            if specVal.find('+') > -1:
                l = specVal.split('+')[1].split(":")
            else:
                l = specVal.split(":")
            specVal = l[0]
            dut = specVal
            try :
                options = options + "SETUPFILENAME = " + l[1] + "\n"
                setupFilename = l[1]
            except :
                pass
        if specKey == "d": 
            if specVal.find('+') > -1:
                specVal = specVal.split('+')[1]
        if specKey == "G":
            qemu_ip = specVal
        if ltbPybotArgumentMap.has_key(specKey):
            if specKey == 'T' : 
                batchType = specVal
            options = options + ltbPybotArgumentMap[specKey] + " = " + specVal +"\n"

# modify the HOST_IP in SETUP csv file
if batchType.lower() == 'host' :               
    newSetupFile = config_qemu_ip (setupFilename,directory,qemu_ip)
    if newSetupFile : 
        options = options.replace(setupFilename,newSetupFile)
                  
# map DOMAIN to --include/--exclude/--suite
m=re.search("(DOMAIN = ROBOT:([^ ]+))\n",options)
if m :
    domain_str = m.group(1)
    domain_str_new = map_domain_to_tag_suite (domain_str)
    options=re.sub(domain_str,domain_str_new,options)

# create robot_varliable.txt     
with open(fileName,'w') as fileHandler:
    fileHandler.writelines(options)
    fileHandler.writelines('# LTB = %s\n' % sys.argv[2])
