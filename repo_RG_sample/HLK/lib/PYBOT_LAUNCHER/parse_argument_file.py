"""
    parse_argument_file 
    To parse argument file and  provide dictionary output
    To get the setup file based on SETUPFILENAME or dynamically findout it
    To get super suite path 
    To get the Pybot options
    Author:  
    Beno K Immanuel 	Developed
    Chunyan 	Modified for command options 
"""

import os,inspect,re,sys,commands,urllib2
try :
    import data_translation
except Exception as inst:
    sys.path.append(os.environ ['ROBOTREPO']+'/LIBS/DATA_TRANSLATION')
    import data_translation

    
def parse_argument_file(fileName,logDir=None):
    """
    parse info in argument file to a dictionary 

    """
    argumentDict = {}   
    try :
        argumentDict = __get_info_from_argument_file (fileName)
    except Exception as inst:
        raise AssertionError("%s-> fail to get info from file %s, exception: %s" \
            % (__name__,fileName,inst))
     
    try:   
        setupFile = get_setup_file(argumentDict)  
    except Exception as inst:
        raise AssertionError ( str(inst) )     
    print "  setup file: ",setupFile
    os.environ['SETUPFILENAME'] = setupFile
    argumentDict['SETUPFILENAME'] = setupFile
   
    
    if argumentDict.has_key ('PRODUCT') :
        anotherArgFile = os.environ['ROBOTREPO'] + "/SW_DESCRIPTION/" + \
        argumentDict['PRODUCT']
        try:
            sys.path.append(anotherArgFile)
            import pybot_options_for_test
            anotherDict = pybot_options_for_test.get_pybot_options_for_test (setupFile,argumentDict)
        except Exception as inst :
            print "  no info gotten from file under '%s': "  % anotherArgFile
            print "    "+ str(inst)
            pass
        else :
            for thisKey in anotherDict : 
                if argumentDict.has_key (thisKey) :
                    argumentDict[thisKey] = argumentDict[thisKey] + "," + anotherDict[thisKey]
                else :
                    argumentDict[thisKey] = anotherDict[thisKey]
   
    if logDir :
        argumentDict['--outputdir'] = logDir

    # based on release info in input file, create release relative tags
    try :
        excludeOption = create_pybot_exclude_option_by_release (argumentDict["RELEASE"])
    except Exception as inst :
        raise AssertionError("%s-> fail to create exclude option by release,exception: %s" \
        % (__name__,inst))       
    robot_version = _get_robot_version()
    if robot_version < "2.9" :
        try :
            argumentDict["--exclude"] = argumentDict["--exclude"] + "," + excludeOption
        except KeyError:
            argumentDict["--exclude"] = excludeOption

    return argumentDict

def __get_info_from_argument_file(argFile) :
 
    if not os.path.isfile(argFile):
        raise AssertionError("%s -> '%s' is not a file" \
        % (__name__,argFile))

    try:
        with open(argFile,'r') as fileHandler:
            data = fileHandler.readlines()        
    except Exception as inst:
            raise AssertionError("%s-> fail to read file %s,exception: %s" \
            % (__name__,argFile,inst))
    outDict = {}
    # options in input file
    for line in data:
        if re.search("^\s*#",line) : # skip comments
            continue
        try:
            key, val = line.split("=")
        except Exception :
            pass  # skip lines without "="
        else:
            # parse elements before and after "=" to a dictionary
            key = key.strip()
            val = re.sub("\"","",val)
            val = val.strip('\n').strip()
            if outDict.has_key(key) :
                outDict[key] = outDict[key] + "," + val
            else :
                outDict[key] = val
    return outDict


def get_setup_file (inputDict) :
    """
    get setup file name from input Dictionary or dynamically create its name
    
    if inputDict has key 'SETUPFILENAME', it is the first priority for target setup, 
    else dynamicly create setup file name and check its existing in specific directory:
      it should exist in /ROBOT_MOUNT or $ROBOTREPO/SETUPS .

    """
    setupFile = ""

    # get setup file name from input dictionary 
    filePathP0 = ''   
    filePathP1 = os.environ['ROBOTREPO'].rstrip('/') + os.path.join("/SETUPS/")
    filePathP2 = '/ROBOT_MOUNT/SETUPS/'
    if inputDict.has_key('SETUPFILENAME'):
        fileName = inputDict['SETUPFILENAME']
        if (fileName.endswith('.csv') or fileName.endswith('.yaml')):
            setupAvailable = True
            if os.path.isfile ( filePathP0+fileName ) :
                setupFile = filePathP0 + fileName
            elif os.path.isfile ( filePathP1 + fileName ):
                setupFile = filePathP1 + fileName
            elif os.path.isfile ( filePathP2 + fileName ):
                setupFile = filePathP2 + fileName
            else :
                print ("  could not find setup file '%s' in both '%s' and '%s'" \
                % (fileName,filePathP1,filePathP2))
                setupAvailable = False
            if setupAvailable :
                return setupFile

    # get setup file name dynamically 
    print ("  try to create setup file name dynamically...")
    for element in ['PRODUCT','SHELF','NT','COVERAGE','ISAMIP']:
        if not inputDict.has_key(element):
            msg = "input argument have no key: " + element
            raise AssertionError("%s-> %s" % (__name__,msg ))

    ipVal = inputDict['ISAMIP'].split(".")
    if  len(ipVal) != 4:
        msg = "ip address of isam is error: " + inputDict['ISAMIP']
        raise AssertionError("%s-> %s" % (__name__,msg ))
 
    try :
        fileName = inputDict['PRODUCT'] + "_" + inputDict['SHELF'] + "_" + \
        inputDict['NT']+ "_"+ inputDict['COVERAGE'].upper() + "_SETUPFILE_" + \
        ipVal[2] + "_" + ipVal[3] + ".csv"
    except Exception as inst :
        raise AssertionError("%s-> fail to create setup file name dynamically:%s" \
        % (__name__,inst ))
                  
    if os.path.isfile ( filePathP1+fileName ) :
        setupFile = filePathP1 + fileName
    elif os.path.isfile ( filePathP2 + fileName ):
        setupFile = filePathP2 + fileName
    else:
        print ("  --could not find setup file '%s' in both '%s' and '%s'" \
        % (fileName,filePathP1,filePathP2))
        raise AssertionError("could not find setup file '%s' in both '%s' and '%s'" \
        % (fileName,filePathP1,filePathP2))

    return setupFile


def get_super_suite_path (inputDict):
    """
    use "PRODUCT" info in input dictionary
    can extend to use "RELEASE info in input dictionary
    """

    suitePath = ""        
    # get suitepath from input dictionary
    try:
        if inputDict.has_key('SUITEPATH') and os.path.isdir(inputDict['SUITEPATH']):
            suitePath = inputDict['SUITEPATH']
            return suitePath
    except Exception as inst:
        raise AssertionError("%s:%s-> \
            fail to get suite path from input dictionary: %s" \
        % (__name__,inspect.stack()[0][3],inst))

    robotBaseDir = os.environ['ROBOTREPO']
    
    suitePath = robotBaseDir+ "/ATS/" + inputDict['PRODUCT']

    return suitePath
    
 
def create_pybot_exclude_option_by_release (release):
    """
    create release related tag for exclude in pybot command
    
    history:
    chunyagu  created  Jan 20th 2014
    """

    release_list = release.lstrip("R").lstrip('r').split(".")

    if len(release_list) != 2 and len(release_list) != 3 :
        inst = "Error release format:" + str(release)+ \
               ", it should looks like : R4.6.01 or R5.1"
        raise AssertionError("%s:%s -> %s" \
        % (__name__,inspect.stack()[0][3],inst))
    if len(release_list) == 2 :
        release_list.append("00")

    exclude_release = []

    for i in range(10) :
        if i > int(release_list[2]) and i < 4 :
            exclude_release.append ("min_rel_r"+release_list[0]+"."+release_list[1]+".0"+str(i))
        if i > int(release_list[1]):
            exclude_release.append ("min_rel_r"+release_list[0]+"."+str(i)+"*") 
        if i > int(release_list[0]) and i < 7 :
            exclude_release.append ("min_rel_r"+str(i)+"."+"*") 
    exclude_release.append("min_rel_r9.9")
    
    for i in range(10) :
        if 0 < i < int(release_list[2]) :
            exclude_release.append ("max_rel_r"+release_list[0]+"."+release_list[1]+".0"+str(i))
        if i < int(release_list[1]) :
            exclude_release.append ("max_rel_r"+release_list[0]+"."+str(i)+"*")
        if i < int(release_list[0]) and i > 1:
            exclude_release.append ("max_rel_r"+str(i)+"."+"*")
    if  release_list[2] != "00" :
         exclude_release.append ("max_rel_r"+release_list[0]+"."+release_list[1])
        
    return ",".join(exclude_release)

def get_table_from_setup_file  (setupFile,tableName=None,objectName="LT") :
    data_translation.create_global_tables(setupFile)
    if not tableName :
        tableName = objectName+"Data"
    return data_translation.get_global_table(tableName)
 
def __get_loop_info (argumentDict) :
    """
        check "--variable = LOOPTYPE: (.*)" in the argumentFile (robot_variable.txt),
        if it exist, test run will loop on every looped object defined in argumentFile
        
        if "--variable = LOOPTYPE: (.*)" isn't existing, 
        test run will go with the basic pybot command
    """
    if not argumentDict.has_key('--variable') :
        return (None,None)
      
    matchType = re.search("LOOPTYPE:\s?([^\,]+)",argumentDict['--variable'])
    if matchType :
        loopType = matchType.group(1).strip()
    else :
        return (None,None) 

    matchList = re.search("LOOPLIST:\s??([^\,]+)",argumentDict['--variable'])    
    if matchList:
        return (loopType,matchList.group(1).strip())
    else :
        return (loopType,None)

def _get_robot_version():
    robot_version = "3.0"
    try :
        output = commands.getoutput("pybot --version")
        obj = re.search ("\s(\d\.\d+)\.?",output)
        robot_version = obj.groups()[0]
    except Exception as inst :
        print "  failed to get pybot verson, take 3.0 as default"
    return robot_version

def create_suspend_options (logDir,product,release,setupFile,board=None,hostOrTarget="TARGET") :
    suspendOption = ""
    hostOrTarget = hostOrTarget.upper()
    if hostOrTarget != "HOST" :
        hostOrTarget = "TARGET"

    try :
        shelf = get_shelf_from_setup (setupFile,logDir+"/robot_variable.txt")
    except Exception as inst :
        print "  failed to get shelf info from '%s'"  % setupFile
        shelf = None

    try :
        suspendFile = product+"_ATC_SUSPEND.txt"
        if not os.path.isfile(logDir+"/"+suspendFile):
            suspendFile = _load_suspend_file(logDir, product)
    except Exception as inst :
        print "  failed to download suspend file" 
        pass
    try :        
        suspendTestList,frList = _get_suspend_test_list (logDir,product,suspendFile,RELEASE="R"+release,LT=board,PLATFORM=hostOrTarget,SHELF=shelf,referTestFile=logDir+"/TEST_CASE.txt")
    except Exception as inst :
        print "  failed to get suspend test list,exception: %s" % str(inst)
        return suspendOption

    if len(suspendTestList) > 0 :
        suspendOption = " --prerunmodifier "+os.environ['ROBOTREPO']+"/LIBS/PRE_RUN_MODIFIER/exclude_tests.py"
        for test in suspendTestList :
            suspendOption = suspendOption + ":" + test
    return suspendOption


def _load_suspend_file (logDir, productName, url=None) :
    #copy txt file under logDir
    suspendFile = productName+"_ATC_SUSPEND.txt"

    if not url :
        url = "http://aww.sh.bel.alcatel.be/tools/Logs/ETSI/ATCSuspended.txt"
    try :
        webPage = urllib2.urlopen(url, timeout=10)
    except Exception as inst : 
        print "  could not get source file in '%s',exception: %s" % (url,inst)	        
	return  suspendFile
    txt_content = webPage.read()    
    f = open(logDir+"/"+suspendFile,'w')     
    f.write(txt_content)
    f.flush()
    f.close
    return suspendFile

def _get_suspend_test_list (logDir,product,suspendFile,**params) :
    """
    parse suspend file according to input params to get atc list
    
    chunyagu  created  Dec 16th,2014
    """
    if not os.path.isfile(logDir+"/"+suspendFile):
        return ([],[])
    atcData = None
    if params.has_key('referTestFile') and os.path.isfile(params['referTestFile']) :
        with open(params['referTestFile']) as fileHandler:
            atcData = fileHandler.readlines()
    for i in range(100) :
        head=commands.getoutput("head -n %s %s" % (i,logDir+"/"+suspendFile))
        if len(head.strip()) > 0 :
            break
    """
    # head format
    ATC RELEASE FRNUMBER LT PLATFORM SHELFdef get_pybot_loop_options (setupFile,logDir,argDict) :
    """
 
    matchString = ""
    for item in head.split() :
        item = item.strip()
        if item == "ATC" or item == "FRNUMBER" :
            matchString = matchString + "([^\s]+)" + "\s+"
        elif item == "RELEASE" :
            matchString = matchString + params[item].replace(".","\.") + "\s+"  
        elif params.has_key(item) and params[item]:
            matchString = matchString + "(([^\s]*" + params[item] + "[^\s]*)|X)\s+"   
        else :
            matchString = matchString + "[^\s]+" + "\s+"
   
    matchString = matchString.rstrip ("\s+")
    try:
        with open(logDir+"/"+suspendFile,'r') as fileHandler:
            data = fileHandler.readlines()
    except Exception as inst :  
        raise AssertionError ("fail to open file '%s', \nexception:'%s' " \
        % (suspendFile,inst))

    testList = []
    frList = []    
    filteredSuspendFile =logDir+"/"+product+"_ATC_SUSPEND_FILTERED.txt"
    f = open(filteredSuspendFile,'w')

    for line in data :
        m = re.search(matchString,line)
        if m :
            if atcData and m.group(1)+"\n" in atcData :
                testList.append(m.group(1))
                frList.append(m.group(2))
                f.write("RELEASE=%s,LT=%s,ATC=%s,FR=%s,SHELF=%s\n" \
                % ( params['RELEASE'],params.setdefault('LT','X'),m.group(1),m.group(2),params.setdefault('SHELF','X')))

    f.flush()
    f.close         
    return (testList,frList)             
    
           
def get_pybot_loop_options (setupFile,logDir,argDict) :
    """
       for every object under test in setupFile, \
       check the SuiteLoopInfo table to find out the suites and run them.
       these suite name and the object name will go as options of pybot command
       
       The object which exist in setup description file \
       but isn't defined in SuiteLoopInfo will be skiped instead of test
    """

    productName = argDict['PRODUCT']
    releaseNo =  argDict['RELEASE']
    if argDict.has_key ('COVERAGE') and argDict['COVERAGE'].upper == "HOST" :
        hostOrTarget = "HOST"
    else :  
        hostOrTarget = "TARGET"        
    
    optionsList = []   
    (loopType,loopObjects) = __get_loop_info (argDict)
    if not loopType :
        return optionsList
        
    if loopObjects :
        loopObjList = loopObjects.split(" ") 

    setupTable = get_table_from_setup_file (setupFile,tableName=loopType+"Data")

    for thisSetupLine in setupTable :
        for item in [loopType+"Type",loopType+"_type",loopType.lower()+"_type" ] :
            if thisSetupLine.has_key (item) :
                objectName = thisSetupLine[item]
                break
       
        if loopObjects and objectName not in loopObjList :
            continue
        try :
            preRunOption = create_suspend_options (logDir,productName,releaseNo,setupFile,board=objectName,hostOrTarget=hostOrTarget)
        except Exception as inst :
            print "  fail to get suspend options,exception: %s" % str(inst)

        loopVarOption = ' --variable ' + loopType.upper() + ':' + objectName
        logOption = ' --log ' + objectName + "_log.html"
        reportOption = ' --report ' + objectName + "_report.html"
        outputOption = ' --output ' + objectName + "_output.xml"    
        debugOption  = ' --debugfile ' + logDir + "/" +  objectName + "_debug_record.txt" 
                
        try:
            optionFileForThisProduct = os.environ['ROBOTREPO'] + "/SW_DESCRIPTION/" + productName
            sys.path.append(optionFileForThisProduct)
            import pybot_options_for_test
        except Exception as inst : 
            print "  failed to import  'pybot_options_for_test' under '%s'" % optionFileForThisProduct

        try:                 
            optionFromLoopDescTable = pybot_options_for_test.get_pybot_loop_options(objectName,productName)
        except Exception as inst :
            print "  no loop info gotten from file under '%s': "  % optionFileForThisProduct
            print "    "+ str(inst)
            pass        
        
        options = loopVarOption + preRunOption + logOption + \
        reportOption + outputOption + debugOption + optionFromLoopDescTable                                                             

        optionsList.append(options)

    return optionsList

def generate_test_list (testFile,execDir) :  
    testCaseStart = False
    keywordStart = False
    try :
       argumentFileHandler = open(testFile,'w')
    except Exception as inst :
        raise AssertionError ("fail to open file '%s' for write, \nexception:'%s' " \
        % (testFile,inst))
    for pathName, pathList, fileList in os.walk(execDir):
        pathList.sort()
        fileList.sort()       
        for thisFile in fileList :
            if not re.search('(.*)\.(txt|robot)',thisFile) :
                continue
            with open(pathName+'/'+thisFile,'r') as fileHandler:
                data = fileHandler.readlines()
            for line in data : 
                if "Suite Setup  " in line :
                    m = re.search ("Suite Setup\s+(.*)",line) 
                    thisSuiteSetup = "setup:"+m.group(1)
                    argumentFileHandler.writelines (thisSuiteSetup + "\n")
                    continue
                elif "Suite Teardown  " in line:
                    m = re.search ("Suite Teardown\s+(.*)",line) 
                    thisSuiteTeardown = "teardown:"+m.group(1)
                    argumentFileHandler.writelines (thisSuiteTeardown + "\n")
                    continue
                elif re.search ("\*\*\* Test Case(s?) \*\*\*",line) :
                    testCaseStart = True
                    continue
                elif re.search ("\*\*\* Keywords \*\*\*",line) :
                    keywordStart = True                  
                    continue
                else :
                    matchTest = re.search ("^([^\s\|]+.*)",line) or re.search ("^\| {1}([^\|]+) {1}\|$",line)
                    if matchTest and testCaseStart and not keywordStart:
                        thisTest = matchTest.group(1) 
                        argumentFileHandler.writelines (thisTest + "\n")
                    continue

            keywordStart = False
            testCaseStart = False

    argumentFileHandler.close()
    return
 
           
def get_shelf_from_setup (setupFile,outFile=None) :
    """
       get shelf type from setup file for suspend ATC check
    """
    shelfSig = "Shelf"
    setupTable = get_table_from_setup_file (setupFile,tableName=shelfSig+"Data")
    objectName = None
    for thisSetupLine in setupTable :
        for item in ["Type","type","shelf_type","Shelf_type",shelfSig+"Type"] :
            if thisSetupLine.has_key (item) :
                objectName = thisSetupLine[item]
                break
    if not objectName :
        print "  could not get shelf type in setup file"
        return None
        
    shelf = re.sub("\-","",objectName).upper()
    
    # write shelf to robot_variable file if it not exists there
    if outFile :
        if not os.path.isfile(outFile):
            print str(outFile) + "is not a file as expected"
        else :
            with open(outFile,'rw+') as fileHandler:
                data = fileHandler.read()
                if not re.search ("SHELF ?\=",data) :
                    fileHandler.writelines ("SHELF = "+shelf+"\n")                         
    return shelf

def get_capability_options(Option,argumentDict,capabfile='capability.yaml'):
    try:
        capabOption = ''
        if argumentDict.has_key('PRODUCT'):
            capabArgFile = os.path.join(os.environ['ROBOTREPO'], "SW_DESCRIPTION", argumentDict['PRODUCT'], capabfile)
            if os.path.exists(capabArgFile):
                argument_lt = ""
                argument_onu = ""
                argument_nt = ""
                argument_shelf = ""
                arguments_list = []
                if Option.find('--variable LT:') != -1:
                    match = '--variable LT:(.*) --[^>]'
                    lt_type = re.search(match, Option).group(1).split(' ')[0].upper()
                    argument_lt = 'LT_'+lt_type
                if Option.find('--variable ONU:') != -1:
                    match = '--variable ONU:(.*) --[^>]'
                    onu_type = re.search(match, Option).group(1).split(' ')[0].upper()
                    argument_onu = 'ONU_'+onu_type
                if argumentDict.has_key('NT'):
                    argument_nt = 'NT_'+argumentDict['NT']
                if argumentDict.has_key('SHELF'):
                    argument_shelf = 'SHELF_'+argumentDict['SHELF']
                for m in [argument_lt,argument_onu,argument_nt,argument_shelf]:
                    if m != "":
                        arguments_list.append(m)
                argument_duts = ','.join(arguments_list)
                arguments=argument_duts+":"+argumentDict['RELEASE']+":"+capabArgFile
                capabOption = " --prerunmodifier capability_driven:"+arguments
        return capabOption
    except Exception as inst:
        raise  AssertionError ("Fail to create capability driven option for  exception: %s" % inst)
