#!/usr/bin/env python
""" 
    To Invoke the Robot Test Suite through pybot options 
    
    INPUT FROMAT: ./pybot_launcher.py \
    --argumentFile $HOME/robot_variable.txt \
    --logDirectory  $HOME \
    --execMode command

    ${ROBOTREPO}/LIBS/PYBOT_LAUNCHER/pybot_launcher.py -a ${HOME}/ROBOT/robot_variable.txt -l ${HOME}/ROBOT_LOG -e command

    --argumentFile: the input argument file
    --logDirectory: the directory to write log
    --execMode:     can be 'command' or 'gui'
    --suitePath:    the directory to load robot suites

Author:  
Beno K Immanuel 	Developed
Chunyan 	        Modified for command options 
Yao Qingfen        2017-3-28  Modify for and "--argumentfile" not in pybotLoopOptions	
"""
import os, re, sys, getopt, time, shutil
import parse_argument_file, commands

pybotOptionsDict = {
            '--name':[],
            '--doc':[],
            '--metadata':[],
            '--settag': [],
            '--test':[],
            '--suite': [],
            '--include': [],           
            '--exclude':[],
            '--rerunfailed':[],
            '--critical':[],
            '--noncritical':[],
            '--variable':[], 
            '--variablefile':[],
            '--outputdir':[],
            '--output':[],
            '--log':[],           
            '--report':[], 
            '--xunit':[],
            '--xunitskipnoncritical':[],
            '--debugfile':[],
            '--timestampoutputs':[],
            '--splitlog':[], 
            '--logtitle':[],            
            '--reporttitle':[],
            '--reportbackground':[],
            '--loglevel':['debug'],
            '--suitestatlevel':[], 
            '--tagstatinclude':[],            
            '--tagstatexclude':[],           
            '--tagstatcombine':[],           
            '--tagdoc':[],
            '--tagstatlink':[],            
            '--removekeywords':[],
            '--flattenkeywords':[],
            '--listener':[],          
            '--warnonskippedfiles':[],
            '--nostatusrc':[],        
            '--runemptysuite':[],
            '--dryrun':[],
            '--exitonfailure':[],
            '--exitonerror':[],
            '--skipteardownonexit':[''],
            '--randomize': [],
            '--prerunmodifier': [],
            '--prerebotmodifier':[],
            '--console':[],
            '--consolewidth':[],
            '--consolecolors':[],
            '--consolemarkers':[],                      
            '--pythonpath':[],
            '--escape':[],
            '--argumentfile':[]                                 
}

def __update_dict_base_keys (paramMapDict) :
    """
    only update pybotOptionsDict's keys with prefix "--" like "--variable"
    split options like "--suite = A,B" to d['--suite'] = ["A","B"]

    update pybotOptionsDict according to paramMapDict
    """
    if not paramMapDict : 
        return pybotOptionsDict

    # get options like  '--variable' : 'release:4.5'
    for key in paramMapDict :
        if not re.search('^--',key) :
            continue
        if key not in pybotOptionsDict.keys() :
            print (" '%s' may not an available option of pybot, ignore it." % key)
        else :         
            pybotOptionsDict[key].extend(paramMapDict[key].split(","))

    # get options like   'RELEASE = 5.2.01'           
    for key in paramMapDict :               
        if key in ['NT','SHELF','COVERAGE','ISAMIP'] or re.search('^--',key) :
            continue
        else :
            pybotOptionsDict['--variable'].append(key+':'+paramMapDict[key])

    # skip if COVERAGE is host, only include 'daily' and 'weekly'
    if paramMapDict.has_key('COVERAGE') and paramMapDict['COVERAGE'].lower() != 'host' :
            
        if len (pybotOptionsDict['--include']) == 0 :
            # domain tag should replace coverage tag
            pybotOptionsDict['--include'].append(paramMapDict['COVERAGE'])
        else :
            taglist=[]
            for includetag in pybotOptionsDict['--include']:
                includetag = paramMapDict['COVERAGE']+'AND'+includetag
                taglist.append(includetag)
            pybotOptionsDict['--include'] = taglist
 
    return pybotOptionsDict


def create_pybot_commands (suitePath,argumentDict,logDir) :
    """
       created pybot command list based on the basic pybot command and looped object options
    """
   
    #create basic pybot command with necessary options
    try :
        __update_dict_base_keys (argumentDict)
    except Exception as inst :
        raise  AssertionError ("got exception: %s", inst) 

    # create pybot options segment
    pybotOptions = ""
    for key in sorted(pybotOptionsDict.keys()):
        value = pybotOptionsDict[key]
        if len(value) == 0:
            continue
        for index in range(len(value)):
            pybotOptions = pybotOptions + " " + key + " " + value[index]

    releaseOption = " --prerunmodifier "+os.environ['ROBOTREPO']+"/LIBS/PRE_RUN_MODIFIER/release_test.py:" + argumentDict['RELEASE']
    pybotOptions = pybotOptions + releaseOption

    # end: create pybot options segment
         
    setupFile = parse_argument_file.get_setup_file(argumentDict)

    try :
        pybotLoopOptions = parse_argument_file.get_pybot_loop_options \
            (setupFile,logDir,argumentDict)
    except Exception as inst :
        print "  fail to create pybot options, exception: "+str(inst)
        sys.exit(1)
    # log/report will be replaced with consistent tool directly
    # pybotProc = "pybot -l NONE -r NONE --debugfile debug_record.txt"   
    if "--debugfile" in pybotOptions or len(pybotLoopOptions) != 0:
        pybotProc = "pybot"
    else:
        pybotProc = "pybot --debugfile debug_record.txt"
    capabOption = parse_argument_file.get_capability_options(pybotOptions,argumentDict)
    if capabOption == '':
        basicCmd = pybotProc + " " + pybotOptions
    else:
        basicCmd = pybotProc + " " + pybotOptions + " " + capabOption

    cmdList = []
    if len(pybotLoopOptions) == 0 :
        suspendOption = parse_argument_file.create_suspend_options \
        (logDir, argumentDict['PRODUCT'],argumentDict['RELEASE'],setupFile,hostOrTarget=argumentDict['COVERAGE'])  
        cmdList.append(basicCmd + " " + suspendOption + " " + suitePath)    
    else :
        for item in pybotLoopOptions :
            pybot_Options = pybotOptions
            if item.find('--variable LT:') != -1:
                pybot_Options = pybot_Options + " " + parse_argument_file.get_capability_options(item,argumentDict)
            else:
                pybot_Options = pybot_Options + capabOption
            updatedOptionsList = (pybot_Options + " " + item + " ").split("--")
            updatedOptions = "--".join(sorted(updatedOptionsList))
            updatedOptions.strip().lstrip()
            cmdList.append(pybotProc + " " + updatedOptions + " " + suitePath)

        
    return cmdList    

if __name__ == '__main__' :

    try:
        opts, args = getopt.getopt \
        (sys.argv[1:],"ha:l:e:s:r:",["argumentFile=","logDirectory=","execMode=","suitePath=","robotRepo=","help"])  
    except getopt.GetoptError:
        print "illegal options: %s" % str(sys.argv[1:])
        sys.exit(1)

    if "-h" in sys.argv[1:] or "--help" in sys.argv[1:] :
        print "Usage:"
        print "1, ./pybot_launcher.py --argumentFile $HOME/robot_variable.txt --logDirectory $HOME --execMode command"
        print "2, ./pybot_launcher.py --argumentFile $HOME/robot_variable.txt --logDirectory $HOME --execMode gui"    
        print "3, ./pybot_launcher.py --argumentFile $HOME/robot_variable.txt --logDirectory $HOME --execMode command --suitePath $HOME/MY_SUITES"            
        sys.exit(0)   
    
    # default values
    argumentFile = None
    suitePath = None
    logFile = "log.html"
    execMode = "command"
    robotRepo = None
    for opt, value in opts:
        if "-a" == opt or "--argumentFile" == opt:
            argumentFile = value
        elif "-l" == opt or "--logDirectory" == opt:
            logDir = value.rstrip ("/")
        elif "-e" == opt or "--execMode" == opt:
            execMode = value
        elif "s" == opt or "--suitePath" == opt:
            suitePath = value
        elif "r" == opt or "--robotRepo" == opt:
            robotRepo = value

    print "1 check the existing of basic robot repo path ..."

    try:
        if not robotRepo :
            robotRepo = os.environ['ROBOTREPO']
        else :
            os.environ['ROBOTREPO'] = robotRepo
    except Exception as inst:
        print "exception: " + inst
        print "ROBOTREPO WAS NOT SET in ~/.bashrc" 
        print "pls execute 'export ROBOTREPO=/repo/$USER/robot' or give '--robotRepo /repo/${USER}/robot' "
        sys.exit(1)

    if not os.path.isdir(robotRepo):
        print "  --",robotRepo,"is not a directory!"
        sys.exit(1)
    
    print "2 parse the argument file ..."
    
    try :    
        argumentDict = parse_argument_file.parse_argument_file(argumentFile,logDir)
    except Exception as inst:
        print "  --fail to parse argument file '%s', exception: '%s'" \
        % (argumentFile,str(inst))
        sys.exit(1)

    if not suitePath :    
        try:
            suitePath=parse_argument_file.get_super_suite_path \
            (argumentDict)
        except Exception as inst:
            print "  --failed to get suite directory for this DUT, exception: ", inst
            sys.exit(1) 

    try :
        parse_argument_file.generate_test_list (logDir+"/TEST_CASE.txt",suitePath)
    except Exception as inst:
        print "  --failed to create ATC list, exception: ", inst
        print "  --will continue running ..."
        
    if execMode == 'gui' :    
        print "3 start ride.py gui running ..."
        os.system('ride.py ' + suitePath)
    suiteRerunFlag = False
    if argumentDict.has_key('--rerunfailed') :
        if argumentDict['--rerunfailed'] == 'enable' :
            suiteRerunFlag = True
        del argumentDict['--rerunfailed']
    if execMode == 'command' : 
        try :
            print "3 create pybot command ..."
            pybotCmds = create_pybot_commands (suitePath,argumentDict,logDir)
        except Exception as inst :
            print "  --fail to create pybot commands, exception: "+str(inst)
            sys.exit(1)           
            
        print "4 write pybot commands to '" + logDir + "/pybotcmd.txt' and execute ..."     
        try :
            with open(logDir+'/pybotcmd.txt',"w+") as fin:
                fin.write("\t\t".join(pybotCmds))
            cs_res = os.popen('cat $ROBOTREPO/env/CS_IDENTIFIER.txt').read()
            with open(argumentFile,"a") as fin:
                fin.write("# CS_Rev = " + cs_res)
                fin.write("# PYBOT = "+"\n\n".join(pybotCmds))

        except Exception as inst :
            print "  --fail to record pybot command into file, exception:"+str(inst)
            
        time.sleep(2) 

        for cmd in pybotCmds :
            try :
                print "  "+cmd
                os.system(cmd) 
                if suiteRerunFlag :  
                    cmdSplit = re.split(r'[(\s)*?(\t)*?]+',cmd)
                    cmdExecutable = cmdSplit.pop(0)
                    cmdDatasource = cmdSplit.pop(-1)
                    cmdOthers = " ".join(cmdSplit)
                    res = re.search(r'([\s]*?--debugfile[\s]+?[\w]+?)\.txt',cmdOthers)
                    if res :
                        cmdOthers = cmdOthers.replace(res.group(1),res.group(1) + '_rerun')
                    else:
                        cmdOthers = cmdOthers + " --debugfile debug_record_rerun.txt"

                    res = re.search(r'([\s]*?--log[\s]+?[\w]+?)\.html',cmdOthers)
                    if res :
                        cmdOthers = cmdOthers.replace(res.group(1),res.group(1) + '_rerun')
                    else:
                        cmdOthers = cmdOthers + " --log log_rerun.html"

                    res = re.search(r'([\s]*?--output[\s]+?[\w]+?)\.xml',cmdOthers)
                    old_outputFile = 'output.xml'
                    if res :
                        cmdOthers = cmdOthers.replace(res.group(1),res.group(1) + '_rerun')
                        listenerLog = res.group(1) + '_rerun_listener'
                        old_outputFile = res.group(1)
                    else:
                        cmdOthers = cmdOthers + " --output output_rerun.xml"
                        listenerLog = 'output_rerun_listener'

                    res = re.search(r'([\s]*?--report[\s]+?[\w]+?)\.html',cmdOthers)
                    if res :
                        cmdOthers = cmdOthers.replace(res.group(1),res.group(1) + '_rerun')
                    else:
                        cmdOthers = cmdOthers + " --report report_rerun.html"

                    cmd = cmdExecutable + " " + cmdOthers + " --rerunfailed " + logDir + '/' + old_outputFile

                    listenerOption = ' --listener ' +os.environ['ROBOTREPO']+"/LIBS/LISTENER/listener_td.py:log_" + logDir + '/' + listenerLog
                    cmd = cmd + listenerOption + " " + cmdDatasource
                    #cmd = cmd + " " + cmdDatasource
                    print "  Rerun pybot command as following:"
                    print "  "+cmd
                    os.system(cmd)
                    #mergeCmd = 'pybot --merge ' + logDir + '/' + outputXML + ' ' +  logDir + '/' + rerun_outputXML
                    #os.system(mergeCmd)
            except Exception as inst:
                print "  --fail to invoke pybot '"+cmd+"', exception:\n", inst
                sys.exit(1)


