"""
read NT LT trace and write messages to specific file
--listener /repo/auto/robot/LIBS/LISTENER/listener_td.py:${target server ip}:${username}:${password}:${how to login target server}:${td_cmd_file}

calling example:
    1 execute specific trace collection for some suites and tests according to trace description file
    --listener /repo/auto/robot/LIBS/LISTENER/listener_td.py:/repo/auto/robot/LIBS/LISTENER/sample_td_describe.csv

    2 execute specific trace collection for some suites and tests according to trace description file
    --listener /repo/auto/robot/LIBS/LISTENER/listener_td.py:/repo/auto/robot/LIBS/LISTENER/sample_td_describe.yaml

    3 execute specific trace collection for some suites and tests according to input parameters. 
      Parameters format examples: key1_value1:key2_value2:key3_value3:key4_value4
      key options: ip,port,username,password,type,log,RobootTime,prompt,PortServerName,PortServerPassword
    --listener /repo/atxuser/robot/LIBS/LISTENER/listener_td.py:ip_135.252.138.77:port_2013:type_terminal_server:username_shell:password_nt:log_/tmp/trace_debug:prompt_'>':PortServerName_root:PortServerPassword_dbps:TraceDescFile_/repo/atxuser/robot/SW_DESCRIPTION/EPON_DPOE/TD_CMD_DESC.csv

    4. execute specific trace collection for some suites and tests according to the table defined in SETUP file 
    --listener /repo/atxuser/robot/LIBS/LISTENER/listener_td.py

"""
import os
import sys
import commands
import time
import re
import txtLogger
import htmlLogger

try:
    import data_translation
except ImportError :
    sys.path.append(os.environ.get('ROBOTREPO')+'/LIBS/DATA_TRANSLATION')
    import data_translation

try :
    import pexpect
except ImportError :
    sys.path.append(os.environ.get('ROBOTREPO')+'/TOOLS/pexpect-2.3_1')
    import pexpect

class listener_td:
    """Pass all listener events to a remote listener

    If called with one argument, that argument is a td config file
    If called with two, the second is a slot number of LT board
    """
    ROBOT_LISTENER_API_VERSION = 2

    def __init__(self, *args):
        try:
	    # Priority: Default, ip and port is necessary, will not be set by default.
            self.ip = ''
            self.port = ''
            self.lt_slot = ''
            self.username = "shell"
            self.password = "nt"
            self.defPassword = "nt"
            self.type = "octopus"
            self.log = "/tmp/td.log"
            self.prompt = "]\#|]%|]\$"
            self.rebootTime = 900
            self.portServerName = 'root'
            self.portServerPassword = 'dbps'
            self.traceDescFile = ''

            # Priority: Low
            setup_file = os.environ['SETUPFILENAME']
            if os.path.exists(setup_file):
                self._get_config_info(setup_file)

            for opt in args:
                if os.path.isfile(opt):
                    # Priority: Middle
                    td_desc_file = opt
                    self.td_desc_file = td_desc_file
                    self._get_config_info(self.td_desc_file)
                else:
                    # Priority: High
                    key,value = opt.split("_",1)
                    print "Key: "+key+"\tValue "+value
                    if key.lower() == "ip":
                        self.ip = value
                    if key.lower() == "port":
                        self.port = value
                    if key.lower() == "username":
                        self.username = value
                    if key.lower() == "password":
                        self.password = value
                    if key.lower() == "type":
                        self.type = value
                    if key.lower() == "log":
                        self.log = value
                    if key.lower() == "prompt":
                        self.prompt = value.strip("'")
                    if key.lower() == "rebootTime":
                        self.rebootTime = int(value)
                    if key.lower() == "lt_slot":
                        self.lt_slot = value
                    if key.lower() == "PortServerName":
                        self.portServerName = value
                    if key.lower() == "PortServerPassword":
                        self.portServerPassword = value
                    if key.lower() == "TraceDescFile" :
                        self.traceDescFile = value
                        self._get_config_info(self.traceDescFile)

            self.log             = self.log + ".log" 
            self.txtLog          = self.log + ".txt"
            self.htmlLog         = self.log + ".html"
            self.interface       = ""
            self.fileHandler     = open (self.log,'w')
            self.txtFileHandler  = open (self.txtLog,'w')
            self.htmlFileHandler = open (self.htmlLog,'w')
            self.if_alive        = False
            self._generateLog('header')

            if self.ip == '' or self.port == '':
                raise AssertionError("Couldn't get necessary IP or Port info, please set them in setup file,")
            else:
                print "IP: %s; PORT: %s" % (self.ip, self.port)  ## CROP
                self._open()
        except Exception as inst:
            s = sys.exc_info()
            raise AssertionError("%s: Failed to init, lineno:%s, caused for the exception: %s" % (__name__,s[2].tb_lineno, inst))

    def _get_config_info(self,config_file):
        if os.path.exists(config_file):
            try:
                data_translation.create_global_tables(config_file)
                dict_TD = data_translation.get_table_line('TDData')
                self.ip = dict_TD.pop('ip',self.ip)
                self.port = dict_TD.pop('port',self.port)
                self.username = dict_TD.pop('username',self.username)
                self.password = dict_TD.pop('password',self.password)
                self.type  = dict_TD.pop('type',self.type)
                self.log = dict_TD.pop('log',self.log)
                self.prompt = dict_TD.pop('prompt',self.prompt)
                self.rebootTime = int(dict_TD.pop('RebootTime',self.rebootTime))
                self.portServerName = dict_TD.pop('PortServerName',self.portServerName)
                self.portServerPassword = dict_TD.pop('PortServerPassword',self.portServerPassword)

            except Exception as inst:
                raise AssertionError("Fail to get config info (table TDData) from setup file %s for Exception: %s" % (config_file,inst))


    def _open(self) :
        if self.type == "octopus" :
            if self.ip and self.port :
                self.connect_by_octopus()
            else :
                raise AssertionError("Failed to get ip and port info,please check listener param1: %s" % self.type)
        elif self.type == "terminal_server" :
            if self.ip and self.port :
                self.connect_by_terminal_server ()
            else :
                raise AssertionError("Failed to get ip,port,username and password info,please check listener param: %s" % self.type)
        elif self.type == "ssh" :
            if self.ip and self.port :
                self.connect_by_ssh ()
            else :
                raise AssertionError("Failed to get ip,port,username and password info,please check listener param: %s" % self.type)

    def write_log_list(self,args,log_info) : 
        dict_log = args
        log_list = log_info.split("\n")
        for each in log_list :
            dict_log['info'] = '%s' % each
            self._generateLog('message',dict_log)

    def write_log(self,log_level=None,log_info=None) :
        dict_log = {}
        dict_log['type'] = log_level
        dict_log['info'] = log_info
        self._generateLog('message',dict_log)

    def _generateLog(self,log_type,args=None) :
        try : 
            prog_html_log = "htmlLogger.generate_html_%s" % log_type
            prog_txt_log  = "txtLogger.generate_txt_%s" % log_type

            html_log = eval(prog_html_log+'(args)')
            txt_log  = eval(prog_txt_log+'(args)')

            if html_log : 
                html_log_list = html_log.split("\n")
                for each in html_log_list : 
                    self.htmlFileHandler.writelines("%s\n" % each)
            if txt_log :
                txt_log_list = txt_log.split("\n")
                for each in txt_log_list : 
                    self.txtFileHandler.writelines("%s\n" % each)

        except Exception as inst:
            raise AssertionError("%s: Failed to generate log, type:%s, info:%s, exception:%s" % (__name__,log_type,args,inst))

    def capture_td_log_by_normal (self,time_out=0.1,exp_prompt=None) :
        if time_out == 0 :
            time_out = 0.1

        if self.interface == "" :
            return
        returnNum = 0
        dict_log = {}
        try :
            self.write_log("info","Send:\\n")
            self.interface.sendline("\n\n")
        except Exception as inst :
            error_info = 'Failed to send enter! ' 
            self.write_log("info",error_info)
            try:
                self.write_log("info",">>>>re-connect<<<<")
                self._open()
            except Exception as inst:
                raise AssertionError ("%s-> fail to send cmd '%s', exception: %s" %
                (__name__,expectResp,send_cmd.replace ("\n","\\n"),inst) )
          
        expect_timeout = time_out
        startTime = time.mktime(time.localtime())
        endTime = startTime + int(expect_timeout)
        sRecv = ''
        while time.mktime(time.localtime()) < endTime :
            #logger.warn("enter while!!")
            #logger.warn("sRecv:%s" % sRecv)
            self.write_log("info","timeout:%s" % str(float(expect_timeout)/100.0))
            try :
                expect_value = self.interface.expect\
                    (['[\s\S]+',pexpect.EOF,pexpect.TIMEOUT],float(expect_timeout)/100.0)
            except Exception as inst :
                s = sys.exc_info()
                raise AssertionError ("unexpected error when sending cmd, lineno: %s, exception: %s: \nsend: %s\nrecv:%s\n"
                % (s[2].lineno,inst,'\n',str(self.interface.before)))

            if expect_value == 0 :
                sRecv = sRecv + self.interface.after
                #logger.warn("sRecv:%s" % sRecv)
                #logger.warn("Prompt:%s" % prompt)
                if self.interface.before :
                    self.write_log("info","found not empty before:%s" % self.interface.before)

                # don't return when prompt is received. still receive data until encounter recv timeout (that means there is no data output )
                #if re.search(exp_prompt,sRecv) :
                #    return sRecv

            elif expect_value == 1 :
                self.write_log("info","encountered eof:\n" +str(self.interface.after))
                self.write_log("info","re-connecting....")
                self.interface.close()
                time.sleep(3)
                try :
                    self._open()
                except Exception as inst :
                    raise AssertionError ("%s:%s -> open ssh failed: %s" \
                    % (__name__,keyword_name,inst))
                self.write_log("info", "re-send command: %s" % '\\n')
                self.interface.sendline('\n')
                expect_value = self.interface.expect\
                    ([exp_prompt,pexpect.EOF,pexpect.TIMEOUT],int(expect_timeout))
                if expect_value == 0 :
                    dict_log['type'] = "info"
                    try:
                        self.write_log_list(dict_log,str(self.interface.before)+str(self.interface.after))
                    except Exception as inst:
                        self.write_log('info','>>>>>>>>>Received before string is EMPTY!!!<<<<<<<<<<<<<<<<<<<')
 
                else :
                    raise AssertionError ("encountered timeout or eof for se-send command:\n" +str(self.interface.after))

            elif expect_value == 2 :
                self.write_log("info","encountered timeout (%s/100 seconds): %s\n" % (expect_timeout,exp_prompt))
                self.write_log("info","BEFORE:%s" % str(self.interface.before))
                # if encounter timeout and sRecv has prompt, then return.
                if re.search(exp_prompt,sRecv) :
                    break 
                time.sleep(0.01)

        if not re.search(exp_prompt,sRecv) :
            self.write_log("info","Can't find prompt '%s' in reply! " % exp_prompt)

        dict_log['type'] = "info"
        try:
            self.write_log_list(dict_log,sRecv)
        except Exception as inst:
            self.write_log('info','>>>>>>>>>Received before string is EMPTY!!!<<<<<<<<<<<<<<<<<<<')

        """
        try :
            returnNum = self.interface.expect([exp_prompt,pexpect.EOF,pexpect.TIMEOUT],timeout=time_out)
           
        except Exception as inst :
            self.fileHandler.writelines("gotten exception: '%s'\n" % inst)
            returnNum = 0

        if returnNum <= 0 :
            dict_log['type'] = "info"

            try: 
                self.write_log_list(dict_log,self.interface.before)  
            except Exception as inst:
                self.write_log('info','>>>>>>>>>Received before string is EMPTY!!!<<<<<<<<<<<<<<<<<<<')

         else:
            # check if session alive
            self.interface.sendline('\n')
            n = self.interface.expect(['[\s\S]+',\
            pexpect.EOF,pexpect.TIMEOUT],timeout=3)

            if n > 0 :
                self.write_log("info",">>>>re-connect<<<<")
                self._open()
        """

    def close_trace (self) :
        if self.interface == "" : return
        try:
            if self.type == "octopus" :
                self._send_cmd ('exit','Logout.*USR.*',20)
            self.interface.terminate()  # to avoid gui pop up when close()
            self.interface.close()
            self.write_log("info","TRACE: CLOSE INTERFACE\n")
        except Exception as inst :
            self.write_log("error","Failed to close session:%s\n" % inst)


    def connect_by_terminal_server(self) : 
        cmd_telnet       = "" 
        currentTime      = time.mktime(time.localtime())
        endTime          = currentTime + self.rebootTime
        dict_log         = {}
        dict_log['type'] = 'info'

        fLogin = "FAIL"

        while currentTime <= endTime :
            try :
                cmd_telnet       = "telnet "+ self.ip 
                self.interface = pexpect.spawn(cmd_telnet)
            except Exception as inst :
                error_log = "Fail to exec 'pexpect.spawn(%s)', exception: %s\n" % (cmd_telnet,inst)
                self.write_log("error", error_log)
                time.sleep(10)
                currentTime = time.mktime(time.localtime())
                continue

            self.write_log("info", cmd_telnet)

            list_login_cmd   = [\
                   ['\n',                     '.*ogin: '],\
                   ['%s'     % self.portServerName, '.*assword: '],\
                   ['%s\n\n' % self.portServerPassword, ''],\
                   ['kill %s\n' % self.port[-2:], '']
            ]

            # login port server and kill connected port
            for each in list_login_cmd:
                send_cmd,prompt = each
                try :
                    reply_info = self._send_cmd (send_cmd,prompt,20)
                    reply_info = "%s" % reply_info 
                    self.write_log_list(dict_log,reply_info)
 
                    if reply_info == "FAIL" : 
                        raise AssertionError("%s: Failed to login terminal server: %s" % (__name__,cmd_telnet))
                except Exception as inst:                  
                    currentTime = time.mktime(time.localtime())
                    error_info = "%s: Failed to send cmd: %s, exception:%s\n" % (__name__,send_cmd,inst)
                    dict_log['type'] = "error"
                    dict_log['info'] = error_info
                    self.write_log_list(dict_log,error_info)
                    self.close_trace()
                    currentTime = time.mktime(time.localtime())
                    raise AssertionError("%s: Failed to send cmd: %s, exception:%s" % (__name__,send_cmd,inst))
            # close port server session
            self.close_trace()
            time.sleep(3)

            cmd_telnet = "telnet "+ self.ip + " " + self.port
            try :
                self.write_log("info", cmd_telnet)
                self.interface = pexpect.spawn(cmd_telnet)

            except Exception as inst :
                error_log = "Fail to exec 'pexpect.spawn(%s)', exception: %s\n" % (cmd_telnet,inst)
                self.write_log("error", error_log)
                time.sleep(10)
                currentTime = time.mktime(time.localtime())
                continue

            try:
                iPasswordCount = 0
                iTimeout       = 0
                while 1:
                    recv = ""
                    send_cmd = ""
                    expect_value = self.interface.expect(['[\s\S]+',pexpect.EOF,pexpect.TIMEOUT],3)
                    if expect_value == 0:
                        recv = self.interface.after
                    else:
                        iTimeout += 1
                        if iTimeout > 1:
                            break
                        else:
                            recv = self.interface.before
                            recv = "%s" % recv
                            self.write_log("info",str(recv))
                            self.write_log("info","send:\\n\\n")
                            self.interface.sendline('\n\n')
                            self.write_log("info",'received:')
                            self.write_log("info",str(recv))                            
                            continue

                    self.write_log("info",str(recv))
                    # checking return information
                    if '#' in recv or '\$' in recv or '%' in recv or '>' in recv or 'isam-reborn login' in recv:
                        fLogin = "PASS"
                        break
                    elif "ogin" in recv:
                        send_cmd = self.username
                    elif "enter new password" in recv:
                        send_cmd = self.password
                    elif "re-enter new password" in recv:
                        send_cmd = self.password
                    elif "assword" in recv:
                        send_cmd = self.password
                        if iPasswordCount == 0 :
                            send_cmd = self.password
                            iPasswordCount += 1
                        elif iPasswordCount == 1 :
                            send_cmd = self.defPassword
                            iPasswordCount += 1
                        else :
                            self.write_log("info","to write '%s', after got 3+ times \
                            of 'password' prompt" % self.password)
                            send_cmd = self.password
                    elif "Are you sure you want to continue connecting" in recv:
                        send_cmd = "yes"
                    elif "Connection refused" in recv:
                        self.write_log("error",str(recv))
                        raise AssertionError(str(recv))
                    elif "Permission denied" in recv or "Connection Refused!" in recv or "Unable to connect\
                    " in recv or "closed by remote host" in recv:
                        break
                    elif 'Host key verification failed' in recv:
                        if self.port == "22" :
                            cmd = "ssh-keygen -R " + self.ip
                        else :
                            cmd = "ssh-keygen -R [" + self.ip + "]:" + self.port
                        cmd = "%s" % cmd
                        self.write_log("info",'exec ' + cmd + "' to clean inconsistent public key" )
                        res = pexpect.run ( cmd )
                        self.write_log("info",str(res))
                        self.interface.close()
                        self.write_log("info","inconsistent public key cleaned, please retry")
                        time.sleep(2)
                        break 
                    else:
                        send_cmd = "\n"
                        continue 

                    self.write_log("info","send:%s" % send_cmd)
                    self.interface.sendline(send_cmd) 
                
            except Exception as inst:
                s = sys.exc_info()
                raise AssertionError("%s: Failed to login NT trace: %s,line:%s" % (__name__,cmd_telnet,s[2].tb_lineno))
                self.interface.close()
                time.sleep(10)
                currentTime = time.mktime(time.localtime())
                continue
            else :
                self.write_log("info",self.interface.after )

            if fLogin == "PASS":
                break

            time.sleep(2)
            currentTime = time.mktime(time.localtime())
        # end while

        if currentTime > endTime:
            self.fileHandler.writelines("fail to login trace in %s seconds\n" % str(self.rebootTime))
            self.interface = ""
            raise AssertionError("%s-> fail to login trace in %s seconds" % (__name__, str(self.rebootTime)))

    def connect_by_octopus(self) :
       
        # octopus STDIO 135.251.200.15:udp:23
        cmd_octopus      = os.environ['ROBOTREPO']+"/PACKAGES/bin/octopus STDIO " + self.ip + ":udp:" + self.port + "\n"   
        currentTime      = time.mktime(time.localtime())
        endTime          = currentTime + self.rebootTime

        dict_log         = {}
        dict_log['type'] = 'info' 
        list_login_cmd   = [\
                   ['\n',                     '.*Login: '],\
                   ['%s'     % self.username, '.*Password: '],\
                   ['%s\n\n' % self.password, ''],\
                           ]

        while currentTime <= endTime :         
            try :
                self.interface = pexpect.spawn(cmd_octopus)
            except Exception as inst :
                error_log = "Fail to exec 'pexpect.spawn(%s)', exception: %s\n" % (cmd_octopus,inst)
                self.write_log("error", error_log)
                time.sleep(10)
                currentTime = time.mktime(time.localtime())
                continue
            
            self.write_log("info", cmd_octopus)

            for each in list_login_cmd:
                send_cmd,prompt = each
                try :
                    reply_info = self._send_cmd (send_cmd,prompt,20)
                    reply_info = "%s" % reply_info 
                    self.write_log_list(dict_log,reply_info)
 
                    if reply_info == "FAIL" : 
                        raise AssertionError("%s: Failed to login NT trace: %s" % (__name__,cmd_octopus))
                except Exception as inst:
                    self.close_trace()
                    currentTime = time.mktime(time.localtime())
                    error_info = "%s: Failed to send cmd: %s, exception:%s\n" % (__name__,send_cmd,inst)
                    dict_log['type'] = "error"
                    dict_log['info'] = error_info
                    self.write_log(dict_log)
                    raise AssertionError("%s: Failed to send cmd: %s, exception:%s" % (__name__,send_cmd,inst))
                 
            self.write_log("info", "Login Success!")          
            break

        if currentTime > endTime :
            self.fileHandler.writelines("fail to login trace in %s seconds\n" % str(self.rebootTime))
            self.interface = ""
            raise AssertionError("%s-> fail to login trace in %s seconds" % (__name__,str(self.rebootTime)))


    def connect_by_ssh (self) :
       
        # ssh -l auto -p 22 135.251.200.132
        cmd_ssh      = "ssh -l "+self.username+" "+self.ip +" -p "+self.port + "\n"  
        currentTime      = time.mktime(time.localtime())
        endTime          = currentTime + self.rebootTime

        dict_log         = {}
        dict_log['type'] = 'info' 

        while currentTime <= endTime :    
            currentTime = time.mktime(time.localtime())
            try :
                self.interface = pexpect.spawn(cmd_ssh,timeout=20)
                self.write_log("info", cmd_ssh)
                expect_value = self.interface.expect(\
                ['Are you sure you want to continue connecting \(yes\/no\)\?',\
                'assword:',\
                'Host key verification failed',\
                'Connection Refused!',\
                'Unable to connect',\
                'closed by remote host',\
                pexpect.EOF,pexpect.TIMEOUT])

            except Exception as inst :
                error_log = "Fail to exec 'pexpect.spawn(%s)', exception: %s\n" % (cmd_ssh,inst) 
                self.write_log("error", error_log)
                time.sleep(10)
                currentTime = time.mktime(time.localtime())
                continue
            else :
                self.write_log("info", self.interface.after )

            if expect_value == 0:
                self.write_log ("info", "send: yes")
                self.interface.sendline("yes")
                self.interface.expect(['assword:', pexpect.EOF,pexpect.TIMEOUT])
                self.write_log("info", self.interface.after )
                self.write_log("info","send: %s" % self.password)
                self.interface.sendline(self.password)
            elif expect_value == 1 : 
                self.write_log("info","send: %s" % self.password)
                self.interface.sendline(self.password)
            elif expect_value == 2 :

                if self.port == "22" :
                    cmd = "ssh-keygen -R " + self.ip
                else :
                    cmd = "ssh-keygen -R [" + self.ip + "]:" + self.port
                self.write_log("info", "exec '" + cmd + "' to clean inconsistent public key" )
                res = pexpect.run ( cmd ) 
                self.write_log("info",str(res))
                self.interface.close()
                self.write_log("info","inconsistent public key cleaned, please retry")
                currentTime = time.mktime(time.localtime())
                continue
            elif expect_value >= 3 :
                self.interface.close()
                self.write_log("info","unexpected error: %s" % str(self.interface.after))
                currentTime = time.mktime(time.localtime())
                continue

            try :      
                expect_value = self.interface.expect([self.prompt,\
                pexpect.EOF, pexpect.TIMEOUT],30)
            except Exception as inst:        
                self.write_log("info","fail to get prompt after giving password")
                self.interface.close()
                currentTime = time.mktime(time.localtime())
                continue
            
            self.write_log("info",self.interface.after)
            if expect_value == 0:
                self.write_log("info", "Login Success!")          
                break

        if currentTime > endTime:
            self.fileHandler.writelines("fail to login trace in %s seconds\n" % str(self.rebootTime))
            self.interface = ""
            raise AssertionError("%s-> fail to login trace in %s seconds" % (__name__, str(self.rebootTime)))

    def _send_cmd  (self,writeStr,expectResp,timeout=10) :
        keyword_name = "_write_read_session"
        dict_log = {}
        dict_log['type'] = "error"
        expectVal = 0
        try :
            self.interface.sendline(writeStr.strip("'"))
        except Exception as inst :
            error_info = 'Failed to send cmd: "%s" expect: %s \n' % (writeStr.replace("\n","\\n"),expectResp)
            self.write_log_list(dict_log,error_info)

            raise AssertionError ("%s-> fail to write '%s', exception: %s" %
            (__name__,expectResp,writeStr.replace ("\n","\\n"),inst) )
    
        try :
            if expectResp :
                returnVal = self.interface.expect([expectResp,pexpect.EOF,pexpect.TIMEOUT],timeout=int(timeout))
                expectVal = 0
            else :
                returnVal = self.interface.expect(['.*>','.*#','.*\$','.*%',pexpect.EOF,pexpect.TIMEOUT],
                timeout=int(timeout))
                expectVal = 3
        except Exception as inst :
            error_info = "%s-> fail to get '%s' when send '%s', exception: %s" % \
            (keyword_name,expectResp,writeStr.replace ("\n","\\n"),inst)
            self.write_log_list(dict_log,error_info)

            raise AssertionError ("%s-> fail to get '%s' when send '%s', exception: %s" %
            (keyword_name,expectResp,writeStr.replace ("\n","\\n"),inst) )

        if returnVal > expectVal :
            self.interface.expect(['.*',pexpect.EOF,pexpect.TIMEOUT],timeout=int(timeout))
            replyInfo = ""
            replyInfo = "%s\n%s" % (self.interface.before, self.interface.after)
            error_info = "%s\n" % replyInfo
            error_info = error_info + "ERROR: unexpected match return: %s, expect prompt:%s\n" % (returnVal,expectResp)
            self.write_log_list(dict_log,error_info)
            return "FAIL"
        else:
            return self.interface.after

    def _execute_td_cmd (self,writeStr,expectResp,timeout=5) :
    
        keyword_name = "_execute_td_cmd"
        dict_log = {}
        dict_log['type'] = "error"
        expectVal = 0
        returnVal = 0
        send_cmd = writeStr.strip("\n").strip("'")

        #print "--------prompt---------------"
        #print self.prompt
 
        try :
            n = self.interface.sendline(send_cmd)
        except Exception as inst :
            error_info = 'Failed to send cmd: "%s" expect: %s \n' % (send_cmd.replace("\n","\\n"),expectResp) 
            self.write_log_list(dict_log,error_info)
            try:
                self.write_log("info",">>>>re-connect<<<<")
                self._open()
            except Exception as inst:
                raise AssertionError ("%s-> fail to send cmd '%s', exception: %s" % 
                (__name__,expectResp,send_cmd.replace ("\n","\\n"),inst) ) 
        
        try :
            prompt_exp = ""
            if expectResp :
                prompt_exp = '[\s\S]+' + expectResp 
            else :
                #prompt_exp = '.*' + send_cmd + '.*[\r|\n]+(.*[\r|\n])?.*[#|\$|%|>]'
                prompt_exp = '[\s\S]+' + self.prompt

            if send_cmd == 'exit' : 
                timeout = 2 
                #prompt_exp = '.*' + send_cmd + '.*[\r|\n]+(.*[\r|\n])?.*[#|\$|%|>|Logout|close]'
                prompt_exp = '[\s\S]+' + "'[" + self.prompt + "|Logout|close]'"                                                            
            
            cmdVal = self.interface.expect([prompt_exp,pexpect.EOF,pexpect.TIMEOUT],timeout=int(timeout))

            if cmdVal > 0:
                if send_cmd == 'exit' :
                    error_info = "%s-> Failed to send 'exit', please check if the session has been closed!!" \
                    % keyword_name 
    
                else :
                    #print "----cmdval------"
                    #print cmdVal
                    error_info = "Failed to get response '%s' when sending '%s'" % \
                    (prompt_exp,send_cmd.replace ("\n","\\n"))
                    self.write_log_list(dict_log,error_info)
                    
                    # check if session alive
                    self.interface.sendline('\n')
                    n = self.interface.expect(['[\s\S]+',\
                    pexpect.EOF,pexpect.TIMEOUT],timeout=3)
                    
                    if n > 0 :
                        self.write_log("info",">>>>re-connect<<<<")
                        self._open()

            return self.interface.after

        except Exception as inst :
            error_info = "%s-> 2.fail to get reply'%s' when send '%s', exception: %s" % \
            (keyword_name,expectResp,send_cmd.replace ("\n","\\n"),inst) 
            self.write_log_list(dict_log,error_info)

            raise AssertionError ("%s-> fail to get reply '%s' when send '%s', exception: %s" % 
            (keyword_name,expectResp,send_cmd.replace ("\n","\\n"),inst) )  

    def capture_td_log_by_cmd (self,td_cmd_list):
        """
        td_cmd_list: specific commands for collecting trace debug
        """
        keyword_name       = "capture_td_log"
        # td_cmd_list        = []
        total_recv_info    = ""

        try : 
            #display td_cmd_list in log
            dict_log = {}
            dict_log['type'] = 'show'
            td_cmd_info = '********TD_CMD_LIST********\n'
            for each in td_cmd_list:
                td_cmd_info = td_cmd_info + '   %s\n' % (each)
            td_cmd_info = td_cmd_info + '***************************'
            self.write_log_list(dict_log,td_cmd_info)

            for each in td_cmd_list:
                td_cmd = ""
                prompt = self.prompt 
                if len(each.split(':')) == 2 :            
                    td_cmd,prompt = each.split(':')
                elif len(each.split(':')) == 1 :
                    td_cmd = str(each.split(':'))
                    td_cmd = td_cmd.strip('[').strip(']')
                else:
                    raise AssertionError("%s->%s: Failed to parse the td_cmd:%s" % (__name__,keyword_name,each))
                
                # send td cmd
                reply_info = self._execute_td_cmd (td_cmd,prompt,5)
              
                # write reply_info in log
                dict_log = {}
                dict_log['type'] = 'show'

                if reply_info == 'FAIL' :
                    reply_info = "SEND: %s FAIL" % (td_cmd) 
                    dict_log['type'] = 'error'
                else:
                    reply_info = "%s" % reply_info

                reply_info = reply_info + "\n---------------------------\n"
                self.write_log_list(dict_log,reply_info)
               
                # set total_recv_info
                total_recv_info = total_recv_info + reply_info 

            return total_recv_info

        except Exception as inst :
            s=sys.exc_info()
            raise AssertionError("%s->%s: Failed to capture td log,line:%s exception:%s" \
            % (__name__,keyword_name,s[2].tb_lineno,inst))
    
    def get_td_timeout_from_desc_file(self,name,name_type,td_type):
        keyword = "get_td_timeout_from_desc_file"

        dict_TdRecord = {}
        timeout = 0 

        try :
            # get td index list 
            dict_TdRecord = data_translation.get_table_line('TdCmdListData',
                            search_condition='ModuleLevel=%s,ModuleName=%s,Type=%s' % (name_type.upper(),name,td_type))
            dict_TdRecord = dict(map(lambda (k,v): (str(k).lower(), str(v)), dict_TdRecord.iteritems()))

        except Exception as inst:
            self.close_trace()
            # raise AssertionError("%s->%s:Failed to get td idx list from desc file:exception:%s\n" % (__name__,keyword,inst))

        if not dict_TdRecord :
            timeout = 0 

            try:
                timeout = dict_TdRecord['timeout']     

            except Exception as inst:
                timeout = 0 
        
        return timeout
    
    def get_td_cmds_from_desc_file(self,name,name_type,td_type):
        """
        """
        keyword = "get_td_cmds_from_desc_file"

        dict_TdRecord = {}
        list_cmd      = []

        try :
            # get td index list 
            dict_TdRecord = data_translation.get_table_line('TdCmdListData',
                            search_condition='ModuleLevel=%s,ModuleName=%s,Type=%s' % (name_type.upper(),name,td_type))
        except Exception as inst:
            self.close_trace()
            #raise AssertionError("%s->%s:Failed to get td idx list from desc file:exception:%s\n" % (__name__,keyword,inst))

        if not dict_TdRecord :
            return None
        try:
            list_tdIndex = dict_TdRecord['TdIndexList'].split(";")
           
            # get each td cmd according to index and append it to list_cmd
            for each_index in list_tdIndex : 
                m = re.search('\S+',each_index) 
                if not m :
                    print ("----skip------")
                    continue 
                dict_tdCmd = data_translation.get_table_line('TdCmdDescData','TdIndex='+each_index)
                if dict_tdCmd :   
                    list_cmd.append(dict_tdCmd['TdCmd'])
                else :
                    list_cmd.append(each_index)
            return list_cmd 

        except Exception as inst:
            self.close_trace()
            s = sys.exc_info()
            raise AssertionError("%s->%s:Failed to get td cmds from desc file:line:%s,exception:%s\n" \
            % (__name__,keyword,s[2].tb_lineno,inst))

    def start_test(self, name, attrs):

        dict_log = {}
        dict_log['name'] = name
        dict_log['time'] = attrs['starttime']
        dict_log['type'] = 'START'

        self._generateLog('test_header',dict_log)

        timeout = self.get_td_timeout_from_desc_file(name,'TEST','START')        
        self.capture_td_log_by_normal(timeout,self.prompt)

        # get default trace cmd for all test
        cmd_list = self.get_td_cmds_from_desc_file('_ALL_','TEST','START')
        if cmd_list :
            self.capture_td_log_by_cmd(cmd_list)

        cmd_list = self.get_td_cmds_from_desc_file(name,'TEST','START') 

        if cmd_list :      
            self.capture_td_log_by_cmd(cmd_list)

        self._generateLog('test_footer',dict_log)

    def end_test(self, name, attrs):
        dict_log = {}
        dict_log['name'] = name
        dict_log['time'] = attrs['endtime']
        dict_log['type'] = 'END'

        self._generateLog('test_header',dict_log)   
        timeout = self.get_td_timeout_from_desc_file(name,'TEST','END')        
        self.capture_td_log_by_normal(timeout,self.prompt)

        # get default trace cmd for all test 
        cmd_list = self.get_td_cmds_from_desc_file('_ALL_','TEST','END')
        if cmd_list :
            self.capture_td_log_by_cmd(cmd_list)

        cmd_list = self.get_td_cmds_from_desc_file(name,'TEST','END') 
        
        if cmd_list :      
            self.capture_td_log_by_cmd(cmd_list)     
        self._generateLog('test_footer',dict_log)

    def start_suite(self, name, attrs):
        dict_log = {}
        dict_log['name'] = name
        dict_log['time'] = attrs['starttime']
        dict_log['type'] = 'START'
        self._generateLog('suite_header',dict_log)

        timeout = self.get_td_timeout_from_desc_file(name,'SUITE','START')        
        self.capture_td_log_by_normal(timeout,self.prompt)

        # get default trace cmd for all suite
        cmd_list = self.get_td_cmds_from_desc_file('_ALL_','SUITE','START')
        if cmd_list :
            self.capture_td_log_by_cmd(cmd_list)

        cmd_list = self.get_td_cmds_from_desc_file(name,'SUITE','START') 
        if cmd_list :      
            self.capture_td_log_by_cmd(cmd_list)
        
        self._generateLog('suite_footer',dict_log)


    def end_suite(self, name, attrs):
        dict_log = {}
        dict_log['name'] = name
        dict_log['time'] = attrs['endtime']
        dict_log['type'] = 'END'

        self._generateLog('suite_header',dict_log)

        timeout = self.get_td_timeout_from_desc_file(name,'SUITE','END')        
        self.capture_td_log_by_normal(timeout,self.prompt)

        # get default trace cmd for all suite 
        cmd_list = self.get_td_cmds_from_desc_file('_ALL_','SUITE','END')
        if cmd_list :
            self.capture_td_log_by_cmd(cmd_list)

        cmd_list = self.get_td_cmds_from_desc_file(name,'SUITE','END') 
        if cmd_list :      
            self.capture_td_log_by_cmd(cmd_list)
        self._generateLog('suite_footer',dict_log)


    def start_keyword(self, name, attrs):
        dict_log = {}
        dict_log['name'] = name
        dict_log['time'] = attrs['starttime']
        dict_log['type'] = 'START'

        timeout = self.get_td_timeout_from_desc_file(name,'KEYWORD','START')   
        cmd_list = self.get_td_cmds_from_desc_file(name,'KEYWORD','START')
 
        if timeout > 0 or cmd_list :  
            self._generateLog('keyword_header',dict_log)

        if timeout > 0:
            self.capture_td_log_by_normal(timeout,self.prompt)

        cmd_list = self.get_td_cmds_from_desc_file(name,'KEYWORD','START') 
        if cmd_list :    
            self.capture_td_log_by_cmd(cmd_list)

        if timeout > 0 or cmd_list :
           self._generateLog('keyword_footer',dict_log)

    def end_keyword(self, name, attrs):
        dict_log = {}
        dict_log['name'] = name
        dict_log['time'] = attrs['endtime']
        dict_log['type'] = 'END'
        
        timeout = self.get_td_timeout_from_desc_file(name,'KEYWORD','END')        
        cmd_list = self.get_td_cmds_from_desc_file(name,'KEYWORD','END')

        if timeout > 0 or cmd_list :
            self._generateLog('keyword_header',dict_log)

        if timeout > 0 :
            print ">>>>>>>>>>>>>>>>>>>>>>timeout > 0"
            self.capture_td_log_by_normal(timeout,self.prompt)

        cmd_list = self.get_td_cmds_from_desc_file(name,'KEYWORD','END')
        if cmd_list :   
            self.capture_td_log_by_cmd(cmd_list)

        if timeout > 0 or cmd_list :
           self._generateLog('keyword_footer',dict_log)

    def message(self, message):
        pass

    def log_message(self, message):
        pass

    def log_file(self, path):
        self.fileHandler.writelines("\nTRACE: LOG FILE PATH: %s\n" % (path))
        pass

    def output_file(self, path):
        pass

    def report_file(self, path):
        pass

    def summary_file(self, path):
        pass

    def debug_file(self, path):
        pass

    def close(self):
        self.write_log("info", "DEBUG: CLOSE TD SESSION")
        self._send_cmd ('exit','.*',20)
        self._generateLog('footer',None)
        self.interface.terminate() # to avoid gui pop up when close()
        self.interface.close()
        self.write_log("info", "################################################")
        self.write_log("info", "DEBUG END AT '%s' \n" % commands.getoutput('date'))
        self.write_log("info", "################################################")
        self.htmlFileHandler.close()
        self.txtFileHandler.close()
        print ("NT TRACE: %s" % os.path.abspath(self.log))

