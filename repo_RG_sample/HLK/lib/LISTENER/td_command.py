import listener_td
from robot.api import logger

TRACE_SESSION = ""

def open_trace_session (ip,port="23",username="shell",password="nt",type="octopus",log="/tmp/td.log",
    td_desc_file=None) :
    """
    open trace session based on ssh protocol

    - *ip:* ip address of ssh server (default 127.0.0.1 will cause error)
    - *port:* port number of ssh server, default value 23
    - *username:* name of ssh user , default value 'shell'
    - *password:* password of the ssh user, default value 'nt'
    - *type:* octopus or terminal_server
    - *log:*  the path of exporting trace log
    - *td_desc_file:* the path of csv file which to define the trace command.  

    Usage Example:
    | open_trace_session | 135.251.200.170 | 
    | open_trace_session | 135.251.200.170 | 23 | shell | nt | octopus | /tmp/td.log | /tmp/td_cmd.csv | 
     
    """

    keyword_name = "open_trace_session"

    global TRACE_SESSION
    try:
        TRACE_SESSION = listener_td.listener_td \
        (ip,port,username,password,type,log,td_desc_file)
        logger.info("TRACE_SESSION:%s" % TRACE_SESSION)
        logger.info("open trace session succesfully!")
    except Exception as inst:
        raise AssertionError("%s:%s-> fail to init listener_td, exception: %s" \
        % (__name__,keyword_name,str(inst)))

def close_trace_session () :
    """
    close trace session
    
    Usage Example:
    | close_trace_session | 

    """
    keyword_name = "close_trace_session"
    global TRACE_SESSION
    TRACE_SESSION.close_trace()
    logger.info("close trace session succesfully!")

def send_trace_cmd (name,name_type,td_type) :
    """
    send trace command 
    - *name:* test case name, suite name or keyword name
    - *name_type:*  TEST, SUITE or KEYWORD
    - *td_type:* START or END
    - *RETURN_VALUE:* the reply info of trace command 

    Usage Example:
    | ${trace_info} | send_trace_cmd | case1 | TEST | START | 
    """

    keyword_name = "send_trace_cmd"
    trace_info = ""
    TRACE_SESSION.capture_td_log_by_normal()
    trace_info = TRACE_SESSION.capture_td_log_by_cmd(name,name_type,td_type)
    return trace_info

