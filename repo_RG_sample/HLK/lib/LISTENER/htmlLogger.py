
INDEX = 0

def get_index():
    global INDEX
    INDEX += 1
    return INDEX

def generate_html_header(args):
    keyword    = "generate_html_header"
    header_tag = '<html lang="en" xmlns="http://www.w3.org/1999/xhtml">\n\
    <head>\n\
          <style type="text/css">\n\
                 h1.intro {color:blue;}\n\
                 p.log    {color:black;}\n\
                 p.error  {color:red;}\n\
                 p.warn   {color:orange;}\n\
                 p.show   {color:blue;}\n\
                 p.import {color:blue;}\n\
                 div.SUITE  {color:black;}\n\
          </style>\n\
          <title>MyPage</title>\n\
    </head>\n\
    \n\
    <script>\n\
    function expend_all() {\n\
        var i = 1; \n\
        var divId = "div" + i; \n\
        var hId   = "h" + i; \n\
        while(document.getElementById(divId)) { \n\
            document.getElementById(divId).style.display=\'block\'; \n\
            titleStr = String(document.getElementById(hId).innerHTML); \n\
            document.getElementById(hId).innerHTML = titleStr.replace(\'-\',\'+\'); \n\
            i = i + 1; \n\
            divId = "div" + i; \n\
            hId   = "h" + i; \n\
        } \n\
    }\n\
    function close_all()  {\n\
       var i = 1; \n\
        var divId = "div" + i; \n\
        var hId   = "h" + i; \n\
        while(document.getElementById(divId)) { \n\
            document.getElementById(divId).style.display=\'none\'; \n\
            titleStr = String(document.getElementById(hId).innerHTML); \n\
            document.getElementById(hId).innerHTML = titleStr.replace(\'+\',\'-\'); \n\
            i = i + 1; \n\
            divId = "div" + i; \n\
            hId   = "h" + i; \n\
        } \n\
    }\n\
    function expend_elem(divId,hId,content){\n\
    var show=document.getElementById(divId).style.display;\n\
    if(show==\'none\')\n\
    {\n\
      document.getElementById(divId).style.display=\'block\';\n\
      document.getElementById(hId).innerHTML= "+  " + content\n\
    }\n\
    else{\n\
      document.getElementById(divId).style.display=\'none\';\n\
      document.getElementById(hId).innerHTML= "-  " + content\n\
    }\n\
    }\n\
    </script>\n\
    \n\
    <body><h1 align="center">TRACE DEBUG LOG</h1>\n\
    <button onclick="expend_all()">Expend All</button>\n\
    <button onclick="close_all()">Close All</button>'

    return header_tag

def generate_html_footer(args):
    keyword    = "generate_html_footer"
    footer_tag = '</body>\n</html>'
    return footer_tag

def generate_html_suite_header(args):
    keyword  = "generate_html_suite_header" 
    log_tag  = ''

    #define dict_args
    dict_args = {}
    str_dict_define = 'dict_args = %s' % args 
    exec(str_dict_define)

    try : 
        index = get_index()
        type  = dict_args['type'].upper()
        name  = dict_args['name']
        time  = dict_args['time']
        if dict_args.has_key('time') and dict_args.has_key('name') and dict_args.has_key('type') :
            log_tag = '\n\
        <div class="log" onclick="expend_elem(\'div%s\',\'h%s\',\'%s SUITE %s\');">\n\
            <h1 id="h%s">+  %s SUITE %s</h1>\n\
        </div>\n\
        <div id="div%s" stype="display:block">\n\
            <p class=\'info\' align=\'left\'>%s</p>' \
        % (index,index,type,name,index,type,name,index,time)
        return log_tag
    except Exception as inst :
        raise AssertionError("%s->%s: Failed to generate suite html header, exception:%s" % (__name__,keyword,inst))    

def generate_html_suite_footer(args):
    keyword = "generate_html_suite_header"
    log_tag = '        </div>'
    return log_tag

def generate_html_test_header(args):
    keyword  = "generate_html_test_header"
    log_tag  = ''

    #define dict_args
    dict_args = {}
    str_dict_define = 'dict_args = %s' % args
    exec(str_dict_define)

    try :
        index = get_index()
        type  = dict_args['type'].upper()
        name  = dict_args['name']
        time  = dict_args['time']
        if dict_args.has_key('time') and dict_args.has_key('name') and dict_args.has_key('type') :
            log_tag = '\n\
        <div class="log" onclick="expend_elem(\'div%s\',\'h%s\',\'%s TEST %s\');">\n\
            <h2 id="h%s">+  %s TEST %s</h2>\n\
        </div>\n\
        <div id="div%s" stype="display:block">\n\
            <p class=\'info\' align=\'left\'>%s</p>' \
        % (index,index,type,name,index,type,name,index,time)
        return log_tag
    except Exception as inst :
        raise AssertionError("%s->%s: Failed to generate suite html header, exception:%s" % (__name__,keyword,inst))

def generate_html_test_footer(args):
    keyword = "generate_html_test_header"
    log_tag = '        </div>'
    return log_tag

def generate_html_keyword_header(args):
    keyword  = "generate_html_keyword_header"
    log_tag  = ''

    #define dict_args
    dict_args = {}
    str_dict_define = 'dict_args = %s' % args
    exec(str_dict_define)

    try :
        index = get_index()
        type  = dict_args['type'].upper()
        name  = dict_args['name']
        time  = dict_args['time']
        if dict_args.has_key('time') and dict_args.has_key('name') and dict_args.has_key('type') :
            log_tag = '\n\
        <div class="log" onclick="expend_elem(\'div%s\',\'h%s\',\'%s KEYWORD %s\');">\n\
            <h3 id="h%s">+  %s KEYWORD %s</h3>\n\
        </div>\n\
        <div id="div%s" stype="display:block">\n\
            <p class=\'info\' align=\'left\'>%s</p>' \
        % (index,index,type,name,index,type,name,index,time)
        return log_tag
    except Exception as inst :
        raise AssertionError("%s->%s: Failed to generate suite html header, exception:%s" % (__name__,keyword,inst))

def generate_html_keyword_footer(args):
    keyword = "generate_html_keyword_header"
    log_tag = '        </div>'
    return log_tag

def generate_html_message(args):
    keyword = "generate_html_message"

    try :
        log_tag = '' 
        if args.has_key('type') and args.has_key('info') : 
            log_tag = '            <p class="%s" align="left">%s</p>' % (args['type'],args['info'])
        return log_tag
    except Exception as inst:
        raise AssertionError("%s->%s, Failed to generate html message, exception:%s" % (__name__,keyword,inst))
