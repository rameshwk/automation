import re
import commands

def generate_txt_header(args) :
    keyword  = "generate_txt_header"
    
    header_tag = '################################################\n\
DEBUG FOR NT AT "' + commands.getoutput('date') +'" \n\
################################################\n'

    return header_tag 

def generate_txt_footer(args) :
    keyword = "generate_txt_footer"

    footer_tag ='TRACE: CLOSE INTERFACE\n\
################################################\n\
DEBUG END AT "' + commands.getoutput('date') + '" \n\
################################################\n'

    return footer_tag

def generate_txt_suite_header(args) :
    keyword  = "generate_txt_header"

    header_tag = '################################################\n\
   %s SUITE %s " %s " \n\
################################################\n' % (args['type'].upper(), args['name'],args['time'])

    return header_tag

def generate_txt_suite_footer(args) :
    keyword  = "generate_txt_suite_header"
    return None

def generate_txt_test_header(args) :
    keyword  = "generate_txt_test_header"

    header_tag = '################################################\n\
   %s TEST %s " %s " \n\
################################################\n' % (args['type'].upper(), args['name'],args['time'])

    return header_tag

def generate_txt_test_footer(args) :
    keyword  = "generate_txt_test_footer"
    return None

def generate_txt_keyword_header(args) :
    keyword  = "generate_txt_keyword_header"

    header_tag = '################################################\n\
   %s KEYWORD %s " %s " \n\
################################################\n' % (args['type'].upper(), args['name'],args['time'])

    return header_tag

def generate_txt_keyword_footer(args) :
    keyword  = "generate_txt_keyword_footer"
    return None

def generate_txt_message(args): 
    keyword = "generate_txt_message"
    log_tag = ''

    try :
        if args.has_key('type') and args.has_key('info') :
            if args['type'].lower() == 'log' or args['type'].lower() == 'show' :  
                log_tag = '%s' % (args['info'])
            else: 
                log_tag = '%s: %s' % (args['type'].upper(),args['info'])

    except Exception as inst:
        raise AssertionError("%s->%s,Failed to generate txt message, exception:%s" % (__name__,keyword,inst))

    return log_tag
