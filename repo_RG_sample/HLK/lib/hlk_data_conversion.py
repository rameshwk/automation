def list_to_dict(args):
    """
        convert a list like ["'a'='aaa'", "'b'='bbb'"] to a dictionary
    """
    x = []
    y = []
    for arg in args:
        arg = arg.encode("ascii")
        arg_list = arg.split('=',1) # first "=" is the seperation sign       
        key=arg_list[0].strip().strip('\'').strip('"')
        try:
            val=arg_list[1].strip('\'').strip('"').strip()
        except IndexError:
            val = "NONE"
        x.append(key)
        y.append(val)

    outdict = dict(zip(x, y)) 
    return outdict    

def string_to_dict(original_string,seperator=','):
    """
        convert a string like "'a'='aaa', 'b'='bbb'" to a dictionary
    """
    out_dictionary = {}
    original_list = original_string.split(seperator)
    for element in original_list:
        element =element.encode("ascii")
        key,value = element.split("=",1)
        key = key.strip()
        value = value.strip()
        if len(value) > 0 :
            out_dictionary[key]=value
        else :
            out_dictionary[key]="NONE"
 
    return out_dictionary
    
def tuple_to_dict(args):
	import re
	list_options = []
	#print " TUPLE DATA",args
	tempdata = str(args)
	sub_val = re.sub('\[','',tempdata.strip())
	sub_val = re.sub('\]','',sub_val)
	sub_val = re.sub('\)','',sub_val)
	converted_val  = re.sub('\(','',sub_val)
	#print " CONVERTED DATA",converted_val
	for item in converted_val.split(',') :
            try :
	    	item = item.strip()
	    	matched = re.search("\s*u'(.*)'", item)
		if matched:
			if matched.group():
                		item = matched.group(1)
            except Exception as inst :
               	pass
            finally :
	    	if item:    
               		list_options.append(item)    
	dict_output = list_to_dict(list_options)
	#print " List output",list_options
	#print " DICT OUT ",dict_output
    	return dict_output 
    	
def create_list_of_list(*args):
        import re
	inner_list=[]
	outer_list=[]
	for arg in args:
		#print '#'+arg+'##'
                pattern=re.compile('\\\\')
    		match=pattern.match(arg)
		if match is not None:
			outer_list.append(inner_list)
 			inner_list=[]
		else:
			inner_list.append(str(arg))
  	return list(outer_list)    	   

def Upper_To_Lower(args):
	return args.lower()

def Lower_To_Upper(args):
	return args.upper()
