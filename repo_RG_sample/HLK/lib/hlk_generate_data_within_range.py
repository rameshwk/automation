import re
import random
from robot.api import logger

def generate_random_mac_within_range(start,end):
	""" Generate random mac within range

     	Example:
	generate_random_mac_within_range '000000000001','000000001001'
	
			Return A mac within range. E.g:000000000101 
	"""
	h_mac_start = str('0x')+str(start)
	h_mac_end = str('0x')+str(end)
	i_mac_start = eval(h_mac_start)
	i_mac_end = eval(h_mac_end)
	i_random = random.randint(i_mac_start,i_mac_end)
	s_random = str(hex(i_random))
	s_random = str.replace(s_random,'0x','') 
	s_result = str.zfill(s_random,12)
	logger.debug("RANDOM MAC : %s " %s_result)
	return s_result


def generate_random_standard_mac_within_range(start,end):
        """ Generate random mac within range

        Example:
        generate_random_mac_within_range '000000000001','000000001001'
        
                        Return A mac within range. E.g:000000000101 
        """
        h_mac_start = str('0x')+str(start)
        h_mac_end = str('0x')+str(end)
        i_mac_start = eval(h_mac_start)
        i_mac_end = eval(h_mac_end)
        i_random = random.randint(i_mac_start,i_mac_end)
        s_random = str(hex(i_random))
        s_random = str.replace(s_random,'0x','')
        s_result1 = str.zfill(s_random,12)
	b = re.findall(r'.{2}', s_result1)
        s_result =':'.join(b)
        logger.debug("RANDOM MAC : %s " %s_result)
        return s_result



def generate_random_ip_within_range(start,end,step='random'):
	""" Generate random ip within range

     	Example:
	generate_random_ip_within_range '10.0.0.1 10.0.1.0'
	
			Return A ip within range. E.g:10.0.0.3
	"""
	l_start = str(start).split('.')
	l_end = str(end).split('.')
	l_i_start = []
	l_i_end = []	
	for item in l_start:
		tmp = int(item)
		l_i_start.append(tmp)
	for item in l_end:
		tmp = int(item)
		l_i_end.append(tmp)
	start_num = l_i_start[3]+256*l_i_start[2]+65536*l_i_start[1]+16777216*l_i_start[0]
	end_num = l_i_end[3]+256*l_i_end[2]+65536*l_i_end[1]+16777216*l_i_end[0]
	i_range = end_num-start_num
	if re.search("random",str(step)) :
		i_random = random.randint(0,i_range)
	else :
		i_random = int(step)
	i_result = start_num+i_random
	s_result = str(i_result//16777216)+'.'+str(i_result%16777216//65536)+'.'+str(i_result%65536/256)+'.'+str(i_result%256)
	logger.debug("IP : %s " %s_result)
	return s_result


def generate_random_ip_list_within_range(start,end,step='random'):
        ip_list = []
	l_start = str(start).split('.')
        l_end = str(end).split('.')
        l_i_start = []
        l_i_end = []
        for item in l_start:
                tmp = int(item)
                l_i_start.append(tmp)
        for item in l_end:
                tmp = int(item)
                l_i_end.append(tmp)
        start_num = l_i_start[3]+256*l_i_start[2]+65536*l_i_start[1]+16777216*l_i_start[0]
        end_num = l_i_end[3]+256*l_i_end[2]+65536*l_i_end[1]+16777216*l_i_end[0]
        if re.search("random",str(step)) :
                i_random = random.randint(0,i_range)
        else :
                i_random = int(step)
	i_results =  range(start_num, end_num, i_random)
	for i_result in i_results: 
            s_result = str(i_result//16777216)+'.'+str(i_result%16777216//65536)+'.'+str(i_result%65536/256)+'.'+str(i_result%256)
            ip_list.append(s_result)
            logger.debug("IP : %s " %s_result)
        logger.debug("IP List : %s " %s_result)
        return ip_list

def check_dhcp_ip_in_range(start,end,ip):
	""" verify the IP that dhcpclient get should been in the dhcp pool.

     	Example:
	check_dhcp_ip_within_range '10.0.0.1 10.0.1.0 10.0.0.3'
	
			Return A ip within range. E.g:10.0.0.3
	"""
	l_start = str(start).split('.')
	l_end = str(end).split('.')
	ip_cover= str(ip).split('.')

	l_i_start = []
	l_i_end = []
	l_ip_cover = []	
	for item in l_start:
		tmp = int(item)
		l_i_start.append(tmp)
	for item in l_end:
		tmp = int(item)
		l_i_end.append(tmp)
	for item in ip_cover:
		tmp=int(item)
		l_ip_cover.append(tmp)
		 
	start_num = l_i_start[3]+256*l_i_start[2]+65536*l_i_start[1]+16777216*l_i_start[0]
	end_num = l_i_end[3]+256*l_i_end[2]+65536*l_i_end[1]+16777216*l_i_end[0]
	ip_num = l_ip_cover[3]+256*l_ip_cover[2]+65536*l_ip_cover[1]+16777216*l_ip_cover[0]
	print "start_num is %s" %start_num
	print "end_num is %s" %end_num
	print "ip_num is %s" %ip_num
	if start_num <= ip_num and ip_num<=end_num:
		logger.debug("LAN Client can get IP %s from DHCP Pool."%ip)
	else:
		logger.debug("LAN Client can't get IP from DHCP Pool")
			

def get_size_of_dhcp_pool(start,end):
        """ return the size of dhcp pool of ont.

        Example:
        check_dhcp_ip_within_range '10.0.0.1 10.0.1.0 10.0.0.3'
        return size of dhcp pool: 3
          
        """
        l_start = str(start).split('.')
        l_end = str(end).split('.')
        l_i_start = []
        l_i_end = []
        
	for item in l_start:
                tmp = int(item)
                l_i_start.append(tmp)
        for item in l_end:
                tmp = int(item)
                l_i_end.append(tmp)

        start_num = l_i_start[3]+256*l_i_start[2]+65536*l_i_start[1]+16777216*l_i_start[0]
        end_num = l_i_end[3]+256*l_i_end[2]+65536*l_i_end[1]+16777216*l_i_end[0]
        i_range = end_num-start_num + 1
	logger.debug("Size of the DHCP Pool is %s."%i_range)
	return i_range

def generate_random_vlan_within_range(start,end,step='random'):
	""" Generate random vlan within range

     	Example:
	generate_random_vlan_within_range '101 110'
	
			Return A vlan within range. E.g:107
	"""
	i_range = int(end)-int(start)
	if re.search("random",str(step)) :
		i_random = random.randint(0,i_range)
	else :
		i_random = int(step)
	i_result = int(start)+i_random
	logger.debug("VLAN : %s " %i_result)
	return i_result

def ipv6_to_dec(ipv6):
    ipv6_list=ipv6.split(":")
    ipv6_sum=0
    ipv6_new_list=[]
    for i,ip in enumerate(ipv6_list):
        n=6-i
        tmp_0=n*"0000"
        ip=ip+tmp_0
        ip_dec=int(str(ip),16)
        ipv6_new_list.append(ip_dec)
        ipv6_sum+=ip_dec
    return ipv6_sum

def ipv6_random(ip_start,ip_end):
    start=ipv6_to_dec(ip_start)
    end=ipv6_to_dec(ip_end)
    num=random.randint(start,end)
    return num

def dec_to_ipv6(ipv6_dec):
    ipv6_hex="%x" % ipv6_dec
    num=len(ipv6_hex)
    tmp_0=(32-num)*"0"
    ipv6_hex_new=tmp_0+ipv6_hex
    return ipv6_hex_new

def f(string,width):
    return [string[x:x+width] for x in range(0,len(string),width)]


def generate_random_ipv6_within_range(start,end):
    """ Generate random ipv6 within range

     	Example:
	generate_random_ipv6_within_range '0000:0000:0000:0000:CCCC:BBBB:AAAA:0000','0000:0000:0000:0000:CCCC:BBBB:AAAA:FFFF'
	
			Return A ipv6 within range. E.g:0000:0000:0000:0000:0000:cccc:bbbb:ddf2
	"""
    ipv6_dec=ipv6_random(start,end)
    ipv6_new=dec_to_ipv6(ipv6_dec)
    a=ipv6_new
    a_list=f(a,4)
    a_new=":".join(a_list)
    return a_new


def generate_random_unmac(start,end):
    mac_1=generate_random_mac_within_range(start,end)
    mac_new=changeMacFormatColon(mac_1,2)
    return mac_new

def changeMacFormatColon(mac_orig):
    mac_list=f(mac_orig,2)
    mac_new=":".join(mac_list)
    return mac_new    


def generate_random_ipv6LLA_within_range(start,end):
    """ Generate random ipv6 within range

     	Example:
	generate_random_ipv6_within_range '0000:0000:0000:0000:CCCC:BBBB:AAAA:0000','0000:0000:0000:0000:CCCC:BBBB:AAAA:FFFF'
	
			Return A ipv6 within range. E.g:fe80:0000:0000:0000:0000:cccc:bbbb:ddf2
	"""
    ipv6_dec=ipv6_random(start,end)
    ipv6_new=dec_to_ipv6(ipv6_dec)
    a=ipv6_new
    a_list=f(a,4)
    a_last=a_list[1:]
    a_listnew=['fe80']+a_last
    a_new=":".join(a_listnew)
    return a_new

def changeHexFromat(dec):
    """ change dec to hex
    Example:
	changeHexFromat eg 111
    Return hex eg 006f
	"""
    hex="%x" % dec
    num=len(hex)
    tmp_0=(4-num)*"0"
    hex_new=tmp_0+hex
    return hex_new


def generate_random_standard_mac_lists(numbers):
    Maclists=[]
    numbers = int(numbers) + 1
    for i in range(1,numbers):
        Maclist=[]
	MAC_START=random.sample("00000000",6)
	print MAC_START
	for j in range(4,7):
	    RANDSTR="".join(random.sample("0123456789abcde",2))
	    Maclist.append(RANDSTR)
	    MAC= MAC_START + Maclist
	RANDMAC="".join(MAC)
	b = re.findall(r'.{2}', RANDMAC)
	standard_mac =':'.join(b)
    	Maclists.append(standard_mac)
    print Maclists
    logger.debug("Maclists is : %s " %Maclists)
    return Maclists


def generate_random_mac_lists(numbers):
    Maclists=[]
    numbers = int(numbers) + 1
    for i in range(1,numbers):
        Maclist=[]
        MAC_START=random.sample("00000000",6)
        print MAC_START
        for j in range(4,7):
            RANDSTR="".join(random.sample("0123456789abcde",2))
            Maclist.append(RANDSTR)
            MAC= MAC_START + Maclist
        RANDMAC="".join(MAC)
        Maclists.append(RANDMAC)
    print Maclists
    logger.debug("Maclists is : %s " %Maclists)
    return Maclists


def modify_network_segment(ip,location,value):
        l_ip = str(ip).split('.')
        print l_ip
	l_i_ip=[]
	for item in l_ip:
	    tmp = str(item)
            l_i_ip.append(tmp)
        print l_i_ip 
	l_i_ip[int(location)-1] = str(value)
	print l_i_ip	
	new_ip = '.'.join(l_i_ip)
	logger.debug("New IP is : %s " %new_ip)
	return new_ip
