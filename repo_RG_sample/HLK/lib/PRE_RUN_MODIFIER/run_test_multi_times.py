"""
Pre-run modifier that selects ATC for multiple execution.
# execute test1 3 times
robot --prerunmodifier /repo/auto/robot/LIBS/PRE_RUN_MODIFIER/run_test_multi_times.py:3:test1 tests.robot
robot --prerunmodifier /repo/auto/robot/LIBS/PRE_RUN_MODIFIER/run_test_multi_times.py:3:test1,test2 tests.robot
robot --prerunmodifier /repo/auto/robot/LIBS/PRE_RUN_MODIFIER/run_test_multi_times.py:3 tests.robot


"""

from robot.api import SuiteVisitor
from copy import deepcopy

class run_test_multi_times (SuiteVisitor):

    def __init__(self, times,test="All"):
        self.x = int(times)
        self.test = test

        print "Repeat test '%s' %s times " %( test, str(times))

    def start_suite(self, suite):
        """Modify suite's tests to contain test x times."""
        #suite.tests = suite.tests[self.start::self.x]

        for t in suite.tests :
            if self.test == "All" or t.name in self.test.split(",") :
                index = suite.tests.index (t)
                for i in range (1,self.x) :
                    t_copy = deepcopy (t)
                    t_copy.name = t.name + "_" + str(i)
                    suite.tests.insert(index+i,t_copy)


    def end_suite(self, suite):
        """Remove suites that are empty after removing tests."""
        suite.suites = [s for s in suite.suites if s.test_count > 0]

    def visit_test(self, test):
        """Avoid visiting tests and their keywords to save a little time."""
        pass

