import os, sys, re
from robot.utils import Matcher
from robot.api import SuiteVisitor
try :
    import data_translation
except Exception as inst:
    sys.path.append(os.environ ['ROBOTREPO']+'/LIBS/DATA_TRANSLATION')
    import data_translation

def _get_table_columns (table_name,search_condition=None):
    """
    get specific columns with search condition and return matched info
    - *table_name:* name of the table
    - *search_condition:* a string with format "key1=value1,key2=value2", default is None
    """

    try :
        totalTableList = data_translation.get_table_lines(table_name,search_condition)
        if totalTableList == []:
            #print "WARNING: Fail to get the table list for table: %s" % table_name
            return {}
    except Exception as inst:
        print "can not get table name %s, exception: %s" % (table_name,inst)
        return {}

    dic = {}  
    columns = totalTableList[0].keys()
    for column in columns:
        value = []
        for item in totalTableList:
            if column in item.keys():
                value.append(item[column])
            else:
                value.append(' ')
        dic[column]=value
    return dic

def _get_matched_tablenames(duts_list,configFile):
    """
    Select all matched table names according to the test env, then sorted them from high to low priority
    history:
    Jia Zhuang created Jan 4th 2018
    """
    try:
        dic = {}
        matched_list = []
        tableNames=data_translation.get_table_names(configFile)
        for name in tableNames:
        # Combined table name is splited with sign & 
            for item in name.split('&'):
                if item not in duts_list:
                    break
            else:
                deep = len(re.findall('&',name))
                name={str(deep):name}
                matched_list.append(name)
        for item in matched_list:
            for k,v in item.items():
                dic.setdefault(k,[]).append(v)
        # Get a sorted list which contains serials of table names with different priorities from low to high
        return [dic[k] for k in sorted(dic.keys())]
    except Exception as inst:
        raise AssertionError('Fail to get matched table names by exception: %s' % f) 


def capability_driven_exclude(duts_list,release,configFile):
    """
    get the exclude tag for capability driven
    history:
    Jia Zhuang updated  Jan 4th 2018
    """
    try:
        ran_rev={}
        ran_rev_list=[]
        exclude_tags=[]
        for dut in duts_list:
            ran_rev = _get_table_columns(dut)
            if ran_rev != {}:
                ran_rev_list = ran_rev['Release']
                for ran_ver in ran_rev_list:
                    rev_min = ran_ver.split('-')[0].replace('.','',1)
                    rev_max = ran_ver.split('-')[1].replace('.','',1)
                    #print "Min rev: %s; Max rev: %s" % (rev_min, rev_max)
                    if float(release.replace('.','',1)) >= float(rev_min) and float(release.replace('.','',1)) <= float(rev_max):
                        rev_info=data_translation.get_table_line(dut,'Release=%s' %ran_ver)
                        if rev_info.has_key('ExcludeTag'):
                            rev_tag=rev_info['ExcludeTag'].split(',')
                            exclude_tags.extend(rev_tag)
        return exclude_tags
        
    except Exception as f:
        raise AssertionError('Fail to parse the setup File by exception: %s' % f) 

def capability_driven_include(duts_list,release,configFile):
    """
    get the include tag for capability driven
    history:
    jia zhuang  created  Jul 24th 2017
    """
    try:
        ran_rev={}
        ran_rev_list=[]
        include_tags=[]

        for dut in duts_list:
            ran_rev = _get_table_columns(dut)
            if ran_rev != {}:
                ran_rev_list = ran_rev['Release']
                for ran_ver in ran_rev_list:
                    rev_min = ran_ver.split('-')[0].replace('.','',1)
                    rev_max = ran_ver.split('-')[1].replace('.','',1)
                    if float(release.replace('.','',1)) >= float(rev_min) and float(release.replace('.','',1)) <= float(rev_max):
                        rev_info=data_translation.get_table_line(dut,'Release=%s' %ran_ver)
                        if rev_info.has_key('IncludeTag'):
                            rev_tag=rev_info['IncludeTag'].split(',')
                            #print "Searched rev_tag line: %s" % rev_tag
                            include_tags.extend(rev_tag)
        return include_tags

    except Exception as f:
        raise AssertionError('Fail to parse the setup File by exception: %s' % f)

def include_tags_filter(include_list,exclude_list):
    """
    According to the Buckets Effect, remove the include tags that be found in the list of exclude tags
    Jia Zhuang created on Jan 4th 2018
    """
    try:
        if include_list:
            for i in include_list:
                tag = i.replace('capab_dep_','capab_')
                if tag in exclude_list:
                    include_list.remove(i)
            return include_list
        else:
            return []
    except Exception as inst:
        raise AssertionError('Fail to filter include tags by exception: %s' % inst)


class capability_driven(SuiteVisitor):
    """
    Usage:
    capability_driven:LT_XXXX,NT_YYYY,SHELF_ZZZZ:$VersionNum:$PATH_of_capability.yaml
    Jia zhuang updated on Jan 8th 2018
    """
    try:
        def __init__(self,duts,release,file=os.environ['ROBOTREPO']+'/SW_DESCRIPTION/GPON/capability.yaml'):
            self.duts = duts.split(',')
            self.release = str(release)
            self.file = file
            data_translation.create_global_tables(self.file)
            self.tableNames = _get_matched_tablenames(self.duts, self.file)

        def _level_filter(self,suite,extags,intags):
            """
            Filter for include tags that be found in the same deep of exclude tags.
            """
            intests = []
            intags = include_tags_filter(intags,extags)
            # According to the Buckets Effect, remove the include cases that be found in the list of exclude cases
            for t in suite.tests:
                if self._is_included(t,intags) and t not in intests:
                    mark = True
                    for tag in extags:
                        if tag in t.tags:
                            mark = False 
                    if mark:
                        intests.append(t)
            return intests


        def _case_filter(self,suite,prio,deep):
            """
            Cases Filter according to different priorities.
            """
            extests = []
            intests = []
            if len(suite.tests) != 0:
                extags = capability_driven_exclude(self.tableNames[prio],self.release,self.file)
                intags = capability_driven_include(self.tableNames[prio],self.release,self.file)
                ##print "EX_Tags: %s" % extags
                ##print "IN_Tags: %s" % intags
                # prio==0 means no combined tableNames matched from config file.
                if prio == 0:
                    extests = [ t for t in suite.tests if self._is_excluded(t,extags) and t not in extests ]
                    intests = self._level_filter(suite,extags,intags)
                # Recursion function is used here for complex conditions, more deeper more higher priority.
                else:
                    # Filter for exclude cases that be found in the next deep(higher priority) of include cases.
                    if prio+1 < deep:
                        intests_next = self._case_filter(suite,prio+1,deep)[1]
                        extests = [ t for t in suite.tests if self._is_excluded(t,extags) ]
                        for case in extests:
                            if case in intests_next:
                                ##print "Case will be removed from exclude list: %s" % case
                                extests.remove(case)
                    else:
                        extests = [ t for t in suite.tests if self._is_excluded(t,extags) ]
                    intests = self._level_filter(suite,extags,intags)
                ##print "Ex_Cases for Prio %d: %s" % (prio,extests)
                ##print "In_Cases for Prio %d: %s" % (prio,intests)
            return (extests,intests)


        def start_suite(self, suite):
            exCases = []
            inCases = []
            deep = len(self.tableNames)
            if deep == 0:
            # No matched tables found for this condition, capability driven is skipped
                pass
            else:
                # priority is sorted from low to high, cases with low priority will be handled first
                for priority in sorted(range(deep),reverse=False):
                    (extests,intests) = self._case_filter(suite,priority,deep)
                    for e in extests:
                        if e not in exCases:
                            exCases.append(e)
                    for i in intests:
                        if i not in inCases:
                            inCases.append(i)
                ##if len(suite.tests) != 0:
                ##    print "Cases will be excluded: %s" % exCases
                ##    print "Cases will be included: %s" % inCases
                suite.tests = [ t for t in suite.tests if t not in exCases or t in inCases ]

        def _is_excluded(self, test,extags):
            if len(extags) != 0:
                for single_tag in extags:
                    matcher = Matcher(single_tag)
                    for test_tag in test.tags:
                        if str(test_tag).startswith('capab_dep_') or matcher.match(test_tag):
                            return True
                return False
            else:
                for test_tag in test.tags:
                    if str(test_tag).startswith('capab_dep_'):
                        return True
                    else:
                        return False

        def _is_included(self, test,intags):
            if len(intags) != 0:
                for single_tag in intags:
                    matcher = Matcher(single_tag)
                    for test_tag in test.tags:
                        if matcher.match(test_tag):
                            return True
                return False
            else:
                return False 

        def end_suite(self, suite):
            """Remove suites that are empty after removing tests."""
            suite.suites = [s for s in suite.suites if s.test_count > 0]

        def visit_test(self, test):
            """Avoid visiting tests and their keywords to save a little time."""
            pass

    except Exception as f:
        print "Debug info for exception: %s" % f
