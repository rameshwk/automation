"""
Pre-run modifier that excludes tests by their name.

Tests to exclude are specified by using a pattern that is both case and space
insensitive and supports '*' (match anything) and '?' (match single character)
as wildcards.

# Exclude all tests ending with 'something' or atc_name1
robot --prerunmodifier /repo/auto/robot/LIBS/PRE_RUN_MODIFIER/exclude_tests.py:something:atc_name1 tests.robot

"""

from robot.api import SuiteVisitor
#from robot.utils import Matcher
from robot.utils import MultiMatcher

class exclude_tests (SuiteVisitor):

    def __init__(self, *pattern):
        #self.matcher = Matcher(pattern)
        self.matchers = MultiMatcher(pattern)

    def start_suite(self, suite):
        """
        Remove tests that match the given pattern.

        visit objects : 
            suite.parent,suite.doc,suite.name,suite.suites,suite.tests,suite.keywords,
            suite.resource,suite.resource.variables
            for i in suite.resource.variables :
                print i.name
                print i.value
        """

        suite.tests = [t for t in suite.tests if not self._is_excluded(t)]

    def _is_excluded(self, test):    
        return self.matchers.match(test.name) or self.matchers.match(test.longname)

    def end_suite(self, suite):
        """Remove suites that are empty after removing tests."""
        suite.suites = [s for s in suite.suites if s.test_count > 0]

    def visit_test(self, test):
        """Avoid visiting tests and their keywords to save a little time."""
        pass

