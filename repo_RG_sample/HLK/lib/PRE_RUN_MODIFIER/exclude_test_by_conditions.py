
m robot.utils import Matcher
from robot.api import SuiteVisitor
import os,re

def sdolt_exclude (*param):
    onos_version = os.environ['ONOS_VERSION']
    fobj = open(onos_version)
    all_content = fobj.read()
    result = re.search('(\S+onos\S+)\s+',all_content)
    onos = result.group(1)
    fobj.close()
 
    if 'nokia' in onos:
        return ['dhcp*']
    else:
        return ['dhcp_l2']    

class ExcludeTestByTag(SuiteVisitor):

    def __init__(self,proc,*param):
        proc = proc + str(param)
        c = compile(proc,'','eval')
        self.exclude_tags = eval(c)    

    def start_suite(self, suite):
        suite.tests = [t for t in suite.tests if not self._is_excluded(t)]
                    
    def _is_excluded(self, test):
        for single_tag in self.exclude_tags:
            matcher = Matcher(single_tag)
            for test_tag in test.tags:
                if matcher.match(test_tag):
                    return True
        return False

    def end_suite(self, suite):
        """Remove suites that are empty after removing tests."""
        suite.suites = [s for s in suite.suites if s.test_count > 0]

    def visit_test(self, test):
        """Avoid visiting tests and their keywords to save a little time."""
        pass

