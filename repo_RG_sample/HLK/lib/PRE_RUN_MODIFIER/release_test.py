import os, sys
from robot.utils import Matcher
from robot.api import SuiteVisitor

def create_pybot_exclude_option_by_release (release):
    """
    create release related tag for exclude in pybot command
    
    history:
    chunyagu  created  Jan 20th 2014
    """

    release_list = release.lstrip("R").lstrip('r').split(".")

    if len(release_list) != 2 and len(release_list) != 3 :
        inst = "Error release format:" + str(release)+ \
               ", it should looks like : R4.6.01 or R5.1"
        raise AssertionError("%s:%s -> %s" \
        % (__name__,inspect.stack()[0][3],inst))
    if len(release_list) == 2 :
        release_list.append("00")

    exclude_release = []

    for i in range(10) :
        if i > int(release_list[2]) and i < 4 :
            exclude_release.append ("min_rel_r"+release_list[0]+"."+release_list[1]+".0"+str(i))
        if i > int(release_list[1]):
            exclude_release.append ("min_rel_r"+release_list[0]+"."+str(i)+"*")
        if i > int(release_list[0]) and i < 7 :
            exclude_release.append ("min_rel_r"+str(i)+"."+"*")
    exclude_release.append("min_rel_r9.9")

    for i in range(10) :
        if 0 < i < int(release_list[2]) :
            exclude_release.append ("max_rel_r"+release_list[0]+"."+release_list[1]+".0"+str(i))
        if i < int(release_list[1]) :
            exclude_release.append ("max_rel_r"+release_list[0]+"."+str(i)+"*")
        if i < int(release_list[0]) and i > 1:
            exclude_release.append ("max_rel_r"+str(i)+"."+"*")
    if  release_list[2] != "00" :
         exclude_release.append ("max_rel_r"+release_list[0]+"."+release_list[1])

    return ",".join(exclude_release)

class release_test(SuiteVisitor):
    try:
        def __init__(self, release):
            self.release = release
            self.exclude_tags= create_pybot_exclude_option_by_release(self.release).split(',')
            print "Exclude tags: %s" % self.exclude_tags

        def start_suite(self, suite):
            suite.tests = [t for t in suite.tests if not self._is_excluded(t)]
                    
        def _is_excluded(self, test):
            for single_tag in self.exclude_tags:
                matcher = Matcher(single_tag)
                for test_tag in test.tags:
                    if matcher.match(test_tag):
                        return True
            return False

        def end_suite(self, suite):
            """Remove suites that are empty after removing tests."""
            suite.suites = [s for s in suite.suites if s.test_count > 0]

        def visit_test(self, test):
            """Avoid visiting tests and their keywords to save a little time."""
            pass

    except Exception as f:
        print "Debug info for exception: %s" % f
