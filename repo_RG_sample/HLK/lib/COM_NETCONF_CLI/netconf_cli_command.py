import os,sys,time,re
from robot.api import logger

try :
    import pexpect
except ImportError :
    pexpect_path = os.environ.get('ROBOTREPO')+'/TOOLS/pexpect-2.3_1'
    sys.path.append(pexpect_path)
    import pexpect
    
#ssh_path = os.environ.get('ROBOTREPO')+'/LIBS/COM_SSH'
#if ssh_path not in sys.path :
#    sys.path.append(ssh_path)
# import com_ssh from the same directory 
from com_ssh import com_ssh
        
NETCONF_CLI_SESSION = {}

class ssh_netconf_cli (com_ssh):
    def __init__(self,ip="127.0.0.1",port="22",username="admin",password="admin",prompt='*.#',timeout=10,init_command=None,command_prompt=None,new_password='Netconf#150'):
        com_ssh.__init__(self,ip,port,username,password,prompt,timeout,new_password)
        if init_command :
            self.init_command = init_command.encode("ascii")
        else :
            self.init_command = None
        if command_prompt :
            self.command_prompt = command_prompt.encode("ascii")
        else :
            self.command_prompt = None
        
        self.new_password     = new_password

    def open_ssh(self,init_command=None,command_prompt=None):
        keyword_name = 'open_ssh'
        iretry_count = 3
        success_init = False
        i = 0
        while (i < iretry_count) :
            i += 1
            try : 
                self._init_ssh()
            except Exception as inst:
                logger.debug("%s:%s-> open ssh failed for the %sth time" % (__name__,keyword_name,str(i)))
                continue
            else :
                success_init = True
                break

        if not success_init :
            raise AssertionError ("%s:%s-> open ssh failed: %s" \
            % (__name__,keyword_name,inst))  
    
        if init_command :
            self.init_command = init_command.encode("ascii")
            self.interface.sendline(self.init_command)
            
            if  not command_prompt :
                self.command_prompt = self.prompt
            else :   
                self.command_prompt = command_prompt.encode("ascii")
            try :      
                expect_value = self.interface.expect([self.command_prompt,\
                pexpect.EOF, pexpect.TIMEOUT],10)
            except Exception as inst:
                logger.debug ("fail to get prompt after '%s'" % init_command)
                
            logger.info("SSH REPLY << %s" % self.interface.after.encode())
                         
            
    def send_command(self,command,timeout=None,prompt=None):
        """
        """
        keyword_name = 'send_command'
        command=command.encode("ascii")
        if prompt == None:
            prompt = self.prompt
        if not timeout :
            expect_timeout = self.timeout
        else :
            expect_timeout = timeout
            
        try :
            self.interface.expect(prompt,1)
        except Exception as inst :
            logger.debug("no prompt in buffer before cmd:"+self.interface.before)
        else :
            logger.debug("clean buffer before cmd:"+self.interface.after.encode())
      
        i = 0
        j = 0            
        self.interface.sendline(command) 
        info = ""  
        while (i < 3) :
            expect_value = self.interface.expect\
            ([prompt,".*\(END\).*",".*--More--.*",pexpect.TIMEOUT,pexpect.EOF],int(expect_timeout))
            if expect_value == 0 :
                info = (info + self.interface.after).encode("ascii")
                current_command = re.sub('\s+','',command)
                current_output  = re.sub('\s+|\b','',info)
                if current_command not in current_output :                          
                    logger.debug("unexpected output:\n"+ str(info))
                    logger.debug('<span style="color: #FFFF00">command was not in output,invoke another prompt</span>',html=True)
                    self.interface.sendline("")
                    self.interface.expect([prompt,pexpect.TIMEOUT,pexpect.EOF],5)
                    info = info + str(self.interface.after.encode("ascii"))
                return info
            elif expect_value == 1 :         
                info = info + self.interface.after.encode("ascii")
                self.interface.sendline("\n")
                continue
            elif expect_value == 2 :                              
                info = info + self.interface.after.encode("ascii")
                self.interface.send(" ")
                j +=1
                if  j > 10 :
                    raise AssertionError("%s:%s-> too long output for '%s'" \
                    % (__name__,keyword_name,command)) 
                continue                     
            elif expect_value > 2 :
                logger.debug ("encountered timeout or eof:\n" +str(self.interface.before))
                self.interface.close()
                time.sleep(3)
                info = ""
                self.open_ssh(self.init_command,self.command_prompt)
                self.interface.sendline(command)
                logger.debug ( "re-send command: %s" % command) 
                i +=1
        if i == 3 :
            raise AssertionError("%s:%s -> failed to send '%s' after 3 times try:\n%s" \
            % (__name__,keyword_name,command,self.interface.after))
        return self.interface.after.encode()


def connect_netconf_cli (*session_info,**connect_info) :
    """
    Build up the SSH session for sending netconf cli command 
    - *session_info[0]:* string, default value is "first_netconf_cli_session"
    - *ip:* netconf server ip address
    - *cli_port:* netconf server port
    - *cli_username:* users defined in netconf aaa
    - *cli_password:* password of above user
    - *cli_prompt:*   the last prompt of netconf cli session
    - *RETURN:*   NETCONF_CLI_SESSION[session_name] or raise Exception
    
    usage example
    | connect_netconf_cli | &session_info_dict |
    | connect_netconf_cli | a_netconf_cli_session | ip='127.0.0.1' | cli_port='2024' | cli_username='admin' | cli_password='admin'| cli_prompt='.*#' | 
    """    

    keyword_name = 'connect_netconf_cli' 
    logger.debug ("%s:%s-> %s"  % (__name__,keyword_name,str(session_info)))
    session_name = ''
    try :
        session_name = session_info[0]
    except Exception as inst :
        session_name = "first_netconf_cli_session"
        
    ip               = connect_info.setdefault('ip','127.0.0.1')
    port             = connect_info.setdefault('cli_port','2024')
    username         = connect_info.setdefault('cli_username','admin')
    password         = connect_info.setdefault('cli_password','admin')
    prompt           = connect_info.setdefault('cli_prompt','.*#')
    new_password     = connect_info.setdefault('cli_new_password',password)

    try:
        NETCONF_CLI_SESSION[session_name] = ssh_netconf_cli (ip,port,username,password,prompt,new_password=new_password)
    except Exception as inst:
        raise AssertionError("%s:%s -> fail to init ssh object: %s" \
        % (__name__,keyword_name,inst))    
            
    try:
        NETCONF_CLI_SESSION[session_name].open_ssh ("config",prompt)
    except Exception as inst:
        s = sys.exc_info()
        raise AssertionError("%s:%s-> fail to open ssh session: lineno:%s, %s" \
        % (__name__,keyword_name,s[2].tb_lineno,inst))  
      
	           
    logger.info ("%s:%s -> success to create netconf cli session: '%s'" \
    % (__name__,keyword_name,str(NETCONF_CLI_SESSION)))
    
    return NETCONF_CLI_SESSION[session_name]
    

def disconnect_netconf_cli (session_name="first_netconf_cli_session",comment="disconnect this netconf cli session") :
    """ 
    Disconnect the connected netconf cli session
    - *session_name:* string, default value is "first_netconf_cli_session"
    - *comment:*  describe the purpose of this session
    
    usage example
    | disconnect_netconf_cli | "first_netconf_cli_session" |
    | disconnect_netconf_cli |

    """
    keyword_name = 'disconnect_netconf_cli'
    logger.debug ("%s:%s -> session_name=%s" % (__name__,keyword_name,session_name))
    try:
        NETCONF_CLI_SESSION[session_name].close_ssh()
    except Exception as inst:
        raise AssertionError("%s:%s -> fail to close SSH session\n exception: %s" \
        % (__name__,keyword_name,inst))          
    else :
        NETCONF_CLI_SESSION.pop(session_name)
        logger.debug("%s:%s -> netconf cli session '%s' closed" % (__name__,keyword_name,session_name))
    return 

def netconf_cli_config (command_index="",*command_elements,**params) :
    """
    send netconf cli configuration command
    - *command_index:* netconf cli command interface
    - *command_elements:* leafs and values in the most inner container
    - *session_name:* string, default value is "first_netconf_cli_session"
    - *prompt:* prompt to identify command end
    - *timeout:* waiting time (seconds) before check return prompt, default value is 0
    - *operation:* "no","","do",etc, default is ""
    - *expect_result:* PASS or FAIL, default value is "PASS" 
     
    usage example:

    # create
    | netconf cli config | interfaces interface | fast type xdsl port-type port-type user-port |
    # delete    
    | netconf cli config | interfaces interface | fast | operation=no | 
    # expect fail
    | netconf cli config | interfaces interface | fast | expect_result=FAIL |

    """
    keyword_name = 'netconf_cli_config'
    logger.debug ("%s:%s -> command_index=%s,command_elements=%s, params=%s" % \
        (__name__,keyword_name,command_index,str(command_elements),str( params)))
    session_name =  params.setdefault('session_name',"first_netconf_cli_session")
    timeout =  params.setdefault('timeout', None)
    operation =  params.setdefault('operation', "")
    expect_result =  params.setdefault('expect_result',"PASS")
    prompt =   params.setdefault('prompt', None)
    
    command_index = re.sub(" +", " ",command_index)
    command = command_index.strip()
    for elem in command_elements :
        elem = re.sub(","," ",elem)
        command = command + " " + elem 

    if os.environ.has_key ("NETCONF_CLI_CHANGE_MAP") :
        maps_dict = _transfer_string_to_dict (os.environ['NETCONF_CLI_CHANGE_MAP'])
        map_command = maps_dict.setdefault (command_index, [])            
        for item in map_command:
            m = re.search ("(.*)(\>|\+|\-)\>(.*)",item)
            if not m :
                raise AssertionError( "%s:%s -> wrong map command" % (__name__,keyword_name) )
            old_string,sign,new_string = m.groups()          
            if sign == ">" :
                command = re.sub(old_string.strip(),new_string.strip().lstrip(),command)
            if sign == "+" :
                command = command + " " + new_string
            if sign == "-" :
                command = re.sub(new_string,"",command)            
    command = operation +" " +command
       
    try:
        res = NETCONF_CLI_SESSION[session_name].send_command(command.lstrip(),timeout=timeout,prompt=prompt)
    except Exception as inst:
        raise AssertionError( "%s:%s-> failed netconf cli '%s', exception:\n %s" \
        % (__name__,keyword_name,command,inst) )
    else :
	logger.info("SSH REPLY << %s" % (res))
	
    status = _check_error(res)
    if status != expect_result.upper() : 
        raise AssertionError("failed netconf cli '%s'"  % (command))


def netconf_cli_check (command_index="",*command_elements,**params):
    """
    to check if specific contents in the command output.
    - *command_index:* netconf cli command interface
    - *command_elements:* leafs and values in the most inner container
    - *session_name:* string, default value is "first_netconf_cli_session"
    - *output_regexp:* the match regexp to command output
    - *prompt:* prompt to identify command end
    - *timeout:* waiting time (seconds) before check return prompt, default value is 0
    - *check_time: * how long time (seconds) before return not found
    - *operation:* "show full-configuration","pwd",,etc, default is "show full-configuration"
    - *expect_result:* PASS or FAIL, default value is "PASS" 
     
    usage example:
    | netconf cli check | interfaces interface | output_regexp=No entries found | timeout=5 |
    | netconf cli check | interfaces interface | fast | output_regexp="type xdsl, port-type user-port" | 
    """
    keyword_name = 'netconf_cli_config'
    logger.debug ("%s:%s -> command_index='%s',command_elements='%s',params=%s" % \
        (__name__,keyword_name,command_index,str(command_elements),str(params)))
    session_name = params.setdefault('session_name',"first_netconf_cli_session")
    timeout = params.setdefault('timeout', None)
    check_time = params.setdefault('check_time', 0)
    operation = params.setdefault('operation', "show full-configuration")
    expect_result = params.setdefault('expect_result',"PASS").upper()
    prompt = params.setdefault('prompt', None)
    output_regexp = params.setdefault('output_regexp',None)
    
    command_index = re.sub(" +", " ",command_index)
    command = command_index.strip()
    for elem in command_elements :
        command = command + " " + elem
        
    if os.environ.has_key ("NETCONF_CLI_CHANGE_MAP") :
        maps_dict = _transfer_string_to_dict (os.environ['NETCONF_CLI_CHANGE_MAP'])
        map_command = maps_dict.setdefault (command_index, [])            
        for item in map_command:
            m = re.search ("(.*)(\>|\+|\-)\>(.*)",item)
            if not m :
                raise AssertionError( "%s:%s -> wrong map command" % (__name__,keyword_name) )
            old_string,sign,new_string = m.groups()          
            if sign == ">" :
                command = re.sub(old_string.strip(),new_string.strip().lstrip(),command)
                output_regexp = re.sub(old_string.strip(),new_string.strip().lstrip(),output_regexp)
            if sign == "+" :
                command = command + " " + new_string.strip().lstrip()
            if sign == "-" :
                command = re.sub(new_string.strip().lstrip(),"",command) 
                output_regexp = re.sub(new_string.strip().lstrip(),"",output_regexp)                 
    command = operation +" " +command
                
    if check_time > 5 :
        interval_time = int(check_time/5)
    else :
        interval_time = 1   

    current_time = time.mktime(time.localtime())
    end_time = current_time + check_time

    while current_time <= end_time : 
        try:
            info = NETCONF_CLI_SESSION[session_name].send_command \
            (command.lstrip(),timeout=timeout,prompt=prompt)
        except AssertionError as inst:    
            raise AssertionError("failed netconf cli '%s' \n exception: %s" \
            % (command, inst))
        logger.info("SSH REPLY << %s" % (info))    
        info = info.replace(command,"")      
        status = _check_error(info)
        if status != expect_result.upper() : 
            raise AssertionError("failed netconf cli '%s'"  % (command))
     
        search_result = "PASS"
        if not output_regexp :
            return [info]
        found = _check_all_items_return_last (output_regexp, info)
        if not found :
            search_result = "FAIL"
            logger.debug("not gotten '%s' in output" % str(output_regexp))
            logger.info("expect_result:%s, search_result:%s" \
            % (expect_result, search_result)) 
            
        if search_result == expect_result :
            return found
        else :
            time.sleep(interval_time)
            current_time = time.mktime(time.localtime())           
            continue
    else :        
        raise AssertionError("no '%s' in cli '%s' output"\
        % (output_regexp,command))
         
                
def netconf_cli_raw_output(command_index="",*command_elements,**params):
    """
    send netconf cli and return the output without any check
    - *command_index:* netconf cli command interface
    - *command_elements:* leafs and values in the most inner container
    - *session_name:* string, default value is "first_netconf_cli_session"
    - *prompt:* prompt to identify command end
    - *timeout:* waiting time (seconds) before check return prompt, default value is 0
    - *operation:* "show full-configuration","pwd",,etc, default is ""
     
    usage example:
    | $output= | netconf cli raw output | interfaces interface | fast | operation="show full-configuration" |
    """
    keyword_name = 'netconf_cli_raw_output'
    logger.debug ("%s:%s -> command_index=%s,command_elements=%s,params=%s" % \
        (__name__,keyword_name,command_index,str(command_elements),str(params)))
    session_name = params.setdefault('session_name',"first_netconf_cli_session")
    timeout = params.setdefault('timeout', None)
    operation = params.setdefault('operation', "")
    prompt = params.setdefault('prompt', None)
    
    command_index = re.sub(" +", " ",command_index)
    command = command_index.strip()
    for elem in command_elements :
        command = command + " " + elem
        
    if os.environ.has_key ("NETCONF_CLI_CHANGE_MAP") :
        maps_dict = _transfer_string_to_dict (os.environ['NETCONF_CLI_CHANGE_MAP'])
        map_command = maps_dict.setdefault (command_index, [])            
        for item in map_command:
            m = re.search ("(.*)(\>|\+|\-)\>(.*)",item)
            if not m :
                raise AssertionError( "%s:%s -> wrong map command" % (__name__,keyword_name) )
            old_string,sign,new_string = m.groups()          
            if sign == ">" :
                command = re.sub(old_string.strip(),new_string.strip().lstrip(),command)
            if sign == "+" :
                command = command + " " + new_string
            if sign == "-" :
                command = re.sub(new_string,"",command)
    command = operation +" " +command
       
    try:
        res = NETCONF_CLI_SESSION[session_name].send_command(command.lstrip(),timeout=timeout,prompt=prompt)
    except Exception as inst:
        raise AssertionError( "%s:%s -> failed netconf cli '%s', exception :\n %s" \
        % (__name__,keyword_name,command,inst) )
    else :
	logger.info("SSH REPLY << %s" % (res))  
    return res
  

def _check_all_items_return_last (search_string, lines) :

    found = True  
    command,lines = lines.split("\n",1)
    search_list = search_string.split(",")
    for item in search_list : 
        item = re.sub(" +"," +",item).strip () 
        res = re.search(item,lines,re.DOTALL) 
        if not res :
            logger.debug("not found: %s" % item)
            found = False
            break
        else :
            logger.debug("found: %s" % item)            
    if found : 
        matched = res.groups()       
        if len(matched) == 0 :
            matched = res.group(0)
    else :
        matched = False  
    return matched 
    
def _transfer_string_to_dict ( input_string ) :
    d = {}
    input_string = input_string.lstrip("{").strip("}")
    for item in re.findall("\'(.*?)\':( *?)\[(.*?)\](,?)",input_string) :
        key,x,value,y = item     
        key = key.strip("'").strip("\"").strip().lstrip("'").lstrip("\"").lstrip()
        value = re.sub(" ?' ?","",value).lstrip ()       
        value_list = value.split(",")
        d[key] = value_list
    return d
     
def _check_error(data):
    """
        check if failure info existing in the data input
    """
    return_val = "PASS"
    error_syntax = 'syntax error:|Possible completions:'
   
    match_error = re.search(error_syntax,data)
    if match_error :
        return_val = "FAIL"
        logger.debug("gotten failure info: %s" % match_error.group(0))

    return  return_val

        
