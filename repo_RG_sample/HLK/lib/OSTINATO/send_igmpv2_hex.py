#! /usr/bin/env python
#interface = 3 -- eth2 1 -- eth1
#dst_mac = 0x01005E000009
#src_mac = 0x001E2ADF0547 
#src_ip = 0xc0a801ca
#dst_ip = 0xe1000009
#msg_tpye = 0x16 -- join 0x17 -- leave
#grp_ip = dst_ip
#-lif 3 -src_mac 0x00AE2ADF0547 -dst_mac 0x01005e000009 -src_ip 192.168.1.202 -dst_ip 225.0.0.9 -msg_type 16 -grp_ip^C25.0.0.9
def check():
	return pkt.interface and pkt.src_mac and pkt.dst_mac and pkt.src_ip and pkt.dst_ip and pkt.msg_type and pkt.grp_ip   

# standard modules
import os
import sys
import time
# ostinato modules 
# (user scripts using the installed package should prepend ostinato. i.e
#  ostinato.core and ostinato.protocols)
from ostinato.core import ost_pb, DroneProxy
from ostinato.protocols.mac_pb2 import mac
from ostinato.protocols.ip4_pb2 import ip4, Ip4
from ostinato.protocols.igmp_pb2 import igmp
import argparse

def auto_int(x):
    return int(x, 0)

parser = argparse.ArgumentParser(

description='send igmp v2 packet',

)      
parser.add_argument('-lif', action='store',

                   dest='interface',
		
		type=auto_int,
                    
help='igmp interface')

parser.add_argument('-src_mac', action='store',

                   dest='src_mac',

                    
type=auto_int,
help='igmp src mac')

parser.add_argument('-dst_mac', action='store',

                   dest='dst_mac',

                    
type=auto_int,
help='igmp dst mac')

parser.add_argument('-src_ip', action='store',

                   dest='src_ip',

                    
type=auto_int,
help='igmp src ip')

parser.add_argument('-dst_ip', action='store',

                   dest='dst_ip',

                    
type=auto_int,
help='igmp dst ip')

parser.add_argument('-msg_type', action='store',

                   dest='msg_type',

                    
type=auto_int,
help='igmp msg type join:16 leave:17')

parser.add_argument('-grp_ip', action='store',

                   dest='grp_ip',

                    
type=auto_int,
help='igmp group ip')

pkt = parser.parse_args()

if check() == None:
	print 'igmpv2 -lif        = %r' % pkt.interface
	print 'igmpv2 -src_mac    = %r' % pkt.src_mac
	print 'igmpv2 -dst_mac    = %r' % pkt.dst_mac
	print 'igmpv2 -src_ip     = %r' % pkt.src_ip
	print 'igmpv2 -dst_ip     = %r' % pkt.dst_ip
	print 'igmpv2 -msg_type   = %r' % pkt.msg_type
	print 'igmpv2 -grp_ip     = %r' % pkt.grp_ip
else:
	print '%x' % pkt.interface
	print '%x' % pkt.src_mac
	print '%x' % pkt.dst_mac
	print '%x' % pkt.src_ip
	print '%x' % pkt.dst_ip
	print '%x' % pkt.msg_type
	print '%x' % pkt.grp_ip
	raw_input('continue?')
	# initialize defaults
	use_defaults = False
	host_name = '127.0.0.1'
	tx_port_number = pkt.interface
	rx_port_number = pkt.interface

	drone = DroneProxy(host_name)
	drone.connect()

	tx_port = ost_pb.PortIdList()
	tx_port.port_id.add().id = tx_port_number;
	rx_port = ost_pb.PortIdList()
	rx_port.port_id.add().id = rx_port_number;

	stream_id = ost_pb.StreamIdList()
	stream_id.port_id.CopyFrom(tx_port.port_id[0])
	stream_id.stream_id.add().id = 1
	drone.addStream(stream_id)

	# configure the stream
	stream_cfg = ost_pb.StreamConfigList()
	stream_cfg.port_id.CopyFrom(tx_port.port_id[0])
	s = stream_cfg.stream.add()
	s.stream_id.id = stream_id.stream_id[0].id
	s.core.is_enabled = True
	s.control.num_packets = 1
	#mac
	p = s.protocol.add()
	p.protocol_id.id = ost_pb.Protocol.kMacFieldNumber
	p.Extensions[mac].dst_mac = pkt.dst_mac
	p.Extensions[mac].src_mac = pkt.src_mac
	#eth
	p = s.protocol.add()
	p.protocol_id.id = ost_pb.Protocol.kEth2FieldNumber
	#ip
	p = s.protocol.add()
	p.protocol_id.id = ost_pb.Protocol.kIp4FieldNumber
	p.Extensions[ip4].src_ip = pkt.src_ip 
	p.Extensions[ip4].dst_ip = pkt.dst_ip 
	p.Extensions[ip4].ttl=0x1
	#igmp
	p = s.protocol.add()
	p.protocol_id.id = ost_pb.Protocol.kIgmpFieldNumber
	ip = p.Extensions[igmp]
	ip.type = pkt.msg_type
	ip.group_address.v4 = pkt.dst_ip
	#payload
	s.protocol.add().protocol_id.id = ost_pb.Protocol.kPayloadFieldNumber

	drone.modifyStream(stream_cfg)
	drone.startCapture(tx_port)

	drone.startTransmit(tx_port)
	time.sleep(3)
	drone.stopTransmit(tx_port)

	drone.stopCapture(tx_port)

	#buff = drone.getCaptureBuffer(tx_port.port_id[0])

	#drone.saveCaptureBuffer(buff, 'capture.pcap')

	drone.disconnect()


#python send_igmpv2.py -lif 0x3 -dst_mac 0x01005E000009 -src_mac 0x001E2ADF0547 -src_ip 0xc0a801ca -dst_ip 0xe1000009 -msg_type 0x16 -grp_ip  0xe1000009
#python send_igmpv2.py -lif 0x3 -dst_mac 0x01005E000009 -src_mac 0x001E2ADF0547 -src_ip 0xc0a801ca -dst_ip 0xe1000009 -msg_type 0x17 -grp_ip  0xe1000009

