#! /usr/bin/env python
#0x16
#0xe1000009

# standard modules
import os
import sys
import time
# ostinato modules 
# (user scripts using the installed package should prepend ostinato. i.e
#  ostinato.core and ostinato.protocols)
from ostinato.core import ost_pb, DroneProxy
from ostinato.protocols.mac_pb2 import mac
from ostinato.protocols.ip4_pb2 import ip4, Ip4
from ostinato.protocols.igmp_pb2 import igmp

igmptype = 0x22 
igmpgrpadd =int(sys.argv[1],16) 
igmpjl = sys.argv[2]
# initialize defaults
use_defaults = False
host_name = '127.0.0.1'
tx_port_number = 3
rx_port_number = 3

drone = DroneProxy(host_name)
print tx_port_number
print rx_port_number
drone.connect()
tx_port = ost_pb.PortIdList()
tx_port.port_id.add().id = tx_port_number;
rx_port = ost_pb.PortIdList()
rx_port.port_id.add().id = rx_port_number;

stream_id = ost_pb.StreamIdList()
stream_id.port_id.CopyFrom(tx_port.port_id[0])
stream_id.stream_id.add().id = 1
drone.addStream(stream_id)

# configure the stream
stream_cfg = ost_pb.StreamConfigList()
stream_cfg.port_id.CopyFrom(tx_port.port_id[0])
s = stream_cfg.stream.add()
s.stream_id.id = stream_id.stream_id[0].id
s.core.is_enabled = True
s.control.num_packets = 1
#mac
p = s.protocol.add()
p.protocol_id.id = ost_pb.Protocol.kMacFieldNumber
p.Extensions[mac].dst_mac = 0x01005e000063
p.Extensions[mac].src_mac = 0x001E2ADF0547 
#ta_send_igmp_message -lif 2 -mac_src 00AE2ADF0547 -mac_dest 01005e000063 -ip_src 192.168.1.202 -ip_dest 225.0.0.99 -igmp_version V3 -message_count 2 -igmp_type 22 -igmp_grp_addr 225.0.0.99 -group_record_list 04000000E1000063 - nb_of_group_records 1



#eth
p = s.protocol.add()
p.protocol_id.id = ost_pb.Protocol.kEth2FieldNumber

#ip
p = s.protocol.add()
p.protocol_id.id = ost_pb.Protocol.kIp4FieldNumber
# reduce typing by creating a shorter reference to p.Extensions[ip4]
print dir(p)
print "....."
ip = p.Extensions[ip4]

ip.src_ip = 0xc0a801ca
ip.dst_ip = igmpgrpadd
#0xe1000009
#0xe1000063
ip.ttl=0x1

#igmp
p = s.protocol.add()
p.protocol_id.id = ost_pb.Protocol.kIgmpFieldNumber
ip = p.Extensions[igmp]
ip.type = igmptype 
ip.max_response_time = 0
print "-------------"
i = (ip.group_records.add())
#i = (ip.group_records.add())
#how to add record with group address and record type????
#= 0xe1000063 
print type(i)
if igmpjl == 'j': 
	i.type = i.kToExclude
elif igmpjl == 'l':
	i.type = i.kToInclude
else:
	i.type = None
print i.type
i.group_address.v4 = 0xe1000063
print s.protocol
raw_input("press and key to continue, shows the total count")
#print dir(ip.group_records)
s.protocol.add().protocol_id.id = ost_pb.Protocol.kPayloadFieldNumber


drone.modifyStream(stream_cfg)
#print s.protocol
drone.startCapture(tx_port)

drone.startTransmit(tx_port)
time.sleep(3)
drone.stopTransmit(tx_port)

drone.stopCapture(tx_port)

#buff = drone.getCaptureBuffer(tx_port.port_id[0])

#drone.saveCaptureBuffer(buff, 'capture.pcap')

drone.disconnect()


