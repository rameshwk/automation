#! /usr/bin/env python
#0x16
#0xe1000009

# standard modules
import os
import sys
import time
# ostinato modules 
# (user scripts using the installed package should prepend ostinato. i.e
#  ostinato.core and ostinato.protocols)
from ostinato.core import ost_pb, DroneProxy
from ostinato.protocols.mac_pb2 import mac
from ostinato.protocols.ip4_pb2 import ip4, Ip4
from ostinato.protocols.igmp_pb2 import igmp
# initialize defaults
use_defaults = False
host_name = '127.0.0.1'
tx_port_number = 3
rx_port_number = 3

drone = DroneProxy(host_name)
print tx_port_number
print rx_port_number
drone.connect()
tx_port = ost_pb.PortIdList()
tx_port.port_id.add().id = tx_port_number;
rx_port = ost_pb.PortIdList()
rx_port.port_id.add().id = rx_port_number;

stream_id = ost_pb.StreamIdList()
stream_id.port_id.CopyFrom(tx_port.port_id[0])
stream_id.stream_id.add().id = 1
drone.addStream(stream_id)

# configure the stream
stream_cfg = ost_pb.StreamConfigList()
stream_cfg.port_id.CopyFrom(tx_port.port_id[0])
s = stream_cfg.stream.add()
s.stream_id.id = stream_id.stream_id[0].id
s.core.is_enabled = True
s.core.len_mode = s.core.e_fl_fixed
s.core.frame_len = 68
s.control.num_packets = 1
#mac
p = s.protocol.add()
p.protocol_id.id = ost_pb.Protocol.kMacFieldNumber
p.Extensions[mac].dst_mac = 0x001E2ADF0547
#0x01005E000009
p.Extensions[mac].src_mac = 0x001E2ADF0547 

#eth
p = s.protocol.add()
p.protocol_id.id = ost_pb.Protocol.kEth2FieldNumber


drone.modifyStream(stream_cfg)

drone.startCapture(tx_port)

drone.startTransmit(tx_port)
time.sleep(3)
drone.stopTransmit(tx_port)

drone.stopCapture(tx_port)

#buff = drone.getCaptureBuffer(tx_port.port_id[0])

#drone.saveCaptureBuffer(buff, 'capture.pcap')

drone.disconnect()


