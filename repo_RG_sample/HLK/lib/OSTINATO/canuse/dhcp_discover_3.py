#! /usr/bin/env python

# standard modules
import os
import sys
import time
# ostinato modules 
# (user scripts using the installed package should prepend ostinato. i.e
#  ostinato.core and ostinato.protocols)
from ostinato.core import ost_pb, DroneProxy
from ostinato.protocols.mac_pb2 import mac
from ostinato.protocols.ip4_pb2 import ip4
from ostinato.protocols.udp_pb2 import udp
from ostinato.protocols.payload_pb2 import payload
from ostinato.protocols.hexdump_pb2 import hexDump


# initialize defaults
use_defaults = False
host_name = '127.0.0.1'
tx_port_number = 3
rx_port_number = 3

drone = DroneProxy(host_name)
print tx_port_number
print rx_port_number
drone.connect()
tx_port = ost_pb.PortIdList()
tx_port.port_id.add().id = tx_port_number;
rx_port = ost_pb.PortIdList()
rx_port.port_id.add().id = rx_port_number;

stream_id = ost_pb.StreamIdList()
stream_id.port_id.CopyFrom(tx_port.port_id[0])
stream_id.stream_id.add().id = 1
drone.addStream(stream_id)

# configure the stream
stream_cfg = ost_pb.StreamConfigList()
stream_cfg.port_id.CopyFrom(tx_port.port_id[0])
s = stream_cfg.stream.add()
s.stream_id.id = stream_id.stream_id[0].id
s.core.len_mode = s.core.e_fl_fixed
s.core.frame_len = 300
s.core.is_enabled = True
s.control.num_packets = 1
#mac
p = s.protocol.add()
p.protocol_id.id = ost_pb.Protocol.kMacFieldNumber
p.Extensions[mac].dst_mac = 0xFFFFFFFFFFFF
print 'dst mac %x' % 0xFFFFFFFFFFFF
p.Extensions[mac].src_mac = 0x001E2ADF0547 
print 'src mac %x' % 0x001e2adf0547


#eth
p = s.protocol.add()
p.protocol_id.id = ost_pb.Protocol.kEth2FieldNumber


#ip
p = s.protocol.add()
p.protocol_id.id = ost_pb.Protocol.kIp4FieldNumber
# reduce typing by creating a shorter reference to p.Extensions[ip4]
ip = p.Extensions[ip4]
ip.src_ip = 0x00000000
print 'src ip %x' % 0x00000000
ip.dst_ip = 0xffffffff
print 'dst ip %x' % 0xffffffff

#udp
p = s.protocol.add()
p.protocol_id.id = ost_pb.Protocol.kUdpFieldNumber
p.Extensions[udp].is_override_dst_port = True
p.Extensions[udp].is_override_src_port = True
p.Extensions[udp].dst_port = 67
p.Extensions[udp].src_port = 68

#payload hexdump
p = s.protocol.add()
p.protocol_id.id = ost_pb.Protocol.kHexDumpFieldNumber
import random
r = '%x' % (random.randint(0x00000000,0xffffffff))
b = '01010600'+r+'0004000000000000000000000000000000000000001e2adf05470000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'+128*2*'0'+'63825363'+'350101'
import binascii
s = binascii.unhexlify(b)
p.Extensions[hexDump].content = s
p.Extensions[hexDump].pad_until_end = True
 
drone.modifyStream(stream_cfg)

drone.startCapture(tx_port)

drone.startTransmit(tx_port)
time.sleep(3)
drone.stopTransmit(tx_port)

drone.stopCapture(tx_port)

#buff = drone.getCaptureBuffer(tx_port.port_id[0])

#drone.saveCaptureBuffer(buff, 'capture.pcap')

drone.disconnect()
