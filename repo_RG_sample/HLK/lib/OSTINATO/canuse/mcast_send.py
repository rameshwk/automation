#! /usr/bin/env python
#l2 mcast is not making any sense
#only l3 mcast is what you should do
#also the mcast must go down, i.e. ta_send_udp_packet -lif 1 -mac_src 00AE2ADF0546 -ip_src 10.18.90.2 02 -src_port 9988 -mac_dest 01005e000063 -ip_dest 225.0.0.99 -dst_port 9988 -me ssage_count 5 -message_delay 100 -c_vlan true -c_vlan_id 1001 -c_vlan_user_prior 0

# standard modules
import os
import sys
import time
# ostinato modules 
# (user scripts using the installed package should prepend ostinato. i.e
#  ostinato.core and ostinato.protocols)
from ostinato.core import ost_pb, DroneProxy
from ostinato.protocols.mac_pb2 import mac
from ostinato.protocols.vlan_pb2 import vlan
from ostinato.protocols.ip4_pb2 import ip4
from ostinato.protocols.udp_pb2 import udp
from ostinato.protocols.payload_pb2 import payload
grpmac = int(sys.argv[1],16)
grpadd = int(sys.argv[2],16)
# initialize defaults
use_defaults = False
host_name = '127.0.0.1'
tx_port_number = 1
rx_port_number = 1

drone = DroneProxy(host_name)
print tx_port_number
print rx_port_number
drone.connect()
tx_port = ost_pb.PortIdList()
tx_port.port_id.add().id = tx_port_number;
rx_port = ost_pb.PortIdList()
rx_port.port_id.add().id = rx_port_number;

stream_id = ost_pb.StreamIdList()
stream_id.port_id.CopyFrom(tx_port.port_id[0])
stream_id.stream_id.add().id = 1
drone.addStream(stream_id)

# configure the stream
stream_cfg = ost_pb.StreamConfigList()
stream_cfg.port_id.CopyFrom(tx_port.port_id[0])
s = stream_cfg.stream.add()
s.stream_id.id = stream_id.stream_id[0].id
s.core.is_enabled = True
s.control.num_packets = 1
#mac
p = s.protocol.add()
p.protocol_id.id = ost_pb.Protocol.kMacFieldNumber
p.Extensions[mac].dst_mac = grpmac
#p.Extensions[mac].dst_mac = 0x01005e000063
#p.Extensions[mac].dst_mac = 0x01005e000009
p.Extensions[mac].src_mac = 0x00AE2ADF0546 

#vlan
p = s.protocol.add()
p.protocol_id.id = ost_pb.Protocol.kVlanFieldNumber
p.Extensions[vlan].vlan_tag = 1001

#eth
p = s.protocol.add()
p.protocol_id.id = ost_pb.Protocol.kEth2FieldNumber

#ip
p = s.protocol.add()
p.protocol_id.id = ost_pb.Protocol.kIp4FieldNumber
#p.Extensions[ip4].dst_ip = 0xe1000063
p.Extensions[ip4].dst_ip = 0xe1000009
p.Extensions[ip4].dst_ip = grpadd
p.Extensions[ip4].src_ip = 0x0a125aca

#udp
p = s.protocol.add()
p.protocol_id.id = ost_pb.Protocol.kUdpFieldNumber
p.Extensions[udp].is_override_dst_port = True
p.Extensions[udp].is_override_src_port = True
p.Extensions[udp].dst_port = 9988
p.Extensions[udp].src_port = 9988

#payload
p = s.protocol.add()
p.protocol_id.id = ost_pb.Protocol.kPayloadFieldNumber
p.Extensions[payload].pattern_mode = 0 
#p.Extensions[payload].pattern = 0x1
#00:00:00:01:00:00:00:01:00:00:00:01:00:00
p.Extensions[payload].pattern = 0x11
drone.modifyStream(stream_cfg)

drone.startCapture(tx_port)

drone.startTransmit(tx_port)
time.sleep(3)
drone.stopTransmit(tx_port)

drone.stopCapture(tx_port)

buff = drone.getCaptureBuffer(tx_port.port_id[0])

drone.saveCaptureBuffer(buff, 'capture.pcap')

drone.disconnect()

drone.disconnect()


