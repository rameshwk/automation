#! /usr/bin/env python
def check():
	return pkt.interface and pkt.src_mac and pkt.dst_mac and pkt.src_ip and pkt.dst_ip and pkt.mvlan and pkt.dst_port
def ipDec2Hex(ip):
    dec1 = ip
    dec = dec1.split('.')
    return '%02x%02x%02x%02x' % (int(dec[0]), int(dec[1]), int(dec[2]), int(dec[3])) 


# standard modules
import os
import sys
import time
# ostinato modules 
# (user scripts using the installed package should prepend ostinato. i.e
#  ostinato.core and ostinato.protocols)
from ostinato.core import ost_pb, DroneProxy
from ostinato.protocols.mac_pb2 import mac
from ostinato.protocols.vlan_pb2 import vlan
from ostinato.protocols.ip4_pb2 import ip4, Ip4
from ostinato.protocols.igmp_pb2 import igmp
from ostinato.protocols.udp_pb2 import udp
from ostinato.protocols.payload_pb2 import payload
import argparse

parser = argparse.ArgumentParser(
	description='send mcast packet',
)      

parser.add_argument('-lif', action='store',
		    dest='interface',
		    type=str,
		    help='igmp interface')

parser.add_argument('-src_mac', action='store',
		    dest='src_mac',
		    type=str,
		    help='igmp src mac')

parser.add_argument('-dst_mac', action='store',
		    dest='dst_mac',
		    type=str,
		    help='igmp dst mac')

parser.add_argument('-src_ip', action='store',
		    dest='src_ip',
		    type=str,
		    help='igmp src ip')

parser.add_argument('-dst_ip', action='store',
		    dest='dst_ip',
		    type=str,
		    help='igmp dst ip')

parser.add_argument('-dst_port', action='store',
		    dest='dst_port',
		    type=str,
		    help='igmp dst port')

parser.add_argument('-mvlan', action='store',
		    dest='mvlan',
		    type=str,
		    help='igmp mvlan')

pkt = parser.parse_args()

if check() == None:
	print 'igmp -lif        = %r' % pkt.interface
	print 'igmp -src_mac    = %r' % pkt.src_mac
	print 'igmp -dst_mac    = %r' % pkt.dst_mac
	print 'igmp -src_ip     = %r' % pkt.src_ip
	print 'igmp -dst_ip     = %r' % pkt.dst_ip
	print 'igmp -dst_port     = %r' % pkt.dst_port
	print 'igmp -mvlan     = %r' % pkt.mvlan
else:
	#print 'igmpv2 -lif        =%x' % int(pkt.interface)
	#print 'igmpv2 -src_mac    =0x%012x' % int(pkt.src_mac,16)
	#print 'igmpv2 -dst_mac    =0x%012x' % int(pkt.dst_mac,16)
	#print 'igmpv2 -src_ip     =0x%012x' % int(ipDec2Hex(pkt.src_ip),16)
	#print 'igmpv2 -dst_ip     =0x%012x' % int(ipDec2Hex(pkt.dst_ip),16)

	pkt.interface = int(pkt.interface)
	pkt.src_mac = int(pkt.src_mac,16)
	pkt.dst_mac = int(pkt.dst_mac,16)
	pkt.src_ip = int(ipDec2Hex(pkt.src_ip),16)
	pkt.dst_ip = int(ipDec2Hex(pkt.dst_ip),16)
	pkt.mvlan = int(pkt.mvlan,10)
	pkt.dst_port = int(pkt.dst_port,10)


	# initialize defaults
	use_defaults = False
	host_name = '127.0.0.1'
	tx_port_number = pkt.interface
	rx_port_number = pkt.interface

	drone = DroneProxy(host_name)
	drone.connect()

	tx_port = ost_pb.PortIdList()
	tx_port.port_id.add().id = tx_port_number;
	rx_port = ost_pb.PortIdList()
	rx_port.port_id.add().id = rx_port_number;

	stream_id = ost_pb.StreamIdList()
	stream_id.port_id.CopyFrom(tx_port.port_id[0])
	stream_id.stream_id.add().id = 1
	drone.addStream(stream_id)

	# configure the stream
	stream_cfg = ost_pb.StreamConfigList()
	stream_cfg.port_id.CopyFrom(tx_port.port_id[0])
	s = stream_cfg.stream.add()
	s.stream_id.id = stream_id.stream_id[0].id
	s.core.is_enabled = True
	s.core.len_mode = s.core.e_fl_fixed
	s.core.frame_len = 68
	s.control.num_packets = 1
	#mac
	p = s.protocol.add()
	p.protocol_id.id = ost_pb.Protocol.kMacFieldNumber
	p.Extensions[mac].dst_mac = pkt.dst_mac
	p.Extensions[mac].src_mac = pkt.src_mac
	#vlan
	p = s.protocol.add()
	p.protocol_id.id = ost_pb.Protocol.kVlanFieldNumber
	p.Extensions[vlan].vlan_tag = pkt.mvlan
	#eth
	p = s.protocol.add()
	p.protocol_id.id = ost_pb.Protocol.kEth2FieldNumber
	#ip
	p = s.protocol.add()
	p.protocol_id.id = ost_pb.Protocol.kIp4FieldNumber
	p.Extensions[ip4].src_ip = pkt.src_ip 
	p.Extensions[ip4].dst_ip = pkt.dst_ip 
	#udp
	p = s.protocol.add()
	p.protocol_id.id = ost_pb.Protocol.kUdpFieldNumber
	p.Extensions[udp].is_override_dst_port = True
	p.Extensions[udp].is_override_src_port = True
	p.Extensions[udp].dst_port = pkt.dst_port
	p.Extensions[udp].src_port = 9988
	#payload
	p = s.protocol.add()
	p.protocol_id.id = ost_pb.Protocol.kPayloadFieldNumber
	p.Extensions[payload].pattern_mode = 0
	#p.Extensions[payload].pattern = 0x1
	#00:00:00:01:00:00:00:01:00:00:00:01:00:00
	p.Extensions[payload].pattern = 0x11


	drone.modifyStream(stream_cfg)
	drone.startCapture(tx_port)

	drone.startTransmit(tx_port)
	time.sleep(3)
	drone.stopTransmit(tx_port)

	drone.stopCapture(tx_port)

#	buff = drone.getCaptureBuffer(tx_port.port_id[0])

#	drone.saveCaptureBuffer(buff, 'capture.pcap')

	drone.disconnect()
#python send_mcast.py -lif 1 -src_mac 001E2ADF0546 -src_ip 10.18.90.202 -dst_mac 01005e000009 -dst_ip 225.0.0.9


