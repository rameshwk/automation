def b(d):
    return bin(d)[2:].zfill(8)
def groupIp2Mac(ip):
    dec1 = ip
    dec = dec1.split('.')
    d0 = int(dec[0])
    d1 = int(dec[1])
    d2 = int(dec[2])
    d3 = int(dec[3])
    d1 = b(d1).split('b')
    d1 = int(d1[0][1:],2)    
    return '0x01005e%02x%02x%02x' % (d1,d2,d3)

def ipDec2Hex(ip):
    dec1 = ip
    dec = dec1.split('.')
    return '0x%02x%02x%02x%02x' % (int(dec[0]), int(dec[1]), int(dec[2]), int(dec[3])) 

def ipHex2Dec(ip):
    hex1 = ip
    hex2 = hex1.split('x')[1]
    h0 = hex2[0:2]
    h1 = hex2[2:4]
    h2 = hex2[4:6]
    h3 = hex2[6:8]
    return '%d.%d.%d.%d' % (int(h0,16),int(h1,16),int(h2,16),int(h3,16))
