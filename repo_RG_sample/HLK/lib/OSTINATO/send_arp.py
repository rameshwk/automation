#! /usr/bin/env python
def check():
	return pkt.interface and pkt.src_mac and pkt.dst_mac and pkt.src_ip and pkt.dst_ip
def ipDec2Hex(ip):
    dec1 = ip
    dec = dec1.split('.')
    return '%02x%02x%02x%02x' % (int(dec[0]), int(dec[1]), int(dec[2]), int(dec[3])) 


# standard modules
import os
import sys
import time
# ostinato modules 
# (user scripts using the installed package should prepend ostinato. i.e
#  ostinato.core and ostinato.protocols)
from ostinato.core import ost_pb, DroneProxy
from ostinato.protocols.mac_pb2 import mac
from ostinato.protocols.vlan_pb2 import vlan
from ostinato.protocols.arp_pb2 import arp
from ostinato.protocols.ip4_pb2 import ip4, Ip4
import argparse

parser = argparse.ArgumentParser(

description='send arp packet',

)      
parser.add_argument('-lif', action='store',

                   dest='interface',
		
		
                    
type=str,
help='arp interface')

parser.add_argument('-src_mac', action='store',

                   dest='src_mac',

                    

type=str,
help='arp src mac')

parser.add_argument('-dst_mac', action='store',

                   dest='dst_mac',

                    

type=str,
help='arp dst mac')

parser.add_argument('-vlan', action='store',

                   dest='vlan',

                    

type=str,
help='arp vlan')
parser.add_argument('-src_ip', action='store',

                   dest='src_ip',

                    

type=str,
help='arp src ip')

parser.add_argument('-dst_ip', action='store',

                   dest='dst_ip',

                    

type=str,
help='arp dst ip')


pkt = parser.parse_args()

if check() == None:
	print 'arp -lif        = %r' % pkt.interface
	print 'arp -src_mac    = %r' % pkt.src_mac
	print 'arp -dst_mac    = %r' % pkt.dst_mac
	print 'arp -vlan       = %r' % pkt.vlan
	print 'arp -src_ip     = %r' % pkt.src_ip
	print 'arp -dst_ip     = %r' % pkt.dst_ip
else:
	#print 'igmpv2 -lif        =%x' % int(pkt.interface)
	#print 'igmpv2 -src_mac    =0x%012x' % int(pkt.src_mac,16)
	#print 'igmpv2 -dst_mac    =0x%012x' % int(pkt.dst_mac,16)
	#print 'igmpv2 -src_ip     =0x%012x' % int(ipDec2Hex(pkt.src_ip),16)
	#print 'igmpv2 -dst_ip     =0x%012x' % int(ipDec2Hex(pkt.dst_ip),16)

	pkt.interface = int(pkt.interface)
	pkt.src_mac = int(pkt.src_mac,16)
	pkt.dst_mac = int(pkt.dst_mac,16)
	if pkt.vlan != None:
		pkt.vlan = int(pkt.vlan)
	pkt.src_ip = int(ipDec2Hex(pkt.src_ip),16)
	pkt.dst_ip = int(ipDec2Hex(pkt.dst_ip),16)

	# initialize defaults
	use_defaults = False
	host_name = '127.0.0.1'
	tx_port_number = pkt.interface
	rx_port_number = pkt.interface

	drone = DroneProxy(host_name)
	drone.connect()

	tx_port = ost_pb.PortIdList()
	tx_port.port_id.add().id = tx_port_number;
	rx_port = ost_pb.PortIdList()
	rx_port.port_id.add().id = rx_port_number;

	stream_id = ost_pb.StreamIdList()
	stream_id.port_id.CopyFrom(tx_port.port_id[0])
	stream_id.stream_id.add().id = 1
	drone.addStream(stream_id)

	# configure the stream
	stream_cfg = ost_pb.StreamConfigList()
	stream_cfg.port_id.CopyFrom(tx_port.port_id[0])
	s = stream_cfg.stream.add()
	s.stream_id.id = stream_id.stream_id[0].id
	s.core.is_enabled = True
	s.core.len_mode = s.core.e_fl_fixed
	s.core.frame_len = 68
	s.control.num_packets = 1
	#mac
	p = s.protocol.add()
	p.protocol_id.id = ost_pb.Protocol.kMacFieldNumber
	p.Extensions[mac].dst_mac = pkt.dst_mac
	p.Extensions[mac].src_mac = pkt.src_mac
	#vlan
	if pkt.vlan != None:
		p = s.protocol.add()
		p.protocol_id.id = ost_pb.Protocol.kVlanFieldNumber
#		p.Extensions[vlan].is_override_tpid= False
#		p.Extensions[vlan].tpid= 33024

		p.Extensions[vlan].vlan_tag = pkt.vlan
	#eth
	p = s.protocol.add()
	p.protocol_id.id = ost_pb.Protocol.kEth2FieldNumber
	#arp
	p = s.protocol.add()
	p.protocol_id.id = ost_pb.Protocol.kArpFieldNumber
	p.Extensions[arp].op_code = 1
        p.Extensions[arp].sender_hw_addr = pkt.src_mac
	p.Extensions[arp].sender_hw_addr_mode = p.Extensions[arp].kFixed
        p.Extensions[arp].sender_hw_addr_count = 16
        p.Extensions[arp].sender_proto_addr = pkt.src_ip
	p.Extensions[arp].sender_proto_addr_mode = p.Extensions[arp].kFixedHost
        p.Extensions[arp].sender_proto_addr_count = 16
	p.Extensions[arp].sender_proto_addr_mask = int(ipDec2Hex('255.255.255.255'),16)

        p.Extensions[arp].target_hw_addr = pkt.dst_mac
        p.Extensions[arp].target_hw_addr = 0x000000000000
	p.Extensions[arp].target_hw_addr_mode = p.Extensions[arp].kFixed
        p.Extensions[arp].target_hw_addr_count = 16
        p.Extensions[arp].target_proto_addr = pkt.dst_ip
	p.Extensions[arp].target_proto_addr_mode = p.Extensions[arp].kFixedHost
        p.Extensions[arp].target_proto_addr_count = 16
	p.Extensions[arp].target_proto_addr_mask = int(ipDec2Hex('255.255.255.255'),16)
	
	#print s.protocol

	drone.modifyStream(stream_cfg)
	drone.startCapture(tx_port)

	drone.startTransmit(tx_port)
	time.sleep(3)
	drone.stopTransmit(tx_port)

	drone.stopCapture(tx_port)

	#buff = drone.getCaptureBuffer(tx_port.port_id[0])

	#drone.saveCaptureBuffer(buff, 'capture.pcap')

	drone.disconnect()
#python send_arp.py -lif 3 -src_ip 192.168.1.202 -src_mac 00AE2ADF0547 -dst_ip 192.168.1.254 -dst_mac ffffffffffff
#python send_arp.py -lif 1 -src_ip 10.18.92.101 -src_mac 00AE2ADF0546 -dst_ip 10.18.92.162 -dst_mac ffffffffffff -vlan 1002

