import re,time,sys,os
from robot.api import logger
from copy import deepcopy
import com_msg
from scp import SCPClient
#from robot.variables import GLOBAL_VARIABLES
from robot.variables.scopes import GlobalVariables

lib_path = os.environ['ROBOTREPO'] +'/LIBS/DATA_TRANSLATION'
if lib_path not in sys.path:
    sys.path.append(lib_path)
import data_translation

try:
    import xml.etree.cElementTree as ET
except ImportError:
    import xml.etree.ElementTree as ET

SSH_SESSION       = {}
DIR_OUTPUT        = ""
MATCHED_PACKET    = [] 

def open_msg_session (ip,username,password,session_name="first_msg_session",prompt=".*\$|.*\#|.*\%",port='22') :
    """
    Build up the session to msg server
    - *ip:*           ip address
    - *port:*         port number for this connectin
    - *username:*     username for create ssh session
    - *password:*     password of ssh session
    - *session_name:* the name of created ssh session
    - *prompt:*       meeting it in command return indicate command executed correctly
    - *RETURN:*       session name or raise error

    Usage Example:
    | open msg session | 135.251.200.101 | atxuser | alcatel01 |
    | open msg session | 135.251.200.101 | atxuser | alcatel01 | 
    |   | session_name=dhcp_ssh_session | prompt=".*#" |
    """
    keyword_name = "open_msg_session"

    logger.debug("%s:%s -> \
    ip=%s,port=%s,username=%s,password=%s,session_name=%s,prompt=%s" \
    % (__name__,keyword_name,ip,port,username,password,session_name,prompt))

    try:
        # read session info from SETUP csv file
        SSH_SESSION[session_name] = com_msg.com_msg \
        (ip,port,username,password,prompt)
    except Exception as inst:
        raise MsgContinueError("%s:%s-> fail to init com_msg, exception: %s" \
        % (__name__,keyword_name,str(inst)))

    try:
        SSH_SESSION[session_name].open_ssh_session()
        SSH_SESSION[session_name].send_command("\n") 
    except Exception as inst:
        raise MsgContinueError("%s:%s-> fail to open_ssh_session, exception: %s" \
        % (__name__,keyword_name,str(inst)))
    else :
        logger.info("Open SSH session success! ")

    # check tshark permission
    cmd_check = "sudo tshark -i ethx"
    check_result = SSH_SESSION[session_name].send_command(cmd_check,'.*\$|.*\#|.*\%')   
    if re.search('don\'t have permission',check_result) :
        steps_info = """
        ************************************************************************************
        ----for example: USER is atxuser----

        ----add USER atxuser to GROUP wireshark----
        [root@localhost robot]# groupadd wireshark
        [root@localhost robot]# usermod -a -G wireshark atxuser

        ----switch to atxuser and add GROUP wireshark----
        [root@localhost robot]# su - atxuser
        [atxuser@localhost ~]$ newgrp wireshark
        [atxuser@localhost ~]$ exit

        ----Grant Capabilities----
        [root@localhost robot]# chgrp wireshark /usr/sbin/dumpcap
        [root@localhost robot]# chmod 750 /usr/sbin/dumpcap
        [root@localhost robot]# setcap cap_net_raw,cap_net_admin=eip /usr/sbin/dumpcap
        [root@localhost robot]# getcap /usr/sbin/dumpcap
        /usr/sbin/dumpcap = cap_net_admin,cap_net_raw+eip

        ----to check if tshark can be working fine with non-root user----
        [root@localhost robot]#su - atxuser
        [atxuser@localhost ~]$ tshark -i eth0 -a duration:10 -V -T pdml > dump.xml &
        ************************************************************************************
        """
        logger.error("Can't run tshark cmd with user %s, pls refer to the following steps to enable non-root capture:" 
        % username)
        logger.error(steps_info)
        raise MsgContinueError("%s:%s-> fail to run tshark cmd with user %s" \
        % (__name__,keyword_name,username))

def close_msg_session (session_name="first_msg_session") :
    """
       disconnect tshark by close tshark session
       - *session_name:* the name of target ssh session
 
    Usage Example:
    | close msg session |
    | close msg session | session_name="dhcp_ssh_session" |  

    """
    keyword_name = "close_msg_session"

    try:
        SSH_SESSION[session_name].close_ssh_session()
        del SSH_SESSION[session_name]
    except Exception as inst:
        raise MsgContinueError("%s:%s-> fail to close SSH session, exception: %s" \
        % (__name__,keyword_name, str(inst)))
    else:
        logger.info("Closed msg session '%s' " % session_name )

def start_capture_msg (nic_port,*args,**param) :
    """
    starting the capture process of tshark
    defaultly keep the capture message in /tmp directory

    - *nic_port:*     nic port, such as eth0, eth1, or eth2, etc                          
    - *args:     *    the options which can be supported by tshark, like "-c", "-a", "-R" 
    - *session_name:* the name of target ssh session  
    - *msg_path:*     the directory to keep captured traffic    
    - *RETURN:  *     tshark PID                                                         
    
    display_filter reference: 
    https://www.wireshark.org/docs/dfref/

    usage example :
    | start_capture_msg | eth0 | session_name=DHCP_session | msg_path= %{HOME} |
    | start_capture_msg | eth2 | -c 10 | -a duration:10 | -R bootp | 
    """
    logger.debug ("%s %s %s" % (nic_port,str(args),str(param)))
    keyword_name = "start_capture_msg"
    tshark_pid   = None
    session_name = "first_msg_session" 
    msg_path     = param.setdefault('msg_path','/tmp')
    file_name    = param.setdefault('file_name','') 
    session_name = param.setdefault('session_name','first_msg_session')
    
    logger.info("---------------------Start Capture Msg-----------------------")
    
    options = ""
    if args :
        try :
            for arg in args :
                options = options + arg + ' '
        except Exception as inst :
            raise MsgContinueError("%s:%s-> fail to check input, exception: %s" \
            % (__name__,keyword_name,str(inst))) 
        
    try:  
        res = SSH_SESSION[session_name].send_command("which tshark ")
        m = re.search("(/\S+/tshark)\s",res)
    except Exception as inst :
        tshark_cmd = "/usr/sbin/tshark"
    else :
        if m :
            tshark_cmd = m.group(1)
        else :
            tshark_cmd = "/usr/sbin/tshark"

    if not file_name : 
        file_name = 'dump_%s_%s.pcap' % (nic_port,session_name)
            
    try :
        tshark_remove_file  = "sudo rm -rf %s/%s" % (msg_path,file_name)     
        #remove old pcap file
        SSH_SESSION[session_name].send_command(tshark_remove_file)
    except Exception as inst :
        raise MsgContinueError("%s:%s-> fail to remove the possible pcap file, exception: %s" \
        % (__name__,keyword_name,str(inst)))  
 
    try :
        tshark_stop_all_cmd = "ps -ef | grep tshark | grep %s | awk \'{print $2}\' | xargs sudo kill -9" % nic_port 
        #stop current capture process
        SSH_SESSION[session_name].send_command(tshark_stop_all_cmd,prompt=".*\#|.*\%|.*\]\$")
        time.sleep(1)
    except Exception as inst :
        raise MsgContinueError("%s:%s-> fail to stop previous tshark capture, exception: %s" \
        % (__name__,keyword_name,str(inst)))  
		
    try :		
        #send start capture cmd
        tshark_capture_cmd  = "sudo %s -i %s %s -w %s/%s &" \
        % (tshark_cmd,nic_port,options,msg_path,file_name)  
        SSH_SESSION[session_name].send_command(tshark_capture_cmd)

        #get tshark pid
        tshark_get_pid_cmd  = "ps -ef | grep tshark "
        pid_result = SSH_SESSION[session_name].send_command(tshark_get_pid_cmd)
        m = re.search("(\d+)\r\n",pid_result)
        if m :
            tshark_pid = m.group(1)
            return tshark_pid
    except Exception as inst :
        raise MsgContinueError("%s:%s-> fail to start capture, exception: %s" \
        % (__name__,keyword_name,str(inst)))
	
    # set global variable list_packets_ethx as empty
    name_packets = "list_packets_" + session_name + '_' + nic_port
    globals()[name_packets] = [] 
    global MATCHED_PACKET
    MATCHED_PACKET = []
	
    return
	
		
def stop_capture_msg (nic_port,session_name="first_msg_session",msg_path="/tmp",file_name='') :
    """
    stop the capture process of tshark
    - * nic_port: eth0,eth1,or eth2,etc * 

    """
    keyword_name = "stop_capture_msg"
    logger.info("---------------------Stop Capture Msg-----------------------")
    try :
        # kill tshark process on this port 
        tshark_stop_all_cmd = "ps -ef | grep tshark | grep %s | awk \'{print $2}\' | xargs sudo kill -9" % nic_port
        SSH_SESSION[session_name].send_command(tshark_stop_all_cmd,prompt=".*\#|.*\%|.*\]\$")
        time.sleep (0.1) 
	# show file size
        if not file_name :
            file_name = 'dump_%s_%s.pcap' % (nic_port,session_name)

        pcap_file = "%s/%s" % (msg_path,file_name)
        SSH_SESSION[session_name].send_command("ls -l " +pcap_file)
	
    except Exception as inst:
        raise MsgContinueError("%s:%s-> fail to stop capture, exception: %s" \
        % (__name__,keyword_name,str(inst)))
    return pcap_file

def check_msg (nic_port,display_filter,check_options,*params) :
    """
    start capture at NIC; 
    stop the capture process after delay_time;
    check and return captured traffic.

    usage example :
    | check_msg | eth0 | all | bootp.option.type=50,bootp.option.type=55,bootp.option.dhcp=Discover |
    |   |  session_name = "dhcp_session" | delay_time=100 | msg_path=/tmp |
    | check_msg | eth1 | bootp | bootp.option.type=50,bootp.option.type=55,bootp.option.dhcp=Discover |

    - *nic_port:*         nic port, such as eth0, eth1, or eth2, etc                                                    
    - *display_filter:*   filter like bootp,icmp,etc. "all" means to display all packets
    - *check_options:*    example: bootp.option.type=50,bootp.option.type=55,
                          bootp.option.dhcp=Discover,packet_count<=5 
    - *params:*           param list: including param "expect_result", "packet_count",
                                     "delay_time","session_name","msg_path"
    - *RETURN:*           all packets captured as list
    
    display_filter reference:
    https://www.wireshark.org/docs/dfref/
    """
    keyword_name = "check_msg"
    logger.debug ("%s %s %s %s" % (nic_port,display_filter,check_options,str(params)))
    if display_filter.lower() != "all" and not re.search("(\A|\,)name\=",check_options) :
        check_options = check_options + "," + "name="+display_filter

    expect_result = "PASS"
    packet_count  = 0
    delay_time    = "5"
    session_name  = "first_msg_session"
    msg_path      = "/tmp"
    # parser param
    for each_param in params :
        if "delay_time" in each_param:
            name,delay_time = each_param.split("=")
            delay_time = delay_time.strip(" ") 
        if "expect_result" in each_param :
            name,expect_result = each_param.split("=")
            expect_result = expect_result.strip(" ")
        if "packet_count" in each_param :
            check_options = check_options + "," + each_param
        if "session_name" in each_param :
            name,session_name = each_param.split("=")
        if "msg_path" in each_param :
            name,msg_path = each_param.split("=")   
    try:
        start_capture_msg (nic_port,session_name=session_name)
        logger.info("-----sleep %s seconds------" % delay_time )
        time.sleep(float(delay_time))
        stop_capture_msg (nic_port,session_name,msg_path)
        
        list_packets = _analyse_traffic_in_xml(nic_port,display_filter,session_name,msg_path)
    except Exception as inst:
        s = sys.exc_info()
        raise MsgContinueError("%s:%s-> fail to analyze xml of traffic, line: %s, exception: %s" \
        % (__name__,keyword_name,s[2].tb_lineno,str(inst)))
              
    try:    
        acture_result = _validate_all_packets(check_options,list_packets)       
    except Exception as inst:
        s = sys.exc_info()
        raise MsgContinueError("%s:%s-> fail to validate traffic, line: %s, exception: %s" \
        % (__name__,keyword_name,s[2].tb_lineno,str(inst)))  
     
    if not acture_result.lower() == expect_result.lower() :
        msg = "traffic check result '%s' is not expected '%s'" \
        % (acture_result,expect_result)
        raise MsgContinueError(msg)
    else :
        logger.info ("traffic check pass")
        
    return list_packets

def get_dhcp_options (packet) : 
    """
    get dhcp options value from given packet
    -*packct:      dhcp packct which is got from check_captured_msg
    -*dict_dhcp_options: return value, the dict of dhcp options 

    usage example :
    | get_dhcp_options | ${packet} |

    """
    keyword_name = "get_dhcp_options"
    dict_dhcp_options = {}
    option_num        = ""
    
    try :
        for proto in packet :
            if proto.has_key("bootp.options") :
                list_option = proto["bootp.options"]
                break
        else :
            raise MsgContinueError("%s:%s-> no dhcp options in this packet:\n %s" \
            % (__name__,keyword_name,str(packet)))
    except Exception as inst :
        s = sys.exc_info()
        raise MsgContinueError("%s:%s-> fail to get dhcp options, lineno:%s, exception:%s" 
        % (__name__,keyword_name,s[2].tb_lineno,str(inst))) 

    for each_option in list_option :
        m = re.search('bootp.option.type::::Option: ?\((\d+)\)',each_option)
        if m :
            # it's a option title
            option_num = m.group(1) 
        else :
            # it's a option body
            l = each_option.split("::::")
            key = l[0]
            value = l[1]
            value1 = l[2]
            name_option_body = re.sub("bootp.option.","",key)
            name_key = "dhcp_option_" + option_num + "." + name_option_body
      
            dict_dhcp_options[name_key] = [value,value1]
  
    return dict_dhcp_options

def check_captured_msg (nic_port,display_filter,check_options,*params) :
    """
    stop the tshark capture process and check traffic info
    before call this function, should call "start_capture_msg" firstly.
	
    usage example :
    | check_captured_msg | eth0 | all | bootp.option.type=50,bootp.option.dhcp=Discover | session_name="dhcp_session" |
    | check_captured_msg | eth0 | bootp | bootp.option.type=50,bootp.option.type=55,bootp.option.dhcp=Discover | 
    |   |  packet_count<=5 | expect_result=FAIL | 
	
    - *nic_port:*          nic port, such as eth0, eth1, or eth2, etc                                                   
    - *display_filter:*    protocol relative filter like bootp,icmp,etc. "all" means to display all packets
    - *check_options: *    example: bootp.option.type=50,bootp.option.type=55,bootp.option.dhcp=Discover,packet_count<=5
    - *params:             param list: including param "expect_result", "packet_count", "session_name","msg_path * 

    display_filter reference:
    https://www.wireshark.org/docs/dfref/
    """
    keyword_name = "check_captured_msg"
    if display_filter.lower() != "all" and not re.search("(\A|\,)name\=",check_options) :
        check_options = check_options + "," + "name="+display_filter

    expect_result = "PASS"
    packet_count  = 0
    capt_reserve  = "FALSE"
    session_name  = "first_msg_session"
    msg_path      = "/tmp"
    # parser param
    for each_param in params :
        if "expect_result" in each_param :
            name,expect_result = each_param.split("=")
            expect_result = expect_result.strip(" ")
        if "packet_count" in each_param :
            check_options = check_options + "," + each_param
        if "capt_reserve" in each_param : 
            name,capt_reserve = each_param.split("=")   
            capt_reserve = capt_reserve.strip(" ")
        if "session_name" in each_param :
            name,session_name = each_param.split("=")  
        if "msg_path" in each_param :
            name,msg_path = each_param.split("=") 
        
    try:
	name_packets = "list_packets_" + session_name + '_' + nic_port
        packet_count = 0
        if not globals()[name_packets] :
            # keep packets list result as global variable
            stop_capture_msg(nic_port,session_name,msg_path)
            globals()[name_packets] = _analyse_traffic_in_xml(nic_port,display_filter,session_name,msg_path)
    except Exception as inst :
        raise MsgContinueError("%s:%s-> fail to stop tshark capture or analyze traffic, exception: %s " \
        % (__name__,keyword_name,str(inst)))
       
    try:    
        acture_result = _validate_all_packets(check_options,globals()[name_packets])       
    except Exception as inst:
        s = sys.exc_info()
        raise MsgContinueError("%s:%s-> fail to get traffic info, line: %s, exception: %s" \
        % (__name__,keyword_name,s[2].tb_lineno,str(inst)))  
     
    if not acture_result.lower() == expect_result.lower() :
        msg = "traffic check result '%s' is not expected '%s'" \
        % (acture_result,expect_result)
        raise MsgContinueError(msg)
    else :
        logger.info ("traffic check pass")

    global MATCHED_PACKET
    return MATCHED_PACKET

def scp_pcap_file_to_local(dest_file_path,local_file_path,session_name="first_msg_session") :
    """
    scp pcap file to local server.
    - *dest_file_path:     the path of pcap file on remote server *
    - *local_file_path:    the path of pcap file on local server  *
    """
    keyword_name = "scp_pcap_file_to_local" 
   
    try:
        ssh_client = SSH_SESSION[session_name].createSSHClient()
        scp = SCPClient(ssh_client.get_transport())
        scp.get(r'%s' % dest_file_path, r'%s' % local_file_path)
        logger.info("Scp Done: from DST host '%s' to LOCAL host '%s'" % (dest_file_path, local_file_path))
    except Exception as inst :
        s=sys.exc_info()
        raise MsgContinueError("%s:%s-> fail to scp pcap file, line: %s, exception: %s" \
        % (__name__,keyword_name,s[2].tb_lineno,str(inst)))

def _analyse_traffic_in_xml (nic_port,display_filter,session_name="first_msg_session",msg_path="/tmp") :
    """
    analyse the dump_eth0.pcap file and get a xml result.
    - *nic_port:           NIC,eth0,eth2 or eth3,etc              *
    - *display_filter:     the filter for display. bootp,icmp,etc *
    - *RETURN:             list of traffic packets                *

    display_filter reference:
    https://www.wireshark.org/docs/dfref/


    """
    keyword_name = "_analyse_traffic_in_xml"
    list_packets = []
    try :

        # clean up before creating xml file
        cmd = "ps -ef | grep tshark"
        SSH_SESSION[session_name].send_command(cmd)
        cmd_rm = "rm -rf %s/dump_%s_%s.xml " % (msg_path,nic_port,session_name)
        SSH_SESSION[session_name].send_command(cmd_rm)

        display_filter = display_filter.lower()
        if display_filter == "all" :
            # all means to display all packets without any display filter
            display_filter = "" 
       
        cmd = "sudo tshark -r %s/dump_%s_%s.pcap -V -T pdml -R \"%s\" > %s/dump_%s_%s.xml" \
        % (msg_path,nic_port,session_name,display_filter,msg_path,nic_port,session_name)
        result = SSH_SESSION[session_name].send_command(cmd)
        if "cut short" in result :
            time.sleep(1)
            SSH_SESSION[session_name].send_command(cmd)

        # show file size
        cmd_ls = "ls -l %s/dump_%s_%s.xml " % (msg_path,nic_port,session_name)
        SSH_SESSION[session_name].send_command(cmd_ls)
        #SSH_SESSION[session_name].send_command("cat %s/dump_%s_%s.xml " % (msg_path,nic_port,session_name) )

        path_result = SSH_SESSION[session_name].send_command("pwd")
        dst_file_path = '%s/dump_%s_%s.xml' % (msg_path,nic_port,session_name)
        local_file_path = GLOBAL_VARIABLES['${OUTPUT_DIR}']
        local_file_path = '%s/get_dump_%s_%s.xml'% (local_file_path,nic_port,session_name)
         
        ssh_client = SSH_SESSION[session_name].createSSHClient()
        scp = SCPClient(ssh_client.get_transport())
        scp.get(r'%s' % dst_file_path, r'%s' % local_file_path)
        logger.info("Scp Done: from DST host '%s' to LOCAL host '%s'" % (dst_file_path, local_file_path))

        list_packets = _convert_xml_to_dict_list(nic_port,local_file_path,session_name)
       
        return list_packets
    except Exception as inst :
        s=sys.exc_info()
        raise MsgContinueError("%s:%s-> fail to analyse traffic in xml format, line: %s, exception: %s" \
        % (__name__,keyword_name,s[2].tb_lineno,str(inst))) 
   

def _convert_xml_to_dict_list (nic_port,file_name,session_name="first_msg_session"):
    """
    
    """
    keyword_name = "_convert_xml_to_dict_list "
    list_packets = []
    list_proto   = []
    dict_proto   = {}

    try :
        tree = ET.parse(file_name)
    except Exception as inst :
        raise AssertionError("%s:%s-> There is no element in %s, exception:%s" \
        % (__name__,keyword_name, file_name, str(inst)) )

    packet_count = 0
    tree_name = ""
    try :
        for child in tree.iter() :
            if child.tag == "packet" : 
                if list_proto :
                    #add new element in list_proto and list_packets
                    list_proto.append(dict_proto)
                    list_packets.append(list_proto)                              

                dict_proto = {}
                list_proto = []

            if child.tag == "proto" :
                if dict_proto :
                   list_proto.append(dict_proto) 
                   dict_proto = {}
                dict_proto['name'] = child.attrib['name']
                if child.attrib.has_key('showname') :
                    dict_proto['showname'] = child.attrib['showname']
 
            if child.tag == "field" :
                show_name = ""
                value = ""
                show = ""
                field_name = child.attrib['name']
                if child.attrib.has_key('showname') : 
                    show_name = child.attrib['showname']
                    
                if child.attrib.has_key('value') :
                    value = child.attrib['value']
                
                if field_name== "" and show_name== "" :
                    if child.attrib.has_key('show') :
                        show = child.attrib['show']
                        if show != '' :
                            ls_show = show.split(':',1)
                            if len(ls_show) > 1 :
                                field_name = ls_show[0]
                                show_name = ls_show[1]
                            else :
                                show_name = show
 
                if dict_proto.has_key(field_name) :
                    show_name_string = dict_proto[field_name][0] + ';;;;;' + show_name
                    value_string     = dict_proto[field_name][1] + ';;;;;' + value
                    dict_proto[field_name] = [show_name_string,value_string] 
                else :
                    dict_proto[field_name] = [show_name,value]
                    
                # deal with dhcp options specially
                if "bootp.option" in child.attrib['name'] :
                    added_info = field_name + "::::" + show_name + "::::" + value
                    if not dict_proto.has_key("bootp.options") :
                        dict_proto["bootp.options"] = []
                    dict_proto["bootp.options"].append(added_info)
                 
        # add the last element in list_proto and list_packets.
        if dict_proto :
            sorted_list = sorted (dict_proto.iteritems(),key=lambda d:d[0])
            sorted_dict_proto = {}
            for item in sorted_list :
                sorted_dict_proto[item[0]] = item[1]
            list_proto.append(sorted_dict_proto)
            
        if list_proto:
            list_packets.append(list_proto) 
               
        return list_packets
           
    except Exception as inst :
        s=sys.exc_info()
        raise MsgContinueError("%s:%s-> fail to parse xml to dict list, line: %s, exception: %s" \
        % (__name__, keyword_name, s[2].tb_lineno, str(inst)))
           

def _convert_check_options_to_dict(str_check_options) :
    keyword_name = "_convert_check_options_to_dict"

    try :
        dict_check_options = {}
        list_check_options = str_check_options.split(",")
        value              = ""
        key                = ""
        for each in list_check_options :
            if "packet_count" in each :
                key = "packet_count"
                value = re.search("([<|=|>]+\d+)",each).group(1)
            else : 
                key,value = each.split("=")
                key   = re.sub('^\s*|\s*$','',key)
                value = re.sub('^\s*|\s*$','',value)

            if dict_check_options.has_key(key) : 
                value = dict_check_options[key] + ";;;;;" + value

            dict_check_options[key] = value

        return dict_check_options
    except Exception as inst :
        s = sys.exc_info()
        raise MsgContinueError("%s:%s-> fail to convert '%s' to dict, lineno:%s, exception:%s" \
        % (__name__,keyword_name,str_check_options,s[2].tb_lineno,str(inst)))
        
    
def _validate_all_packets (check_options,list_packets) :
    """
    comparing packets content with condition in check_options 
    """
    keyword_name = "_validate_packets"
    try :
        if not list_packets :
            logger.info("No Packets Received")
            return "FAIL"
 
        is_matched = "FAIL"
        is_check_packet_count = None

        compdict = _convert_check_options_to_dict(check_options)
        if compdict.has_key('packet_count'):
            is_check_packet_count = True
            expect_packets_oper = re.search("([<|=|>]+)",compdict['packet_count']).group(1)
            expect_packets = re.search('(\d+)',compdict['packet_count']).group(1)
            del compdict['packet_count']
        else :
            is_check_packet_count = False
        
 
        msg = "Received packet(s) : "+ str(len(list_packets))
        logger.info(msg)

        logger.info("check_options:%s" % str(compdict))       
 
        matched_packet_num = 0
        current_packet_num = 0

        for each_packet in list_packets :
            current_packet_num = current_packet_num + 1
            logger.trace ( "packet to check: \n" + str(each_packet))
            matched_packet = _validate_single_packet(compdict,each_packet,current_packet_num)
            if matched_packet :
                matched_packet_num = matched_packet_num + 1
                logger.debug ("packet No." + str(current_packet_num) + " matched")
                global MATCHED_PACKET
                MATCHED_PACKET = each_packet
            else :
                msg = "packet No.%s not matched:\n" % str(current_packet_num)
                i = 0
                for layer in each_packet :
                    msg = msg + layer['name'] + " :: " + str(layer) + "\n"
                logger.debug (msg)  

        logger.info (str(matched_packet_num)+" packet(s) matched")
        check_result = "FAIL"
        if not is_check_packet_count :
            logger.info ("Expected packet(s): >= 1")
            if matched_packet_num >= 1 :
                check_result = "PASS"
        else :
            logger.info ("Expected packet(s): "+str(expect_packets_oper)+str(expect_packets))

            if expect_packets_oper == "=" : 
                expect_packets_oper = "==" 
            check_packet_cond = "matched_packet_num %s %s" % (str(expect_packets_oper),expect_packets)
            is_matched = eval(check_packet_cond)
            if is_matched :
                check_result = "PASS"
            
        if check_result == "FAIL" :
            msg = "matched packet number '%s' is not expected " % str(matched_packet_num)
            logger.info("<span style=\"color: #FF8C00\">"+msg+"</span>",html=True)
        return check_result
        
    except Exception as inst :
        s = sys.exc_info()
        raise MsgContinueError("%s:%s -> fail to validate all packets, line: %s, exception: %s"\
        % (__name__,keyword_name,s[2].tb_lineno,str(inst)))

		        
def _validate_single_packet (compdict,packet,packet_id) : 

    all_key_matched = True
    if compdict.has_key('name') :
        for this_proto in packet :
            if this_proto['name'] == compdict['name'] :
                compdict.pop ('name')   
                break
        else :
            msg = "packet No.%s : protocol '%s' found" % (str(packet_id),key)
            logger.info("<span style=\"color: #FF8C00\">"+msg+"</span>",html=True)
            return [] 
    for key in compdict.keys() :
        is_this_key_matched = False  
        found_this_key = False
        for this_proto in packet :             
            if this_proto.has_key(key) :
                found_this_key = True
                # if compdict[key] has multiple values, should convert it to list
                compValue_list = []
                compValue_list = compdict[key].split(";;;;;")
                for this_compValue in compValue_list :
                    if this_compValue.lower() in str(this_proto[key][0]).lower() \
                    or this_compValue.lower() in str(this_proto[key][1]).lower() \
                    or this_compValue.lower() in str(this_proto[key]).lower():
                        is_this_key_matched = True
                        break
                    else :
                        msg = "packet No.%s (%s) : '%s' gotten is not expected '%s' for '%s'" \
                        % (str(packet_id),this_proto['name'],str(this_proto[key]),compdict[key],key)
                        logger.info("<span style=\"color: #FF8C00\">"+msg+"</span>",html=True)
                        
            if is_this_key_matched :
                break   ;# only if key is matched, we stop check another proto layer
        if not found_this_key :    
            msg = "packet No.%s : no option '%s' found" % (str(packet_id),key)
            logger.info("<span style=\"color: #FF8C00\">"+msg+"</span>",html=True) 
      
        if not is_this_key_matched :
            all_key_matched = False
 
    if all_key_matched == False :     
        return []
    else :
        return packet                 
 
class MsgContinueError (AssertionError) :
    ROBOT_CONTINUE_ON_FAILURE = True
            





            
