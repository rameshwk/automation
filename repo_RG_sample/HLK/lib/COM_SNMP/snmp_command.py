"""
To support linux version snmp operation, you need following package installed in your linux:
net-snmp.x86_64
net-snmp-devel.x86_64
net-snmp-utils.x86_64
"""
import os,sys,time,re,commands
from robot.api import logger
from pysnmp.entity.rfc3413.oneliner import cmdgen
from pysnmp.proto import rfc1902

ssh_path = os.environ.get('ROBOTREPO')+'/HLK/lib/COM_SSH'
if ssh_path not in sys.path :
    sys.path.append(ssh_path)
import ssh_command
     

CMD_GEN = cmdgen.CommandGenerator()

SNMP_SESSION = {}

def connect_snmp_service (snmp_client,username=None,password=None, \
    prompt='.*(#|\$|%)',port='22',session_name="first_ssh_snmp_session") :
    """ 
    Build up the snmp session based on SSH 
           
    service snmpd must be starting on snmp_client
    - *snmp_client: *   ip of snmp client 
    - *username:    *   username of the snmp client SSH server
    - *password:    *   password of the snmp client SSH server
    - *prompt:      *   the last prompt of SSH session
    - *session_name:*   session name of the connection, default value 'first_ssh_snmp_session'.
    - *RETURN VALUE :* 	SNMP_SESSION[session_name]
    
    history:    
      Jan 21th 2014, chunyan guo developed

    Usage Example:
    | connect snmp service | 135.251.200.171 | test | 123456 | .*(\\#|\\$|%) |  
    | connect snmp service | ${NET_SNMP_IP} | ${NET_SNMP_USER} | ${NET_SNMP_PWD} | session_name=second_snmp_session | 

    """

    logger.debug("%s-> snmp_client=%s username=%s password=%s \
    prompt=%s port=%s session_name=%s" \
    % (__name__,snmp_client,username,password,prompt,port,session_name))

    try:
        SNMP_SESSION[session_name] = ssh_command.connect_ssh \
	(snmp_client,port,username,password,prompt,session_name)
    except Exception as inst:
        raise AssertionError("%s-> fail to connect SSH, exception: %s" \
        % (__name__,inst))
    
    # may unnecessary to start snmpd service
    try :
        res = SNMP_SESSION[session_name].send_command('service snmpd restart')
    except Exception as inst:
        raise AssertionError("%s-> fail to start snmp service by '%s', \
        exception: %s" % (__name__,command,inst))        
    else :
        logger.info("SNMP REPLY<< %s " % res)   
    
    try :
        res = SNMP_SESSION[session_name].send_command('bash')
    except Exception as inst:
        raise AssertionError("%s-> fail to change to bash: '%s', \
        exception: %s" % (__name__,inst))
    else :
        logger.info("SNMP REPLY<< %s " % res)

    logger.debug("%s-> SSH session created: %s of %s " \
    % (__name__,session_name,str(SNMP_SESSION)))         
    return SNMP_SESSION[session_name]

def disconnect_snmp_service (session_name="first_ssh_snmp_session") :
    """ 
    Disconnect the SSH session for net-snmp in snmp client server
    
    history:    
      Jan 21th 2014, chunyan guo developed    

    - *session_name:* session name of the connection, default value 'first_ssh_snmp_session'. 
    
    Usage Example:
    | disconnect snmp service | 
    | disconnect snmp service | second_snmp_session |   
    """
    try :
        res = SNMP_SESSION[session_name].send_command('exit')
    except Exception as inst:
        raise AssertionError("%s-> fail to quit from bash: '%s', \
        exception: %s" % (__name__,inst))
    else :
        logger.info("SNMP REPLY<< %s " % res)    
 
    try:
        ssh_command.disconnect_ssh(session_name)
    except Exception as inst:
        raise AssertionError("%s-> fail to close SSH session, exception: %s" \
        % (__name__,inst))         
 
    else :
        logger.info("%s-> SSH session '%s' closed" % (__name__,session_name))
    
    SNMP_SESSION.pop(session_name)
    return


def send_snmp_set_command(command,timeout=0,session_name="first_ssh_snmp_session",\
expect_result="pass"):
    """
    send snmpset command by SSH session on snmp client
  
    - *command:*        snmp set command to be executed
    - *timeout:*        connection timeout time
    - *session_name:*   session name of the connection, default value 'first_ssh_snmp_session'.    
    - *expect_result:*  pass or fail, default value is "pass" 
    - *RETURN VALUE :*  pass
    
    history:    
      Jan 21th 2014, chunyagu  	Developed

    Usage Example:
    | send snmp set command | snmpset -v 2c -c dpoe 135.251.200.170 1.3.6.1.4.1.4491.2.1 i 3000 | session_name=snmp_session | 
    | ${set_mib_value} | send snmp set command | snmpset -v 2c -c dpoe 135.251.200.170 1.3.6.1.4.1.4491.2.1 i 3000 | 
    """
    
    logger.debug ("%s-> %s,%s " % (__name__,command,session_name))
    target_value = command.split(" ",8)[-1]
    target_value = target_value.strip ("'")
    target_value = target_value.strip ("\"")

    try :     
        resp = SNMP_SESSION[session_name].send_command(command,timeout=timeout)
    except Exception as inst:
        raise RobotNoBreakError("fail to send command '%s'\nexception %s" \
        % (command,inst))        
    else :
        logger.info("SNMP REPLY<< %s " % resp)

    error_info = ""
    result = "pass"    
    for item in ["Error in packet","WrongType","Bad value notation","notWritable","No Response"]:
       if item in resp :
           result = "fail"
           error_info = item
           break
    
    if result == "pass":
        m = re.search("= (\S+): (.*?)(\n|\r)+",resp)
        if m :
            mib_type = m.group(1)
            mib_value = m.group(2)
        else :
            result = "fail"
            error_info = "fail to parse response \n'%s'" % resp
         
    if result != expect_result :
        raise RobotNoBreakError("unexpected response for '%s'\n exception: '%s'" \
        % (command,resp))

    if expect_result=="pass":  
        return (mib_type,mib_value)
    else :
        return (result,error_info)


def send_snmp_get_command(command,timeout=0,session_name="first_ssh_snmp_session",\
expect_result="pass"):
    """
    send snmpget command by SSH session on snmp client
  
    - *command:*        snmp get command to be executed
    - *timeout:*        connection timeout time
    - *session_name:*   session name of the connection, default value 'first_ssh_snmp_session'.
    - *expect_result:*  pass or fail, default value is "pass"
    - *RETURN VALUE :*  mib type and mib value 
    
    history:    
      Jan 21th 2014, chunyagu  	Developed
      May 11th,2015, chunyagu   update to support multiple 

    Usage Example:
    | ${type} | ${value} | send_snmp_get_command | snmpget -v 2c -c dpoe 135.251.200.170 1.3.6.1.4.1.4491.2.1 |
    | send_snmp_get_command | snmpget -v 2c -c dpoe 135.251.200.170 1.3.6.1.4.1.4491.2.1 | 
    """
    
    logger.debug ("%s-> %s,%s " % (__name__,command,session_name))
    expect_result = expect_result.lower()
    try :     
        resp = SNMP_SESSION[session_name].send_command(command,timeout=timeout)
    except Exception as inst:
        raise RobotNoBreakError("fail to send command '%s'\nexception: %s" \
        % (command,inst))        
    else :
        logger.info("SNMP REPLY<< %s " % resp)
        
    (result,error_info) = __check_error (resp)
    
    if result == "pass":
        if not re.search("(\r|\n)+(.*) += +(.*)(\r|\n)+",resp) :
            result = "fail"
            error_info = "parse error for '%s'" % resp
        else :      
            mib_type = []
            mib_value = []  
            for item in re.findall("(\r|\n)+(.*?) += +(.*)",resp) :
                x,mib_oid,mib_desc = item
                m = re.search("(\S+): ?([^\r]+)",mib_desc)
                if m :
                    mib_type.append (m.group(1))
                    mib_value.append (m.group(2))
                else :
                    mib_type.append (None)
                    mib_value.append (None)     
    
    if result != expect_result :
        raise RobotNoBreakError("unexpected response for '%s'" % command) 
    
    if expect_result == "pass":
        if len(mib_type) == 1 :
            mib_type = mib_type[0]
            mib_value = mib_value[0]
        return (mib_type,mib_value)
    else:
        return (result,error_info)


def send_snmp_walk_command(command,timeout=0,session_name="first_ssh_snmp_session",\
expect_result="pass"):
    """
    send snmpwalk command by SSH session on snmp client
   
    - *command:*        snmp walk command to be executed
    - *timeout:*        connection timeout time
    - *session_name:*   session name of the connection, default value 'first_ssh_snmp_session'.    
    - *expect_result:*  pass or fail, default value is "pass"
    - *RETURN VALUE :*  result of snmp walk command
    
    history:    
      Jan 21th 2014, chunyagu  	Developed

    Usage Example:
    | ${get_mib_value} | send snmp walk command | snmpwalk -v 2c -c private 10.10.10.202 1.3.6.1.4.1.4491.2.1.25.1.1.1.1 |
    | send snmp walk command | snmpwalk -v 2c -c private 10.10.10.202 1.3.6.1.4.1.4491.2.1.25.1.1.1.1 |
    """
    
    logger.debug ("%s-> command=%s,%s " % (__name__,command,session_name))
    try :     
        resp = SNMP_SESSION[session_name].send_command(command,timeout=timeout)
    except Exception as inst:
        raise RobotNoBreakError("fail to send command '%s'\nexception: %s" \
        % (command,inst))        
    else :
        logger.info("SNMP REPLY<< %s " % resp)
        
    (result,error_info) = __check_error (resp)
    
    if result == "pass" :
        try:    
            m = re.search("(\S+ = .*(\r|\n)+)+",resp)
            normal_resp = m.group(0)
        except Exception as inst :
            result = "fail"
            error_info = "parse error for '%s'" % resp

    if result != expect_result :
        raise RobotNoBreakError("unexpected response for '%s'" % command)

    if expect_result == "pass":
        return normal_resp
    else:
        return error_info


def send_snmp_bulk_get_command(command,timeout=0,session_name="first_ssh_snmp_session",\
expect_result="pass"):
    """
    send snmpbulkget command by SSH session on snmp client
    
    snmpbulkget -v2c -Cn0 -Cr10 -c dpoe -t 100 135.251.202.192 \
    1.3.6.1.2.1.10.127.7.1.11.1.3 \
    1.3.6.1.2.1.10.127.7.1.3.1.7 \
    1.3.6.1.2.1.10.127.7.1.4.1.4 

    - *command:*        snmp bulk command to be executed
    - *timeout:*        connection timeout time
    - *session_name:*   session name of the connection, default value 'first_ssh_snmp_session'.    
    - *expect_result:*  pass or fail, default value is "pass"
    - *RETURN VALUE:*   all mib info gotten
    
    history:    
      May 12th 2015, chunyagu  	Developed

    Usage Example:
    | ${result_bulk} | send_snmp_bulk_get_command | snmpbulkget -On -Cn0 -Cr100 -v 2c -c private 10.10.10.202 1.3.6.1.4.1.44 1.3.6.1.4.1.41 |

    """
    
    logger.debug ("%s-> command=%s,%s " % (__name__,command,session_name))
    try :     
        resp = SNMP_SESSION[session_name].send_command(command,timeout=timeout)
    except Exception as inst:
        raise RobotNoBreakError("failed command '%s'\nexception: %s" \
        % (command,inst))        
    else :
        logger.info("SNMP REPLY<< %s " % resp)
        
    (result,error_info) = __check_error (resp)
    
    if result == "pass" :
        try:    
            m = re.search("(\S+ = \S+:.*(\r|\n)+)+",resp)
            normal_resp = m.group(0)
        except Exception as inst :
            result = "fail"
            error_info = "parse error for '%s'" % resp

    if result != expect_result :
        raise RobotNoBreakError("unexpected response for '%s'" % command)
      
    if expect_result == "pass":
        return normal_resp
    else:
        return error_info

def get_snmp_all_oids (command,timeout=0,session_name="first_ssh_snmp_session"): 
    """
    send snmpwalk or snmpbulkget command to get all retrieved oids
    
    - *command:*        snmp walk or bulkget command to be executed
    - *timeout:*        connection timeout time
    - *session_name:*   session name of the connection, default value 'first_ssh_snmp_session'.  
    - *RETURN VALUE :* (oids, values)
    
    history:    
      Jun 16th 2015, chunyagu  	Developed

    Usage Example:
    | ${core0_oids} | ${value} | get snmp all oids | snmpbulkget -v 2c -Cn0 -Cr33 -On -c private 135.251.200.202 .1.3.6.1.6.3.16.1.1 | 
    """
    
    logger.debug ("%s-> %s,%s " % (__name__,command,session_name))
    try :     
        resp = SNMP_SESSION[session_name].send_command(command,timeout=timeout)
    except Exception as inst:
        raise RobotNoBreakError("failed command '%s'\nexception: %s" \
        % (command,inst))        
    else :
        logger.info("SNMP REPLY<< %s " % resp)
        
    (result,error_info) = __check_error (resp)
    if result != "pass":
        raise RobotNoBreakError("failed command '%s'\nexception: %s" \
        % (command,resp))         
    
    mib_oids = []
    mib_values = []  
    for item in re.findall("(\r|\n){1}([^\s]+) += +([^\r]+)",resp) :
        x,mib_oid,mib_desc = item
        mib_oids.append(mib_oid)
        mib_values.append(mib_desc)
     
    return (mib_oids,mib_values)

def get_mib_OID_index(walk_value,search_value):
    """
    Searching from retrieved walk result, to get oid of searched value
    - *walk_value:*   retrieved walk result
    - *search_value:* want to searched value
    - *RETURN VALUE:*       OID index of matched searche value  
    
    Usage Example:
    | ${index} | get mib OID index | ${get_walk_value} | ${ont_mac} | 
    """
    keyword="get_mib_OID_index"

    walk_value = walk_value.replace(' ','')
    try :
        m = re.search("\.(\d+)=\S+:%s"%search_value,walk_value)    
    except Exception as inst :
        raise AssertionError("%s:%s -> \
        can't get mib OID index, exception: %s" \
        % (__name__,keyword, inst))

    if m:
        if m.group():
            logger.debug("group0:%s" % m.group(0))
            logger.debug("group1:%s" % m.group(1))
            return m.group(1)

    raise AssertionError("%s:%s -> \
        no matched mib OID!" % (__name__,keyword)) 
 
def get_snmp_raw_output (command,timeout=0,session_name="first_ssh_snmp_session"):
    """    
    send any snmp command by SSH session on snmp client server
    no error check introduced in this keyword
  
    - *command:*        snmp command to be executed
    - *timeout:*        connection timeout time
    - *session_name:*   session name of the connection, default value 'first_ssh_snmp_session'. 
    - *RETURN VALUE:*   the raw output of snmp command
    
    history:    
      Jan 21th 2014, chunyagu  	Developed

    Usage Example:
    | ${minrate} | get_snmp_raw_output | snmpwalk -v1 -c dpoe 135.251.200.130 1.3.6.1.4.1.4491.2.1.21.1.8.1.7 | 
    """
    
    logger.debug ("%s-> command=%s,session_name=%s " % (__name__,command,session_name))

    try : 
        res = SNMP_SESSION[session_name].send_command(command,timeout=timeout)
    except Exception as inst:
        raise RobotNoBreakError("%s-> %s" % (__name__,inst)) 
                    
    logger.info("SNMP REPLY<< %s" % res)
    return res


##########################################################################
# following keywords use python snmp packet
# not implemented since robot server may not be snmp client
##########################################################################
def snmp_set (snmp_agent,objects,snmp_version='2c',community='public',options=None) :
    """
    perform snmp SET operation

    the action is similar to Net-SNMP command:
    snmpset -v 2c -c public 10.1.1.1 1.3.6.1.2.1.1.9.1.3.1 s 'new value'
   
    - *snmp_agent:*   snmp agent ip address
    - *objects:*      mib oid
    - *snmp_version:* snmp version, default value is 2c
    - *community:*    snmp community, public, private,etc
    - *RETURN_VALUE:* the output result of snmp set command
 
    Usage Example:
    | snmp_set | 135.251.200.144 | 10.1.1.1 1.3.6.1.2.1.1.9.1.3.1 s 'new value' | 2c | dpoe |
    """
    object_items = ""
    for item in objects.split(',') :
        (object_id, object_value) = objects.split('=')
        (object_type, object_value) = object_value.split(':',1)
        object_items = object_items + \
        "("+object_id+",eval('rfc1902.'+"+object_type+")("+object_value+"))"+","
    object_items = object_items.rstrip(',')
    exec_command = "cmdGen.setCmd(cmdgen.CommunityData('"+community+"'),"\
    +"cmdgen.UdpTransportTarget(('"+snmp_agent+"',161)),"
    
    error_info, error_status, error_index, var_binds = CMD_GEN.setCmd(
        cmdgen.CommunityData(community),
        cmdgen.UdpTransportTarget((snmp_agent,161)),
        (object_id,eval('rfc1902.'+object_type)(object_value))
    )
    if error_info :
        raise RobotNoBreakError(error_info)
    else :
        if error_status :
            raise RobotNoBreakError ('%s at %s' % (
                error_status.prettyPrint(),
                error_index and var_binds[int(error_index)-1] or '?'
                )
            )
        else :
            logger.info('SNMP REPLY << ' + str(var_binds))
	    result_arr = {}
            for name,val in var_binds :
                result_arr[name.prettyPrint()]= val.prettyPrint()
            return result_arr   

def snmp_get (snmp_agent,objects,snmp_version='2c',community='public',options=None, expect_output=None) :
    """
    perform snmp GET operation

    the action is similar to Net-SNMP command:
    snmpget -v 2c -c public 10.1.1.1 1.3.6.1.2.1.1.9.1.3.1
    
    history:
      Jan 21th 2014, chunyagu developed

    - *snmp_agent:*   snmp agent ip address
    - *objects:*      mib oid
    - *snmp_version:* snmp version, default value is 2c
    - *community:*    snmp community, public, private,etc
    - *RETURN_VALUE:* the output result of snmp get command

    Usage Example:
    | snmp_get | 135.251.200.144 | 1.3.6.1.4.1.4491.2.1.21.1.1,1.3.6.1.4.1.4491.2.1.21.1.2 | 2c | dpoe |

    """
    global CMD_GEN
    
    object_items = ""
    for item in objects.split(',') :
        object_items = object_items + "'" + item + "'" + ","
    object_items = object_items.rstrip(',')

    exec_command ="CMD_GEN.getCmd(cmdgen.CommunityData('"+community+"')," \
    + "cmdgen.UdpTransportTarget(('"+snmp_agent+"',161))," \
    + object_items + ")"
    
    error_info, error_status, error_index, var_binds = eval(exec_command) 

    if error_info :
       raise RobotNoBreakError(error_info)
    else :
        if error_status :
            raise RobotNoBreakError ('%s at %s' % (
                error_status.prettyPrint(),
                error_index and var_binds[int(error_index)-1] or '?'
	    )
            )
        else :
            logger.info('SNMP REPLY << ' + str(var_binds))
            result_arr = {}
            for name,val in var_binds :
                result_arr[name.prettyPrint()] = val.prettyPrint()
    if expect_output and expect_output not in str(result_arr):
        raise RobotNoBreakError("fail to get expect output '%s' in result '%s'" \
        % (expect_output,str(result_arr))) 
    else :
        return result_arr

def snmp_walk (snmp_agent,objects,snmp_version='2c',community='public',options=None) :
    """
    perform snmp GETNEXT operation
    
    the action is similar to Net-SNMP command:
    snmpwalk -v2c -c public demo.snmplabs.com 1.3.6.1.2.1.2.2.1.2
    
    history:
      Jan 21th 2014, chunyagu developed   

    - *snmp_agent:*   snmp agent ip address
    - *objects:*      mib oid
    - *snmp_version:* snmp version, default value is 2c
    - *community:*    snmp community, public, private,etc
    - *RETURN_VALUE:* the output result of snmp walk command

    Usage Example:
    | snmp_walk | 135.251.200.144 | 1.3.6.1.4.1.4491.2.1.21.1.1,1.3.6.1.4.1.4491.2.1.21.1.2 | 2c | dpoe |
 
    """   
    
    global CMD_GEN
    
    object_items = ""
    for item in objects.split(',') :
        object_items = object_items + "'" + item + "'" + ","
    object_items = object_items.rstrip(',')
    
    exec_command ="CMD_GEN.nextCmd(cmdgen.CommunityData('"+community+"')," \
    + "cmdgen.UdpTransportTarget(('"+snmp_agent+"',161))," \
    + object_items + ")"
    
    error_info, error_status, error_index, var_bind_table = eval(exec_command) 
    if error_info :
        raise RobotNoBreakError(error_info)
    else :
        if error_status :
            raise RobotNoBreakError ('%s at %s' % (
                error_status.prettyPrint(),
                error_index and var_bind_table[-1][int(error_index)-1] or '?'
                )
            )
        else :
            logger.info('SNMP REPLY << ' + str(var_bind_table))
            result_arr = {}
            for var_bind_table_row in var_bind_table :
                for name,val in var_bind_table_row :
                    result_arr[name.prettyPrint()] = val.prettyPrint()
            return result_arr

def snmp_bulk_walk (snmp_agent,objects,snmp_version='2c',community='public',options=None) :
    """
    perform snmp GETBULK operation

    the action is similar to Net-SNMP command:
    snmpbulkwalk -v 2c -c public demo.snmplabs.com 1.3.6.1.2.1.2.2.1.2
   
    - *snmp_agent:*   snmp agent ip address 
    - *objects:*      mib oid
    - *snmp_version:* snmp version, default value is 2c
    - *community:*    snmp community, public, private,etc
    - *RETURN_VALUE:* the output result of snmp bulk walk command

    Usage Example:  
    | snmp_bulk_walk | 135.251.200.144 | 1.3.6.1.4.1.4491.2.1.21.1.1,1.3.6.1.4.1.4491.2.1.21.1.2 | 2c | dpoe |
      
    history:
      Jan 21th 2014, chunyagu developed
    """
   
    global CMD_GEN
    
    object_items = ""
    for item in objects.split(',') :
        object_items = object_items + "'" + item + "'" + ","
    object_items = object_items.rstrip(',')
    
    exec_command ="CMD_GEN.bulkCmd(cmdgen.CommunityData('"+community+"')," \
    + "cmdgen.UdpTransportTarget(('"+snmp_agent+"',161)),0,25," \
    + object_items + ")"
    
    error_info, error_status, error_index, var_bind_table = eval(exec_command) 

    if error_info :
        raise RobotNoBreakError(error_info)
    else :
        if error_status :
            raise RobotNoBreakError ('%s at %s' % (
                error_status.prettyPrint(),
                error_index and var_bind_table[-1][int(error_index)-1] or '?'
                )
            )
        else :
            logger.info('SNMP REPLY << ' + str(var_bind_table))
	    result_arr = {}
	    for var_bind_table_row in var_bind_table :
                    for name,val in var_bind_table_row :
                        result_arr[name.prettyPrint()] = val.prettyPrint()
            return result_arr


def __check_error (response) :
    if re.search ("No Response from", response) :
        return ("fail","No Response")
    elif re.search ("noSuchName", response) :
        return  ("fail","NoSuchName")
    elif re.search ("badValue", response) :
        return  ("fail","badValue")
    elif re.search ("No Such", response) :
        return  ("fail","No Such Object/Instance")
    else :
        return ("pass",None)
        
class RobotNoBreakError (AssertionError) :
    ROBOT_CONTINUE_ON_FAILURE = True
  


