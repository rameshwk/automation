#coding=utf-8
import os
import xml.dom.minidom

def _return_node_id(tr69node, node_id, nodename):
    print "current node_id is %s" % node_id
    print "current node_id.nodeName is %s" % node_id.nodeName
    print "current node_id's parentNode nodeName is %s" % node_id.parentNode.nodeName
    nodename = node_id.parentNode.nodeName + nodename
    print nodename
    if node_id.parentNode.nodeName == "#document":
        print '$'*100
        print "match tr69node finished, this node is the last node you need!!!!"
        print '$'*100
        return True
    else:
        if tr69node.find(nodename) != -1:
            print "%s is in the tr69 node %s" % (node_id.parentNode.nodeName, tr69node)
            return _return_node_id(tr69node, node_id.parentNode, nodename)
        else:
            print "%s is not in the tr69 node %s" % (node_id.parentNode.nodeName, tr69node)
            print "%s is not a valid node, please check the next one." % node_id
            return False

def get_nodes(path, node):
    fd = []
    print isinstance(node, list)
    if isinstance(node, list):
        fd = node
        print "node list is %s" % fd
    else:
        node_file = os.path.join(path, node)
        f = open(node_file, 'r')
        fr = f.readlines()
        print fr
        for i in fr:
            print i
        print len(fr)

        for i in fr:
            print len(i)
            if i.endswith('\r\n'):
                j = i[:-2]
            else:
                j = i[:-1]
            fd.append(j)

        for i in fd:
            print i
        print len(fd)
    return fd



def get_node_value(path, node, xml_file):

    data_xml = os.path.join(path, xml_file)
    dom = xml.dom.minidom.parse(data_xml)
    root = dom.documentElement
    print 'node name is: %s' % root.nodeName
    print 'node vlaue is %s' % root.nodeValue
    print 'node Type is %s' % root.nodeType
    print 'node Elenment is %s' % root.ELEMENT_NODE
    fd = []
    print isinstance(node, list)
    if isinstance(node, list):
        fd = node
        print "node list is %s" % fd
    else:
        node_file = os.path.join(path, node)
        f = open(node_file, 'r')
        fr = f.readlines()
        print fr
        for i in fr:
            print i
        print len(fr)

        for i in fr:
            print len(i)
            if i.endswith('\r\n'):
                j = i[:-2]
            else:
                j = i[:-1]
            fd.append(j)

        for i in fd:
            print i
        print len(fd)

    dict_getnodes = {}

    for j in fd:
        nodes_by_tag = root.getElementsByTagName(j.split('.')[-1])
        print nodes_by_tag
        print len(nodes_by_tag)
        for i in nodes_by_tag:
            if _return_node_id(j, i, i.nodeName):
                print i.getAttribute("v")
                if i.hasAttribute("v"):
                    print "the node get value %s" % i.getAttribute("v")
                    value = i.getAttribute("v")
                else:
                    print "this node have no value Attribute 'v', set value as 'NoValue' "
                    value = 'NoValue'
                dict_getnodes.setdefault(j, value)
    print type(fd)
    print len(fd)
    print dict_getnodes
    print len(dict_getnodes)
    return dict_getnodes

if __name__ == '__main__':
    nodelist = ['InternetGatewayDevice.LANDevice.1.WLANConfiguration.1.PreSharedKey.1.PreSharedKey', 'InternetGatewayDevice.LANDevice.1.WLANConfiguration.1.WEPKey.1.WEPKey', 'InternetGatewayDevice.LANDevice.1.WLANConfiguration.1.WEPKey.2.WEPKey', 'InternetGatewayDevice.LANDevice.1.WLANConfiguration.1.WEPKey.3.WEPKey', 'InternetGatewayDevice.LANDevice.1.WLANConfiguration.1.WEPKey.4.WEPKey', 'InternetGatewayDevice.LANDevice.1.WLANConfiguration.1.X_ASB_COM_radiusPassword2', 'InternetGatewayDevice.LANDevice.1.WLANConfiguration.1.radiusPassword', 'InternetGatewayDevice.ManagementServer.ConnectionRequestPassword', 'InternetGatewayDevice.ManagementServer.Password', 'InternetGatewayDevice.Services.VoiceService.1.VoiceProfile.1.H323.AuthPassword', 'InternetGatewayDevice.Services.VoiceService.1.VoiceProfile.1.Line.1.SIP.AuthPassword', 'InternetGatewayDevice.Services.VoiceService.1.VoiceProfile.1.Line.1.SIP.EventSubscribe.1.AuthPassword', 'InternetGatewayDevice.Services.VoiceService.1.VoiceProfile.1.Line.2.SIP.AuthPassword', 'InternetGatewayDevice.Services.VoiceService.1.VoiceProfile.1.Line.2.SIP.EventSubscribe.1.AuthPassword', 'InternetGatewayDevice.Services.VoiceService.1.VoiceProfile.1.SIP.InboundAuthPassword', 'InternetGatewayDevice.WANDevice.1.WANConnectionDevice.1.WANIPConnection.1.X_CT-COM_DDNSConfiguration.1.DDNSPassword', 'InternetGatewayDevice.WANDevice.1.WANConnectionDevice.1.WANPPPConnection.1.X_CT-COM_DDNSConfiguration.1.DDNSPassword', 'InternetGatewayDevice.WANDevice.1.X_ALU-COM_USBDongleInterfaceConfig.AccessPoint.Password', 'InternetGatewayDevice.X_ALU-COM_LanAccessCfg.FtpPassword', 'InternetGatewayDevice.X_ALU-COM_SmartHome.IotGWCtrl.ACSPassword', 'InternetGatewayDevice.X_ALU-COM_VPNDevice.VPNClient.Password', 'InternetGatewayDevice.X_ALU-COM_VPNDevice.VPNClient.Pskey', 'InternetGatewayDevice.X_ASB_COM_GPONInfo.SUBSLOCID.Password', 'InternetGatewayDevice.X_Authentication.Account.Password', 'InternetGatewayDevice.X_Authentication.Account.PresetPassword', 'InternetGatewayDevice.X_Authentication.WebAccount.Password', 'InternetGatewayDevice.X_Authentication.WebAccount.PresetPassword', 'InternetGatewayDevice.X_Authentication.X_ASB_COM_BDAccount.Password', 'InternetGatewayDevice.X_CT-COM_UserInfo.UserId']
    get_node_value('/tftpboot', 'nodes', 'datamodel.xml')


