import re,getopt
from robot.api import logger

def get_string_by_regexp(string, pattern):
    """get string by regexp
    """
    t = re.search(pattern, string)
    return t.group(1)

def list_to_dict(input_list):
    """switch list to dict
    """
    key_list = []
    value_list = []
    n_dict = {}
    for num,item in enumerate(input_list):
        if num%2 == 1:
            key_list.append(item)
        if num%2 == 0:
            value_list.append(item)
    n_dict = dict((key,value) for key,value in zip(key_list,value_list))
    return n_dict
