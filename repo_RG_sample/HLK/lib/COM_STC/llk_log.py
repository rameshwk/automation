# Common libraries
import inspect
from robot.api import logger

from robot.libraries.BuiltIn import BuiltIn
from robot.libraries.BuiltIn import RobotNotRunningError
import logging
import sys

def llk_log_begin_proc():
  """
  Log start of procedure. Print procedure name and arguments to log file.
  Args:
  Returns:
  Examples:
     llk_log_begin_proc()
  """
 
  sCallerName = llk_log_get_caller_name()  
  lArgs, lVargs, lKwargs, dKeyVals = llk_log_get_caller_args()
  
  sMessage = "START PROC: " + str(sCallerName) + ": "
  for sArg in lArgs:
    sMessage = sMessage + str(sArg) + "=" + str(dKeyVals[sArg]) + ","
  if lVargs != None:
    sMessage = sMessage + str(lVargs) + "=" + str(dKeyVals[lVargs]) + ","
  if lKwargs != None:
    sMessage = sMessage + str(lKwargs) + "=" + str(dKeyVals[lKwargs]) + ","
  
  llk_log_debug(sMessage)

def llk_log_end_proc(*args):
  """
  Log end of procedure.
  Args: List of returned values
  Returns:
  Examples:
     llk_log_end_proc(sTestResult)
  """
  
  sCallerName = llk_log_get_caller_name() 
  sMessage = "END PROC: " + str(sCallerName) + ": "  
  for sArg in args:
    sMessage = sMessage + str(sArg) + ","

  llk_log_debug(sMessage)
    
def llk_log_get_caller_name(skip=2):
  """
  Get a name of a caller in the format module.class.method
  Args:
    `skip` specifies how many levels of stack to skip while getting caller
    name. skip=1 means "who calls me", skip=2 "who calls my caller" etc.
  Returns:
    Caller name  (An empty string is returned if skipped levels exceed stack height)
  Examples:
      llk_log_get_caller_name(skip=2)  
  Code Source: https://gist.github.com/techtonik/2151727
  """ 
  stack = inspect.stack()
  start = 0 + skip
  if len(stack) < start + 1:
    return ''
  parentframe = stack[start][0]    
  
  name = []
  module = inspect.getmodule(parentframe)
  # `modname` can be None when frame is executed directly in console
  # TODO(techtonik): consider using __main__
  if module:
      name.append(module.__name__)
  # detect classname
  if 'self' in parentframe.f_locals:
      # I don't know any way to detect call from the object method
      # XXX: there seems to be no way to detect static method call - it will
      #      be just a function call
      name.append(parentframe.f_locals['self'].__class__.__name__)
  codename = parentframe.f_code.co_name
  if codename != '<module>':  # top level usually
      name.append( codename ) # function or a method
  del parentframe
  return ".".join(name)

def llk_log_get_caller_args(skip=2):
  """
  Get arguments passed to caller proc
  Args:
    `skip` specifies how many levels of stack to skip while getting caller
    name. skip=1 means "who calls me", skip=2 "who calls my caller" etc.
  Returns:
    Caller arguments (An empty string is returned if skipped levels exceed stack height)
  Examples:
    llk_log_get_caller_args(skip=2)
  """
  stack = inspect.stack()
  start = 0 + skip
  if len(stack) < start + 1:
    return ''
  oCaller = inspect.stack()[start][0]
  return inspect.getargvalues(oCaller)

def llk_log_debug(sMsg):
  _llk_log_msg('debug',sMsg)

def llk_log_info(sMsg):
  _llk_log_msg('info',sMsg)
  
def llk_log_trace(sMsg):
  _llk_log_msg('debug',sMsg)

def llk_log_warn(sMsg):
  _llk_log_msg('warn',sMsg)

def llk_log_error(sMsg):
  _llk_log_msg('error',sMsg)

def llk_log_msg(sMsgType,sMsg):
  _llk_log_msg(sMsgType,sMsg)


def _llk_log_msg(sMsgType,sMsg):
  """
  Manages logging to either Robot or Python
  depending on which environment is in use.
  Args:
    sMsgType: trace|debug|info|warn
    sMsg: Text message to display
  Returns:
  """ 
  try:
    BuiltIn().get_variables()
    # Robot Logger
    sLoggerLib = logger
  except RobotNotRunningError:
    # Python Logger
    if sMsgType.lower() == 'trace':  #Trace level not supported on python logger
      sMsgType = 'debug'
    oPyLogger = logging.getLogger()
    oPyLogger.setLevel(getattr(logging,sMsgType.upper()))
    sFormat = '%(asctime)s [%(levelname)s]: %(message)s'
    logging.basicConfig(format=sFormat)
    sLoggerLib = logging

  oLogger = getattr(sLoggerLib,sMsgType.lower())
  oLogger(sMsg)



