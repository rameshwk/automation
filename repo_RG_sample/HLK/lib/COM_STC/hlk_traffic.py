#!/usr/bin/env python

import os,sys,time,re

try:
    for lib_path in [\
    os.environ['ROBOTREPO'] +'/HLKS',
    os.environ['ROBOTREPO'] +'/LIBS/COM_STC'] :
        if lib_path not in sys.path:
            sys.path.append(lib_path)
except Exception as inst:
    raise AssertionError("%s -> Fail to set sys.path, %s" % (__name__,inst))

from llk_stc import *
from robot.libraries.BuiltIn import BuiltIn
from robot.api import logger
import data_translation

class hlk_traffic(object):

    ROBOT_LIBRARY_SCOPE = 'GLOBAL'

    def __init__(self):
        """
        Initialize the global variables and fetch the data from the platfrom.csv file to store in a dictionary.

        Args:
            N/A

        Return Value:
            N/A
        """
        #create an object of llk_stc for invoking llk_stc methods.
        self.oLlkStc = llk_stc()
        # Dictionary to map the input parameters of hlk_stc_config_streamblock as keys and the corresponding llk
        # configuration methods as values
        self.dStreamblockItf = {'ethii': self.oLlkStc.llk_stc_config_streamblock_ethii_itf,
                                'arp': self.oLlkStc.llk_stc_config_streamblock_arp_itf,
                                'rarp': self.oLlkStc.llk_stc_config_streamblock_rarp_itf,
                                'pppoe': self.oLlkStc.llk_stc_config_streamblock_pppoe_itf,
				'mpls_ip': self.oLlkStc.llk_stc_config_streamblock_mplsip_itf,
				'mpls_eth': self.oLlkStc.llk_stc_config_streamblock_mplseth_itf,
                                'cfm': self.oLlkStc.llk_stc_config_streamblock_cfm_itf,
                                'ccm': self.oLlkStc.llk_stc_config_streamblock_ccm_itf,
                                'lbr': self.oLlkStc.llk_stc_config_streamblock_lbr_itf,
                                'lbm': self.oLlkStc.llk_stc_config_streamblock_lbm_itf,
                                'ltm': self.oLlkStc.llk_stc_config_streamblock_ltm_itf,
                                'vlan': self.oLlkStc.llk_stc_config_streamblock_vlan_itf,
                                'ipv4': self.oLlkStc.llk_stc_config_streamblock_ipv4_itf,
                                'ipv6': self.oLlkStc.llk_stc_config_streamblock_ipv6_itf,
                                'igmp': self.oLlkStc.llk_stc_config_streamblock_igmp_itf,
                                'igmpv2': self.oLlkStc.llk_stc_config_streamblock_igmpv2_itf,
                                'tcp': self.oLlkStc.llk_stc_config_streamblock_tcp_itf,
                                'udp': self.oLlkStc.llk_stc_config_streamblock_udp_itf,
                                'dhcp': self.oLlkStc.llk_stc_config_streamblock_dhcp_itf,
                                'rip': self.oLlkStc.llk_stc_config_streamblock_rip_itf,
                                'load': self.oLlkStc.llk_stc_config_streamblock_load,
                                'frame': self.oLlkStc.llk_stc_config_streamblock,
                                'frameconfig':self.oLlkStc.llk_stc_streamblock_frameconfig
                                }

        self.dDeviceItf = { 'ethii': self.oLlkStc.llk_stc_config_device_ethii_itf,
                            'vlan': self.oLlkStc.llk_stc_config_device_vlan_itf,
                            'ipv4': self.oLlkStc.llk_stc_config_device_ipv4_itf,
                            'ipv6': self.oLlkStc.llk_stc_config_device_ipv6_itf,
                            'device': self.oLlkStc.llk_stc_config_device,
                            'ppp':self.oLlkStc.llk_stc_config_device_ppp_itf,
                            'pppoe':self.oLlkStc.llk_stc_config_device_pppoe_itf,
                            'ipv6ll': self.oLlkStc.llk_stc_config_device_ipv6_itf,
                          }

        self.dStreamFieldType = {'ethii': 'ethernet:ethernetii',
                                'ipv4': 'ipv4:IPv4',
                                'ipv6': 'ipv6:IPv6',
                                'tcp': 'tcp:Tcp',
                                'udp': 'udp:Udp'
                                }

        self.dAllowedParam = {'step':'stepvalue',
                              'count':'recyclecount',
                              'value':'data',
                              'usestreams': 'enablestream',
                              'incrtype': 'modifiermode'
                             }
        self.dLineSpeed = { '10M' : 'SPEED_10M',
                            '100M': 'SPEED_100M',
                            '1G': 'SPEED_1G',
                            '10G': 'SPEED_10G',
                            '40G': 'SPEED_40G',
                            '100G': 'SPEED_100G'
                          }
        self.dDeviceType = { 'dhcpv4client':[],
                             'dhcpv4server':[],
                             'igmphost':[],
                             'pppoeclient':[],
                             'pppoeserver':[],
                             'dhcpv6server':[],
                             'dhcpv6client':[],
                             '8021xclient':[],
                             'ospfv2router':[],
                             'ospfv3router':[],
                             'riprouter':[],
                             'bgprouter':[],
                             'syncedevice':[],
                             'eoamnode':[]
                           }

        self.dDevices = { }
        self.lStartDevices = []
        #STC port id object
        self.dPortIDObjects = {}
        # STC streamblock objects
        self.dStreamblockObjects = {}
        # List of streamblocks on which traffic is started
        self.lStartTrafficSB = []
        # List of ports on which capture is running
        self.lStartCapturePortID = []
        # Table data of TrafficMapData obtained from the platform.csv file
        self.ldTrafficMapData = data_translation.get_global_table('TrafficGenMapData')
        # Table data of STC Version obtained from the platform.csv file
        self.ldSTCVersionData = data_translation.get_table_line('TrafficToolsData',\
        'Type=SpirentTcl')
        # Table for STC Port Data
        self.ldSTCPortData = data_translation.get_global_table('TrafficGenPortData')
        # Method to update the dictionary of STC port id objects
        # Some products has no TralfficGenMapData table
        if self.ldTrafficMapData:
            self._hlk_traffic_create_port_id()
        
        # Measure object
        self.dMeasureObjects = {}
        # Dictionary with keys as a tuple of streamblockname , modifiername and value as streamblock object and
        # rangemodifier object
        self.dRangeModifier = {}

    def hlk_traffic_connect(self):
        """
        Connect to STC Ports.

        Args:
            N/A

        Returns:
            N/A

        Examples:
            hlk_traffic_startup()
        """
        llk_log_begin_proc()

        self.oLlkStc.llk_stc_connect()

        llk_log_end_proc()


    def hlk_traffic_disconnect(self):
        """
        Delete the Streamblock objects and disconnect from STC Ports

        Args:
            N/A

        Returns:
            N/A

        Example:
            hlk_traffic_disconnect()
        """
        llk_log_begin_proc()

        self.oLlkStc.llk_stc_disconnect()

        llk_log_end_proc()

    def hlk_traffic_delete_ports(self):
        """
        Delete the port objects and disconnect from STC Ports

        Args:
            N/A

        Returns:
            N/A

        Example:
            hlk_traffic_delete_ports()
        """
        llk_log_begin_proc()

        # unattach ports
        self.oLlkStc.llk_stc_disconnect()

        # delete ports
        if self.dPortIDObjects:
            for sPortID in self.dPortIDObjects.values():
                self.oLlkStc.llk_stc_delete(sPortID)
            self.dPortIDObjects.clear()

        # delete project
        self.oLlkStc.llk_stc_delete(self.oLlkStc.oStcProject)

        llk_log_end_proc()

    def hlk_traffic_cleanup(self):
        """
        Delete the Streamblock objects and disconnect from STC Ports

        Args:
            N/A

        Returns:
            N/A

        Example:
            hlk_traffic_cleanup()
        """
        llk_log_begin_proc()

        if self.dStreamblockObjects:
            for sStream in self.dStreamblockObjects.values():
                self.oLlkStc.llk_stc_delete(sStream)
            self.dStreamblockObjects.clear()
        self.oLlkStc.llk_stc_disconnect()

        llk_log_end_proc()

    def hlk_traffic_get_port_id(self, **kwargs):
        """
        Retrieve associated STC port used by ONT

        Args:

            ontMac(Required if ontName is not given): MAC address of the ONT/ONU.
                Default: 0
                Type: Integer

            ontUni(Required): ONT Uni port number.
                Default: ''
                Type: Integer

            lif(Required): Type of Line interface.
                Default: 0
                Type: Integer
                Values: Network, Client.

            ontName(Required if ontMac is not given): Name of the ONT/ONU.
                Default: 0
                Type: Integer

        Return:
            A unique combination of port IP address, Slot number and Port number(Ex : //124.3.3.104/6/10) referred as port ID.

        Example:
            1) ${portID} =  hlk_traffic_get_port_id   ontMac=184a.6f45.92df    ontUni=2    lif=Network

            2) ${portID} =  hlk_traffic.hlk_traffic_get_port_id    ontName=XE040_1    ontUni=2    lif=Network
       
        """
        llk_log_begin_proc()

        assert (kwargs.keys()), "No arguments given as input for hlk_traffic_get_port_id"
        dKwargs = dict(map(lambda (k,v): (str(k).lower(), str(v)), kwargs.iteritems()))
        llk_log_end_proc()
        assert (('ontmac' in dKwargs.keys() or 'ontname' in dKwargs.keys()) and 'ontuni' in dKwargs.keys() and 'lif' in dKwargs.keys()),\
            "ontMac/ontName, ontUni and lif should be given as input parameters for hlk_traffic_get_port_id"
        llk_log_end_proc()
        assert (str(dKwargs['lif']).lower() in ['network', 'client']),"lif takes the input value of Network or Client only." \
                                                                      "The given input for lif is %s " % str(dKwargs['lif']).lower()

        bMacCond = False
        bNameCond = False
        dTrafficMapData = {}
        sPortId = ''
        sSearchCond = ''

        sUni = dKwargs['ontuni']
        if sUni:
            del dKwargs['ontuni']
        else:
            raise Exception ("The input entered for ontuni %s is not valid" % str(sUni))

        sLif = dKwargs['lif'].lower()
        if sLif:
            del dKwargs['lif']
        else:
            raise Exception ("The input entered for lif %s is not valid" % str(sLif))

        if dKwargs.get('ontmac'):
            sSearchCond = dKwargs['ontmac']
            if sSearchCond:
                del dKwargs['ontmac']
            else:
                raise Exception ("The input entered for ontmac %s is not valid" % str(sSearchCond))
            bMacCond = True
        elif dKwargs.get('ontname'):
            sSearchCond = dKwargs['ontname']
            if sSearchCond:
                del dKwargs['ontname']
            else:
                raise Exception ("The input entered for ontname %s is not valid" % str(sSearchCond))
            bNameCond = True
        else:
            raise Exception (" The input value given for ontname/ontmac is not valid!!")

        if dKwargs:
            llk_log_warn("The input parameters %s given are being ignored" % ','.join(key) for key in dKwargs.keys())

        if sSearchCond:
            if bMacCond:
                dTrafficMapData = data_translation.get_table_line('TrafficGenMapData',"OntMac="+sSearchCond+","+"OntUni="+sUni)
            elif bNameCond:
                dTrafficMapData = data_translation.get_table_line('TrafficGenMapData',"OntName="+sSearchCond+","+"OntUni="+sUni)
            else:
                raise Exception("The input parameter entered for either ontmac or ontname is incorrect")
        else:
            llk_log_end_proc()
            raise Exception("The input value entered for ontmac/ontname " + str(sSearchCond) + " is not valid")

        if dTrafficMapData:
            if sLif == 'network':
                sPortId = '//' + str(dTrafficMapData['NetworkIPAddress'])+'/'+str(dTrafficMapData['NetworkSlot'])+'/'+\
                          str(dTrafficMapData['NetworkPort'])
            elif sLif == 'client':
                sPortId = '//' + str(dTrafficMapData['ClientIPAddress'])+'/'+str(dTrafficMapData['ClientSlot'])+'/'+\
                          str(dTrafficMapData['ClientPort'])
            else:
                raise Exception("The input given for lif " + str(sLif) + " is not valid!!")
        else:
            raise Exception("There is no entry found in the TrafficGenMapData table of platform.csv file matching the given input "
                                "of ontmac/ontname "+ str(sSearchCond) + " and ontuni " + str(sUni))

        llk_log_end_proc()
        return sPortId

    def _hlk_traffic_create_port_id(self):
        """
        Create a port id. This is for INTERNAL use only.

        Args:
            N/A
        Return:
            N/A

        Example:
            _hlk_traffic_create_port_id()
        """

        if self.ldTrafficMapData:
            self.dPortIDObjects = {}
            for dTrafficMapData in self.ldTrafficMapData:
                sNetworkPortId = '//' + str(dTrafficMapData['NetworkIPAddress'])+'/'+str(dTrafficMapData['NetworkSlot'])+\
                                 '/'+str(dTrafficMapData['NetworkPort'])

                sClientPortId = '//' + str(dTrafficMapData['ClientIPAddress'])+'/'+str(dTrafficMapData['ClientSlot'])+\
                                '/' + str(dTrafficMapData['ClientPort'])

                if sNetworkPortId:
                    if sNetworkPortId not in self.dPortIDObjects.keys():
                        self.dPortIDObjects[sNetworkPortId] = ''
                else:
                    raise Exception("Network Port ID not found in the platform.csv file")

                if sClientPortId:
                    if sClientPortId not in self.dPortIDObjects.keys():
                        self.dPortIDObjects[sClientPortId] = ''
                else:
                    raise Exception("Client Port ID not found in the platform.csv file")
        else:
            raise Exception("There are no entries found in the table TrafficGenMapData" )

    def hlk_traffic_config_port(self, **kwargs):
        """
        Configure specified traffic generator port.  Create port if it does not already exist.

        Args:
            portID(Required)- port id of the port obtained using hlk_traffic_get_port_id
                Default: ''
                Type: String

            Active - Whether this object will be active when you call the apply function.
                Values: TRUE, FALSE
                Default: TRUE
                Type: bool
            AdvancedInterleaving - Enable the option to interleave packets.
                Values: TRUE, FALSE
                Default: FALSE
                Type: bool
            BurstSize - Number of frames in each burst transmitted.
                Default: 1
                Type: u32
            Duration - Length of packet transmission.
                Default: 30
                Type: double
            DurationMode - Length of transmission mode (cont/bursts/secs).
                Values: BURSTS, CONTINUOUS, SECONDS, STEP, TABLE_REPETITIONS
                Default: CONTINUOUS
                Type: enum
            FixedLoad - Fixed load value.
                Default: 10
                Type: double
            InterFrameGap - Gap (bytes) between frames in the same burst.
                Default: 12
                Type: double
            InterFrameGapUnit - Unit for inter-frame gap.
                Values: BITS_PER_SECOND, BYTES, FRAMES_PER_SECOND, KILOBITS_PER_SECOND, MEGABITS_PER_SECOND, MILLISECONDS, NANOSECONDS, PERCENT_LINE_RATE
                Default: BYTES
                Type: enum
            JumboFrameThreshold - TX frame sizes above this threshold are jumbo frames.
                Default: 1518
                Type: s16
            LoadMode - Load mode as fixed or random rate.
                Values: FIXED, RANDOM
                Default: FIXED
                Type: enum
            LoadUnit - Load unit for overall port load (port-based scheduling).
                Values: BITS_PER_SECOND, FRAMES_PER_SECOND, INTER_BURST_GAP, KILOBITS_PER_SECOND, MEGABITS_PER_SECOND, PERCENT_LINE_RATE
                Default: PERCENT_LINE_RATE
                Type: enum
            Name - A user-defined name for this object.
                Default: "" (empty string)
                Type: string
            OversizeFrameThreshold - TX frame sizes above this threshold are over-sized frames.
                Default: 9018
                Type: s16
            RandomLengthSeed - Seed value for random frame length.
                Range: 1 - 16777215
                Default: 10900842
                Type: s32
            RandomMaxLoad - Maximum random port load (port-based scheduling).
                Default: 100
                Type: double
            RandomMinLoad - Minimum random port load (port-based scheduling).
                Default: 10
                Type: double
            SchedulingMode - Scheduling mode (port/rate/priority/manual).
                Values: MANUAL_BASED, PORT_BASED, PRIORITY_BASED, RATE_BASED
                Default: PORT_BASED
                Type: enum
            StepSize - Number of packets to transmit per step. Applicable to step mode.
                Default: 1
                Type: u16
            TimestampLatchMode - Position timestamp on frame location.
                Values: END_OF_FRAME, START_OF_FRAME
                Default: START_OF_FRAME
                Type: enum
            UndersizeFrameThreshold - TX frame sizes below this threshold are under-sized frames.
                Default: 64
                Type: s16
            MediaType - Port interface type
                Values: FIBER, COPPER
                Default: COPPER
                Type: string
            Speed - Line speed of port
                Values: 10M, 100M, 1G, 10G, 40G, 100G.
                Default: None
                Type: string
                        
        Return:
            N/A

        Example:
            1) hlk_traffic_config_port    portID=${port1_id}    burstSize=1    duration=5    durationMode=BURSTS    schedulingMode=PORT_BASED
               This configuration can be used to capture the packets

            2) hlk_traffic_config_port    portID=${port2_id}    burstSize=1    durationMode=CONTINUOUS    schedulingMode=PORT_BASED
               This configuration can be used to measure the rates of the traffic

        Important Note->
            1) If you want to configure the traffic on port for the purpose of capturing the packets, then it is
               necessary to use the DurationMode as BURSTS or SECONDS or STEP. If not then number of packets generated
               cannot be saved in a capture file resulting in unexpected behaviour.

            2) If you want to measure the rates of the traffic on the port, then it is suggested to make the DurationMode
               as CONTINUOUS as it takes some time for the traffic to stabilize.

            3) SchedulingMode should be made RATE_BASED if load(say 10 Mbps) is to be specified for a streamblock created
               under the port. This is not possible when SchedulingMode is configured as PORT_BASED.
        """
        llk_log_begin_proc()

        assert (kwargs.keys()), "No arguments given as input for hlk_traffic_config_port"
        dKwargs = dict(map(lambda (k,v): (str(k).lower(), str(v)), kwargs.iteritems()))
        llk_log_end_proc()

        assert ('portid' in dKwargs.keys()), "Input should consist of a port id to be created/configured"

        sPortId = str(dKwargs.get('portid'))

        bCreatePort = False

        if sPortId:
            if sPortId in self.dPortIDObjects.keys():
                sPortObject = self.dPortIDObjects.get(sPortId)
            else:
                llk_log_end_proc()
                raise Exception ("Port ID " + sPortId + " is not valid!! Please mention the correct port id to configure it")

            if sPortObject:
                llk_log_info("Since the port is already created, the input parameters will be used to configure it!!")
                del dKwargs['portid']

                self.oLlkStc.llk_stc_config_burstsize(sPortObject,**dKwargs)
            else:
                llk_log_debug("The port does not exist. So it will be created first before configuring.")

                sRegEx = re.match(r'//(.*?)/(.*?)/(.*)',sPortId)
                sIPaddress = sRegEx.group(1)
                sSlot = sRegEx.group(2)
                sPort = sRegEx.group(3)
                dTrafficGenPortData = data_translation.get_table_line('TrafficGenPortData',"IPAddress=" + sIPaddress +
                                                                      "," + "Slot=" + sSlot + ',' +
                                                                      "Port=" + sPort
                                                                      )

                sSpeed     = ""
                sMediaType = ""
                if dTrafficGenPortData:
                    sSpeed = str(dTrafficGenPortData['LineSpeed'])
                    sMediaType = str(dTrafficGenPortData['MediaType'])
                else:
                    try:
                        sMediaType = str(dKwargs.get('mediatype'))
                        sSpeed = str(dKwargs.get('speed'))
                    except Exception as inst:
                        raise Exception ("Entry corresponding to the port id "+ sPortId +" is not found in the platform.csv file")

                if sSpeed in self.dLineSpeed.keys():
                    sSpeed = self.dLineSpeed[sSpeed]
                elif sSpeed == 'x':
                    llk_log_debug("Port is configured at default Line Speed")
                    sSpeed = 'x'
                else:
                    raise Exception("The Line speed should be one of : 10M, 100M, 1G, 10G, 40G, 100G.")

                self.dPortIDObjects[sPortId] = self.oLlkStc.llk_stc_create_port(sIPaddress,sSlot,sPort,sMediaType,sPortId,sSpeed)

                del dKwargs['portid']
                try:
                    del dKwargs['speed']
                    del dKwargs['mediatype']
                except Exception as inst:
                    llk_log_debug("Delete speed and mediaType!")

                bCreatePort = True

                if bCreatePort:
                    llk_log_debug("The port has been created successfully and the input parameters will be used to configure it")
                    if dKwargs:
                        if dKwargs.keys():
                            self.oLlkStc.llk_stc_config_burstsize(self.dPortIDObjects[sPortId], **dKwargs)
                    #else:
                    #    llk_log_end_proc()
                    #    raise Exception ("There are no input arguments provided to configure the port")
                else:
                    raise Exception ("Unable to create the port wiht the port id " + sPortId)
        else:
            raise Exception ("The port id entered" + str(sPortId) + "is invalid")
        llk_log_end_proc()

    def hlk_traffic_config_streamblock(self,**kwargs):
        """
        Configure a streamblock. Create a streamblock if it does not exist.

        Args:
            name(Required): Name of the streamblock to be configured/created.
                Default:'Streamblock'
                Type: String

            Note -> ontMac,ontUni,lif,ontName are required only for creating streamblocks but not for configuring it.
            portId(Required if ontName or ontMac is not given): the port id of sending stream. 
                Default: N/A
                Type: String
                Format: //135.251.200.12/2/1

            ontMac(Required if portId or ontName is not given): MAC address of the ONT/ONU.
                Default: 0
                Type: Integer

            ontUni(Required if portId is not given): ONT Uni port number.
                Default: ''
                Type: Integer

            lif(Required if portId is not given): Type of Line interface.
                Default: 0
                Type: Integer
                Values: Network, Client.

            ontName(Required if portId or ontMac is not given): Name of the ONT/ONU.
                Default: 0
                Type: Integer

            Note -> Following are the parameters required to configure a streamblock. These are not required for creating one.

            type:
                The type of interface to be created for the streamblock.
                Default: 0
                Type: Integer
                Values: 'ethii', 'arp', 'vlan', 'ipv4', 'ipv6', 'tcp', 'udp', 'load' & 'frame'

            Based on the value of 'type', the following parameters can be given as input to configure it.
            i) ethii:
                dstMac -
                    Default: 00:00:01:00:00:01
                    Type: MACADDR
                srcMac -
                    Default: 00:10:94:00:00:02
                    Type: MACADDR
                etherType -
                    Values: 0200 (XEROX PUP), 0201 (PUP Addr Trans), 0400 (Nixdorf), 0600 (XEROX NS IDP), 0660 (DLOG),
                    0661 (DLOG2), 0800 (Internet IP), 0801 (X.75 Internet), 0802 (NBS Internet), 0803 (ECMA Internet),
                    0804 (Chaosnet), 0805 (X.25 Level 3), 0806 (ARP), 0807 (XNS Compatibility), 0808 (Frame Relay ARP),
                    8035 (RARP), 86DD (IPv6), 880B (PPP), 8809 (Slow Protocol), 8847 (MPLS Unicast), 8848 (MPLS Multicast),
                    8863 (PPPoE Discovery), 8864 (PPPoE Session), 88E7 (PBB), 8906 (FCoE), 8914 (FIP)
                    Default: 88B5 (Local Experimental Ethertype)
                preamble -
                    Default: 55555555555555d5
                    Type: OCTETSTRING

            ii) arp:
                hardware Hardware type
                    Type: HardwareByte
                    Default: 0001
                    Possible Values:
                    Value Description
                    0001 Ethernet
                ihAddr Hardware address length
                    Type: INTEGER
                    Default: 6
                ipAddr Protocol address length
                    Type: INTEGER
                    Default: 4
                    A text name for the object. This attribute is required when you use stream block modifiers such as RangeModifier,
                    RandomModifier, and TableModifier. See the description of the OffsetReference attribute for these modifier objects
                    for more information.
                Name
                    Type: string
                operation Operation code
                    Type: ARPOperationByte
                    Default: 1
                    Possible Values:
                    Value Description
                    0 Unknown
                    1 ARP Request
                    2 ARP Reply
                protocol Protocol type
                    Type: ProtocolByte
                    Default: 0800
                    Possible Values:
                    Value Description
                    0800 Internet IP
                senderHwAddr Sender hardware address
                    Type: MACADDR
                    Default: 00:00:01:00:00:02
                senderPAddr Sender IP address
                    Type: IPV4ADDR
                    Default: 192.85.1.2
                targetHwAddr Target hardware address
                    Type: MACADDR
                    Default: 00:00:00:00:00:00
                targetPAddr Target IP address
                    Type: IPV4ADDR
                    Default: 0.0.0.0

            iii) vlan:
                vlanX(X: Number of the vlan tag) -
                    Default: ''
                    Type: INTEGER (0-4093)

                priX(X: Number of the vlan tag) -
                    Default: 000
                    Type: BITSTRING or INTEGER (0-7)

                Note-> If multiple vlan tags are to be configured, then each one of them should be numbered.

            iv) ipv4:
                checksum -
                    Default: 0
                    Type: INTEGER
                destAddr -
                    Default: 192.0.0.1
                    Type: IPV4ADDR
                destPrefixLength -
                    Default: 24
                    Type: INTEGER
                fragOffset -
                    Default: 0
                    Type: INTEGER
                gateway -
                    Default: 192.85.1.1
                    Type: IPV4ADDR
                prefixLength -
                    Default: 24
                    Type: INTEGER
                protocol -
                    Values: 0 (HOPOPT), 1 (ICMP), 2 (IGMP), 3 (GGP), 4 (IP), 5 (ST), 6 (TCP), 7 (CBT), 8 (EGP), 9 (IGP),
                    10 (BBN-RCC-MON), 11 (NVP-II), 12 (PUP), 13 (ARGUS), 14 (EMCON), 15 (XNET), 16 (CHAOS), 17 (UDP),
                    18 (MUX), 19 (DCN-MEAS), 20 (HMP), 21 (PRM), 22 (XNS-IDP), 23 (TRUNK-1), 24 (TRUNK-2), 25 (LEAF-1),
                    26 (LEAF-2), 27 (RDP), 28 (IRTP), 29 (ISO-TP4), 30 (NETBLT), 31 (MFE-NSP), 32 (MERIT-INP), 33 (SEP),
                    34 (3PC), 35 (IDPR), 36 (XTP), 37 (DDP), 38 (IDPR-CMTP), 39 (TP++), 40 (IL), 41 (IPv6), 42 (SDRP),
                    43 (IPv6-Route), 44 (IPv6-Frag), 45 (IDRP), 46 (RSVP), 47 (GRE), 48 (MHRP), 49 (BNA), 50 (ESP),
                    51 (AH), 52 (I-NLSP), 53 (SWIPE), 54 (NARP), 55 (MOBILE), 56 (TLSP), 57 (SKIP), 58 (IPv6-ICMP),
                    59 (IPv6-NoNxt), 60 (IPv6-Opts), 62 (CFTP), 64 (SAT-EXPAK), 65 (KRYPTOLAN), 66 (RVD), 67 (IPPC),
                    69 (SAT-MON), 70 (VISA), 71 (IPCV), 72 (CPNX), 73 (CPHB), 74 (WSN), 75 (PVP), 76 (BR-SAT-MON),
                    77 (SUN-ND), 78 (WB-MON), 79 (WB-EXPAK), 80 (ISO-IP), 81 (VMTP), 82 (SECURE-VMTP), 83 (VINES),
                    84 (TTP), 85 (NSFNET-IGP), 86 (DGP), 87 (TCF), 88 (EIGRP), 89 (OSPFIGP), 90 (Sprite-RPC), 91 (LARP),
                    92 (MTP), 93 (AX.25), 94 (IPIP), 95 (MICP), 96 (SCC-SP), 97 (ETHERIP), 98 (ENCAP), 100 (GMTP),
                    101 (IFMP), 102 (PNNI), 103 (PIM), 104 (ARIS), 105 (SCPS), 106 (QNX), 107 (A/N), 108 (IPComp),
                    109 (SNP), 110 (Compaq-Peer), 111 (IPX-in-IP), 112 (VRRP), 113 (PGM), 115 (L2TP), 116 (DDX),
                    117 (IATP), 118 (STP), 119 (SRP), 120 (UTI), 121 (SMP), 122 (SM), 123 (PTP), 124 (ISIS over IPv4),
                    125 (FIRE), 126 (CRTP), 127 (CRUDP), 128 (SSCOPMCE), 129 (IPLT), 130 (SPS), 131 (PIPE), 132 (SCTP),
                    133 (FC), 134 (RSVP-E2E-IGNORE), 135 (Mobility Header), 136 (UDPLite), 137 (MPLS-in-IP),
                    253 (Experimental), 255 (Reserved)
                    Default: 253
                sourceAddr -
                    Default: 192.85.1.2
                    Type: IPV4ADDR
                totalLength -
                    Default: 20
                    Type: INTEGER
                ttl -
                    Default: 255
                    Type: INTEGER
                identification -
                    Type: INTEGER
                    Default: 0
                ihl -
                    Type: INTEGER
                    Default: 5
                version
                    Type: INTEGER
                    Default: 4

            v) ipv6:
                destAddr
                    Type: IPV6ADDR
                    Default: 2000::1
                destPrefixLength
                    Type: INTEGER
                    Default: 64
                flowLabel
                    Type: INTEGER
                    Default: 0
                gateway
                    Type: IPV6ADDR
                    Default: ::0
                hopLimit
                    Type: INTEGER
                    Default: 255
                    A text name for the object. This attribute is required when you use stream block modifiers such as RangeModifier,
                    RandomModifier, and TableModifier. See the description of the OffsetReference attribute for these modifier objects for more
                    information.
                Name
                    Type: string
                nextHeader
                    Type: IpProtocolNumbers
                    Default: 59
                Possible Values:
                    0 HOPOPT
                    1 ICMP
                    2 IGMP
                    3 GGP
                    4 IP
                    5 ST
                    6 TCP
                    7 CBT
                    8 EGP
                    9 IGP
                    10 BBN-RCC-MON
                    11 NVP-II
                    12 PUP
                    13 ARGUS
                    14 EMCON
                    15 XNET
                    16 CHAOS
                    17 UDP
                    18 MUX
                    19 DCN-MEAS
                    20 HMP
                    21 PRM
                    22 XNS-IDP
                    23 TRUNK-1
                    24 TRUNK-2
                    25 LEAF-1
                    26 LEAF-2
                    27 RDP
                    28 IRTP
                    29 ISO-TP4
                    30 NETBLT
                    31 MFE-NSP
                    32 MERIT-INP
                    33 SEP
                    34 3PC
                    35 IDPR
                    36 XTP
                    37 DDP
                    38 IDPR-CMTP
                    39 TP++
                    40 IL
                    41 IPv6
                    42 SDRP
                    43 IPv6-Route
                    44 IPv6-Frag
                    45 IDRP
                    46 RSVP
                    47 GRE
                    48 MHRP
                    49 BNA
                    50 ESP
                    51 AH
                    52 I-NLSP
                    53 SWIPE
                    54 NARP
                    55 MOBILE
                    56 TLSP
                    57 SKIP
                    58 IPv6-ICMP
                    59 IPv6-NoNxt
                    60 IPv6-Opts
                    62 CFTP
                    64 SAT-EXPAK
                    65 KRYPTOLAN
                    66 RVD
                    67 IPPC
                    69 SAT-MON
                    70 VISA
                    71 IPCV
                    72 CPNX
                    73 CPHB
                    74 WSN
                    75 PVP
                    76 BR-SAT-MON
                    77 SUN-ND
                    78 WB-MON
                    79 WB-EXPAK
                    80 ISO-IP
                    81 VMTP
                    82 SECURE-VMTP
                    83 VINES
                    84 TTP
                    85 NSFNET-IGP
                    86 DGP
                    87 TCF
                    88 EIGRP
                    89 OSPFIGP
                    90 Sprite-RPC
                    91 LARP
                    92 MTP
                    93 AX.25
                    94 IPIP
                    95 MICP
                    96 SCC-SP
                    97 ETHERIP
                    98 ENCAP
                    100 GMTP
                    101 IFMP
                    102 PNNI
                    103 PIM
                    104 ARIS
                    105 SCPS
                    106 QNX
                    107 A/N
                    108 IPComp
                    109 SNP
                    110 Compaq-Peer
                    111 IPX-in-IP
                    112 VRRP
                    113 PGM
                    115 L2TP
                    116 DDX
                    117 IATP
                    118 STP
                    119 SRP
                    120 UTI
                    121 SMP
                    122 SM
                    123 PTP
                    124 ISIS over IPv4
                    125 FIRE
                    126 CRTP
                    127 CRUDP
                    128 SSCOPMCE
                    129 IPLT
                    130 SPS
                    131 PIPE
                    132 SCTP
                    133 FC
                    134 RSVP-E2E-IGNORE
                    135 Mobility Header
                    136 UDPLite
                    137 MPLS-in-IP
                    253 Experimental
                    255 Reserved
                payloadLength
                    Type: INTEGER
                    Default: 0
                prefixLength
                    Type: INTEGER
                    Default: 64
                sourceAddr
                    Type: IPV6ADDR
                    Default: 2000::2
                trafficClass
                    Type: INTEGER
                    Default: 0
                version
                    Type: INTEGER
                    Default: 6

            vi) tcp:
                ackBit
                    Type: BITSTRING
                    Default: 1
                ackNum
                    Type: INTEGER
                    Default: 234567
                checksum
                    Type: INTEGER
                    Default: Automatically calculated for each packet. (If you set this to 0, the checksum will not be calculated and will be
                    the same for each packet.)
                cwrBit
                    Type: BITSTRING
                    Default: 0
                destPort
                    Type: WellKnownPorts
                    Default: 1024
                    Possible Values:
                    1 TCPMUX
                    2 CompressNet
                    3 CompressProcess
                    5 RJE
                    7 ECHO
                    9 DISCARD
                    11 SYSTAT
                    13 DAYTIME
                    17 QOTD
                    18 MSP
                    19 CHARGEN
                    20 FTPDATA
                    21 FTP
                    22 SSH
                    23 Telnet
                    25 SMTP
                    27 NSW
                    29 MSG-ICP
                    31 MSG-AUTH
                    33 DSP
                    37 TIME
                    38 RAP
                    39 RLP
                    41 Graphics
                    42 Nameserver
                    43 WHOIS
                    44 MPM-flags
                    45 MPM
                    46 MPM-send
                    47 NI FTP
                    48 AUDIT
                    49 TACACS
                    50 RE-MAIL
                    51 LA-MAINT
                    52 XNS-time
                    53 DNS
                    54 XNS
                    55 ISI-GL
                    56 XNS-auth
                    69 TFTP
                    70 GOPHER
                    79 FINGER
                    80 HTTP
                    88 KERBEROS
                    119 NNTP
                    123 NTP
                    161 SNMP
                    162 SNMPTRAP
                    179 BGP
                    194 IRC
                    520 RIP
                    521 RIPNG
                    3784 BFD
                ecnBit
                    Type: BITSTRING
                    Default: 0
                finBit
                    Type: BITSTRING
                    Default: 0
                    A text name for the object. This attribute is required when you use stream block modifiers such as RangeModifier,
                    RandomModifier, and TableModifier. See the description of the OffsetReference attribute for these modifier objects for
                    more information.
                Name
                    Type: string
                offset
                    Type: INTEGER
                    Default: 5
                pshBit
                    Type: BITSTRING
                    Default: 0
                reserved
                    Type: BITSTRING
                    Default: 0000
                rstBit
                    Type: BITSTRING
                    Default: 0
                seqNum
                    Type: INTEGER
                    Default: 123456
                sourcePort
                    Type: INTEGER
                    Default: 1024
                synBit
                    Type: BITSTRING
                    Default: 0
                urgBit
                    Type: BITSTRING
                    Default: 0
                urgentPtr
                    Type: INTEGER
                    Default: 0
                window
                    Type: INTEGER
                    Default: 4096

            vii) udp:
                checksum
                    Type: INTEGER
                    Default: Automatically calculated for each packet. (If you set this to 0, the checksum will not be calculated and will be the same
                    for each packet.)
                destPort
                    Type: WellKnownPorts
                    Default: 1024
                    Possible Values:
                    1 TCPMUX
                    2 CompressNet
                    3 CompressProcess
                    5 RJE
                    7 ECHO
                    9 DISCARD
                    11 SYSTAT
                    13 DAYTIME
                    17 QOTD
                    18 MSP
                    19 CHARGEN
                    20 FTPDATA
                    21 FTP
                    22 SSH
                    23 Telnet
                    25 SMTP
                    27 NSW
                    29 MSG-ICP
                    31 MSG-AUTH
                    33 DSP
                    37 TIME
                    38 RAP
                    39 RLP
                    41 Graphics
                    42 Nameserver
                    43 WHOIS
                    44 MPM-flags
                    45 MPM
                    46 MPM-send
                    47 NI FTP
                    48 AUDIT
                    49 TACACS
                    50 RE-MAIL
                    51 LA-MAINT
                    52 XNS-time
                    53 DNS
                    54 XNS
                    55 ISI-GL
                    56 XNS-auth
                    69 TFTP
                    70 GOPHER
                    79 FINGER
                    80 HTTP
                    88 KERBEROS
                    119 NNTP
                    123 NTP
                    161 SNMP
                    162 SNMPTRAP
                    179 BGP
                    194 IRC
                    520 RIP
                    521 RIPNG
                    3784 BFD
                length
                    Type: INTEGER
                    Default: 0
                    A text name for the object. This attribute is required when you use stream block modifiers such as RangeModifier, RandomModifier,
                    and TableModifier. See the description of the OffsetReference attribute for these modifier objects for more information.
                Name
                    Type: string
                sourcePort
                    Type: INTEGER
                    Default: 1024

            viii) load:
                InterFrameGap - Gap (in bytes) between frames in same burst (priority-based scheduling).
                    Default: 12
                    Type: INTEGER
                InterFrameGapUnit - Unit for inter-frame gap.
                    Values: BITS_PER_SECOND, BYTES, FRAMES_PER_SECOND, KILOBITS_PER_SECOND, MEGABITS_PER_SECOND,
                    MILLISECONDS, NANOSECONDS, PERCENT_LINE_RATE
                    Default: BYTES
                Load - Load value set on stream block.
                    Default: 10
                    Type: double
                LoadUnit - Load unit applied to stream block.
                    Values: BITS_PER_SECOND, FRAMES_PER_SECOND, INTER_BURST_GAP, INTER_BURST_GAP_IN_MILLISECONDS,
                    INTER_BURST_GAP_IN_NANOSECONDS, KILOBITS_PER_SECOND, MEGABITS_PER_SECOND, PERCENT_LINE_RATE
                    Default: PERCENT_LINE_RATE

            ix) frame:
                FillType - Fill pattern type to be used for the payload.
                    Values: CONSTANT, DECR, INCR, PRBS
                    Default: CONSTANT
                    Type: enum
                FrameLengthMode - Frame length mode used by this stream.
                    Values: AUTO, DECR, FIXED, IMIX, INCR, RANDOM
                    Default: FIXED
                    Type: enum
                FixedFrameLength - Fixed value for frame length.
                    Range: 12 - 16383
                    Default: 128
                    Type: integer
                MaxFrameLength - Maximum frame length for random mode.
                    Range: 12 - 16383
                    Default: 256
                    Type: integer
                MinFrameLength - Minimum frame length for random mode.
                    Range: 12 - 16383
                    Default: 128
                    Type: integer

            dConfigParam: An input dictionary with the key value pairs as paramters and it's values to be used for configuring streamblocks.
                Default:None
                Type:Dictionary

        Return:
            N/A

        Example:
            1) hlk_traffic_config_streamblock    name=SB_1    ontMac=00:00:01:00:00:01    ontUni=2    lif=Network
               This would create a streamblock with name SB_1 at the port which is identified using the ontMac, ontUni and lif

            2) hlk_traffic_config_streamblock    name=SB_1    portid=//135.251.247.12/2/2    
               This would create a streamblock with name SB_1 at the port //135.251.247.12/2/2

            3) hlk_traffic_config_streamblock    name=SB_1    type=vlan    vlan1=4020    pri1=111
               This wold configure the streambock name SB_1 with a vlan interface.
               If the streamblock does not exist then it would throw an error.

            4) ${dEthernetParams} =  Create_Dictionary    srcMac=00:00:40:20:77:50    dstMac=00:00:01:00:00:01    etherType=0800
               hlk_traffic_config_streamblock    name=SB_2    type=ethii    dConfigParam=${dEthernetParams}    srcMac=00:10:94:40:20:01

               To replace any parameter in the dictionary, mention the key and it's new value as an argument.
               This would confgiure a streamblock named SB_2 (if it exists) with the parameters mentioned in the dictionary
               and replacing only the srcMac parameter.
               Note-> Key should be mentioned as 'dConfigParam' inorder to consider the input dictionary

        """
        llk_log_begin_proc()

        assert (kwargs.keys()), "No arguments given as input for hlk_traffic_config_streamblock"
        dKwargs = dict(map(lambda (k,v): (str(k).lower(), v), kwargs.iteritems()))
        llk_log_end_proc()
        assert ( (  ('ontmac' in dKwargs.keys() or 'ontname' in dKwargs.keys()) 
                        and 'name' in dKwargs.keys() 
                        and 'ontuni' in dKwargs.keys()
                        and 'lif' in dKwargs.keys()) 
                     or ('name' in dKwargs.keys() and 'type' in dKwargs.keys())
                     or ('name' in dKwargs.keys() and 'streamtype' in dKwargs.keys())
                     or ('name' in dKwargs.keys() and 'portid' in dKwargs.keys())),\
        "Input should consist of streamblock name along with ontmac/ontname, ontuni, lif or streamblock name " \
        "with type of interface and it's parameters to configure the streamblock"

        sSBName = str(dKwargs['name'])
        if sSBName:
            del dKwargs['name']
        else:
            raise Exception ("The input entered for streamblock name %s is not valid" % str(sSBName))

        if sSBName in self.dStreamblockObjects:
            # streamBlock has been created,only to config it
            if 'ontmac' in dKwargs.keys() or 'ontname' in dKwargs.keys():
                llk_log_info("Streamblock is already created!!")
                if dKwargs.get('ontmac'):
                    del dKwargs['ontmac']
                if dKwargs.get('ontname'):
                    del dKwargs['ontname']
                if dKwargs.get('ontuni'):
                    del dKwargs['ontuni']
                if dKwargs.get('lif'):
                    del dKwargs['lif']
            if 'type' in dKwargs.keys() or 'streamtype' in dKwargs.keys():
                self._hlk_traffic_config_streamblock_itf(sSBName,dKwargs)
        elif 'portid' in dKwargs.keys() :
            # if portid is given, create a streamblock on this port
            sPortId = dKwargs['portid'] 
            if sPortId in self.dPortIDObjects.keys():
                if self.dPortIDObjects[sPortId]:
                    oStreamBlock = self.oLlkStc.llk_stc_create_streamblock(self.dPortIDObjects[sPortId],sSBName)
                    self.dStreamblockObjects[sSBName] = oStreamBlock
                    llk_log_info("stream block:%s->%s" % (sSBName, self.dStreamblockObjects[sSBName]))
                    #self.dStreamblockObjects[sSBName].append(oStreamBlock)
                else:
                    llk_log_end_proc()
                    raise Exception("Port not created for port id %s.Please create it first before creating a streamblock" 
                    % sPortId)
            else:
                raise Exception("Port ID not found!! Please mention the port ID in the correct format!!")

        elif ('ontmac' in dKwargs.keys() or 'ontname' in dKwargs.keys()) and 'ontuni' in dKwargs.keys() and 'lif' in dKwargs.keys():
            # If ontmac or ontname is given, find out its port and then create a Streamblock 
            bMacCond = False
            bNameCond = False
            sSearch = ''
            sPortId = ''
            dTrafficMapData = {}

            if 'ontmac' in dKwargs.keys():
                sSearch = str(dKwargs['ontmac'])
                if sSearch:
                    bMacCond = True
                    del dKwargs['ontmac']
                else:
                    raise Exception ("The input entered for ontmac %s is not valid" % str(sSearch))
            elif 'ontname' in dKwargs.keys():
                sSearch = str(dKwargs['ontname'])
                if sSearch:
                    bNameCond = True
                    del dKwargs['ontname']
                else:
                    raise Exception ("The input entered for ontname %s is not valid" % str(sSearch))

            sUni = str(dKwargs['ontuni'])
            if sUni:
                del dKwargs['ontuni']
            else:
                raise Exception ("The input entered for ontuni %s is not valid" % str(sUni))

            sLif = str(dKwargs['lif'])
            if sLif:
                del dKwargs['lif']
            else:
                raise Exception ("The input entered for lif %s is not valid" % str(sLif))

            if sSearch:
                if bMacCond:
                    dTrafficMapData = data_translation.get_table_line('TrafficGenMapData',"OntMac="+sSearch+","+"OntUni="+sUni)
                elif bNameCond:
                    dTrafficMapData = data_translation.get_table_line('TrafficGenMapData',"OntName="+sSearch+","+"OntUni="+sUni)
                else:
                    raise Exception("The input parameter entered for either ontmac or ontname is incorrect")
            else:
                llk_log_end_proc()
                raise Exception("The input value entered for ontmac/ontname " + str(sSearch) + " is not valid")

            if dTrafficMapData:
                if sLif.lower() == 'client':
                    sPortId = '//' + str(dTrafficMapData['ClientIPAddress'])+'/'+str(dTrafficMapData['ClientSlot'])+\
                                    '/' + str(dTrafficMapData['ClientPort'])
                elif sLif.lower() == 'network':
                    sPortId = '//' + str(dTrafficMapData['NetworkIPAddress'])+'/'+str(dTrafficMapData['NetworkSlot'])+\
                                                     '/'+str(dTrafficMapData['NetworkPort'])
                else:
                    raise Exception("The input given for lif " + str(sLif) + " is not valid!!")
            else:
                raise Exception("There is no entry found in the TrafficGenMapData table of platform.csv file matching the given input "
                                "of ontmac/ontname "+ str(sSearch) + " and ontuni " + str(sUni))

            if sPortId in self.dPortIDObjects.keys():
                if self.dPortIDObjects[sPortId]:
                    oStreamBlock = self.oLlkStc.llk_stc_create_streamblock(self.dPortIDObjects[sPortId],sSBName)
                    self.dStreamblockObjects[sSBName] = oStreamBlock
                    #self.dStreamblockObjects[sSBName].append(oStreamBlock)
                else:
                    llk_log_end_proc()
                    raise Exception("Port not created for port id %s.Please create it first before creating a streamblock" % sPortId)
            else:
                raise Exception("Port ID not found!! Please mention the port ID in the correct format!!")

            if 'type' in dKwargs.keys() or 'streamtype' in dKwargs.keys():
                self._hlk_traffic_config_streamblock_itf(sSBName,dKwargs)
        else:
            llk_log_end_proc()
            raise Exception, "You are tyring to configure a streamblock which is not created yet. Please create the " \
                                            "streamblock first and then configure it"
        llk_log_end_proc()

    def _hlk_traffic_config_streamblock_itf(self, sSBName, dKwargs):
        """
        Configure a streamblock interface. This is for INTERNAL use only

        Args:
            sSBName: Name of the streamblock to be configured
                Default: ''
                Type: String

            dKwargs: Dictionary of parameters to be configured
                Default: None
                Type: Dictionary

        Return:
            N/A
        """
        dLlkKwargs = {}

        # get sType from streamType or type argument
        sType = ''
        try:
            sType = dKwargs.pop('streamtype')
        except Exception as inst:
            sType = dKwargs.pop('type')

        dConfigParam = {}
        dConfigParam = dKwargs.get('dconfigparam')
        if dConfigParam:
            del dKwargs['dconfigparam']
            dConfigParam = dict(map(lambda (k,v): (str(k).lower(), v), dConfigParam.iteritems()))

        if dConfigParam and dKwargs.keys():
            for key in dKwargs.keys():
                dConfigParam[key] = dKwargs[key]
            dLlkKwargs = dConfigParam.copy()
        elif dConfigParam:
            dLlkKwargs = dConfigParam.copy()
        elif dKwargs.keys():
            dLlkKwargs = dKwargs.copy()

        if sType in self.dStreamblockItf.keys():
            self.dStreamblockItf[str(sType)](self.dStreamblockObjects[str(sSBName)], **dLlkKwargs)
        else:
            raise Exception ("The input value entered for type %s is invalid" % str(sType))

    def hlk_traffic_get_switch_vlan(self,**kwargs):
        """
        Get the switch vlan ID for a particular port.

        Args:
            ontMac(Required if ontName is not given): MAC address of the ONT/ONU.
                Default: 0
                Type: Integer

            ontUni(Required): ONT Uni port number.
                Default: ''
                Type: Integer

            lif(Required): Type of Line interface.
                Default: 0
                Type: Integer
                Values: Network, Client.

            ontName(Required if ontMac is not given): Name of the ONT/ONU.
                Default: 0
                Type: Integer

        Return:
            N/A

        Example:
            1) ${vlanID} =  hlk_traffic_get_switch_vlan   ontMac=184a.6f45.92df    ontUni=2    lif=Client

            2) ${vlanID} =  hlk_traffic_get_switch_vlan    ontName=XE040_1    ontUni=2    lif=Client

        """
        assert (kwargs.keys()), "No arguments given as input for hlk_traffic_get_switch_vlan"
        dKwargs = dict(map(lambda (k,v): (str(k).lower(), str(v)), kwargs.iteritems()))
        llk_log_end_proc()
        assert (('ontmac' in dKwargs.keys() or 'ontname' in dKwargs.keys()) and 'ontuni' in dKwargs.keys() and 'lif' in dKwargs.keys()),\
            "ontMac/ontName, ontUni and lif should be given as input parameters for hlk_traffic_get_switch_vlan"
        llk_log_end_proc()
        assert (str(dKwargs['lif']).lower() in ['network', 'client']),"lif takes the input value of Network or Client only." \
                                                                      "The given input for lif is %s " % str(dKwargs['lif']).lower()

        bMacCond = False
        bNameCond = False
        dSwitchVlanMapData = {}
        sSwitchVlan = ''
        sSearchCond = ''

        sUni = dKwargs['ontuni']
        if sUni:
            del dKwargs['ontuni']
        else:
            raise Exception ("The input entered for ontuni %s is not valid" % str(sUni))

        sLif = dKwargs['lif'].lower()
        if sLif:
            del dKwargs['lif']
        else:
            raise Exception ("The input entered for lif %s is not valid" % str(sLif))

        if dKwargs.get('ontmac'):
            sSearchCond = dKwargs['ontmac']
            if sSearchCond:
                del dKwargs['ontmac']
            else:
                raise Exception ("The input entered for ontmac %s is not valid" % str(sSearchCond))
            bMacCond = True
        elif dKwargs.get('ontname'):
            sSearchCond = dKwargs['ontname']
            if sSearchCond:
                del dKwargs['ontname']
            else:
                raise Exception ("The input entered for ontname %s is not valid" % str(sSearchCond))
            bNameCond = True
        else:
            raise Exception (" The input value given for ontname/ontmac is not valid!!")

        if dKwargs:
            llk_log_warn("The input parameters %s given are being ignored" % ','.join(key) for key in dKwargs.keys())

        if sSearchCond:
            if bMacCond:
                dSwitchVlanMapData = data_translation.get_table_line('SwitchVlanMapData',"OntMac="+sSearchCond+","+"OntUni="+sUni)
            elif bNameCond:
                dSwitchVlanMapData = data_translation.get_table_line('SwitchVlanMapData',"OntName="+sSearchCond+","+"OntUni="+sUni)
            else:
                raise Exception("The input parameter entered for either ontmac or ontname is incorrect")
            if dSwitchVlanMapData:
                if str(dSwitchVlanMapData['SwitchVlan']) != 'x':
                    sSwitchVlan = str(dSwitchVlanMapData['SwitchVlan'])
                else:
                    llk_log_info("There is no switch vlan configured for ontmac/ontname %s with ontuni %s" % (sSearchCond,sUni))
            else:
                llk_log_end_proc()
                raise Exception ("There is no entry found in the SwitchVlanMapData table of platform.csv file matching the given input "
                                 "of ontmac/ontname "+ str(sSearchCond) + " and ontuni " + str(sUni))

        llk_log_end_proc()
        return sSwitchVlan

    def hlk_traffic_start_traffic(self, **kwargs):
        """
        Start the traffic on streamblocks

        Args:
            streamblock (Required): Name of the streamblock
                Default: ''
                Type: String/List

        Return:
            N/A

        Example:
            1)hlk_traffic_start_traffic    streamblock=all

            2)hlk_traffic_start_traffic    streamblock=[SB_2]

            3)${lStartTrafficSB} =    Create_List    SB_1    SB_2
              hlk_traffic_start_traffic    streamblock=${lStartTrafficSB}
        """
        llk_log_begin_proc()

        assert (kwargs.keys()), "No arguments given as input for hlk_traffic_start_traffic"
        dKwargs = dict(map(lambda (k,v): (str(k), v), kwargs.iteritems()))
        llk_log_end_proc()
        assert ('streamblock' in dKwargs.keys()), "Streamblock names on which traffic is to be started must be specified"

        lStreamblockNames = []
        self.lStartTrafficSB = []
        if type(dKwargs['streamblock']) is list:
            lStreamblockNames = list(map(lambda (k): str(k), dKwargs['streamblock']))
        elif str(dKwargs['streamblock']).lower() == 'all':
            if self.dStreamblockObjects:
                lStreamblockNames = self.dStreamblockObjects.keys()
        else:
            lStreamblockNames = str(dKwargs['streamblock']).strip("[](){}").split(',')

        del dKwargs['streamblock']
        if dKwargs:
            llk_log_warn("The input parameters %s given are being ignored" % ','.join(key) for key in dKwargs.keys())

        if lStreamblockNames:
            for sStreamblockName in lStreamblockNames:
                if sStreamblockName in self.dStreamblockObjects.keys():
                    self.lStartTrafficSB.append(self.dStreamblockObjects[sStreamblockName])
                else:
                    llk_log_warn("Streamblock with name " + sStreamblockName + " is not created!!")
        else:
            raise Exception("No streamblock names are given in the input arguments!!")

        if self.lStartTrafficSB:
            self.oLlkStc.llk_stc_start_traffic(*self.lStartTrafficSB)
        else:
            raise Exception("Traffic could not be started as there are no streamblocks found with the given name in the input")

        llk_log_end_proc()

    def hlk_traffic_stop_traffic(self,**kwargs):
        """
        Stop the traffic on streamblocks

        Args:
            streamblock(Required): Name of the streamblock
                Default: ''
                Type: String/List

        Return:
            N/A

        Example:
            1)hlk_traffic_stop_traffic    streamblock=all

            2)hlk_traffic_stop_traffic    streamblock=[SB_2]

            3)${lStopTrafficSB} =    Create_List    SB_1    SB_2
              hlk_traffic_stop_traffic    streamblock=${lStopTrafficSB}
        """
        llk_log_begin_proc()

        assert (kwargs.keys()), "No arguments given as input for hlk_traffic_stop_traffic"
        dKwargs = dict(map(lambda (k,v): (str(k), v), kwargs.iteritems()))
        llk_log_end_proc()
        assert ('streamblock' in dKwargs.keys()), "Streamblock names on which traffic is to be stopped must be specified"

        lStopTrafficSBObjects = []
        if type(dKwargs['streamblock']) is list:
            lStopTrafficSBNames = list(map(lambda (k): str(k), dKwargs['streamblock']))
        elif str(dKwargs['streamblock']).lower() == 'all':
            if self.lStartTrafficSB:
                self.oLlkStc.llk_stc_stop_traffic(*self.lStartTrafficSB)
            else:
                raise Exception ("Traffic is not started on any streamblocks to be stopped!!")
            del dKwargs['streamblock']
            if dKwargs:
                llk_log_warn("The input parameters %s given are being ignored" % ','.join(key) for key in dKwargs.keys())
            llk_log_end_proc()
            return
        else:
            lStopTrafficSBNames = str(dKwargs['streamblock']).strip("[](){}").split(',')

        del dKwargs['streamblock']
        if dKwargs:
            llk_log_warn("The input parameters %s given are being ignored" % ','.join(key) for key in dKwargs.keys())

        if lStopTrafficSBNames:
            for sStopTrafficSBName in lStopTrafficSBNames:
                if sStopTrafficSBName in self.dStreamblockObjects.keys():
                    if self.dStreamblockObjects[sStopTrafficSBName] in self.lStartTrafficSB:
                        lStopTrafficSBObjects.append(self.dStreamblockObjects[sStopTrafficSBName])
                    else:
                        llk_log_warn("Traffic is not started on the streamblock " + sStopTrafficSBName)
                else:
                    llk_log_warn("Streamblock " + sStopTrafficSBName + " is not created!!")
        else:
            raise Exception("No streamblock names are given in the input arguments!!")

        if lStopTrafficSBObjects:
            self.oLlkStc.llk_stc_stop_traffic(*lStopTrafficSBObjects)
        else:
            raise Exception("No valid streamblocks given as input to stop the traffic!!")
        llk_log_end_proc()

    def hlk_traffic_delete_streamblock(self,**kwargs):
        """
        Delete Streamblocks

        Args:
            streamblock(Required): streamblock of the streamblock
                Default: ''
                Type: String/List

        Return:
            N/A

        Example:
            1)hlk_traffic_delete_streamblock    streamblock=all

            2)hlk_traffic_delete_streamblock    streamblock=[SB_2]

            3)${lDeleteSB} =    Create_List    SB_1    SB_2
              hlk_traffic_delete_streamblock    streamblock=${lDeleteSB}
        """
        llk_log_begin_proc()

        assert (kwargs.keys()), "No arguments given as input for hlk_traffic_delete_streamblock"
        dKwargs = dict(map(lambda (k,v): (str(k).lower(), v), kwargs.iteritems()))
        llk_log_end_proc()
        assert ('streamblock' in dKwargs.keys()), "Input should consist of name of the streamblock which is to be deleted"

        sSBName = str(dKwargs.get('streamblock'))

        lDeleteSBName = []

        if sSBName:
            if type(dKwargs['streamblock']) is list:
                lDeleteSBName = dKwargs['streamblock']
            elif str(dKwargs['streamblock']).lower() == 'all':
                lDeleteSBName = self.dStreamblockObjects.keys()
            else:
                lDeleteSBName = str(dKwargs['streamblock']).strip("[](){}").split(',')
        else:
            raise Exception("There is no input entered for streamblock names with key 'name'")

        if lDeleteSBName:
            for sDeleteSBName in lDeleteSBName:
                if sDeleteSBName in self.dStreamblockObjects.keys():
                    if self.dStreamblockObjects[sDeleteSBName]:
                        self.oLlkStc.llk_stc_delete(self.dStreamblockObjects[sDeleteSBName])
                        del self.dStreamblockObjects[sDeleteSBName]
                    else:
                        llk_log_warn("There is no streamblock object created for a streamblock with name %s" % str(sDeleteSBName))
                else:
                    llk_log_warn("Streamblock with name " + sDeleteSBName + "is not created!!")
        else:
            raise Exception("There is no input entered for streamblock names with key 'name'")
        llk_log_end_proc()

    def hlk_traffic_start_measure(self,**kwargs):
        """
        Start test result measurement of streamblock.

        Args:
            resultType(Required): Type of result measurement
                Type: string
                Default: "RxStreamSummaryResults"
                Values: RxStreamSummaryResults,TxStreamResults

            configType: Configuration object under which the results are to be collected.
                Type: string
                Default: "Streamblock"
                Values: Streamblock,Port,Project

            clearResults: Clear the results before starting taking the measurements
                Default : FALSE
                Type: bool
                Values: TRUE/FALSE

            waitTime: Waiting time before starting to collect the results
                Default : 0
                Type: Integer
            FileNamePrefix
                Type: string
                Default: "" (empty string)
            Interval
                Type: u32
                Default: 1

        Return:
            N/A

        Example:
            1) hlk_traffic_start_measure    clearResults=TRUE    waitTime=10    resultType=RxStreamSummaryResults    configType=StreamBlock
        """
        llk_log_begin_proc()
        if kwargs:
            dKwargs = dict(map(lambda (k,v): (str(k).lower(), v), kwargs.iteritems()))
        else:
            dKwargs = {}

        if 'clearresults' in dKwargs.keys() and str(dKwargs['clearresults']) is not '':
            assert (str(dKwargs['clearresults']).lower() in ['true','false']), \
                   "The value entered for clearresults should be either TRUE or FALSE but you have entered %s" % kwargs['clearresults']
            if str(dKwargs['clearresults']).lower() == 'true':
                # List of port's results to be cleared
                lClearPortList = self.dPortIDObjects.values()
                if lClearPortList:
                    self.oLlkStc.llk_stc_clear_results(*lClearPortList)
                else:
                    llk_log_warn("No ports exist in order to clear the result!!")
            else:
                llk_log_debug("Earlier results will not be cleared!!")
        else:
            llk_log_debug("Since clear results is not given, a default of FALSE is assumed")


        if 'filenameprefix' in dKwargs.keys():
            sFileNamePrefix = dKwargs['waittime']
        else:
            sFileNamePrefix = ""

        if 'interval' in dKwargs.keys():
            sInterval = dKwargs['interval']
        else:
            sInterval = "1"
        
        if 'waittime' in dKwargs.keys() and str(dKwargs['waittime']) is not '':
            self.oLlkStc.llk_stc_send_command("stc::sleep " + str(dKwargs['waittime']))
        else:
            llk_log_info("Since wait time is not mentioned, a default of 0 is assumed")

        lResultType = []
        if 'resulttype' not in dKwargs.keys():
            lResultType.append('RxStreamSummaryResults')
        elif type(dKwargs['resulttype']) is list:
            lResultType = dKwargs['resulttype']
        else:
            lResultType = str(dKwargs['resulttype']).strip("[](){}").split(',')
            if '' in lResultType:
                lResultType.remove('')
                lResultType.append('RxStreamSummaryResults')

        lConfigType = []
        if 'configtype' not in dKwargs.keys():
            lConfigType.append('StreamBlock')
        elif type(dKwargs['configtype']) is list:
            lConfigType = dKwargs['configtype']
        else:
            lConfigType = str(dKwargs['configtype']).strip("[](){}").split(',')
            if '' in lConfigType:
                lConfigType.remove('')
                lConfigType.append('StreamBlock')

        if lResultType:
            iLenConfigType = len(lConfigType)
            lResultType = list(set(lResultType))
            iLenResultType = len(lResultType)

            if iLenResultType != iLenConfigType:
                llk_log_info("For every result type to be configured there should be a corresponding config type mentioned."
                            "If not, then the config type is defaulted to 'StreamBlock'")

                if iLenConfigType < iLenResultType:
                    for i in range(iLenConfigType,iLenResultType):
                        lConfigType.append('StreamBlock')
                        llk_log_info("Default config type of StreamBlock assigned to %s" % str(lResultType[i]))
            #Subscribe to the test results
            for i in range(0,iLenResultType):
                self.dMeasureObjects[str(lResultType[i])] = self.oLlkStc.llk_stc_subscribe(resultType=lResultType[i],configType=lConfigType[i])
        else:
            llk_log_end_proc()
            raise Exception("Arguments for result type %s is not valid" % str(lResultType))
        llk_log_end_proc()

    def hlk_traffic_stop_measure(self,**kwargs):
        """
        Stops measurement of streamblocks. By default, all the measurements would be stopped.

        Args:
            resultType: Type of result measurement to be stopped which has been started earlier.
                Type: string
                Default: ''
                Values: RxStreamSummaryResults,TxStreamResults

        Return:
            N/A

        Example:
            hlk_traffic_stop_measure()
        """
        llk_log_begin_proc()
        if kwargs:
            dKwargs = dict(map(lambda (k,v): (str(k).lower(), v), kwargs.iteritems()))
        else:
            dKwargs = {}
            llk_log_info("Since there are no arguments given for hlk_traffic_stop_measure, all the measures running will be stopped")

        lResultType = []
        if dKwargs:
            if 'resulttype' in dKwargs.keys():
                if type(dKwargs['resulttype']) is list:
                    lResultType = dKwargs['resulttype']
                else:
                    lResultType = str(dKwargs['resulttype']).strip("[](){}").split(',')
                    if '' in lResultType:
                        lResultType.remove('')
            else:
                llk_log_info("Arguments entered other than result type are ignored")

            if lResultType:
                for sResultType in lResultType:
                    if sResultType in self.dMeasureObjects.keys():
                        self.oLlkStc.llk_stc_unsubscribe(self.dMeasureObjects[str(sResultType)])
                    else:
                        llk_log_info("Measure not initiated for the result type %s" % str(sResultType))
            else:
                llk_log_info("No valid result types entered in order to stop the measure. Hence all the running measures will be stopped")
                for oMeasure in self.dMeasureObjects.values():
                    self.oLlkStc.llk_stc_unsubscribe(oMeasure)
        else:
            for oMeasure in self.dMeasureObjects.values():
                self.oLlkStc.llk_stc_unsubscribe(oMeasure)
        llk_log_end_proc()

    def hlk_traffic_retrieve_measure(self, **kwargs):
        """
        Retrieve the test result parameters that were measured.
        Important Note-> This should be run while traffic is running.

        Args:
            streamblock(Required): Name of the streamblock
                Type: string
                Default: ''

            resultType(Required): Type of result measurement to be retrieved.
                Type: string
                Default: ''
                Values: RxStreamSummaryResults,TxStreamResults

            stream: Stream number of the streamblock
                Type: Integer
                Default: All streams will be retrieved

            measure: Measurement parameters to be retrieved.
                Type: String/List
                Default: ''
                Values: Depends on the resultType set in hlk_traffic_start_measure.

                For example, if the resultType is RxStreamSummaryResults, the following parameters are some that can be
                queried from the test data.

                BitCount: Count of total bits received.
                    Type: u64
                    Default: 0

                BitRate: Rate at which bits are received.
                    Type: u64
                    Default: 0

                DroppedFrameCount: Number of frames dropped in transit.
                    Type: u64
                    Default: 0

                FrameCount: Number of frames received.
                    Type: u64
                    Default: 0

                L1BitCount: Count of total layer1 bits received.
                    Type: u64
                    Default: 0

                L1BitRate: Rate at which layer1 bits are received.
                    Type: u64
                    Default: 0

            Note->
            For a complete list of measurable parameters, please refer to the Spirent Test Center documentation in
            the file path /apps/Spirent/Spirent_TestCenter_4.52/Spirent_TestCenter_Automation_Obj_Ref

        Return:
            Nested dictionary with each key value pair corresponding to a stream of a streamblock.
            The key is the name of the streamblock appended with the value of stream index with it.
            The value is a dictionary with all the data related to the parameters queried from the test result data.

            For example if the name of the streamblcok is US2 which has only one streamblock without any streams in it.
            The entry in the dictionary would be as follows.
            'US2:0' --> Contains the flowcount only.
            'US2:1' --> Entry for streamblock parameters collected from test data.

            {'US2:0': {'flowcount': '1'},
             'US2:1': {'prbsbiterrorratio': '0', 'droppedframepercent': '0', 'droppedframecount': '0', 'reorderedframecount': '0',
                    'lateframecount': '0', 'framecount': '100', 'bitrate': '936', 'page': '7', 'prbsbiterrorcount': '0'}
            }

            If there are multiple subflows associated with a streamblock, then there would be multiple entries each corresponding
            to a stream within the streamblock. In the below example, the streamblock name is DS1 and there are three streams

            {'DS1:0': {'flowcount': '3'},
            'DS1:1': {'prbsbiterrorratio': '0', 'droppedframepercent': '0', 'droppedframecount': '0', 'reorderedframecount': '0',
                    'lateframecount': '0', 'framecount': '9', 'bitrate': '0', 'page': '2', 'prbsbiterrorcount': '0'},
            'DS1:2': {'prbsbiterrorratio': '0', 'droppedframepercent': '0', 'droppedframecount': '0', 'reorderedframecount': '0',
                    'lateframecount': '0', 'framecount': '8', 'bitrate': '7496', 'page': '1', 'prbsbiterrorcount': '0'},
            'DS1:3': {'prbsbiterrorratio': '0', 'droppedframepercent': '0', 'droppedframecount': '0', 'reorderedframecount': '0',
                    'lateframecount': '0', 'framecount': '8', 'bitrate': '0', 'page': '1', 'prbsbiterrorcount': '0'},
            }

        Examples:
            1) ${Retreive_Measure1} = hlk_traffic_retrieve_measure    streamblock=US1    type=RxStreamSummaryResults
                                                                      measure=[L1BitRate,DroppedFrameCount]

            2) ${lMeasureParam} =  Create_List    BitRate    FrameCount
               ${Retreive_Measure2} =  hlk_traffic_retrieve_measure    streamblock=US1    type=RxStreamSummaryResults
                                                                       measure=${lMeasureParam}
        """
        llk_log_begin_proc()

        ddMeasureValues = {}
        dReturnMeasureValues = {}
        assert (kwargs.keys()), "No measure parameters given as input for hlk_traffic_retrieve_measure"
        dKwargs = dict(map(lambda (k,v): (str(k).lower(), v), kwargs.iteritems()))
        llk_log_end_proc()
        assert ('streamblock' in dKwargs.keys()),"Name of the streamblock to be retrieved is not given. " \
                                                 "Please mention it with key as 'streamblock' and value as streamblock name"

        assert ('measure' in dKwargs.keys()),"Parameters to be measured is not given. " \
                                           "Please mention the key as measure and value as a list of parameters to be measured"
        assert ('type' in dKwargs.keys()),"Type of measure to be started is not given. " \
                                           "Please mention the key as type and value as a type of measurement"
        sSBName = ''
        sType = ''

        if dKwargs.get('streamblock'):
            sSBName = dKwargs['streamblock']
            del dKwargs['streamblock']

        if dKwargs.get('type'):
            sType = dKwargs['type']
            del dKwargs['type']

        if dKwargs.get('stream'):
            if type(dKwargs['stream']) is list:
                lStreamIndex = dKwargs['stream']
            else:
                lStreamIndex = str(dKwargs['stream']).strip("[](){}").split(',')
        else:
            lStreamIndex = []

        if type(dKwargs['measure']) is list:
            lMeasureParameters = dKwargs['measure']
        else:
            lMeasureParameters = str(dKwargs['measure']).strip("[](){}").split(',')

        lMeasureParameters = map(lambda k: (str(k).lower().strip(' \'')), lMeasureParameters)

        if lMeasureParameters:
            if sType in self.dMeasureObjects.keys():
                ddMeasureValues = self.oLlkStc.llk_stc_measure_subflow(self.dMeasureObjects[str(sType)], *lMeasureParameters)
            else:
                raise Exception("The measure type requested %s is not started in hlk_traffic_start_measure" % str(sType))

            logger.debug("all sub flow:%s" % ddMeasureValues)

            if ddMeasureValues:
                if sSBName and lStreamIndex:
                    for sStreamIndex in lStreamIndex:
                        sMeasureKey = str(sSBName)+":"+str(sStreamIndex)
                        if sMeasureKey in ddMeasureValues.keys():
                            dReturnMeasureValues[sMeasureKey] = ddMeasureValues[sMeasureKey]
                        else:
                            llk_log_warn("Measure does not exist for the given Streamblock %s, stream count %s "% (str(sSBName),str(sStreamIndex)))
                elif sSBName and not lStreamIndex:
                    if str(sSBName)+":0" in ddMeasureValues.keys():
                        sTotalStreamCnt = int(ddMeasureValues[str(sSBName)+":0"]['flowcount'])
                        for i in range(0,sTotalStreamCnt+1):
                            sMeasureKey = str(sSBName)+":"+str(i)
                            if sMeasureKey in ddMeasureValues.keys():
                                dReturnMeasureValues[sMeasureKey] = ddMeasureValues[sMeasureKey]
                            else:
                                llk_log_warn("Measure does not exist for the given Streamblock %s, stream count %s" % (str(sSBName),str(i)))
                    else:
                        llk_log_end_proc()
                        raise Exception("Measure for the streamblock %s is not recorded!!" % str(sSBName))
                else:
                    dReturnMeasureValues = ddMeasureValues
            else:
                llk_log_end_proc()
                raise Exception("Measure values not found for the measure type %s" % str(sType))
        else:
            raise Exception ("Measure parameters entered %s is invalid" % str(lMeasureParameters))

        if dReturnMeasureValues:
            return dReturnMeasureValues
        else:
            raise Exception ("No measure values found for some/all of the Streamblock %s and the stream count %s" % (str(sSBName),str(lStreamIndex)))

    def hlk_traffic_verify_measure(self, **kwargs):
        """
        Verify the parameters that were measured during the tests.

        Args:
            streamblock(Required): Name of the streamblock
                Type: string
                Default: ''

            stream: Stream number of the streamblock
                Type: Integer
                Default: All streams will be verified

            resultType(Required): Type of result measurement to be retrieved.
                Type: string
                Default: ''
                Values: RxStreamSummaryResults,TxStreamResults

            dExpectedMeasure(Optional, if parameters are given as arguments):
                Dictionary with key value pairs of parameters that are to be verified. This is an optional input as the
                parameters can also be given directly as input to the method.

                If both the dictionary and the parameters are given, then the common parameters would be replaced by the
                one specified as an argument to the method rather than in the dictionary.

                Note->
                    Measure parameters can be given with tolerance level as well by appending _tol to the parameter.
                    The tolerance level can be in either percentage or an absolute value.

                    For example, if bitrate is to be measured then the input parameters can be given as
                    bitrate=30, bitrate_tol=5%

                    Also, a default tolerance level can also be specified by giving default_tol in percentage.
                    This is applied to all the parameters for which there is no tolerance level given. If not mentioned,
                    it is taken as 5%

                    For checking the inequality conditions, the criteria must be menioned as a list of conditions with key
                    as parameter name. This can be seen in the example 3 below.

                    Parameters that can be verified depends on the resultType.

                    For example, if the resultType is RxStreamSummaryResults, the following parameters are some that can be
                    queried from the test data.

                    BitCount, BitRate, DroppedFrameCount, FrameCount, L1BitCount, L1BitRate.

                    For a complete list of measurable parameters, please refer to the Spirent Test Center documentation which
                    can be found in the file path /apps/Spirent/Spirent_TestCenter_4.52/Spirent_TestCenter_Automation_Obj_Ref

        Return:
            N/A

        Example:
            1) ${dMeasureValues} =  Create_Dictionary    BitRate=30    BitRate_tol=13%    FrameCount=99    FrameCount_tol=9%

               hlk_traffic_verify_measure    streamblock=US1    stream=5    type=RxStreamSummaryResults
                                             dExpectedMeasure=${dMeasureValues}

               Note-> The key argument when providing dictionary as input should be 'dExpectedMeasure'

            2) hlk_traffic_verify_measure    streamblock=US1    stream=5    type=RxStreamSummaryResults    L1BitRate=1000000
                                             L1BitRate_tol=10    DroppedFrameCount=0    DroppedFrameCount_tol=0

            3) ${dMeasureValues} =  Create_Dictionary    BitRate=88888    BitRate_tol=5%    FrameCount=99    FrameCount_Tol=9%
                                                         droppedFrameCount=0    DroppedFrameCount_tol=1    l1Bitrate=1000000    L1bitRate_tol=10

               hlk_traffic_verify_measure    streamblock=US1    type=RxStreamSummaryResults    l1Bitrate=[<=1000010,>=99990]
                                    droppedFrameCount=[>=0]    FrameCount=[!=0]    dexpectedmeasure=${dMeasureValues}
        """

        assert (kwargs.keys()), "No measure parameters given as input for hlk_traffic_retrieve_measure"
        dKwargs = dict(map(lambda (k,v): (str(k).lower(), v), kwargs.iteritems()))
        llk_log_end_proc()
        assert ('type' in dKwargs.keys()), "Input should consist of the type of measure results to be retrieved"

        sExpResult = 'pass'
        
        for each in ['exp_result','expect_result'] :
            if dKwargs.has_key(each) :
                sExpResult = dKwargs.pop(each).lower()

        bResult = "PASS"
        sSBName = ''
        sType = ''
        sStreamIndex = ''
        lStreamIndex = []
        sTotalStreamCnt = ''
        dResultFailed = {}
        lStreamNumbers = []
        lVerifyParamRange = []
        lVerifyParamTol = []
        lRetrieveParam = set()

        if dKwargs.get('streamblock'):
            sSBName = dKwargs['streamblock']
            del dKwargs['streamblock']

        llk_log_info("=====================================================================")
        llk_log_info("Verify Measure Streamblock=%s" % sSBName)

        if dKwargs.get('type'):
            sType = dKwargs['type']
            del dKwargs['type']

        if dKwargs.get('stream'):
            sStreamIndex = dKwargs['stream']
            if sStreamIndex:
                if type(dKwargs['stream']) is list:
                    lStreamIndex = dKwargs['stream']
                else:
                    lStreamIndex = str(dKwargs['stream']).strip("[](){}").split(',')
            del dKwargs['stream']

        dExpectedMeasure = dKwargs.get('dexpectedmeasure')
        dLlkKwargs = {}

        if dExpectedMeasure:
            del dKwargs['dexpectedmeasure']
            dExpectedMeasure = dict(map(lambda (k,v): (str(k).lower(), v), dExpectedMeasure.iteritems()))

        if dExpectedMeasure and dKwargs.keys():
            for key in dKwargs.keys():
                dExpectedMeasure[str(key)] = dKwargs[key]
            dLlkKwargs = dExpectedMeasure.copy()
        elif dExpectedMeasure:
            dLlkKwargs = dExpectedMeasure.copy()
        elif dKwargs.keys():
            dLlkKwargs = dKwargs.copy()

        lVerifyParam = []
        sDefaultTol = '5%'

        if dLlkKwargs:
            if dLlkKwargs.keys():
                dLlkKwargs = dict(map(lambda (k,v): (str(k).lower(), v), dLlkKwargs.iteritems()))
            else:
                llk_log_end_proc()
                raise Exception("No input parameters given to verify!!")

        if 'default_tol' in dLlkKwargs.keys():
            if str(dLlkKwargs['default_tol'])[-1] == '%':
                sDefaultTol = str(dLlkKwargs['default_tol'])
            else:
                llk_log_end_proc()
                raise Exception ("The default tolerance should be given in percentage(%) units")
        else:
            llk_log_debug("Since default_tol is not mentioned in the input arguments, a value of 5% is set!!")

        for key in dLlkKwargs.keys():
            if '<' in str(dLlkKwargs[key]) or '=' in str(dLlkKwargs[key]) or '>' in str(dLlkKwargs[key]) or '!' in str(dLlkKwargs[key]):
                lRange = str(dLlkKwargs[key]).strip('[]{}()').split(';')
                lRetrieveParam.add(str(key))
                for sRange in lRange:
                    lVerifyParamRange.append(str(key)+str(sRange).strip(' '))
            elif '_tol' not in str(key):
                lVerifyParam.append(str(key).strip(' '))
                lRetrieveParam.add(str(key))

        for sVerifyParam in lVerifyParam:
            if str(sVerifyParam)+'_tol' in dLlkKwargs.keys():
                lVerifyParamTol.append(dLlkKwargs[str(sVerifyParam)+'_tol'])
            else:
                lVerifyParamTol.append(sDefaultTol)

        if lRetrieveParam :
            ddRetrieveMeasure = self.oLlkStc.llk_stc_measure_subflow(self.dMeasureObjects[str(sType)], *lRetrieveParam)
        else:
            llk_log_end_proc()
            raise Exception("No input parameters given to verify!!")

        if not sStreamIndex:
            if str(sSBName)+":0" in ddRetrieveMeasure.keys():
                sTotalStreamCnt = int(ddRetrieveMeasure[str(sSBName)+":0"]['flowcount'])
            else:
                raise Exception("Streamblock not found while retrieving measure")

        if sTotalStreamCnt:
            for i in range(1,sTotalStreamCnt+1):
                lStreamNumbers.append(i)
        elif lStreamIndex:
            lStreamNumbers = lStreamIndex
        else:
            llk_log_end_proc()
            raise Exception("No valid stream count found for the streamblock %s " % str(sSBName))

        for sStreamNumber in lStreamNumbers:
            sSBNameStream = str(sSBName)+":"+str(sStreamNumber)
            if sSBNameStream in ddRetrieveMeasure.keys():
                dStreamMeasureValues = ddRetrieveMeasure[sSBNameStream]
                if dStreamMeasureValues:
                    cnt = 0
                    lResultFailed = []
                    for param in lVerifyParam:
                        if dStreamMeasureValues[str(param).lower()]:
                            fExpectedValue = float(dLlkKwargs[str(param)])
                            fObtainedValue = float(dStreamMeasureValues[str(param).lower()])
                            if lVerifyParamTol[cnt][-1] == '%':
                                fToleranceValue = float(str(lVerifyParamTol[cnt]).strip('%'))/100.0
                                fAllowedTolerance = fObtainedValue*fToleranceValue
                            else:
                                fAllowedTolerance = float(str(lVerifyParamTol[cnt]))

                            if (fExpectedValue-fAllowedTolerance) <= fObtainedValue <= (fExpectedValue+fAllowedTolerance):
                                sResult = 'PASS'
                            else:
                                sResult = 'FAIL'
                                lResultFailed.append(param)

                            llk_log_info("Measure parameter : %-15s" % str(param) + "\t" + "Expected value : %-15s" % str(fExpectedValue)
                                        + "\t" + "Obtained value : %-15s" % str(fObtainedValue) + "\t" + "Criteria : %-10s"
                                        % ("+/-"+str(lVerifyParamTol[cnt])) + "\t" + "Result : %-5s" % sResult)
                            cnt += 1
                        else:
                            llk_log_warn("Parameter %s is not measured for streamblock %s stream %s" % (str(param),str(sSBName),str(sStreamNumber)))


                    for sVerifyParamRange in lVerifyParamRange:
                        oParam = re.search(r'([A-Za-z0-9]*)[<=>!]*([0-9]*)',sVerifyParamRange)
                        sParam = oParam.group(1)
                        fExpectedValue = float(oParam.group(2))
                        fObtainedValue = float(dStreamMeasureValues[str(sParam).lower()])
                        sVerifyParamExp = str(sVerifyParamRange).replace(str(sParam),str(fObtainedValue))
                        if eval(sVerifyParamExp):
                            sResult = 'PASS'
                        else:
                            sResult = 'FAIL'
                            lResultFailed.append(sVerifyParamRange)

                        llk_log_info("Measure parameter : %-20s" % str(sParam) + "\t" + "Expected value : %-20s" % str(fExpectedValue)
                                    + "\t" + "Obtained value : %-20s" % str(fObtainedValue) + "\t" + "Criteria : %-30s"
                                    % str(sVerifyParamRange) + "\t" + "Result : %-5s" % sResult)
                    if lResultFailed:
                        dResultFailed[sSBNameStream] = lResultFailed[:]
                else:
                    llk_log_warn("No measure parameters collected for the streamblock %s" % str(sSBName)+":"+str(sStreamNumber))
            else:
                llk_log_warn("Streamblock %s with stream %s is not measured!!" % (str(sSBName),str(sStreamNumber)))

        if dResultFailed:
            for sStream in dResultFailed.keys():
                sResultFailed = ",".join([sFailParam for sFailParam in dResultFailed[sStream]])
                llk_log_info("Measure for Streamblock %s failed for the parameter(s) : %s" % (sStream,sResultFailed))
            llk_log_end_proc()
            #raise StcContinueFailure("Traffic Verification Failed!!")
            bResult = "FAIL"

        llk_log_info("=====================================================================")
        llk_log_end_proc()

        if bResult.lower() != sExpResult.lower() :
            return "fail"
        else :
            return "pass" 
        

    def hlk_traffic_verify_measure_port(self, **kwargs):
        """
        Verify the parameters that were measured during the tests.

        Args:
            streamblock(Required): Name of the streamblock
                Type: string
                Default: ''

            stream: Stream number of the streamblock
                Type: Integer
                Default: All streams will be verified

            resultType(Required): Type of result measurement to be retrieved.
                Type: string
                Default: ''
                Values: RxStreamSummaryResults,TxStreamResults

            dExpectedMeasure(Optional, if parameters are given as arguments):
                Dictionary with key value pairs of parameters that are to be verified. This is an optional input as the
                parameters can also be given directly as input to the method.

                If both the dictionary and the parameters are given, then the common parameters would be replaced by the
                one specified as an argument to the method rather than in the dictionary.

                Note->
                    Measure parameters can be given with tolerance level as well by appending _tol to the parameter.
                    The tolerance level can be in either percentage or an absolute value.

                    For example, if bitrate is to be measured then the input parameters can be given as
                    bitrate=30, bitrate_tol=5%

                    Also, a default tolerance level can also be specified by giving default_tol in percentage.
                    This is applied to all the parameters for which there is no tolerance level given. If not mentioned,
                    it is taken as 5%

                    For checking the inequality conditions, the criteria must be menioned as a list of conditions with key
                    as parameter name. This can be seen in the example 3 below.

                    Parameters that can be verified depends on the resultType.

                    For example, if the resultType is RxStreamSummaryResults, the following parameters are some that can be
                    queried from the test data.

                    BitCount, BitRate, DroppedFrameCount, FrameCount, L1BitCount, L1BitRate.

                    For a complete list of measurable parameters, please refer to the Spirent Test Center documentation which
                    can be found in the file path /apps/Spirent/Spirent_TestCenter_4.52/Spirent_TestCenter_Automation_Obj_Ref

        Return:
            N/A

        Example:
            1) ${dMeasureValues} =  Create_Dictionary    BitRate=30    BitRate_tol=13%    FrameCount=99    FrameCount_tol=9%

               hlk_traffic_verify_measure_port    side=network    stream=5    type=RxStreamSummaryResults
                                             dExpectedMeasure=${dMeasureValues}

               Note-> The key argument when providing dictionary as input should be 'dExpectedMeasure'

            2) hlk_traffic_verify_measure_port    side=network    stream=5    type=RxStreamSummaryResults    L1BitRate=1000000
                                             L1BitRate_tol=10    DroppedFrameCount=0    DroppedFrameCount_tol=0

            3) ${dMeasureValues} =  Create_Dictionary    BitRate=88888    BitRate_tol=5%    FrameCount=99    FrameCount_Tol=9%
                                                         droppedFrameCount=0    DroppedFrameCount_tol=1    l1Bitrate=1000000    L1bitRate_tol=10

               hlk_traffic_verify_measure_port    side=network    type=RxStreamSummaryResults    l1Bitrate=[<=1000010,>=99990]
                                    droppedFrameCount=[>=0]    FrameCount=[!=0]    dexpectedmeasure=${dMeasureValues}
        """
        try : 
            assert (kwargs.keys()), "No measure parameters given as input for hlk_traffic_verify_measure_port"
            dKwargs = dict(map(lambda (k,v): (str(k).lower(), v), kwargs.iteritems()))
            llk_log_begin_proc()
            assert ('type' in dKwargs.keys()), "Input should consist of the type of measure results to be retrieved"
            assert ('portid' in dKwargs.keys()), "Input should consist of the type of measure results to be retrieved"
    
            sExpResult = 'pass'
            
            for each in ['exp_result','expect_result'] :
                if dKwargs.has_key(each) :
                    sExpResult = dKwargs.pop(each).lower()
    
            bResult = "PASS"
            sSBName = ''
            sType = ''
            sStreamIndex = ''
            lStreamIndex = []
            sTotalStreamCnt = ''
            dResultFailed = {}
            lStreamNumbers = []
            lVerifyParamRange = []
            lVerifyParamTol = []
            lRetrieveParam = set()
    
            sPortId = dKwargs.pop('portid','')
    
            llk_log_info("Verify Measure Port=%s" % sPortId)
    
            if dKwargs.get('type'):
                sType = dKwargs['type']
                del dKwargs['type']
    
            dExpectedMeasure = dKwargs.get('dexpectedmeasure')
            dLlkKwargs = {}
    
            if dExpectedMeasure:
                del dKwargs['dexpectedmeasure']
                dExpectedMeasure = dict(map(lambda (k,v): (str(k).lower(), v), dExpectedMeasure.iteritems()))
    
            if dExpectedMeasure and dKwargs.keys():
                for key in dKwargs.keys():
                    dExpectedMeasure[str(key)] = dKwargs[key]
                dLlkKwargs = dExpectedMeasure.copy()
            elif dExpectedMeasure:
                dLlkKwargs = dExpectedMeasure.copy()
            elif dKwargs.keys():
                dLlkKwargs = dKwargs.copy()
    
            lVerifyParam = []
            sDefaultTol = '5%'
    
            if dLlkKwargs:
                if dLlkKwargs.keys():
                    dLlkKwargs = dict(map(lambda (k,v): (str(k).lower(), v), dLlkKwargs.iteritems()))
                else:
                    llk_log_end_proc()
                    raise Exception("No input parameters given to verify!!")
    
            if 'default_tol' in dLlkKwargs.keys():
                if str(dLlkKwargs['default_tol'])[-1] == '%':
                    sDefaultTol = str(dLlkKwargs['default_tol'])
                else:
                    llk_log_end_proc()
                    raise Exception ("The default tolerance should be given in percentage(%) units")
            else:
                llk_log_debug("Since default_tol is not mentioned in the input arguments, a value of 5% is set!!")
    
            for key in dLlkKwargs.keys():
                if '<' in str(dLlkKwargs[key]) or '=' in str(dLlkKwargs[key]) or '>' in str(dLlkKwargs[key]) or '!' in str(dLlkKwargs[key]):
                    lRange = str(dLlkKwargs[key]).strip('[]{}()').split(';')
                    lRetrieveParam.add(str(key))
                    for sRange in lRange:
                        lVerifyParamRange.append(str(key)+str(sRange).strip(' '))
                elif '_tol' not in str(key):
                    lVerifyParam.append(str(key).strip(' '))
                    lRetrieveParam.add(str(key))
    
            for sVerifyParam in lVerifyParam:
                if str(sVerifyParam)+'_tol' in dLlkKwargs.keys():
                    lVerifyParamTol.append(dLlkKwargs[str(sVerifyParam)+'_tol'])
                else:
                    lVerifyParamTol.append(sDefaultTol)

            if lRetrieveParam :
                ddRetrieveMeasure = self.oLlkStc.llk_stc_measure_port(self.dMeasureObjects[str(sType)], *lRetrieveParam)
            else:
                llk_log_end_proc()
                raise Exception("No input parameters given to verify!!")
    
            """
            if not sStreamIndex:
                if str(sSBName)+":0" in ddRetrieveMeasure.keys():
                    sTotalStreamCnt = int(ddRetrieveMeasure[str(sSBName)+":0"]['flowcount'])
                else:
                    raise Exception("Streamblock not found while retrieving measure")
    
            if sTotalStreamCnt:
                for i in range(1,sTotalStreamCnt+1):
                    lStreamNumbers.append(i)
            elif lStreamIndex:
                lStreamNumbers = lStreamIndex
            else:
                llk_log_end_proc()
                raise Exception("No valid stream count found for the streamblock %s " % str(sSBName))
            """
            for eachResult in ddRetrieveMeasure :
                if sPortId not in ddRetrieveMeasure[eachResult]['portuiname']:
                    continue
                else : 
                    dStreamMeasureValues = ddRetrieveMeasure[eachResult]
                    if dStreamMeasureValues:
                        cnt = 0
                        lResultFailed = []
                        for param in lVerifyParam:
                            if dStreamMeasureValues[str(param).lower()]:
                                fExpectedValue = float(dLlkKwargs[str(param)])
                                fObtainedValue = float(dStreamMeasureValues[str(param).lower()])
                                if lVerifyParamTol[cnt][-1] == '%':
                                    fToleranceValue = float(str(lVerifyParamTol[cnt]).strip('%'))/100.0
                                    fAllowedTolerance = fObtainedValue*fToleranceValue
                                else:
                                    fAllowedTolerance = float(str(lVerifyParamTol[cnt]))
    
                                if (fExpectedValue-fAllowedTolerance) <= fObtainedValue <= (fExpectedValue+fAllowedTolerance):
                                    sResult = 'PASS'
                                else:
                                    sResult = 'FAIL'
                                    lResultFailed.append(param)
    
                                llk_log_info("Measure parameter : %-15s" % str(param) + "\t" + "Expected value : %-15s" % str(fExpectedValue)
                                            + "\t" + "Obtained value : %-15s" % str(fObtainedValue) + "\t" + "Criteria : %-10s"
                                            % ("+/-"+str(lVerifyParamTol[cnt])) + "\t" + "Result : %-5s" % sResult)
                                cnt += 1
                            else:
                                llk_log_warn("Parameter %s is not measured for streamblock %s stream %s" % (str(param),str(sSBName),str(sStreamNumber)))
    
    
                        for sVerifyParamRange in lVerifyParamRange:
                            oParam = re.search(r'([A-Za-z0-9]*)[<=>!]*([0-9]*)',sVerifyParamRange)
                            sParam = oParam.group(1)
                            fExpectedValue = float(oParam.group(2))
                            fObtainedValue = float(dStreamMeasureValues[str(sParam).lower()])
                            sVerifyParamExp = str(sVerifyParamRange).replace(str(sParam),str(fObtainedValue))
                            if eval(sVerifyParamExp):
                                sResult = 'PASS'
                            else:
                                sResult = 'FAIL'
                                lResultFailed.append(sVerifyParamRange)
    
                            llk_log_info("Measure parameter : %-20s" % str(sParam) + "\t" + "Expected value : %-20s" % str(fExpectedValue)
                                        + "\t" + "Obtained value : %-20s" % str(fObtainedValue) + "\t" + "Criteria : %-30s"
                                        % str(sVerifyParamRange) + "\t" + "Result : %-5s" % sResult)
                        if lResultFailed:
                            dResultFailed[sPortId] = lResultFailed[:]
                    else:
                        llk_log_warn("No measure parameters collected for the port %s" % str(sPortId))
    
            if dResultFailed:
                for sStream in dResultFailed.keys():
                    sResultFailed = ",".join([sFailParam for sFailParam in dResultFailed[sPortId]])
                    llk_log_info("Measure for Port %s failed for the parameter(s) : %s" % (sPortId,sResultFailed))
                llk_log_end_proc()
                bResult = "FAIL"
    
            llk_log_end_proc()
    
            if bResult.lower() != sExpResult.lower() :
                return "fail"
            else :
                return "pass" 
        except Exception as inst:
            s = sys.exc_info()
            logger.error("Exception: %s, Lineno: %s" % (inst,s[2].tb_lineno))
            raise AssertionError('hlk_traffic_verify_measure_port-> Exception:%s, Lineno:%s' % (inst,s[2].tb_lineno))
        
    def hlk_traffic_start_capture(self, **kwargs):
        """
        Start capture on specific/all ports.

        Args:
            portID(Required)- port id of the port obtained using hlk_traffic_get_port_id
                Default: ''
                Type: String/List
                Special Value: 'all' to consider all the ports.

        Return:
            N/A

        Example:
            1)hlk_traffic_start_capture    portID=[${port1_id},${port2_id}]

            2)hlk_traffic_start_capture    portID=all

            3)${lStartCapture} =    Create_List    ${port1_id}    ${port2_id}
              hlk_traffic_start_capture    portID=${lStartCapture}
        """

        llk_log_begin_proc()

        assert (kwargs.keys()), "No arguments given as input for hlk_traffic_start_capture"
        dKwargs = dict(map(lambda (k,v): (str(k).lower(), v), kwargs.iteritems()))
        llk_log_end_proc()
        assert ('portid' in dKwargs.keys()), "Input should consist of a port id on which capture is to be started"

        sPortId = str(dKwargs.get('portid'))
        self.lStartCapturePortID = []
        if sPortId:
            if type(dKwargs['portid']) is list:
                self.lStartCapturePortID = dKwargs['portid']
            elif str(dKwargs['portid']).lower() == 'all':
                if self.dPortIDObjects:
                    for sPortIDObject in self.dPortIDObjects.keys():
                        if self.dPortIDObjects[sPortIDObject]:
                            self.lStartCapturePortID.append(sPortIDObject)
                        else:
                            llk_log_warn("No port object created for port id %s" % str(sPortIDObject))
                else:
                    raise Exception("No port ids created. First create the port and then start the capture")
            else:
                self.lStartCapturePortID = str(dKwargs['portid']).strip("[](){}").split(',')
        else:
            raise Exception("The port id %s is invalid!!" % str(sPortId))

        del dKwargs['portid']
        if dKwargs:
            llk_log_warn("The input parameters %s given are being ignored" % ','.join(key) for key in dKwargs.keys())

        if self.lStartCapturePortID:
            for sStartCapturePortID in self.lStartCapturePortID:
                if sStartCapturePortID in self.dPortIDObjects.keys():
                    if self.dPortIDObjects[sStartCapturePortID]:
                        self.oLlkStc.llk_stc_start_capture(self.dPortIDObjects[sStartCapturePortID])
                    else:
                        llk_log_warn("No port object created for port id %s" % str(sStartCapturePortID))
                else:
                    llk_log_info("No port with the port id %s exists" % str(sStartCapturePortID))
        else:
            llk_log_warn("No valid port ids given as input. Please re-check the value given for portID")
        llk_log_end_proc()


    def hlk_traffic_stop_capture_without_parse_pkt(self,**kwargs):
        """
        Stop capture on specific/all ports.

        Args:
            portID(Required)- port id of the port obtained using hlk_traffic_get_port_id
                Default: ''
                Type: String/List
                Special Value: 'all' to consider all the ports.

        Return:
            N/A

        Example:
            1)hlk_traffic_stop_capture_without_parse_pkt    portID=[${port1_id},${port2_id}]

            2)hlk_traffic_stop_capture_without_parse_pkt    portID=all

            3)${lStopCapture} =    Create_List    ${port1_id}    ${port2_id}
              hlk_traffic_stop_capture_without_parse_pkt    portID=${lStopCapture}
        """
        llk_log_begin_proc()

        assert (kwargs.keys()), "No arguments given as input for hlk_traffic_stop_capture_without_parse_pkt"
        dKwargs = dict(map(lambda (k,v): (str(k).lower(), v), kwargs.iteritems()))
        assert ('portid' in dKwargs.keys()), "Input should consist of a port id on which capture is to be stopped"

        sPortId = str(dKwargs.get('portid'))
        lStopCapturePortID = []

        dKwargs.setdefault("parse_count",'1')

        if sPortId:
            if type(dKwargs['portid']) is list:
                lStopCapturePortID = dKwargs['portid']
            elif str(dKwargs['portid']).lower() == 'all':
                if self.lStartCapturePortID:
                    lStopCapturePortID = self.lStartCapturePortID
                else:
                    raise Exception ("Capture is not started on any port. Please start the capture before stopping it!!")
            else:
                lStopCapturePortID = str(dKwargs['portid']).strip("[](){}").split(',')
        else:
            raise Exception("The port id %s is invalid!!" % str(sPortId))

        del dKwargs['portid']
        #if dKwargs:
        #    llk_log_warn("The keys entered other than portid are ignored")

        if lStopCapturePortID:
            for sStopCapturePortID in lStopCapturePortID:
                if sStopCapturePortID in self.dPortIDObjects.keys():
                    if self.dPortIDObjects[sStopCapturePortID]:
                        self.oLlkStc.llk_stc_stop_capture_without_parse_pkt(self.dPortIDObjects[sStopCapturePortID])
                    else:
                        llk_log_warn("No port object created for port id %s" % str(sStopCapturePortID))
                else:
                    llk_log_info("No port with the port id %s exists" % str(sStopCapturePortID))
        else:
            llk_log_warn("No valid port ids given as input. Please re-check the value given for portID")
        llk_log_end_proc()


    def hlk_traffic_stop_capture(self,**kwargs):
        """
        Stop capture on specific/all ports.

        Args:
            portID(Required)- port id of the port obtained using hlk_traffic_get_port_id
                Default: ''
                Type: String/List
                Special Value: 'all' to consider all the ports.

        Return:
            N/A

        Example:
            1)hlk_traffic_stop_capture    portID=[${port1_id},${port2_id}]

            2)hlk_traffic_stop_capture    portID=all

            3)${lStopCapture} =    Create_List    ${port1_id}    ${port2_id}
              hlk_traffic_stop_capture    portID=${lStopCapture}
        """
        llk_log_begin_proc()

        assert (kwargs.keys()), "No arguments given as input for hlk_traffic_stop_capture"
        dKwargs = dict(map(lambda (k,v): (str(k).lower(), v), kwargs.iteritems()))
        llk_log_end_proc()
        assert ('portid' in dKwargs.keys()), "Input should consist of a port id on which capture is to be stopped"

        sPortId = str(dKwargs.get('portid'))
        lStopCapturePortID = []

        if sPortId:
            if type(dKwargs['portid']) is list:
                lStopCapturePortID = dKwargs['portid']
            elif str(dKwargs['portid']).lower() == 'all':
                if self.lStartCapturePortID:
                    lStopCapturePortID = self.lStartCapturePortID
                else:
                    raise Exception ("Capture is not started on any port. Please start the capture before stopping it!!")
            else:
                lStopCapturePortID = str(dKwargs['portid']).strip("[](){}").split(',')
        else:
            raise Exception("The port id %s is invalid!!" % str(sPortId))

        del dKwargs['portid']
        #if dKwargs:
        #    llk_log_warn("The keys entered other than portid are ignored")

        if lStopCapturePortID:
            for sStopCapturePortID in lStopCapturePortID:
                if sStopCapturePortID in self.dPortIDObjects.keys():
                    if self.dPortIDObjects[sStopCapturePortID]:
                        self.oLlkStc.llk_stc_stop_capture(self.dPortIDObjects[sStopCapturePortID])
                    else:
                        llk_log_warn("No port object created for port id %s" % str(sStopCapturePortID))
                else:
                    llk_log_info("No port with the port id %s exists" % str(sStopCapturePortID))
        else:
            llk_log_warn("No valid port ids given as input. Please re-check the value given for portID")
        llk_log_end_proc()

    def hlk_traffic_retrieve_capture(self, **kwargs):
        """
        Retrieve specific/all packets captured on a port based on a filter criteria.

        This method is used to retrieve the packets captured on a port using the port id and filter criteria.

        In the input parameters, the filter criteria should be mentioned with a filter key and value being a list of
        conditions.
        If no filter criteria is provided, then all the packets captured on the port will be returned.

        Args:
            portID(Required)- port id of the port obtained using hlk_traffic_get_port_id
                Default: ''
                Type: String

            filter: Filter criteria given as list of key value pairs to be applied for all the packets
                Default: ''
                Type: List
                Note->
                    1) All the criteria mentioned should be in the same format as it appears in the packet.
                    2) To specify vlan tags as filter criteria use vlan along with the order number.
                       For example, if there are three vlan tags configured, then they can be addressed as vlan1,vlan2 and vlan3 respectively
                       and their attributes as vlan1.id,vlan2.pri etc

        Return:
            Specific/All packets captured on a port.

            Packet structure is a List of dictionaries with each dictionary containing the key value paris as fields parsed
            from the packet

            The 'key' holds the attribute value 'name' of the field. If the field is nested, then the attribute value of 'name'
            will be the string concatenation of parent field's attribute value 'name' and child fields attribute 'name' with the
            separator as '__'(double underscore). This is done since some keys might have the same attribute value of 'name'.
            The 'value' holds the attribute values 'show' provided they exist for the field.

            Generally, the key value pair would be 'name': 'show'

            Example for the fields:
            {'arp.opcode': '2', 'eth.fcs': '601882514' , 'eth.fcs__eth.fcs_good': '1'}

        Example:
            1) ${Retreive_Capture} = hlk_traffic_retrieve_capture    portID=${port1_id}
               This would return all the packets that were captured on the port with portID ${port1_id}

            2) ${Retreive_Capture} = hlk_traffic_retrieve_capture    portID=${port1_id}    filter=[eth.dst__eth.addr=00:00:01:00:00:01,vlan.id=4020]
                This would return all the packets that were captured on the port with portID ${port1_id}
                and satisfy the filter criteria
        """
        llk_log_begin_proc()

        assert (kwargs.keys()), "No arguments given as input"
        dKwargs = dict(map(lambda (k,v): (str(k).lower(), v), kwargs.iteritems()))
        llk_log_end_proc()
        assert ('portid' in dKwargs.keys()), "Input should consist of a port id on which capture is to be retrieved"

        sRetrieveCapturePortID = str(dKwargs.get('portid')).strip("[](){}")
        dParsedFilter = {}
        if 'filter' in dKwargs.keys():
            lFilterCriteria = str(dKwargs['filter']).strip("[](){}").split(',')
            if lFilterCriteria:

                for sFilterCriteria in lFilterCriteria:
                    lParsedFilter = str(sFilterCriteria).strip("[](){}").split('=')
                    if lParsedFilter:
                        if len(lParsedFilter) == 2:
                            dParsedFilter[str(lParsedFilter[0])] = str(lParsedFilter[1])
        ldAllCapturePackets = []


        if sRetrieveCapturePortID:
            if sRetrieveCapturePortID in self.dPortIDObjects.keys():
                if self.dPortIDObjects[sRetrieveCapturePortID]:
                    llk_log_info("capture on the port : " + str(sRetrieveCapturePortID))
                    oPortObject = self.dPortIDObjects[sRetrieveCapturePortID]
                    oCaptureObject = self.oLlkStc.dPortCaptureId[oPortObject]
                    ldAllCapturePackets = self.oLlkStc.dCapturePkt[oCaptureObject]
                else:
                    llk_log_info("No packets were captured on the port!!")
            else:
                llk_log_end_proc()
                raise Exception("Port capture was not done on the port " + str(sRetrieveCapturePortID))
        else:
            llk_log_end_proc()
            raise Exception("No port ID given as input!!")

        ldRetrieveCapturePackets = []
        if ldAllCapturePackets:
            idAllCapturePackets = len(ldAllCapturePackets)
            if dParsedFilter:
                iCnt = 1
                iPktCnt = 0
                llk_log_info("Filtering packets with criteria: ")
                while(iPktCnt < idAllCapturePackets):                    
                    if int(ldAllCapturePackets[iPktCnt]['num']) == iCnt:
                        bFilterFlag = False
                        for sFilterKey in dParsedFilter.keys():
                            if not (sFilterKey in ldAllCapturePackets[iPktCnt].keys() and ldAllCapturePackets[iPktCnt][sFilterKey] == dParsedFilter[sFilterKey]):
                                bFilterFlag = True
                                break
                        if bFilterFlag:
                            llk_log_info("Packet " + str(iCnt) + " did not pass the filter criteria")
                        else:
                            llk_log_info("Packet " + str(iCnt) + " passed the filter criteria")
                            llk_log_debug(ldAllCapturePackets[iPktCnt])
                            ldRetrieveCapturePackets.append(ldAllCapturePackets[iPktCnt])
                        iPktCnt += 1
                    else:
                        llk_log_debug("Packet %s is missing!!" % str(iCnt))
                    iCnt += 1
                llk_log_end_proc()
                return ldRetrieveCapturePackets[:]
            else:
                llk_log_debug("There are no filter parameters mentioned. Hence all the packets that are captured on the port will be retrieved/verified")
                iCnt = 1
                iPktCnt = 0
                while(iPktCnt < idAllCapturePackets):
                    if int(ldAllCapturePackets[iPktCnt]['num']) == iCnt:
                        llk_log_debug("Packet " + ldAllCapturePackets[iPktCnt]['num'] + " is retrieved ")
                        llk_log_trace(ldAllCapturePackets[iPktCnt])
                        ldRetrieveCapturePackets.append(ldAllCapturePackets[iPktCnt])
                        iPktCnt += 1
                    else:
                        llk_log_debug("Packet %s is missing!!" % str(iCnt))
                    iCnt += 1
                llk_log_end_proc()
                return ldRetrieveCapturePackets[:]
        else:
            llk_log_end_proc()
            raise Exception("There are no packets captured on the port with port id " + str(sRetrieveCapturePortID))

    def hlk_traffic_verify_capture_without_parse_pkt(self, **kwargs):
        """
        Verify the fields of the packet captured on the port.

        Args:
            portID(Required): Port ID obtained from hlk_traffic_get_port_id
            Default: ''
            Type: String

            verifyParam(Required): Parameter/field's value to be verified in a packet
            Default: None
            Type: List of key value pairs
            Special value: 'pktcount' = X (X: Number of packets). This verifies the number of packets captured

            filter : Parameter/field's value based on which the captured packets are to be filtered
            Default: None
            Type: List of key value pairs

            Note -> To use vlan tags as filter or verify criteria use vlan along with the order number.For example, if there
            are three vlan tags configured, then they can be addressed as vlan1,vlan2 and vlan3 respectively and their
            attributes as vlan1.id,vlan2.pri etc

        Return:
            N/A

        Example:
            1) hlk_traffic.hlk_traffic_verify_capture_without_parse_pkt    portID=${port1_id}    verifyParam=[pktCount=10,vlan1.id=4020]

               This would verify all the packets that were captured on the port port1_id against the conditions
               mentioned in verifyParam and displays the ones that satisfy(if any).

            2) hlk_traffic.hlk_traffic_verify_capture_without_parse_pkt    portID=${port1_id}    filter=[eth.dst__eth.addr=00:00:01:00:00:01]
                                                         verifyParam=[pktCount=10,vlan1.id=4020]

               This would first filter all the packets that were captured on the port port1_id based on the filter criteria
               and then verify each one of them against the verify criteria mentioned in verifyParam.

               It displays the packets that satisfy the criteria(if any).
        """
        llk_log_begin_proc()
        assert (kwargs.keys()), "No arguments given as input for hlk_traffic_verify_capture_without_parse_pkt"
        dKwargs = dict(map(lambda (k,v): (str(k).lower(), v), kwargs.iteritems()))
        assert ('verifyparam' in dKwargs.keys()), "Parameters for verifying the packets not found!! " \
                                                 "If there are no parameters to verify please use retrieve capture instead!! "


        dParsedVerify = {}
        compdict = dict(map(lambda (k,v): (str(k).lower(), str(v)), dKwargs['verifyparam'].iteritems()))

        try :      
            real_result = ''
            expect_result = compdict.pop('expect_result','pass')

            check_packet_count = False
            parse_count = compdict.pop('parse_count',1)
            expect_packets =''

            if compdict.has_key('packet_count'):
                check_packet_count = True
                #expect_packets = int(compdict['packet_count'])
                expect_packets = compdict['packet_count']
                del compdict['packet_count']
            else :
                check_packet_count = False

            sPortId = str(dKwargs.pop('portid'))
            
            sPdmlName = ''
            sPcapName = ''
            try : 
                sPcapName = self.oLlkStc.dCapturePcap[self.dPortIDObjects[sPortId]] 
                sPdmlName = sPcapName.replace('pcap','pdml')
            except Exception as inst:
                sPdmlName = ''
                sPcapName = ''

            if expect_packets == '0' and sPdmlName == '' : 
                logger.info("No any packets is captured!")
                return "pass" 

            # get matched_packet_num by 'tshark -z'
            iMatchedPacketCount = ''
            sValidCond = ''
            for key in compdict.keys() :
                if re.search('vlan.*',key) :
                    # replace vlanxx.id to vlan.id, vlanxx.priorty to vlan.priorty 
                    new_key = re.sub('vlan\d+.','vlan.',key)
                    sValidCond = sValidCond + new_key + '==' + compdict[key] + '&&'
                elif key == 'ta_protocol' :
                    sValidCond = sValidCond + compdict[key].lower() + '&&'    
                    compdict.pop('ta_protocol')
                elif '__' in key or '!!' in key:
                    continue
                else : 
                    sValidCond = sValidCond + key + '==' + compdict[key] + '&&'
            sValidCond = sValidCond[:-2]

            #TRAF_OUTPUT_DIR = BuiltIn().replace_variables('${OUTPUTDIR}') + "/TRAFFIC"
            TRAF_OUTPUT_DIR = self.oLlkStc.TRAF_OUTPUT_DIR 
            sTsharkExe = os.popen('which tshark').readlines()
            if sTsharkExe :
                m = re.search('(.*)',sTsharkExe[0])
                if m :
                    sTsharkExe = m.group(1)
            else :
                raise StcContinueFailure("tshark is not installed!") 

            sValidCmd = sTsharkExe + ' -z "io,phs,%s"'%sValidCond + ' -r ' + os.path.join(TRAF_OUTPUT_DIR,sPcapName) + ' | grep eth | grep frames'
            logger.info(sValidCmd)
            lFrameResult = os.popen(sValidCmd).readlines()
            logger.info("%s" % lFrameResult)
            if not lFrameResult :
                iMatchedPacketCount = 0
            else : 
                for eachResult in lFrameResult :
                    if 'frames' in str(eachResult) :
                        m = re.search('frames:(\d+)',eachResult)
                        if m :
                            iMatchedPacketCount = int(m.group(1))
                            break
            
            # need check packet content 
            if int(iMatchedPacketCount) > 0 : 
                # verify the matched packet content by file pdml,for qinq vlan or dhcp options
                """
                sValidCond = ''
                for key in compdict.keys() :
                    sValidCond = sValidCond + key + '==' + compdict[key] + '&&'
                    sValidCond = sValidCond[:-2]
                """
                sCountCmd = ''
                if int(parse_count) >= 1 :
                    sCountCmd = ' -c ' + str(parse_count) 

                sConvertCmd = sTsharkExe + sCountCmd + ' -R "%s" '%sValidCond + ' -r ' + os.path.join(TRAF_OUTPUT_DIR,sPcapName) + ' -T pdml > ' + os.path.join(TRAF_OUTPUT_DIR,sPdmlName) 
                logger.info(sConvertCmd)
                os.system(sConvertCmd)
                self.oLlkStc.llk_stc_parse_pdml(self.dPortIDObjects[sPortId],sPdmlName)

                #/usr/sbin/tshark  -c 1 -R "ip.src==192.85.1.2&&eth.src==00:00:00:00:65:00" -r /tmp/RIDEYy2iIp.d/TRAFFIC/capture1_08-23-2017_18-37-11.pcap -T pdml > /tmp/tmp.pdml

                result = self.hlk_traffic_verify_capture(portID=sPortId,verifyParam=compdict)    
                if result == 'fail' : 
                    logger.debug("packet can't match compdict: %s" % compdict)
                    iMatchedPacketCount = 0

            logger.info ("Matched packet(s) : "+str(iMatchedPacketCount))
            
            if not check_packet_count :
                logger.info ("Expected packet(s): >= 1")
                if iMatchedPacketCount >= 1 :
                    real_result = "pass"
                else :
                    real_result = "fail" 
            else :
                logger.info ("Expected packet(s): "+str(expect_packets))
                cmd_validate_pkt_cnt = ''
                sMin = ''
                sMax = ''
                try:    
                    if '(' in str(expect_packets) and ')' in str(expect_packets) :
                        sMin,sMax = expect_packets.strip('\(\)').split(',')
                        if sMin and sMax :
                            cmd_validate_pkt_cnt = "float(sMin) <= float(iMatchedPacketCount) <= float(sMax)"
                        elif sMin :
                            cmd_validate_pkt_cnt = "float(sMin) <= float(iMatchedPacketCount)"
                        elif sMax :
                            cmd_validate_pkt_cnt = "float(iMatchedPacketCount) <= float(sMax)"
                        else :
                            raise AssertionError("Invalid param format: %s" % expect_packets)
                    elif '<' in str(expect_packets) or '>' in str(expect_packets) or '!' in str(expect_packets) :
                        cmd_validate_pkt_cnt = str(iMatchedPacketCount) + str(expect_packets)
                    else :                       
                        cmd_validate_pkt_cnt = str(iMatchedPacketCount) + '==' + str(expect_packets)
                except Exception as inst: 
                    llk_log_end_proc() 
                    s = sys.exc_info()
                    logger.warn("lineno:%s,exception:%s" % (s[2].tb_lineno,inst))
                    raise AssertionError("Invalid param format, expect_packet: %s" % expect_packets)

                if eval(cmd_validate_pkt_cnt) :
                    real_result = "pass"
                else :
                    msg = "Matched packet(s) '%s' is not equal Expected packet(s) '%s'" \
                    % (str(iMatchedPacketCount),str(expect_packets))
                    logger.warn("<span style=\"color: #FF8C00\">"+msg+"</span>",html=True)
                    real_result = "fail"
        except Exception as inst:
            llk_log_end_proc()
            s = sys.exc_info()
            logger.warn("lineno:%s,exception:%s" % (s[2].tb_lineno,inst))
            raise AssertionError('hlk_traffic_verify_capture_without_parse_pkt->lineno:%s, exception:%s' % (s[2].tb_lineno,inst))

        llk_log_end_proc()

        if expect_result.lower() != real_result.lower() :
            logger.info("Verify Capture Failed, expect:%s,real:%s" % (expect_result.lower(),real_result.lower()))
            return 'fail'
        else :
            logger.info("Verify Capture Passed, expect:%s,real:%s" % (expect_result.lower(),real_result.lower()))
            return "pass"
    
    def hlk_traffic_verify_capture(self, **kwargs):
        """
        Verify the fields of the packet captured on the port.

        Args:
            portID(Required): Port ID obtained from hlk_traffic_get_port_id
            Default: ''
            Type: String

            verifyParam(Required): Parameter/field's value to be verified in a packet
            Default: None
            Type: List of key value pairs
            Special value: 'pktcount' = X (X: Number of packets). This verifies the number of packets captured

            filter : Parameter/field's value based on which the captured packets are to be filtered
            Default: None
            Type: List of key value pairs

            Note -> To use vlan tags as filter or verify criteria use vlan along with the order number.For example, if there
            are three vlan tags configured, then they can be addressed as vlan1,vlan2 and vlan3 respectively and their
            attributes as vlan1.id,vlan2.pri etc

        Return:
            N/A

        Example:
            1) hlk_traffic.hlk_traffic_verify_capture    portID=${port1_id}    verifyParam=[pktCount=10,vlan1.id=4020]

               This would verify all the packets that were captured on the port port1_id against the conditions
               mentioned in verifyParam and displays the ones that satisfy(if any).

            2) hlk_traffic.hlk_traffic_verify_capture    portID=${port1_id}    filter=[eth.dst__eth.addr=00:00:01:00:00:01]
                                                         verifyParam=[pktCount=10,vlan1.id=4020]

               This would first filter all the packets that were captured on the port port1_id based on the filter criteria
               and then verify each one of them against the verify criteria mentioned in verifyParam.

               It displays the packets that satisfy the criteria(if any).
        """
        llk_log_begin_proc()
        assert (kwargs.keys()), "No arguments given as input for hlk_traffic_verify_capture"
        dKwargs = dict(map(lambda (k,v): (str(k).lower(), v), kwargs.iteritems()))
        assert ('verifyparam' in dKwargs.keys()), "Parameters for verifying the packets not found!! " \
                                                 "If there are no parameters to verify please use retrieve capture instead!! "

        dParsedVerify = {}
        packets = self.hlk_traffic_retrieve_capture(**kwargs)
        compdict = dict(map(lambda (k,v): (str(k).lower(), str(v)), dKwargs['verifyparam'].iteritems()))


        logger.info("Verify Condiction:%s" % compdict)

        try :        
            msg = "Received packet(s) : "+ str(len(packets))
            logger.debug(msg)
            
            if compdict.has_key('packet_count'):
                check_packet_count = True
                #expect_packets = int(compdict['packet_count'])
                expect_packets = compdict['packet_count']
                del compdict['packet_count']
            else :
                check_packet_count = False
            
            matched_packet_num = 0
            current_packet_num = 0
            for packet in packets :
                current_packet_num = current_packet_num + 1
                llk_log_debug("Packet No.%s: %s" % (current_packet_num,packet))            
                # we hope to check hex mode opt82_agent_remote_id
                # but it only exist in dhcp options as a part
                # if pcta support it by hex mode, we can remove this 'if'
                if compdict.has_key('opt82_agent_remote_id_hex') :
                    if packet.has_key('dhcp_options') :
                        if packet['dhcp_options'].find(compdict['opt82_agent_remote_id_hex']) >= 0:
                            packet['opt82_agent_remote_id_hex'] = \
                            compdict['opt82_agent_remote_id_hex']
                        else:
                            llk_log_trace("no opt82_agent_remote_id_hex in captured packet: %s" 
                            % (str(current_packet_num)))
                    else:
                        llk_log_trace("no dhcp_options in captured packet: %s" % str(current_packet_num))
                if compdict.has_key('cfm_flags') :
                    packet['cfm_flags'] = str(int(packet['cfm_flags'],16))
                    compdict['cfm_flags'] = str(int(compdict['cfm_flags']))
            
                packet_matched = True
                for each in sorted(packet) :
                    logger.trace("each:%s" % each)

                for key in compdict.keys() :

                    # if key has sub_key
                    if '[' in str(key):
                        sKey = key.replace('[','').replace(']','')          
                        lKeys = sKey.split(',')
                        sValue = compdict[key].replace('[','').replace(']','')
                        lValues = sValue.split(',')
                        iKeyNum = len(lKeys)
                        iValueNum = len(lValues)
                        assert (iKeyNum == iValueNum), 'invalid account of %s,%s'% (key,sValue)
                        is_found = None
                        is_key_exist = True
                        array_index_name = ''
                        for array_index in range(0,100) :
                            is_found = True 
                            for key_index in range(0,iKeyNum):
                                each_key = lKeys[key_index]
                                each_value = lValues[key_index]
                                search_key = array_index_name + each_key
                                if packet.has_key(search_key) :
                                    # the key exist
                                    #if packet[search_key] != each_value :
                                    # only in is ok
                                    if each_value not in packet[search_key] : 
                                        msg = "%s: expect: %s, get: %s" % (search_key,each_value,packet[search_key])
                                        logger.debug("<span style=\"color: #0000FF\">"+msg+"</span>",html=True)
                                        is_found = False
                                        # break
                                else:
                                    msg = "No value for option '"+search_key+"' in packet No."+ str(current_packet_num)
                                    logger.debug("<span style=\"color: #FF8C00\">"+msg+"</span>",html=True)

                                    is_key_exist = False
                                    break

                            if not is_key_exist :
                                packet_matched = False
                                break

                            if not is_found :
                                # try for next array
                                array_index_name = '[' + str(array_index+1) + ']'
                                continue            
                            else : 
                                break 
                    
                    else :  
                        if packet.has_key(key) :
                            msg = "%s: expect: %s, get: %s" % (key,compdict[key],packet[key])
                            logger.debug("<span style=\"color: #0000FF\">"+msg+"</span>",html=True)
                            if (type(packet[key]) is list and compdict[key] not in packet[key]) \
                            or (type(packet[key]) is not list and compdict[key] not in packet[key] ) :
                                packet_matched = False
                                msg = "'%s' is not expected '%s' for option '%s' in packet No.%s" \
                                % (str(packet[key]),compdict[key],key,str(current_packet_num))
                                logger.debug ("<span style=\"color: #FF8C00\">"+msg+"</span>",html=True)                 
                        else :
                            packet_matched = False
                            msg = "No value for option '"+key+"' in packet No."+ str(current_packet_num)
                            logger.info("<span style=\"color: #FF8C00\">"+msg+"</span>",html=True)               
                            break
            
                if packet_matched :
                    matched_packet_num = matched_packet_num + 1
                    logger.debug ("Packet No." + str(current_packet_num) + " Matched")
                else :
                    logger.debug ("Packet No." + str(current_packet_num) + " Not Matched")
                
            logger.debug ("Matched packet(s) : "+str(matched_packet_num))
            
            if not check_packet_count :
                logger.debug ("Expected packet(s): >= 1")
                if matched_packet_num >= 1 :
                    return "pass"
                else :
                    return "fail" 
            else :
                logger.debug ("Expected packet(s): "+str(expect_packets))
                cmd_validate_pkt_cnt = ''
                try:                           
                    cmd_validate_pkt_cnt = matched_packet_num + '==' + int(expect_packets)
                except Exception as inst: 
                    # expect_packets has char '<','>','!','='
                    cmd_validate_pkt_cnt = str(matched_packet_num) + str(expect_packets)

                if eval(cmd_validate_pkt_cnt) :
                    return "pass"
                else :
                    msg = "Matched packet(s) '%s' is not equal Expected packet(s) '%s'" \
                    % (str(matched_packet_num),str(expect_packets))
                    logger.warn("<span style=\"color: #FF8C00\">"+msg+"</span>",html=True)
                    return "fail"
                
        except Exception as inst:
            s = sys.exc_info()
            raise AssertionError('hlk_traffic_verify_capture->lineno:%s, exception:%s' % (s[2].tb_lineno,inst))

        llk_log_end_proc()

    def hlk_traffic_config_streamblock_range_modifier(self, **kwargs):
        """
        Create/configure a range modifier for a streamblock.

        Args:
            streamblock(Required): Name of the streamblock
            Default: ''
            Type:String

            type(Required): Type of streamblock's interface to be modified
            Default: ''
            Type:String
            Values: ethii,ipv4,ipv6,tcp,udp,vlanX(X: Vlan tag number)

            field(Required): Field of the streamblock to be modified
            Default: ''
            Type:String
            Values:
                i) ethii:
                    dstMac -
                        Default: 00:00:01:00:00:01
                        Type: MACADDR
                    srcMac -
                        Default: 00:10:94:00:00:02
                        Type: MACADDR
                    etherType -
                        Values: 0200 (XEROX PUP), 0201 (PUP Addr Trans), 0400 (Nixdorf), 0600 (XEROX NS IDP), 0660 (DLOG),
                        0661 (DLOG2), 0800 (Internet IP), 0801 (X.75 Internet), 0802 (NBS Internet), 0803 (ECMA Internet),
                        0804 (Chaosnet), 0805 (X.25 Level 3), 0806 (ARP), 0807 (XNS Compatibility), 0808 (Frame Relay ARP),
                        8035 (RARP), 86DD (IPv6), 880B (PPP), 8809 (Slow Protocol), 8847 (MPLS Unicast), 8848 (MPLS Multicast),
                        8863 (PPPoE Discovery), 8864 (PPPoE Session), 88E7 (PBB), 8906 (FCoE), 8914 (FIP)
                        Default: 88B5 (Local Experimental Ethertype)
                    preamble -
                        Default: 55555555555555d5
                        Type: OCTETSTRING

                ii) ipv4:
                    checksum -
                        Default: 0
                        Type: INTEGER
                    destAddr -
                        Default: 192.0.0.1
                        Type: IPV4ADDR
                    destPrefixLength -
                        Default: 24
                        Type: INTEGER
                    fragOffset -
                        Default: 0
                        Type: INTEGER
                    gateway -
                        Default: 192.85.1.1
                        Type: IPV4ADDR
                    prefixLength -
                        Default: 24
                        Type: INTEGER
                    protocol -
                        Values: 0 (HOPOPT), 1 (ICMP), 2 (IGMP), 3 (GGP), 4 (IP), 5 (ST), 6 (TCP), 7 (CBT), 8 (EGP), 9 (IGP),
                        10 (BBN-RCC-MON), 11 (NVP-II), 12 (PUP), 13 (ARGUS), 14 (EMCON), 15 (XNET), 16 (CHAOS), 17 (UDP),
                        18 (MUX), 19 (DCN-MEAS), 20 (HMP), 21 (PRM), 22 (XNS-IDP), 23 (TRUNK-1), 24 (TRUNK-2), 25 (LEAF-1),
                        26 (LEAF-2), 27 (RDP), 28 (IRTP), 29 (ISO-TP4), 30 (NETBLT), 31 (MFE-NSP), 32 (MERIT-INP), 33 (SEP),
                        34 (3PC), 35 (IDPR), 36 (XTP), 37 (DDP), 38 (IDPR-CMTP), 39 (TP++), 40 (IL), 41 (IPv6), 42 (SDRP),
                        43 (IPv6-Route), 44 (IPv6-Frag), 45 (IDRP), 46 (RSVP), 47 (GRE), 48 (MHRP), 49 (BNA), 50 (ESP),
                        51 (AH), 52 (I-NLSP), 53 (SWIPE), 54 (NARP), 55 (MOBILE), 56 (TLSP), 57 (SKIP), 58 (IPv6-ICMP),
                        59 (IPv6-NoNxt), 60 (IPv6-Opts), 62 (CFTP), 64 (SAT-EXPAK), 65 (KRYPTOLAN), 66 (RVD), 67 (IPPC),
                        69 (SAT-MON), 70 (VISA), 71 (IPCV), 72 (CPNX), 73 (CPHB), 74 (WSN), 75 (PVP), 76 (BR-SAT-MON),
                        77 (SUN-ND), 78 (WB-MON), 79 (WB-EXPAK), 80 (ISO-IP), 81 (VMTP), 82 (SECURE-VMTP), 83 (VINES),
                        84 (TTP), 85 (NSFNET-IGP), 86 (DGP), 87 (TCF), 88 (EIGRP), 89 (OSPFIGP), 90 (Sprite-RPC), 91 (LARP),
                        92 (MTP), 93 (AX.25), 94 (IPIP), 95 (MICP), 96 (SCC-SP), 97 (ETHERIP), 98 (ENCAP), 100 (GMTP),
                        101 (IFMP), 102 (PNNI), 103 (PIM), 104 (ARIS), 105 (SCPS), 106 (QNX), 107 (A/N), 108 (IPComp),
                        109 (SNP), 110 (Compaq-Peer), 111 (IPX-in-IP), 112 (VRRP), 113 (PGM), 115 (L2TP), 116 (DDX),
                        117 (IATP), 118 (STP), 119 (SRP), 120 (UTI), 121 (SMP), 122 (SM), 123 (PTP), 124 (ISIS over IPv4),
                        125 (FIRE), 126 (CRTP), 127 (CRUDP), 128 (SSCOPMCE), 129 (IPLT), 130 (SPS), 131 (PIPE), 132 (SCTP),
                        133 (FC), 134 (RSVP-E2E-IGNORE), 135 (Mobility Header), 136 (UDPLite), 137 (MPLS-in-IP),
                        253 (Experimental), 255 (Reserved)
                        Default: 253
                    sourceAddr -
                        Default: 192.85.1.2
                        Type: IPV4ADDR
                    totalLength -
                        Default: 20
                        Type: INTEGER
                    ttl -
                        Default: 255
                        Type: INTEGER
                    identification -
                        Type: INTEGER
                        Default: 0
                    ihl -
                        Type: INTEGER
                        Default: 5
                    version
                        Type: INTEGER
                        Default: 4

                iii) ipv6:
                    destAddr
                        Type: IPV6ADDR
                        Default: 2000::1
                    destPrefixLength
                        Type: INTEGER
                        Default: 64
                    flowLabel
                        Type: INTEGER
                        Default: 0
                    gateway
                        Type: IPV6ADDR
                        Default: ::0
                    hopLimit
                        Type: INTEGER
                        Default: 255
                        A text name for the object. This attribute is required when you use stream block modifiers such as RangeModifier,
                        RandomModifier, and TableModifier. See the description of the OffsetReference attribute for these modifier objects for more
                        information.
                    Name
                        Type: string
                    nextHeader
                        Type: IpProtocolNumbers
                        Default: 59
                    Possible Values:
                        0 HOPOPT
                        1 ICMP
                        2 IGMP
                        3 GGP
                        4 IP
                        5 ST
                        6 TCP
                        7 CBT
                        8 EGP
                        9 IGP
                        10 BBN-RCC-MON
                        11 NVP-II
                        12 PUP
                        13 ARGUS
                        14 EMCON
                        15 XNET
                        16 CHAOS
                        17 UDP
                        18 MUX
                        19 DCN-MEAS
                        20 HMP
                        21 PRM
                        22 XNS-IDP
                        23 TRUNK-1
                        24 TRUNK-2
                        25 LEAF-1
                        26 LEAF-2
                        27 RDP
                        28 IRTP
                        29 ISO-TP4
                        30 NETBLT
                        31 MFE-NSP
                        32 MERIT-INP
                        33 SEP
                        34 3PC
                        35 IDPR
                        36 XTP
                        37 DDP
                        38 IDPR-CMTP
                        39 TP++
                        40 IL
                        41 IPv6
                        42 SDRP
                        43 IPv6-Route
                        44 IPv6-Frag
                        45 IDRP
                        46 RSVP
                        47 GRE
                        48 MHRP
                        49 BNA
                        50 ESP
                        51 AH
                        52 I-NLSP
                        53 SWIPE
                        54 NARP
                        55 MOBILE
                        56 TLSP
                        57 SKIP
                        58 IPv6-ICMP
                        59 IPv6-NoNxt
                        60 IPv6-Opts
                        62 CFTP
                        64 SAT-EXPAK
                        65 KRYPTOLAN
                        66 RVD
                        67 IPPC
                        69 SAT-MON
                        70 VISA
                        71 IPCV
                        72 CPNX
                        73 CPHB
                        74 WSN
                        75 PVP
                        76 BR-SAT-MON
                        77 SUN-ND
                        78 WB-MON
                        79 WB-EXPAK
                        80 ISO-IP
                        81 VMTP
                        82 SECURE-VMTP
                        83 VINES
                        84 TTP
                        85 NSFNET-IGP
                        86 DGP
                        87 TCF
                        88 EIGRP
                        89 OSPFIGP
                        90 Sprite-RPC
                        91 LARP
                        92 MTP
                        93 AX.25
                        94 IPIP
                        95 MICP
                        96 SCC-SP
                        97 ETHERIP
                        98 ENCAP
                        100 GMTP
                        101 IFMP
                        102 PNNI
                        103 PIM
                        104 ARIS
                        105 SCPS
                        106 QNX
                        107 A/N
                        108 IPComp
                        109 SNP
                        110 Compaq-Peer
                        111 IPX-in-IP
                        112 VRRP
                        113 PGM
                        115 L2TP
                        116 DDX
                        117 IATP
                        118 STP
                        119 SRP
                        120 UTI
                        121 SMP
                        122 SM
                        123 PTP
                        124 ISIS over IPv4
                        125 FIRE
                        126 CRTP
                        127 CRUDP
                        128 SSCOPMCE
                        129 IPLT
                        130 SPS
                        131 PIPE
                        132 SCTP
                        133 FC
                        134 RSVP-E2E-IGNORE
                        135 Mobility Header
                        136 UDPLite
                        137 MPLS-in-IP
                        253 Experimental
                        255 Reserved
                    payloadLength
                        Type: INTEGER
                        Default: 0
                    prefixLength
                        Type: INTEGER
                        Default: 64
                    sourceAddr
                        Type: IPV6ADDR
                        Default: 2000::2
                    trafficClass
                        Type: INTEGER
                        Default: 0
                    version
                        Type: INTEGER
                        Default: 6

                iv) tcp:
                    ackBit
                        Type: BITSTRING
                        Default: 1
                    ackNum
                        Type: INTEGER
                        Default: 234567
                    checksum
                        Type: INTEGER
                        Default: Automatically calculated for each packet. (If you set this to 0, the checksum will not be calculated and will be
                        the same for each packet.)
                    cwrBit
                        Type: BITSTRING
                        Default: 0
                    destPort
                        Type: WellKnownPorts
                        Default: 1024
                        Possible Values:
                        1 TCPMUX
                        2 CompressNet
                        3 CompressProcess
                        5 RJE
                        7 ECHO
                        9 DISCARD
                        11 SYSTAT
                        13 DAYTIME
                        17 QOTD
                        18 MSP
                        19 CHARGEN
                        20 FTPDATA
                        21 FTP
                        22 SSH
                        23 Telnet
                        25 SMTP
                        27 NSW
                        29 MSG-ICP
                        31 MSG-AUTH
                        33 DSP
                        37 TIME
                        38 RAP
                        39 RLP
                        41 Graphics
                        42 Nameserver
                        43 WHOIS
                        44 MPM-flags
                        45 MPM
                        46 MPM-send
                        47 NI FTP
                        48 AUDIT
                        49 TACACS
                        50 RE-MAIL
                        51 LA-MAINT
                        52 XNS-time
                        53 DNS
                        54 XNS
                        55 ISI-GL
                        56 XNS-auth
                        69 TFTP
                        70 GOPHER
                        79 FINGER
                        80 HTTP
                        88 KERBEROS
                        119 NNTP
                        123 NTP
                        161 SNMP
                        162 SNMPTRAP
                        179 BGP
                        194 IRC
                        520 RIP
                        521 RIPNG
                        3784 BFD
                    ecnBit
                        Type: BITSTRING
                        Default: 0
                    finBit
                        Type: BITSTRING
                        Default: 0
                        A text name for the object. This attribute is required when you use stream block modifiers such as RangeModifier,
                        RandomModifier, and TableModifier. See the description of the OffsetReference attribute for these modifier objects for
                        more information.
                    Name
                        Type: string
                    offset
                        Type: INTEGER
                        Default: 5
                    pshBit
                        Type: BITSTRING
                        Default: 0
                    reserved
                        Type: BITSTRING
                        Default: 0000
                    rstBit
                        Type: BITSTRING
                        Default: 0
                    seqNum
                        Type: INTEGER
                        Default: 123456
                    sourcePort
                        Type: INTEGER
                        Default: 1024
                    synBit
                        Type: BITSTRING
                        Default: 0
                    urgBit
                        Type: BITSTRING
                        Default: 0
                    urgentPtr
                        Type: INTEGER
                        Default: 0
                    window
                        Type: INTEGER
                        Default: 4096

                v) udp:
                    checksum
                        Type: INTEGER
                        Default: Automatically calculated for each packet. (If you set this to 0, the checksum will not be calculated and will be the same
                        for each packet.)
                    destPort
                        Type: WellKnownPorts
                        Default: 1024
                        Possible Values:
                        1 TCPMUX
                        2 CompressNet
                        3 CompressProcess
                        5 RJE
                        7 ECHO
                        9 DISCARD
                        11 SYSTAT
                        13 DAYTIME
                        17 QOTD
                        18 MSP
                        19 CHARGEN
                        20 FTPDATA
                        21 FTP
                        22 SSH
                        23 Telnet
                        25 SMTP
                        27 NSW
                        29 MSG-ICP
                        31 MSG-AUTH
                        33 DSP
                        37 TIME
                        38 RAP
                        39 RLP
                        41 Graphics
                        42 Nameserver
                        43 WHOIS
                        44 MPM-flags
                        45 MPM
                        46 MPM-send
                        47 NI FTP
                        48 AUDIT
                        49 TACACS
                        50 RE-MAIL
                        51 LA-MAINT
                        52 XNS-time
                        53 DNS
                        54 XNS
                        55 ISI-GL
                        56 XNS-auth
                        69 TFTP
                        70 GOPHER
                        79 FINGER
                        80 HTTP
                        88 KERBEROS
                        119 NNTP
                        123 NTP
                        161 SNMP
                        162 SNMPTRAP
                        179 BGP
                        194 IRC
                        520 RIP
                        521 RIPNG
                        3784 BFD
                    length
                        Type: INTEGER
                        Default: 0
                        A text name for the object. This attribute is required when you use stream block modifiers such as RangeModifier, RandomModifier,
                        and TableModifier. See the description of the OffsetReference attribute for these modifier objects for more information.
                    Name
                        Type: string
                    sourcePort
                        Type: INTEGER
                        Default: 1024

                vi) vlan:
                    vlan -
                        Default:
                        Type: INTEGER (0-4093)
                    pri -
                        Default: 000
                        Type: BITSTRING or INTEGER (0-7)

            name: Name of the range modifier
            Default: "" (empty string)
            Type: string

            mask: Mask with data bytes to be modified.
            Default: FF
            Type: string

            step: Step value of the modifier.
            Default: 01
            Type: string

            count: Number of times value will change.
            Default: 0
            Type: Integer

            useStreams: Whether streams or VFDs to generate modified values.
            Values: TRUE, FALSE
            Default: FALSE
            Type: bool

            incrType: Step mode.
            Values: DECR, INCR, SHUFFLE
            Default: INCR
            Type: enum

            Value: Initial value of the 'field' to be modified.
            Default: 00
            Type: string

            dConfigParam: Dictionary with key value pairs as name and value of the parameters to be configured
            Default: None
            Type: Dictionary

        Return:
            N/A

        Example:
            1) hlk_traffic_config_streamblock_range    streamblock=DS1    type=ipv4    field=destAddr    name=ipv4_modifier
                                                       mask=255.255.255.255    step=0.0.0.1    count=5    useStreams=TRUE
                                                       incrType=INCR    value=20.20.20.20

            2) ${dParam} =   Create_Dictionary    name=eth_modifier    mask=00:00:00:FF:FF:FF    step=00:00:00:00:00:10
                                                  count=2    useStreams=FALSE    incrType=DECR
               hlk_traffic_config_streamblock_range_modifier    streamblock=US1    type=ipv6    field=sourceAddr    dModifierParam=${dParam})

               Note-> Key should be mentioned as 'dModifierParam' inorder to consider the input dictionary

            3) ${dParam} =   Create_Dictionary    name=eth_modifier    mask=00:00:00:FF:FF:FF    step=00:00:00:00:00:10
                                                  count=2    useStreams=FALSE    incrType=DECR
               hlk_traffic_config_streamblock_range_modifier    streamblock=US1    type=ipv6    field=sourceAddr    dModifierParam=${dParam})
                                                               count=5    incrType=INCR

               To replace any parameter in the dictionary, mention the key and it's new value as an argument.
               In this case, the new count value is 5 and incrType is INCR instead of DECR.
        """
        llk_log_begin_proc()
        assert (kwargs.keys()), "No arguments given as input for hlk_traffic_config_streamblock_range_modifier"
        dKwargs = dict(map(lambda (k,v): (str(k).lower(), v), kwargs.iteritems()))
        llk_log_end_proc()
        assert ('streamblock' in dKwargs.keys()), "Streamblock name should be specified"
        assert ('type' in dKwargs.keys()), "Type of interface to be edited should be specified"
        assert ('field' in dKwargs.keys()), "Field name to be modified should be specified"

        sStreamblockName = str(dKwargs.get('streamblock'))
        if sStreamblockName:
            del dKwargs['streamblock']
        else:
            llk_log_end_proc()
            raise Exception ("The input value for the streamblock name %s is invalid" % str(sStreamblockName))

        sType = str(dKwargs.get('type'))
        if sType:
            del dKwargs['type']
        elif sType not in self.dStreamFieldType.keys():
            llk_log_end_proc()
            raise Exception ("The input value for the streamblock's type of interface %s is invalid" % str(sType))

        sField = str(dKwargs.get('field'))
        if sField:
            del dKwargs['field']
        else:
            llk_log_end_proc()
            raise Exception ("The input value for the streamblock's field name %s is invalid" % str(sField))

        if sStreamblockName in self.dStreamblockObjects.keys():
            oStreamblock = self.dStreamblockObjects[sStreamblockName]
        else:
            llk_log_end_proc()
            raise Exception ("No streamblock created with the streamblock name %s" % sStreamblockName)

        if 'vlan' in sType:
            sTypeObjectName = self.oLlkStc._llk_stc_get_vlan_field_name(oStreamblock,sType)
        else:
            sTypeObjectName = self.oLlkStc._llk_stc_get_field_value(oStreamblock,self.dStreamFieldType[sType],'name')

        if sTypeObjectName:
            sOffsetReference = sTypeObjectName + '.' + sField
        else:
            llk_log_end_proc()
            raise Exception ("There is no object name available for type %s!!" % sType)

        dLlkKwargs = {}
        dConfigParam = dKwargs.get('dmodifierparam')

        if dConfigParam:
            del dKwargs['dmodifierparam']
            dConfigParam = dict(map(lambda (k,v): (str(k).lower(), str(v)), dConfigParam.iteritems()))

        if dConfigParam and dKwargs.keys():
            for key in dKwargs.keys():
                dConfigParam[key] = dKwargs[key]
            dLlkKwargs = dConfigParam
        elif dConfigParam:
            dLlkKwargs = dConfigParam.copy()
        elif dKwargs.keys():
            dLlkKwargs = dKwargs.copy()

        dLlkKwargs = dict(map(lambda (k,v): (str(k).lower(), str(v)), dLlkKwargs.iteritems()))

        llk_log_info(dLlkKwargs)
        tRangeModifier = (sStreamblockName,sOffsetReference)

        for key in dLlkKwargs.keys():
            if key in self.dAllowedParam.keys():
                dLlkKwargs[self.dAllowedParam[key]] = dLlkKwargs[key]
                del dLlkKwargs[key]

        dLlkKwargs['offsetreference'] = sOffsetReference

        if 'data' not in dLlkKwargs.keys():
            if 'vlan' in sType:
                sValue = self.oLlkStc._llk_stc_get_vlan_field_value(oStreamblock, sType, sField)
            else:
                sValue = self.oLlkStc._llk_stc_get_field_value(oStreamblock, self.dStreamFieldType[sType], sField)

            if sValue:
                dLlkKwargs['data'] = sValue
            else:
                llk_log_warn("Since the value for %s is not configured, default value of '0' is assumed" % sField)

        if tRangeModifier in self.dRangeModifier.keys():
            oRangeModifier = self.dRangeModifier[tRangeModifier]
            self.oLlkStc.llk_stc_config_streamblock_range_modifier(oRangeModifier,**dLlkKwargs)
        else:
            oRangeModifier = self.oLlkStc.llk_stc_create_streamblock_range_modifier(oStreamblock,**dLlkKwargs)
            if oRangeModifier:
                self.dRangeModifier[tRangeModifier] = oRangeModifier
            else:
                llk_log_end_proc()
                raise Exception("Range modifier cannot be created. "
                                "Please check the input arguments for the keyword hlk_traffic_config_streamblock_range_modifier")

        llk_log_end_proc()

    def hlk_traffic_send_command(self,command):
        """
        Send a STC command
        Args:
            command: STC command
                Type: String
                Default: ""

        Return:
           N/A

        Example:
             hlk_traffic_send_command("stc::create project")
        """
        """
        :param command:
        :return:
        """
        self.oLlkStc.llk_stc_send_command(command)

    def hlk_traffic_operate_device_ipv4(self,**kwargs) :

        llk_log_begin_proc()
        assert (kwargs.keys()), "No arguments given as input for hlk_traffic_operate_device_ipv4"
        dKwargs = dict(map(lambda (k,v): (str(k).lower(), v), kwargs.iteritems()))
        assert ('device' in dKwargs.keys()), "Name of the device should be given!!"
        assert ('operation' in dKwargs.keys()), "operation should be given!!"

        sDevice    = dKwargs.pop('device')
        sOperation = dKwargs.pop('operation').lower()
        oDevice     = self.dDevices[sDevice]['oDevice']

        # define related stc commands
        dOperation = {
        'startarpnd':'stc::perform ArpNdStartCommand -HandleList %s;\
          stc::apply'
          % (oDevice)
        }

        # send stc command
        if sOperation.lower() in dOperation.keys():
            lCmd = dOperation[sOperation].split(";")
            for each_cmd in lCmd :
                self.oLlkStc.llk_stc_send_command(each_cmd)
        else:
            llk_log_end_proc()
            raise AssertionError("The operation is not supported:%s" % operation)

        llk_log_end_proc()     


    def hlk_traffic_config_device(self,**kwargs):
        """
        Create/Configure a device.

        Args:
            kwargs:

                name(Required): Name of the device
                    Default: ""
                    Type: String

                portID(Required when creating a new device): Port ID obtained from hlk_traffic_get_port_id
                    Default: ''
                    Type: String

                type(Required when configuring device parameters/interface): Type of device interface
                    Default: ""
                    Type: String
                    Values: device, ethii, vlan, ipv4, ipv6

                Configurable parameters based on the type:

                i) ethii:
                    Active - Whether this object will be active when you call the apply function.
                        Values: TRUE, FALSE
                        Default: TRUE
                        Type: bool
                    Authenticator - Authenticator identifier string.
                        Default: default
                        Type: string
                    IfCountPerLowerIf - Number of interfaces in this interface object.
                        Default: 1
                        Type: u32
                    IfRecycleCount - How many to times to increment source MAC address before returning to the starting value.
                        Default: 0
                        Type: u32
                    IsLoopbackIf - Whether this is a loopback interface.
                        Values: TRUE, FALSE
                        Default: FALSE
                        Type: bool
                    IsRange - Whether to use a combination of attributes to generate a range of source MAC addresses (SourceMac, SrcMacStep, SrcMacStepMask, SrcMacRepeatCount, and IfRecycleCount), or to use the SrcMacList attribute to specify the source MAC addresses.
                        Values: TRUE, FALSE
                        Default: TRUE
                        Type: bool
                    Name - A user-defined name for this object.
                        Default: "" (empty string)
                        Type: string
                    SourceMac - Source MAC address. Generate more than one source MAC address by using SrcMacRepeatCount, SrcMacStep, SrcMacStepMask, and IfRecycleCount.
                        Default: 00:10:94:00:00:02
                        Type: mac
                    SrcMacList - A Tcl list of source MAC addresses. IsRange must be FALSE.
                        Default: 0
                        Type: mac
                    SrcMacRepeatCount - Source MAC address repeat count.
                        Default: 0
                        Type: u32
                    SrcMacStep - Source MAC address step value.
                        Default: 00:00:00:00:00:01
                        Type: mac
                    SrcMacStepMask - Source MAC address step mask.
                        Default: 00:00:FF:FF:FF:FF
                        Type: mac
                    UseDefaultPhyMac - Whether or not to use source MAC address from physical interface.
                        Values: TRUE, FALSE
                        Default: FALSE
                        Type: bool

                ii) ipv4:
                    Active - Whether this object will be active when you call the apply function.
                        Values: TRUE, FALSE
                        Default: TRUE
                        Type: bool
                    Address - IPv4 address.
                        Value: IPv4 address
                        Default: 192.85.1.3
                        Type: ip
                    AddrList - A Tcl list of IPv4 addresses. IsRange must be FALSE.
                        Value: IPv4 address
                        Default: 0
                        Type: ip
                    AddrRepeatCount - How many times to repeat the same IPv4 address before incrementing it.
                        Default: 0
                        Type: u32
                    AddrResolver - Address resolver identifier.
                        Default: default
                        Type: string
                    AddrStep - IPv4 address step value.
                        Value: IPv4 address
                        Default: 0.0.0.1
                        Type: ip
                    AddrStepMask - IPv4 address step mask.
                        Value: IPv4 address
                        Default: 255.255.255.255
                        Type: ip
                    EnableGatewayLearning - NOTE: This attribute is deprecated. It will be removed in subsequent releases, so it is recommended that you do not use it. Not used on this object.
                        Values: TRUE, FALSE
                        Default: FALSE
                        Type: bool
                    Gateway - IPv4 gateway address.
                        Value: IPv4 address
                        Default: 192.85.1.1
                        Type: ip
                    GatewayList - A Tcl list of IPv4 gateway addresses. IsRange must be FALSE.
                        Value: IPv4 address
                        Default: 0
                        Type: ip
                    GatewayMac - Gateway MAC address.
                        Default: 00:00:01:00:00:01
                        Type: mac
                    GatewayMacResolver - Gateway MAC address resolver identifier.
                        Default: default
                        Type: string
                    GatewayRecycleCount - How many times to increment the IPv4 gateway address before returning to the starting value.
                        Default: 0
                        Type: u32
                    GatewayRepeatCount -
                        Default: 0
                        Type: u32
                    GatewayStep -
                        Value: IPv4 address
                        Default: 0.0.0.0
                        Type: ip
                    IfCountPerLowerIf - Number of interfaces in this interface object.
                        Default: 1
                        Type: u32
                    IfRecycleCount - How many times to increment the IPv4 address before returning to the starting value.
                        Default: 0
                        Type: u32
                    IsLoopbackIf - Whether this is a loopback interface.
                        Values: TRUE, FALSE
                        Default: FALSE
                        Type: bool
                    IsRange - Whether to use a combination of attributes to generate a range of addresses, or to use a list attribute (AddrList or GatewayList) to specify the addresses. The beginning address attributes are Address and Gateway.
                        Values: TRUE, FALSE
                        Default: TRUE
                        Type: bool
                    Name - A user-defined name for this object.
                        Default: "" (empty string)
                        Type: string
                    NeedsAuthentication - Whether this interface needs authentication.
                        Values: TRUE, FALSE
                        Default: FALSE
                        Type: bool
                    PrefixLength - IPv4 address prefix length.
                        Range: 0 - 32
                        Default: 24
                        Type: u8
                    ResolveGatewayMac - Whether to resolve the gateway MAC address.
                        Values: TRUE, FALSE
                        Default: TRUE
                        Type: bool
                    SkipReserved - Whether to skip reserved addresses.
                        Values: TRUE, FALSE
                        Default: TRUE
                        Type: bool
                    Tos - IPv4 ToS value.
                        Range: 0 - 255
                        Default: 192
                        Type: u8
                    TosType -
                        Values: DIFFSERV, TOS
                        Default: TOS
                        Type: enum
                    Ttl - IPv4 TTL value.
                        Range: 0 - 255
                        Default: 255
                        Type: u8
                    UseIpAddrRangeSettingsForGateway -
                        Values: TRUE, FALSE
                        Default: FALSE
                        Type: bool
                    UsePortDefaultIpv4Gateway - Whether to use the logical port's default IPv4 gateway.
                        Values: TRUE, FALSE
                        Default: FALSE
                        Type: bool

                iii) vlan:
                    vlan - Vlan ID
                        Default:
                        Type: INTEGER (0-4093)
                    pri - Priority
                        Default: 000
                        Type: BITSTRING or INTEGER (0-7)

                iv) ipv6:
                    Active - Whether this object will be active when you call the apply function.
                        Values: TRUE, FALSE
                        Default: TRUE
                        Type: bool
                    Address - IPv6 address.
                        Default: 2000::2
                        Type: ipv6
                    AddrList - A Tcl list of IPv6 addresses. IsRange must be FALSE.
                        Default: 0
                        Type: ipv6
                    AddrRepeatCount - How many times to repeat the same IPv6 address before incrementing it.
                        Default: 0
                        Type: u32
                    AddrResolver - IPv6 address resolver identifier.
                        Default: default
                        Type: string
                    AddrStep - IPv6 address step value.
                        Default: 0000::1
                        Type: ipv6
                    AddrStepMask - IPv6 address step mask.
                        Default: FFFF:FFFF:FFFF:FFFF:FFFF:FFFF:FFFF:FFFF
                        Type: ipv6
                    AllocateEui64LinkLocalAddress -
                        Values: TRUE, FALSE
                        Default: FALSE
                        Type: bool
                    EnableGatewayLearning - Whether to enable IPv6 learning for the gateway IP and MAC addresses.
                        Values: TRUE, FALSE
                        Default: FALSE
                        Type: bool
                    FlowLabel - Flow label.
                        Range: 0 - 1048575
                        Default: 7
                        Type: u32
                    Gateway - IPv6 gateway address.
                        Default: ::0
                        Type: ipv6
                    GatewayList - A Tcl list of IPv6 gateway addresses. IsRange must be FALSE.
                        Default: 0
                        Type: ipv6
                    GatewayMac - Gateway MAC address.
                        Default: 00:00:01:00:00:01
                        Type: mac
                    GatewayMacResolver - Gateway MAC address resolver identifier.
                        Default: default
                        Type: string
                    GatewayRecycleCount -
                        Default: 0
                        Type: u32
                    GatewayRepeatCount -
                        Default: 0
                        Type: u32
                    GatewayStep -
                        Default: 0000::0000
                        Type: ipv6
                    HopLimit - Hop limit.
                        Range: 0 - 255
                        Default: 255
                        Type: u8
                    IfCountPerLowerIf - Number of interfaces in this interface object.
                        Default: 1
                        Type: u32
                    IfRecycleCount - How many times to increment the IPv6 address before returning to the starting value.
                        Default: 0
                        Type: u32
                    IsLoopbackIf - Whether this is a loopback interface.
                        Values: TRUE, FALSE
                        Default: FALSE
                        Type: bool
                    IsRange - Whether to use a combination of attributes to generate a range of addresses, or to use a list attribute (AddrList or GatewayList) to specify the addresses. The beginning address attributes are Address and Gateway.
                        Values: TRUE, FALSE
                        Default: TRUE
                        Type: bool
                    Name - A user-defined name for this object.
                        Default: "" (empty string)
                        Type: string
                    NeedsAuthentication - Whether this interface needs authentication.
                        Values: TRUE, FALSE
                        Default: FALSE
                        Type: bool
                    PrefixLength - IPv6 address prefix length.
                        Range: 0 - 128
                        Default: 64
                        Type: u8
                    ResolveGatewayMac - Whether to resolve gateway MAC address.
                        Values: TRUE, FALSE
                        Default: TRUE
                        Type: bool
                    TrafficClass - Traffic class.
                        Range: 0 - 255
                        Default: 0
                        Type: u8
                    UseIpAddrRangeSettingsForGateway -
                        Values: TRUE, FALSE
                        Default: FALSE
                        Type: bool
                    UsePortDefaultIpv6Gateway - Whether to use the logical port's default IPv6 gateway.
                        Values: TRUE, FALSE
                        Default: FALSE
                        Type: bool

                v) device: Configurable parameters for a device
                    Active - Whether this object will be active when you call the apply function.
                        Values: TRUE, FALSE
                        Default: TRUE
                        Type: bool
                    DeviceCount - Number of devices in the device block.
                        Range: 1 - 0xFFFFFFFF
                        Default: 1
                        Type: u32
                    EnablePingResponse - Whether the emulated device will respond to ping.
                        Values: TRUE, FALSE
                        Default: FALSE
                        Type: bool
                    Ipv6RouterId - IPv6 Router ID.
                        Default: 2000::1
                        Type: ipv6
                    Ipv6RouterIdStep - IPv6 Router ID Step.
                        Default: 0000::1
                        Type: ipv6
                    Name - A user-defined name for this object.
                        Default: "" (empty string)
                        Type: string
                    RouterId - Router ID.
                        Value: IPv4 address
                        Default: 192.0.0.1
                        Type: ip
                    RouterIdStep - Router ID Step.
                        Value: IPv4 address
                        Default: 0.0.0.1
                        Type: ip

        Example:
            1) For creating a new device with interface:
                hlk_traffic_config_device(name=device1,portID=${sNetworkPortId},type=ethii,srcMac=00:00:40:20:77:50,dstMac=00:00:01:00:00:01)
            2) For creating a new device without interface:
                hlk_traffic_config_device(name=device1,portID=${sNetworkPortId})
            2) For configuring an existing device:
                hlk_traffic_config_device(name=device1,type=ethii,srcMac=00:00:40:20:77:50,dstMac=00:00:01:00:00:01)

        """
        llk_log_begin_proc()
        assert (kwargs.keys()), "No arguments given as input for hlk_traffic_config_device"
        dKwargs = dict(map(lambda (k,v): (str(k).lower(), v), kwargs.iteritems()))
        assert ('name' in dKwargs.keys()), "Name of the device to be created is not found!!"

        sName = dKwargs.get('name')
        if sName:
            del dKwargs['name']
        else:
            llk_log_end_proc()
            raise Exception ("The input value for name of the device %s is invalid" % sName)

        if sName in self.dDevices.keys():

            sItfType = dKwargs.get('type')
            if not sItfType:
                llk_log_end_proc()
                raise Exception ("The input value for type of the device %s is invalid" % sItfType)
            #Configure the existing device
            sPortId = dKwargs.get('portid')
            if sPortId:
                logger.info("Since the device is already created, port id is being ignored!!")
                del dKwargs['portid']

            if sItfType in self.dDevices[sName].keys():
                # itf already created
                self._hlk_traffic_config_streamblock_device_itf(sDevice=self.dDevices[sName]['oDevice'],sItf=self.dDevices[sName][sItfType],**dKwargs)
            elif sItfType in self.dDeviceItf.keys():
                # create a new itf
                oItf = self._hlk_traffic_config_streamblock_device_itf(sDevice=self.dDevices[sName]['oDevice'],sItf=sItfType,**dKwargs)
                self.dDevices[sName][sItfType] = oItf
            else:
                llk_log_end_proc()
                raise Exception ("The input value for type of the device interface %s is invalid" % sItfType)
            return self.dDevices[sName][sItfType]
        else:
            sPortId = dKwargs.get('portid')
            if sPortId and sPortId in self.dPortIDObjects.keys():
                del dKwargs['portid']
            else:
                llk_log_end_proc()
                raise Exception ("The input value for port id of the device %s is invalid" % sPortId)

            # Create device
            sPortObj = self.dPortIDObjects[sPortId]
            sDevice = self.oLlkStc.llk_stc_config_device(oPort=sPortObj,**dKwargs)
            if sDevice:
                self.dDevices[sName] = {}
                self.dDevices[sName]['oDevice'] = sDevice
                self.dDevices[sName]['oPort'] = sPortObj

            sItfType = dKwargs.get('type')
            if sItfType:
                oItf = self._hlk_traffic_config_streamblock_device_itf(sDevice=sDevice,sItf=sItfType,**dKwargs)
                self.dDevices[sName][sItfType] = oItf

            return self.dDevices[sName]['oDevice']

    def hlk_traffic_config_ipv6_device(self, **kwargs):
        """
        Return:
            N/A

        """
        llk_log_begin_proc()
        assert (kwargs.keys()), "No arguments given as input for hlk_traffic_config_dhcpv6_server"
        dKwargs = dict(map(lambda (k, v): (str(k).lower(), v), kwargs.iteritems()))
        #print ('dKwargs:', dKwargs)
        assert ('device' in dKwargs.keys()), "Name of the created device should be given!!"
        

        sDevice = str(dKwargs.get('device'))
        #print ('sDevice:', sDevice)

        if sDevice:
            del dKwargs['device']
        else:
            llk_log_end_proc()
            raise Exception("The input value given for the device %s is invalid" % str(sDevice))

        if sDevice not in self.dDevices.keys():
            llk_log_end_proc()
            raise Exception("The device %s is not created." % str(sDevice))

        


        #print ('self.dDevices[sDevice]:',self.dDevices[sDevice])

        # set toplevel,primary , uses itf and stacking device protocols
        if 'ipv6' in self.dDevices[sDevice].keys():
            self.oLlkStc.llk_stc_device_toplevel_itf(self.dDevices[sDevice]['oDevice'], self.dDevices[sDevice]['ipv6'])
            #self.oLlkStc.llk_stc_device_toplevel_itf(self.dDevices[sDevice]['oDevice'], self.dDevices[sDevice]['ipv6ll'])
            self.oLlkStc.llk_stc_device_primary_itf(self.dDevices[sDevice]['oDevice'], self.dDevices[sDevice]['ipv6ll'])
            #self.oLlkStc.llk_stc_device_uses_itf(self.dDevices[sDevice]['dhcpv6server'], self.dDevices[sDevice]['ipv6'])
            self.oLlkStc.llk_stc_AffiliationPort_itf(self.dDevices[sDevice]['oDevice'], self.dDevices[sDevice]['oPort'])

        else:
            raise Exception("Both dhcpv6 server and IPv6 interfaces should be configured")

        # stacking device protocols
        self.hlk_traffic_stack_dhcpv6_protocols(sDevice)
        #print ('self.dDevices[sDevice]:',self.dDevices[sDevice])
        #self.dDeviceType['dhcpv6server'].append(sDevice)
        #print ('dhcpv6_server_oItf:', oItf)
        return self.dDevices[sDevice]['oDevice']



    def _hlk_traffic_config_streamblock_device_itf(self, sDevice='', sItf='', **dKwargs):
        """
        Configure a device interface. This is for INTERNAL use only

        Args:
            sDevice: STC Device Object
                Default: ''
                Type: String

            sItf: Device interface to be configured
                Default: ''
                Type: String
                Values: ethii, ipv4, ipv6, vlan

            dKwargs: Dictionary of parameters to be configured
                Default: None
                Type: Dictionary
                Values: Refer to hlk_traffic_config_device for detailed description of all the configurable parameters

        Return:
            N/A

        Example:
            _hlk_traffic_config_streamblock_device_itf(sDevice=device1,sItf=ehtii,SourceMac=00:10:94:00:00:02)
        """
        llk_log_begin_proc()
        dLlkKwargs = {}

        sType = dKwargs.get('type')
        if sType:
            del dKwargs['type']
        else:
            llk_log_end_proc()
            raise Exception ("The input value given for type %s is invalid" % str(sType))

        dConfigParam = dKwargs.get('dconfigparam')
        if dConfigParam:
            del dKwargs['dconfigparam']
            dConfigParam = dict(map(lambda (k,v): (str(k).lower(), v), dConfigParam.iteritems()))

        if dConfigParam and dKwargs.keys():
            for key in dKwargs.keys():
                dConfigParam[key] = dKwargs[key]
            dLlkKwargs = dConfigParam.copy()
        elif dConfigParam:
            dLlkKwargs = dConfigParam.copy()
        elif dKwargs.keys():
            dLlkKwargs = dKwargs.copy()

        if sType in self.dDeviceItf.keys():
            if sType == 'device':
                self.oLlkStc.llk_stc_config_device(oDevice=sDevice,**dLlkKwargs)
            else:
                # check if itf exist
                sCommand = "stc::get " + sDevice + " -children-" + str(sType) + 'if'
                oItf = str.split(str(self.oLlkStc.llk_stc_send_command(sCommand)))
                # create or modify itf
                oItf = self.dDeviceItf[str(sType)](sDevice,oItf,**dLlkKwargs)
                return oItf
        else:
            llk_log_end_proc()
            raise Exception("The input value entered for type %s is invalid" % str(sType))



    def hlk_traffic_config_deviceb(self,**kwargs):
        """

        Example:
            1) For creating a new device with interface:
                hlk_traffic_config_device(name=device1,portID=${sNetworkPortId},type=ethii,srcMac=00:00:40:20:77:50,dstMac=00:00:01:00:00:01)
            2) For creating a new device without interface:
                hlk_traffic_config_device(name=device1,portID=${sNetworkPortId})
            2) For configuring an existing device:
                hlk_traffic_config_device(name=device1,type=ethii,srcMac=00:00:40:20:77:50,dstMac=00:00:01:00:00:01)

        """
        llk_log_begin_proc()
        assert (kwargs.keys()), "No arguments given as input for hlk_traffic_config_device"
        dKwargs = dict(map(lambda (k,v): (str(k).lower(), v), kwargs.iteritems()))
        assert ('name' in dKwargs.keys()), "Name of the device to be created is not found!!"

        #print ('htcd_dKwargs',dKwargs)
        sName = dKwargs.get('name')
        #print ('htcd_sName',sName)

        if sName:
            del dKwargs['name']
        else:
            llk_log_end_proc()
            raise Exception ("The input value for name of the device %s is invalid" % sName)

        if sName in self.dDevices.keys():
            #Configure the existing device
            sPortId = dKwargs.get('portid')
            #print ('htcd_sPortId1',sPortId)
            if sPortId:
                logger.info("Since the device is already created, port id is being ignored!!")
                del dKwargs['portid']

            sItfType = dKwargs.get('type')
            #print ('htcd_sItfType',sItfType)
            if sItfType in self.dDevices[sName].keys():
                # itf already created
                self._hlk_traffic_config_streamblock_device_itfb(sDevice=self.dDevices[sName]['oDevice'],sItf=self.dDevices[sName][sItfType],**dKwargs)
            elif sItfType in self.dDeviceItf.keys():
                # create a new itf
                oItf = self._hlk_traffic_config_streamblock_device_itfb(sDevice=self.dDevices[sName]['oDevice'],sItf=sItfType,**dKwargs)
                self.dDevices[sName][sItfType] = oItf
                #print ('htcd_oItf',oItf)
            else:
                llk_log_end_proc()
                raise Exception ("The input value for type of the device interface %s is invalid" % sItfType)
        else:
            sPortId = dKwargs.get('portid')
            #print ('htcd_sPortId2',sPortId)
            if sPortId and sPortId in self.dPortIDObjects.keys():
                del dKwargs['portid']
            else:
                llk_log_end_proc()
                raise Exception ("The input value for port id of the device %s is invalid" % sPortId)

            # Create device
            sPortObj = self.dPortIDObjects[sPortId]
            sDevice = self.oLlkStc.llk_stc_config_device(oPort=sPortObj,**dKwargs)
            #print ('htcd_sPortObj',sPortObj)
            #print ('htcd_sDevice',sDevice)
            if sDevice:
                self.dDevices[sName] = {}
                self.dDevices[sName]['oDevice'] = sDevice
                self.dDevices[sName]['oPort'] = sPortObj

            sType = dKwargs.get('type')
            #print ('htcd_sType',sType)
            if sType:
                self._hlk_traffic_config_streamblock_device_itfb(sDevice=sDevice,**dKwargs)

    def _hlk_traffic_config_streamblock_device_itfb(self, sDevice='', sItf='', **dKwargs):
        """
        Configure a device interface. This is for INTERNAL use only

        Example:
            _hlk_traffic_config_streamblock_device_itf(sDevice=device1,sItf=ehtii,SourceMac=00:10:94:00:00:02)
        """
        llk_log_begin_proc()
        dLlkKwargs = {}

        sType = dKwargs.get('type')
        #print ('htcsd_sType',sType)
        if sType:
            del dKwargs['type']
        else:
            llk_log_end_proc()
            raise Exception ("The input value given for type %s is invalid" % str(sType))

        dConfigParam = dKwargs.get('dconfigparam')
        #print ('htcsd_dConfigParam',dConfigParam)
        if dConfigParam:
            del dKwargs['dconfigparam']
            dConfigParam = dict(map(lambda (k,v): (str(k).lower(), v), dConfigParam.iteritems()))
            #print ('htcsd_dConfigParam',dConfigParam)

        if dConfigParam and dKwargs.keys():
            for key in dKwargs.keys():
                dConfigParam[key] = dKwargs[key]
                ##print ('htcsd_dConfigParam[key]',dConfigParam[key])
            dLlkKwargs = dConfigParam.copy()
            ##print ('htcsd_dLlkKwargs1',dLlkKwargs)
        elif dConfigParam:
            dLlkKwargs = dConfigParam.copy()
            ##print ('htcsd_dLlkKwargs2',dLlkKwargs)
        elif dKwargs.keys():
            dLlkKwargs = dKwargs.copy()
            #print ('htcsd_dLlkKwargs3',dLlkKwargs)

        if sType in self.dDeviceItf.keys():
            if sType == 'device':
                self.oLlkStc.llk_stc_config_device(oDevice=sDevice,**dLlkKwargs)
            else:
                # check if itf exist
                #sCommand = "stc::get " + sDevice + " -children-" + str(sType) + 'if'
                ##print ('htcsd_sCommand1',sCommand)
                #oItf = str.split(str(self.oLlkStc.llk_stc_send_command(sCommand)))
                ##print ('htcsd_oItf1',oItf)
                # create or modify itf
                oItf = []
                oItf = self.dDeviceItf[str(sType)](sDevice,oItf,**dLlkKwargs)
                ##print ('htcsd_oItf2',oItf)
                return oItf
        else:
            llk_log_end_proc()
            raise Exception("The input value entered for type %s is invalid" % str(sType))





    def hlk_traffic_config_dhcp_client(self,**kwargs):
        """
        Create/Configure DHCP client device.

        Args:
            device(Required): Name of the device to be configured
                Type: String
                Default: ""

            Active - Whether this object will be active when you call the apply function.
                Values: TRUE, FALSE
                Default: TRUE
                Type: bool
            CircuitId - Specify the Circuit ID field in the message sent by the relay agent. Use wildcard characters to make each circuit ID unique.
                Value: 1 - 128characters
                Default: circuitId_@p
                Type: string
            ClientRelayAgent - Not exposed in the GUI.
                Values: TRUE, FALSE
                Default: FALSE
                Type: bool
            DefaultHostAddrPrefixLength - Default prefix length for the host address.
                Range: 0 - 32
                Default: 24
                Type: u8
            DNAv4DestIp - DNAv4 destination IP address.
                Value: IPv4 address
                Default: 192.85.1.1
                Type: ip
            DNAv4DestMac - DNAv4 destination MAC address.
                Default: 00-10-01-00-00-01
                Type: mac
            EnableArpServerId - Enable or disable ARP on the Server ID.
                Values: TRUE, FALSE
                Default: FALSE
                Type: bool
            EnableAutoRetry - Enable auto retry. Auto retry will retry sessions that fail to initially come up.
                Values: TRUE, FALSE
                Default: FALSE
                Type: bool
            EnableCircuitId - Enable the circuit ID sub-option in DHCP messages.
                Values: TRUE, FALSE
                Default: FALSE
                Type: bool
            EnableRelayAgent - Enables/disables the RFC 3046 relay agent option.
                Values: TRUE, FALSE
                Default: FALSE
                Type: bool
            EnableRelayLinkSelection - Enable relay agent link selection sub-option in DHCP messages sent from emulated relay agent.
                Values: TRUE, FALSE
                Default: FALSE
                Type: bool
            EnableRelayServerIdOverride - Enable relay agent server identifier override sub-option in DHCP messages sent from emulated relay agent.
                Values: TRUE, FALSE
                Default: FALSE
                Type: bool
            EnableRelayVPNID - Enable relay agent virtual subnet selection sub-option in DHCP messages sent from emulated relay agent.
                Values: TRUE, FALSE
                Default: FALSE
                Type: bool
            EnableRemoteId - Enable remote ID sub-option in DHCP messages sent from emulated relay agent.
                Values: TRUE, FALSE
                Default: FALSE
                Type: bool
            EnableRouterOption - Enable the router option (option 3) specified in RFC 2132.
                Values: TRUE, FALSE
                Default: FALSE
                Type: bool
            HostName - Unique hostname of emulated client. Use wildcard characters to make each hostname unique.
                Value: 1 - 32characters
                Default: client_@p-@b-@s
                Type: string
            Ipv4Tos - Provides an indication of the quality of service wanted.
                Default: 192
                Type: u8
            Name - A user-defined name for this object.
                Default: "" (empty string)
                Type: string
            OptionList - A list of Option 55 numbers for the DHCP request messages on each session block.
                Default: 1
                Type: u8
            RelayAgentIpv4Addr - Relay agent message source IP address.
                Value: IPv4 address
                Default: 0.0.0.0
                Type: ip
            RelayAgentIpv4AddrMask - IP Mask to apply to Relay Local IP address.
                Value: IPv4 address
                Default: 255.255.0.0
                Type: ip
            RelayAgentIpv4AddrStep - IP Step to be applied to the Relay Local IP address.
                Value: IPv4 address
                Default: 0.0.0.1
                Type: ip
            RelayClientMacAddrMask - MAC mask that will be applied to the Relay Client MAC address.
                Default: 00-00-00-ff-ff-ff
                Type: mac
            RelayClientMacAddrStart - Starting value for the MAC address.
                Default: 00-10-01-00-00-01
                Type: mac
            RelayClientMacAddrStep - MAC step that will be applied to the Relay Client MAC address.
                Default: 00-00-00-00-00-01
                Type: mac
            RelayLinkSelection - Link selection field for the message sent by the relay agent.
                Value: IPv4 address
                Default: 192.85.1.1
                Type: ip
            RelayPoolIpv4Addr - Number of Relay Agent networks.
                Value: IPv4 address
                Default: 0.0.0.0
                Type: ip
            RelayPoolIpv4AddrStep - Relay pool IPv4 address step.
                Value: IPv4 address
                Default: 0.0.1.0
                Type: ip
            RelayServerIdOverride - Server identifier override field for the message sent by the relay agent.
                Value: IPv4 address
                Default: 192.85.1.1
                Type: ip
            RelayServerIpv4Addr - Relay agent message destination IP address.
                Value: IPv4 address
                Default: 0.0.0.0
                Type: ip
            RelayServerIpv4AddrStep - IP Step to be applied to the Relay Server IP address.
                Value: IPv4 address
                Default: 0.0.0.1
                Type: ip
            RemoteId - Remote ID field for the message sent by the relay agent.
                Value: 1 - 128characters
                Default: remoteId_@p-@b-@s
                Type: string
            RetryAttempts - Number of times to retry the session after the initial failure (each retry will use a new transaction ID).
                Range: 1 - 4294967295
                Default: 4
                Type: u32
            UseBroadcastFlag - Enable/disable broadcast bit in DHCP control plane packets.
                Values: TRUE, FALSE
                Default: TRUE
                Type: bool
            UseClientMacAddrForDataplane - Use client MAC address for the data plane.
                Values: TRUE, FALSE
                Default: FALSE
                Type: bool
            UsePartialBlockState - Flag indicating partial block state as used.
                Values: TRUE, FALSE
                Default: FALSE
                Type: bool
            VPNId - An identifier of virtual subnet selection information
                Value: 1 - 128characters
                Default: spirent_@p
                Type: string
            VPNType - Type of VPN-ID
                Values: NVT_ASCII, RFC_2685
                Default: NVT_ASCII
                Type: enum

        Return:
            N/A

        Example:
            hlk_traffic_config_dhcp_client(device='device1',DNAv4DestIp=192.85.1.1,VPNId=spirent_@p)
        """
        llk_log_begin_proc()
        assert (kwargs.keys()), "No arguments given as input for hlk_traffic_config_dhcp_client"
        dKwargs = dict(map(lambda (k,v): (str(k).lower(), v), kwargs.iteritems()))
        assert ('device' in dKwargs.keys()), "Name of the created device should be given!!"

        sKwargs = {}
        sDevice = str(dKwargs.get('device'))
        if sDevice:
            del dKwargs['device']
        else:
            llk_log_end_proc()
            raise Exception("The input value given for the device %s is invalid" % str(sDevice))

        if sDevice not in self.dDevices.keys():
            llk_log_end_proc()
            raise Exception("The device %s is not created." % str(sDevice))
        else:
            dKwargs['Active'] = 'TRUE'
            logger.info("keys:%s" % self.dDevices[sDevice].keys())
            if 'dhcpv4client' in self.dDevices[sDevice].keys():
                self.oLlkStc.llk_stc_config_device_dhcpv4_itf(oDevice=sDevice, oDHCPv4If=self.dDevices[sDevice]['dhcpv4client'].strip('{}[]()'), **dKwargs)
            else:
                oItf = self.oLlkStc.llk_stc_config_device_dhcpv4_itf(oDevice=self.dDevices[sDevice]['oDevice'], **dKwargs)
                if oItf:
                    self.dDevices[sDevice]['dhcpv4client'] = oItf
                else:
                    llk_log_end_proc()
                    raise Exception("DHCP client could not be configured")

            """
            #Stacking the protocols
            sKwargs = sorted(dKwargs)
            lVlans = []
            for key in sKwargs.keys():
                if 'vlan' in key:
                    lVlans.append(sKwargs[key])

            if lVlans:
                iNumVlans = len(lVlans)
                if iNumVlans > 1:
                    for i in range(0,iNumVlans-1):
                        self.oLlkStc.llk_stc_device_stackedon_itf(lVlans[i+1],lVlans[i])

                if 'ethii' in self.dDevices[sDevice].keys():
                    self.oLlkStc.llk_stc_device_stackedon_itf(lVlans[0],self.dDevices[sDevice]['ethii'])
                else:
                    llk_log_end_proc()
                    raise Exception("Ethernet interface is not created in order to create a DHCP client")

                if 'ipv4' in self.dDevices[sDevice].keys():
                    self.oLlkStc.llk_stc_device_stackedon_itf(self.dDevices[sDevice]['ipv4'],lVlans[-1])
                else:
                    llk_log_end_proc()
                    raise Exception("IP interface is not created in order to create a DHCP client")


            if 'ethii' in self.dDevices[sDevice].keys() and 'ipv4' in self.dDevices[sDevice].keys():
                self.oLlkStc.llk_stc_device_stackedon_itf(self.dDevices[sDevice]['ipv4'],self.dDevices[sDevice]['ethii'])
            else:
                llk_log_end_proc()
                raise Exception("Ethernet and IP interfaces are to be created in order to create a DHCP client")

            self.oLlkStc.llk_stc_device_toplevel_itf(self.dDevices[sDevice]['oDevice'],self.dDevices[sDevice]['ipv4'])
            self.oLlkStc.llk_stc_device_primary_itf(self.dDevices[sDevice]['oDevice'],self.dDevices[sDevice]['ipv4'])
            #self.oLlkStc.llk_stc_device_uses_itf(self.dDevices[sDevice]['oDevice'],self.dDevices[sDevice]['ipv4'])
            """
            # set toplevel,primary and uses itf
            self.oLlkStc.llk_stc_device_toplevel_itf(self.dDevices[sDevice]['oDevice'],self.dDevices[sDevice]['ipv4'])
            self.oLlkStc.llk_stc_device_primary_itf(self.dDevices[sDevice]['oDevice'],self.dDevices[sDevice]['ipv4'])
            self.oLlkStc.llk_stc_device_uses_itf(self.dDevices[sDevice]['dhcpv4client'],self.dDevices[sDevice]['ipv4'])

            # stacking device protocols
            self.hlk_traffic_stack_device_protocols(sDevice)

            self.dDeviceType['dhcpv4client'].append(sDevice)

    def hlk_traffic_config_dhcp_server(self,**kwargs):
        """
        Create/Configure DHCPv4 server, DHCPv4 server pool and DHCPv4 default server pool.

        Args:
            Input is given in the form of key value pairs where in each value is a list of parameters to be configured separated by ','
            The keys can take the value 'device', 'serverconfig', 'serverpoolconfig', 'serverdefaultpoolconfig'

            kwargs:
                device(Required): Name of the device to be configured
                    Type: String
                    Default: ""

                serverconfig(Required for configuring server/server pool/server default pool) : List of parameters for configuring a server
                    Type: List
                    Default: []
                    Values:
                    Active - Whether this object will be active when you call the apply function.
                        Values: TRUE, FALSE
                        Default: TRUE
                        Type: bool
                    AssignStrategy - The strategy that server choose address pools which are used for assign address.
                        Values: CIRCUIT_ID, GATEWAY, LINK_SELECTION, POOL_BY_POOL, REMOTE_ID, VPN_ID
                        Default: GATEWAY
                        Type: enum
                    DeclineReserveTime - Time in seconds an address will be reserved after it is declined by the client.
                        Default: 10
                        Type: u32
                    EnableOverlapAddress - Enable reuse addresses based on circuit ID.
                        Values: TRUE, FALSE
                        Default: FALSE
                        Type: bool
                    HostName - Server host name.
                        Value: 1 - 32characters
                        Default: server_@p-@b-@s
                        Type: string
                    Ipv4Tos - Provides an indication of the quality of service wanted.
                        Default: 192
                        Type: u8
                    LeaseTime - Default lease time in seconds.
                        Default: 3600
                        Type: u32
                    MinAllowedLeaseTime - Minimum allowed lease time in seconds.
                        Default: 600
                        Type: u32
                    Name - A user-defined name for this object.
                        Default: "" (empty string)
                        Type: string
                    OfferReserveTime - Time in seconds an address will be reserved while the server is waiting for a response for an offer.
                        Default: 10
                        Type: u32
                    RebindingTimePercent - (T2) Percent of the lease time at which the client should begin the rebinding process.
                        Range: 0.0 - 200.0
                        Default: 87.5
                        Type: double
                    RenewalTimePercent - (T1) Percent of the lease time at which the client should begin the renewal process.
                        Range: 0.0 - 200.0
                        Default: 50.0
                        Type: double
                    UsePartialBlockState - Flag indicating partial block state as used.
                        Values: TRUE, FALSE
                        Default: FALSE
                        Type: bool

                serverpoolconfig (Required for configuring server pool): List of parameters for configuring a server pool
                    Type: List
                    Default: []
                    Values:
                        Active - Whether this object will be active when you call the apply function.
                            Values: TRUE, FALSE
                            Default: TRUE
                            Type: bool
                        AddrIncrement - Pool address network increment.
                            Default: 1
                            Type: u32
                        AddrIncrementPerRouter - NOTE: This attribute is deprecated. It will be removed in subsequent releases, so it is recommended that you do not use it. Per router increment.
                            Default: 0
                            Type: u32
                        CircuitId - Generate a list of circuit ID.
                            Value: 1 - 128characters
                            Default: circuitId_@p
                            Type: string
                        CircuitIdCount - Maximum circuit ID count.
                            Default: 1
                            Type: u32
                        DomainName - Domain name (option 15).
                            Default: "" (empty string)
                            Type: string
                        DomainNameServerList - Domain name servers (option 6).
                            Value: IPv4 address
                            Default: 0
                            Type: ip
                        EnablePoolAddrPrefix - Enable this pool to use a customized prefix length.
                            Values: TRUE, FALSE
                            Default: FALSE
                            Type: bool
                        HostAddrCount - Number of addresses in a pool.
                            Default: 254
                            Type: u32
                        HostAddrPrefixLength - Customized prefix length value.
                            Range: 0 - 32
                            Default: 24
                            Type: u8
                        HostAddrStep - Pool host address step.
                            Value: IPv4 address
                            Default: 0.0.0.1
                            Type: ip
                        LimitHostAddrCount - Limit host address count.
                            Values: TRUE, FALSE
                            Default: FALSE
                            Type: bool
                        Name - A user-defined name for this object.
                            Default: "" (empty string)
                            Type: string
                        NetworkCount - Number of pools.
                            Default: 1
                            Type: u32
                        PrefixLength - Pool prefix length.
                            Range: 0 - 32
                            Default: 24
                            Type: u8
                        RemoteId - Generate a list of remote ID.
                            Value: 1 - 128characters
                            Default: remoteId_@p-@b-@s
                            Type: string
                        RemoteIdCount - Maximum remote ID count.
                            Range: 1 - 20
                            Default: 1
                            Type: u32
                        RouterList - Router addresses (option 3).
                            Value: IPv4 address
                            Default: 0
                            Type: ip
                        StartIpList - Pool starting IP address.
                            Value: IPv4 address
                            Default: 192.0.1.0
                            Type: ip
                        VPNId - Generate a list of VPN ID.
                            Value: 1 - 128characters
                            Default: spirent_@p
                            Type: string
                        VPNIdCount - Maximum VPN ID count.
                            Default: 1
                            Type: u32
                        VPNType - Type of VPN-ID
                            Values: NVT_ASCII, RFC_2685
                            Default: NVT_ASCII
                            Type: enum

                serverdefaultpoolconfig(Required for configuring default server pool) : List of parameters for configuring a server default pool
                    Type: List
                    Default: []
                    Values:
                        Active - Whether this object will be active when you call the apply function.
                            Values: TRUE, FALSE
                            Default: TRUE
                            Type: bool
                        AddrIncrement - Pool address network increment.
                            Default: 1
                            Type: u32
                        AddrIncrementPerRouter - NOTE: This attribute is deprecated. It will be removed in subsequent releases, so it is recommended that you do not use it. Per router increment.
                            Default: 0
                            Type: u32
                        CircuitId - Generate a list of circuit ID.
                            Value: 1 - 128characters
                            Default: circuitId_@p
                            Type: string
                        CircuitIdCount - Maximum circuit ID count.
                            Default: 1
                            Type: u32
                        DomainName - Domain name (option 15).
                            Default: "" (empty string)
                            Type: string
                        DomainNameServerList - Domain name servers (option 6).
                            Value: IPv4 address
                            Default: 0
                            Type: ip
                        EnablePoolAddrPrefix - Enable this pool to use a customized prefix length.
                            Values: TRUE, FALSE
                            Default: FALSE
                            Type: bool
                        HostAddrCount - Number of addresses in a pool.
                            Default: 254
                            Type: u32
                        HostAddrPrefixLength - Customized prefix length value.
                            Range: 0 - 32
                            Default: 24
                            Type: u8
                        HostAddrStep - Pool host address step.
                            Value: IPv4 address
                            Default: 0.0.0.1
                            Type: ip
                        LimitHostAddrCount - Limit host address count.
                            Values: TRUE, FALSE
                            Default: FALSE
                            Type: bool
                        Name - A user-defined name for this object.
                            Default: "" (empty string)
                            Type: string
                        NetworkCount - Number of pools.
                            Default: 1
                            Type: u32
                        PrefixLength - Pool prefix length.
                            Range: 0 - 32
                            Default: 24
                            Type: u8
                        RemoteId - Generate a list of remote ID.
                            Value: 1 - 128characters
                            Default: remoteId_@p-@b-@s
                            Type: string
                        RemoteIdCount - Maximum remote ID count.
                            Range: 1 - 20
                            Default: 1
                            Type: u32
                        RouterList - Router addresses (option 3).
                            Value: IPv4 address
                            Default: 0
                            Type: ip
                        StartIpList - Pool starting IP address.
                            Value: IPv4 address
                            Default: 192.0.1.0
                            Type: ip
                        VPNId - Generate a list of VPN ID.
                            Value: 1 - 128characters
                            Default: spirent_@p
                            Type: string
                        VPNIdCount - Maximum VPN ID count.
                            Default: 1
                            Type: u32
                        VPNType - Type of VPN-ID
                            Values: NVT_ASCII, RFC_2685
                            Default: NVT_ASCII
                            Type: enum

        Return:
            N/A

        Example:
            hlk_traffic_config_dhcp_server(device=devcie1, serverConfig=[HostName="host1"], serverdefaultpoolconfig=[StartIpList=192.0.1.0], serverpoolconfig=[Active=TRUE])
        """
        llk_log_begin_proc()
        assert (kwargs.keys()), "No arguments given as input for hlk_traffic_config_dhcp_server"
        dKwargs = dict(map(lambda (k,v): (str(k).lower(), v), kwargs.iteritems()))
        assert ('device' in dKwargs.keys()), "Name of the created device should be given!!"
        assert ('serverconfig' in dKwargs.keys()), "Name of the created device should be given!!"

        oItf = None

        sDevice = str(dKwargs.get('device'))
        if sDevice:
            del dKwargs['device']
        else:
            llk_log_end_proc()
            raise Exception("The input value given for the device %s is invalid" % str(sDevice))

        if sDevice not in self.dDevices.keys():
            llk_log_end_proc()
            raise Exception("The device %s is not created." % str(sDevice))

        if 'serverconfig' in dKwargs.keys():
            dServerPoolConfigParsed = {}
            dServerPoolConfigParsed = self._hlk_traffic_parse_server_param(sServerConfigType='serverconfig',**dKwargs)

            if 'dhcpv4server' in self.dDevices[sDevice].keys():
                self.oLlkStc.llk_stc_config_device_dhcpv4_server_itf(oDevice=self.dDevices[sDevice]['oDevice'], oDHCPv4If=self.dDevices[sDevice]['dhcpv4server'], **dServerPoolConfigParsed)
                logger.info("DHCPv4 server has been successfully configured.")
            else:
                oItf = self.oLlkStc.llk_stc_config_device_dhcpv4_server_itf(oDevice=self.dDevices[sDevice]['oDevice'], **dServerPoolConfigParsed)
                if oItf:
                    self.dDevices[sDevice]['dhcpv4server'] = oItf
                    logger.info("DHCPv4 server has been successfully created.")
                else:
                    llk_log_end_proc()
                    raise Exception("DHCPv4 server could not be configured")

        if 'serverpoolconfig' in dKwargs.keys():
            dServerPoolConfigParsed = {}

            if 'dhcpv4server' not in self.dDevices[sDevice].keys():
                raise Exception("Server is not created. Create it before configuring Server Pool")

            dServerPoolConfigParsed = self._hlk_traffic_parse_server_param(sServerConfigType='serverpoolconfig',**dKwargs)

            if 'dhcpv4serverpool' in self.dDevices[sDevice].keys():
                self.oLlkStc.llk_stc_config_dhcpv4_server_pool(oDhcpServerConfig=self.dDevices[sDevice]['dhcpv4server'], oDhcpServerPoolConfig=self.dDevices[sDevice]['dhcpv4serverpool'], **dServerPoolConfigParsed)
                logger.info("DHCPv4 server pool has been successfully configured.")
            else:
                oItf = self.oLlkStc.llk_stc_config_dhcpv4_server_pool(oDhcpServerConfig=self.dDevices[sDevice]['dhcpv4server'], **dServerPoolConfigParsed)
                if oItf:
                    self.dDevices[sDevice]['dhcpv4serverpool'] = oItf
                    logger.info("DHCPv4 server pool has been successfully created")
                else:
                    llk_log_end_proc()
                    raise Exception("DHCPv4 server pool could not be configured")

        if 'serverdefaultpoolconfig' in dKwargs.keys():
            dServerDefaultPoolConfigParsed = {}

            if 'dhcpv4server' not in self.dDevices[sDevice].keys():
                raise Exception("Server is not created. Create it before configuring Server Pool")

            dServerDefaultPoolConfigParsed = self._hlk_traffic_parse_server_param(sServerConfigType='serverdefaultpoolconfig',**dKwargs)

            if 'dhcpv4serverdefaultpool' in self.dDevices[sDevice].keys():
                self.oLlkStc.llk_stc_config_dhcpv4_server_default_pool(oDhcpServerConfig=self.dDevices[sDevice]['dhcpv4server'], oDhcpServerPoolConfig=self.dDevices[sDevice]['dhcpv4serverdefaultpool'], **dServerDefaultPoolConfigParsed)
                logger.info("DHCPv4 server pool has been successfully configured.")
            else:
                oItf = self.oLlkStc.llk_stc_config_dhcpv4_server_default_pool(oDhcpServerConfig=self.dDevices[sDevice]['dhcpv4server'], **dServerDefaultPoolConfigParsed)
                if oItf:
                    self.dDevices[sDevice]['dhcpv4serverdefaultpool'] = oItf
                    logger.info("DHCPv4 server default pool has been successfully created")
                else:
                    llk_log_end_proc()
                    raise Exception("DHCPv4 server default pool could not be configured")

        # set toplevel,primary and uses itf
        if 'dhcpv4server' in self.dDevices[sDevice].keys() and 'ipv4' in self.dDevices[sDevice].keys():
            self.oLlkStc.llk_stc_device_toplevel_itf(self.dDevices[sDevice]['oDevice'],self.dDevices[sDevice]['ipv4'])
            self.oLlkStc.llk_stc_device_primary_itf(self.dDevices[sDevice]['oDevice'],self.dDevices[sDevice]['ipv4'])
            self.oLlkStc.llk_stc_device_uses_itf(self.dDevices[sDevice]['dhcpv4server'],self.dDevices[sDevice]['ipv4'])
        else:
            raise Exception("Both DHCPv4 server and IPv4 interfaces should be configured")

        # stacking device protocols
        self.hlk_traffic_stack_device_protocols(sDevice)

        self.dDeviceType['dhcpv4server'].append(sDevice)

        # dhcp options
        for each_key in dKwargs.keys() :
            if 'option_' in str(each_key).lower() : 
                m = re.search('option_(\d+)',each_key.lower())
                if m :
                    dOption = {}
                    # set optionType
                    dOption['optionType'] = m.group(1)

                    # set other option params from input 
                    lOptions = dKwargs[each_key].strip('{}[]()').split(',')
                    for each_option in lOptions:
                        key,value=each_option.split('=')
                        dOption[key] = value
                    
                    # create option object
                    self.oLlkStc.llk_stc_config_dhcpv4_options(
                    self.dDevices[sDevice]['dhcpv4serverdefaultpool'],**dOption)

        return oItf


    def hlk_traffic_operate_dhcp_server(self,**kwargs) :

        llk_log_begin_proc()
        assert (kwargs.keys()), "No arguments given as input for hlk_traffic_operate_dhcp_server"
        dKwargs = dict(map(lambda (k,v): (str(k).lower(), v), kwargs.iteritems()))
        assert ('device' in dKwargs.keys()), "Name of the dhcp server should be given!!"
        assert ('operation' in dKwargs.keys()), "operation should be given!!"

        sDevice    = dKwargs.pop('device')
        sOperation = dKwargs.pop('operation').lower() 
        oDhcpServer = self.dDevices[sDevice]['dhcpv4server']
        oDevice     = self.dDevices[sDevice]['oDevice']

        # define related stc commands
        dOperation = {
        'forcerenew':'stc::perform Dhcpv4ForceRenewFromServerCommand -ServerList %s;\
          stc::apply'
          % (oDhcpServer)
        }

        # send stc command
        if sOperation.lower() in dOperation.keys():
            lCmd = dOperation[sOperation].split(";")
            for each_cmd in lCmd :
                self.oLlkStc.llk_stc_send_command(each_cmd) 
        else:
            llk_log_end_proc()
            raise AssertionError("The operation is not supported:%s" % operation)

        llk_log_end_proc()


    def hlk_traffic_operate_dhcp_client(self,**kwargs) :

        llk_log_begin_proc()
        assert (kwargs.keys()), "No arguments given as input for hlk_traffic_operate_dhcp_client"
        dKwargs = dict(map(lambda (k,v): (str(k).lower(), v), kwargs.iteritems()))
        assert ('device' in dKwargs.keys()), "Name of the dhcp client should be given!!"
        assert ('operation' in dKwargs.keys()), "operation should be given!!"

        sDevice    = dKwargs.pop('device')
        sOperation = dKwargs.pop('operation')
        oDhcpBlkCfg = self.dDevices[sDevice]['dhcpv4client']
        oDevice     = self.dDevices[sDevice]['oDevice']

        # define related stc commands
        dOperation = {
        'bind':'stc::perform Dhcpv4Bind -BlockList %s'%(oDhcpBlkCfg),\
        'release':'stc::perform Dhcpv4Release -BlockList %s' % (oDhcpBlkCfg),\
        'renew':'stc::perform Dhcpv4Renew -BlockList %s' % (oDhcpBlkCfg),\
        'abort':'stc::perform Dhcpv4Abort -BlockList %s' % (oDhcpBlkCfg),\
        'rebind':'stc::perform Dhcpv4Rebind -BlockList %s' % (oDhcpBlkCfg)
        }

        # send stc command
        if sOperation.lower() in dOperation.keys():
            lCmd = dOperation[sOperation].split(";")
            for each_cmd in lCmd :
                self.oLlkStc.llk_stc_send_command(each_cmd)
            # apply
            self.oLlkStc.llk_stc_send_command('stc::apply')   
        else:
            llk_log_end_proc()
            raise AssertionError("The operation is not supported:%s" % operation)

        llk_log_end_proc()


    def hlk_traffic_config_igmp_host(self, **kwargs):
        """
        Return:
            N/A

        """
        llk_log_begin_proc()
        assert (kwargs.keys()), "No arguments given as input for hlk_traffic_config_igmp_host"
        dKwargs = dict(map(lambda (k, v): (str(k).lower(), v), kwargs.iteritems()))
        ##print ('dKwargs:', dKwargs)
        assert ('device' in dKwargs.keys()), "Name of the created device should be given!!"
        assert ('hostconfig' in dKwargs.keys()), "Name of the created device should be given!!"

        oItf = None

        sDevice = str(dKwargs.get('device'))
        ##print ('sDevice:', sDevice)

        if sDevice:
            del dKwargs['device']
        else:
            llk_log_end_proc()
            raise Exception("The input value given for the device %s is invalid" % str(sDevice))

        if sDevice not in self.dDevices.keys():
            llk_log_end_proc()
            raise Exception("The device %s is not created." % str(sDevice))

        if 'hostconfig' in dKwargs.keys():
            dHostConfigParsed = {}
            dHostConfigParsed = self._hlk_traffic_parse_server_param(sServerConfigType='hostconfig', **dKwargs)
            #print ('dHostConfigParsed1:', dHostConfigParsed)

            if 'igmphost' in self.dDevices[sDevice].keys():
                self.oLlkStc.llk_stc_config_device_igmp_host_itf(oDevice=self.dDevices[sDevice]['oDevice'],
                                                             oIGMPHostIf=self.dDevices[sDevice]['igmphost'],
                                                                 **dHostConfigParsed)
                logger.info("IGMP host has been successfully configured.")
            else:
                oItf = self.oLlkStc.llk_stc_config_device_igmp_host_itf(oDevice=self.dDevices[sDevice]['oDevice'],
                                                                        **dHostConfigParsed)
                if oItf:
                    self.dDevices[sDevice]['igmphost'] = oItf
                    logger.info("IGMP host has been successfully created.")
                else:
                    llk_log_end_proc()
                    raise Exception("IGMP host could not be configured")

        """
        if 'groupmembership' in dKwargs.keys():
            dGroupMembershipParsed = {}

            if 'igmphost' not in self.dDevices[sDevice].keys():
                raise Exception("Host is not created. Create it before configuring Group Membership")

            dGroupMembershipParsed = self._hlk_traffic_parse_server_param(
            sServerConfigType='groupmembership', **dKwargs)
            ##print ('dGroupMembershipParsed:', dGroupMembershipParsed)
            if 'digmpgroupmembership' in self.dDevices[sDevice].keys():
                self.oLlkStc.llk_stc_config_igmp_groupmembership(
                    oIgmpHostConfig=self.dDevices[sDevice]['igmphost'],
                    oIgmpGroupMembership=self.dDevices[sDevice]['digmpgroupmembership'],
                    **dGroupMembershipParsed)
                logger.info("IGMP GroupMembership has been successfully configured.")
            else:
                oItf = self.oLlkStc.llk_stc_config_igmp_groupmembership(
                    oIgmpHostConfig=self.dDevices[sDevice]['igmphost'], **dGroupMembershipParsed)
                if oItf:
                    self.dDevices[sDevice]['digmpgroupmembership'] = oItf
                    logger.info("IGMP GroupMembership has been successfully created")
                else:
                    llk_log_end_proc()
                    raise Exception("IGMP GroupMembership could not be configured")
        """


        if 'groupmembership' in dKwargs.keys():
            dGroupMembershipParsed = {}

            if 'igmphost' not in self.dDevices[sDevice].keys():
                raise Exception("Host is not created. Create it before configuring Group Membership")

            dGroupMembershipParsed = self._hlk_traffic_parse_server_param(
            sServerConfigType='groupmembership', **dKwargs)
            ##print ('dGroupMembershipParsed:', dGroupMembershipParsed)
            if 'digmpgroupmembership' in self.dDevices[sDevice].keys():
                self.oLlkStc.llk_stc_config_igmp_groupmembership(
                    oIgmpHostConfig=self.dDevices[sDevice]['igmphost'],
                    oIgmpGroupMembership=self.dDevices[sDevice]['digmpgroupmembership'],
                    **dGroupMembershipParsed)
                logger.info("IGMP GroupMembership has been successfully configured.")
            else:
                oItf = self.oLlkStc.llk_stc_config_igmp_groupmembership(
                    oIgmpHostConfig=self.dDevices[sDevice]['igmphost'], **dGroupMembershipParsed)
                if oItf:
                    self.dDevices[sDevice]['digmpgroupmembership'] = oItf
                    logger.info("IGMP GroupMembership has been successfully created")
                else:
                    llk_log_end_proc()
                    raise Exception("IGMP GroupMembership could not be configured")


        if 'multicastsource' in dKwargs.keys():
            dMulticastSourceParsed = {}

            if 'igmphost' not in self.dDevices[sDevice].keys():
                raise Exception("Host is not created. Create it before configuring Multicast Source")

            dMulticastSourceParsed = self._hlk_traffic_parse_server_param(
            sServerConfigType='multicastsource', **dKwargs)
            #print ('dMulticastSourceParsed:', dMulticastSourceParsed)
            if 'kipv4networkblock' in self.dDevices[sDevice].keys():
                self.oLlkStc.llk_stc_config_multicast_source(
                    oIgmpGroupMembership=self.dDevices[sDevice]['digmpgroupmembership'],
                    oipv4networkblock=self.dDevices[sDevice]['kipv4networkblock'],                  
                    **dMulticastSourceParsed)
                logger.info("Multicast Source has been successfully configured.")
            else:
                oItf = self.oLlkStc.llk_stc_config_multicast_source(
                oIgmpGroupMembership=self.dDevices[sDevice]['digmpgroupmembership'],**dMulticastSourceParsed)
                ##print ('oItf111:', oItf)
                if oItf:
                    self.dDevices[sDevice]['kipv4networkblock'] = oItf
                    logger.info("Multicast Source has been successfully created")
                else:
                    llk_log_end_proc()
                    raise Exception("Multicast Source could not be configured")


        if 'multicastgroups' in dKwargs.keys():
            dipv4groupsParsed = {}

            if 'igmphost' not in self.dDevices[sDevice].keys():
                raise Exception("Host is not created. Create it before configuring Multicast Groups")

            dipv4groupsParsed = self._hlk_traffic_parse_server_param(sServerConfigType='multicastgroups', **dKwargs)
            ##print ('dipv4groupsParsed:', dipv4groupsParsed)
            if 'dipv4networkblock' in self.dDevices[sDevice].keys():
                self.oLlkStc.llk_stc_config_ipv4group(
                     oipv4group=self.dDevices[sDevice]['dipv4networkblock'], **dipv4groupsParsed)
                logger.info("Multicast Groups has been successfully configured.")
            else:
                oItf = self.oLlkStc.llk_stc_config_ipv4group(**dipv4groupsParsed)
                if oItf:
                    self.dDevices[sDevice]['dipv4networkblock'] = oItf
                    logger.info("Multicast Groups has been successfully created")
                else:
                    llk_log_end_proc()
                    raise Exception("Multicast Groups could not be configured")
            

        # set toplevel,primary and uses itf
        if 'igmphost' in self.dDevices[sDevice].keys() and 'ipv4' in self.dDevices[sDevice].keys():
            self.oLlkStc.llk_stc_device_toplevel_itf(self.dDevices[sDevice]['oDevice'], self.dDevices[sDevice]['ipv4'])
            self.oLlkStc.llk_stc_device_primary_itf(self.dDevices[sDevice]['oDevice'], self.dDevices[sDevice]['ipv4'])
            self.oLlkStc.llk_stc_device_uses_itf(self.dDevices[sDevice]['igmphost'], self.dDevices[sDevice]['ipv4'])
            self.oLlkStc.llk_stc_AffiliationPort_itf(self.dDevices[sDevice]['oDevice'], self.dDevices[sDevice]['oPort'])
            self.oLlkStc.llk_stc_SubscribedGroups_itf(sIgmpGroupMembership=self.dDevices[sDevice]['digmpgroupmembership'], sipv4group=self.dDevices[sDevice]['dipv4networkblock'])
        else:
            raise Exception("Both IGMP host and IPv4 interfaces should be configured")

        # stacking device protocols
        self.hlk_traffic_stack_device_protocols(sDevice)
        ##print ('self.dDevices[sDevice]:',self.dDevices[sDevice])
        self.dDeviceType['igmphost'].append(sDevice)


        ##print ('oItf:', oItf)
        return oItf

    def hlk_traffic_operate_igmp_host(self,**kwargs) :

        llk_log_begin_proc()
        assert (kwargs.keys()), "No arguments given as input for hlk_traffic_operate_igmp_host"
        dKwargs = dict(map(lambda (k,v): (str(k).lower(), v), kwargs.iteritems()))
        assert ('device' in dKwargs.keys()), "Name of the igmp host should be given!!"
        assert ('operation' in dKwargs.keys()), "operation should be given!!"

        sDevice    = dKwargs.pop('device')
        sOperation = dKwargs.pop('operation')
        oIgmpHost = self.dDevices[sDevice]['igmphost']
        oDevice     = self.dDevices[sDevice]['oDevice']

        # define related stc commands
        dOperation = {
        'rejoingroups':'stc::perform IgmpMldRejoinGroups -BlockList %s'%(oIgmpHost),\
        'joingroups':'stc::perform IgmpMldJoinGroups -BlockList %s' % (oIgmpHost),\
        'leavegroups':'stc::perform IgmpMldLeaveGroups -BlockList %s' % (oIgmpHost)
        }

        # send stc command
        if sOperation.lower() in dOperation.keys():
            lCmd = dOperation[sOperation].split(";")
            for each_cmd in lCmd :
                self.oLlkStc.llk_stc_send_command(each_cmd)
            # apply
            self.oLlkStc.llk_stc_send_command('stc::apply')   
        else:
            llk_log_end_proc()
            raise AssertionError("The operation is not supported:%s" % (operation))

        llk_log_end_proc()



    def hlk_traffic_config_pppoe_client(self, **kwargs):
        """
        Return:
            N/A

        """
        llk_log_begin_proc()
        assert (kwargs.keys()), "No arguments given as input for hlk_traffic_config_pppoe_client"
        dKwargs = dict(map(lambda (k, v): (str(k).lower(), v), kwargs.iteritems()))
        print ('dKwargs:', dKwargs)
        assert ('device' in dKwargs.keys()), "Name of the created device should be given!!"
        assert ('clientconfig' in dKwargs.keys()), "Name of the created device should be given!!"

        oItf = None

        sDevice = str(dKwargs.get('device'))
        print ('sDevice:', sDevice)

        if sDevice:
            del dKwargs['device']
        else:
            llk_log_end_proc()
            raise Exception("The input value given for the device %s is invalid" % str(sDevice))

        if sDevice not in self.dDevices.keys():
            llk_log_end_proc()
            raise Exception("The device %s is not created." % str(sDevice))

        if 'clientconfig' in dKwargs.keys():
            dClientConfigParsed = {}
            dClientConfigParsed = self._hlk_traffic_parse_server_param(sServerConfigType='clientconfig', **dKwargs)
            print ('dClientConfigParsed1:', dClientConfigParsed)

            if 'pppoeclient' in self.dDevices[sDevice].keys():
                self.oLlkStc.llk_stc_config_device_pppoe_client_itf(oDevice=self.dDevices[sDevice]['oDevice'],
                                                             oPppoeClientIf=self.dDevices[sDevice]['pppoeclient'],
                                                                 **dClientConfigParsed)
                logger.info("PPPOE client has been successfully configured.")
            else:
                oItf = self.oLlkStc.llk_stc_config_device_pppoe_client_itf(oDevice=self.dDevices[sDevice]['oDevice'],
                                                                        **dClientConfigParsed)
                if oItf:
                    self.dDevices[sDevice]['pppoeclient'] = oItf
                    logger.info("PPPOE client has been successfully created.")
                else:
                    llk_log_end_proc()
                    raise Exception("PPPOE client could not be configured")


        if 'emulationmode' in dKwargs.keys():
            EmulationModeParsed = {}

            if 'pppoeclient' not in self.dDevices[sDevice].keys():
                raise Exception("Device is not created. Create it before configuring Ipv4NetworkBlock")

            EmulationModeParsed = self._hlk_traffic_parse_server_param(sServerConfigType='emulationmode', **dKwargs)
            print ('EmulationModeParsed:', EmulationModeParsed)
            if 'dpppoxportconfig' in self.dDevices[sDevice].keys():
                self.oLlkStc.llk_stc_config_pppoe_pppoxportcfgclient(
                    oPortClient=self.dDevices[sDevice]['oPort'],
                    oPppoxPortConfig=self.dDevices[sDevice]['dpppoxportconfig'],
                    **EmulationModeParsed )
                logger.info("Pppox Port has been successfully configured.")
            else:
                oItf = self.oLlkStc.llk_stc_config_pppoe_pppoxportcfgclient(
                    oPortClient=self.dDevices[sDevice]['oPort'], **EmulationModeParsed)
                if oItf:
                    self.dDevices[sDevice]['dpppoxportconfig'] = oItf
                    logger.info("Pppox Port has been successfully created")
                else:
                    llk_log_end_proc()
                    raise Exception("Pppox Port could not be configured")

        #print ('self.dDevices[sDevice]:',self.dDevices[sDevice])

        # set toplevel,primary , uses itf and stacking device protocols
        if 'pppoeclient' in self.dDevices[sDevice].keys() and 'ipv4' in self.dDevices[sDevice].keys():
            self.oLlkStc.llk_stc_device_toplevel_itf(self.dDevices[sDevice]['oDevice'], self.dDevices[sDevice]['ipv4'])
            self.oLlkStc.llk_stc_device_primary_itf(self.dDevices[sDevice]['oDevice'], self.dDevices[sDevice]['ipv4'])
            self.oLlkStc.llk_stc_AffiliationPort_itf(self.dDevices[sDevice]['oDevice'], self.dDevices[sDevice]['oPort'])
            self.oLlkStc.llk_stc_device_uses_itf(self.dDevices[sDevice]['pppoeclient'], self.dDevices[sDevice]['ipv4'])
            self.oLlkStc.llk_stc_device_uses_itf(self.dDevices[sDevice]['pppoeclient'], self.dDevices[sDevice]['pppoe'])

            self.oLlkStc.llk_stc_device_stackedon_itf(self.dDevices[sDevice]['ipv4'], self.dDevices[sDevice]['ppp'])
            self.oLlkStc.llk_stc_device_stackedon_itf(self.dDevices[sDevice]['ppp'], self.dDevices[sDevice]['pppoe'])
            #self.oLlkStc.llk_stc_device_stackedon_itf(self.dDevices[sDevice]['pppoe'], self.dDevices[sDevice]['ethii'])
            
        else:
            raise Exception("Both pppoe client and IPv4 interfaces should be configured")

        # stacking device protocols
        self.hlk_traffic_stack_pppoe_protocols(sDevice)
        print ('self.dDevices[sDevice]:',self.dDevices[sDevice])
        self.dDeviceType['pppoeclient'].append(sDevice)


        print ('pppoe_client_oItf:', oItf)
        return oItf


    def hlk_traffic_config_pppoe_server(self, **kwargs):
        """
        Return:
            N/A

        """
        llk_log_begin_proc()
        assert (kwargs.keys()), "No arguments given as input for hlk_traffic_config_pppoe_server"
        dKwargs = dict(map(lambda (k, v): (str(k).lower(), v), kwargs.iteritems()))
        print ('dKwargs:', dKwargs)
        assert ('device' in dKwargs.keys()), "Name of the created device should be given!!"
        assert ('serverconfig' in dKwargs.keys()), "Name of the created device should be given!!"

        oItf = None

        sDevice = str(dKwargs.get('device'))
        print ('sDevice:', sDevice)

        if sDevice:
            del dKwargs['device']
        else:
            llk_log_end_proc()
            raise Exception("The input value given for the device %s is invalid" % str(sDevice))

        if sDevice not in self.dDevices.keys():
            llk_log_end_proc()
            raise Exception("The device %s is not created." % str(sDevice))

        if 'serverconfig' in dKwargs.keys():
            dServerConfigParsed = {}
            dServerConfigParsed = self._hlk_traffic_parse_server_param(sServerConfigType='serverconfig', **dKwargs)
            print ('dServerConfigParsed1:', dServerConfigParsed)

            if 'pppoeserver' in self.dDevices[sDevice].keys():
                self.oLlkStc.llk_stc_config_device_pppoe_server_itf(oDevice=self.dDevices[sDevice]['oDevice'],
                                                             oPppoeServerIf=self.dDevices[sDevice]['pppoeserver'],
                                                                 **dServerConfigParsed)
                logger.info("PPPOE server has been successfully configured.")
            else:
                oItf = self.oLlkStc.llk_stc_config_device_pppoe_server_itf(oDevice=self.dDevices[sDevice]['oDevice'],
                                                                        **dServerConfigParsed)
                if oItf:
                    self.dDevices[sDevice]['pppoeserver'] = oItf
                    logger.info("PPPOE server has been successfully created.")
                else:
                    llk_log_end_proc()
                    raise Exception("PPPOE server could not be configured")


        if 'emulationmode' in dKwargs.keys():
            EmulationModeParsed = {}

            if 'pppoeserver' not in self.dDevices[sDevice].keys():
                raise Exception("Device is not created. Create it before configuring emulation type")

            EmulationModeParsed = self._hlk_traffic_parse_server_param(sServerConfigType='emulationmode', **dKwargs)
            print ('EmulationModeParsed:', EmulationModeParsed)
            if 'dpppoxportconfig' in self.dDevices[sDevice].keys():
                self.oLlkStc.llk_stc_config_pppoe_pppoxportcfgclient(
                    oPortClient=self.dDevices[sDevice]['oPort'],
                    oPppoxPortConfig=self.dDevices[sDevice]['dpppoxportconfig'],
                    **EmulationModeParsed )
                logger.info("Pppox Port has been successfully configured.")
            else:
                oItf = self.oLlkStc.llk_stc_config_pppoe_pppoxportcfgclient(
                    oPortClient=self.dDevices[sDevice]['oPort'], **EmulationModeParsed)
                if oItf:
                    self.dDevices[sDevice]['dpppoxportconfig'] = oItf
                    logger.info("Pppox Port has been successfully created")
                else:
                    llk_log_end_proc()
                    raise Exception("Pppox Port could not be configured")


        if 'pppoeserverpool' in dKwargs.keys():
            dPppoeServerPoolParsed = {}

            if 'pppoeserver' not in self.dDevices[sDevice].keys():
                raise Exception("Device is not created. Create it before configuring pppoe server pool")

            dPppoeServerPoolParsed  = self._hlk_traffic_parse_server_param(
            sServerConfigType='pppoeserverpool', **dKwargs)
            print ('dPppoeServerPoolParsed:', dPppoeServerPoolParsed)
            if 'dpppoeserveripv4peerpool' in self.dDevices[sDevice].keys():
                self.oLlkStc.llk_stc_config_pppoe_serverpool(
                    oPppoeServerBlockConfig=self.dDevices[sDevice]['pppoeserver'],
                    oPppoeServerIpv4PeerPool=self.dDevices[sDevice]['dpppoeserveripv4peerpool'],
                    **dPppoeServerPoolParsed)
                logger.info("PPPOE server pool has been successfully configured.")
            else:
                oItf = self.oLlkStc.llk_stc_config_pppoe_serverpool(
                    oPppoeServerBlockConfig=self.dDevices[sDevice]['pppoeserver'], **dPppoeServerPoolParsed)
                if oItf:
                    self.dDevices[sDevice]['dpppoeserveripv4peerpool'] = oItf
                    logger.info("PPPOE server pool has been successfully created")
                else:
                    llk_log_end_proc()
                    raise Exception("PPPOE server pool could not be configured")


        print ('self.dDevices[sDevice]_server:',self.dDevices[sDevice])


        # set toplevel,primary , uses itf and stacking device protocols
        if 'pppoeserver' in self.dDevices[sDevice].keys() and 'ipv4' in self.dDevices[sDevice].keys():
            self.oLlkStc.llk_stc_device_toplevel_itf(self.dDevices[sDevice]['oDevice'], self.dDevices[sDevice]['ipv4'])
            self.oLlkStc.llk_stc_device_primary_itf(self.dDevices[sDevice]['oDevice'], self.dDevices[sDevice]['ipv4'])
            self.oLlkStc.llk_stc_device_uses_itf(self.dDevices[sDevice]['pppoeserver'], self.dDevices[sDevice]['ipv4'])
            self.oLlkStc.llk_stc_device_uses_itf(self.dDevices[sDevice]['pppoeserver'], self.dDevices[sDevice]['pppoe'])
            self.oLlkStc.llk_stc_AffiliationPort_itf(self.dDevices[sDevice]['oDevice'], self.dDevices[sDevice]['oPort'])
            self.oLlkStc.llk_stc_device_stackedon_itf(self.dDevices[sDevice]['ipv4'], self.dDevices[sDevice]['ppp'])
            self.oLlkStc.llk_stc_device_stackedon_itf(self.dDevices[sDevice]['ppp'], self.dDevices[sDevice]['pppoe'])
            #self.oLlkStc.llk_stc_device_stackedon_itf(self.dDevices[sDevice]['pppoe'], self.dDevices[sDevice]['ethii'])
            
        else:
            raise Exception("Both pppoe server and IPv4 interfaces should be configured")

        # stacking device protocols
        self.hlk_traffic_stack_pppoe_protocols(sDevice)
        print ('self.dDevices[sDevice]:',self.dDevices[sDevice])
        self.dDeviceType['pppoeserver'].append(sDevice)


        print ('pppoe_server_oItf:', oItf)
        return oItf


    def hlk_traffic_operate_pppoe_server(self,**kwargs) :

        llk_log_begin_proc()
        assert (kwargs.keys()), "No arguments given as input for hlk_traffic_operate_pppoe_server"
        dKwargs = dict(map(lambda (k,v): (str(k).lower(), v), kwargs.iteritems()))
        assert ('device' in dKwargs.keys()), "Name of the pppoe server should be given!!"
        assert ('operation' in dKwargs.keys()), "operation should be given!!"

        sDevice    = dKwargs.pop('device')
        sOperation = dKwargs.pop('operation')
        oPppoeServer = self.dDevices[sDevice]['pppoeserver']
        oDevice     = self.dDevices[sDevice]['oDevice']

        # define related stc commands
        dOperation = {
        'pauseserver':'stc::perform PppoxPause -BlockList %s'%(oPppoeServer),\
        'connectserver':'stc::perform PppoxConnect -BlockList %s'%(oPppoeServer),\
        'disconnectserver':'stc::perform PppoxDisconnect -BlockList %s'%(oPppoeServer),\
        'resumeserver':'stc::perform PppoxResume -BlockList %s'%(oPppoeServer),\
        'retryserver':'stc::perform PppoxRetry -BlockList %s'%(oPppoeServer),\
        'abortserver':'stc::perform PppoxAbort -BlockList %s'%(oPppoeServer)
        }

        # send stc command
        if sOperation.lower() in dOperation.keys():
            lCmd = dOperation[sOperation].split(";")
            for each_cmd in lCmd :
                self.oLlkStc.llk_stc_send_command(each_cmd)
            # apply
            self.oLlkStc.llk_stc_send_command('stc::apply')   
        else:
            llk_log_end_proc()
            raise AssertionError("The operation is not supported:%s" % (operation))

        llk_log_end_proc()


    def hlk_traffic_operate_pppoe_client(self,**kwargs) :

        llk_log_begin_proc()
        assert (kwargs.keys()), "No arguments given as input for hlk_traffic_operate_pppoe_client"
        dKwargs = dict(map(lambda (k,v): (str(k).lower(), v), kwargs.iteritems()))
        assert ('device' in dKwargs.keys()), "Name of the pppoe client should be given!!"
        assert ('operation' in dKwargs.keys()), "operation should be given!!"

        sDevice    = dKwargs.pop('device')
        sOperation = dKwargs.pop('operation')
        oPppoeClient = self.dDevices[sDevice]['pppoeclient']
        oDevice     = self.dDevices[sDevice]['oDevice']

        # define related stc commands
        dOperation = {
        'pauseclient':'stc::perform PppoxPause -BlockList %s'%(oPppoeClient),\
        'connectclient':'stc::perform PppoxConnect -BlockList %s'%(oPppoeClient),\
        'disconnectclient':'stc::perform PppoxDisconnect -BlockList %s'%(oPppoeClient),\
        'resumeclient':'stc::perform PppoxResume -BlockList %s'%(oPppoeClient),\
        'retryclient':'stc::perform PppoxRetry -BlockList %s'%(oPppoeClient),\
        'abortclient':'stc::perform PppoxAbort -BlockList %s'%(oPppoeClient)
        }

        # send stc command
        if sOperation.lower() in dOperation.keys():
            lCmd = dOperation[sOperation].split(";")
            for each_cmd in lCmd :
                self.oLlkStc.llk_stc_send_command(each_cmd)
            # apply
            self.oLlkStc.llk_stc_send_command('stc::apply')   
        else:
            llk_log_end_proc()
            raise AssertionError("The operation is not supported:%s" % (operation))

        llk_log_end_proc()


    def hlk_traffic_config_dhcpv6_server(self, **kwargs):
        """
        Return:
            N/A

        """
        llk_log_begin_proc()
        assert (kwargs.keys()), "No arguments given as input for hlk_traffic_config_dhcpv6_server"
        dKwargs = dict(map(lambda (k, v): (str(k).lower(), v), kwargs.iteritems()))
        #print ('dKwargs:', dKwargs)
        assert ('device' in dKwargs.keys()), "Name of the created device should be given!!"
        assert ('serverconfig' in dKwargs.keys()), "Name of the created device should be given!!"

        oItf = None

        sDevice = str(dKwargs.get('device'))
        #print ('sDevice:', sDevice)

        if sDevice:
            del dKwargs['device']
        else:
            llk_log_end_proc()
            raise Exception("The input value given for the device %s is invalid" % str(sDevice))

        if sDevice not in self.dDevices.keys():
            llk_log_end_proc()
            raise Exception("The device %s is not created." % str(sDevice))

        if 'serverconfig' in dKwargs.keys():
            dServerConfigParsed = {}
            dServerConfigParsed = self._hlk_traffic_parse_server_param(sServerConfigType='serverconfig', **dKwargs)
            #print ('dServerConfigParsed1:', dServerConfigParsed)

            if 'dhcpv6server' in self.dDevices[sDevice].keys():
                self.oLlkStc.llk_stc_config_device_dhcpv6_server_itf(oDevice=self.dDevices[sDevice]['oDevice'],
                                                             oDhcpv6ServerIf=self.dDevices[sDevice]['dhcpv6server'],
                                                                 **dServerConfigParsed)
                logger.info("Dhcpv6 server has been successfully configured.")
            else:
                oItf = self.oLlkStc.llk_stc_config_device_dhcpv6_server_itf(oDevice=self.dDevices[sDevice]['oDevice'],
                                                                        **dServerConfigParsed)
                if oItf:
                    self.dDevices[sDevice]['dhcpv6server'] = oItf
                    logger.info("Dhcpv6 server has been successfully created.")
                else:
                    llk_log_end_proc()
                    raise Exception("Dhcpv6 server could not be configured")       

        if 'serverdefaultprefixpool' in dKwargs.keys():
            dserverdefaultprefixpoolparsed = {}

            if 'dhcpv6server' not in self.dDevices[sDevice].keys():
                raise Exception("Device is not created. Create it before configuring dhcpv6 server default prefix pool")

            dserverdefaultprefixpoolparsed = self._hlk_traffic_parse_server_param(
            sServerConfigType='serverdefaultprefixpool', **dKwargs)
            #print ('dserverdefaultprefixpoolparsed:', dserverdefaultprefixpoolparsed)
            if 'kdhcpv6defaultprefixpool' in self.dDevices[sDevice].keys():
                self.oLlkStc.llk_stc_config_dhcpv6_defaultprefixpool(
                    oDhcpv6ServerConfig=self.dDevices[sDevice]['dhcpv6server'],
                    oDhcpv6DefaultPrefixPool=self.dDevices[sDevice]['kdhcpv6defaultprefixpool'],
                    **dserverdefaultprefixpoolparsed)
                logger.info("dhcpv6 server default prefix pool has been successfully configured.")
            else:
                oItf = self.oLlkStc.llk_stc_config_dhcpv6_defaultprefixpool(
                    oDhcpv6ServerConfig=self.dDevices[sDevice]['dhcpv6server'], **dserverdefaultprefixpoolparsed)
                if oItf:
                    self.dDevices[sDevice]['kdhcpv6defaultprefixpool'] = oItf
                    logger.info("dhcpv6 server default prefix pool has been successfully created")
                else:
                    llk_log_end_proc()
                    raise Exception("dhcpv6 server default prefix pool could not be configured")


        if 'serverdefaultaddrpool' in dKwargs.keys():
            dserverdefaultaddrpoolparsed = {}

            if 'dhcpv6server' not in self.dDevices[sDevice].keys():
                raise Exception("Device is not created. Create it before configuring dhcpv6 server default address pool")

            dserverdefaultaddrpoolparsed = self._hlk_traffic_parse_server_param(
            sServerConfigType='serverdefaultaddrpool', **dKwargs)
            #print ('dserverdefaultaddrpoolparsed:', dserverdefaultaddrpoolparsed)
            if 'kdhcpv6defaultaddrpool' in self.dDevices[sDevice].keys():
                self.oLlkStc.llk_stc_config_dhcpv6_defaultaddrpool(
                    oDhcpv6ServerConfig=self.dDevices[sDevice]['dhcpv6server'],
                    oDhcpv6DefaultAddrPool=self.dDevices[sDevice]['kdhcpv6defaultaddrpool'],
                    **dserverdefaultaddrpoolparsed)
                logger.info("dhcpv6 server default address pool has been successfully configured.")
            else:
                oItf = self.oLlkStc.llk_stc_config_dhcpv6_defaultaddrpool(
                    oDhcpv6ServerConfig=self.dDevices[sDevice]['dhcpv6server'], **dserverdefaultaddrpoolparsed)
                if oItf:
                    self.dDevices[sDevice]['kdhcpv6defaultaddrpool'] = oItf
                    logger.info("dhcpv6 server default address pool has been successfully created")
                else:
                    llk_log_end_proc()
                    raise Exception("dhcpv6 server default address pool could not be configured")



        #print ('self.dDevices[sDevice]:',self.dDevices[sDevice])

        # set toplevel,primary , uses itf and stacking device protocols
        if 'dhcpv6server' in self.dDevices[sDevice].keys() and 'ipv6' in self.dDevices[sDevice].keys():
            self.oLlkStc.llk_stc_device_toplevel_itf(self.dDevices[sDevice]['oDevice'], self.dDevices[sDevice]['ipv6'])
            #self.oLlkStc.llk_stc_device_toplevel_itf(self.dDevices[sDevice]['oDevice'], self.dDevices[sDevice]['ipv6ll'])
            self.oLlkStc.llk_stc_device_primary_itf(self.dDevices[sDevice]['oDevice'], self.dDevices[sDevice]['ipv6ll'])
            self.oLlkStc.llk_stc_device_uses_itf(self.dDevices[sDevice]['dhcpv6server'], self.dDevices[sDevice]['ipv6'])
            self.oLlkStc.llk_stc_AffiliationPort_itf(self.dDevices[sDevice]['oDevice'], self.dDevices[sDevice]['oPort'])

        else:
            raise Exception("Both dhcpv6 server and IPv6 interfaces should be configured")

        # stacking device protocols
        self.hlk_traffic_stack_dhcpv6_protocols(sDevice)
        #print ('self.dDevices[sDevice]:',self.dDevices[sDevice])
        self.dDeviceType['dhcpv6server'].append(sDevice)
        #print ('dhcpv6_server_oItf:', oItf)
        return oItf

    def hlk_traffic_operate_dhcpv6_server(self,**kwargs) :

        llk_log_begin_proc()
        assert (kwargs.keys()), "No arguments given as input for hlk_traffic_operate_dhcpv6_server"
        dKwargs = dict(map(lambda (k,v): (str(k).lower(), v), kwargs.iteritems()))
        assert ('device' in dKwargs.keys()), "Name of the dhcpv6 server should be given!!"
        assert ('operation' in dKwargs.keys()), "operation should be given!!"

        sDevice    = dKwargs.pop('device')
        sOperation = dKwargs.pop('operation')
        oDhcpv6Server = self.dDevices[sDevice]['dhcpv6server']
        oDevice     = self.dDevices[sDevice]['oDevice']

        # define related stc commands
        dOperation = {
        'startserver':'stc::perform Dhcpv6StartServer -ServerList %s'%(oDhcpv6Server),\
        'stopserver':'stc::perform Dhcpv6StopServer -ServerList %s'%(oDhcpv6Server),\
        'rebindserver':'stc::perform Dhcpv6ServerReconfigureRebind -ServerList %s'%(oDhcpv6Server),\
        'renewserver':'stc::perform Dhcpv6ServerReconfigureRenew -ServerList %s'%(oDhcpv6Server),\
        'abortserver':'stc::perform Dhcpv6AbortServer -ServerList %s'%(oDhcpv6Server)
        }

        # send stc command
        if sOperation.lower() in dOperation.keys():
            lCmd = dOperation[sOperation].split(";")
            for each_cmd in lCmd :
                self.oLlkStc.llk_stc_send_command(each_cmd)
            # apply
            self.oLlkStc.llk_stc_send_command('stc::apply')   
        else:
            llk_log_end_proc()
            raise AssertionError("The operation is not supported:%s" % (operation))

        llk_log_end_proc()


    def hlk_traffic_config_dhcpv6_client(self, **kwargs):
        """
        Return:
            N/A
        """
        llk_log_begin_proc()
        assert (kwargs.keys()), "No arguments given as input for hlk_traffic_config_dhcpv6_client"
        dKwargs = dict(map(lambda (k, v): (str(k).lower(), v), kwargs.iteritems()))
        #print ('dKwargs:', dKwargs)
        assert ('device' in dKwargs.keys()), "Name of the created device should be given!!"
        assert ('clientconfig' in dKwargs.keys()), "Name of the created device should be given!!"

        oItf = None

        sDevice = str(dKwargs.get('device'))
        #print ('sDevice:', sDevice)

        if sDevice:
            del dKwargs['device']
        else:
            llk_log_end_proc()
            raise Exception("The input value given for the device %s is invalid" % str(sDevice))

        if sDevice not in self.dDevices.keys():
            llk_log_end_proc()
            raise Exception("The device %s is not created." % str(sDevice))

        if 'clientconfig' in dKwargs.keys():
            dClientConfigParsed = {}
            dClientConfigParsed = self._hlk_traffic_parse_server_param(sServerConfigType='clientconfig', **dKwargs)
            #print ('dclientConfigParsed1:', dClientConfigParsed)

            if 'dhcpv6client' in self.dDevices[sDevice].keys():
                self.oLlkStc.llk_stc_config_device_dhcpv6_client_itf(oDevice=self.dDevices[sDevice]['oDevice'],
                                                             oDhcpv6ClientIf=self.dDevices[sDevice]['dhcpv6client'],
                                                                 **dClientConfigParsed)
                logger.info("Dhcpv6 client has been successfully configured.")
            else:
                oItf = self.oLlkStc.llk_stc_config_device_dhcpv6_client_itf(oDevice=self.dDevices[sDevice]['oDevice'],
                                                                        **dClientConfigParsed)
                if oItf:
                    self.dDevices[sDevice]['dhcpv6client'] = oItf
                    logger.info("Dhcpv6 client has been successfully created.")
                else:
                    llk_log_end_proc()
                    raise Exception("Dhcpv6 client could not be configured")       
        
        #print ('self.dDevices[sDevice]:',self.dDevices[sDevice])

        # set toplevel,primary , uses itf and stacking device protocols
        if 'dhcpv6client' in self.dDevices[sDevice].keys() and 'ipv6' in self.dDevices[sDevice].keys():
            self.oLlkStc.llk_stc_device_toplevel_itf(self.dDevices[sDevice]['oDevice'], self.dDevices[sDevice]['ipv6'])
            #self.oLlkStc.llk_stc_device_toplevel_itf(self.dDevices[sDevice]['oDevice'], self.dDevices[sDevice]['ipv6ll'])
            self.oLlkStc.llk_stc_device_primary_itf(self.dDevices[sDevice]['oDevice'], self.dDevices[sDevice]['ipv6ll'])
            self.oLlkStc.llk_stc_device_uses_itf(self.dDevices[sDevice]['dhcpv6client'], self.dDevices[sDevice]['ipv6'])
            self.oLlkStc.llk_stc_AffiliationPort_itf(self.dDevices[sDevice]['oDevice'], self.dDevices[sDevice]['oPort'])

        else:
            raise Exception("Both dhcpv6 client and IPv6 interfaces should be configured")

        # stacking device protocols
        self.hlk_traffic_stack_dhcpv6_protocols(sDevice)
        #print ('self.dDevices[sDevice]:',self.dDevices[sDevice])
        self.dDeviceType['dhcpv6client'].append(sDevice)

        #print ('dhcpv6_client_oItf:', oItf)
        return oItf

    def hlk_traffic_operate_dhcpv6_client(self,**kwargs) :

        llk_log_begin_proc()
        assert (kwargs.keys()), "No arguments given as input for hlk_traffic_operate_dhcpv6_client"
        dKwargs = dict(map(lambda (k,v): (str(k).lower(), v), kwargs.iteritems()))
        assert ('device' in dKwargs.keys()), "Name of the dhcpv6 client should be given!!"
        assert ('operation' in dKwargs.keys()), "operation should be given!!"

        sDevice    = dKwargs.pop('device')
        sOperation = dKwargs.pop('operation')
        oDhcpv6Client = self.dDevices[sDevice]['dhcpv6client']
        oDevice     = self.dDevices[sDevice]['oDevice']

        # define related stc commands
        dOperation = {
        'bindclient':'stc::perform Dhcpv6Bind -BlockList %s'%(oDhcpv6Client),\
        'rebindclient':'stc::perform Dhcpv6Rebind -BlockList %s'%(oDhcpv6Client),\
        'renewclient':'stc::perform Dhcpv6Renew -BlockList %s'%(oDhcpv6Client),\
        'releaseclient':'stc::perform Dhcpv6Release -BlockList %s'%(oDhcpv6Client),\
        'confirmsessions':'stc::perform Dhcpv6Confirm -BlockList %s'%(oDhcpv6Client),\
        'abortclient':'stc::perform Dhcpv6Abort -BlockList %s'%(oDhcpv6Client),\
        'inforequest':'stc::perform Dhcpv6InfoRequest -BlockList %s'%(oDhcpv6Client)
         
        }

        # send stc command
        if sOperation.lower() in dOperation.keys():
            lCmd = dOperation[sOperation].split(";")
            for each_cmd in lCmd :
                self.oLlkStc.llk_stc_send_command(each_cmd)
            # apply
            self.oLlkStc.llk_stc_send_command('stc::apply')   
        else:
            llk_log_end_proc()
            raise AssertionError("The operation is not supported:%s" % (operation))

        llk_log_end_proc()

    def hlk_traffic_config_8021x_auth(self, **kwargs):
        """
        Return:
            N/A

        """
        llk_log_begin_proc()
        assert (kwargs.keys()), "No arguments given as input for hlk_traffic_config_8021x_auth"
        dKwargs = dict(map(lambda (k, v): (str(k).lower(), v), kwargs.iteritems()))
        #print ('dKwargs:', dKwargs)
        assert ('device' in dKwargs.keys()), "Name of the created device should be given!!"
        assert ('8021xconfig' in dKwargs.keys()), "Name of the created device should be given!!"

        oItf = None

        sDevice = str(dKwargs.get('device'))
        #print ('sDevice:', sDevice)

        if sDevice:
            del dKwargs['device']
        else:
            llk_log_end_proc()
            raise Exception("The input value given for the device %s is invalid" % str(sDevice))

        if sDevice not in self.dDevices.keys():
            llk_log_end_proc()
            raise Exception("The device %s is not created." % str(sDevice))

        if '8021xconfig' in dKwargs.keys():
            dClientConfigParsed = {}
            dClientConfigParsed = self._hlk_traffic_parse_server_param(sServerConfigType='8021xconfig', **dKwargs)
            #print ('dClientConfigParsed1:', dClientConfigParsed)

            if '8021xclient' in self.dDevices[sDevice].keys():
                self.oLlkStc.llk_stc_config_device_8021x_auth_itf(oDevice=self.dDevices[sDevice]['oDevice'],
                                                             o8021xClientIf=self.dDevices[sDevice]['8021xclient'],
                                                                 **dClientConfigParsed)
                logger.info("802.1x authentication has been successfully configured.")
            else:
                oItf = self.oLlkStc.llk_stc_config_device_8021x_auth_itf(oDevice=self.dDevices[sDevice]['oDevice'],
                                                                        **dClientConfigParsed)
                if oItf:
                    self.dDevices[sDevice]['8021xclient'] = oItf
                    logger.info("8021x client has been successfully created.")
                else:
                    llk_log_end_proc()
                    raise Exception("8021x client could not be configured")


        if 'md5' in dKwargs.keys():
            Md5Parsed = {}

            if '8021xclient' not in self.dDevices[sDevice].keys():
                raise Exception("Device is not created. Create it before configuring MD5 username and password")

            Md5Parsed = self._hlk_traffic_parse_server_param(sServerConfigType='md5', **dKwargs)
            #print ('Md5Parsed:', Md5Parsed)
            if 'kdot1xeapmd5config' in self.dDevices[sDevice].keys():
                self.oLlkStc.llk_stc_config_8021x_md5config(
                    o8021xBlockConfig=self.dDevices[sDevice]['8021xclient'],
                    oMd5Config=self.dDevices[sDevice]['kdot1xeapmd5config'],
                    **Md5Parsed )
                logger.info("Md5 config has been successfully configured.")
            else:
                oItf = self.oLlkStc.llk_stc_config_8021x_md5config(
                    o8021xBlockConfig=self.dDevices[sDevice]['8021xclient'], **Md5Parsed)
                if oItf:
                    self.dDevices[sDevice]['kdot1xeapmd5config'] = oItf
                    logger.info("Md5 config has been successfully created")
                else:
                    llk_log_end_proc()
                    raise Exception("Md5 config could not be configured")

        #print ('self.dDevices[sDevice]:',self.dDevices[sDevice])

        # set toplevel,primary , uses itf and stacking device protocols
        if '8021xclient' in self.dDevices[sDevice].keys() and 'ipv4' in self.dDevices[sDevice].keys():
            self.oLlkStc.llk_stc_device_toplevel_itf(self.dDevices[sDevice]['oDevice'], self.dDevices[sDevice]['ipv4'])
            self.oLlkStc.llk_stc_device_primary_itf(self.dDevices[sDevice]['oDevice'], self.dDevices[sDevice]['ipv4'])
            self.oLlkStc.llk_stc_AffiliationPort_itf(self.dDevices[sDevice]['oDevice'], self.dDevices[sDevice]['oPort'])
            self.oLlkStc.llk_stc_device_uses_itf(self.dDevices[sDevice]['8021xclient'], self.dDevices[sDevice]['ethii'])
            
        else:
            raise Exception("Both 802.1x authentication and IPv4 interfaces should be configured")

        # stacking device protocols
        self.hlk_traffic_stack_device_protocols(sDevice)
        #print ('self.dDevices[sDevice]:',self.dDevices[sDevice])
        self.dDeviceType['8021xclient'].append(sDevice)

        #print ('8021x_auth_oItf:', oItf)
        return oItf


    def hlk_traffic_operate_8021x_auth(self,**kwargs) :

        llk_log_begin_proc()
        assert (kwargs.keys()), "No arguments given as input for hlk_traffic_operate_8021x_auth"
        dKwargs = dict(map(lambda (k,v): (str(k).lower(), v), kwargs.iteritems()))
        assert ('device' in dKwargs.keys()), "Name of the 802.1x authentication should be given!!"
        assert ('operation' in dKwargs.keys()), "operation should be given!!"

        sDevice    = dKwargs.pop('device')
        sOperation = dKwargs.pop('operation')
        o8021xClient = self.dDevices[sDevice]['8021xclient']
        oDevice     = self.dDevices[sDevice]['oDevice']

        # define related stc commands
        dOperation = {
        'startauth':'stc::perform Dot1xStartAuth -ObjectList %s'%(o8021xClient),\
        'logoutsession':'stc::perform Dot1xLogout -ObjectList %s'%(o8021xClient),\
        'abortauth':'stc::perform Dot1xAbortAuth -ObjectList %s'%(o8021xClient)
        }

        # send stc command
        if sOperation.lower() in dOperation.keys():
            lCmd = dOperation[sOperation].split(";")
            for each_cmd in lCmd :
                self.oLlkStc.llk_stc_send_command(each_cmd)
            # apply
            self.oLlkStc.llk_stc_send_command('stc::apply')   
        else:
            llk_log_end_proc()
            raise AssertionError("The operation is not supported:%s" % (operation))

        llk_log_end_proc()


    def hlk_traffic_config_ospfv3_router(self, **kwargs):
        llk_log_begin_proc()
        assert (kwargs.keys()), "No arguments given as input for hlk_traffic_config_ospfv3_router"
        dKwargs = dict(map(lambda (k, v): (str(k).lower(), v), kwargs.iteritems()))
        assert ('routerconfig' in dKwargs.keys()), "Name of the created device should be given!!"
        
        oItf = None

        sDevice = str(dKwargs.get('device'))

        if sDevice:
            del dKwargs['device']
        else:
            llk_log_end_proc()
            raise Exception("The input value given for the device %s is invalid" % str(sDevice))

        if sDevice not in self.dDevices.keys():
            llk_log_end_proc()
            raise Exception("The device %s is not created." % str(sDevice))


        if 'routerconfig' in dKwargs.keys():
            dRouterConfigParsed = {}
            dRouterConfigParsed = self._hlk_traffic_parse_server_param(sServerConfigType='routerconfig', **dKwargs)

            if 'ospfv3router' in self.dDevices[sDevice].keys():
                self.oLlkStc.llk_stc_config_device_ospfv3_router_itf(oDevice=self.dDevices[sDevice]['oDevice'],
                                                             kOspfv3RouterIf=self.dDevices[sDevice]['ospfv3router'],
                                                                 **dRouterConfigParsed)
                logger.info("ospfv3 router has been successfully configured.")
            else:
                oItf = self.oLlkStc.llk_stc_config_device_ospfv3_router_itf(oDevice=self.dDevices[sDevice]['oDevice'],
                                                                        **dRouterConfigParsed)
                if oItf:
                    self.dDevices[sDevice]['ospfv3router'] = oItf
                    logger.info("ospfv3 router has been successfully created.")
                else:
                    llk_log_end_proc()
                    raise Exception("ospfv3 router could not be configured")
        
        ExtRouteCountParsed = {}

        if 'ospfv3router' not in self.dDevices[sDevice].keys():
            raise Exception("Device is not created. Create it before configuring External LSA")

        if 'kexternallsablock' in self.dDevices[sDevice].keys():
            self.oLlkStc.llk_stc_config_ospfv3_externallsa(
                oOspfv3RouterConfig=self.dDevices[sDevice]['ospfv3router'],
                oExternalLsa=self.dDevices[sDevice]['kexternallsablock'],**ExtRouteCountParsed )
            logger.info("External LSA has been successfully configured.")
        else:
            oItf = self.oLlkStc.llk_stc_config_ospfv3_externallsa(
                oOspfv3RouterConfig=self.dDevices[sDevice]['ospfv3router'], **ExtRouteCountParsed)
            if oItf:
                self.dDevices[sDevice]['kexternallsablock'] = oItf
                logger.info("External LSA has been successfully created")
            else:
                llk_log_end_proc()
                raise Exception("External LSA could not be configured")
         
        
        if 'extrouteaddr' in dKwargs.keys():
            ExtRouteAddrParsed = {}

            if 'ospfv3router' not in self.dDevices[sDevice].keys():
                raise Exception("Device is not created. Create it before configuring external route address")
    
            ExtRouteAddrParsed = self._hlk_traffic_parse_server_param(sServerConfigType='extrouteaddr', **dKwargs)
            #print ('ExtRouteAddrParsed:', ExtRouteAddrParsed)
            if 'kipv6networkblock' in self.dDevices[sDevice].keys():
                self.oLlkStc.llk_stc_config_ospfv3_extrouteaddr(
                    oExternalLsaBlock=self.dDevices[sDevice]['kexternallsablock'],
                    oIpv6NetworkBlock=self.dDevices[sDevice]['kipv6networkblock'],
                    **ExtRouteAddrParsed )
                logger.info("external route address has been successfully configured.")
            else:
                oItf = self.oLlkStc.llk_stc_config_ospfv3_extrouteaddr(
                    oExternalLsaBlock=self.dDevices[sDevice]['kexternallsablock'], **ExtRouteAddrParsed)
                if oItf:
                    self.dDevices[sDevice]['kipv6networkblock'] = oItf
                    logger.info("external route address has been successfully created")
                else:
                    llk_log_end_proc()
                    raise Exception("external route address could not be configured")


        # set toplevel,primary , uses itf
        if 'ospfv3router' in self.dDevices[sDevice].keys() and 'ipv6' in self.dDevices[sDevice].keys():
            self.oLlkStc.llk_stc_device_toplevel_itf(self.dDevices[sDevice]['oDevice'], '"%s %s"' % (self.dDevices[sDevice]['ipv6'],self.dDevices[sDevice]['ipv6ll']))
            self.oLlkStc.llk_stc_device_primary_itf(self.dDevices[sDevice]['oDevice'], self.dDevices[sDevice]['ipv6'])
            self.oLlkStc.llk_stc_AffiliationPort_itf(self.dDevices[sDevice]['oDevice'], self.dDevices[sDevice]['oPort'])
            self.oLlkStc.llk_stc_device_uses_itf(self.dDevices[sDevice]['ospfv3router'], self.dDevices[sDevice]['ipv6'])
        else:
            raise Exception("Both ospfv3 router and IPv6 interfaces should be configured")

        # stacking device protocols
        self.hlk_traffic_stack_dhcpv6_protocols(sDevice)
        self.dDeviceType['ospfv3router'].append(sDevice)
        llk_log_end_proc()
        return oItf

    
    def hlk_traffic_config_ospfv2_router(self, **kwargs):
        """
        Return:
            N/A

        """
        llk_log_begin_proc()
        assert (kwargs.keys()), "No arguments given as input for hlk_traffic_config_ospfv2_router"
        dKwargs = dict(map(lambda (k, v): (str(k).lower(), v), kwargs.iteritems()))
        #print ('dKwargs:', dKwargs)
        assert ('device' in dKwargs.keys()), "Name of the created device should be given!!"
        assert ('routerconfig' in dKwargs.keys()), "Name of the created device should be given!!"

        oItf = None

        sDevice = str(dKwargs.get('device'))
        #print ('sDevice:', sDevice)

        if sDevice:
            del dKwargs['device']
        else:
            llk_log_end_proc()
            raise Exception("The input value given for the device %s is invalid" % str(sDevice))

        if sDevice not in self.dDevices.keys():
            llk_log_end_proc()
            raise Exception("The device %s is not created." % str(sDevice))

        if 'routerconfig' in dKwargs.keys():
            dRouterConfigParsed = {}
            dRouterConfigParsed = self._hlk_traffic_parse_server_param(sServerConfigType='routerconfig', **dKwargs)
            #print ('dRouterConfigParsed1:', dRouterConfigParsed)

            if 'ospfv2router' in self.dDevices[sDevice].keys():
                self.oLlkStc.llk_stc_config_device_ospfv2_router_itf(oDevice=self.dDevices[sDevice]['oDevice'],
                                                             kOspfv2RouterIf=self.dDevices[sDevice]['ospfv2router'],
                                                                 **dRouterConfigParsed)
                logger.info("ospfv2 router has been successfully configured.")
            else:
                oItf = self.oLlkStc.llk_stc_config_device_ospfv2_router_itf(oDevice=self.dDevices[sDevice]['oDevice'],
                                                                        **dRouterConfigParsed)
                if oItf:
                    self.dDevices[sDevice]['ospfv2router'] = oItf
                    logger.info("ospfv2 router has been successfully created.")
                else:
                    llk_log_end_proc()
                    raise Exception("ospfv2 router could not be configured")


        if 'extroutecount' in dKwargs.keys():
            ExtRouteCountParsed = {}

            if 'ospfv2router' not in self.dDevices[sDevice].keys():
                raise Exception("Device is not created. Create it before configuring External LSA")

            ExtRouteCountParsed = self._hlk_traffic_parse_server_param(sServerConfigType='extroutecount', **dKwargs)
            #print ('ExtRouteCountParsed:', ExtRouteCountParsed)
            if 'kexternallsablock' in self.dDevices[sDevice].keys():
                self.oLlkStc.llk_stc_config_ospfv2_externallsa(
                    oOspfv2RouterConfig=self.dDevices[sDevice]['ospfv2router'],
                    oExternalLsa=self.dDevices[sDevice]['kexternallsablock'],
                    **ExtRouteCountParsed )
                logger.info("External LSA has been successfully configured.")
            else:
                oItf = self.oLlkStc.llk_stc_config_ospfv2_externallsa(
                    oOspfv2RouterConfig=self.dDevices[sDevice]['ospfv2router'], **ExtRouteCountParsed)
                if oItf:
                    self.dDevices[sDevice]['kexternallsablock'] = oItf
                    logger.info("External LSA has been successfully created")
                else:
                    llk_log_end_proc()
                    raise Exception("External LSA could not be configured")



        if 'extrouteaddr' in dKwargs.keys():
            ExtRouteAddrParsed = {}

            if 'ospfv2router' not in self.dDevices[sDevice].keys():
                raise Exception("Device is not created. Create it before configuring external route address")

            ExtRouteAddrParsed = self._hlk_traffic_parse_server_param(sServerConfigType='extrouteaddr', **dKwargs)
            #print ('ExtRouteAddrParsed:', ExtRouteAddrParsed)
            if 'kipv4networkblock' in self.dDevices[sDevice].keys():
                self.oLlkStc.llk_stc_config_ospfv2_extrouteaddr(
                    oExternalLsaBlock=self.dDevices[sDevice]['kexternallsablock'],
                    oIpv4NetworkBlock=self.dDevices[sDevice]['kipv4networkblock'],
                    **ExtRouteAddrParsed )
                logger.info("external route address has been successfully configured.")
            else:
                oItf = self.oLlkStc.llk_stc_config_ospfv2_extrouteaddr(
                    oExternalLsaBlock=self.dDevices[sDevice]['kexternallsablock'], **ExtRouteAddrParsed)
                if oItf:
                    self.dDevices[sDevice]['kipv4networkblock'] = oItf
                    logger.info("external route address has been successfully created")
                else:
                    llk_log_end_proc()
                    raise Exception("external route address could not be configured")    


        #print ('self.dDevices[sDevice]:',self.dDevices[sDevice])

        # set toplevel,primary , uses itf 
        if 'ospfv2router' in self.dDevices[sDevice].keys() and 'ipv4' in self.dDevices[sDevice].keys():
            self.oLlkStc.llk_stc_device_toplevel_itf(self.dDevices[sDevice]['oDevice'], self.dDevices[sDevice]['ipv4'])
            self.oLlkStc.llk_stc_device_primary_itf(self.dDevices[sDevice]['oDevice'], self.dDevices[sDevice]['ipv4'])
            self.oLlkStc.llk_stc_AffiliationPort_itf(self.dDevices[sDevice]['oDevice'], self.dDevices[sDevice]['oPort'])
            self.oLlkStc.llk_stc_device_uses_itf(self.dDevices[sDevice]['ospfv2router'], self.dDevices[sDevice]['ipv4'])
            
        else:
            raise Exception("Both ospfv2 router and IPv4 interfaces should be configured")

        # stacking device protocols
        self.hlk_traffic_stack_device_protocols(sDevice)
        #print ('self.dDevices[sDevice]:',self.dDevices[sDevice])
        self.dDeviceType['ospfv2router'].append(sDevice)

        #print ('ospfv2router_oItf:', oItf)
        return oItf

    def hlk_traffic_operate_ospfv3_router(self,**kwargs) :

        llk_log_begin_proc()
        assert (kwargs.keys()), "No arguments given as input for hlk_traffic_operate_ospfv3_router"
        dKwargs = dict(map(lambda (k,v): (str(k).lower(), v), kwargs.iteritems()))
        assert ('device' in dKwargs.keys()), "Name of the ospfv3 router should be given!!"
        assert ('operation' in dKwargs.keys()), "operation should be given!!"

        sDevice    = dKwargs.pop('device')
        sOperation = dKwargs.pop('operation')
        oOspfv2Router = self.dDevices[sDevice]['ospfv3router']
        oDevice     = self.dDevices[sDevice]['oDevice']

        # define related stc commands
        dOperation = {
        'startospfv3':'stc::perform ProtocolStart -ProtocolList %s'%(oOspfv3Router),\
        'stopospfv3':'stc::perform ProtocolStop -ProtocolList %s'%(oOspfv3Router)
        }

        # send stc command
        if sOperation.lower() in dOperation.keys():
            lCmd = dOperation[sOperation].split(";")
            for each_cmd in lCmd :
                self.oLlkStc.llk_stc_send_command(each_cmd)
            # apply
            self.oLlkStc.llk_stc_send_command('stc::apply')
        else:
            llk_log_end_proc()
            raise AssertionError("The operation is not supported:%s" % (operation))

        llk_log_end_proc()

    def hlk_traffic_operate_ospfv2_router(self,**kwargs) :

        llk_log_begin_proc()
        assert (kwargs.keys()), "No arguments given as input for hlk_traffic_operate_ospfv2_router"
        dKwargs = dict(map(lambda (k,v): (str(k).lower(), v), kwargs.iteritems()))
        assert ('device' in dKwargs.keys()), "Name of the ospfv2 router should be given!!"
        assert ('operation' in dKwargs.keys()), "operation should be given!!"

        sDevice    = dKwargs.pop('device')
        sOperation = dKwargs.pop('operation')
        oOspfv2Router = self.dDevices[sDevice]['ospfv2router']
        oDevice     = self.dDevices[sDevice]['oDevice']

        # define related stc commands
        dOperation = {
        'startospfv2':'stc::perform ProtocolStart -ProtocolList %s'%(oOspfv2Router),\
        'stopospfv2':'stc::perform ProtocolStop -ProtocolList %s'%(oOspfv2Router)        
        }

        # send stc command
        if sOperation.lower() in dOperation.keys():
            lCmd = dOperation[sOperation].split(";")
            for each_cmd in lCmd :
                self.oLlkStc.llk_stc_send_command(each_cmd)
            # apply
            self.oLlkStc.llk_stc_send_command('stc::apply')   
        else:
            llk_log_end_proc()
            raise AssertionError("The operation is not supported:%s" % (operation))

        llk_log_end_proc()


    def hlk_traffic_config_rip_router(self, **kwargs):
        """
        Return:
            N/A

        """
        llk_log_begin_proc()
        assert (kwargs.keys()), "No arguments given as input for hlk_traffic_config_rip_router"
        dKwargs = dict(map(lambda (k, v): (str(k).lower(), v), kwargs.iteritems()))
        #print ('dKwargs:', dKwargs)
        assert ('device' in dKwargs.keys()), "Name of the created device should be given!!"
        assert ('routerconfig' in dKwargs.keys()), "Name of the created device should be given!!"

        oItf = None

        sDevice = str(dKwargs.get('device'))
        #print ('sDevice:', sDevice)

        if sDevice:
            del dKwargs['device']
        else:
            llk_log_end_proc()
            raise Exception("The input value given for the device %s is invalid" % str(sDevice))

        if sDevice not in self.dDevices.keys():
            llk_log_end_proc()
            raise Exception("The device %s is not created." % str(sDevice))

        if 'routerconfig' in dKwargs.keys():
            dRouterConfigParsed = {}
            dRouterConfigParsed = self._hlk_traffic_parse_server_param(sServerConfigType='routerconfig', **dKwargs)
            #print ('dRouterConfigParsed1:', dRouterConfigParsed)

            if 'riprouter' in self.dDevices[sDevice].keys():
                self.oLlkStc.llk_stc_config_device_rip_router_itf(oDevice=self.dDevices[sDevice]['oDevice'],
                                                             kRipRouterIf=self.dDevices[sDevice]['riprouter'],
                                                                 **dRouterConfigParsed)
                logger.info("rip router has been successfully configured.")
            else:
                oItf = self.oLlkStc.llk_stc_config_device_rip_router_itf(oDevice=self.dDevices[sDevice]['oDevice'],
                                                                        **dRouterConfigParsed)
                if oItf:
                    self.dDevices[sDevice]['riprouter'] = oItf
                    logger.info("rip router has been successfully created.")
                else:
                    llk_log_end_proc()
                    raise Exception("rip router could not be configured")


        if 'riprouteparams' in dKwargs.keys():
            RipRouteParamsParsed = {}

            if 'riprouter' not in self.dDevices[sDevice].keys():
                raise Exception("Device is not created. Create it before configuring ripv4 route params")

            RipRouteParamsParsed = self._hlk_traffic_parse_server_param(sServerConfigType='riprouteparams', **dKwargs)
            #print ('RipRouteParamsParsed:', RipRouteParamsParsed)
            if 'kripv4routeparams' in self.dDevices[sDevice].keys():
                self.oLlkStc.llk_stc_config_rip_routeparams(
                    oRipRouterConfig=self.dDevices[sDevice]['riprouter'],
                    oRipv4RouteParams=self.dDevices[sDevice]['kripv4routeparams'],
                    **RipRouteParamsParsed )
                logger.info("Rip route params has been successfully configured.")
            else:
                oItf = self.oLlkStc.llk_stc_config_rip_routeparams(
                    oRipRouterConfig=self.dDevices[sDevice]['riprouter'], **RipRouteParamsParsed)
                if oItf:
                    self.dDevices[sDevice]['kripv4routeparams'] = oItf
                    logger.info("Rip route params has been successfully created")
                else:
                    llk_log_end_proc()
                    raise Exception("Rip route params has could not be configured")



        if 'riprouteaddr' in dKwargs.keys():
            RipRouteAddrParsed = {}

            if 'riprouter' not in self.dDevices[sDevice].keys():
                raise Exception("Device is not created. Create it before configuring rip route address")

            RipRouteAddrParsed = self._hlk_traffic_parse_server_param(sServerConfigType='riprouteaddr', **dKwargs)
            #print ('RipRouteAddrParsed:', RipRouteAddrParsed)
            if 'kipv4networkblock' in self.dDevices[sDevice].keys():
                self.oLlkStc.llk_stc_config_rip_routeaddr(
                    oRipv4RouteParams=self.dDevices[sDevice]['kripv4routeparams'],
                    oIpv4NetworkBlock=self.dDevices[sDevice]['kipv4networkblock'],
                    **RipRouteAddrParsed )
                logger.info("Rip route address has been successfully configured.")
            else:
                oItf = self.oLlkStc.llk_stc_config_rip_routeaddr(
                    oRipv4RouteParams=self.dDevices[sDevice]['kripv4routeparams'], **RipRouteAddrParsed)
                if oItf:
                    self.dDevices[sDevice]['kipv4networkblock'] = oItf
                    logger.info("Rip route address has been successfully created")
                else:
                    llk_log_end_proc()
                    raise Exception("Rip route address could not be configured")    


        #print ('self.dDevices[sDevice]:',self.dDevices[sDevice])

        # set toplevel,primary , uses itf 
        if 'riprouter' in self.dDevices[sDevice].keys() and 'ipv4' in self.dDevices[sDevice].keys():
            self.oLlkStc.llk_stc_device_toplevel_itf(self.dDevices[sDevice]['oDevice'], self.dDevices[sDevice]['ipv4'])
            self.oLlkStc.llk_stc_device_primary_itf(self.dDevices[sDevice]['oDevice'], self.dDevices[sDevice]['ipv4'])
            self.oLlkStc.llk_stc_AffiliationPort_itf(self.dDevices[sDevice]['oDevice'], self.dDevices[sDevice]['oPort'])
            self.oLlkStc.llk_stc_device_uses_itf(self.dDevices[sDevice]['riprouter'], self.dDevices[sDevice]['ipv4'])
            
        else:
            raise Exception("Both rip router and IPv4 interfaces should be configured")

        # stacking device protocols
        self.hlk_traffic_stack_device_protocols(sDevice)
        #print ('self.dDevices[sDevice]:',self.dDevices[sDevice])
        self.dDeviceType['riprouter'].append(sDevice)

        #print ('riprouteroItf:', oItf)
        return oItf


    def hlk_traffic_operate_rip_router(self,**kwargs) :

        llk_log_begin_proc()
        assert (kwargs.keys()), "No arguments given as input for hlk_traffic_operate_rip_router"
        dKwargs = dict(map(lambda (k,v): (str(k).lower(), v), kwargs.iteritems()))
        assert ('device' in dKwargs.keys()), "Name of the rip router should be given!!"
        assert ('operation' in dKwargs.keys()), "operation should be given!!"

        sDevice    = dKwargs.pop('device')
        sOperation = dKwargs.pop('operation')
        oRipRouter = self.dDevices[sDevice]['riprouter']
        oDevice     = self.dDevices[sDevice]['oDevice']

        # define related stc commands
        dOperation = {
        'startrip':'stc::perform ProtocolStart -ProtocolList %s'%(oRipRouter),\
        'stoprip':'stc::perform ProtocolStop -ProtocolList %s'%(oRipRouter)
        
        }

        # send stc command
        if sOperation.lower() in dOperation.keys():
            lCmd = dOperation[sOperation].split(";")
            for each_cmd in lCmd :
                self.oLlkStc.llk_stc_send_command(each_cmd)
            # apply
            self.oLlkStc.llk_stc_send_command('stc::apply')   
        else:
            llk_log_end_proc()
            raise AssertionError("The operation is not supported:%s" % (operation))

        llk_log_end_proc()


    def hlk_traffic_config_bgp_router(self, **kwargs):
        """
        Return:
            N/A

        """
        llk_log_begin_proc()
        assert (kwargs.keys()), "No arguments given as input for hlk_traffic_config_bgp_router"
        dKwargs = dict(map(lambda (k, v): (str(k).lower(), v), kwargs.iteritems()))
        #print ('dKwargs:', dKwargs)
        assert ('device' in dKwargs.keys()), "Name of the created device should be given!!"
        assert ('routerconfig' in dKwargs.keys()), "Name of the created device should be given!!"

        oItf = None

        sDevice = str(dKwargs.get('device'))
        #print ('sDevice:', sDevice)

        if sDevice:
            del dKwargs['device']
        else:
            llk_log_end_proc()
            raise Exception("The input value given for the device %s is invalid" % str(sDevice))

        if sDevice not in self.dDevices.keys():
            llk_log_end_proc()
            raise Exception("The device %s is not created." % str(sDevice))

        if 'routerconfig' in dKwargs.keys():
            dRouterConfigParsed = {}
            dRouterConfigParsed = self._hlk_traffic_parse_server_param(sServerConfigType='routerconfig', **dKwargs)
            #print ('dRouterConfigParsed1:', dRouterConfigParsed)

            if 'bgprouter' in self.dDevices[sDevice].keys():
                self.oLlkStc.llk_stc_config_device_bgp_router_itf(oDevice=self.dDevices[sDevice]['oDevice'],
                                                             kBgpRouterIf=self.dDevices[sDevice]['bgprouter'],
                                                                 **dRouterConfigParsed)
                logger.info("bgp router has been successfully configured.")
            else:
                oItf = self.oLlkStc.llk_stc_config_device_bgp_router_itf(oDevice=self.dDevices[sDevice]['oDevice'],
                                                                        **dRouterConfigParsed)
                if oItf:
                    self.dDevices[sDevice]['bgprouter'] = oItf
                    logger.info("bgp router has been successfully created.")
                else:
                    llk_log_end_proc()
                    raise Exception("bgp router could not be configured")


        if 'bgprouteparams' in dKwargs.keys():
            BgpRouteParamsParsed = {}

            if 'bgprouter' not in self.dDevices[sDevice].keys():
                raise Exception("Device is not created. Create it before configuring ripv4 route params")

            BgpRouteParamsParsed = self._hlk_traffic_parse_server_param(sServerConfigType='bgprouteparams', **dKwargs)
            #print ('BgpRouteParamsParsed:', BgpRouteParamsParsed)
            if 'kbgpv4routeparams' in self.dDevices[sDevice].keys():
                self.oLlkStc.llk_stc_config_bgp_routeparams(
                    oBgpRouterConfig=self.dDevices[sDevice]['bgprouter'],
                    oBgpIpv4RouteConfig=self.dDevices[sDevice]['kbgpv4routeparams'],
                    **BgpRouteParamsParsed )
                logger.info("bgp route params has been successfully configured.")
            else:
                oItf = self.oLlkStc.llk_stc_config_bgp_routeparams(
                    oBgpRouterConfig=self.dDevices[sDevice]['bgprouter'], **BgpRouteParamsParsed)
                if oItf:
                    self.dDevices[sDevice]['kbgpv4routeparams'] = oItf
                    logger.info("bgp route params has been successfully created")
                else:
                    llk_log_end_proc()
                    raise Exception("bgp route params has could not be configured")



        if 'bgprouteaddr' in dKwargs.keys():
            BgpRouteAddrParsed = {}

            if 'bgprouter' not in self.dDevices[sDevice].keys():
                raise Exception("Device is not created. Create it before configuring bgp route address")

            BgpRouteAddrParsed = self._hlk_traffic_parse_server_param(sServerConfigType='bgprouteaddr', **dKwargs)
            #print ('BgpRouteAddrParsed:', BgpRouteAddrParsed)
            if 'kipv4networkblock' in self.dDevices[sDevice].keys():
                self.oLlkStc.llk_stc_config_bgp_routeaddr(
                    oBgpIpv4RouteConfig=self.dDevices[sDevice]['kbgpv4routeparams'],
                    oIpv4NetworkBlock=self.dDevices[sDevice]['kipv4networkblock'],
                    **BgpRouteAddrParsed )
                logger.info("bgp route address has been successfully configured.")
            else:
                oItf = self.oLlkStc.llk_stc_config_bgp_routeaddr(
                    oBgpIpv4RouteConfig=self.dDevices[sDevice]['kbgpv4routeparams'], **BgpRouteAddrParsed)
                if oItf:
                    self.dDevices[sDevice]['kipv4networkblock'] = oItf
                    logger.info("bgp route address has been successfully created")
                else:
                    llk_log_end_proc()
                    raise Exception("bgp route address could not be configured")    


        #print ('self.dDevices[sDevice]:',self.dDevices[sDevice])

        # set toplevel,primary , uses itf 
        if 'bgprouter' in self.dDevices[sDevice].keys() and 'ipv4' in self.dDevices[sDevice].keys():
            self.oLlkStc.llk_stc_device_toplevel_itf(self.dDevices[sDevice]['oDevice'], self.dDevices[sDevice]['ipv4'])
            self.oLlkStc.llk_stc_device_primary_itf(self.dDevices[sDevice]['oDevice'], self.dDevices[sDevice]['ipv4'])
            self.oLlkStc.llk_stc_AffiliationPort_itf(self.dDevices[sDevice]['oDevice'], self.dDevices[sDevice]['oPort'])
            self.oLlkStc.llk_stc_device_uses_itf(self.dDevices[sDevice]['bgprouter'], self.dDevices[sDevice]['ipv4'])
            
        else:
            raise Exception("Both bgp router and IPv4 interfaces should be configured")

        # stacking device protocols
        self.hlk_traffic_stack_device_protocols(sDevice)
        #print ('self.dDevices[sDevice]:',self.dDevices[sDevice])
        self.dDeviceType['bgprouter'].append(sDevice)

        #print ('bgprouteroItf:', oItf)
        return oItf


    def hlk_traffic_operate_bgp_router(self,**kwargs) :

        llk_log_begin_proc()
        assert (kwargs.keys()), "No arguments given as input for hlk_traffic_operate_bgp_router"
        dKwargs = dict(map(lambda (k,v): (str(k).lower(), v), kwargs.iteritems()))
        assert ('device' in dKwargs.keys()), "Name of the bgp router should be given!!"
        assert ('operation' in dKwargs.keys()), "operation should be given!!"

        sDevice    = dKwargs.pop('device')
        sOperation = dKwargs.pop('operation')
        oBgpRouter = self.dDevices[sDevice]['bgprouter']
        oDevice     = self.dDevices[sDevice]['oDevice']

        # define related stc commands
        dOperation = {
        'startbgp':'stc::perform ProtocolStart -ProtocolList %s'%(oBgpRouter),\
        'stopbgp':'stc::perform ProtocolStop -ProtocolList %s'%(oBgpRouter)
        
        }

        # send stc command
        if sOperation.lower() in dOperation.keys():
            lCmd = dOperation[sOperation].split(";")
            for each_cmd in lCmd :
                self.oLlkStc.llk_stc_send_command(each_cmd)
            # apply
            self.oLlkStc.llk_stc_send_command('stc::apply')   
        else:
            llk_log_end_proc()
            raise AssertionError("The operation is not supported:%s" % (operation))

        llk_log_end_proc()

    def hlk_traffic_config_synce_device(self, **kwargs):
        """
        Return:
            N/A

        """
        llk_log_begin_proc()
        assert (kwargs.keys()), "No arguments given as input for hlk_traffic_config_synce_device"
        dKwargs = dict(map(lambda (k, v): (str(k).lower(), v), kwargs.iteritems()))
        #print ('dKwargs:', dKwargs)
        assert ('device' in dKwargs.keys()), "Name of the created device should be given!!"
        assert ('synceconfig' in dKwargs.keys()), "Name of the created device should be given!!"

        oItf = None

        sDevice = str(dKwargs.get('device'))
        #print ('sDevice:', sDevice)

        if sDevice:
            del dKwargs['device']
        else:
            llk_log_end_proc()
            raise Exception("The input value given for the device %s is invalid" % str(sDevice))

        if sDevice not in self.dDevices.keys():
            llk_log_end_proc()
            raise Exception("The device %s is not created." % str(sDevice))

        if 'synceconfig' in dKwargs.keys():
            dClientConfigParsed = {}
            dClientConfigParsed = self._hlk_traffic_parse_server_param(sServerConfigType='synceconfig', **dKwargs)
            #print ('dClientConfigParsed1:', dClientConfigParsed)

            if 'syncedevice' in self.dDevices[sDevice].keys():
                self.oLlkStc.llk_stc_config_device_synce_itf(oDevice=self.dDevices[sDevice]['oDevice'],
                                                             oSynceDeviceIf=self.dDevices[sDevice]['syncedevice'],
                                                                 **dClientConfigParsed)
                logger.info("SyncEth Device has been successfully configured.")
            else:
                oItf = self.oLlkStc.llk_stc_config_device_synce_itf(oDevice=self.dDevices[sDevice]['oDevice'],
                                                                        **dClientConfigParsed)
                if oItf:
                    self.dDevices[sDevice]['syncedevice'] = oItf
                    logger.info("SyncEth Device has been successfully created.")
                else:
                    llk_log_end_proc()
                    raise Exception("SyncEth Device could not be configured")


        if 'synceoptiontype' in dKwargs.keys():
            Md5Parsed = {}

            if 'syncedevice' not in self.dDevices[sDevice].keys():
                raise Exception("Device is not created. Create it before configuring synce option type")

            Md5Parsed = self._hlk_traffic_parse_server_param(sServerConfigType='synceoptiontype', **dKwargs)
            #print ('Md5Parsed:', Md5Parsed)
            if 'ksyncethportconfig' in self.dDevices[sDevice].keys():
                self.oLlkStc.llk_stc_config_synceth_port(oPort=self.dDevices[sDevice]['oPort'],
                oSyncePortIf=self.dDevices[sDevice]['ksyncethportconfig'],
                **Md5Parsed )
                logger.info("synce option type has been successfully configured.")
            else:
                oItf = self.oLlkStc.llk_stc_config_synceth_port(oPort=self.dDevices[sDevice]['oPort'], **Md5Parsed)
                if oItf:
                    self.dDevices[sDevice]['ksyncethportconfig'] = oItf
                    logger.info("synce option type  has been successfully created")
                else:
                    llk_log_end_proc()
                    raise Exception("synce option type  could not be configured")

        #print ('self.dDevices[sDevice]:',self.dDevices[sDevice])

        # set toplevel,primary , uses itf and stacking device protocols
        if 'syncedevice' in self.dDevices[sDevice].keys() and 'ipv4' in self.dDevices[sDevice].keys():
            self.oLlkStc.llk_stc_device_toplevel_itf(self.dDevices[sDevice]['oDevice'], self.dDevices[sDevice]['ipv4'])
            self.oLlkStc.llk_stc_device_primary_itf(self.dDevices[sDevice]['oDevice'], self.dDevices[sDevice]['ipv4'])
            self.oLlkStc.llk_stc_AffiliationPort_itf(self.dDevices[sDevice]['oDevice'], self.dDevices[sDevice]['oPort'])
            self.oLlkStc.llk_stc_device_uses_itf(self.dDevices[sDevice]['syncedevice'], self.dDevices[sDevice]['ethii'])
            
        else:
            raise Exception("Both synce device and IPv4 interfaces should be configured")

        # stacking device protocols
        self.hlk_traffic_stack_device_protocols(sDevice)
        #print ('self.dDevices[sDevice]:',self.dDevices[sDevice])
        self.dDeviceType['syncedevice'].append(sDevice)
        return oItf

    def hlk_traffic_modify_synce_ql(self,**kwargs):
        llk_log_begin_proc()
        assert (kwargs.keys()), "No arguments given as input for hlk_traffic_modify_synce_ql"
        dKwargs = dict(map(lambda (k,v): (str(k).lower(), v), kwargs.iteritems()))
        assert ('device' in dKwargs.keys()), "Name of the device should be given!!"
        assert ('ql' in dKwargs.keys()), "Quality level should be given!!"
        sDevice    = dKwargs.pop('device')
        sql    = dKwargs.pop('ql')
        oDevice     = self.dDevices[sDevice]['oDevice']
        ksyncedeviceconfig=self.oLlkStc.llk_stc_send_command('stc::get '+oDevice+" -children-SyncEthDeviceConfig")
        oItf=self.oLlkStc.llk_stc_send_command("stc::config "+ksyncedeviceconfig +" -QualityLevel "+sql)
        self.oLlkStc.llk_stc_send_command('stc::apply') 
        return oItf
        llk_log_end_proc()

    def hlk_traffic_config_eoam_node(self, **kwargs):
        """
        Return:
            N/A

        """
        llk_log_begin_proc()
        assert (kwargs.keys()), "No arguments given as input for hlk_traffic_config_eoam_node"
        dKwargs = dict(map(lambda (k, v): (str(k).lower(), v), kwargs.iteritems()))
        ##print ('dKwargs:', dKwargs)
        assert ('device' in dKwargs.keys()), "Name of the created device should be given!!"
        assert ('nodeconfig' in dKwargs.keys()), "Name of the created device should be given!!"

        oItf = None

        sDevice = str(dKwargs.get('device'))
        ##print ('sDevice:', sDevice)

        if sDevice:
            del dKwargs['device']
        else:
            llk_log_end_proc()
            raise Exception("The input value given for the device %s is invalid" % str(sDevice))

        if sDevice not in self.dDevices.keys():
            llk_log_end_proc()
            raise Exception("The device %s is not created." % str(sDevice))

        if 'nodeconfig' in dKwargs.keys():
            dNodeConfigParsed = {}
            dNodeConfigParsed = self._hlk_traffic_parse_server_param(sServerConfigType='nodeconfig', **dKwargs)
            #print ('dNodeConfigParsed1:', dNodeConfigParsed)

            if 'eoamnode' in self.dDevices[sDevice].keys():
                self.oLlkStc.llk_stc_config_device_eoam_node_itf(oDevice=self.dDevices[sDevice]['oDevice'],
                                                             oEoamNodeIf=self.dDevices[sDevice]['eoamnode'],
                                                                 **dNodeConfigParsed)
                logger.info("EOAM node has been successfully configured.")
            else:
                oItf = self.oLlkStc.llk_stc_config_device_eoam_node_itf(oDevice=self.dDevices[sDevice]['oDevice'],
                                                                        **dNodeConfigParsed)
                if oItf:
                    self.dDevices[sDevice]['eoamnode'] = oItf
                    logger.info("EOAM node has been successfully created.")
                else:
                    llk_log_end_proc()
                    raise Exception("EOAM node could not be configured")

        if 'eoammtptcfg' in dKwargs.keys():
            dEoamMPConfigParsed = {}

            if 'eoamnode' not in self.dDevices[sDevice].keys():
                raise Exception("Host is not created. Create it before configuring EoamMaintenancePointConfig")

            dEoamMPConfigParsed = self._hlk_traffic_parse_server_param(
            sServerConfigType='eoammtptcfg', **dKwargs)
            ##print ('dEoamGenConfigParsed:', dEoamGenConfigParsed)
            if 'deoammpconfig' in self.dDevices[sDevice].keys():
                self.oLlkStc.llk_stc_config_eoammpconfig(
                    oEoamMPConfig=self.dDevices[sDevice]['deoammpconfig'],
                    oEoamNodeConfig=self.dDevices[sDevice]['eoamnode'],  
                    **dEoamMPConfigParsed)
                logger.info("EoamMaintenancePointConfig has been successfully configured.")
            else:
                oItf = self.oLlkStc.llk_stc_config_eoammpconfig(oEoamNodeConfig=self.dDevices[sDevice]['eoamnode'],**dEoamMPConfigParsed)
                if oItf:
                    self.dDevices[sDevice]['deoammpconfig'] = oItf
                    logger.info("EoamMaintenancePointConfig has been successfully created")
                else:
                    llk_log_end_proc()
                    raise Exception("EoamMaintenancePointConfig could not be configured")


        if 'eoamgencfg' in dKwargs.keys():
            dEoamGenConfigParsed = {}

            if 'eoamnode' not in self.dDevices[sDevice].keys():
                raise Exception("Host is not created. Create it before configuring EoamGenConfig")

            dEoamGenConfigParsed = self._hlk_traffic_parse_server_param(
            sServerConfigType='eoamgencfg', **dKwargs)
            ##print ('dEoamGenConfigParsed:', dEoamGenConfigParsed)
            if 'deoamgenconfig' in self.dDevices[sDevice].keys():
                self.oLlkStc.llk_stc_config_eoamgenconfig(
                    oEoamGenConfig=self.dDevices[sDevice]['deoamgenconfig'],
                    **dEoamGenConfigParsed)
                logger.info("Eoamgenconfig has been successfully configured.")
            else:
                oItf = self.oLlkStc.llk_stc_config_eoamgenconfig(**dEoamGenConfigParsed)
                if oItf:
                    self.dDevices[sDevice]['deoamgenconfig'] = oItf
                    logger.info("Eeoamgenconfig has been successfully created")
                else:
                    llk_log_end_proc()
                    raise Exception("Eeoamgenconfig could not be configured")

        if 'eoammegcfg' in dKwargs.keys():
            dEoamMegConfigParsed = {}

            if 'eoamnode' not in self.dDevices[sDevice].keys():
                raise Exception("Host is not created. Create it before configuring EoamMegConfig")

            dEoamMegConfigParsed = self._hlk_traffic_parse_server_param(
            sServerConfigType='eoammegcfg', **dKwargs)
            ##print ('dEoamGenConfigParsed:', dEoamGenConfigParsed)
            if 'deoammegconfig' in self.dDevices[sDevice].keys():
                self.oLlkStc.llk_stc_config_eoammegconfig(
                    oEoamMegConfig=self.dDevices[sDevice]['deoammegconfig'],
                    **dEoamMegConfigParsed)
                logger.info("Eoammegconfig has been successfully configured.")
            else:
                oItf = self.oLlkStc.llk_stc_config_eoammegconfig(**dEoamMegConfigParsed)
                if oItf:
                    self.dDevices[sDevice]['deoammegconfig'] = oItf
                    logger.info("Eeoammegconfig has been successfully created")
                else:
                    llk_log_end_proc()
                    raise Exception("Eeoammegconfig could not be configured")


        if 'eoamremotemepcfg' in dKwargs.keys():
            dEoamRemoteMepCfgParsed = {}

            if 'eoamnode' not in self.dDevices[sDevice].keys():
                raise Exception("Device is not created. Create it before configuring EoamRemoteMepCfg")

            dEoamRemoteMepCfgParsed = self._hlk_traffic_parse_server_param(
            sServerConfigType='eoamremotemepcfg', **dKwargs)
            #print ('dEoamRemoteMepCfgParsed:', dEoamRemoteMepCfgParsed)
            if 'keoamremotemepcfg' in self.dDevices[sDevice].keys():
                self.oLlkStc.llk_stc_config_eoam_remotemepcfg(
                    oEoamMegConfig=self.dDevices[sDevice]['deoammegconfig'],
                    oEoamRemoteMepCfg=self.dDevices[sDevice]['keoamremotemepcfg'],                  
                    **dEoamRemoteMepCfgParsed)
                logger.info("EoamRemoteMegEndPointConfig has been successfully configured.")
            else:
                oItf = self.oLlkStc.llk_stc_config_eoam_remotemepcfg(
                oEoamMegConfig=self.dDevices[sDevice]['deoammegconfig'],**dEoamRemoteMepCfgParsed)
                ##print ('oItf111:', oItf)
                if oItf:
                    self.dDevices[sDevice]['keoamremotemepcfg'] = oItf
                    logger.info("EoamRemoteMegEndPointConfig has been successfully created")
                else:
                    llk_log_end_proc()
                    raise Exception("EoamRemoteMegEndPointConfig could not be configured")

        argslist=self.dDevices[sDevice]['vlan']
        print ('vlanlist:', argslist)
        vlanlist = ''.join(argslist)
        # set toplevel,primary and uses itf
        if 'eoamnode' in self.dDevices[sDevice].keys() and 'ipv4' in self.dDevices[sDevice].keys():
            self.oLlkStc.llk_stc_device_toplevel_itf(self.dDevices[sDevice]['oDevice'], self.dDevices[sDevice]['ipv4'])
            self.oLlkStc.llk_stc_device_primary_itf(self.dDevices[sDevice]['oDevice'], self.dDevices[sDevice]['ipv4'])
            self.oLlkStc.llk_stc_device_uses_itf(self.dDevices[sDevice]['eoamnode'], argslist[-1])
            #self.oLlkStc.llk_stc_device_uses_itf(self.dDevices[sDevice]['eoamnode'], self.dDevices[sDevice]['vlan'])
            #self.oLlkStc.llk_stc_AffiliationPort_itf(self.dDevices[sDevice]['oDevice'], self.dDevices[sDevice]['vlan'])
            self.oLlkStc.llk_stc_MegAssociation_itf(sEoamMtPtConfig=self.dDevices[sDevice]['deoammpconfig'], sEoamMegConfig=self.dDevices[sDevice]['deoammegconfig'])
        else:
            raise Exception("Both EOAM node and IPv4 interfaces should be configured")

        # stacking device protocols
        self.hlk_traffic_stack_device_protocols(sDevice)
        ##print ('self.dDevices[sDevice]:',self.dDevices[sDevice])
        self.dDeviceType['eoamnode'].append(sDevice)


        ##print ('oItf:', oItf)
        return oItf


    def hlk_traffic_delete_eoamgenconfig(self):
        """
        Example:
            hlk_traffic_delete_eoamgenconfig()
        """
        llk_log_begin_proc()
        self.oLlkStc.llk_stc_delete_eoamgenconfig()
        llk_log_end_proc()



    def hlk_traffic_operate_device(self,**kwargs) :
        llk_log_begin_proc()
        assert (kwargs.keys()), "No arguments given as input for hlk_traffic_operate_device"
        dKwargs = dict(map(lambda (k,v): (str(k).lower(), v), kwargs.iteritems()))
        assert ('device' in dKwargs.keys()), "Name of the device should be given!!"
        assert ('operation' in dKwargs.keys()), "operation should be given!!"

        sDevice    = dKwargs.pop('device')
        sOperation = dKwargs.pop('operation')
        oDevice     = self.dDevices[sDevice]['oDevice']

        # define related stc commands
        dOperation = {
        'startdevice':'stc::perform DeviceStartCommand -DeviceList %s'%(oDevice),\
        'stopdevice':'stc::perform DeviceStopCommand -DeviceList %s' %(oDevice)
        }

        # send stc command
        if sOperation.lower() in dOperation.keys():
            lCmd = dOperation[sOperation].split(";")
            for each_cmd in lCmd :
                self.oLlkStc.llk_stc_send_command(each_cmd)
            # apply
            self.oLlkStc.llk_stc_send_command('stc::apply')   
        else:
            llk_log_end_proc()
            raise AssertionError("The operation is not supported:%s" % (operation))

        llk_log_end_proc()

    def hlk_traffic_operate_startarpnd(self,**kwargs) :
        llk_log_begin_proc()
        assert (kwargs.keys()), "No arguments given as input for hlk_traffic_operate_startarpnd"
        dKwargs = dict(map(lambda (k,v): (str(k).lower(), v), kwargs.iteritems()))
        #assert ('device' in dKwargs.keys()), "Name of the device should be given!!"
        #assert ('operation' in dKwargs.keys()), "operation should be given!!"
        #sOperation = dKwargs.pop('operation')

        if 'device' in dKwargs.keys():
            assert ('device' in dKwargs.keys()), "Name of the device should be given!!"
            sDevice    = dKwargs.pop('device')
            oDevice     = self.dDevices[sDevice]['oDevice']
            #dOperation = {'startarpnddevice':'stc::perform ArpNdStart -HandleList %s'%(oDevice)}
            self.oLlkStc.llk_stc_send_command("stc::perform ArpNdStart -HandleList "+oDevice)
            self.oLlkStc.llk_stc_send_command('stc::apply') 
            logger.info("All devices attempted Arps/ND resolved successfully.")
        else:
            assert ('port' in dKwargs.keys()), "Name of the device or port should be given!!" 
            sDevice    = dKwargs.pop('port')
            oDevice     = self.dDevices[sDevice]['oDevice']
            oPort=self.oLlkStc.llk_stc_send_command("stc::get " +oDevice + " -affiliationport-Targets") 
            #dOperation = {'startarpndport':'stc::perform ArpNdStart -HandleList %s'%(oPort)}
            self.oLlkStc.llk_stc_send_command("stc::perform ArpNdStart -HandleList "+oPort)
            self.oLlkStc.llk_stc_send_command('stc::apply') 
            logger.info("All ports attempted Arps/ND resolved successfully.")
        llk_log_end_proc()

    def _hlk_traffic_parse_server_param(self,sServerConfigType, **dKwargs):
        """
        Internal method to parse server config parameters.

        Args:
            sServerConfigType : Configuration type for the server
                Type: String
                Default: ""
                Values: serverconfig, serverpoolconfig, serverdefaultpoolconfig.

            dKwargs: Parameters to be configured for server
                Type: Dictionary
                Default: {}
                Values : Depends on sServerConfigType (Same as described in hlk_traffic_config_dhcp_server)

        Return:
            dServerConfigParsed : Dictionary of parameters to be configured with key as parameter name and value as the parameter's value

        Example:
            _hlk_traffic_parse_server_param(sServerConfigType="serverconfig",Active=TRUE,HostName="host1")
        """
        dServerConfigParsed = {}
        if sServerConfigType in dKwargs.keys():
            lServerConfigParam = str(dKwargs[str(sServerConfigType)]).strip("[](){}").split(',')

            for sServerConfigParam in lServerConfigParam:
                lServerConfigParsed = str(sServerConfigParam).strip("[](){}").split('=')
                if lServerConfigParsed:
                    if len(lServerConfigParsed) == 2:
                        dServerConfigParsed[str(lServerConfigParsed[0]).lower()] = str(lServerConfigParsed[1])
                    else:
                        logger.warn("Config parameter format %s is not correct" % str(sServerConfigParam))

        return dServerConfigParsed

    def hlk_traffic_start_devices(self, **kwargs):
        """
        Start the devices.

        Args:
            device (Required): Name of the device
                Default: ''
                Type: String/List

        Return:
            N/A

        Example:
            1)hlk_traffic_start_devices    device=all

            2)hlk_traffic_start_devices    device=[device1,device2]

            3)${lStartDevices} =    Create_List    device1    device2
              hlk_traffic_start_devices    device=${lStartDevices}
        """
        llk_log_begin_proc()

        assert (kwargs.keys()), "No arguments given as input for hlk_traffic_start_devices"
        dKwargs = dict(map(lambda (k,v): (str(k), v), kwargs.iteritems()))
        llk_log_end_proc()
        assert ('device' in dKwargs.keys()), "Device names on which traffic is to be started must be specified"

        self.lStartDevices = []
        if type(dKwargs['device']) is list:
            lDeviceNames = list(map(lambda (k): str(k), dKwargs['device']))
        elif str(dKwargs['device']).lower() == 'all':
            lDeviceNames = self.dDevices.keys()
        else:
            lDeviceNames = str(dKwargs['device']).strip("[](){}").split(',')

        del dKwargs['device']
        if dKwargs:
            logger.warn("The input parameters %s given are being ignored" % ','.join(key) for key in dKwargs.keys())

        if lDeviceNames:
            for sDevice in lDeviceNames:
                if sDevice in self.dDevices.keys():
                    self.lStartDevices.append(self.dDevices[sDevice]['oDevice'])
                else:
                    self.lStartDevices.append(sDevice)
                    logger.info("Device with name " + sDevice + " is appened to start device list!!")
        else:
            raise Exception("No device names are given in the input arguments!!")

        if self.lStartDevices:
            self.oLlkStc.llk_stc_start_device(10,*self.lStartDevices)
        else:
            raise Exception("Devices could not be started as there are no devices found with the given name")

        llk_log_end_proc()

    def hlk_traffic_stop_devices(self, **kwargs):
        """
        Stop the devices.

        Args:
            device (Required): Name of the device
                Default: ''
                Type: String/List

        Return:
            N/A

        Example:
            1)hlk_traffic_stop_devices    device=all

            2)hlk_traffic_stop_devices    device=[device1,device2]

            3)${lStopDevices} =    Create_List    device1    device2
              hlk_traffic_stop_devices    device=${lStopDevices}
        """
        llk_log_begin_proc()

        assert (kwargs.keys()), "No arguments given as input for hlk_traffic_stop_devices"
        dKwargs = dict(map(lambda (k,v): (str(k), v), kwargs.iteritems()))
        llk_log_end_proc()
        assert ('device' in dKwargs.keys()), "Device names on which traffic is to be started must be specified"

        lStopDevicesObjects = []
        if type(dKwargs['device']) is list:
            lStopDevicesNames = list(map(lambda (k): str(k), dKwargs['device']))
        elif str(dKwargs['device']).lower() == 'all':
            if self.lStartDevices:
                self.oLlkStc.llk_stc_stop_device(*self.lStartDevices)
            else:
                raise Exception ("The devices have not been started to be stopped!!")
            del dKwargs['device']
            if dKwargs:
                logger.warn("The input parameters %s given are being ignored" % ','.join(key) for key in dKwargs.keys())
            llk_log_end_proc()
            return
        else:
            lStopDevicesNames = str(dKwargs['device']).strip("[](){}").split(',')

        del dKwargs['device']
        if dKwargs:
            logger.warn("The input parameters %s given are being ignored" % ','.join(key) for key in dKwargs.keys())

        if lStopDevicesNames:
            for sStopDeviceName in lStopDevicesNames:
                if sStopDeviceName in self.dDevices.keys():
                    if self.dDevices[sStopDeviceName] in self.lStartDevices:
                        lStopDevicesObjects.append(self.dDevices[sStopDeviceName])
                    else:
                        logger.warn("Device %s is not started" % sStopDeviceName)
                else:
                    logger.warn("Device " + sStopDeviceName + " is not created!!")
        else:
            raise Exception("No Device names are given in the input arguments!!")

        if lStopDevicesObjects:
            self.oLlkStc.llk_stc_stop_device(*lStopDevicesObjects)
        else:
            raise Exception("No valid devices given as input to stop!!")
        llk_log_end_proc()

    def hlk_traffic_config_bound_streamblock(self,**kwargs):
        """
        Create / Configure a bound streamblock

        Args:
            kwargs:
                name(Required while creating a bound streamblock): Name of the streamblock to be configured/created.
                    Default:'Streamblock'
                    Type: String

                portID(Required while creating a bound streamblock)- port id of the port obtained using hlk_traffic_get_port_id
                    Default: ''
                    Type: String

                device(Required while creating a bound streamblock): Name of the device
                    Type: String
                    Default: ""

                type(Required while configuring a streamblock): Interface type to be configured for the streamblock
                    Type: String
                    Default: ""
                    Values: ipv4, ipv6, tcp, udp

                Depending on the type, the following parameters can be configured:
                    i) ipv4:
                        checksum -
                            Default: 0
                            Type: INTEGER
                        destAddr -
                            Default: 192.0.0.1
                            Type: IPV4ADDR
                        destPrefixLength -
                            Default: 24
                            Type: INTEGER
                        fragOffset -
                            Default: 0
                            Type: INTEGER
                        gateway -
                            Default: 192.85.1.1
                            Type: IPV4ADDR
                        prefixLength -
                            Default: 24
                            Type: INTEGER
                        protocol -
                            Values: 0 (HOPOPT), 1 (ICMP), 2 (IGMP), 3 (GGP), 4 (IP), 5 (ST), 6 (TCP), 7 (CBT), 8 (EGP), 9 (IGP),
                            10 (BBN-RCC-MON), 11 (NVP-II), 12 (PUP), 13 (ARGUS), 14 (EMCON), 15 (XNET), 16 (CHAOS), 17 (UDP),
                            18 (MUX), 19 (DCN-MEAS), 20 (HMP), 21 (PRM), 22 (XNS-IDP), 23 (TRUNK-1), 24 (TRUNK-2), 25 (LEAF-1),
                            26 (LEAF-2), 27 (RDP), 28 (IRTP), 29 (ISO-TP4), 30 (NETBLT), 31 (MFE-NSP), 32 (MERIT-INP), 33 (SEP),
                            34 (3PC), 35 (IDPR), 36 (XTP), 37 (DDP), 38 (IDPR-CMTP), 39 (TP++), 40 (IL), 41 (IPv6), 42 (SDRP),
                            43 (IPv6-Route), 44 (IPv6-Frag), 45 (IDRP), 46 (RSVP), 47 (GRE), 48 (MHRP), 49 (BNA), 50 (ESP),
                            51 (AH), 52 (I-NLSP), 53 (SWIPE), 54 (NARP), 55 (MOBILE), 56 (TLSP), 57 (SKIP), 58 (IPv6-ICMP),
                            59 (IPv6-NoNxt), 60 (IPv6-Opts), 62 (CFTP), 64 (SAT-EXPAK), 65 (KRYPTOLAN), 66 (RVD), 67 (IPPC),
                            69 (SAT-MON), 70 (VISA), 71 (IPCV), 72 (CPNX), 73 (CPHB), 74 (WSN), 75 (PVP), 76 (BR-SAT-MON),
                            77 (SUN-ND), 78 (WB-MON), 79 (WB-EXPAK), 80 (ISO-IP), 81 (VMTP), 82 (SECURE-VMTP), 83 (VINES),
                            84 (TTP), 85 (NSFNET-IGP), 86 (DGP), 87 (TCF), 88 (EIGRP), 89 (OSPFIGP), 90 (Sprite-RPC), 91 (LARP),
                            92 (MTP), 93 (AX.25), 94 (IPIP), 95 (MICP), 96 (SCC-SP), 97 (ETHERIP), 98 (ENCAP), 100 (GMTP),
                            101 (IFMP), 102 (PNNI), 103 (PIM), 104 (ARIS), 105 (SCPS), 106 (QNX), 107 (A/N), 108 (IPComp),
                            109 (SNP), 110 (Compaq-Peer), 111 (IPX-in-IP), 112 (VRRP), 113 (PGM), 115 (L2TP), 116 (DDX),
                            117 (IATP), 118 (STP), 119 (SRP), 120 (UTI), 121 (SMP), 122 (SM), 123 (PTP), 124 (ISIS over IPv4),
                            125 (FIRE), 126 (CRTP), 127 (CRUDP), 128 (SSCOPMCE), 129 (IPLT), 130 (SPS), 131 (PIPE), 132 (SCTP),
                            133 (FC), 134 (RSVP-E2E-IGNORE), 135 (Mobility Header), 136 (UDPLite), 137 (MPLS-in-IP),
                            253 (Experimental), 255 (Reserved)
                            Default: 253
                        sourceAddr -
                            Default: 192.85.1.2
                            Type: IPV4ADDR
                        totalLength -
                            Default: 20
                            Type: INTEGER
                        ttl -
                            Default: 255
                            Type: INTEGER
                        identification -
                            Type: INTEGER
                            Default: 0
                        ihl -
                            Type: INTEGER
                            Default: 5
                        version
                            Type: INTEGER
                            Default: 4

                    ii) ipv6:
                        destAddr
                            Type: IPV6ADDR
                            Default: 2000::1
                        destPrefixLength
                            Type: INTEGER
                            Default: 64
                        flowLabel
                            Type: INTEGER
                            Default: 0
                        gateway
                            Type: IPV6ADDR
                            Default: ::0
                        hopLimit
                            Type: INTEGER
                            Default: 255
                            A text name for the object. This attribute is required when you use stream block modifiers such as RangeModifier,
                            RandomModifier, and TableModifier. See the description of the OffsetReference attribute for these modifier objects for more
                            information.
                        Name
                            Type: string
                        nextHeader
                            Type: IpProtocolNumbers
                            Default: 59
                        Possible Values:
                            0 HOPOPT
                            1 ICMP
                            2 IGMP
                            3 GGP
                            4 IP
                            5 ST
                            6 TCP
                            7 CBT
                            8 EGP
                            9 IGP
                            10 BBN-RCC-MON
                            11 NVP-II
                            12 PUP
                            13 ARGUS
                            14 EMCON
                            15 XNET
                            16 CHAOS
                            17 UDP
                            18 MUX
                            19 DCN-MEAS
                            20 HMP
                            21 PRM
                            22 XNS-IDP
                            23 TRUNK-1
                            24 TRUNK-2
                            25 LEAF-1
                            26 LEAF-2
                            27 RDP
                            28 IRTP
                            29 ISO-TP4
                            30 NETBLT
                            31 MFE-NSP
                            32 MERIT-INP
                            33 SEP
                            34 3PC
                            35 IDPR
                            36 XTP
                            37 DDP
                            38 IDPR-CMTP
                            39 TP++
                            40 IL
                            41 IPv6
                            42 SDRP
                            43 IPv6-Route
                            44 IPv6-Frag
                            45 IDRP
                            46 RSVP
                            47 GRE
                            48 MHRP
                            49 BNA
                            50 ESP
                            51 AH
                            52 I-NLSP
                            53 SWIPE
                            54 NARP
                            55 MOBILE
                            56 TLSP
                            57 SKIP
                            58 IPv6-ICMP
                            59 IPv6-NoNxt
                            60 IPv6-Opts
                            62 CFTP
                            64 SAT-EXPAK
                            65 KRYPTOLAN
                            66 RVD
                            67 IPPC
                            69 SAT-MON
                            70 VISA
                            71 IPCV
                            72 CPNX
                            73 CPHB
                            74 WSN
                            75 PVP
                            76 BR-SAT-MON
                            77 SUN-ND
                            78 WB-MON
                            79 WB-EXPAK
                            80 ISO-IP
                            81 VMTP
                            82 SECURE-VMTP
                            83 VINES
                            84 TTP
                            85 NSFNET-IGP
                            86 DGP
                            87 TCF
                            88 EIGRP
                            89 OSPFIGP
                            90 Sprite-RPC
                            91 LARP
                            92 MTP
                            93 AX.25
                            94 IPIP
                            95 MICP
                            96 SCC-SP
                            97 ETHERIP
                            98 ENCAP
                            100 GMTP
                            101 IFMP
                            102 PNNI
                            103 PIM
                            104 ARIS
                            105 SCPS
                            106 QNX
                            107 A/N
                            108 IPComp
                            109 SNP
                            110 Compaq-Peer
                            111 IPX-in-IP
                            112 VRRP
                            113 PGM
                            115 L2TP
                            116 DDX
                            117 IATP
                            118 STP
                            119 SRP
                            120 UTI
                            121 SMP
                            122 SM
                            123 PTP
                            124 ISIS over IPv4
                            125 FIRE
                            126 CRTP
                            127 CRUDP
                            128 SSCOPMCE
                            129 IPLT
                            130 SPS
                            131 PIPE
                            132 SCTP
                            133 FC
                            134 RSVP-E2E-IGNORE
                            135 Mobility Header
                            136 UDPLite
                            137 MPLS-in-IP
                            253 Experimental
                            255 Reserved
                        payloadLength
                            Type: INTEGER
                            Default: 0
                        prefixLength
                            Type: INTEGER
                            Default: 64
                        sourceAddr
                            Type: IPV6ADDR
                            Default: 2000::2
                        trafficClass
                            Type: INTEGER
                            Default: 0
                        version
                            Type: INTEGER
                            Default: 6

                    iii) tcp:
                        ackBit
                            Type: BITSTRING
                            Default: 1
                        ackNum
                            Type: INTEGER
                            Default: 234567
                        checksum
                            Type: INTEGER
                            Default: Automatically calculated for each packet. (If you set this to 0, the checksum will not be calculated and will be
                            the same for each packet.)
                        cwrBit
                            Type: BITSTRING
                            Default: 0
                        destPort
                            Type: WellKnownPorts
                            Default: 1024
                            Possible Values:
                            1 TCPMUX
                            2 CompressNet
                            3 CompressProcess
                            5 RJE
                            7 ECHO
                            9 DISCARD
                            11 SYSTAT
                            13 DAYTIME
                            17 QOTD
                            18 MSP
                            19 CHARGEN
                            20 FTPDATA
                            21 FTP
                            22 SSH
                            23 Telnet
                            25 SMTP
                            27 NSW
                            29 MSG-ICP
                            31 MSG-AUTH
                            33 DSP
                            37 TIME
                            38 RAP
                            39 RLP
                            41 Graphics
                            42 Nameserver
                            43 WHOIS
                            44 MPM-flags
                            45 MPM
                            46 MPM-send
                            47 NI FTP
                            48 AUDIT
                            49 TACACS
                            50 RE-MAIL
                            51 LA-MAINT
                            52 XNS-time
                            53 DNS
                            54 XNS
                            55 ISI-GL
                            56 XNS-auth
                            69 TFTP
                            70 GOPHER
                            79 FINGER
                            80 HTTP
                            88 KERBEROS
                            119 NNTP
                            123 NTP
                            161 SNMP
                            162 SNMPTRAP
                            179 BGP
                            194 IRC
                            520 RIP
                            521 RIPNG
                            3784 BFD
                        ecnBit
                            Type: BITSTRING
                            Default: 0
                        finBit
                            Type: BITSTRING
                            Default: 0
                            A text name for the object. This attribute is required when you use stream block modifiers such as RangeModifier,
                            RandomModifier, and TableModifier. See the description of the OffsetReference attribute for these modifier objects for
                            more information.
                        Name
                            Type: string
                        offset
                            Type: INTEGER
                            Default: 5
                        pshBit
                            Type: BITSTRING
                            Default: 0
                        reserved
                            Type: BITSTRING
                            Default: 0000
                        rstBit
                            Type: BITSTRING
                            Default: 0
                        seqNum
                            Type: INTEGER
                            Default: 123456
                        sourcePort
                            Type: INTEGER
                            Default: 1024
                        synBit
                            Type: BITSTRING
                            Default: 0
                        urgBit
                            Type: BITSTRING
                            Default: 0
                        urgentPtr
                            Type: INTEGER
                            Default: 0
                        window
                            Type: INTEGER
                            Default: 4096

                    iv) udp:
                        checksum
                            Type: INTEGER
                            Default: Automatically calculated for each packet. (If you set this to 0, the checksum will not be calculated and will be the same
                            for each packet.)
                        destPort
                            Type: WellKnownPorts
                            Default: 1024
                            Possible Values:
                            1 TCPMUX
                            2 CompressNet
                            3 CompressProcess
                            5 RJE
                            7 ECHO
                            9 DISCARD
                            11 SYSTAT
                            13 DAYTIME
                            17 QOTD
                            18 MSP
                            19 CHARGEN
                            20 FTPDATA
                            21 FTP
                            22 SSH
                            23 Telnet
                            25 SMTP
                            27 NSW
                            29 MSG-ICP
                            31 MSG-AUTH
                            33 DSP
                            37 TIME
                            38 RAP
                            39 RLP
                            41 Graphics
                            42 Nameserver
                            43 WHOIS
                            44 MPM-flags
                            45 MPM
                            46 MPM-send
                            47 NI FTP
                            48 AUDIT
                            49 TACACS
                            50 RE-MAIL
                            51 LA-MAINT
                            52 XNS-time
                            53 DNS
                            54 XNS
                            55 ISI-GL
                            56 XNS-auth
                            69 TFTP
                            70 GOPHER
                            79 FINGER
                            80 HTTP
                            88 KERBEROS
                            119 NNTP
                            123 NTP
                            161 SNMP
                            162 SNMPTRAP
                            179 BGP
                            194 IRC
                            520 RIP
                            521 RIPNG
                            3784 BFD
                        length
                            Type: INTEGER
                            Default: 0
                            A text name for the object. This attribute is required when you use stream block modifiers such as RangeModifier, RandomModifier,
                            and TableModifier. See the description of the OffsetReference attribute for these modifier objects for more information.
                        Name
                            Type: string
                        sourcePort
                            Type: INTEGER
                            Default: 1024

                    v) load:
                        InterFrameGap - Gap (in bytes) between frames in same burst (priority-based scheduling).
                            Default: 12
                            Type: INTEGER
                        InterFrameGapUnit - Unit for inter-frame gap.
                            Values: BITS_PER_SECOND, BYTES, FRAMES_PER_SECOND, KILOBITS_PER_SECOND, MEGABITS_PER_SECOND,
                            MILLISECONDS, NANOSECONDS, PERCENT_LINE_RATE
                            Default: BYTES
                        Load - Load value set on stream block.
                            Default: 10
                            Type: double
                        LoadUnit - Load unit applied to stream block.
                            Values: BITS_PER_SECOND, FRAMES_PER_SECOND, INTER_BURST_GAP, INTER_BURST_GAP_IN_MILLISECONDS,
                            INTER_BURST_GAP_IN_NANOSECONDS, KILOBITS_PER_SECOND, MEGABITS_PER_SECOND, PERCENT_LINE_RATE
                            Default: PERCENT_LINE_RATE

                    vi) frame:
                        FillType - Fill pattern type to be used for the payload.
                            Values: CONSTANT, DECR, INCR, PRBS
                            Default: CONSTANT
                            Type: enum
                        FrameLengthMode - Frame length mode used by this stream.
                            Values: AUTO, DECR, FIXED, IMIX, INCR, RANDOM
                            Default: FIXED
                            Type: enum
                        FixedFrameLength - Fixed value for frame length.
                            Range: 12 - 16383
                            Default: 128
                            Type: integer
                        MaxFrameLength - Maximum frame length for random mode.
                            Range: 12 - 16383
                            Default: 256
                            Type: integer
                        MinFrameLength - Minimum frame length for random mode.
                            Range: 12 - 16383
                            Default: 128
                            Type: integer
                        SrcBinding - Source object for the streamblock
                            Type: STC Object
                            Default: None
                        DstBinding - Destination object for the streamblock
                            Type: STC Object
                            Default: None
        Return:
            N/A

        Example:
            1) For creating a bound streamblock:
            hlk_traffic_config_bound_streamblock    name=UPSTREAM    portID=${clientPortID}    device=device1

            2) For configuring a streamblock interface:
            hlk_traffic_config_bound_streamblock    name=UPSTREAM    type=ipv4    sourceAddr=40.20.1.1    destAddr=40.20.1.2    gateway=40.20.1.2    prefixLength=24

            3) For configuring source and destination:
            hlk_traffic_config_bound_streamblock    name=UPSTREAM    type=frame    srcBinding=oIPv41    dstBinding=oIPv42

            4) Create and configure an interface for the streamblock:
            hlk_traffic_config_bound_streamblock    name=UPSTREAM    portID=${clientPortID}    device=device1    type=ipv4
                                                    sourceAddr=40.20.1.1    destAddr=40.20.1.2    gateway=40.20.1.2    prefixLength=24
        """
        llk_log_begin_proc()

        assert (kwargs.keys()), "No arguments given as input for hlk_traffic_config_device_streamblock"
        dKwargs = dict(map(lambda (k,v): (str(k).lower(), v), kwargs.iteritems()))
        llk_log_end_proc()
        assert ('name' in dKwargs.keys()), "Streamblock names to be created/configured must be specified"

        sSBName = str(dKwargs['name'])
        if sSBName:
            del dKwargs['name']
        else:
            raise Exception ("The input entered for streamblock name %s is not valid" % str(sSBName))

        sSBType = ''
        try: 
            sSBType = dKwargs.get('streamtype')
        except Exception as inst:
            sSBType = dKwargs.get('type')

        sDeviceName = str(dKwargs['device'])
        if sSBName:
            del dKwargs['device']

        sPortID = str(dKwargs['portid'])
        if sPortID:
            del dKwargs['portid']

        if sPortID and sDeviceName:
            oStreamBlock = self.oLlkStc.llk_stc_create_streamblock(self.dPortIDObjects[sPortID],sSBName)
            self.dStreamblockObjects[sSBName] = oStreamBlock
            if sSBName not in self.dDevices[sDeviceName].keys() or sSBName not in self.dStreamblockObjects.keys():
                self.dDevices[sDeviceName][sSBName] = oStreamBlock
            else:
                raise Exception("Streamblock with the given name %s already exists!" % str(sSBName))
            if sSBType:
                self._hlk_traffic_config_streamblock_itf(sSBName,dKwargs)
        elif sSBType:
            if sSBName in self.dStreamblockObjects.keys():
                self._hlk_traffic_config_streamblock_itf(sSBName,dKwargs)
        else:
            raise Exception("The input should either consist of streamblock's name, port ID and device's name to create a bound streamblock "
                            "or name of the streamblock and the type of interface for configuring an interface for the streamblock")

    def hlk_traffic_stack_device_protocols(self,oDevice):
        """
        Stacking the protocols of device
        """
        llk_log_begin_proc()

        sDevice = ""
        if str(oDevice) in self.dDevices.keys():
            sDevice = self.dDevices[oDevice]['oDevice']
        else:
            llk_log_end_proc()
            raise AssertionError("%s don't exist" % oDevice)
            
        if 'vlan' in self.dDevices[oDevice].keys():
            lVlans = []
            lVlans = self.dDevices[oDevice]['vlan']
            iNumVlans = len(lVlans)
            if iNumVlans > 1:
                for i in range(0,iNumVlans-1):
                    self.oLlkStc.llk_stc_device_stackedon_itf(lVlans[i+1],lVlans[i])

            if 'ethii' in self.dDevices[oDevice].keys():
                self.oLlkStc.llk_stc_device_stackedon_itf(lVlans[0],self.dDevices[oDevice]['ethii'])
            else:
                llk_log_end_proc()
                raise Exception("Ethernet interface is not created in order to stack device protocols")

            if 'ipv4' in self.dDevices[oDevice].keys():
                self.oLlkStc.llk_stc_device_stackedon_itf(self.dDevices[oDevice]['ipv4'],lVlans[-1])
            else:
                llk_log_end_proc()
                raise Exception("IP interface is not created in order to stack device protocols")
        else: 
            # no vlan itf, append 'ethii' to end of 'ipv4'    
            if 'ethii' in self.dDevices[oDevice].keys() and 'ipv4' in self.dDevices[oDevice].keys():
                self.oLlkStc.llk_stc_device_stackedon_itf(self.dDevices[oDevice]['ipv4'],self.dDevices[oDevice]['ethii'])
            else:
                llk_log_end_proc()
                raise Exception("Ethernet and IP interfaces are to be created in order to stack device protocols") 

            llk_log_end_proc()

    def hlk_traffic_stack_pppoe_protocols(self,oDevice):
        """
        Stacking the protocols of device
        """
        llk_log_begin_proc()

        sDevice = ""
        if str(oDevice) in self.dDevices.keys():
            sDevice = self.dDevices[oDevice]['oDevice']
        else:
            llk_log_end_proc()
            raise AssertionError("%s don't exist" % oDevice)
            
        if 'vlan' in self.dDevices[oDevice].keys():
            lVlans = []
            lVlans = self.dDevices[oDevice]['vlan']
            iNumVlans = len(lVlans)
            if iNumVlans > 1:
                for i in range(0,iNumVlans-1):
                    self.oLlkStc.llk_stc_device_stackedon_itf(lVlans[i+1],lVlans[i])

            if 'ethii' in self.dDevices[oDevice].keys():
                self.oLlkStc.llk_stc_device_stackedon_itf(lVlans[0],self.dDevices[oDevice]['ethii'])
            else:
                llk_log_end_proc()
                raise Exception("Ethernet interface is not created in order to stack device protocols")

            if 'pppoe' in self.dDevices[oDevice].keys():
                self.oLlkStc.llk_stc_device_stackedon_itf(self.dDevices[oDevice]['pppoe'],lVlans[-1])
            else:
                llk_log_end_proc()
                raise Exception("IP interface is not created in order to stack device protocols")
        else: 
            # no vlan itf, append 'ethii' to end of 'pppoe'    
            if 'ethii' in self.dDevices[oDevice].keys() and 'pppoe' in self.dDevices[oDevice].keys():
                self.oLlkStc.llk_stc_device_stackedon_itf(self.dDevices[oDevice]['pppoe'],self.dDevices[oDevice]['ethii'])
            else:
                llk_log_end_proc()
                raise Exception("Ethernet and IP interfaces are to be created in order to stack device protocols") 

            llk_log_end_proc()


    def hlk_traffic_stack_dhcpv6_protocols(self,oDevice):
        """
        Stacking the protocols of device
        """
        llk_log_begin_proc()

        sDevice = ""
        if str(oDevice) in self.dDevices.keys():
            sDevice = self.dDevices[oDevice]['oDevice']
        else:
            llk_log_end_proc()
            raise AssertionError("%s don't exist" % oDevice)
            
        if 'vlan' in self.dDevices[oDevice].keys():
            lVlans = []
            lVlans = self.dDevices[oDevice]['vlan']
            iNumVlans = len(lVlans)
            if iNumVlans > 1:
                for i in range(0,iNumVlans-1):
                    self.oLlkStc.llk_stc_device_stackedon_itf(lVlans[i+1],lVlans[i])

            if 'ethii' in self.dDevices[oDevice].keys():
                self.oLlkStc.llk_stc_device_stackedon_itf(lVlans[0],self.dDevices[oDevice]['ethii'])
            else:
                llk_log_end_proc()
                raise Exception("Ethernet interface is not created in order to stack device protocols")

            if 'ipv6' in self.dDevices[oDevice].keys():
                self.oLlkStc.llk_stc_device_stackedon_itf(self.dDevices[oDevice]['ipv6'],lVlans[-1])
                self.oLlkStc.llk_stc_device_stackedon_itf(self.dDevices[oDevice]['ipv6ll'],lVlans[-1])
            else:
                llk_log_end_proc()
                raise Exception("IPv6 interface is not created in order to stack device protocols")
        else: 
            # no vlan itf, append 'ethii' to end of 'ipv6'    
            if 'ethii' in self.dDevices[oDevice].keys() and 'ipv6' in self.dDevices[oDevice].keys():
                self.oLlkStc.llk_stc_device_stackedon_itf(self.dDevices[oDevice]['ipv6'],self.dDevices[oDevice]['ethii'])
                self.oLlkStc.llk_stc_device_stackedon_itf(self.dDevices[oDevice]['ipv6ll'],self.dDevices[oDevice]['ethii'])
            else:
                llk_log_end_proc()
                raise Exception("Ethernet and IPv6 interfaces are to be created in order to stack device protocols") 

            llk_log_end_proc()




    def hlk_traffic_config_ieee1588v2(self,oDevice,**kwargs):
        """
        Create/Configure ieee1588v2Clock device
        """
        llk_log_begin_proc()
        
        # check kwargs
        assert (kwargs.keys()), "No arguments given as input for hlk_traffic_config_ieee1588v2Clock"
        dKwargs = dict(map(lambda (k,v): (str(k).lower(), v), kwargs.iteritems()))

        # set toplevel,primary and uses itf
        self.oLlkStc.llk_stc_device_toplevel_itf(self.dDevices[oDevice]['oDevice'],self.dDevices[oDevice]['ipv4'])
        self.oLlkStc.llk_stc_device_primary_itf(self.dDevices[oDevice]['oDevice'],self.dDevices[oDevice]['ipv4'])
        #self.oLlkStc.llk_stc_device_uses_itf(oIeee1588v2ClockConfig,self.dDevices[oDevice]['ipv4'])

        # stacking device protocols
        self.hlk_traffic_stack_device_protocols(oDevice)

        # DELETED: CONVERT TO function
        # sCommand = "stc::config emulateddevice1 -AffiliationPort-targets  port2"
        # self.oLlkStc.llk_stc_send_command(sCommand)

        # config object oIeee1588v2ClockConfig
        oIeee1588v2ClockConfig = ''
        if 'clockconfig' in dKwargs.keys():
            # prepare dict of dClockConfig
            lClockConfig = dKwargs['clockconfig'].strip('[](){}').split(',')
            dClockConfig = {}
            for eachConf in lClockConfig:
                lConfigParsed = eachConf.strip("[](){}").split('=')
                if lConfigParsed:
                    if len(lConfigParsed) == 2:
                        dClockConfig[str(lConfigParsed[0]).lower()] = str(lConfigParsed[1])
                    else:
                        logger.warn("Config parameter format %s is not correct" % str(eachConf))
            # set sClock
            sClock = ''
            if 'oclock' in dKwargs.keys():
                # modify the exist oIeee1588v2ClockConfig
                sClock = dKwargs['oclock']
            else:
                # create a oIeee1588v2ClockConfig
                sClock = ''

            # config device ieee1588v2ClockConfig
            oIeee1588v2ClockConfig = self.oLlkStc.llk_stc_config_device_ieee1588v2ClockConfig(oDevice=self.dDevices[oDevice]['oDevice'], oClockCofig=sClock, **dClockConfig)

        # config device configuration
        if 'deviceconfig' in dKwargs.keys():
            lDeviceConfig = dKwargs['deviceconfig'].strip('[](){}').split(',')
            dDeviceConfig = {}
            for eachConf in lDeviceConfig:
                lConfigParsed = eachConf.strip("[](){}").split('=')
                if lConfigParsed:
                    if len(lConfigParsed) == 2:
                        dDeviceConfig[str(lConfigParsed[0]).lower()] = str(lConfigParsed[1])
                    else:
                        logger.warn("Config parameter format %s is not correct" % str(eachConf))
            dDeviceConfig['name'] = oDevice
            hlk_traffic_config_device(**dDeviceConfig)

        # DELETED
        # sCommand = "stc::apply"
        # self.oLlkStc.llk_stc_send_command(sCommand)

        llk_log_end_proc()

        return oIeee1588v2ClockConfig

    def hlk_traffic_verify_device_status(self,oDevice,configName,**kwargs):
        
        sDevice = self.dDevices[oDevice]['oDevice']
        result = self.oLlkStc.llk_stc_verify_device_state(sDevice,configName,**kwargs)

        if result :
            return "PASS"
        else :
            return "FAIL"

    def hlk_traffic_verify_ipv6arpnd_status(self,oDevice,configName,**kwargs):
        
        sDevice = self.dDevices[oDevice]['oDevice']
        result = self.oLlkStc.llk_stc_verify_ipv6arpnd_state(sDevice,configName,**kwargs)

        if result :
            return "PASS"
        else :
            return "FAIL"


class StcContinueFailure (AssertionError) :
    ROBOT_CONTINUE_ON_FAILURE = True

