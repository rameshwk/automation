#!/usr/bin/env python
import re
import os
import sys
import time
import Tkinter
from robot.libraries.BuiltIn import BuiltIn
from robot.api import logger
from robot.libraries.BuiltIn import RobotNotRunningError
from llk_log import *
import logging

try:
    for lib_path in [\
    os.environ['ROBOTREPO'] +'/HLKS',
    os.environ['ROBOTREPO'] +'/LIBS/COM_STC',
    os.environ['ROBOTREPO'] +'/LIBS/DATA_TRANSLATION'] :
        if lib_path not in sys.path:
            sys.path.append(lib_path)
except Exception as inst:
    raise AssertionError("%s -> Fail to set sys.path, %s" % (__name__,inst))

import llk_traffic
import data_translation


class llk_stc(object):

    ROBOT_LIBRARY_SCOPE = 'GLOBAL'

    def __init__(self):

        self.oTcl = Tkinter.Tcl()
        try:
            try : 
                self.TRAF_OUTPUT_DIR = BuiltIn().replace_variables('${OUTPUTDIR}') + "/TRAFFIC"
            except Exception as inst:
                self.TRAF_OUTPUT_DIR = "/tmp/TRAFFIC"
            #self.TRAF_OUTPUT_DIR = "/home1/user/abvenkat/robot-mso/LOG/abvenkat_2016-08-02_11:32:55"
            if not os.path.exists(self.TRAF_OUTPUT_DIR):
                os.makedirs(self.TRAF_OUTPUT_DIR)

            """
            # set the path of ta_traffic_cmd.log
            sTrafCmdLogPath = self.TRAF_OUTPUT_DIR + '/ta_traffic_cmd.log'
            # delete the old ta_traffic_cmd.log
            sDeleteFileCmd = 'rm -rf %s' % sTrafCmdLogPath
            os.system(sDeleteFileCmd)

            # create a new ta_traffic_cmd.log
            open (sTrafCmdLogPath,'a')
            """

        except RobotNotRunningError:
          pass

        #Dictionary with key value pairs as Port ID and Capture ID respectively
        self.dPortCaptureId = {}
        #Dictonary with key value pairs as Capture ID and list of dictionary of Parsed packets
        self.dCapturePkt = {}
        self.dCapturePcap = {}
        # STC project object
        self.oStcProject = "project1"
        self.sVersion = None
        self.bLoadSTC = False


    def llk_stc_load(self,sVersion,sStcPath):
        """
        Initializes STC library
        Args:
            sVersion: Version of STC tcl package to be loaded
        Returns:
            N/A
        Examples:
            llk_stc_load(4.52)
        """
        llk_log_begin_proc()

        if self.bLoadSTC is False:
            llk_log_debug("Sourcing STC Library: " + sStcPath)
            try:
                self.oTcl.eval('source ' + sStcPath)
            except Exception, sError:
                llk_log_warn(sError)
                llk_log_end_proc()
                raise IOError
            llk_log_debug("Succesfully loaded STC Library version " + sVersion)
            self.bLoadSTC = True
            self.oStcProject = self.llk_stc_send_command('stc::create project')
            llk_log_debug("Succesfully initialized STC project")
        else:
            llk_log_debug("STC library has already been loaded!!")

        llk_log_end_proc()

    def llk_stc_send_command(self,sCommand,apply=False):
        """
        Loads the STC library for the first time provided that the version is given in a consistent manner in the
        platform csv file.
        Once the library is loaded, it executes the commands that is passed to the method.
        Args:
            sCommand: STC tcl command to be executed
        Returns:
            sOutput:  STC command response
        Examples:
            llk_stc_send_command("stc::get system1")
        """
        #llk_log_begin_proc()

        try:
            sOutput = ''
            if self.bLoadSTC is False:
                ldSTCVersion = data_translation.get_table_line('TrafficToolsData','Type=SpirentTcl') 
                if ldSTCVersion:
                    sVersion = ldSTCVersion['Version']
                    sPath = ldSTCVersion['Path']
                    self.llk_stc_load(sVersion,sPath)

            if self.bLoadSTC is True:
                logger.debug("<span style=\"color: #238E23\">CMD EXEC: " + sCommand+"</span>",html=True)
                # write STC command to log file
                logger_cmd_ta = logging.getLogger("ta_traffic_cmd")
                logger_cmd_ta.debug('>CMD EXEC: ' + sCommand)

                sOutput = self.oTcl.eval(sCommand)
                if sOutput.__len__() > 0:
                    logger.debug("<span style=\"color: #238E23\">CMD RESP: " + sOutput+"</span>",html=True)
                    # write STC command to log file
                    logger_cmd_ta.debug('>CMD RESP: ' + sOutput + '\r\n')

                if apply:
                    self.oTcl.eval("stc::apply")
                    logger_cmd_ta.debug('>CMD EXEC: ' + 'stc::apply')

        except Exception, sError:
            s = sys.exc_info()
            print("line:%s" % s[2].tb_lineno)
            llk_log_warn(sError)
            #llk_log_end_proc()
            raise RuntimeError
        #llk_log_end_proc()
        return sOutput

    def llk_stc_session_connect(self,sServer, sSession, sOwner):
        """
        Connect to existing session on STC Lab Server
        Args:
            sServer: Lab server chassis IP
            sSession: Session Name
            sOwner: Session Owner
        Returns:
            N/A
        Examples:
            llk_stc_session_connect(172.22.179.239, "dev", "gsweezy")
        """
        llk_log_begin_proc()

        self.llk_stc_send_command('stc::perform CSTestSessionConnectCommand -Host ' + sServer +
                                  ' -CreateNewTestSession FALSE -TestSessionName ' + sSession + ' -OwnerId ' + sOwner)
        llk_log_info("Successfully connected to STC Session: " + sSession)

        llk_log_end_proc()

    def llk_stc_create_port(self,sChassisIp, iSlot, iPort, sType, sName,sLineSpeed='x'):
        """
        Create STC port instance
        Args:vlan
            sChassisIp: Chassis IP
            iSlot: Chassis Slot
            iPort: Chassis Port
            sType: Port interface type (FIBER|COPPER)
            sName: User given name (Ex: Network)
        Returns:
            oStcPort: Port object reference
        Examples:
           llk_stc_create_port(124.150.10.50, 7, 4, Copper, Network)
        """
        llk_log_begin_proc()

        sLocation = "//" + sChassisIp + "/" + iSlot + "/" + iPort
        self.llk_stc_send_command("stc::create port -under " + str(self.oStcProject) + " -location " + sLocation + " -useDefaultHost FALSE -name " + sName)
        oStcPort = str.split(str(self.llk_stc_send_command("stc::get " + str(self.oStcProject) + " -children-port")))[-1]

        # Configure interface type
        if sType.upper() == "FIBER":
            if sLineSpeed != 'x':
                oFiberPhy = self.llk_stc_send_command("stc::create EthernetFiber -under " + oStcPort + " -LineSpeed " + sLineSpeed)
            else:
                oFiberPhy = self.llk_stc_send_command("stc::create EthernetFiber -under " + oStcPort)
            self.llk_stc_send_command("stc::config " + oStcPort + " -activePhy " + oFiberPhy)
        elif sType.upper() == "COPPER":
            if sLineSpeed != 'x':
                oCopperPhy = self.llk_stc_send_command("stc::create EthernetCopper -under " + oStcPort + " -LineSpeed " + sLineSpeed)
                self.llk_stc_send_command("stc::config " + oStcPort + " -activePhy " + oCopperPhy)
            else:
                llk_log_info("Default type is already Copper!!")
        else:
            llk_log_warn("Invalid type. Expected either FIBER or COPPER.")
            llk_log_end_proc()
            raise ValueError

        #Configure rate based load per streamblock
        oGen = self.llk_stc_send_command("stc::get " + oStcPort + " -children-generator")
        oGenConfig = self.llk_stc_send_command("stc::get " + oGen + " -children-generatorconfig")
        self.llk_stc_send_command("stc::config " + oGenConfig + " -SchedulingMode RATE_BASED")
        llk_log_info("Successfully created port config for " + sLocation)
        llk_log_end_proc()
        return oStcPort

    def llk_stc_config_burstsize(self, oPort, **kwargs) :
        """
        Configure the burst size of the streamblocks under the port object

        Input Arguments:
            oPort - The port object on which the burst parameter is to be configured

            **kwargs:
                Active - Whether this object will be active when you call the apply function.
                    Values: TRUE, FALSE
                    Default: TRUE
                    Type: bool
                AdvancedInterleaving - Enable the option to interleave packets.
                    Values: TRUE, FALSE
                    Default: FALSE
                    Type: bool
                BurstSize - Number of frames in each burst transmitted.
                    Default: 1
                    Type: u32
                Duration - Length of packet transmission.
                    Default: 30
                    Type: double
                DurationMode - Length of transmission mode (cont/bursts/secs).
                    Values: BURSTS, CONTINUOUS, SECONDS, STEP, TABLE_REPETITIONS
                    Default: CONTINUOUS
                    Type: enum
                FixedLoad - Fixed load value.
                    Default: 10
                    Type: double
                InterFrameGap - Gap (bytes) between frames in the same burst.
                    Default: 12
                    Type: double
                InterFrameGapUnit - Unit for inter-frame gap.
                    Values: BITS_PER_SECOND, BYTES, FRAMES_PER_SECOND, KILOBITS_PER_SECOND, MEGABITS_PER_SECOND, MILLISECONDS, NANOSECONDS, PERCENT_LINE_RATE
                    Default: BYTES
                    Type: enum
                JumboFrameThreshold - TX frame sizes above this threshold are jumbo frames.
                    Default: 1518
                    Type: s16
                LoadMode - Load mode as fixed or random rate.
                    Values: FIXED, RANDOM
                    Default: FIXED
                    Type: enum
                LoadUnit - Load unit for overall port load (port-based scheduling).
                    Values: BITS_PER_SECOND, FRAMES_PER_SECOND, INTER_BURST_GAP, KILOBITS_PER_SECOND, MEGABITS_PER_SECOND, PERCENT_LINE_RATE
                    Default: PERCENT_LINE_RATE
                    Type: enum
                Name - A user-defined name for this object.
                    Default: "" (empty string)
                    Type: string
                OversizeFrameThreshold - TX frame sizes above this threshold are over-sized frames.
                    Default: 9018
                    Type: s16
                RandomLengthSeed - Seed value for random frame length.
                    Range: 1 - 16777215
                    Default: 10900842
                    Type: s32
                RandomMaxLoad - Maximum random port load (port-based scheduling).
                    Default: 100
                    Type: double
                RandomMinLoad - Minimum random port load (port-based scheduling).
                    Default: 10
                    Type: double
                SchedulingMode - Scheduling mode (port/rate/priority/manual).
                    Values: MANUAL_BASED, PORT_BASED, PRIORITY_BASED, RATE_BASED
                    Default: PORT_BASED
                    Type: enum
                StepSize - Number of packets to transmit per step. Applicable to step mode.
                    Default: 1
                    Type: u16
                TimestampLatchMode - Position timestamp on frame location.
                    Values: END_OF_FRAME, START_OF_FRAME
                    Default: START_OF_FRAME
                    Type: enum
                UndersizeFrameThreshold - TX frame sizes below this threshold are under-sized frames.
                    Default: 64
                    Type: s16

        Return value:
            N/A

        Ex:  llk_stc_config_burstsize(Network,BurstSize=1,DurationMode=CONTINUOUS)
        """

        # Get the generatorconfig object associated with the port object
        llk_log_begin_proc()

        oGen = self.llk_stc_send_command("stc::get " + oPort + " -children-generator")
        oGenConfig = self.llk_stc_send_command("stc::get " + oGen + " -children-generatorconfig")

        sCommand = "stc::config " + oGenConfig
        for key in kwargs:
            sCommand = sCommand + " -" + key + " " + str(kwargs[key])

        self.llk_stc_send_command(sCommand)
        llk_log_debug("Successfully configured the burst parameters for all the streamblocks under the port")

        llk_log_end_proc()

    def llk_stc_connect(self):

        """
        Connect to STC Ports
        Args:
            N/A
        Returns:
            N/A
        Examples:
            llk_stc_connect()
        """
        llk_log_begin_proc()

        self.llk_stc_send_command("stc::perform attachPorts -autoConnect true -portList [ stc::get "
                                  + str(self.oStcProject) + " -children-Port ]")
        llk_log_debug("Successfully connected to STC ports")

        llk_log_end_proc()

    def llk_stc_disconnect(self):
        """
        Disconnect from STC Ports
        Args:
            N/A
        Returns:
            N/A
        Examples:
            llk_stc_disconnect()
        """

        llk_log_begin_proc()

        self.llk_stc_send_command("stc::perform detachPorts -portList [ stc::get " + str(self.oStcProject)
                                  + " -children-Port ]")
        llk_log_info("Succesfully disconnected from STC ports")

        llk_log_end_proc()

    def llk_stc_create_host(self,oPort,**kwargs):
        """
        Create host in STC
        Args:
            oPort: STC port object
        Returns:

        """
        llk_log_begin_proc()
        sCommand = "stc::create Host -under " + str(self.oStcProject)
        for key in kwargs:
            sCommand = sCommand + " -" + key + " " + str(kwargs[key])
        oHost = self.llk_stc_send_command(sCommand)
        llk_log_info("Successfully created host")
        self.llk_stc_send_command("stc::config " + oHost + " -AffiliatedPort " + oPort)
        llk_log_info("Successfully affiliated port to the host device")
        llk_log_end_proc()
        return oHost

    def llk_stc_config_device(self, oDevice='', oPort='', **kwargs):
        """
        Configure a device in STC. Create one if it does not exist.

        Args:
            oDevice(Required for creating a new device) - Emulated device object
                Default: ""
                Type: String

            oPort(Required for configuring an existing device): STC port object
                Default: ""
                Type: String

            kwargs(Optional):
                Active - Whether this object will be active when you call the apply function.
                    Values: TRUE, FALSE
                    Default: TRUE
                    Type: bool
                DeviceCount - Number of devices in the device block.
                    Range: 1 - 0xFFFFFFFF
                    Default: 1
                    Type: u32
                EnablePingResponse - Whether the emulated device will respond to ping.
                    Values: TRUE, FALSE
                    Default: FALSE
                    Type: bool
                Ipv6RouterId - IPv6 Router ID.
                    Default: 2000::1
                    Type: ipv6
                Ipv6RouterIdStep - IPv6 Router ID Step.
                    Default: 0000::1
                    Type: ipv6
                Name - A user-defined name for this object.
                    Default: "" (empty string)
                    Type: string
                RouterId - Router ID.
                    Value: IPv4 address
                    Default: 192.0.0.1
                    Type: ip
                RouterIdStep - Router ID Step.
                    Value: IPv4 address
                    Default: 0.0.0.1
                    Type: ip
        Returns:
            oDevice - Emulated device object
                Default: ""
                Type: String

        Example:
            llk_stc_config_device(oPort=port1,oDevice=device1,Ipv6RouterId=2000::1)
        """
        llk_log_begin_proc()

        if oDevice:
            sCommand = "stc::config " + oDevice
            for key in kwargs:
                sCommand = sCommand + " -" + key + " " + kwargs[key]
            self.llk_stc_send_command(sCommand)
            logger.info("Successfully configured a device")
            llk_log_end_proc()
            return oDevice
        else:
            sCommand = "stc::create EmulatedDevice -under " + str(self.oStcProject) + " -AffiliatedPort " + oPort
            #sCommand = "stc::create EmulatedDevice -under " + str(self.oStcProject) 
            for key in kwargs:
                sCommand = sCommand + " -" + key + " " + kwargs[key]
            oDevice = self.llk_stc_send_command(sCommand)
            logger.info("Successfully created device")
            llk_log_end_proc()
            return oDevice

    def llk_stc_config_device_ieee1588v2ClockConfig(self, oDevice='', oClockCofig='', **kwargs):
        """
        Create/Configure an ieee1588v2ClockConfig configuration for the device

        Args:
            oDevice(Required for creating a new ehternet interface) - Emulated device object
                Default: ""
                Type: String

            oClockCofig(Required when configuring an existing ieee1588v2ClockConfig configuration) - ieee1588v2ClockConfig object
                Default: ""
                Type: String

            kwargs(Optional):
                Active - Whether this object will be active when you call the apply function.
                    Values: TRUE, FALSE
                    Default: TRUE
                    Type: bool

                Name - A user-defined name for this object.
                    Default: "" (empty string)
                    Type: string

                AnnounceReceiptTimeout - 
                    Type: u8
                    Default: 3
                    Range: 3 - 255

                ClockAccuracy - 
                    Type: enum
                    Default: LESS_001_0US
                    Possible Values:
                    LOCAL_CLOCK_ACCURACY	
                    LESS_025_0NS	
                    LESS_100_0NS	
                    LESS_250_0NS	
                    LESS_001_0US	
                    LESS_002_5US	
                    LESS_010_0US	
                    LESS_025_0US	
                    LESS_100_0US	
                    LESS_250_0US	
                    LESS_001_0MS	
                    LESS_002_5MS	
                    LESS_010_0MS	
                    LESS_025_0MS	
                    LESS_100_0MS	
                    LESS_250_0MS	
                    LESS_001_0S	
                    LESS_010_0S	
                    GREATER_010_0S

        Return:
            oClockCofig - ieee1588v2ClockConfig object

        Example:
            1) To configure an object
                llk_stc_config_device_ethii_itf(oEthIIIf=ipv41,SourceMac=00:10:94:00:00:02)

            2) To create an object
                llk_stc_config_device_ethii_itf(oDevice=device1,SourceMac=00:10:94:00:00:02)
        """
        llk_log_begin_proc()

        if oClockCofig:
            sCommand = "stc::config " + oClockCofig
            for key in kwargs:
                sCommand = sCommand + " -" + key + " " + kwargs[key]
            self.llk_stc_send_command(sCommand)
            logger.info("Successfully configured ieee1588v2ClockConfig!")
            llk_log_end_proc()
            return oClockCofig
        else:
            sCommand = "stc::create ieee1588v2ClockConfig -under " + str(oDevice)
            for key in kwargs:
                sCommand = sCommand + " -" + key + " " + kwargs[key]
            oConf = self.llk_stc_send_command(sCommand)
            logger.info("Successfully created ieee1588v2ClockConfig!")
            llk_log_end_proc()
            return oConf

    def llk_stc_config_device_vlan_itf(self, oDevice,oVlanIf='',**kwargs):
        """
        Create/Configure a vlan interface for the device

        Args:
            oDevice(Required) - Emulated device object
                Default: ""
                Type: String

        kwargs(Required):
            Keyword list of vlan attributes to be created or modified. keywords should be of the form vlan1, pri1

            vlan -
                Default:
                Type: INTEGER (0-4093)
            pri -
                Default: 000
                Type: BITSTRING or INTEGER (0-7)

        Returns:
            N/A

        Examples:
            llk_stc_config_device_vlan_itf(oDevice=device1, vlan1=101, pri1=001, vlan2=201, pri2=010)
        """
        llk_log_begin_proc()

        lVlanItf = str.split(str(self.llk_stc_send_command("stc::get " + oDevice + " -children-vlanif")))
        iVlansConfigured = len(lVlanItf)

        #Loop through configured stc vlans.  If vlan1 exists in kwargs dict then modify.
        #Pop value from dict then repeat for remaining stc vlans

        # Modify existing vlans
        for i in range(1,iVlansConfigured+1):
            oVlanItf =  str.split(str(self.llk_stc_send_command("stc::get " + oDevice + " -children-vlanif")))[i-1]
            if 'vlan'+str(i) in kwargs.keys() and 'pri'+str(i) in kwargs.keys():
                sFormatPriKey = self._llk_stc_format_priority(kwargs.pop('pri' + str(i)))
                self.llk_stc_send_command("stc::config " + oVlanItf + " -vlanid " + kwargs.pop("vlan" + str(i)) +" -priority " + sFormatPriKey)
            elif 'vlan'+str(i) in kwargs.keys():
                self.llk_stc_send_command("stc::config " + oVlanItf + " -vlanid " + kwargs.pop("vlan" + str(i)))
            elif 'pri'+str(i) in kwargs:
                self.llk_stc_send_command("stc::config " + oVlanItf + " -priority " + kwargs.pop("pri" + str(i)))

        # Now that configured STC vlans have been modified and removed from kwargs dict, remaining kwargs will be NEW vlans.
        # Create remaining vlans
        # Check only for vlan keys
        sVlanExp = re.compile('vlan.*',re.IGNORECASE)
        for sKey in sorted(kwargs):
            if sVlanExp.match(sKey):
                sPriKey = sKey.replace("vlan","pri")
                if sPriKey in kwargs:
                    #sFormatPriKey = self._llk_stc_format_priority(kwargs.pop(sPriKey))
                    sFormatPriKey =  kwargs.pop(sPriKey)
                    self.llk_stc_send_command("stc::create VlanIf -under " + oDevice + " -vlanid " + kwargs.pop(sKey) + " -priority " + str(sFormatPriKey))
                else:
                    self.llk_stc_send_command("stc::create VlanIf -under " + oDevice + " -vlanid " + kwargs.pop(sKey))
        logger.info("Successfully configured vlan interface for the device!!")
 
        # return current vlan list
        lVlanItf = str.split(str(self.llk_stc_send_command("stc::get " + oDevice + " -children-vlanif")))

        llk_log_end_proc()

        return lVlanItf

    def llk_stc_config_device_ethii_itf(self, oDevice='', oEthIIIf='', **kwargs):
        """
        Create/Configure an Ethernet interface for the device

        Args:
            oDevice(Required for creating a new ehternet interface) - Emulated device object
                Default: ""
                Type: String

            oEthIIIf(Required when configuring an existing ehternet interface) - Ethernet interface object
                Default: ""
                Type: String

            kwargs(Optional):
                Active - Whether this object will be active when you call the apply function.
                    Values: TRUE, FALSE
                    Default: TRUE
                    Type: bool
                Authenticator - Authenticator identifier string.
                    Default: default
                    Type: string
                IfCountPerLowerIf - Number of interfaces in this interface object.
                    Default: 1
                    Type: u32
                IfRecycleCount - How many to times to increment source MAC address before returning to the starting value.
                    Default: 0
                    Type: u32
                IsLoopbackIf - Whether this is a loopback interface.
                    Values: TRUE, FALSE
                    Default: FALSE
                    Type: bool
                IsRange - Whether to use a combination of attributes to generate a range of source MAC addresses (SourceMac, SrcMacStep, SrcMacStepMask, SrcMacRepeatCount, and IfRecycleCount), or to use the SrcMacList attribute to specify the source MAC addresses.
                    Values: TRUE, FALSE
                    Default: TRUE
                    Type: bool
                Name - A user-defined name for this object.
                    Default: "" (empty string)
                    Type: string
                SourceMac - Source MAC address. Generate more than one source MAC address by using SrcMacRepeatCount, SrcMacStep, SrcMacStepMask, and IfRecycleCount.
                    Default: 00:10:94:00:00:02
                    Type: mac
                SrcMacList - A Tcl list of source MAC addresses. IsRange must be FALSE.
                    Default: 0
                    Type: mac
                SrcMacRepeatCount - Source MAC address repeat count.
                    Default: 0
                    Type: u32
                SrcMacStep - Source MAC address step value.
                    Default: 00:00:00:00:00:01
                    Type: mac
                SrcMacStepMask - Source MAC address step mask.
                    Default: 00:00:FF:FF:FF:FF
                    Type: mac
                UseDefaultPhyMac - Whether or not to use source MAC address from physical interface.
                    Values: TRUE, FALSE
                    Default: FALSE
                    Type: bool

        Return:
            oItf - Ethernet Interface object

        Example:
            1) To configure an object
                llk_stc_config_device_ethii_itf(oEthIIIf=ipv41,SourceMac=00:10:94:00:00:02)

            2) To create an object
                llk_stc_config_device_ethii_itf(oDevice=device1,SourceMac=00:10:94:00:00:02)
        """
        llk_log_begin_proc()

        if oEthIIIf:
            sCommand = "stc::config " + oEthIIIf
            for key in kwargs:
                sCommand = sCommand + " -" + key + " " + kwargs[key]
            self.llk_stc_send_command(sCommand)
            logger.info("Successfully configured Ethernet device interface")
            llk_log_end_proc()
            return oEthIIIf
        else:
            sCommand = "stc::create EthIIIf -under " + str(oDevice)
            for key in kwargs:
                sCommand = sCommand + " -" + key + " " + kwargs[key]
            oItf = self.llk_stc_send_command(sCommand)
            logger.info("Successfully created Ethernet device interface")
            llk_log_end_proc()
            return oItf

    def llk_stc_config_device_ipv4_itf(self, oDevice='', oIPv4If='', **kwargs):
        """
        Create/Configure an ipv4 interface for the device

        Args:
            oDevice(Required for creating a new ipv4 interface) - Emulated device object
                Default: ""
                Type: String

            oIPv4If(Required when configuring an existing ipv4 interface) - IPv4 interface object
                Default: ""
                Type: String

            kwargs(Optional):
                Active - Whether this object will be active when you call the apply function.
                    Values: TRUE, FALSE
                    Default: TRUE
                    Type: bool
                Address - IPv4 address.
                    Value: IPv4 address
                    Default: 192.85.1.3
                    Type: ip
                AddrList - A Tcl list of IPv4 addresses. IsRange must be FALSE.
                    Value: IPv4 address
                    Default: 0
                    Type: ip
                AddrRepeatCount - How many times to repeat the same IPv4 address before incrementing it.
                    Default: 0
                    Type: u32
                AddrResolver - Address resolver identifier.
                    Default: default
                    Type: string
                AddrStep - IPv4 address step value.
                    Value: IPv4 address
                    Default: 0.0.0.1
                    Type: ip
                AddrStepMask - IPv4 address step mask.
                    Value: IPv4 address
                    Default: 255.255.255.255
                    Type: ip
                EnableGatewayLearning - NOTE: This attribute is deprecated. It will be removed in subsequent releases, so it is recommended that you do not use it. Not used on this object.
                    Values: TRUE, FALSE
                    Default: FALSE
                    Type: bool
                Gateway - IPv4 gateway address.
                    Value: IPv4 address
                    Default: 192.85.1.1
                    Type: ip
                GatewayList - A Tcl list of IPv4 gateway addresses. IsRange must be FALSE.
                    Value: IPv4 address
                    Default: 0
                    Type: ip
                GatewayMac - Gateway MAC address.
                    Default: 00:00:01:00:00:01
                    Type: mac
                GatewayMacResolver - Gateway MAC address resolver identifier.
                    Default: default
                    Type: string
                GatewayRecycleCount - How many times to increment the IPv4 gateway address before returning to the starting value.
                    Default: 0
                    Type: u32
                GatewayRepeatCount -
                    Default: 0
                    Type: u32
                GatewayStep -
                    Value: IPv4 address
                    Default: 0.0.0.0
                    Type: ip
                IfCountPerLowerIf - Number of interfaces in this interface object.
                    Default: 1
                    Type: u32
                IfRecycleCount - How many times to increment the IPv4 address before returning to the starting value.
                    Default: 0
                    Type: u32
                IsLoopbackIf - Whether this is a loopback interface.
                    Values: TRUE, FALSE
                    Default: FALSE
                    Type: bool
                IsRange - Whether to use a combination of attributes to generate a range of addresses, or to use a list attribute (AddrList or GatewayList) to specify the addresses. The beginning address attributes are Address and Gateway.
                    Values: TRUE, FALSE
                    Default: TRUE
                    Type: bool
                Name - A user-defined name for this object.
                    Default: "" (empty string)
                    Type: string
                NeedsAuthentication - Whether this interface needs authentication.
                    Values: TRUE, FALSE
                    Default: FALSE
                    Type: bool
                PrefixLength - IPv4 address prefix length.
                    Range: 0 - 32
                    Default: 24
                    Type: u8
                ResolveGatewayMac - Whether to resolve the gateway MAC address.
                    Values: TRUE, FALSE
                    Default: TRUE
                    Type: bool
                SkipReserved - Whether to skip reserved addresses.
                    Values: TRUE, FALSE
                    Default: TRUE
                    Type: bool
                Tos - IPv4 ToS value.
                    Range: 0 - 255
                    Default: 192
                    Type: u8
                TosType -
                    Values: DIFFSERV, TOS
                    Default: TOS
                    Type: enum
                Ttl - IPv4 TTL value.
                    Range: 0 - 255
                    Default: 255
                    Type: u8
                UseIpAddrRangeSettingsForGateway -
                    Values: TRUE, FALSE
                    Default: FALSE
                    Type: bool
                UsePortDefaultIpv4Gateway - Whether to use the logical port's default IPv4 gateway.
                    Values: TRUE, FALSE
                    Default: FALSE
                    Type: bool

        Return:
            oItf - IPv4 Interface object

        Example:

            1) To configure an object
            llk_stc_config_device_ipv4_itf(oIPv4If=ipv41,Address=00:10:94:00:00:02)

            2) To create an object
            llk_stc_config_device_ipv4_itf(oDevice=device1,Address=00:10:94:00:00:02)
        """
        llk_log_begin_proc()

        if oIPv4If:
            sCommand = "stc::config " + oIPv4If
            for key in kwargs:
                sCommand = sCommand + " -" + key + " " + kwargs[key]
            self.llk_stc_send_command(sCommand)
            logger.info("Successfully configured IPv4 device interface")
            llk_log_end_proc()
            return oIPv4If
        else:
            sCommand = "stc::create Ipv4If -under " + str(oDevice)
            for key in kwargs:
                sCommand = sCommand + " -" + key + " " + kwargs[key]
            oItf = self.llk_stc_send_command(sCommand)
            logger.info("Successfully created IPv4 device interface")
            llk_log_end_proc()
            return oItf

    def llk_stc_config_device_ipv6_itf(self, oDevice='', oIPv6If='', **kwargs):
        """
        Create/Configure an ipv6 interface for the device

        Args:
            oDevice(Required for creating a new ipv6 interface) - Emulated device object
                Default: ""
                Type: String

            oIPv6If(Required when configuring an existing ipv6 interface) - IPv6 interface object
                Default: ""
                Type: String

            kwargs(Optional):
                Active - Whether this object will be active when you call the apply function.
                    Values: TRUE, FALSE
                    Default: TRUE
                    Type: bool
                Address - IPv6 address.
                    Default: 2000::2
                    Type: ipv6
                AddrList - A Tcl list of IPv6 addresses. IsRange must be FALSE.
                    Default: 0
                    Type: ipv6
                AddrRepeatCount - How many times to repeat the same IPv6 address before incrementing it.
                    Default: 0
                    Type: u32
                AddrResolver - IPv6 address resolver identifier.
                    Default: default
                    Type: string
                AddrStep - IPv6 address step value.
                    Default: 0000::1
                    Type: ipv6
                AddrStepMask - IPv6 address step mask.
                    Default: FFFF:FFFF:FFFF:FFFF:FFFF:FFFF:FFFF:FFFF
                    Type: ipv6
                AllocateEui64LinkLocalAddress -
                    Values: TRUE, FALSE
                    Default: FALSE
                    Type: bool
                EnableGatewayLearning - Whether to enable IPv6 learning for the gateway IP and MAC addresses.
                    Values: TRUE, FALSE
                    Default: FALSE
                    Type: bool
                FlowLabel - Flow label.
                    Range: 0 - 1048575
                    Default: 7
                    Type: u32
                Gateway - IPv6 gateway address.
                    Default: ::0
                    Type: ipv6
                GatewayList - A Tcl list of IPv6 gateway addresses. IsRange must be FALSE.
                    Default: 0
                    Type: ipv6
                GatewayMac - Gateway MAC address.
                    Default: 00:00:01:00:00:01
                    Type: mac
                GatewayMacResolver - Gateway MAC address resolver identifier.
                    Default: default
                    Type: string
                GatewayRecycleCount -
                    Default: 0
                    Type: u32
                GatewayRepeatCount -
                    Default: 0
                    Type: u32
                GatewayStep -
                    Default: 0000::0000
                    Type: ipv6
                HopLimit - Hop limit.
                    Range: 0 - 255
                    Default: 255
                    Type: u8
                IfCountPerLowerIf - Number of interfaces in this interface object.
                    Default: 1
                    Type: u32
                IfRecycleCount - How many times to increment the IPv6 address before returning to the starting value.
                    Default: 0
                    Type: u32
                IsLoopbackIf - Whether this is a loopback interface.
                    Values: TRUE, FALSE
                    Default: FALSE
                    Type: bool
                IsRange - Whether to use a combination of attributes to generate a range of addresses, or to use a list attribute (AddrList or GatewayList) to specify the addresses. The beginning address attributes are Address and Gateway.
                    Values: TRUE, FALSE
                    Default: TRUE
                    Type: bool
                Name - A user-defined name for this object.
                    Default: "" (empty string)
                    Type: string
                NeedsAuthentication - Whether this interface needs authentication.
                    Values: TRUE, FALSE
                    Default: FALSE
                    Type: bool
                PrefixLength - IPv6 address prefix length.
                    Range: 0 - 128
                    Default: 64
                    Type: u8
                ResolveGatewayMac - Whether to resolve gateway MAC address.
                    Values: TRUE, FALSE
                    Default: TRUE
                    Type: bool
                TrafficClass - Traffic class.
                    Range: 0 - 255
                    Default: 0
                    Type: u8
                UseIpAddrRangeSettingsForGateway -
                    Values: TRUE, FALSE
                    Default: FALSE
                    Type: bool
                UsePortDefaultIpv6Gateway - Whether to use the logical port's default IPv6 gateway.
                    Values: TRUE, FALSE
                    Default: FALSE
                    Type: bool

        Return:
            oItf - IPv6 Interface object

        Example:
            1) To configure an object
            llk_stc_config_device_ipv6_itf(oIPv6If=ipv61,Address=2000::2)

            2) To create an object
            llk_stc_config_device_ipv6_itf(oDevice=device1,Address=2000::2)
        """

        llk_log_begin_proc()

        if oIPv6If:
            sCommand = "stc::config " + oIPv6If
            for key in kwargs:
                sCommand = sCommand + " -" + key + " " + kwargs[key]
            self.llk_stc_send_command(sCommand)
            logger.info("Successfully configured IPv6 device interface")
            llk_log_end_proc()
            return oIPv6If
        else:
            sCommand = "stc::create Ipv6If -under " + str(oDevice)
            for key in kwargs:
                sCommand = sCommand + " -" + key + " " + kwargs[key]
            oItf = self.llk_stc_send_command(sCommand)
            logger.info("Successfully created IPv6 device interface")
            llk_log_end_proc()
            return oItf

    def llk_stc_config_device_ipv6_itf2(self, oDevice='', oIPv6If2='', **kwargs):
        """
        Create/Configure an ipv6 interface for the device

        Return:
            oItf - IPv6 Interface object

        Example:
            1) To configure an object
            llk_stc_config_device_ipv6_itf(oIPv6If=ipv61,Address=2000::2)

            2) To create an object
            llk_stc_config_device_ipv6_itf(oDevice=device1,Address=2000::2)
        """

        llk_log_begin_proc()

        if oIPv6If2:
            sCommand = "stc::config " + oIPv6If2
            for key in kwargs:
                sCommand = sCommand + " -" + key + " " + kwargs[key]
            self.llk_stc_send_command(sCommand)
            logger.info("Successfully configured IPv6 device interface")
            llk_log_end_proc()
            return oIPv6If2
        else:
            sCommand = "stc::create Ipv6If -under " + str(oDevice)
            for key in kwargs:
                sCommand = sCommand + " -" + key + " " + kwargs[key]
            oItf = self.llk_stc_send_command(sCommand)
            logger.info("Successfully created IPv6 device interface")
            llk_log_end_proc()
            return oItf


    def llk_stc_config_device_ppp_itf(self, oDevice='', oPppIf='', **kwargs):
        """
        Create/Configure a ppp interface for the device
        """
        llk_log_begin_proc()

        if oPppIf:
            sCommand = "stc::config " + oPppIf
            for key in kwargs:
                sCommand = sCommand + " -" + key + " " + kwargs[key]
            self.llk_stc_send_command(sCommand)
            logger.info("Successfully configured Ppp device interface")
            llk_log_end_proc()
            return oPppIf
        else:
            sCommand = "stc::create PppIf -under " + str(oDevice)
            for key in kwargs:
                sCommand = sCommand + " -" + key + " " + kwargs[key]
            oItf = self.llk_stc_send_command(sCommand)
            logger.info("Successfully created Ppp device interface")
            llk_log_end_proc()
            return oItf


    def llk_stc_config_device_pppoe_itf(self, oDevice='', oPppoeIf='', **kwargs):
        """
        Create/Configure a pppoe interface for the device

        """
        llk_log_begin_proc()

        if oPppoeIf:
            sCommand = "stc::config " + oPppoeIf
            for key in kwargs:
                sCommand = sCommand + " -" + key + " " + kwargs[key]
            self.llk_stc_send_command(sCommand)
            logger.info("Successfully configured Pppoe device interface")
            llk_log_end_proc()
            return oPppoeIf
        else:
            sCommand = "stc::create PppoeIf -under " + str(oDevice)
            for key in kwargs:
                sCommand = sCommand + " -" + key + " " + kwargs[key]
            oItf = self.llk_stc_send_command(sCommand)
            logger.info("Successfully created Pppoe device interface")
            llk_log_end_proc()
            return oItf





    def llk_stc_config_device_dhcpv4_itf(self, oDevice='', oDHCPv4If='', **kwargs):
        """
        Create/Configure an dhcpv4 interface for the device

        Args:
            oDevice(Required for creating a new dhcpv4 interface) - Emulated device object
                Default: ""
                Type: String

            oDHCPv4If(Required when configuring an existing dhcpv4 interface) - DHCPv4 interface object
                Default: ""
                Type: String

            kwargs(Optional):
                Active - Whether this object will be active when you call the apply function.
                    Values: TRUE, FALSE
                    Default: TRUE
                    Type: bool
                CircuitId - Specify the Circuit ID field in the message sent by the relay agent. Use wildcard characters to make each circuit ID unique.
                    Value: 1 - 128characters
                    Default: circuitId_@p
                    Type: string
                ClientRelayAgent - Not exposed in the GUI.
                    Values: TRUE, FALSE
                    Default: FALSE
                    Type: bool
                DefaultHostAddrPrefixLength - Default prefix length for the host address.
                    Range: 0 - 32
                    Default: 24
                    Type: u8
                DNAv4DestIp - DNAv4 destination IP address.
                    Value: IPv4 address
                    Default: 192.85.1.1
                    Type: ip
                DNAv4DestMac - DNAv4 destination MAC address.
                    Default: 00-10-01-00-00-01
                    Type: mac
                EnableArpServerId - Enable or disable ARP on the Server ID.
                    Values: TRUE, FALSE
                    Default: FALSE
                    Type: bool
                EnableAutoRetry - Enable auto retry. Auto retry will retry sessions that fail to initially come up.
                    Values: TRUE, FALSE
                    Default: FALSE
                    Type: bool
                EnableCircuitId - Enable the circuit ID sub-option in DHCP messages.
                    Values: TRUE, FALSE
                    Default: FALSE
                    Type: bool
                EnableRelayAgent - Enables/disables the RFC 3046 relay agent option.
                    Values: TRUE, FALSE
                    Default: FALSE
                    Type: bool
                EnableRelayLinkSelection - Enable relay agent link selection sub-option in DHCP messages sent from emulated relay agent.
                    Values: TRUE, FALSE
                    Default: FALSE
                    Type: bool
                EnableRelayServerIdOverride - Enable relay agent server identifier override sub-option in DHCP messages sent from emulated relay agent.
                    Values: TRUE, FALSE
                    Default: FALSE
                    Type: bool
                EnableRelayVPNID - Enable relay agent virtual subnet selection sub-option in DHCP messages sent from emulated relay agent.
                    Values: TRUE, FALSE
                    Default: FALSE
                    Type: bool
                EnableRemoteId - Enable remote ID sub-option in DHCP messages sent from emulated relay agent.
                    Values: TRUE, FALSE
                    Default: FALSE
                    Type: bool
                EnableRouterOption - Enable the router option (option 3) specified in RFC 2132.
                    Values: TRUE, FALSE
                    Default: FALSE
                    Type: bool
                HostName - Unique hostname of emulated client. Use wildcard characters to make each hostname unique.
                    Value: 1 - 32characters
                    Default: client_@p-@b-@s
                    Type: string
                Ipv4Tos - Provides an indication of the quality of service wanted.
                    Default: 192
                    Type: u8
                Name - A user-defined name for this object.
                    Default: "" (empty string)
                    Type: string
                OptionList - A list of Option 55 numbers for the DHCP request messages on each session block.
                    Default: 1
                    Type: u8
                RelayAgentIpv4Addr - Relay agent message source IP address.
                    Value: IPv4 address
                    Default: 0.0.0.0
                    Type: ip
                RelayAgentIpv4AddrMask - IP Mask to apply to Relay Local IP address.
                    Value: IPv4 address
                    Default: 255.255.0.0
                    Type: ip
                RelayAgentIpv4AddrStep - IP Step to be applied to the Relay Local IP address.
                    Value: IPv4 address
                    Default: 0.0.0.1
                    Type: ip
                RelayClientMacAddrMask - MAC mask that will be applied to the Relay Client MAC address.
                    Default: 00-00-00-ff-ff-ff
                    Type: mac
                RelayClientMacAddrStart - Starting value for the MAC address.
                    Default: 00-10-01-00-00-01
                    Type: mac
                RelayClientMacAddrStep - MAC step that will be applied to the Relay Client MAC address.
                    Default: 00-00-00-00-00-01
                    Type: mac
                RelayLinkSelection - Link selection field for the message sent by the relay agent.
                    Value: IPv4 address
                    Default: 192.85.1.1
                    Type: ip
                RelayPoolIpv4Addr - Number of Relay Agent networks.
                    Value: IPv4 address
                    Default: 0.0.0.0
                    Type: ip
                RelayPoolIpv4AddrStep - Relay pool IPv4 address step.
                    Value: IPv4 address
                    Default: 0.0.1.0
                    Type: ip
                RelayServerIdOverride - Server identifier override field for the message sent by the relay agent.
                    Value: IPv4 address
                    Default: 192.85.1.1
                    Type: ip
                RelayServerIpv4Addr - Relay agent message destination IP address.
                    Value: IPv4 address
                    Default: 0.0.0.0
                    Type: ip
                RelayServerIpv4AddrStep - IP Step to be applied to the Relay Server IP address.
                    Value: IPv4 address
                    Default: 0.0.0.1
                    Type: ip
                RemoteId - Remote ID field for the message sent by the relay agent.
                    Value: 1 - 128characters
                    Default: remoteId_@p-@b-@s
                    Type: string
                RetryAttempts - Number of times to retry the session after the initial failure (each retry will use a new transaction ID).
                    Range: 1 - 4294967295
                    Default: 4
                    Type: u32
                UseBroadcastFlag - Enable/disable broadcast bit in DHCP control plane packets.
                    Values: TRUE, FALSE
                    Default: TRUE
                    Type: bool
                UseClientMacAddrForDataplane - Use client MAC address for the data plane.
                    Values: TRUE, FALSE
                    Default: FALSE
                    Type: bool
                UsePartialBlockState - Flag indicating partial block state as used.
                    Values: TRUE, FALSE
                    Default: FALSE
                    Type: bool
                VPNId - An identifier of virtual subnet selection information
                    Value: 1 - 128characters
                    Default: spirent_@p
                    Type: string
                VPNType - Type of VPN-ID
                    Values: NVT_ASCII, RFC_2685
                    Default: NVT_ASCII
                    Type: enum

        Return:
            oItf - DHCPv4 Interface object

        Example:
            1) To configure an object
            llk_stc_config_device_dhcpv4_itf(oDHCPv4If=dhcpv41,Active=True)

            2) To create an object
            llk_stc_config_device_dhcpv4_itf(oDevice=device1,Address=2000::2)
        """

        llk_log_begin_proc()

        if oDHCPv4If:
            sCommand = "stc::config " + oDHCPv4If
            for key in kwargs:
                sCommand = sCommand + " -" + key + " " + kwargs[key]
            self.llk_stc_send_command(sCommand)
            logger.info("Successfully configured DHCPv4 device interface")
            llk_log_end_proc()
            return oDHCPv4If
        else:
            sCommand = "stc::create Dhcpv4BlockConfig -under " + str(oDevice)
            for key in kwargs:
                sCommand = sCommand + " -" + key + " " + kwargs[key]
            oItf = self.llk_stc_send_command(sCommand)
            logger.info("Successfully created DHCPv4 device interface")
            llk_log_end_proc()
            return oItf

    def llk_stc_config_device_dhcpv4_server_itf(self, oDevice='', oDHCPv4ServerIf='', **kwargs):
        """
        Create/Configure a dhcpv4 server interface for the device

        Args:
            oDevice(Required for creating a new dhcpv4 server interface) - Emulated device object
                Default: ""
                Type: String

            oDHCPv4ServerIf(Required when configuring an existing dhcpv4 server interface) - DHCPv4 server interface object
                Default: ""
                Type: String

            kwargs(Optional):
                Active - Whether this object will be active when you call the apply function.
                    Values: TRUE, FALSE
                    Default: TRUE
                    Type: bool
                AssignStrategy - The strategy that server choose address pools which are used for assign address.
                    Values: CIRCUIT_ID, GATEWAY, LINK_SELECTION, POOL_BY_POOL, REMOTE_ID, VPN_ID
                    Default: GATEWAY
                    Type: enum
                DeclineReserveTime - Time in seconds an address will be reserved after it is declined by the client.
                    Default: 10
                    Type: u32
                EnableOverlapAddress - Enable reuse addresses based on circuit ID.
                    Values: TRUE, FALSE
                    Default: FALSE
                    Type: bool
                HostName - Server host name.
                    Value: 1 - 32characters
                    Default: server_@p-@b-@s
                    Type: string
                Ipv4Tos - Provides an indication of the quality of service wanted.
                    Default: 192
                    Type: u8
                LeaseTime - Default lease time in seconds.
                    Default: 3600
                    Type: u32
                MinAllowedLeaseTime - Minimum allowed lease time in seconds.
                    Default: 600
                    Type: u32
                Name - A user-defined name for this object.
                    Default: "" (empty string)
                    Type: string
                OfferReserveTime - Time in seconds an address will be reserved while the server is waiting for a response for an offer.
                    Default: 10
                    Type: u32
                RebindingTimePercent - (T2) Percent of the lease time at which the client should begin the rebinding process.
                    Range: 0.0 - 200.0
                    Default: 87.5
                    Type: double
                RenewalTimePercent - (T1) Percent of the lease time at which the client should begin the renewal process.
                    Range: 0.0 - 200.0
                    Default: 50.0
                    Type: double
                UsePartialBlockState - Flag indicating partial block state as used.
                    Values: TRUE, FALSE
                    Default: FALSE
                    Type: bool

        Return:
            oItf - DHCPv4 Server Interface object

        Example:
            1) To configure an object
               llk_stc_config_device_dhcpv4_itf(oDevice=device1,RebindingTimePercent=90)

            2) To configure an object
               llk_stc_config_device_dhcpv4_itf(oDHCPv4ServerIf=dhcpv4server1,RebindingTimePercent=90)
        """
        llk_log_begin_proc()

        if oDHCPv4ServerIf:
            sCommand = "stc::config " + oDHCPv4ServerIf
            for key in kwargs:
                sCommand = sCommand + " -" + key + " " + kwargs[key]
            self.llk_stc_send_command(sCommand)
            logger.info("Successfully configured DHCPv4 server device interface")
            llk_log_end_proc()
            return oDHCPv4ServerIf
        else:
            sCommand = "stc::create Dhcpv4ServerConfig -under " + str(oDevice)
            for key in kwargs:
                sCommand = sCommand + " -" + key + " " + kwargs[key]
            oItf = self.llk_stc_send_command(sCommand)
            logger.info("Successfully created DHCPv4 server device interface")
            llk_log_end_proc()
            return oItf

    def llk_stc_config_dhcpv4_options(self,oDhcpServerDefaultPoolConfig='',**kwargs):
        """
        Create the option of dhcpv4 server

        Args:
            oDhcpServerDefaultPoolConfig(Required for creating a new dhcpv4 server option) - DHCPv4 server default pool config object

            kwargs:
                EnableWildcards - Expand wildcards in the message option.
                    Type: bool
                    Default: FALSE
                    Possible Values:
                    Value 	Description
                    TRUE    Wildcards in the message option will be substituted for values.
                    FALSE   Wildcards in the message option will not be substituted for values.
                HexValue - Indicate the string should/should not be treated as hexadecimal.
                    Type: bool
                    Default: FALSE
                    Possible Values:
                    Value 	Description
                    TRUE    String is in hexadecimal format.
                    FALSE   String is not in hexadecimal format.
                MsgType - Include the message option in OFFER, ACK, or NAK.
                    Type: enum
                    Default: OFFER|ACK
                    Possible Values:
                    Value 	Description
                    OFFER 	Include the message option in the OFFER message.
                    ACK 	Include the message option in the ACK message.
                    NAK 	Include the message option in the NAK message.
                OptionType - Option value.
                    Type: u8
                    Default: 0
                    Range: 0 - 255
                Payload - Specifies the ASCII message option. Wildcards can be used.
                    Type: string
                    Default: "" (empty string)     
        Return:
            oItf - Dhcpv4ServerMsgOption object

        Example:
            1) llk_stc_config_dhcpv4_options(self,oDhcpServerDefaultPoolConfig=server1,msgType=OFFER,HexValue=FALSE,optionType=66)
        """      

        llk_log_begin_proc()

        if oDhcpServerDefaultPoolConfig:
            sCommand = "stc::create Dhcpv4ServerMsgOption -under " + oDhcpServerDefaultPoolConfig
            for key in kwargs:
                sCommand = sCommand + " -" + key + " " + kwargs[key]
            oDhcpOption = self.llk_stc_send_command(sCommand)
            logger.debug("Successfully configured DHCPv4 option:%s" % str(kwargs))
            llk_log_end_proc()
            return oDhcpOption

    def llk_stc_config_dhcpv4_server_default_pool(self, oDhcpServerConfig='', oDhcpServerDefaultPoolConfig='',**kwargs):
        """
        Create/Configure default server pool for the dhcpv4 device interface.

        Args:
            oDhcpServerConfig(Required for creating a new dhcpv4 server config) - DHCPv4 server config object

            oDhcpServerDefaultPoolConfig(Required when configuring an existing dhcpv4 default server pool) - DHCPv4 default server pool object
                Default: ""
                Type: String

            kwargs:
                Active - Whether this object will be active when you call the apply function.
                    Values: TRUE, FALSE
                    Default: TRUE
                    Type: bool
                AddrIncrement - Pool address network increment.
                    Default: 1
                    Type: u32
                AddrIncrementPerRouter - NOTE: This attribute is deprecated. It will be removed in subsequent releases, so it is recommended that you do not use it. Per router increment.
                    Default: 0
                    Type: u32
                CircuitId - Generate a list of circuit ID.
                    Value: 1 - 128characters
                    Default: circuitId_@p
                    Type: string
                CircuitIdCount - Maximum circuit ID count.
                    Default: 1
                    Type: u32
                DomainName - Domain name (option 15).
                    Default: "" (empty string)
                    Type: string
                DomainNameServerList - Domain name servers (option 6).
                    Value: IPv4 address
                    Default: 0
                    Type: ip
                EnablePoolAddrPrefix - Enable this pool to use a customized prefix length.
                    Values: TRUE, FALSE
                    Default: FALSE
                    Type: bool
                HostAddrCount - Number of addresses in a pool.
                    Default: 254
                    Type: u32
                HostAddrPrefixLength - Customized prefix length value.
                    Range: 0 - 32
                    Default: 24
                    Type: u8
                HostAddrStep - Pool host address step.
                    Value: IPv4 address
                    Default: 0.0.0.1
                    Type: ip
                LimitHostAddrCount - Limit host address count.
                    Values: TRUE, FALSE
                    Default: FALSE
                    Type: bool
                Name - A user-defined name for this object.
                    Default: "" (empty string)
                    Type: string
                NetworkCount - Number of pools.
                    Default: 1
                    Type: u32
                PrefixLength - Pool prefix length.
                    Range: 0 - 32
                    Default: 24
                    Type: u8
                RemoteId - Generate a list of remote ID.
                    Value: 1 - 128characters
                    Default: remoteId_@p-@b-@s
                    Type: string
                RemoteIdCount - Maximum remote ID count.
                    Range: 1 - 20
                    Default: 1
                    Type: u32
                RouterList - Router addresses (option 3).
                    Value: IPv4 address
                    Default: 0
                    Type: ip
                StartIpList - Pool starting IP address.
                    Value: IPv4 address
                    Default: 192.0.1.0
                    Type: ip
                VPNId - Generate a list of VPN ID.
                    Value: 1 - 128characters
                    Default: spirent_@p
                    Type: string
                VPNIdCount - Maximum VPN ID count.
                    Default: 1
                    Type: u32
                VPNType - Type of VPN-ID
                    Values: NVT_ASCII, RFC_2685
                    Default: NVT_ASCII
                    Type: enum

        Return:
            oItf - DHCPv4 Server Interface object

        Example:
            1) llk_stc_config_dhcpv4_server_default_pool(oDevice=device1,oDHCPv4ServerIf=dhcpv4server1,StartIpList=192.85.1.0,Active=True)
        """

        llk_log_begin_proc()

        if oDhcpServerDefaultPoolConfig:
            sCommand = "stc::config " + oDhcpServerDefaultPoolConfig
            for key in kwargs:
                sCommand = sCommand + " -" + key + " " + kwargs[key]
            self.llk_stc_send_command(sCommand)
            logger.info("Successfully configured DHCPv4 default server pool")
            llk_log_end_proc()
            return oDhcpServerDefaultPoolConfig
        else:
            oDhcpServerDefaultPoolConfig = self.llk_stc_send_command("stc::get " + oDhcpServerConfig + " -children-dhcpv4serverdefaultpoolconfig")
            sCommand = "stc::config " + oDhcpServerDefaultPoolConfig
            for key in kwargs:
                sCommand = sCommand + " -" + key + " " + kwargs[key]
            self.llk_stc_send_command(sCommand)
            logger.info("Successfully created DHCPv4 default server pool interface")
            llk_log_end_proc()
            return oDhcpServerDefaultPoolConfig

    def llk_stc_config_dhcpv4_server_pool(self, oDhcpServerConfig='', oDhcpServerPoolConfig='', **kwargs):
        """
        Configure server pool for the dhcpv4 device.

        Args:
            oDhcpServerConfig(Required for creating a new dhcpv4 server config) - DHCPv4 server config object

            oDhcpServerPoolConfig(Required when configuring an existing dhcpv4 default server pool) - DHCPv4 default server pool object
                Default: ""
                Type: String

            kwargs:
                Active - Whether this object will be active when you call the apply function.
                    Values: TRUE, FALSE
                    Default: TRUE
                    Type: bool
                AddrIncrement - Pool address network increment.
                    Default: 1
                    Type: u32
                AddrIncrementPerRouter - NOTE: This attribute is deprecated. It will be removed in subsequent releases, so it is recommended that you do not use it. Per router increment.
                    Default: 0
                    Type: u32
                CircuitId - Generate a list of circuit ID.
                    Value: 1 - 128characters
                    Default: circuitId_@p
                    Type: string
                CircuitIdCount - Maximum circuit ID count.
                    Default: 1
                    Type: u32
                DomainName - Domain name (option 15).
                    Default: "" (empty string)
                    Type: string
                DomainNameServerList - Domain name servers (option 6).
                    Value: IPv4 address
                    Default: 0
                    Type: ip
                EnablePoolAddrPrefix - Enable this pool to use a customized prefix length.
                    Values: TRUE, FALSE
                    Default: FALSE
                    Type: bool
                HostAddrCount - Number of addresses in a pool.
                    Default: 254
                    Type: u32
                HostAddrPrefixLength - Customized prefix length value.
                    Range: 0 - 32
                    Default: 24
                    Type: u8
                HostAddrStep - Pool host address step.
                    Value: IPv4 address
                    Default: 0.0.0.1
                    Type: ip
                LimitHostAddrCount - Limit host address count.
                    Values: TRUE, FALSE
                    Default: FALSE
                    Type: bool
                Name - A user-defined name for this object.
                    Default: "" (empty string)
                    Type: string
                NetworkCount - Number of pools.
                    Default: 1
                    Type: u32
                PrefixLength - Pool prefix length.
                    Range: 0 - 32
                    Default: 24
                    Type: u8
                RemoteId - Generate a list of remote ID.
                    Value: 1 - 128characters
                    Default: remoteId_@p-@b-@s
                    Type: string
                RemoteIdCount - Maximum remote ID count.
                    Range: 1 - 20
                    Default: 1
                    Type: u32
                RouterList - Router addresses (option 3).
                    Value: IPv4 address
                    Default: 0
                    Type: ip
                StartIpList - Pool starting IP address.
                    Value: IPv4 address
                    Default: 192.0.1.0
                    Type: ip
                VPNId - Generate a list of VPN ID.
                    Value: 1 - 128characters
                    Default: spirent_@p
                    Type: string
                VPNIdCount - Maximum VPN ID count.
                    Default: 1
                    Type: u32
                VPNType - Type of VPN-ID
                    Values: NVT_ASCII, RFC_2685
                    Default: NVT_ASCII
                    Type: enum
        Return:
            oItf - DHCPv4 Server Pool object

        Example:
            llk_stc_config_dhcpv4_server_default_pool(oDhcpServerConfig=dhcpserverconfig1,oDhcpServerPoolConfig=dhcpserverpoolconfig1,StartIpList=192.85.1.0,Active=True)
        """

        llk_log_begin_proc()

        if oDhcpServerPoolConfig:
            sCommand = "stc::config " + oDhcpServerPoolConfig
            for key in kwargs:
                sCommand = sCommand + " -" + key + " " + kwargs[key]
            self.llk_stc_send_command(sCommand)
            logger.info("Successfully configured DHCPv4 default server pool")
            llk_log_end_proc()
            return oDhcpServerPoolConfig
        else:
            sCommand = "stc::create Dhcpv4ServerPoolConfig -under " + str(oDhcpServerConfig)
            for key in kwargs:
                sCommand = sCommand + " -" + key + " " + kwargs[key]
            oItf = self.llk_stc_send_command(sCommand)
            logger.info("Successfully created DHCPv4 server pool interface")
            llk_log_end_proc()
            return oItf



    def llk_stc_config_device_igmp_host_itf(self, oDevice='', oIGMPHostIf='', **kwargs):
        """
        Create/Configure a dhcpv4 server interface for the device

        """
        llk_log_begin_proc()

        if oIGMPHostIf:
            sCommand = "stc::config " + oIGMPHostIf
            ##print ('sCommand1:', sCommand)
            for key in kwargs:
                sCommand = sCommand + " -" + key + " " + kwargs[key]
                ##print ('sCommand2:', sCommand)
            self.llk_stc_send_command(sCommand)
            logger.info("Successfully configured IGMP host interface")
            llk_log_end_proc()
            ##print ('oIGMPHostIf:', oIGMPHostIf)
            return oIGMPHostIf
        else:
            sCommand = "stc::create IgmpHostConfig -under " + str(oDevice)
            ##print ('sCommand3:', sCommand)
            for key in kwargs:
                sCommand = sCommand + " -" + key + " " + kwargs[key]
                ##print ('sCommand4:', sCommand)
            oItf = self.llk_stc_send_command(sCommand)
            logger.info("Successfully created IGMP host interface")
            logger.debug('sCommand4:', oItf)
            llk_log_end_proc()
            ##print ('IgmpHostConfig:', oItf)
            return oItf
    """
    def llk_stc_config_igmp_groupmembership(self, oIgmpHostConfig='', oIgmpGroupMembership='', **kwargs):
        llk_log_begin_proc()

        if oIgmpGroupMembership:
            sCommand = "stc::config " + oIgmpGroupMembership
            ##print ('sCommand5:', sCommand)
            for key in kwargs:
                sCommand = sCommand + " -" + key + " " + kwargs[key]
                ##print ('sCommand6:', sCommand)
            self.llk_stc_send_command(sCommand)
            logger.info("Successfully configured IGMP GroupMembership")
            llk_log_end_proc()
            ##print ('oIgmpGroupMembership1:', oIgmpGroupMembership)
            return oIgmpGroupMembership
        else:
            oIgmpGroupMembership = self.llk_stc_send_command(
                "stc::create IgmpGroupMembership -under " +oIgmpHostConfig)
            oipv4networkblock1 = self.llk_stc_send_command(
                "stc::get " + oIgmpGroupMembership + " -children-ipv4networkblock")
            sCommand = "stc::config " + oipv4networkblock1
            ##print ('sCommand7:', sCommand)
            for key in kwargs:
                sCommand = sCommand + " -" + key + " " + kwargs[key]
                ##print ('sCommand8:', sCommand)
            self.llk_stc_send_command(sCommand)
            logger.info("Successfully created IGMP GroupMembership")
            logger.debug('sCommand8:', sCommand)
            llk_log_end_proc()
            ##print ('oIgmpGroupMembership2:', oIgmpGroupMembership)
            return oIgmpGroupMembership
    """

    def llk_stc_config_igmp_groupmembership(self, oIgmpHostConfig='', oIgmpGroupMembership='', **kwargs):
        llk_log_begin_proc()

        if oIgmpGroupMembership:
            sCommand = "stc::config " + oIgmpGroupMembership
            ##print ('sCommand5:', sCommand)
            for key in kwargs:
                sCommand = sCommand + " -" + key + " " + kwargs[key]
                ##print ('sCommand6:', sCommand)
            self.llk_stc_send_command(sCommand)
            logger.info("Successfully configured IGMP GroupMembership")
            llk_log_end_proc()
            ##print ('oIgmpGroupMembership1:', oIgmpGroupMembership)
            return oIgmpGroupMembership
        else:
            oIgmpGroupMembership = self.llk_stc_send_command(
                "stc::create IgmpGroupMembership -under " +oIgmpHostConfig)
            #oipv4networkblock1 = self.llk_stc_send_command(
            #    "stc::get " + oIgmpGroupMembership + " -children-ipv4networkblock")
            sCommand = "stc::config " + oIgmpGroupMembership
            ##print ('sCommand7:', sCommand)
            for key in kwargs:
                sCommand = sCommand + " -" + key + " " + kwargs[key]
                ##print ('sCommand8:', sCommand)
            self.llk_stc_send_command(sCommand)
            logger.info("Successfully created IGMP GroupMembership")
            llk_log_end_proc()
            ##print ('oIgmpGroupMembership2:', oIgmpGroupMembership)
            return oIgmpGroupMembership


    def llk_stc_config_multicast_source(self, oIgmpGroupMembership='', oipv4networkblock='', **kwargs):
        llk_log_begin_proc()

        if oipv4networkblock:
            sCommand = "stc::config " + oipv4networkblock
            for key in kwargs:
                sCommand = sCommand + " -" + key + " " + kwargs[key]
                ##print ('sCommand6:', sCommand)
            self.llk_stc_send_command(sCommand)
            logger.info("Successfully configured multicast source")
            llk_log_end_proc()
            ##print ('oIgmpGroupMembership1:', oIgmpGroupMembership)
            return oipv4networkblock
        else:
            oipv4networkblock = self.llk_stc_send_command(
                "stc::get " + oIgmpGroupMembership + " -children-ipv4networkblock")
            sCommand = "stc::config " + oipv4networkblock
            ##print ('sCommand7:', sCommand)
            for key in kwargs:
                sCommand = sCommand + " -" + key + " " + kwargs[key]
                ##print ('sCommand8:', sCommand)
            self.llk_stc_send_command(sCommand)
            logger.info("Successfully created multicast source")
            llk_log_end_proc()
            ##print ('oipv4networkblock:', oipv4networkblock)
            return oipv4networkblock




    def llk_stc_config_ipv4group(self, oipv4group='',**kwargs):
        """
    
        """
        llk_log_begin_proc()

        if oipv4group:
            sCommand = "stc::config " + oipv4group
            ##print ('sCommand5:', sCommand)
            for key in kwargs:
                sCommand = sCommand + " -" + key + " " + kwargs[key]
                ##print ('sCommand6:', sCommand)
            self.llk_stc_send_command(sCommand)
            logger.info("Successfully configured Ipv4Group")
            llk_log_end_proc()
            ##print ('oipv4group1:', oipv4group)
            return oipv4group
        else:
            """
            oipv4group = self.llk_stc_send_command(
                "stc::create Ipv4Group -under project1" )
            """
            oipv4group = self.llk_stc_send_command(
                "stc::create Ipv4Group -under " + self.oStcProject )
            oipv4networkblock2 = self.llk_stc_send_command(
                "stc::get " + oipv4group + " -children-ipv4networkblock")
            sCommand = "stc::config " + oipv4networkblock2
            ##print ('sCommand7:', sCommand)
            for key in kwargs:
                sCommand = sCommand + " -" + key + " " + kwargs[key]
                ##print ('sCommand8:', sCommand)
            self.llk_stc_send_command(sCommand)
            logger.info("Successfully created  Ipv4Group")
            llk_log_end_proc()
            ##print ('oipv4group2:', oipv4group)
            return oipv4group

    def llk_stc_config_device_synce_itf(self, oDevice='', oSynceDeviceIf='', **kwargs):
        """
        Create/Configure a SYNCE DEVICE interface for the device

        """
        llk_log_begin_proc()

        if  oSynceDeviceIf:
            sCommand = "stc::config " + oSynceDeviceIf
            #print ('sCommand1:', sCommand)
            for key in kwargs:
                sCommand = sCommand + " -" + key + " " + kwargs[key]
                #print ('sCommand2:', sCommand)
            oItf1 = self.llk_stc_send_command(sCommand)
            logger.info("Successfully configured SYNCE DEVICE interface")
            llk_log_end_proc()
            STC_SESSION[session_name].oLlkStc.llk_stc_send_command("stc::apply")
            return oItf1
        else:
            sCommand = "stc::create SyncEthDeviceConfig -under " + str(oDevice)
            #print ('sCommand3:', sCommand)
            for key in kwargs:
                sCommand = sCommand + " -" + key + " " + kwargs[key]
                #print ('sCommand4:', sCommand)
            oItf = self.llk_stc_send_command(sCommand)
            logger.info("Successfully created SYNCE DEVICE interface")
            llk_log_end_proc()
            #print ('Dot1xSupplicantBlockConfig :', oItf)
            return oItf


    def llk_stc_config_synceth_port(self,oPort='', oSyncePortIf='', **kwargs):
        llk_log_begin_proc()

        if oSyncePortIf:
            sCommand = "stc::config " + oSyncePortIf
            #print ('sCommand5:', sCommand)
            for key in kwargs:
                sCommand = sCommand + " -" + key + " " + kwargs[key]
                #print ('sCommand6:', sCommand)
            self.llk_stc_send_command(sCommand)
            logger.info("Successfully configured SyncePortIf")
            llk_log_end_proc()
            #print ('oMd5Config1:', oMd5Config)
            return oSyncePortIf
        else:
            sCommand ="stc::create SyncEthPortConfig -under " + oPort
            #print ('sCommand7:', sCommand)
            for key in kwargs:
                sCommand = sCommand + " -" + key + " " + kwargs[key]
                #print ('sCommand8:', sCommand)
            oItf = self.llk_stc_send_command(sCommand)
            logger.info("Successfully created SyncePortIf")
            llk_log_end_proc()
            return oItf



    def llk_stc_SubscribedGroups_itf(self,sIgmpGroupMembership='', sipv4group=''):
        """
        """
        llk_log_begin_proc()
        self.llk_stc_send_command("stc::config " + sIgmpGroupMembership + " -SubscribedGroups-targets " + sipv4group)
        llk_log_info("Succesfully stacked the interfaces(2)")
        llk_log_end_proc()


    def llk_stc_AffiliationPort_itf(self, oDevice, oItfType):
        """
        """
        llk_log_begin_proc()
        self.llk_stc_send_command("stc::config " + oDevice + " -AffiliationPort-targets " + oItfType, apply=False)
        llk_log_debug("Succesfully configured the AffiliationPort")
        llk_log_end_proc()


    def llk_stc_config_device_pppoe_client_itf(self, oDevice='', oPppoeClientIf='', **kwargs):
        """
        Create/Configure a PPPOE client interface for the device

        """
        llk_log_begin_proc()

        if oPppoeClientIf:
            sCommand = "stc::config " + oPppoeClientIf
            print ('sCommand1:', sCommand)
            for key in kwargs:
                sCommand = sCommand + " -" + key + " " + kwargs[key]
                print ('sCommand2:', sCommand)
            self.llk_stc_send_command(sCommand)
            logger.info("Successfully configured PPPOE client interface")
            llk_log_end_proc()
            print ('oPppoeClientIf:', oPppoeClientIf)
            return oPppoeClientIf
        else:
            sCommand = "stc::create PppoeClientBlockConfig -under " + str(oDevice)
            print ('sCommand3:', sCommand)
            for key in kwargs:
                sCommand = sCommand + " -" + key + " " + kwargs[key]
                print ('sCommand4:', sCommand)
            oItf = self.llk_stc_send_command(sCommand)
            logger.info("Successfully created PPPOE client interface")
            logger.debug('sCommand4:', sCommand)
            llk_log_end_proc()
            print ('PppoeClientBlockConfig :', oItf)
            return oItf

    def llk_stc_config_device_pppoe_server_itf(self, oDevice='', oPppoeServerIf='', **kwargs):
        """
        Create/Configure a PPPOE server interface for the device

        """
        llk_log_begin_proc()

        if oPppoeServerIf:
            sCommand = "stc::config " + oPppoeServerIf
            print ('sCommand1:', sCommand)
            for key in kwargs:
                sCommand = sCommand + " -" + key + " " + kwargs[key]
                print ('sCommand2:', sCommand)
            self.llk_stc_send_command(sCommand)
            logger.info("Successfully configured PPPOE server interface")
            llk_log_end_proc()
            print ('oPppoeServerIf:', oPppoeServerIf)
            return oPppoeServerIf
        else:
            sCommand = "stc::create PppoeServerBlockConfig -under " + str(oDevice)
            print ('sCommand3:', sCommand)
            for key in kwargs:
                sCommand = sCommand + " -" + key + " " + kwargs[key]
                print ('sCommand4:', sCommand)
            oItf = self.llk_stc_send_command(sCommand)
            logger.info("Successfully created PPPOE server interface")
            logger.debug('sCommand4:', sCommand)
            llk_log_end_proc()
            print ('PppoeServerBlockConfig :', oItf)
            return oItf

    def llk_stc_config_pppoe_serverpool(self, oPppoeServerBlockConfig='', oPppoeServerIpv4PeerPool='', **kwargs):
        llk_log_begin_proc()

        if oPppoeServerIpv4PeerPool:
            sCommand = "stc::config " + oPppoeServerIpv4PeerPool
            print ('sCommand5:', sCommand)
            for key in kwargs:
                sCommand = sCommand + " -" + key + " " + kwargs[key]
                print ('sCommand6:', sCommand)
            self.llk_stc_send_command(sCommand)
            logger.info("Successfully configured pppoe server pool")
            llk_log_end_proc()
            print ('oPppoeServerIpv4PeerPool1:', oPppoeServerIpv4PeerPool)
            return oPppoeServerIpv4PeerPool
        else:
            oPppoeServerIpv4PeerPool = self.llk_stc_send_command(
                "stc::get " + oPppoeServerBlockConfig + " -children-PppoeServerIpv4PeerPool")
            sCommand = "stc::config " + oPppoeServerIpv4PeerPool
            print ('sCommand7:', sCommand)
            for key in kwargs:
                sCommand = sCommand + " -" + key + " " + kwargs[key]
                print ('sCommand8:', sCommand)
            self.llk_stc_send_command(sCommand)
            logger.info("Successfully created pppoe server pool")
            logger.debug('sCommand8:', sCommand)
            llk_log_end_proc()
            print ('oPppoeServerIpv4PeerPool2:', oPppoeServerIpv4PeerPool)
            return oPppoeServerIpv4PeerPool



    def llk_stc_config_pppoe_pppoxportcfgclient(self, oPortClient='', oPppoxPortConfig='', **kwargs):
        llk_log_begin_proc()

        if oPppoxPortConfig:
            sCommand = "stc::config " + oPppoxPortConfig
            print ('sCommand5:', sCommand)
            for key in kwargs:
                sCommand = sCommand + " -" + key + " " + kwargs[key]
                print ('sCommand6:', sCommand)
            self.llk_stc_send_command(sCommand)
            logger.info("Successfully configured PppoxPort")
            llk_log_end_proc()
            print ('oPppoxPortConfig1:', oPppoxPortConfig)
            return oPppoxPortConfig
        else:
            oPppoxPortCfgclient = self.llk_stc_send_command(
                "stc::get " + oPortClient +" -children-PppoxPortConfig")
            sCommand = "stc::config " + oPppoxPortCfgclient
            print ('sCommand7:', sCommand)
            for key in kwargs:
                sCommand = sCommand + " -" + key + " " + kwargs[key]
                print ('sCommand8:', sCommand)
            self.llk_stc_send_command(sCommand)
            logger.info("Successfully created PppoxPort")
            logger.debug('sCommand8:', sCommand)
            llk_log_end_proc()
            print ('oPppoxPortCfgclient2:', oPppoxPortCfgclient)
            return oPppoxPortCfgclient

    def llk_stc_config_device_dhcpv6_server_itf(self, oDevice='', oDhcpv6ServerIf='', **kwargs):
        """
        Create/Configure a dhcpv6 server interface for the device

        """
        llk_log_begin_proc()

        if oDhcpv6ServerIf:
            sCommand = "stc::config " + oDhcpv6ServerIf
            #print ('sCommand1:', sCommand)
            for key in kwargs:
                sCommand = sCommand + " -" + key + " " + kwargs[key]
                #print ('sCommand2:', sCommand)
            self.llk_stc_send_command(sCommand)
            logger.info("Successfully configured dhcpv6 server interface")
            llk_log_end_proc()
            #print ('oDhcpv6ServerIf:', oDhcpv6ServerIf)
            return oDhcpv6ServerIf
        else:
            sCommand = "stc::create Dhcpv6ServerConfig -under " + str(oDevice)
            #print ('sCommand3:', sCommand)
            for key in kwargs:
                sCommand = sCommand + " -" + key + " " + kwargs[key]
                #print ('sCommand4:', sCommand)
            oItf = self.llk_stc_send_command(sCommand)
            logger.info("Successfully created dhcpv6 server interface")
            #print ('sCommand4:', sCommand)
            llk_log_end_proc()
            #print ('Dhcpv6ServerConfig :', oItf)
            return oItf

    def llk_stc_config_dhcpv6_defaultprefixpool(self, oDhcpv6ServerConfig='', oDhcpv6DefaultPrefixPool='', **kwargs):
        llk_log_begin_proc()

        if oDhcpv6DefaultPrefixPool:
            sCommand = "stc::config " + oDhcpv6DefaultPrefixPool
            #print ('sCommand5:', sCommand)
            for key in kwargs:
                sCommand = sCommand + " -" + key + " " + kwargs[key]
                #print ('sCommand6:', sCommand)
            self.llk_stc_send_command(sCommand)
            logger.info("Successfully configured dhcpv6 server default prefix pool")
            llk_log_end_proc()
            #print ('oDhcpv6DefaultPrefixPool1:', oDhcpv6DefaultPrefixPool)
            return oDhcpv6DefaultPrefixPool
        else:
            oDhcpv6DefaultPrefixPool = self.llk_stc_send_command(
                "stc::get " + oDhcpv6ServerConfig + " -children-Dhcpv6ServerDefaultPrefixPoolConfig")
            sCommand = "stc::config " + oDhcpv6DefaultPrefixPool
            #print ('sCommand7:', sCommand)
            for key in kwargs:
                sCommand = sCommand + " -" + key + " " + kwargs[key]
            self.llk_stc_send_command(sCommand)
            logger.info("Successfully created dhcpv6 server default prefix pool")
            #print ('sCommand8:', sCommand)
            llk_log_end_proc()
            #print ('oDhcpv6DefaultPrefixPool2:', oDhcpv6DefaultPrefixPool)
            return oDhcpv6DefaultPrefixPool


    def llk_stc_config_dhcpv6_defaultaddrpool(self, oDhcpv6ServerConfig='', oDhcpv6DefaultAddrPool='', **kwargs):
        llk_log_begin_proc()

        if oDhcpv6DefaultAddrPool:
            sCommand = "stc::config " + oDhcpv6DefaultAddrPool
            #print ('sCommand5:', sCommand)
            for key in kwargs:
                sCommand = sCommand + " -" + key + " " + kwargs[key]
                #print ('sCommand6:', sCommand)
            self.llk_stc_send_command(sCommand)
            logger.info("Successfully configured dhcpv6 server default address pool")
            llk_log_end_proc()
            #print ('oDhcpv6DefaultAddrPool1:', oDhcpv6DefaultAddrPool)
            return oDhcpv6DefaultAddrPool
        else:
            oDhcpv6DefaultAddrPool = self.llk_stc_send_command(
                "stc::get " + oDhcpv6ServerConfig + " -children-Dhcpv6ServerDefaultAddrPoolConfig")
            sCommand = "stc::config " + oDhcpv6DefaultAddrPool
            #print ('sCommand7:', sCommand)
            for key in kwargs:
                sCommand = sCommand + " -" + key + " " + kwargs[key]
            self.llk_stc_send_command(sCommand)
            logger.info("Successfully created dhcpv6 server default address pool")
            #print ('sCommand8:', sCommand)
            llk_log_end_proc()
            #print ('oDhcpv6DefaultAddrPool2:', oDhcpv6DefaultAddrPool)
            return oDhcpv6DefaultAddrPool

    def llk_stc_config_device_dhcpv6_client_itf(self, oDevice='', oDhcpv6ClientIf='', **kwargs):
        """
        Create/Configure a dhcpv6 client interface for the device
        """
        llk_log_begin_proc()

        if oDhcpv6ClientIf:
            sCommand = "stc::config " + oDhcpv6ClientIf
            #print ('sCommand1:', sCommand)
            for key in kwargs:
                sCommand = sCommand + " -" + key + " " + kwargs[key]
                #print ('sCommand2:', sCommand)
            self.llk_stc_send_command(sCommand)
            logger.info("Successfully configured dhcpv6 client interface")
            llk_log_end_proc()
            #print ('oDhcpv6ClientIf:', oDhcpv6ClientIf)
            return oDhcpv6ClientIf
        else:
            sCommand = "stc::create Dhcpv6BlockConfig -under " + str(oDevice)
            #print ('sCommand3:', sCommand)
            for key in kwargs:
                sCommand = sCommand + " -" + key + " " + kwargs[key]
                #print ('sCommand4:', sCommand)
            oItf = self.llk_stc_send_command(sCommand)
            logger.info("Successfully created dhcpv6 client interface")
            #print ('sCommand4:', sCommand)
            llk_log_end_proc()
            #print ('Dhcpv6BlockConfig :', oItf)
            return oItf

    def llk_stc_config_device_8021x_auth_itf(self, oDevice='', o8021xClientIf='', **kwargs):
        """
        Create/Configure a 8021x client interface for the device

        """
        llk_log_begin_proc()

        if o8021xClientIf:
            sCommand = "stc::config " + o8021xClientIf
            #print ('sCommand1:', sCommand)
            for key in kwargs:
                sCommand = sCommand + " -" + key + " " + kwargs[key]
                #print ('sCommand2:', sCommand)
            self.llk_stc_send_command(sCommand)
            logger.info("Successfully configured 802.1x authentication interface")
            llk_log_end_proc()
            #print ('o8021xClientIf:', o8021xClientIf)
            return o8021xClientIf
        else:
            sCommand = "stc::create Dot1xSupplicantBlockConfig -under " + str(oDevice)
            #print ('sCommand3:', sCommand)
            for key in kwargs:
                sCommand = sCommand + " -" + key + " " + kwargs[key]
                #print ('sCommand4:', sCommand)
            oItf = self.llk_stc_send_command(sCommand)
            logger.info("Successfully created 802.1x authentication interface")
            llk_log_end_proc()
            #print ('Dot1xSupplicantBlockConfig :', oItf)
            return oItf


    def llk_stc_config_8021x_md5config(self, o8021xBlockConfig='', oMd5Config='', **kwargs):
        llk_log_begin_proc()

        if oMd5Config:
            sCommand = "stc::config " + oMd5Config
            #print ('sCommand5:', sCommand)
            for key in kwargs:
                sCommand = sCommand + " -" + key + " " + kwargs[key]
                #print ('sCommand6:', sCommand)
            self.llk_stc_send_command(sCommand)
            logger.info("Successfully configured Md5Config")
            llk_log_end_proc()
            #print ('oMd5Config1:', oMd5Config)
            return oMd5Config
        else:
            oDot1xEapMd5Config = self.llk_stc_send_command(
                "stc::get " + o8021xBlockConfig +" -children-Dot1xEapMd5Config")
            sCommand = "stc::config " + oDot1xEapMd5Config
            #print ('sCommand7:', sCommand)
            for key in kwargs:
                sCommand = sCommand + " -" + key + " " + kwargs[key]
                #print ('sCommand8:', sCommand)
            self.llk_stc_send_command(sCommand)
            logger.info("Successfully created Md5Config")
            llk_log_end_proc()
            #print ('oDot1xEapMd5Config:', oDot1xEapMd5Config)
            return oDot1xEapMd5Config

    def llk_stc_config_device_ospfv3_router_itf(self, oDevice='', kOspfv3RouterIf='', **kwargs):
        """
        Create/Configure a ospfv3 router interface for the device

        """
        llk_log_begin_proc()

        if kOspfv3RouterIf:
            sCommand = "stc::config " + kOspfv3RouterIf
            #print ('sCommand1:', sCommand)
            for key in kwargs:
                sCommand = sCommand + " -" + key + " " + kwargs[key]
            self.llk_stc_send_command(sCommand)
            logger.info("Successfully configured ospfv3 router interface")
            llk_log_end_proc()
            return kOspfv3RouterIf
        else:
            sCommand = "stc::create Ospfv3RouterConfig -under " + str(oDevice)
            for key in kwargs:
                sCommand = sCommand + " -" + key + " " + kwargs[key]
            oItf = self.llk_stc_send_command(sCommand)
            logger.info("Successfully created ospfv3 router interface")
            llk_log_end_proc()
            return oItf


    def llk_stc_config_device_ospfv2_router_itf(self, oDevice='', kOspfv2RouterIf='', **kwargs):
        """
        Create/Configure a ospfv2 router interface for the device

        """
        llk_log_begin_proc()

        if kOspfv2RouterIf:
            sCommand = "stc::config " + kOspfv2RouterIf
            #print ('sCommand1:', sCommand)
            for key in kwargs:
                sCommand = sCommand + " -" + key + " " + kwargs[key]
                #print ('sCommand2:', sCommand)
            self.llk_stc_send_command(sCommand)
            logger.info("Successfully configured ospfv2 router interface")
            llk_log_end_proc()
            #print ('kOspfv2RouterIf:', kOspfv2RouterIf)
            return kOspfv2RouterIf
        else:
            sCommand = "stc::create Ospfv2RouterConfig -under " + str(oDevice)
            #print ('sCommand3:', sCommand)
            for key in kwargs:
                sCommand = sCommand + " -" + key + " " + kwargs[key]
                #print ('sCommand4:', sCommand)
            oItf = self.llk_stc_send_command(sCommand)
            logger.info("Successfully created ospfv2 router interface")
            llk_log_end_proc()
            #print ('Ospfv2RouterConfig :', oItf)
            return oItf

    def llk_stc_config_ospfv3_externallsa(self, oOspfv3RouterConfig='', oExternalLsa='', **kwargs):
        llk_log_begin_proc()

        if oExternalLsa:
            sCommand = "stc::config " + oExternalLsa
            for key in kwargs:
                sCommand = sCommand + " -" + key + " " + kwargs[key]
            self.llk_stc_send_command(sCommand)
            logger.info("Successfully configured External Lsa")
            llk_log_end_proc()
            return oExternalLsa
        else:
            sCommand = "stc::create Ospfv3AsExternalLsaBlock -under " + oOspfv3RouterConfig
            for key in kwargs:
                sCommand = sCommand + " -" + key + " " + kwargs[key]
            oItf = self.llk_stc_send_command(sCommand)
            logger.info("Successfully created External Lsa")
            llk_log_end_proc()
            return oItf

    def llk_stc_config_ospfv3_extrouteaddr(self, oExternalLsaBlock='', oIpv6NetworkBlock='', **kwargs):
        llk_log_begin_proc()
        if oIpv6NetworkBlock:
            sCommand = "stc::config " + oIpv6NetworkBlock
            for key in kwargs:
                sCommand = sCommand + " -" + key + " " + kwargs[key]
            self.llk_stc_send_command(sCommand)
            logger.info("Successfully configured external route address")
            llk_log_end_proc()
            return oIpv6NetworkBlock
        else:
            oIpv6NetworkBlock = self.llk_stc_send_command(
                "stc::get " + oExternalLsaBlock +" -children-Ipv6NetworkBlock")
            sCommand = "stc::config " + oIpv6NetworkBlock
            for key in kwargs:
                sCommand = sCommand + " -" + key + " " + kwargs[key]
            self.llk_stc_send_command(sCommand)
            logger.info("Successfully created external route address")
            llk_log_end_proc()
            return oIpv6NetworkBlock


    def llk_stc_config_ospfv2_externallsa(self, oOspfv2RouterConfig='', oExternalLsa='', **kwargs):
        llk_log_begin_proc()

        if oExternalLsa:
            sCommand = "stc::config " + oExternalLsa
            #print ('sCommand5:', sCommand)
            for key in kwargs:
                sCommand = sCommand + " -" + key + " " + kwargs[key]
                #print ('sCommand6:', sCommand)
            self.llk_stc_send_command(sCommand)
            logger.info("Successfully configured External Lsa")
            llk_log_end_proc()
            #print ('oExternalLsa:', oExternalLsa)
            return oExternalLsa
        else:
            sCommand = "stc::create ExternalLsaBlock -under " + oOspfv2RouterConfig
            for key in kwargs:
                sCommand = sCommand + " -" + key + " " + kwargs[key]
            oItf = self.llk_stc_send_command(sCommand)
            logger.info("Successfully created External Lsa")
            llk_log_end_proc()
            return oItf

    def llk_stc_config_ospfv2_extrouteaddr(self, oExternalLsaBlock='', oIpv4NetworkBlock='', **kwargs):
        llk_log_begin_proc()

        if oIpv4NetworkBlock:
            sCommand = "stc::config " + oIpv4NetworkBlock
            #print ('sCommand5:', sCommand)
            for key in kwargs:
                sCommand = sCommand + " -" + key + " " + kwargs[key]
                #print ('sCommand6:', sCommand)
            self.llk_stc_send_command(sCommand)
            logger.info("Successfully configured external route address")
            llk_log_end_proc()
            #print ('oIpv4NetworkBlock1:', oIpv4NetworkBlock)
            return oIpv4NetworkBlock
        else:
            oIpv4NetworkBlock = self.llk_stc_send_command(
                "stc::get " + oExternalLsaBlock +" -children-Ipv4NetworkBlock")
            sCommand = "stc::config " + oIpv4NetworkBlock
            #print ('sCommand7:', sCommand)
            for key in kwargs:
                sCommand = sCommand + " -" + key + " " + kwargs[key]
                #print ('sCommand8:', sCommand)
            self.llk_stc_send_command(sCommand)
            logger.info("Successfully created external route address")
            llk_log_end_proc()
            #print ('oIpv4NetworkBlock:', oIpv4NetworkBlock)
            return oIpv4NetworkBlock

    def llk_stc_config_device_rip_router_itf(self, oDevice='', kRipRouterIf='', **kwargs):
        """
        Create/Configure a rip router interface for the device

        """
        llk_log_begin_proc()

        if kRipRouterIf:
            sCommand = "stc::config " + kRipRouterIf
            #print ('sCommand1:', sCommand)
            for key in kwargs:
                sCommand = sCommand + " -" + key + " " + kwargs[key]
                #print ('sCommand2:', sCommand)
            self.llk_stc_send_command(sCommand)
            logger.info("Successfully configured rip router interface")
            llk_log_end_proc()
            #print ('kRipRouterIf:', kRipRouterIf)
            return kRipRouterIf
        else:
            sCommand = "stc::create RipRouterConfig -under " + str(oDevice)
            #print ('sCommand3:', sCommand)
            for key in kwargs:
                sCommand = sCommand + " -" + key + " " + kwargs[key]
                #print ('sCommand4:', sCommand)
            oItf = self.llk_stc_send_command(sCommand)
            logger.info("Successfully created rip router interface")
            llk_log_end_proc()
            #print ('RipRouterConfig :', oItf)
            return oItf


    def llk_stc_config_rip_routeparams(self, oRipRouterConfig='', oRipv4RouteParams='', **kwargs):
        llk_log_begin_proc()

        if oRipv4RouteParams:
            sCommand = "stc::config " + oRipv4RouteParams
            #print ('sCommand5:', sCommand)
            for key in kwargs:
                sCommand = sCommand + " -" + key + " " + kwargs[key]
                #print ('sCommand6:', sCommand)
            self.llk_stc_send_command(sCommand)
            logger.info("Successfully configured rip route params")
            llk_log_end_proc()
            #print ('oRipv4RouteParams:', oRipv4RouteParams)
            return oRipv4RouteParams
        else:
            sCommand = "stc::create Ripv4RouteParams -under " + oRipRouterConfig
            for key in kwargs:
                sCommand = sCommand + " -" + key + " " + kwargs[key]
            oItf = self.llk_stc_send_command(sCommand)
            logger.info("Successfully created rip route params")
            llk_log_end_proc()
            return oItf

    def llk_stc_config_rip_routeaddr(self, oRipv4RouteParams='', oIpv4NetworkBlock='', **kwargs):
        llk_log_begin_proc()

        if oIpv4NetworkBlock:
            sCommand = "stc::config " + oIpv4NetworkBlock
            #print ('sCommand5:', sCommand)
            for key in kwargs:
                sCommand = sCommand + " -" + key + " " + kwargs[key]
                #print ('sCommand6:', sCommand)
            self.llk_stc_send_command(sCommand)
            logger.info("Successfully configured RIP route address")
            llk_log_end_proc()
            #print ('oIpv4NetworkBlock1:', oIpv4NetworkBlock)
            return oIpv4NetworkBlock
        else:
            oIpv4NetworkBlock = self.llk_stc_send_command(
                "stc::get " + oRipv4RouteParams +" -children-Ipv4NetworkBlock")
            sCommand = "stc::config " + oIpv4NetworkBlock
            #print ('sCommand7:', sCommand)
            for key in kwargs:
                sCommand = sCommand + " -" + key + " " + kwargs[key]
                #print ('sCommand8:', sCommand)
            self.llk_stc_send_command(sCommand)
            logger.info("Successfully created RIP route address")
            llk_log_end_proc()
            #print ('oIpv4NetworkBlock:', oIpv4NetworkBlock)
            return oIpv4NetworkBlock

    def llk_stc_config_device_bgp_router_itf(self, oDevice='', kBgpRouterIf='', **kwargs):
        """
        Create/Configure a bgp router interface for the device

        """
        llk_log_begin_proc()

        if kBgpRouterIf:
            sCommand = "stc::config " + kBgpRouterIf
            #print ('sCommand1:', sCommand)
            for key in kwargs:
                sCommand = sCommand + " -" + key + " " + kwargs[key]
                #print ('sCommand2:', sCommand)
            self.llk_stc_send_command(sCommand)
            logger.info("Successfully configured bgp router interface")
            llk_log_end_proc()
            #print ('kBgpRouterIf:', kBgpRouterIf)
            return kBgpRouterIf
        else:
            sCommand = "stc::create BgpRouterConfig -under " + str(oDevice)
            #print ('sCommand3:', sCommand)
            for key in kwargs:
                sCommand = sCommand + " -" + key + " " + kwargs[key]
                #print ('sCommand4:', sCommand)
            oItf = self.llk_stc_send_command(sCommand)
            logger.info("Successfully created bgp router interface")
            llk_log_end_proc()
            #print ('BgpRouterConfig :', oItf)
            return oItf


    def llk_stc_config_bgp_routeparams(self, oBgpRouterConfig='', oBgpIpv4RouteConfig='', **kwargs):
        llk_log_begin_proc()

        if oBgpIpv4RouteConfig:
            sCommand = "stc::config " + oBgpIpv4RouteConfig
            #print ('sCommand5:', sCommand)
            for key in kwargs:
                sCommand = sCommand + " -" + key + " " + kwargs[key]
                #print ('sCommand6:', sCommand)
            self.llk_stc_send_command(sCommand)
            logger.info("Successfully configured bgp route params")
            llk_log_end_proc()
            #print ('oBgpIpv4RouteConfig:', oBgpIpv4RouteConfig)
            return oBgpIpv4RouteConfig
        else:
            sCommand = "stc::create BgpIpv4RouteConfig -under " + oBgpRouterConfig
            for key in kwargs:
                sCommand = sCommand + " -" + key + " " + kwargs[key]
            oItf = self.llk_stc_send_command(sCommand)
            logger.info("Successfully created bgp route params")
            llk_log_end_proc()
            return oItf

    def llk_stc_config_bgp_routeaddr(self, oBgpIpv4RouteConfig='', oIpv4NetworkBlock='', **kwargs):
        llk_log_begin_proc()

        if oIpv4NetworkBlock:
            sCommand = "stc::config " + oIpv4NetworkBlock
            #print ('sCommand5:', sCommand)
            for key in kwargs:
                sCommand = sCommand + " -" + key + " " + kwargs[key]
                #print ('sCommand6:', sCommand)
            self.llk_stc_send_command(sCommand)
            logger.info("Successfully configured bgp route address")
            llk_log_end_proc()
            #print ('oIpv4NetworkBlock1:', oIpv4NetworkBlock)
            return oIpv4NetworkBlock
        else:
            oIpv4NetworkBlock = self.llk_stc_send_command(
                "stc::get " + oBgpIpv4RouteConfig +" -children-Ipv4NetworkBlock")
            sCommand = "stc::config " + oIpv4NetworkBlock
            #print ('sCommand7:', sCommand)
            for key in kwargs:
                sCommand = sCommand + " -" + key + " " + kwargs[key]
                #print ('sCommand8:', sCommand)
            self.llk_stc_send_command(sCommand)
            logger.info("Successfully created bgp route address")
            llk_log_end_proc()
            #print ('oIpv4NetworkBlock:', oIpv4NetworkBlock)
            return oIpv4NetworkBlock

    def llk_stc_config_device_eoam_node_itf(self, oDevice='',oEoamNodeIf='',**kwargs):
        """
        Create/Configure a eoam node for the device

        """
        llk_log_begin_proc()

        if oEoamNodeIf:
            sCommand = "stc::config " + oEoamNodeIf
            ##print ('sCommand1:', sCommand)
            for key in kwargs:
                sCommand = sCommand + " -" + key + " " + kwargs[key]
                ##print ('sCommand2:', sCommand)
            self.llk_stc_send_command(sCommand)
            logger.info("Successfully configured EoamNodeConfig")
            llk_log_end_proc()
            ##print ('oIGMPHostIf:', oIGMPHostIf)
            return oEoamNodeIf
        else:
            sCommand = "stc::create EoamNodeConfig -under " + oDevice
            ##print ('sCommand3:', sCommand)
            for key in kwargs:
                sCommand = sCommand + " -" + key + " " + kwargs[key]
                ##print ('sCommand4:', sCommand)
            oItf = self.llk_stc_send_command(sCommand)
            logger.info("Successfully created EoamNodeConfig")
            llk_log_end_proc()
            print ('EoamNodeConfig:', oItf)
            return oItf

    def llk_stc_config_eoammpconfig(self, oEoamMPConfig='', oEoamNodeConfig='', **kwargs):
        """
        Create/Configure a EoamMaintenancePointConfig for the device

        """
        llk_log_begin_proc()

        if oEoamMPConfig:
            sCommand = "stc::config " + oEoamMPConfig
            print ('sCommand1:', sCommand)
            for key in kwargs:
                sCommand = sCommand + " -" + key + " " + kwargs[key]
                print ('sCommand2:', sCommand)
            self.llk_stc_send_command(sCommand)
            logger.info("Successfully configured Eoam MaintenancePointConfig")
            llk_log_end_proc()
            print ('EoamMaintenancePointConfig1:', oEoamMPConfig)
            return oEoamMPConfig
        else:
            oEoamMaintenancePointConfig = self.llk_stc_send_command(
                "stc::get "+ oEoamNodeConfig + " -children-EoamMaintenancePointConfig")
            sCommand = "stc::config " + oEoamMaintenancePointConfig
            print ('sCommand3:', sCommand)
            for key in kwargs:
                sCommand = sCommand + " -" + key + " " + kwargs[key]
                print ('sCommand4:', sCommand)
            oItf = self.llk_stc_send_command(sCommand)
            logger.info("Successfully created Eoam MaintenancePointConfig")
            logger.debug('sCommand4:', oItf)
            llk_log_end_proc()
            print ('EoamMaintenancePointConfig2:', oItf)
            return oEoamMaintenancePointConfig

    def llk_stc_config_eoamgenconfig(self, oEoamGenConfig='',**kwargs):
        """
    
        """
        llk_log_begin_proc()

        if oEoamGenConfig:
            sCommand = "stc::config " + oEoamGenConfig
            ##print ('sCommand5:', sCommand)
            for key in kwargs:
                sCommand = sCommand + " -" + key + " " + kwargs[key]
                ##print ('sCommand6:', sCommand)
            self.llk_stc_send_command(sCommand)
            logger.info("Successfully configured EoamGenConfig")
            llk_log_end_proc()
            ##print ('oipv4group1:', oipv4group)
            return oEoamGenConfig
        else:
            oEoamGenConfig = self.llk_stc_send_command(
                "stc::create EoamGenConfig -under " + self.oStcProject )
            sCommand = "stc::config " + oEoamGenConfig
            ##print ('sCommand7:', sCommand)
            for key in kwargs:
                sCommand = sCommand + " -" + key + " " + kwargs[key]
                ##print ('sCommand8:', sCommand)
            self.llk_stc_send_command(sCommand)
            logger.info("Successfully created EoamGenConfig")
            llk_log_end_proc()
            ##print ('oipv4group2:', oipv4group)
            return oEoamGenConfig


     
    def llk_stc_config_eoammegconfig(self, oEoamMegConfig='',**kwargs):
        """
    
        """
        llk_log_begin_proc()

        if oEoamMegConfig:
            sCommand = "stc::config " + oEoamMegConfig
            ##print ('sCommand5:', sCommand)
            for key in kwargs:
                sCommand = sCommand + " -" + key + " " + kwargs[key]
                ##print ('sCommand6:', sCommand)
            self.llk_stc_send_command(sCommand)
            logger.info("Successfully configured EoamMegConfig")
            llk_log_end_proc()
            ##print ('oipv4group1:', oipv4group)
            return oEoamMegConfig
        else:
            oEoamMegConfig = self.llk_stc_send_command(
                "stc::create EoamMegConfig -under " + self.oStcProject )
            sCommand = "stc::config " + oEoamMegConfig
            ##print ('sCommand7:', sCommand)
            for key in kwargs:
                sCommand = sCommand + " -" + key + " " + kwargs[key]
                ##print ('sCommand8:', sCommand)
            self.llk_stc_send_command(sCommand)
            logger.info("Successfully created EoamMegConfig")
            llk_log_end_proc()
            ##print ('oipv4group2:', oipv4group)
            return oEoamMegConfig

    def llk_stc_config_eoam_remotemepcfg(self, oEoamMegConfig='',oEoamRemoteMepCfg='',**kwargs):
        """
    
        """
        llk_log_begin_proc()

        if oEoamRemoteMepCfg:
            sCommand = "stc::config " + oEoamRemoteMepCfg
            ##print ('sCommand5:', sCommand)
            for key in kwargs:
                sCommand = sCommand + " -" + key + " " + kwargs[key]
                ##print ('sCommand6:', sCommand)
            self.llk_stc_send_command(sCommand)
            logger.info("Successfully configured EoamMegConfig")
            llk_log_end_proc()
            ##print ('oipv4group1:', oipv4group)
            return oEoamRemoteMepCfg
        else:
            oEoamRemoteMegEndPointConfig = self.llk_stc_send_command(
                "stc::create EoamRemoteMegEndPointConfig -under " + oEoamMegConfig )
            sCommand = "stc::config " + oEoamRemoteMegEndPointConfig
            ##print ('sCommand7:', sCommand)
            for key in kwargs:
                sCommand = sCommand + " -" + key + " " + kwargs[key]
                ##print ('sCommand8:', sCommand)
            self.llk_stc_send_command(sCommand)
            logger.info("Successfully created EoamMegConfig")
            llk_log_end_proc()
            ##print ('oipv4group2:', oipv4group)
            return oEoamRemoteMegEndPointConfig

    def llk_stc_MegAssociation_itf(self,sEoamMtPtConfig='', sEoamMegConfig=''):
        """
        """
        llk_log_begin_proc()
        self.llk_stc_send_command("stc::config " + sEoamMtPtConfig + " -MegAssociation-targets " + sEoamMegConfig)
        llk_log_info("MP is succesfully associated to a MEG")
        llk_log_end_proc()

    def llk_stc_delete_eoamgenconfig(self):
        """
        Examples:
            llk_stc_delete_eoamgenconfig()
        """

        llk_log_begin_proc()

        keoamgenconfig=self.llk_stc_send_command("stc::get " + str(self.oStcProject)
                                  + " -children-eoamgenconfig ")
        self.llk_stc_send_command("stc::delete " + keoamgenconfig)
        llk_log_info("Succesfully delete EoamGenConfig from STC project")

        llk_log_end_proc()


    def llk_stc_start_all_devices(self):
        """
        Start all the devices

        Args:
            N/A
        Return:
            N/A

        Example:
            llk_stc_start_all_devices()
        """
        llk_log_begin_proc()
        self.llk_stc_send_command("stc::perform DevicesStartAll")
        llk_log_info("Successfully started all the devices")
        llk_log_end_proc()

    def llk_stc_stop_all_devices(self):
        """
        Stop all the devices

        Args:
            N/A

        Return:
            N/A

        Example:
            llk_stc_stop_all_devices()
        """
        llk_log_begin_proc()
        self.llk_stc_send_command("stc::perform DevicesStopAll")
        llk_log_info("Successfully stopped all the devices")
        llk_log_end_proc()

    def llk_stc_start_device(self,iDeviceUpTime=5,*lDevices):
        """
        Start specific devices

        Args:
            lDevices: List of devices to be started
                Type: List
                Default: []

            iDeviceUpTime: Time to wait for the devices to be in UP state.
                Type: Integer
                Default: 5s

        Return:
            N/A

        Example:
            ${devices} = Create_List ${device1}    ${device2}
            llk_stc_start_device(${devices},10)
        """
        llk_log_begin_proc()
        sCommand = "stc::perform DeviceStartCommand -DeviceList { "
        for oDevice in lDevices:
            sCommand = sCommand + " " + str(oDevice)
        sCommand = sCommand + " } "

        self.llk_stc_send_command(sCommand)

        sCommand = "stc::config emulateddevice1 -AffiliationPort-targets  port1 "
        """
        iTimer = 0
        iDeviceCnt = len(lDevices)
        iDeviceUpCnt = 0
        if iDeviceUpTime:
            logger.info("Waiting for devices to be up!!")
            while iTimer <= int(iDeviceUpTime):
                for oDevice in lDevices:
                    oDhcpClientObj = self.llk_stc_send_command("stc::get " + str(oDevice) + " -children-dhcpv4blockconfig")
                    if oDhcpClientObj : 
                        if self.llk_stc_verify_dhcp_client_state(oDhcpClientObj,"BOUND"):
                            iDeviceUpCnt += 1
                if iDeviceCnt == iDeviceUpCnt:
                    break
                else:
                    time.sleep(1)
                    iTimer += 1

        if iDeviceCnt == iDeviceUpCnt:
            logger.info("All the devices are UP!!")
        else:
            llk_log_end_proc()
            raise Exception("Devices are not up even after the wait time. Check the device configuration!!")
        """
        llk_log_end_proc()

    def llk_stc_stop_device(self,*lDevices):
        """
        Stop specific devices

        Args:
            lDevices: List of devices to be stopped
                Type: List
                Default: []

        Return:
            N/A

        Example:
            ${devices} = Create_List ${device1}    ${device2}
            llk_stc_stop_device(${devices})
        """
        llk_log_begin_proc()
        sCommand = "stc::perform DeviceStopCommand -DeviceList"
        sCommand = sCommand + ' { '
        for oDevice in lDevices:
            sCommand = sCommand + " " + str(oDevice)
        sCommand = sCommand + ' } '
        self.llk_stc_send_command(sCommand)
        logger.info("Devices successfully stopped!!")
        llk_log_end_proc()

    def llk_stc_start_arp_nd(self):
        """
        Start ARP/ND on all the streamblocks

        Args:
            N/A
        Return:
            N/A
        Example:
            llk_stc_start_arp_nd()
        """
        llk_log_begin_proc()
        self.llk_stc_send_command("stc::perform ArpNdStartOnAllStreamBlocks")
        llk_log_info("Successfully started arp on all the streamblocks")
        llk_log_end_proc()

    def llk_stc_retrieve_arp_nd_state(self,oStreamblock):
        """
        Retrieve ARP/ND state of a streamblock.

        Args:
            oStreamblock - Streamblock object
                Type : String
                Default : ""

        Return:
            sStreamblockState - State of the streamblock
                Type : String
                Default : ""
                Values: NEED_RESOLVE, READY

        Example:
            llk_stc_retrieve_arp_nd_state(streamblock1)
        """
        llk_log_begin_proc()
        sStreamblockState = self.llk_stc_send_command("stc::get " + oStreamblock + " -State")
        llk_log_end_proc()
        return sStreamblockState

    def llk_stc_verify_streamblock_arp_nd_state(self,oStreamblock, sDesiredStreamblockState):
        """
        Verify ARP/ND state of a streamblock.

        Args:
            oStreamblock - Streamblock object
                Type : String
                Default : ""

            sDesiredStreamblockState - Expected state of streamblock
                Type : String
                Default : ""
                Values: NEED_RESOLVE, READY

        Return:
            True or False

        Example:
            llk_stc_verify_streamblock_arp_nd_state(streamblock1,"READY")
        """
        llk_log_begin_proc()
        sStreamblockState = self.llk_stc_send_command("stc::get " + oStreamblock + " -State")
        if str(sDesiredStreamblockState) == str(sStreamblockState):
            llk_log_end_proc()
            return True
        else:
            llk_log_end_proc()
            return False

    def llk_stc_retrieve_dhcp_client_state(self,oDhcpDevice):
        """
        Retrieve state of a DHCP client device.

        Args:
            oDhcpDevice - DHCP client device object
                Type : String
                Default : ""

        Return:
            sDhcpDeviceState - DHCP client device state
                Type : String
                Default : ""
                Values: AUTORENEW, BOUND, GROUPREQ, IDLE, PENDING, REBIND, RELEASE, RENEW, REQUEST

        Example:
            llk_stc_retrieve_dhcp_client_state(dhcpdevice1)
        """
        llk_log_begin_proc()
        sDhcpDeviceState = self.llk_stc_send_command("stc::get " + oDhcpDevice + " -BlockState")
        llk_log_end_proc()
        return sDhcpDeviceState

    def llk_stc_retrieve_dhcp_server_state(self,oDhcpDevice):
        """
        Retrieve state of a DHCP server device.

        Args:
            oDhcpDevice - DHCP server device object
                Type : String
                Default : ""

        Return:
            sDhcpDeviceState - DHCP server device state
                Type : String
                Default : ""
                Values: NONE, UP

        Example:
            llk_stc_retrieve_dhcp_server_state(dhcpserverdevice1)
        """
        llk_log_begin_proc()
        sDhcpDeviceState = self.llk_stc_send_command("stc::get " + oDhcpDevice + " -ServerState")
        llk_log_end_proc()
        return sDhcpDeviceState

    def llk_stc_verify_dhcp_client_state(self,oDhcpDevice, sDesiredDhcpDeviceState):
        """
        Verify DHCP client state

        Args:
            oDhcpDevice - DHCP server device object
                Type : String
                Default : ""

            sDesiredDhcpDeviceState - Expected state of DHCP client device
                Type : String
                Default : ""
                Values: AUTORENEW, BOUND, GROUPREQ, IDLE, PENDING, REBIND, RELEASE, RENEW, REQUEST

        Return:
            True or False

        Example:
            llk_stc_verify_dhcp_client_state(oDhcpDevice=dhcpdevice1,sDesiredDhcpDeviceState='AUTORENEW')
        """
        llk_log_begin_proc()
        sDhcpDeviceState = self.llk_stc_send_command("stc::get " + oDhcpDevice + " -BlockState")
        if str(sDesiredDhcpDeviceState) == str(sDhcpDeviceState):
            llk_log_end_proc()
            return True
        else:
            llk_log_end_proc()
            return False

    def llk_stc_verify_dhcp_server_state(self,oDhcpDevice, sDesiredDhcpDeviceState):
        """
        Verify DHCP server state

        Args:
            oDhcpDevice - DHCP server device object
                Type : String
                Default : ""

            sDesiredDhcpDeviceState - Expected state of DHCP server device
                Type : String
                Default : ""
                Values: NONE, UP

        Return:
            True or False

        Example:
            llk_stc_verify_dhcp_server_state(oDhcpDevice=dhcpdevice1,sDesiredDhcpDeviceState='UP')
        """
        llk_log_begin_proc()
        sDhcpDeviceState = self.llk_stc_send_command("stc::get " + oDhcpDevice + " -ServerState")
        if str(sDesiredDhcpDeviceState) == str(sDhcpDeviceState):
            llk_log_end_proc()
            return True
        else:
            llk_log_end_proc()
            return False

    def llk_stc_device_toplevel_itf(self,oDevice, oItfType):
        """
        Configure top level interface for a device

        Args:
            oDevice - STC Device object

            oItfType - STC Device Interface type

        Return:
            N/A

        Example:
            llk_stc_device_toplevel_itf(oDevice=dhcpdevice1,oItfType=oEthIIIf)
        """
        llk_log_begin_proc()
        self.llk_stc_send_command("stc::config " + oDevice + " -TopLevelIf-targets " + oItfType, apply=False)
        llk_log_debug("Succesfully configured the top level interface")
        llk_log_end_proc()

    def llk_stc_device_primary_itf(self,oDevice, oItfType):
        """
        Configure primary level interface for a device

        Args:
            oDevice - STC Device object

            oItfType - STC Device Interface type

        Return:
            N/A

        Example:
            llk_stc_device_primary_itf(oDevice=dhcpdevice1,oItfType=oEthIIIf)
        """

        llk_log_begin_proc()
        self.llk_stc_send_command("stc::config " + oDevice + " -PrimaryIf-targets " + oItfType, apply=False)
        llk_log_debug("Succesfully configured the primary interface")
        llk_log_end_proc()

    def llk_stc_device_stackedon_itf(self,oUpperItf, oLowerItf):
        """
        Stack one interface on another

        Args:
            oUpperItf - Upper interface object

            oLowerItf - Lower interface object

        Return:
            N/A

        Example:
            llk_stc_device_stackedon_itf(oIPv4If,oEthIIIf)
        """

        llk_log_begin_proc()
        self.llk_stc_send_command("stc::config " + oUpperItf + " -StackedOnEndpoint-targets " + oLowerItf)
        llk_log_info("Succesfully stacked the interfaces")
        llk_log_end_proc()

    def llk_stc_device_uses_itf(self, oDeviceItf, oItfType):
        """
        Associate the interface to be used by the device

        Args:
            oDeviceItf - STC device interface object

            oItfType - Interface type object

        Return:
            N/A

        Example:
            llk_stc_device_uses_itf(oDevice=dhcpdevice1,oItfType=oEthIIIf)
        """

        llk_log_begin_proc()
        self.llk_stc_send_command("stc::config " + oDeviceItf + " -UsesIf-targets " + oItfType)
        llk_log_info("Succesfully associated the interface to the device")
        llk_log_end_proc()

    def llk_stc_create_device_role(self, sDeviceRole):
        """
        Create a device role.

        Args:
            sDeviceRole - Device role
            Type : String
            Default: ""

        Return:
            N/A

        Example:
            llk_stc_create_device_role("Host 1")
        """
        llk_log_begin_proc()
        sCommand = "stc::create DeviceRole -under " + self.oStcProject + " -name " + sDeviceRole
        oDeviceRole = self.llk_stc_send_command(sCommand)
        llk_log_info("Successfully created device role target")
        llk_log_end_proc()
        return oDeviceRole

    def llk_stc_associate_device_role(self, oDevice, oDeviceRole):
        """
        Attach device role tags to the emulated devices.

        Args:
            oDevice - STC device object

            oDeviceRole - Device role object

        Return:
            N/A

        Example:
            llk_stc_associate_device_role(oDevice=device1,oDeviceRole=devicerole1)
        """
        llk_log_begin_proc()
        self.llk_stc_send_command("stc::config " + oDevice + " -AssociatedDeviceRole-targets " + oDeviceRole)
        llk_log_info("Successfully configured device role target")
        llk_log_end_proc()

    def llk_stc_device_tag(self, oDevice, sDeviceType):
        """
        Create a device tag and attach it to the emulated device

        Args:
            oDevice - STC device object

            sDeviceType - Device type
                Type: String
                Default: ""
                Values: Host, Router, Client, Server, Core, Edge

        Return:
            N/A

        Example:
            llk_stc_device_tag(oDevice=device1,sDeviceType=Router)
        """
        llk_log_begin_proc()
        dTags = {'Host': 0,'Router': 1, 'Client': 2,'Server': 3,'Core': 4,'Edge': 5}
        oTags = self.llk_stc_send_command("stc::get " + self.oStcProject + " -children-Tags")
        lTag = self.llk_stc_send_command("stc::get " + oTags + " -children")
        oTag = lTag[dTags[str(sDeviceType)]]
        sCommand = "stc::config " + oDevice + " -DefaultTag-targets " + oTag
        self.llk_stc_send_command(sCommand)
        llk_log_info("Successfully configured device tag")
        llk_log_end_proc()

    def llk_stc_create_streamblock(self,oPort, sName):
        """
        Create raw streamblock in STC
        Args:
            oPort: Port object instance.
            sName: User given name for streamblock.
        Returns:
            oStreamblock: Streamblock object reference
        Examples:
            llk_stc_create_streamblock("port1", "Downstream1")
        """

        llk_log_begin_proc()

        self.llk_stc_send_command("stc::create streamBlock -under " + oPort + " -name " + str(sName))
        oStreamblock =  str.split(str(self.llk_stc_send_command("stc::get " + oPort + " -children-streamBlock")))[-1]
        llk_log_debug("Successfully created streamblock")

        llk_log_end_proc()
        return oStreamblock

    def llk_stc_config_streamblock(self,oStreamblock, **kwargs):
        """
        Configure a streamblock in STC
        Args:
            oStreamblock: Streamblock object reference. (Ex: streamblock1)
        Keyword Args:
            Keyword list of streamblock attributes to be modified.

            FillType - Fill pattern type to be used for the payload.
                Values: CONSTANT, DECR, INCR, PRBS
                Default: CONSTANT
                Type: enum
            FrameLengthMode - Frame length mode used by this stream.
                Values: AUTO, DECR, FIXED, IMIX, INCR, RANDOM
                Default: FIXED
                Type: enum
            FixedFrameLength - Fixed value for frame length.
                Range: 12 - 16383
                Default: 128
                Type: integer
            MaxFrameLength - Maximum frame length for random mode.
                Range: 12 - 16383
                Default: 256
                Type: integer
            MinFrameLength - Minimum frame length for random mode.
                Range: 12 - 16383
                Default: 128
                Type: integer
        Returns:
            N/A
        Examples:
            llk_stc_config_streamblock(FrameLengthMode=Random, MinFrameLength=500, MaxFrameLength=1500)
        """
        llk_log_begin_proc()

        sCommand = "stc::config " + oStreamblock
        for key in kwargs:
            sCommand = sCommand + " -" + key + " " + str(kwargs[key])

        self.llk_stc_send_command(sCommand)
        llk_log_debug("Successfully configured streamblock")

        llk_log_end_proc()
    
    def llk_stc_config_streamblock_mplsip_itf(self, oStreamblock, **kwargs):
        """
        For ethii mpls
        """
 	llk_log_begin_proc()

	#delete ipv4 layer
        llk_log_debug("Delete ipv4 layer!")
        oIpv4Itf = self.llk_stc_send_command("stc::get " + oStreamblock + " -children-ipv4:IPv4")
        self.llk_stc_send_command("stc::get " + oIpv4Itf)
        self.llk_stc_send_command("stc::delete " + oIpv4Itf)	

        oItf =  self.llk_stc_send_command("stc::create mpls:Mpls -under " + oStreamblock)
	oIPf =  self.llk_stc_send_command("stc::create ipv4:IPv4 -under " + oStreamblock)	

        sCommand = "stc::config " + oItf
        if kwargs:
            for key in kwargs:
                sCommand = sCommand + " -" + key + " " + str(kwargs[key])
        self.llk_stc_send_command(sCommand)
        llk_log_debug("Successfully configured streamblock mpls interface")

        llk_log_end_proc()

    def llk_stc_config_streamblock_mplseth_itf(self, oStreamblock, **kwargs):
        """
        For ethii mpls
        """
 	llk_log_begin_proc()

	#delete ipv4 layer
        llk_log_debug("Delete ipv4 layer!")
        oIpv4Itf = self.llk_stc_send_command("stc::get " + oStreamblock + " -children-ipv4:IPv4")
        self.llk_stc_send_command("stc::get " + oIpv4Itf)
        self.llk_stc_send_command("stc::delete " + oIpv4Itf)	

        oItf =  self.llk_stc_send_command("stc::create mpls:Mpls -under " + oStreamblock)
	#oIEth = self.llk_stc_send_command("stc::get " + oItf + " -children-ethernet:Ethernetii")
	#self.llk_stc_send_command("stc::delete " + oIEth)

        sCommand = "stc::config " + oItf
        if kwargs:
            for key in kwargs:
                sCommand = sCommand + " -" + key + " " + str(kwargs[key])
        self.llk_stc_send_command(sCommand)
        llk_log_debug("Successfully configured streamblock mpls interface")

        llk_log_end_proc()



    def llk_stc_config_streamblock_ethii_itf(self, oStreamblock, **kwargs):
        """
        Configure ethernetii interface
        Args:
            oStreamblock: Streamblock object reference. (Ex: streamblock1)
        Keyword Args:
            Keyword list of ethernet attributes to be modified.

            dstMac -
                Default: 00:00:01:00:00:01
                Type: MACADDR
            srcMac -
                Default: 00:10:94:00:00:02
                Type: MACADDR
            etherType -
                Values: 0200 (XEROX PUP), 0201 (PUP Addr Trans), 0400 (Nixdorf), 0600 (XEROX NS IDP), 0660 (DLOG),
                0661 (DLOG2), 0800 (Internet IP), 0801 (X.75 Internet), 0802 (NBS Internet), 0803 (ECMA Internet),
                0804 (Chaosnet), 0805 (X.25 Level 3), 0806 (ARP), 0807 (XNS Compatibility), 0808 (Frame Relay ARP),
                8035 (RARP), 86DD (IPv6), 880B (PPP), 8809 (Slow Protocol), 8847 (MPLS Unicast), 8848 (MPLS Multicast),
                8863 (PPPoE Discovery), 8864 (PPPoE Session), 88E7 (PBB), 8906 (FCoE), 8914 (FIP)
                Default: 88B5 (Local Experimental Ethertype)
            preamble -
                Default: 55555555555555d5
                Type: OCTETSTRING
        Returns:
            N/A
        Examples:
            llk_stc_config_streamblock_ethii_itf("streamblock1", dstMac=00:00:00:00:00:01, srcMac=00:00:00:00:00:02)
        """
        llk_log_begin_proc()

        oItf =  self.llk_stc_send_command("stc::get " + oStreamblock + " -children-ethernet:ethernetii")

        # DELETED
        self.llk_stc_send_command("stc::get " + oItf)

        sCommand = "stc::config " + oItf
        if kwargs:
            for key in kwargs:
                if 'mac' in str(key) : 
                    fValid = self._llk_stc_is_valid_mac_address(kwargs[key])
                    if not fValid :
                        llk_log_end_proc()
                        raise AssertionError("Invalid format of %s : %s" % (key,kwargs[key]))

                sCommand = sCommand + " -" + key + " " + str(kwargs[key])

        self.llk_stc_send_command(sCommand)
        llk_log_debug("Successfully configured streamblock ethernetii interface")

        llk_log_end_proc()

    def llk_stc_config_streamblock_ipv4_itf(self,oStreamblock, **kwargs):
        """
        Configure ipv4 interface
        Args:
            oStreamblock: Streamblock object reference. Ex: streamblock1
        Keyword Args:
            Keyword list of ipv4 attributes to be modified.

            checksum -
                Default: 0
                Type: INTEGER
            destAddr -
                Default: 192.0.0.1
                Type: IPV4ADDR
            destPrefixLength -
                Default: 24
                Type: INTEGER
            fragOffset -
                Default: 0
                Type: INTEGER
            gateway -
                Default: 192.85.1.1
                Type: IPV4ADDR
            prefixLength -
                Default: 24
                Type: INTEGER
            protocol -
                Values: 0 (HOPOPT), 1 (ICMP), 2 (IGMP), 3 (GGP), 4 (IP), 5 (ST), 6 (TCP), 7 (CBT), 8 (EGP), 9 (IGP),
                10 (BBN-RCC-MON), 11 (NVP-II), 12 (PUP), 13 (ARGUS), 14 (EMCON), 15 (XNET), 16 (CHAOS), 17 (UDP),
                18 (MUX), 19 (DCN-MEAS), 20 (HMP), 21 (PRM), 22 (XNS-IDP), 23 (TRUNK-1), 24 (TRUNK-2), 25 (LEAF-1),
                26 (LEAF-2), 27 (RDP), 28 (IRTP), 29 (ISO-TP4), 30 (NETBLT), 31 (MFE-NSP), 32 (MERIT-INP), 33 (SEP),
                34 (3PC), 35 (IDPR), 36 (XTP), 37 (DDP), 38 (IDPR-CMTP), 39 (TP++), 40 (IL), 41 (IPv6), 42 (SDRP),
                43 (IPv6-Route), 44 (IPv6-Frag), 45 (IDRP), 46 (RSVP), 47 (GRE), 48 (MHRP), 49 (BNA), 50 (ESP),
                51 (AH), 52 (I-NLSP), 53 (SWIPE), 54 (NARP), 55 (MOBILE), 56 (TLSP), 57 (SKIP), 58 (IPv6-ICMP),
                59 (IPv6-NoNxt), 60 (IPv6-Opts), 62 (CFTP), 64 (SAT-EXPAK), 65 (KRYPTOLAN), 66 (RVD), 67 (IPPC),
                69 (SAT-MON), 70 (VISA), 71 (IPCV), 72 (CPNX), 73 (CPHB), 74 (WSN), 75 (PVP), 76 (BR-SAT-MON),
                77 (SUN-ND), 78 (WB-MON), 79 (WB-EXPAK), 80 (ISO-IP), 81 (VMTP), 82 (SECURE-VMTP), 83 (VINES),
                84 (TTP), 85 (NSFNET-IGP), 86 (DGP), 87 (TCF), 88 (EIGRP), 89 (OSPFIGP), 90 (Sprite-RPC), 91 (LARP),
                92 (MTP), 93 (AX.25), 94 (IPIP), 95 (MICP), 96 (SCC-SP), 97 (ETHERIP), 98 (ENCAP), 100 (GMTP),
                101 (IFMP), 102 (PNNI), 103 (PIM), 104 (ARIS), 105 (SCPS), 106 (QNX), 107 (A/N), 108 (IPComp),
                109 (SNP), 110 (Compaq-Peer), 111 (IPX-in-IP), 112 (VRRP), 113 (PGM), 115 (L2TP), 116 (DDX),
                117 (IATP), 118 (STP), 119 (SRP), 120 (UTI), 121 (SMP), 122 (SM), 123 (PTP), 124 (ISIS over IPv4),
                125 (FIRE), 126 (CRTP), 127 (CRUDP), 128 (SSCOPMCE), 129 (IPLT), 130 (SPS), 131 (PIPE), 132 (SCTP),
                133 (FC), 134 (RSVP-E2E-IGNORE), 135 (Mobility Header), 136 (UDPLite), 137 (MPLS-in-IP),
                253 (Experimental), 255 (Reserved)
                Default: 253
            sourceAddr -
                Default: 192.85.1.2
                Type: IPV4ADDR
            totalLength -
                Default: 20
                Type: INTEGER
            ttl -
                Default: 255
                Type: INTEGER
            identification -
                Type: INTEGER
                Default: 0
            ihl -
                Type: INTEGER
                Default: 5
            version
                Type: INTEGER
                Default: 4

        Returns:
            N/A
        Examples:
            llk_stc_config_streamblock_ipv4_itf("streamblock1", sourceAddr=30.30.0.2, destAddr=50.50.0.1)
        """
        llk_log_begin_proc()

        dArgs = dict(map(lambda (k,v): (str(k).lower(), v), kwargs.iteritems()))

        # Delete the IPv6 child of the streamblock object
        oIPv6 = self.llk_stc_send_command("stc::get "+ oStreamblock +" -children-ipv6:IPv6")
        if oIPv6:
            self.llk_stc_send_command("stc::delete "+oIPv6)
            llk_log_info("Deleted the IPv6 interface")

        #Configure IPv4 interface
        oIPv4 = self.llk_stc_send_command("stc::get "+ oStreamblock +" -children-ipv4:IPv4")

        if not oIPv4:
            self.llk_stc_send_command("stc::create ipv4:IPv4 -under "+ oStreamblock)

        attributes_list = ['ihl','totallength','identification','fragoffset','ttl','protocol','checksum',
                           'sourceaddr','destaddr','prefixlength','destprefixlength','gateway','version']

        oIPv4 =  self.llk_stc_send_command("stc::get " + oStreamblock + " -children-ipv4:IPv4")
        sCommand = "stc::config " + oIPv4
        # flag to indicate if there are any IPv4 header fields to be configured
        bFlag = False

        # Sending STC commands to configure IPv4 header fields
        for key in attributes_list:
            if key in dArgs.keys():
                sCommand = sCommand + " -" + key + " " + str(dArgs[key])
                dArgs.pop(key)
                bFlag = True

        #STC header field configuration command is sent only if the flag is set
        if bFlag:
            self.llk_stc_send_command(sCommand)
            llk_log_debug("Successfully created ipv4 interface")

        # Sending STC commands to configure IPv4 header options fields
        for key in dArgs.keys():
            if (str(key) == 'ip_header_options' and dArgs['ip_header_options'] != 'x'):
                #Configure ipv4 header options
                self.llk_stc_send_command("stc::create options -under "+oIPv4)
                llk_log_info("Created IPv4 options")
                oIpHeaderOptions =  str.split(str(self.llk_stc_send_command("stc::get " + oIPv4 + " -children-options")))[-1]
                self.llk_stc_send_command("stc::create IPv4HeaderOption -under "+ oIpHeaderOptions)
                oIpv4HeaderOptions =  str.split(str(self.llk_stc_send_command("stc::get " + oIpHeaderOptions + " -children-IPv4HeaderOption")))[-1]
                llk_log_info("Created IPv4 Header Options")

                if 'ip_header_option_rtralert' in dArgs.keys() and dArgs['ip_header_option_rtralert'] != 'x':
                    sOptions = "stc::create rtrAlert -under "+ oIpv4HeaderOptions
                    if 'rtralert_optn_type' in dArgs.keys() and dArgs['rtralert_optn_type'] != 'x':
                        sOptions += " -type "+ dArgs['rtralert_optn_type']
                    if 'rtralert_length' in dArgs.keys() and dArgs['rtralert_length'] != 'x':
                        sOptions += " -length "+dArgs['rtralert_length']
                    if 'rtralert_value' in dArgs.keys() and dArgs['rtralert_value'] != 'x':
                        sOptions += " -routerAlert "+dArgs['rtralert_value']
                    self.llk_stc_send_command(sOptions)
                    llk_log_info("Successfully configured the RTR alert value of IPv4 header options")
            elif (str(key) == 'DSCP' and dArgs['DSCP'] != 'x'):

                # Configure DSCP.Differentiated services in IP header has 6 bits. The input here is given as an Integer.
                # It is then converted to binary format (6 bits).The higher 3 bits are configured as attribute dscpHigh
                # and lower 3 bits are configured as attribute dscpLow.

                dscp_high = int(bin(int(dArgs['DSCP']))[2:][0:3],2)
                dscp_low = int(bin(int(dArgs['DSCP']))[2:][3:6],2)
                oDscp = self.llk_stc_send_command("stc::get "+oIPv4+" -children-tosdiffserv")
                if oDscp:
                    llk_log_info("Diffserv object exists!!")
                    pass
                else:
                    self.llk_stc_send_command("stc::create tosdiffserv -under "+ oIPv4)
                    oDscp =  str.split(str(self.llk_stc_send_command("stc::get " + oIPv4 + " -children-tosdiffserv")))[-1]
                    llk_log_info("New Diffserv child object created under IPv4 object")

                self.llk_stc_send_command("stc::create diffServ -under "+oDscp+" -dscpHigh "+str(dscp_high)+" -dscpLow "+str(dscp_low))
                llk_log_info("Successfully configured DSCP for IPv4 interface!!")

        llk_log_end_proc()

    def llk_stc_config_streamblock_vlan_itf(self,oStreamblock, **kwargs):
        """
        Configure vlan inteface
        Args:
            oStreamblock: Streamblock object reference. Ex: streamblock1
        Keyword Args:
            Keyword list of vlan attributes to be created or modified. keywords should be of the form vlan1, pri1

            vlan -
                Default:
                Type: INTEGER (0-4093)
            pri -
                Default: 000
                Type: BITSTRING or INTEGER (0-7)
            cfi -
                Default: 0
                Type: u8 (0-1)
            tpid -
                Default: 33024
                Type: u16
        Returns:
            N/A
        Examples:
            llk_stc_config_streamblock_vlan_itf(vlan1=101, pri1=001, vlan2=201, pri2=010)
        """

        llk_log_begin_proc()

        oEthItf =  self.llk_stc_send_command("stc::get " + oStreamblock + " -children-ethernet:ethernetii")
        oVlansItf =  self.llk_stc_send_command("stc::get " + oEthItf + " -children-vlans")
        lVlanItf =  str.split(str(self.llk_stc_send_command("stc::get " + oVlansItf + " -children-vlan")))
        iVlansConfigured = len(lVlanItf)

        #Loop through configured stc vlans.  If vlan1 exists in kwargs dict then modify.
        #Pop value from dict then repeat for remaining stc vlans

        # Modify existing vlans
        for i in range(1,iVlansConfigured+1):
            oEthItf =  self.llk_stc_send_command("stc::get " + oStreamblock + " -children-ethernet:ethernetii")
            oVlansItf =  self.llk_stc_send_command("stc::get " + oEthItf + " -children-vlans")
            oVlanItf =  str.split(str(self.llk_stc_send_command("stc::get " + oVlansItf + " -children-vlan")))[i-1]

            sCmd = "stc::config " + oVlanItf
            if 'vlan'+str(i) in kwargs.keys() :
                sCmd = sCmd + " -id " + kwargs.pop("vlan" + str(i))
            if 'pri'+str(i) in kwargs.keys() :
                sFormatPriKey = self._llk_stc_format_priority(kwargs.pop('pri' + str(i)))
                sCmd = sCmd + " -pri " + sFormatPriKey
            if 'cfi'+str(i) in kwargs.keys() :
                sCmd = sCmd + " -cfi " + kwargs.pop("cfi" + str(i))
            if 'tpid'+str(i) in kwargs.keys() :
                sCmd = sCmd + " -tpid " + kwargs.pop("tpid" + str(i))

            self.llk_stc_send_command(sCmd)

        # Now that configured STC vlans have been modified and removed from kwargs dict, remaining kwargs will be NEW vlans.
        # Create remaining vlans
        # Check only for vlan keys
        sVlanExp = re.compile('vlan.*',re.IGNORECASE)
        for sKey in sorted(kwargs):
            if sVlanExp.match(sKey):
                oEthItf =  self.llk_stc_send_command("stc::get " + oStreamblock + " -children-ethernet:ethernetii")
                oVlansItf =  self.llk_stc_send_command("stc::get " + oEthItf + " -children-vlans")
                sPriKey = sKey.replace("vlan","pri")
                sCfiKey = sKey.replace("vlan","cfi")
                sTpidKey = sKey.replace("vlan","tpid")

                sCmd = "stc::create vlan -under " + oVlansItf + " -id " + kwargs.pop(sKey)

                if sPriKey in kwargs:
                    sFormatPriKey = self._llk_stc_format_priority(kwargs.pop(sPriKey))
                    sCmd = sCmd + " -pri " + str(sFormatPriKey)

                if sCfiKey in kwargs:
                   sCmd = sCmd + " -cfi " + kwargs.pop(sCfiKey)

                if sTpidKey in kwargs:
                   sCmd = sCmd + " -tpid " + kwargs.pop(sTpidKey)

                self.llk_stc_send_command(sCmd)

        llk_log_debug("Successfully configured streamblock vlan interface")
        llk_log_end_proc()

    def llk_stc_config_streamblock_arp_itf(self,oStreamblock, **kwargs):
        """
        Configure the ARP interface for the streamblock

        Input Arguments:
            oStreamblock: Stream block object

            kwargs:
                hardware Hardware type
                    Type: HardwareByte
                    Default: 0001
                    Possible Values:
                    Value Description
                    0001 Ethernet
                ihAddr Hardware address length
                    Type: INTEGER
                    Default: 6
                ipAddr Protocol address length
                    Type: INTEGER
                    Default: 4
                    A text name for the object. This attribute is required when you use stream block modifiers such as RangeModifier,
                    RandomModifier, and TableModifier. See the description of the OffsetReference attribute for these modifier objects
                    for more information.
                Name
                    Type: string
                operation Operation code
                    Type: ARPOperationByte
                    Default: 1
                    Possible Values:
                    Value Description
                    0 Unknown
                    1 ARP Request
                    2 ARP Reply
                protocol Protocol type
                    Type: ProtocolByte
                    Default: 0800
                    Possible Values:
                    Value Description
                    0800 Internet IP
                senderHwAddr Sender hardware address
                    Type: MACADDR
                    Default: 00:00:01:00:00:02
                senderPAddr Sender IP address
                    Type: IPV4ADDR
                    Default: 192.85.1.2
                targetHwAddr Target hardware address
                    Type: MACADDR
                    Default: 00:00:00:00:00:00
                targetPAddr Target IP address
                    Type: IPV4ADDR
                    Default: 0.0.0.0


        Return value:
            N/A

        Example:
            llk_stc_config_streamblock_arp_itf("streamblock1",protocol=0900,senderHwAddr=00:00:00:00:00:05,senderPAddr=2.2.2.2,
                                               targetHwAddr=00:00:00:00:00:09,targetPAddr=3.3.3.3)

        """
        #Configure the ethernet interface to be of type 0806

        llk_log_begin_proc()

        #delete ipv4 layer
        llk_log_debug("Delete ipv4 layer!")
        oIpv4Itf = self.llk_stc_send_command("stc::get " + oStreamblock + " -children-ipv4:IPv4")
        self.llk_stc_send_command("stc::get " + oIpv4Itf)
        self.llk_stc_send_command("stc::delete " + oIpv4Itf)

        llk_log_debug("Configured the ethernet interface to be ARP")
        oEthItf =  self.llk_stc_send_command("stc::get " + oStreamblock + " -children-ethernet:ethernetii")
        self.llk_stc_send_command("stc::config "+oEthItf+" -ethertype 0806 ")

        #create and configure ARP packet with the parameters of source and dest mac add, protocol add and op code

        llk_log_debug("Configure arp interface!")
        self.llk_stc_send_command("stc::create arp:ARP -under " + oStreamblock)
        oArpPacket =  self.llk_stc_send_command("stc::get " + oStreamblock + " -children-arp:ARP")
        sCommand = "stc::config " + oArpPacket
        for key in kwargs:
            sCommand = sCommand + " -" + key + " " + str(kwargs[key])
        self.llk_stc_send_command(sCommand)
        llk_log_info("Configured the ARP interface")

        self.llk_stc_send_command("stc::apply")

        llk_log_end_proc()

    def llk_stc_config_streamblock_rarp_itf(self,oStreamblock, **kwargs):
        """
        Configure the RARP interface for the streamblock

        Input Arguments:
            oStreamblock: Stream block object

            kwargs:
                hardware Hardware type
                    Type: HardwareByte
                    Default: 0001
                    Possible Values:
                    Value Description
                    0001 Ethernet
                ihAddr Hardware address length
                    Type: INTEGER
                    Default: 6
                ipAddr Protocol address length
                    Type: INTEGER
                    Default: 4
                    A text name for the object. This attribute is required when you use stream block modifiers such as RangeModifier,
                    RandomModifier, and TableModifier. See the description of the OffsetReference attribute for these modifier objects
                    for more information.
                Name
                    Type: string
                operation Operation code
                    Type: RARPOperationByte
                    Default: 1
                    Possible Values:
                    Value Description
                    0 Unknown
                    1 RARP Request
                    2 RARP Reply
                protocol Protocol type
                    Type: ProtocolByte
                    Default: 0800
                    Possible Values:
                    Value Description
                    0800 Internet IP
                senderHwAddr Sender hardware address
                    Type: MACADDR
                    Default: 00:00:01:00:00:02
                senderPAddr Sender IP address
                    Type: IPV4ADDR
                    Default: 192.85.1.2
                targetHwAddr Target hardware address
                    Type: MACADDR
                    Default: 00:00:00:00:00:00
                targetPAddr Target IP address
                    Type: IPV4ADDR
                    Default: 0.0.0.0


        Return value:
            N/A

        Example:
            llk_stc_config_streamblock_rarp_itf("streamblock1",protocol=0900,senderHwAddr=00:00:00:00:00:05,senderPAddr=2.2.2.2,
                                               targetHwAddr=00:00:00:00:00:09,targetPAddr=3.3.3.3)

        """
        #Configure the ethernet interface to be of type 0806
        llk_log_begin_proc()

        #delete ipv4 layer
        llk_log_debug("Delete ipv4 layer!")
        oIpv4Itf = self.llk_stc_send_command("stc::get " + oStreamblock + " -children-ipv4:IPv4")
        self.llk_stc_send_command("stc::get " + oIpv4Itf)
        self.llk_stc_send_command("stc::delete " + oIpv4Itf)

        llk_log_debug("Configured the ethernet interface to be RARP")
        oEthItf =  self.llk_stc_send_command("stc::get " + oStreamblock + " -children-ethernet:ethernetii")
        self.llk_stc_send_command("stc::config "+oEthItf+" -ethertype 0806 ")

        #create and configure RARP packet with the parameters of source and dest mac add, protocol add and op code

        llk_log_debug("Configure rarp interface!")
        self.llk_stc_send_command("stc::create rarp:RARP -under " + oStreamblock)
        oRarpPacket =  self.llk_stc_send_command("stc::get " + oStreamblock + " -children-rarp:RARP")
        sCommand = "stc::config " + oRarpPacket
        for key in kwargs:
            sCommand = sCommand + " -" + key + " " + str(kwargs[key])
        self.llk_stc_send_command(sCommand)
        llk_log_info("Configured the RARP interface")

        self.llk_stc_send_command("stc::apply")

        llk_log_end_proc()

    def llk_stc_config_streamblock_pppoe_itf(self,oStreamblock, **kwargs):
        """
        Configure the PPPoE interface for the streamblock

        Input Arguments:
            oStreamblock: Stream block object

            kwargs:
                Code
                     Type: PPPoECode
                     Default: 9
                     Possible Values:
                     Value 	Description
                     9 	PADI
                     7 	PADO
                     25 	PADR
                     101 	PADS
                     167 	PADT 
                length
                    Type: INTEGER
                    Default: 0 
                Name
                    Type: string
                    A text name for the object. This attribute is required when you use stream block modifiers such as RangeModifier, RandomModifier, and TableModifier. See the description of the OffsetReference attribute for these modifier objects for more information.
                sessionId
                    Type: INTEGER
                    Default: 0
                type
                    Type: INTEGER
                    Default: 1
                version
                    Type: INTEGER
                    Default: 1

        Return value:
            N/A

        Example:
            llk_stc_config_streamblock_pppoe_itf("streamblock1",code=9)

        """
        #Configure the ethernet interface to be of type 0806
        llk_log_begin_proc()
	dPPPoEacName = {}
        #delete ipv4 layer
        llk_log_debug("Delete ipv4 layer!")
        oIpv4Itf = self.llk_stc_send_command("stc::get " + oStreamblock + " -children-ipv4:IPv4")
        self.llk_stc_send_command("stc::get " + oIpv4Itf)
        self.llk_stc_send_command("stc::delete " + oIpv4Itf)

        sProtocolName = 'pppoe:PPPoESession'
        sProtocolType = '8864'
        if kwargs.has_key('code'): 
            kwargs['code'] = str(int(kwargs['code'],16))
            if kwargs['code'] in ['9','7','25','101','167'] :
                sProtocolName = 'pppoe:PPPoEDiscovery'
                sProtocolType = '8863'

        # modify ethii layer
        llk_log_debug("Configured the ethernet interface to be PPPoE")
        oEthItf =  self.llk_stc_send_command("stc::get " + oStreamblock + " -children-ethernet:ethernetii")
        self.llk_stc_send_command("stc::config "+oEthItf+" -ethertype " + sProtocolType )

        #create and configure PPPoE packet with the input parameters 
        llk_log_debug("Configure PPPoE interface!")

        self.llk_stc_send_command("stc::create " + sProtocolName + " -under " + oStreamblock)
        oPppoePacket =  self.llk_stc_send_command("stc::get " + oStreamblock + " -children-" + sProtocolName)
        sCommand = "stc::config " + oPppoePacket
        
        for key in kwargs:
            # skip cfmheader
            if 'acname' in str(key) : 
                newKey = key.replace('acname.','')
                dPPPoEacName[newKey] = kwargs[key]
                continue          
            # others arguments will be configured on tag interface
            sCommand = sCommand + " -" + key + " " + str(kwargs[key])  
	self.llk_stc_send_command(sCommand)
   
	if sProtocolName == 'pppoe:PPPoEDiscovery' :
	    llk_log_info("Configure the tags interface!")
            oPppoeTags = self.llk_stc_send_command("stc::create tags -under " + oPppoePacket)
	    oPppoeTag  = self.llk_stc_send_command("stc::create PPPoETag -under " + oPppoeTags)
	    if dPPPoEacName :
 	        oPppoeAcName = self.llk_stc_send_command("stc::create acName -under " + oPppoeTag)
	        sCommand = "stc::config " + oPppoeAcName
                for key in dPPPoEacName:
                    sCommand = sCommand + " -" + key + " " + str(dPPPoEacName[key])
                self.llk_stc_send_command(sCommand)

        self.llk_stc_send_command("stc::apply")

        llk_log_end_proc()

    def llk_stc_config_streamblock_tcp_itf(self,oStreamblock, **kwargs):
        """
        Configure the TCP interface for a streamblock

        Input Arguments:
            oStreamblock : Streamblock object
        kwargs :
            ackBit
                Type: BITSTRING
                Default: 1
            ackNum
                Type: INTEGER
                Default: 234567
            checksum
                Type: INTEGER
                Default: Automatically calculated for each packet. (If you set this to 0, the checksum will not be calculated and will be
                the same for each packet.)
            cwrBit
                Type: BITSTRING
                Default: 0
            destPort
                Type: WellKnownPorts
                Default: 1024
                Possible Values:
                1 TCPMUX
                2 CompressNet
                3 CompressProcess
                5 RJE
                7 ECHO
                9 DISCARD
                11 SYSTAT
                13 DAYTIME
                17 QOTD
                18 MSP
                19 CHARGEN
                20 FTPDATA
                21 FTP
                22 SSH
                23 Telnet
                25 SMTP
                27 NSW
                29 MSG-ICP
                31 MSG-AUTH
                33 DSP
                37 TIME
                38 RAP
                39 RLP
                41 Graphics
                42 Nameserver
                43 WHOIS
                44 MPM-flags
                45 MPM
                46 MPM-send
                47 NI FTP
                48 AUDIT
                49 TACACS
                50 RE-MAIL
                51 LA-MAINT
                52 XNS-time
                53 DNS
                54 XNS
                55 ISI-GL
                56 XNS-auth
                69 TFTP
                70 GOPHER
                79 FINGER
                80 HTTP
                88 KERBEROS
                119 NNTP
                123 NTP
                161 SNMP
                162 SNMPTRAP
                179 BGP
                194 IRC
                520 RIP
                521 RIPNG
                3784 BFD
            ecnBit
                Type: BITSTRING
                Default: 0
            finBit
                Type: BITSTRING
                Default: 0
                A text name for the object. This attribute is required when you use stream block modifiers such as RangeModifier,
                RandomModifier, and TableModifier. See the description of the OffsetReference attribute for these modifier objects for
                more information.
            Name
                Type: string
            offset
                Type: INTEGER
                Default: 5
            pshBit
                Type: BITSTRING
                Default: 0
            reserved
                Type: BITSTRING
                Default: 0000
            rstBit
                Type: BITSTRING
                Default: 0
            seqNum
                Type: INTEGER
                Default: 123456
            sourcePort
                Type: INTEGER
                Default: 1024
            synBit
                Type: BITSTRING
                Default: 0
            urgBit
                Type: BITSTRING
                Default: 0
            urgentPtr
                Type: INTEGER
                Default: 0
            window
                Type: INTEGER
                Default: 4096

        Return value:
            N/A

        Example:
            llk_stc_config_streamblock_tcp_itf("streamblock1",sourcePort=80,destPort=102,seqNum=111,ackNum=222)
        """
        llk_log_begin_proc()
        oTcp =  self.llk_stc_send_command("stc::get " + oStreamblock + " -children-tcp:Tcp")
        if not oTcp:
            self.llk_stc_send_command("stc::create tcp:Tcp -under "+oStreamblock)
            oTcp =  self.llk_stc_send_command("stc::get " + oStreamblock + " -children-tcp:Tcp")
        sCommand = "stc::config " + oTcp
        for key in kwargs:
            sCommand = sCommand + " -" + key + " " + str(kwargs[key])
        self.llk_stc_send_command(sCommand)
        llk_log_info("Successfully configured TCP interface")
        llk_log_end_proc()

    def llk_stc_config_streamblock_ethernetssm_itf(self,oStreamblock, **kwargs):
        llk_log_begin_proc()
        assert ('ssmconfig' in kwargs.keys()), "Name of the ssmconfig should be given!!"
        sconfig    = kwargs.pop('ssmconfig')
        sCommand=self.llk_stc_send_command("stc::config "+oStreamblock+" -FrameConfig" + " " + sconfig)
        #for key in kwargs:
        #    sCommand = sCommand + " -" + key + " " + str(kwargs[key])
        self.llk_stc_send_command(sCommand)
        llk_log_info("Successfully configured EthernetSSM")

        llk_log_end_proc()

    def llk_stc_config_streamblock_udp_itf(self,oStreamblock, **kwargs):
        """
        Configure the UDP interface for a streamblock

        Input Arguments:
            oStreamblock : Streamblock object
        kwargs :
            checksum
                Type: INTEGER
                Default: Automatically calculated for each packet. (If you set this to 0, the checksum will not be calculated and will be the same
                for each packet.)
            destPort
                Type: WellKnownPorts
                Default: 1024
                Possible Values:
                1 TCPMUX
                2 CompressNet
                3 CompressProcess
                5 RJE
                7 ECHO
                9 DISCARD
                11 SYSTAT
                13 DAYTIME
                17 QOTD
                18 MSP
                19 CHARGEN
                20 FTPDATA
                21 FTP
                22 SSH
                23 Telnet
                25 SMTP
                27 NSW
                29 MSG-ICP
                31 MSG-AUTH
                33 DSP
                37 TIME
                38 RAP
                39 RLP
                41 Graphics
                42 Nameserver
                43 WHOIS
                44 MPM-flags
                45 MPM
                46 MPM-send
                47 NI FTP
                48 AUDIT
                49 TACACS
                50 RE-MAIL
                51 LA-MAINT
                52 XNS-time
                53 DNS
                54 XNS
                55 ISI-GL
                56 XNS-auth
                69 TFTP
                70 GOPHER
                79 FINGER
                80 HTTP
                88 KERBEROS
                119 NNTP
                123 NTP
                161 SNMP
                162 SNMPTRAP
                179 BGP
                194 IRC
                520 RIP
                521 RIPNG
                3784 BFD
            length
                Type: INTEGER
                Default: 0
                A text name for the object. This attribute is required when you use stream block modifiers such as RangeModifier, RandomModifier,
                and TableModifier. See the description of the OffsetReference attribute for these modifier objects for more information.
            Name
                Type: string
            sourcePort
                Type: INTEGER
                Default: 1024

        Return value:
            N/A

        Example:
            llk_stc_config_streamblock_udp_itf("streamblock1",sourcePort=655,destPort=800,length=15,checksum=22)
        """
        # create udp object and configure it with source and destination port
        llk_log_begin_proc()

        self.llk_stc_send_command("stc::create udp:Udp -under "+oStreamblock)
        oUdp =  self.llk_stc_send_command("stc::get " + oStreamblock + " -children-udp:Udp")
        sCommand = "stc::config " + oUdp
        for key in kwargs:
            sCommand = sCommand + " -" + key + " " + str(kwargs[key])
        self.llk_stc_send_command(sCommand)
        llk_log_info("Successfully configured UDP interface")

        llk_log_end_proc()

    def llk_stc_config_streamblock_ipv6_itf(self,oStreamblock, **kwargs):
        """
        Configure the IPv6 interface for a streamblock

        Input Arguments:
            oStreamblock : Streamblock object
        kwargs :
            destAddr
                Type: IPV6ADDR
                Default: 2000::1
            destPrefixLength
                Type: INTEGER
                Default: 64
            flowLabel
                Type: INTEGER
                Default: 0
            gateway
                Type: IPV6ADDR
                Default: ::0
            hopLimit
                Type: INTEGER
                Default: 255
                A text name for the object. This attribute is required when you use stream block modifiers such as RangeModifier,
                RandomModifier, and TableModifier. See the description of the OffsetReference attribute for these modifier objects for more
                information.
            Name
                Type: string
            nextHeader
                Type: IpProtocolNumbers
                Default: 59
            Possible Values:
                0 HOPOPT
                1 ICMP
                2 IGMP
                3 GGP
                4 IP
                5 ST
                6 TCP
                7 CBT
                8 EGP
                9 IGP
                10 BBN-RCC-MON
                11 NVP-II
                12 PUP
                13 ARGUS
                14 EMCON
                15 XNET
                16 CHAOS
                17 UDP
                18 MUX
                19 DCN-MEAS
                20 HMP
                21 PRM
                22 XNS-IDP
                23 TRUNK-1
                24 TRUNK-2
                25 LEAF-1
                26 LEAF-2
                27 RDP
                28 IRTP
                29 ISO-TP4
                30 NETBLT
                31 MFE-NSP
                32 MERIT-INP
                33 SEP
                34 3PC
                35 IDPR
                36 XTP
                37 DDP
                38 IDPR-CMTP
                39 TP++
                40 IL
                41 IPv6
                42 SDRP
                43 IPv6-Route
                44 IPv6-Frag
                45 IDRP
                46 RSVP
                47 GRE
                48 MHRP
                49 BNA
                50 ESP
                51 AH
                52 I-NLSP
                53 SWIPE
                54 NARP
                55 MOBILE
                56 TLSP
                57 SKIP
                58 IPv6-ICMP
                59 IPv6-NoNxt
                60 IPv6-Opts
                62 CFTP
                64 SAT-EXPAK
                65 KRYPTOLAN
                66 RVD
                67 IPPC
                69 SAT-MON
                70 VISA
                71 IPCV
                72 CPNX
                73 CPHB
                74 WSN
                75 PVP
                76 BR-SAT-MON
                77 SUN-ND
                78 WB-MON
                79 WB-EXPAK
                80 ISO-IP
                81 VMTP
                82 SECURE-VMTP
                83 VINES
                84 TTP
                85 NSFNET-IGP
                86 DGP
                87 TCF
                88 EIGRP
                89 OSPFIGP
                90 Sprite-RPC
                91 LARP
                92 MTP
                93 AX.25
                94 IPIP
                95 MICPsend_command
                96 SCC-SP
                97 ETHERIP
                98 ENCAP
                100 GMTP
                101 IFMP
                102 PNNI
                103 PIM
                104 ARIS
                105 SCPS
                106 QNX
                107 A/N
                108 IPComp
                109 SNP
                110 Compaq-Peer
                111 IPX-in-IP
                112 VRRP
                113 PGM
                115 L2TP
                116 DDX
                117 IATP
                118 STP
                119 SRP
                120 UTI
                121 SMP
                122 SM
                123 PTP
                124 ISIS over IPv4
                125 FIRE
                126 CRTP
                127 CRUDP
                128 SSCOPMCE
                129 IPLT
                130 SPS
                131 PIPE
                132 SCTP
                133 FC
                134 RSVP-E2E-IGNORE
                135 Mobility Header
                136 UDPLite
                137 MPLS-in-IP
                253 Experimental
                255 Reserved
            payloadLength
                Type: INTEGER
                Default: 0
            prefixLength
                Type: INTEGER
                Default: 64
            sourceAddr
                Type: IPV6ADDR
                Default: 2000::2
            trafficClass
                Type: INTEGER
                Default: 0
            version
                Type: INTEGER
                Default: 6

        Return value:
            N/A

        Example:
            llk_stc_config_streamblock_ipv6_itf("streamblock1",version=6,trafficClass=1,flowLabel=10,payloadLength=10)
        """

        # Delete the IPv4 child of the streamblock object
        llk_log_begin_proc()

        oIPv4 = self.llk_stc_send_command("stc::get "+ oStreamblock +" -children-ipv4:Ipv4 ")
        if oIPv4:
            self.llk_stc_send_command("stc::delete "+oIPv4)
            llk_log_info("Deleted the IPv4 interface")
        #Create IPv6 object
        self.llk_stc_send_command("stc::create ipv6:IPv6 -under "+ oStreamblock)
        oIPv6 =  self.llk_stc_send_command("stc::get " + oStreamblock + " -children-ipv6:IPv6")
        sCommand = "stc::config " + oIPv6
        for key in kwargs:
            sCommand = sCommand + " -" + key + " " + str(kwargs[key])
        self.llk_stc_send_command(sCommand)
        llk_log_info("Successfully configured ipv6 interface")

        llk_log_end_proc()

    def llk_stc_config_streamblock_load(self,oStreamblock, **kwargs):
        """
        Configures traffic load attributes of streamblock
        Args:
            oStreamblock: Streamblock object reference
        Keyword Args:
            Keyword list of load profile attributes to be modified.

            InterFrameGap - Gap (in bytes) between frames in same burst (priority-based scheduling).
                Default: 12
                Type: INTEGER
            InterFrameGapUnit - Unit for inter-frame gap.
                Values: BITS_PER_SECOND, BYTES, FRAMES_PER_SECOND, KILOBITS_PER_SECOND, MEGABITS_PER_SECOND,
                MILLISECONDS, NANOSECONDS, PERCENT_LINE_RATE
                Default: BYTES
            Load - Load value set on stream block.
                Default: 10
                Type: double
            LoadUnit - Load unit applied to stream block.
                Values: BITS_PER_SECOND, FRAMES_PER_SECOND, INTER_BURST_GAP, INTER_BURST_GAP_IN_MILLISECONDS,
                INTER_BURST_GAP_IN_NANOSECONDS, KILOBITS_PER_SECOND, MEGABITS_PER_SECOND, PERCENT_LINE_RATE
                Default: PERCENT_LINE_RATE
        Returns:
            N/A
        Examples:
            llk_stc_config_streamblock_load("streamblock1", load=1, loadunit=MEGABITS_PER_SECOND)
        """

        llk_log_begin_proc()

        oLoadProfile = self.llk_stc_send_command("stc::get " + oStreamblock + " -affiliationstreamblockloadprofile")

        sCommand = "stc::config " + oLoadProfile
        for key in kwargs:
            sCommand = sCommand + " -" + key + " " + str(kwargs[key])

        self.llk_stc_send_command(sCommand)
        llk_log_debug("Successfully configured streamblock load profile")

        llk_log_end_proc()

    def llk_stc_config_streamblock_dhcp_itf(self,oStreamblock, **kwargs):
        """
        Configure the dhcp interface for the streamblock

        Input Arguments:
            oStreamblock: Stream block object

            kwargs:
                Code
                     Type: PPPoECode
                     Default: 9
                     Possible Values:
                     Value 	Description
                     9 	PADI
                     7 	PADO
                     25 	PADR
                     101 	PADS
                     167 	PADT 
                length
                    Type: INTEGER
                    Default: 0 
                Name
                    Type: string
                    A text name for the object. This attribute is required when you use stream block modifiers such as RangeModifier, RandomModifier, and TableModifier. See the description of the OffsetReference attribute for these modifier objects for more information.
                sessionId
                    Type: INTEGER
                    Default: 0
                type
                    Type: INTEGER
                    Default: 1
                version
                    Type: INTEGER
                    Default: 1

        Return value:
            N/A

        Example:
            llk_stc_config_streamblock_dhcp_itf("streamblock1",messageType=1)

        """
        llk_log_begin_proc()

        sProtocolName = 'dhcp:Dhcpclientmsg'
        if kwargs.has_key('messagetype'):
            if kwargs['messagetype'] in ['2','4'] :
                sProtocolName = 'dhcp:Dhcpservermsg'

        #create and configure dhcp packet with the input parameters 
        llk_log_debug("Configure DHCP interface!")
        self.llk_stc_send_command("stc::create " + sProtocolName + " -under " + oStreamblock)
        oDhcpPacket =  self.llk_stc_send_command("stc::get " + oStreamblock + " -children-" + sProtocolName)
        sCommand = "stc::config " + oDhcpPacket
        for key in kwargs:
            sCommand = sCommand + " -" + key + " " + str(kwargs[key])
        self.llk_stc_send_command(sCommand)
        llk_log_info("Configured the DHCP interface")

        #configure the frame as auto length
        self.llk_stc_send_command("stc::config " + oStreamblock + ' -framelengthmode auto')
        llk_log_end_proc()

    def llk_stc_config_streamblock_rip_itf(self,oStreamblock, **kwargs):
        """
        integrate RIP v1 and v2 
        """
        sCommand = ""
        oRipEntry = ""
        llk_log_begin_proc()
	rip_version = kwargs.setdefault('version','VER1')
	llk_log_debug("Configure RIP interface!")
        if rip_version == 'VER1' :
	    oRipPacket = self.llk_stc_send_command("stc::create rip:Ripv1 -under " + oStreamblock)
            sCommand = "stc::config " + oRipPacket
            kwargs['version'] = '1'
	elif rip_version == 'VER2' :
            oRipPacket = self.llk_stc_send_command("stc::create rip:Ripv2 -under " + oStreamblock)
            sCommand = "stc::config " + oRipPacket
	    kwargs['version'] = '2'

	for key in kwargs:
            sCommand = sCommand + " -" + key + " " + str(kwargs[key])
        self.llk_stc_send_command(sCommand)

        #config the rip entries
        llk_log_info("Configure the rip entries interface!")
        if rip_version == 'VER1' :
            oRipEntries = self.llk_stc_send_command("stc::create rip1Entries -under " + oRipPacket)
            oRipEntry = self.llk_stc_send_command("stc::create Rip1Entry -under " + oRipEntries)
	elif rip_version == 'VER2' :
            oRipEntries = self.llk_stc_send_command("stc::create rip2Entries -under " + oRipPacket)
            oRipEntry = self.llk_stc_send_command("stc::create Rip2Entry -under " + oRipEntries)
	

        #configure the frame as auto length
        self.llk_stc_send_command("stc::config " + oStreamblock + ' -framelengthmode auto')
        llk_log_end_proc()



    def llk_stc_config_streamblock_igmp_itf(self,oStreamblock, **kwargs):
        """
        integrate IGMP v2 and v3 for both query and report
        """
        llk_log_begin_proc()
 
        igmpProtocolMap = {'V2':'self.llk_stc_config_streamblock_igmpv2_itf',
                           'V3Q':'self.llk_stc_config_streamblock_query_itf',
                           'V3R':'self.llk_stc_config_streamblock_report_itf'}

        try : 
            igmp_version = kwargs.setdefault('version','V2')
            if kwargs['version'] == 'V3' and kwargs['type'] == '11' :
                igmp_version = 'V3Q'
            elif kwargs['version'] == 'V3' and kwargs['type'] == '16' :
                igmp_version = 'V3R'
            kwargs.pop('version')
            sCmd = igmpProtocolMap[igmp_version]+'(oStreamblock, **kwargs)'
            eval(sCmd)
        except Exception as inst:
            s = sys.exc_info()
            raise AssertionError("exception:%s,lineno:%s" % (inst,s[2].tb_lineno))

        llk_log_end_proc()

    def llk_stc_config_streamblock_query_itf(self,oStreamblock, **kwargs):
        """
        For IGMP V3 query
        """
        llk_log_begin_proc()
	llk_log_debug("Configure IGMPV3 Query interface!")
	self.llk_stc_send_command("stc::create igmp:Igmpv3Query -under " + oStreamblock)
	oIGMPv3Packet =  self.llk_stc_send_command("stc::get " + oStreamblock + " -children-igmp:Igmpv3Query")
	sCommand = "stc::config " + oIGMPv3Packet
	
        #config the addrList
        llk_log_info("Configure the addrList interface!")
        oaddrList = self.llk_stc_send_command("stc::create addrList -under " + oIGMPv3Packet)
        oipv4Addr = self.llk_stc_send_command("stc::create Ipv4Addr -under " + oaddrList)

	for key in kwargs:
            sCommand = sCommand + " -" + key + " " + str(kwargs[key])
        self.llk_stc_send_command(sCommand)

        #configure the frame as auto length
        self.llk_stc_send_command("stc::config " + oStreamblock + ' -framelengthmode auto')
        llk_log_end_proc()

    def llk_stc_config_streamblock_report_itf(self,oStreamblock, **kwargs):
        """
        For IGMP V3 report
        """
        llk_log_begin_proc()
	llk_log_debug("Configure IGMPV3 Report interface!")
	self.llk_stc_send_command("stc::create igmp:Igmpv3Report -under " + oStreamblock)
	oIGMPv3Packet =  self.llk_stc_send_command("stc::get " + oStreamblock + " -children-igmp:Igmpv3Report")
	sCommand = "stc::config " + oIGMPv3Packet
	
        #config the addrList
        llk_log_info("Configure the grpRecords interface!")
        ogrpRecords = self.llk_stc_send_command("stc::create grpRecords -under " + oIGMPv3Packet)
        ogrpRecord = self.llk_stc_send_command("stc::create GroupRecord -under " + ogrpRecords)
        oaddrList = self.llk_stc_send_command("stc::create addrList -under " + ogrpRecord)
        oipv4Addr = self.llk_stc_send_command("stc::create Ipv4Addr -under " + oaddrList)

	for key in kwargs:
            sCommand = sCommand + " -" + key + " " + str(kwargs[key])
        self.llk_stc_send_command(sCommand)

        #configure the frame as auto length
        self.llk_stc_send_command("stc::config " + oStreamblock + ' -framelengthmode auto')
        llk_log_end_proc()
        

    def llk_stc_config_streamblock_igmpv2_itf(self,oStreamblock, **kwargs):
        """
        Configure the dhcp interface for the streamblock

        Input Arguments:
            oStreamblock: Stream block object

            kwargs:
                checksum
                     Type: INTEGER
                     Default: 0
                length
                    Type: INTEGER
                    Default: 0 
                

        Return value:
            N/A

        Example:
            llk_stc_config_streamblock_igmpv2_itf("streamblock1",groupAddress='255.0.0.2',type='11')

        """
        llk_log_begin_proc()

        #create and configure igmpv2 packet with the input parameters 
        llk_log_debug("Configure IGMPV2 interface!")
        self.llk_stc_send_command("stc::create igmp:Igmpv2 -under " + oStreamblock)
        oIgmpv2Packet =  self.llk_stc_send_command("stc::get " + oStreamblock + " -children-igmp:Igmpv2")
        sCommand = "stc::config " + oIgmpv2Packet
        for key in kwargs:
            sCommand = sCommand + " -" + key + " " + str(kwargs[key])
        self.llk_stc_send_command(sCommand)
        llk_log_info("Configured the IGMPV2 interface")

        #configure the frame as auto length
        self.llk_stc_send_command("stc::config " + oStreamblock + ' -framelengthmode auto')
        llk_log_end_proc()

    def llk_stc_config_streamblock_cfm_itf(self,oStreamblock, **kwargs):
        """
        Configure the lbr interface for the streamblock

        Input Arguments:
            oStreamblock: Stream block object

            kwargs:


        Return value:
            N/A

        Example:
            llk_stc_config_streamblock_lbr_itf("streamblock1",messageType=1)

        """
        dCfmProtocolMap = {'1':'self.llk_stc_config_streamblock_ccm_itf',
                           '2':'self.llk_stc_config_streamblock_lbr_itf',
                           '3':'self.llk_stc_config_streamblock_lbm_itf',
                           '5':'self.llk_stc_config_streamblock_ltm_itf'}

        sOpCode = kwargs.setdefault('opcode','1')
        sCmd = dCfmProtocolMap[sOpCode]+'(oStreamblock, **kwargs)'
        eval(sCmd)
    
        llk_log_end_proc()

    def llk_stc_config_streamblock_ccm_itf(self,oStreamblock, **kwargs):
        """
        Configure the ccm interface for the streamblock

        Input Arguments:
            oStreamblock: Stream block object

            kwargs:
                Code
                     Type: PPPoECode
                     Default: 9
                     Possible Values:
                     Value 	Description
                     9 	PADI
                     7 	PADO
                     25 	PADR
                     101 	PADS
                     167 	PADT 
                length
                    Type: INTEGER
                    Default: 0 
                Name
                    Type: string
                    A text name for the object. This attribute is required when you use stream block modifiers such as RangeModifier, RandomModifier, and TableModifier. See the description of the OffsetReference attribute for these modifier objects for more information.
                sessionId
                    Type: INTEGER
                    Default: 0
                type
                    Type: INTEGER
                    Default: 1
                version
                    Type: INTEGER
                    Default: 1

        Return value:
            N/A

        Example:
            llk_stc_config_streamblock_ccm_itf("streamblock1",messageType=1)

        """
        llk_log_begin_proc()

        # set default value
        dCfmHeader = {}
        dMaid = {}
        # stc_config_streamblock	name=US1	type=ccm	sequenceNumber=31983	maepi=1	cfmHeader.mdLevel=5	maid.mdnf=04	maid.smaf=02	maid.sman=6d61323030	maid.mdn=6d6435	ITU-TY1731=00000000000000000000000000000000	maid.padding=000000000000000000000000000000000000000000000000000000000000000000000000	
        kwargs.setdefault('sequenceNumber','31983')
        kwargs.setdefault('maepi','1')
        dCfmHeader.setdefault('mdlevel','5')
        dMaid.setdefault('mdnf','04')
        dMaid.setdefault('smaf','02')
        dMaid.setdefault('sman','6d61323030')
        dMaid.setdefault('mdn','6d6435')
        dMaid.setdefault('padding','000000000000000000000000000000000000000000000000000000000000000000000000')

        # delete ipv4 layer
        llk_log_debug("Delete ipv4 layer!")
        oIpv4Itf = self.llk_stc_send_command("stc::get " + oStreamblock + " -children-ipv4:IPv4")
        self.llk_stc_send_command("stc::get " + oIpv4Itf)
        self.llk_stc_send_command("stc::delete " + oIpv4Itf)

        # modify ethii layer
        llk_log_debug("Configured the ethernet interface to be CCM")
        oEthItf =  self.llk_stc_send_command("stc::get " + oStreamblock + " -children-ethernet:ethernetii")
        self.llk_stc_send_command("stc::config "+oEthItf+" -ethertype 8902"  )

        #create and configure ccm packet with the input parameters 
        llk_log_debug("Configure CCM interface!")
        self.llk_stc_send_command("stc::create serviceoam:CCM -under " + oStreamblock)
        oCcmPacket =  self.llk_stc_send_command("stc::get " + oStreamblock + " -children-serviceoam:CCM")
        sCommand = "stc::config " + oCcmPacket
        for key in kwargs:
            # skip cfmheader
            if 'cfmheader' in str(key) : 
                newKey = key.replace('cfmheader.','')
                dCfmHeader[newKey] = kwargs[key]
                continue
            # skip MAID
            if 'maid' in str(key) :
                newKey = key.replace('maid.','')
                dMaid[newKey] = kwargs[key]
                continue
           
            # others arguments will be configured on CCM interface
            sCommand = sCommand + " -" + key + " " + str(kwargs[key])

        self.llk_stc_send_command(sCommand)
        llk_log_info("Configured the CCM interface")

        #config cfmHeader
        if dCfmHeader : 
            llk_log_info("Configure the CfmHeader interface!")
            oCfmHeader = self.llk_stc_send_command("stc::create cfmHeader -under " + oCcmPacket)
            sCommand = "stc::config " + oCfmHeader
            for key in dCfmHeader.keys() :
                sCommand = sCommand + ' -' + key + " " + str(dCfmHeader[key])
            self.llk_stc_send_command(sCommand)    

        #config MAID
        if dMaid : 
            llk_log_info("Configure the MAID interface!")
            # create maid object
            oMaid = self.llk_stc_send_command("stc::create MAID -under " + oCcmPacket)

            #config theMDN and theMDNL
            # config format as MD Name Format as 'character string:4' 
            self.llk_stc_send_command("stc::config " + oMaid + ' -mdnf 04')
            oTheMdnl = self.llk_stc_send_command("stc::create theMDNL -under " + oMaid)
            oMdn_length = self.llk_stc_send_command("stc::create MDN_length -under " + oTheMdnl)

            oTheMdn = self.llk_stc_send_command("stc::create theMDN -under " + oMaid)
            oMaintain = self.llk_stc_send_command("stc::create MaintenanceDomainName -under " + oTheMdn)
            self.llk_stc_send_command("stc::config " + oMaintain + ' -mdn ' + dMaid.pop('mdn')  )

            # config padding
            oThePad = self.llk_stc_send_command("stc::create thePad -under " + oMaid)
            oPadding = self.llk_stc_send_command("stc::create padding -under " + oThePad)
            self.llk_stc_send_command("stc::config " + oPadding + ' -pad ' + dMaid.pop('padding')  )

            #config maid
            sCommand = "stc::config " + oMaid
            for key in dMaid :
                sCommand = sCommand + ' -' + key + " " + str(dMaid[key])
            self.llk_stc_send_command(sCommand)
        
        # configure EndTlv 
        llk_log_info("Configure the Endtlv interface!")
        oTheCcmTlvs = self.llk_stc_send_command("stc::create theCCMTLVS -under " + oCcmPacket)
        oCcmTlvList = self.llk_stc_send_command("stc::create CCMTLVList -under " + oTheCcmTlvs)
        oEndTlv = self.llk_stc_send_command("stc::create EndTlv -under " + oCcmTlvList)

        # configure the frame as auto length
        self.llk_stc_send_command("stc::config " + oStreamblock + ' -framelengthmode auto')
        llk_log_end_proc()


    def llk_stc_config_streamblock_lbr_itf(self,oStreamblock, **kwargs):
        """
        Configure the lbr interface for the streamblock

        Input Arguments:
            oStreamblock: Stream block object

            kwargs:


        Return value:
            N/A

        Example:
            llk_stc_config_streamblock_lbr_itf("streamblock1",messageType=1)

        """
        llk_log_begin_proc()

        # set default value
        dCfmHeader = {}
        # stc_config_streamblock	name=US1	type=ccm	sequenceNumber=31983	maepi=1	cfmHeader.mdLevel=5	maid.mdnf=04	maid.smaf=02	maid.sman=6d61323030	maid.mdn=6d6435	ITU-TY1731=00000000000000000000000000000000	maid.padding=000000000000000000000000000000000000000000000000000000000000000000000000	

        kwargs.setdefault('opcode','2')
        kwargs.setdefault('lbtid','4')
        dCfmHeader.setdefault('mdlevel','5')

        # delete ipv4 layer
        llk_log_debug("Delete ipv4 layer!")
        oIpv4Itf = self.llk_stc_send_command("stc::get " + oStreamblock + " -children-ipv4:IPv4")
        self.llk_stc_send_command("stc::get " + oIpv4Itf)
        self.llk_stc_send_command("stc::delete " + oIpv4Itf)

        # modify ethii layer
        llk_log_debug("Configured the ethernet interface to be LBR")
        oEthItf =  self.llk_stc_send_command("stc::get " + oStreamblock + " -children-ethernet:ethernetii")
        self.llk_stc_send_command("stc::config "+oEthItf+" -ethertype 8902"  )

        #create and configure lbr packet with the input parameters 
        llk_log_debug("Configure LBR interface!")
        self.llk_stc_send_command("stc::create serviceoam:LBR -under " + oStreamblock)
        oLBRPacket =  self.llk_stc_send_command("stc::get " + oStreamblock + " -children-serviceoam:LBR")
        #sCommand = "stc::config " + oLBRPacket

        sCommand = ''
        for key in kwargs:
            # skip cfmheader
            if 'cfmheader' in str(key) : 
                newKey = key.replace('cfmheader.','')
                dCfmHeader[newKey] = kwargs[key]
                continue
           
            # others arguments will be configured on LBR interface
            sCommand = sCommand + " -" + key + " " + str(kwargs[key])

        if sCommand:
            sCommand = "stc::config " + oLBRPacket + ' ' + sCommand
            self.llk_stc_send_command(sCommand)
            llk_log_info("Configured the LBR interface")

        #config cfmHeader
        if dCfmHeader : 
            llk_log_info("Configure the CfmHeader interface!")
            oCfmHeader = self.llk_stc_send_command("stc::create cfmHeader -under " + oLBRPacket)
            sCommand = "stc::config " + oCfmHeader
            for key in dCfmHeader.keys() :
                sCommand = sCommand + ' -' + key + " " + str(dCfmHeader[key])
            self.llk_stc_send_command(sCommand)    
 
        # configure EndTlv 
        llk_log_info("Configure the Endtlv interface!")
        oTheLBRTlvs = self.llk_stc_send_command("stc::create theLBRTLVS -under " + oLBRPacket)
        oLBRTlvList = self.llk_stc_send_command("stc::create LBMTLVList -under " + oTheLBRTlvs)
        oEndTlv = self.llk_stc_send_command("stc::create EndTlv -under " + oLBRTlvList)

        # configure senderIDTLV
        #oSenderIDTlv = self.llk_stc_send_command("stc::create SenderIDTLV -under " + oLBRTlvList)

        # configure the frame as auto length
        self.llk_stc_send_command("stc::config " + oStreamblock + ' -framelengthmode auto')
        llk_log_end_proc()

    def llk_stc_config_streamblock_ltm_itf(self,oStreamblock, **kwargs):
        """
        Configure the ltm interface for the streamblock
        Input Arguments:
            oStreamblock: Stream block object

            kwargs:
        Return value:
            N/A

        Example:
            llk_stc_config_streamblock_ltm_itf("streamblock1",messageType=1)

        """
        llk_log_begin_proc()
        # set default value
        dCfmHeader = {}

        kwargs.setdefault('opcode','5')
        dCfmHeader.setdefault('mdlevel','5')

        # delete ipv4 layer
        llk_log_debug("Delete ipv4 layer!")
        oIpv4Itf = self.llk_stc_send_command("stc::get " + oStreamblock + " -children-ipv4:IPv4")
        self.llk_stc_send_command("stc::get " + oIpv4Itf)
        self.llk_stc_send_command("stc::delete " + oIpv4Itf)
  
        # modify ethii layer
        llk_log_debug("Configured the ethernet interface to be LTM")
        oEthItf =  self.llk_stc_send_command("stc::get " + oStreamblock + " -children-ethernet:ethernetii")
        self.llk_stc_send_command("stc::config "+oEthItf+" -ethertype 8902"  )

        #create and configure ltm packet with the input parameters
        llk_log_debug("Configure LTM interface!")
        self.llk_stc_send_command("stc::create serviceoam:LTM -under " + oStreamblock)
        oLTMPacket =  self.llk_stc_send_command("stc::get " + oStreamblock + " -children-serviceoam:LTM")
        #sCommand = "stc::config " + oLTMPacket 

        sCommand = ''
        for key in kwargs:
            # skip cfmheader
            if 'cfmheader' in str(key) :
                newKey = key.replace('cfmheader.','')
                dCfmHeader[newKey] = kwargs[key]
                continue

            # others arguments will be configured on LTM interface
            sCommand = sCommand + " -" + key + " " + str(kwargs[key])

        if sCommand:
            sCommand = "stc::config " + oLTMPacket + ' ' + sCommand
            self.llk_stc_send_command(sCommand)
            llk_log_info("Configured the LTM interface")

        #config cfmHeader
        if dCfmHeader :
            llk_log_info("Configure the CfmHeader interface!")
            oCfmHeader = self.llk_stc_send_command("stc::create cfmHeader -under " + oLTMPacket)
            sCommand = "stc::config " + oCfmHeader
            for key in dCfmHeader.keys() :
                sCommand = sCommand + ' -' + key + " " + str(dCfmHeader[key])
            self.llk_stc_send_command(sCommand)

        # configure EndTlv
        llk_log_info("Configure the Endtlv interface!")
        oTheLTMTlvs = self.llk_stc_send_command("stc::create theLTMTLVS -under " + oLTMPacket)
        oLTMTlvList = self.llk_stc_send_command("stc::create LTMTLVList -under " + oTheLTMTlvs)
        oEndTlv = self.llk_stc_send_command("stc::create EndTlv -under " + oLTMTlvList)

        # configure senderIDTLV
        oSenderIDTlv = self.llk_stc_send_command("stc::create SenderIDTLV -under " + oLTMTlvList)

        # configure the frame as auto length
        self.llk_stc_send_command("stc::config " + oStreamblock + ' -framelengthmode auto')
        llk_log_end_proc()

    def llk_stc_config_streamblock_lbm_itf(self,oStreamblock, **kwargs):
        """
        Configure the lbm interface for the streamblock

        Input Arguments:
            oStreamblock: Stream block object

            kwargs:


        Return value:
            N/A

        Example:
            llk_stc_config_streamblock_lbm_itf("streamblock1",messageType=1)

        """
        llk_log_begin_proc()

        # set default value
        dCfmHeader = {}

        kwargs.setdefault('opcode','3')
        kwargs.setdefault('lbtid','4')
        dCfmHeader.setdefault('mdlevel','5')

        # delete ipv4 layer
        llk_log_debug("Delete ipv4 layer!")
        oIpv4Itf = self.llk_stc_send_command("stc::get " + oStreamblock + " -children-ipv4:IPv4")
        self.llk_stc_send_command("stc::get " + oIpv4Itf)
        self.llk_stc_send_command("stc::delete " + oIpv4Itf)

        # modify ethii layer
        llk_log_debug("Configured the ethernet interface to be LBM")
        oEthItf =  self.llk_stc_send_command("stc::get " + oStreamblock + " -children-ethernet:ethernetii")
        self.llk_stc_send_command("stc::config "+oEthItf+" -ethertype 8902"  )

        #create and configure lbm packet with the input parameters 
        llk_log_debug("Configure LBM interface!")
        self.llk_stc_send_command("stc::create serviceoam:LBM -under " + oStreamblock)
        oLBMPacket =  self.llk_stc_send_command("stc::get " + oStreamblock + " -children-serviceoam:LBM")
        #sCommand = "stc::config " + oLBMPacket

        sCommand = ''
        for key in kwargs:
            # skip cfmheader
            if 'cfmheader' in str(key) : 
                newKey = key.replace('cfmheader.','')
                dCfmHeader[newKey] = kwargs[key]
                continue
           
            # others arguments will be configured on LBM interface
            sCommand = sCommand + " -" + key + " " + str(kwargs[key])

        if sCommand:
            sCommand = "stc::config " + oLBMPacket + ' ' + sCommand
            self.llk_stc_send_command(sCommand)
            llk_log_info("Configured the LBM interface")

        #config cfmHeader
        if dCfmHeader : 
            llk_log_info("Configure the CfmHeader interface!")
            oCfmHeader = self.llk_stc_send_command("stc::create cfmHeader -under " + oLBMPacket)
            sCommand = "stc::config " + oCfmHeader
            for key in dCfmHeader.keys() :
                sCommand = sCommand + ' -' + key + " " + str(dCfmHeader[key])
            self.llk_stc_send_command(sCommand)    
 
        # configure EndTlv 
        llk_log_info("Configure the Endtlv interface!")
        oTheLBMTlvs = self.llk_stc_send_command("stc::create theLBMTLVS -under " + oLBMPacket)
        oLBMTlvList = self.llk_stc_send_command("stc::create LBMTLVList -under " + oTheLBMTlvs)
        oEndTlv = self.llk_stc_send_command("stc::create EndTlv -under " + oLBMTlvList)

        # configure senderIDTLV
        oSenderIDTlv = self.llk_stc_send_command("stc::create SenderIDTLV -under " + oLBMTlvList)

        # configure the frame as auto length
        self.llk_stc_send_command("stc::config " + oStreamblock + ' -framelengthmode auto')
        llk_log_end_proc()

    def llk_stc_streamblock_frameconfig(self,oStreamblock,**kwargs):

        llk_log_begin_proc()

        if oStreamblock:
            sCommand = "stc::config " + oStreamblock 
            for key in kwargs:
                sCommand = sCommand + " -" + key + " " + kwargs[key]
            self.llk_stc_send_command(sCommand)
            logger.debug("Successfully configured streamblock with frameconfig mode!")
        
        llk_log_end_proc()
        return oStreamblock 

    def llk_stc_create_streamblock_range_modifier(self,oStreamblock,**kwargs):
        """

        :return:
        """
        llk_log_begin_proc()

        oRangeModifier = self.llk_stc_send_command("stc::create rangemodifier -under " + oStreamblock)
        self.llk_stc_config_streamblock_range_modifier(oRangeModifier,**kwargs)

        llk_log_end_proc()
        return oRangeModifier

    def llk_stc_config_streamblock_range_modifier(self,oRangeModifier,**kwargs):
        """

        :return:
        """
        llk_log_begin_proc()
        sCommand = "stc::config " + oRangeModifier
        for key in kwargs:
            sCommand = sCommand + " -" + key + " " + str(kwargs[key])

        self.llk_stc_send_command(sCommand)
        llk_log_info("Successfully configured range modifier")
        llk_log_end_proc()

    def llk_stc_get_attributes(self, oObject, *args):
        """
        Return specified attributes of any STC object.  Returns all attributes if attribute is ommited.
        Args:
            oObject: Object reference
            *args:  Argument list of object attributes to retrieve.
        Returns:
            sResult: Command response
        Examples:
            llk_stc_get_attributes("streamblock1", "MinFrameLength")
        """
        llk_log_begin_proc()

        sCommand = "stc::get " + oObject
        for arg in args:
            sCommand = sCommand + " -" + str(arg)
        sResult = self.llk_stc_send_command(sCommand)

        llk_log_end_proc()
        return sResult

    def llk_stc_start_traffic(self,*args):
        """
        Starts traffic on ports or streamblocks
        Args:
            *args: List of port or streamblock object references.  All associated streamblocks will be started
            for any port arg.  If no args are given.  Traffic is started for all streamblocks.
        Returns:
            N/A
        Examples:
            llk_stc_start_traffic("streamblock1", "streamblock2")
        """

        # Get unique set of streamblocks
        llk_log_begin_proc()

        lPorts = set()
        lStreamblocks = set()

        if len(args) == 0:
            # If no args are given then get streamblocks for all ports
            lAllPorts = self.llk_stc_send_command("stc::get " + str(self.oStcProject) + " -children-port").split()

            for oPort in lAllPorts:
                lPortStreamblocks = self.llk_stc_send_command("stc::get " + oPort + " -children-streamblock").split()
                for oPortStreamblock in lPortStreamblocks:
                    lPorts.add(oPort)
                    lStreamblocks.add(oPortStreamblock)
        else:
            # Sort ports and streamblocks in arg list
            for arg in args:
                if re.compile("port.*").match(arg):
                    # Add port to set
                    lPorts.add(arg)
                    # Add port streamblocks to set
                    lPortStreamblocks = self.llk_stc_send_command("stc::get " + arg + " -children-streamblock").split()
                    for oPortStreamblock in lPortStreamblocks:
                        lStreamblocks.add(oPortStreamblock)
                elif re.compile("streamblock.*").match(arg):
                    # Add streamblock to set
                    lStreamblocks.add(arg)
                    # Add parent ports for to set
                    oPort = self.llk_stc_send_command("stc::get " + arg + " -parent")
                    #ostream = self.llk_stc_send_command("stc::get " + oPort + " -children-streamblock")
                    #self.llk_stc_send_command("stc::get " + ostream )
                    lPorts.add(oPort)

        # Start traffic for targeted streamblocks
        self.llk_stc_send_command("stc::apply")
        self.llk_stc_send_command("stc::perform StreamBlockStart -StreamBLockList \"" + ' '.join(list(lStreamblocks))+ "\"")
        # Check that generator is running for each identified port
        for oPort in list(lPorts):
            oGen = self.llk_stc_send_command("stc::get " + oPort + " -children-generator")

            bDone = False
            iRemainingAttempts = 12

            while iRemainingAttempts and not bDone:
                iRemainingAttempts -= 1
                sGenState = self.llk_stc_send_command("stc::get " + oGen + " -state")
                llk_log_info(oGen + " is " + sGenState)
                if re.compile("PENDING_.*").match(sGenState):
                    llk_log_debug("Waiting 5 seconds")
                    self.llk_stc_send_command("stc::perform Wait -WaitTime 5")
                else:
                    bDone = True

            if sGenState != "RUNNING":
                if iRemainingAttempts == 0:
                    llk_log_warn("Timed out waiting for " + oGen + " to start")
                else:
                    llk_log_warn("Failed to start " + oGen)
                llk_log_end_proc()
                raise RuntimeError
        llk_log_end_proc()

    def llk_stc_stop_traffic(self,*args):
        """
        Stops traffic on ports or streamblocks
        Args:
            *args: List of port or streamblock object references.  All associated streamblocks will be stopped
            for any port arg.  If no args are given.  Traffic is stopped for all streamblocks.
        Returns:
            N/A
        Examples:
            llk_stc_start_traffic("streamblock1", "streamblock2")
        """

        # Get unique set of streamblocks
        llk_log_begin_proc()

        lPorts = set()
        lStreamblocks = set()

        if len(args) == 0:
            # If no args are given then get streamblocks for all ports
            lAllPorts = self.llk_stc_send_command("stc::get " + str(self.oStcProject) + " -children-port").split()

            for oPort in lAllPorts:
                lPortStreamblocks = self.llk_stc_send_command("stc::get " + oPort + " -children-streamblock").split()
                for oPortStreamblock in lPortStreamblocks:
                    lPorts.add(oPort)
                    lStreamblocks.add(oPortStreamblock)
        else:
            # Sort ports and streamblocks in arg list
            for arg in args:
                if re.compile("port.*").match(arg):
                    # Add port to set
                    lPorts.add(arg)
                    # Add port streamblocks to set
                    lPortStreamblocks = self.llk_stc_send_command("stc::get " + arg + " -children-streamblock").split()
                    for oPortStreamblock in lPortStreamblocks:
                        lStreamblocks.add(oPortStreamblock)
                elif re.compile("streamblock.*").match(arg):
                    # Add streamblock to set
                    lStreamblocks.add(arg)
                    # Add parent ports for to set
                    oPort = self.llk_stc_send_command("stc::get " + arg + " -parent")
                    lPorts.add(oPort)

        # Start traffic for targeted streamblocks
        self.llk_stc_send_command("stc::perform StreamBlockStop -StreamBLockList \"" + ' '.join(list(lStreamblocks))+ "\"")

        # Check that generator is not pending for each identified port
        for oPort in list(lPorts):
            oGen = self.llk_stc_send_command("stc::get " + oPort + " -children-generator")

            bDone = False
            iRemainingAttempts = 12

            while iRemainingAttempts and not bDone:
                iRemainingAttempts -= 1
                sGenState = self.llk_stc_send_command("stc::get " + oGen + " -state")
                llk_log_info(oGen + " is " + sGenState)
                if re.compile("PENDING_.*").match(sGenState):
                    llk_log_debug("Waiting 5 seconds")
                    self.llk_stc_send_command("stc::perform Wait -WaitTime 5")
                else:
                    bDone = True

            if re.compile("PENDING_.*").match(sGenState):
                if iRemainingAttempts == 0:
                    llk_log_warn("Timed out waiting for " + oGen + " to stop")
                llk_log_end_proc()
                raise RuntimeError

        llk_log_end_proc()

    def llk_stc_start_capture(self,oPort):
        """
        Start capture on specified port
        Args:
            oPort: Port object reference
        Returns:
            N/A
        Examples:
            llk_stc_start_capture(port1)
        """
        llk_log_begin_proc()

        oCapture = self.llk_stc_send_command("stc::get " + oPort + " -children-capture")
        self.llk_stc_send_command("stc::perform captureStart -captureProxyId " + oCapture)
        llk_log_debug("Sucessfully started capture!!")
        llk_log_end_proc()

    def llk_stc_stop_capture_without_parse_pkt(self,oPort):
        """
        Stop capture on specified port
        Args:
            oPort: Port object reference
        Returns:
            N/A
        Examples:
            llk_stc_stop_capture(port1)
        """

        llk_log_begin_proc()

        oCapture = self.llk_stc_send_command("stc::get " + oPort + " -children-capture")
        self.llk_stc_send_command("stc::perform captureStop -captureProxyId " + oCapture)

        # Associate Ports with their corresponding capture ids
        self.dPortCaptureId[oPort] = oCapture
        llk_log_debug(self.dPortCaptureId)

        # Only save if pkts were captured
        iPktCount = int(self.llk_stc_send_command("stc::get " + oCapture + " -PktCount"))
        if iPktCount > 0:
            sFilePrefix = oCapture + time.strftime("_%m-%d-%Y_%H-%M-%S")
            sPcapName = sFilePrefix + ".pcap"
            self.llk_stc_send_command("stc::perform captureDataSave -captureProxyId " + oCapture +
                                      " -filenameFormat PCAP -filename \"" + sPcapName + "\" -filenamepath " + str(self.TRAF_OUTPUT_DIR))
            self.dCapturePcap[oPort] = self.TRAF_OUTPUT_DIR + '/' + sPcapName
            #self.dCapturePcap[oPort] = sPcapName
            llk_log_info("Successfully stopped the capture on ports!!")
        else:
            llk_log_info("No packets were captured")

        llk_log_end_proc()

    def llk_stc_parse_pdml(self,oPort,pdmlName,parse_count=1):
        """
        Parse pdmp file to dict and set this dict to self.dCapturePkt[oCapture] 
        Args:
            oPort: Port object reference
            pdmlPath: the path of pdml file
        Returns:
            N/A
        Examples:
            llk_stc_parse_pdml(port1,pdmlName)
        """
        llk_log_begin_proc()

        try :
            oCapture = self.llk_stc_send_command("stc::get " + oPort + " -children-capture")
            self.llk_stc_send_command("stc::perform captureStop -captureProxyId " + oCapture)
 
            # Associate Ports with their corresponding capture ids
            self.dPortCaptureId[oPort] = oCapture
            llk_log_debug(self.dPortCaptureId)

            self.dCapturePkt[oCapture] = llk_traffic.llk_traffic_parse_pdml(pdmlName,str(self.TRAF_OUTPUT_DIR))
            llk_log_debug(self.dCapturePkt)
            llk_log_debug("Successfully parse pdml file : %s !!" % pdmlName)

        except Exception as inst:
            s = sys.exc_info()
            raise AssertionError("llk_stc_parse_pdml->Exception:%s, Line:%s" % (inst,s[2].tb_lineno))
        llk_log_end_proc()

    def llk_stc_stop_capture(self,oPort):
        """
        Stop capture on specified port
        Args:
            oPort: Port object reference
        Returns:
            N/A
        Examples:
            llk_stc_stop_capture(port1)
        """

        llk_log_begin_proc()

        oCapture = self.llk_stc_send_command("stc::get " + oPort + " -children-capture")
        self.llk_stc_send_command("stc::perform captureStop -captureProxyId " + oCapture)

        # Associate Ports with their corresponding capture ids
        self.dPortCaptureId[oPort] = oCapture
        llk_log_debug(self.dPortCaptureId)

        # Only save if pkts were captured
        iPktCount = int(self.llk_stc_send_command("stc::get " + oCapture + " -PktCount"))
        if iPktCount > 0:
            sFilePrefix = oCapture + time.strftime("_%m-%d-%Y_%H-%M-%S")
            sPcapName = sFilePrefix + ".pcap"
            self.llk_stc_send_command("stc::perform captureDataSave -captureProxyId " + oCapture +
                                      " -filenameFormat PCAP -filename \"" + sPcapName + "\" -filenamepath " + str(self.TRAF_OUTPUT_DIR))
            
            llk_traffic.llk_traffic_pcap_to_pdml(sPcapName,str(self.TRAF_OUTPUT_DIR))
            self.dCapturePkt[oCapture] = llk_traffic.llk_traffic_parse_pdml(sPcapName[:-5]+".pdml",str(self.TRAF_OUTPUT_DIR))
            self.dCapturePcap[oPort] = self.TRAF_OUTPUT_DIR + '/' + sPcapName
            #self.dCapturePcap[oPort] = sPcapName
            llk_log_debug(self.dCapturePkt)
            llk_log_info("Successfully stopped the capture on ports!!")
        else:
            llk_log_info("No packets were captured")

        llk_log_end_proc()


    def _llk_stc_is_valid_mac_address(self,sMacAddr):
        """
        Validate if the format of mac address is like 00:00:00:11:22:33
        Args:
            sMacAddr: the mac address string
        Return: 
            True or False
        """
        valid = re.compile(r"[A-Fa-f0-9]{2}:[A-Fa-f0-9]{2}:[A-Fa-f0-9]{2}:[A-Fa-f0-9]{2}:[A-Fa-f0-9]{2}:[A-Fa-f0-9]{2}",
                re.VERBOSE|re.IGNORECASE)

        if valid.match(sMacAddr) :
            return True
        else :
            return False

    def _llk_stc_format_priority(self,iPriority):
        """
        Converts priority bit to a 3 digit binary value (6 -> 110)
        Args:
            iPriority: Priority as a string.  Accepts either binary or integer. Returns value as a binary string.
        Returns: Value as 3 digit binary string (Ex: 110)
        Examples:
            _llk_stc_format_priority(6)
        """

        # Note: It's expected that iPriority is a string input.  Accepting integer inputs can be problematic since input
        # with a leading "0" is interpreted as octal.  This is similar to hex with a leading "x".
        # Consequently 010 as an integer input would be interpreted at 8
        llk_log_begin_proc()

        if set(str(iPriority)) <= set('10'):
            # Value is already in binary format
            llk_log_end_proc()
            return iPriority
        else:
            # Format in binary.  Ex: 2 -> 010
            llk_log_end_proc()
            return '{0:03b}'.format(int(iPriority))

    def llk_stc_delete(self,oObj):
        """
        Deletes the STC object created.
        Args:
            oObj: The STC object to be deleted
        Return:
            N/A
        """
        llk_log_begin_proc()

        if oObj:
            self.llk_stc_send_command("stc::delete " + oObj)
            llk_log_info("STC object "+ oObj +" successfully deleted")
        else:
            llk_log_info("STC object to delete not found!!")

        llk_log_end_proc()

    def llk_stc_subscribe(self,**kwargs):
        """
        Input Args:
            ConfigType
                Configuration object under which you want the results.
                Type: string
                Default: "" (empty string)
            DisablePaging
                Type: bool
                Default: FALSE
                Possible Values:
                Value Description
                TRUE
                FALSE
            FileNamePrefix
                Type: string
                Default: "" (empty string)
            FilterList Type: handle
                Default: 0
            Interval
                Type: u32
                Default: 1
            OutputChildren
                Type: bool
                Default: FALSE
                Possible Values:
                Value Description
                TRUE
                FALSE
            OutputFormat
                Filename extension of the output filename.
                Type: string
                Default: csv
            Parent
                This must be the handle of the Project object.
                Type: handle
                Default: 0
            RecordsPerPage
                Type: u16
                Default: 100
            ResultParent
                Object in the configuration under which you want results. If you want results for every port, use the Project object.
                If you want results for just specific ports, use the port handles.
                Type: handle
                Default: 0
            ResultType
                Results object type to use. There are many of these to choose from. Some examples: AnalyzerPortResults,
                PppoeSessionResults, Ospfv2RouterResults.
                Type: string
                Default: "" (empty string)
            ViewAttributeList
                Type: string
                Default: "" (empty string)
            ViewName
                Type: string
                Default: "" (empty string)
        Return value:
            Result data set object.
        Example:
            llk_stc_subscribe (resultType=TxStreamBlockResults,configType=StreamBlock,filenamePrefix=sb1)
        """

        llk_log_begin_proc()

        lArgs = ["resultType","configType"]
        bFlag = False

        for arg in lArgs:
            if arg in kwargs.keys():
                continue
            else:
                bFlag = True
                llk_log_warn("Required argument %s is not provided" % arg)

        if bFlag:
            llk_log_info("Exiting measure subscribe unsuccessfully!!")
            llk_log_end_proc()
            return
        else:
            # configure ResultOptions including LATENCY_JITTER
            oResultOptions = self.llk_stc_send_command("stc::get project1 -children-ResultOptions" )
            self.llk_stc_send_command("stc::get %s" % oResultOptions)
            self.llk_stc_send_command("stc::config %s -ResultViewMode \"LATENCY_JITTER\" -JitterMode \"RFC4689ABSOLUTEVALUE\"" % oResultOptions) 
            self.llk_stc_send_command("stc::get %s" % oResultOptions)
            self.llk_stc_send_command("stc::apply")

            sCommand = "stc::subscribe -Parent " + str(self.oStcProject)
            for key in kwargs:
                sCommand = sCommand + " -" + key + " " + str(kwargs[key])
            oResultDataSet = self.llk_stc_send_command(sCommand)
            llk_log_debug("Successfully subscribed to the measurement!!")
            llk_log_end_proc()
            return oResultDataSet

    def llk_stc_measure_subflow(self,oResultDataSet,*args,**kwargs):
        """
        Input Args:
            ResultDataSet:
                ResultDataSet handle returned from the corresponding subscribe command.
                Type: handle
                Default: 0
            *args:
                This changes depending on the type of results(-resultType) used to collect data in ResultDataSet.
                For example, if the resultType is RxStreamSummaryResults, the following parameters are some that can be
                queried from the test data.
                AvgInterarrival
                    Time Average inter-arrival time measured in 1 microsecond units.
                    Type: double
                    Default: 0
                AvgJitter
                    Type: double
                    Default: 0
                AvgLatency
                    Average transfer delay measured in 1 microsecond units.
                    Type: double
                    Default: 0
                BitCount
                    Count of total bits received.
                    Type: u64
                    Default: 0
                BitRate
                    Rate at which bits are received.
                    Type: u64
                    Default: 0
                CellCount
                    Count of total cells received.
                    Type: u64
                    Default: 0
                CellRate
                    Rate at which cells are received.
                    Type: u64
                    Default: 0
                CounterTimestamp
                    Time when the counter was stored.
                    Type: u64
                    Default: 0
                    DroppedFrameCount
                    Number of frames dropped in transit.
                    Type: u64
                    Default: 0
                DroppedFramePercent
                    Percentage of dropped frames.
                    Type: double
                    Default: 0
                DroppedFramePercentRate
                    Percentage rate of the dropped frame.
                    Type: double
                    Default: 0
            **kwargs:
                RateSleepTime: Time to wait before collecting the rate values
                Type: INTEGER
                Default: 0 seconds
                PageSleepTime: Time to wait after configuring the page
                Type: INTEGER
                Default: 1 second
        Return:
            ddMeasureData:
            Nested dictionary with each key value pair corresponding to a subflow of a streamblock.
            The key is the name of the streamblock appended with the value of subflow with it.The value is a dictionary
            with all the data related to the parameters queried from the test result data

            For example if the name of the streamblcok is US2 which has only one streamblock without any subflows in it.
            The entry in the dictionary would be as follows.
            'US2:0' --> Entry for aggregate streamblock results
            'US2:1' --> Entry for streamblock parameters collected from test data.

            'US2:0': {'flowcount': '1'}
            'US2:1': {'prbsbiterrorratio': '0', 'droppedframepercent': '0', 'droppedframecount': '0', 'reorderedframecount': '0',
                    'lateframecount': '0', 'framecount': '100', 'bitrate': '936', 'page': '7', 'prbsbiterrorcount': '0'},

            If there are multiple subflows associated with a streamblock, then there would be multiple entries each corresponding
            to a subflow within the streamblock. In the below example, the streamblock name is DS1 and there are three subflows

            'DS1:0': {'flowcount': '3'}
            'DS1:1': {'prbsbiterrorratio': '0', 'droppedframepercent': '0', 'droppedframecount': '0', 'reorderedframecount': '0',
                    'lateframecount': '0', 'framecount': '9', 'bitrate': '0', 'page': '2', 'prbsbiterrorcount': '0'},
            'DS1:2': {'prbsbiterrorratio': '0', 'droppedframepercent': '0', 'droppedframecount': '0', 'reorderedframecount': '0',
                    'lateframecount': '0', 'framecount': '8', 'bitrate': '7496', 'page': '1', 'prbsbiterrorcount': '0'},
            'DS1:3': {'prbsbiterrorratio': '0', 'droppedframepercent': '0', 'droppedframecount': '0', 'reorderedframecount': '0',
                    'lateframecount': '0', 'framecount': '8', 'bitrate': '0', 'page': '1', 'prbsbiterrorcount': '0'},

        Example:
            llk_stc_measure_subflow("resultdataset1",BitRate,FrameCount,DroppedFrameCount,ReorderedFrameCount,LateFrameCount)
        """

        llk_log_begin_proc()

        dMeasureEachSubFlow = {}
        ddMeasureAllSubFlows = {}
        iSubFlow = 0
        iRateSleepTime=0
        iPageSleepTime=1

        #Updating wait time for collecting rates, if this value is not given in the input then the test will not wait
        #for the values like bitrates
        if 'RateSleepTime' in kwargs.keys():
            iRateSleepTime = kwargs['RateSleepTime']

        #Updating wait time for page, this is necessary sleep time to be given inorder to configure the page
        if 'PageSleepTime' in kwargs.keys():
            iPageSleepTime = kwargs['PageSleepTime']

        #Total number of pages collected
        iTotalpages = self.llk_stc_send_command("stc::get "+oResultDataSet+ " -TotalPageCount")

        #Traverse through each page collected
        for page in range(1,int(iTotalpages)+1):
            self.llk_stc_send_command("stc::config "+oResultDataSet+ " -PageNumber " + str(page))
            self.llk_stc_send_command("stc::sleep "+ str(iPageSleepTime))
            llk_log_debug("The page currently configured is "+str(page))
            sResultHandles = self.llk_stc_send_command("stc::get "+oResultDataSet+ " -ResultHandleList")
            lResultHandles = sResultHandles.split()

            #timer value to check the value of bitrate periodically in between the sleep time
            iTimer = 0
            #Wait time for collecting the rates like -BitRate. This is done for each subflow
            if iRateSleepTime:
                llk_log_info("Waiting for Rates to be collected!!")
                while iTimer <= int(iRateSleepTime):
                    if int(self.llk_stc_send_command("stc::get "+ lResultHandles[0] +" -BitRate")) != 0:
                        llk_log_info("Rates are collected, exiting sleep!!")
                        break
                    else:
                        time.sleep(1)
                        iTimer += 1

            #Traverse through each handle collected in a page
            for handle in lResultHandles:
                sParentSB = self.llk_stc_send_command("stc::get "+ handle +" -parent")
                sParentSBName = self.llk_stc_send_command("stc::get "+ sParentSB +" -Name")
                sSBName = sParentSBName+":"

                if sSBName in ddMeasureAllSubFlows.keys():
                    #If the streamblock already exists, increment the subflow value
                    iSubFlow += 1
                else:
                    #This is a new streamblock, hence make the subflow value as 1
                    iSubFlow = 1
                    dMeasureEachSubFlow['flowcount'] = self.llk_stc_send_command("stc::get "+ sParentSB +" -FlowCount")
                    ddMeasureAllSubFlows[sParentSBName+":0"] = dMeasureEachSubFlow.copy()
                    dMeasureEachSubFlow.clear()

                sSubFlowkey = sParentSBName+":"+str(iSubFlow)
                dMeasureEachSubFlow['page'] = str(page)

                if args:
                    sCommand = "stc::get " + handle
                    for param in args:
                        sCommand += " -" + param
                    sReturnVal = self.llk_stc_send_command(sCommand)
                    lReturnVal = sReturnVal.split()
                    iLenList = len(lReturnVal)
                    if iLenList == 1:
                        dMeasureEachSubFlow[str(param)] = sReturnVal
                    else:
                        for i in range(0,iLenList,2):
                            dMeasureEachSubFlow[str(lReturnVal[i])[1:]] = lReturnVal[i+1]

                ddMeasureAllSubFlows[sSubFlowkey] = dMeasureEachSubFlow.copy()
                dMeasureEachSubFlow.clear()

        llk_log_end_proc()
        return ddMeasureAllSubFlows

    def llk_stc_measure_port(self,oResultDataSet,*args,**kwargs):
        """
        Input Args:
            ResultDataSet:
                ResultDataSet handle returned from the corresponding subscribe command.
                Type: handle
                Default: 0
            *args:
                This changes depending on the type of results(-resultType) used to collect data in ResultDataSet.
                For example, if the resultType is RxStreamSummaryResults, the following parameters are some that can be
                queried from the test data.
                AvgInterarrival
                    Time Average inter-arrival time measured in 1 microsecond units.
                    Type: double
                    Default: 0
                AvgJitter
                    Type: double
                    Default: 0
                AvgLatency
                    Average transfer delay measured in 1 microsecond units.
                    Type: double
                    Default: 0
                BitCount
                    Count of total bits received.
                    Type: u64
                    Default: 0
                BitRate
                    Rate at which bits are received.
                    Type: u64
                    Default: 0
                CellCount
                    Count of total cells received.
                    Type: u64
                    Default: 0
                CellRate
                    Rate at which cells are received.
                    Type: u64
                    Default: 0
                CounterTimestamp
                    Time when the counter was stored.
                    Type: u64
                    Default: 0
                    DroppedFrameCount
                    Number of frames dropped in transit.
                    Type: u64
                    Default: 0
                DroppedFramePercent
                    Percentage of dropped frames.
                    Type: double
                    Default: 0
                DroppedFramePercentRate
                    Percentage rate of the dropped frame.
                    Type: double
                    Default: 0
            **kwargs:
                RateSleepTime: Time to wait before collecting the rate values
                Type: INTEGER
                Default: 0 seconds
                PageSleepTime: Time to wait after configuring the page
                Type: INTEGER
                Default: 1 second
        Return:
            ddMeasureData:
            Nested dictionary with each key value pair corresponding to a subflow of a streamblock.
            The key is the name of the streamblock appended with the value of subflow with it.The value is a dictionary
            with all the data related to the parameters queried from the test result data

            For example if the name of the streamblcok is US2 which has only one streamblock without any subflows in it.
            The entry in the dictionary would be as follows.
            'US2:0' --> Entry for aggregate streamblock results
            'US2:1' --> Entry for streamblock parameters collected from test data.

            'US2:0': {'flowcount': '1'}
            'US2:1': {'prbsbiterrorratio': '0', 'droppedframepercent': '0', 'droppedframecount': '0', 'reorderedframecount': '0',
                    'lateframecount': '0', 'framecount': '100', 'bitrate': '936', 'page': '7', 'prbsbiterrorcount': '0'},

            If there are multiple subflows associated with a streamblock, then there would be multiple entries each corresponding
            to a subflow within the streamblock. In the below example, the streamblock name is DS1 and there are three subflows

            'DS1:0': {'flowcount': '3'}
            'DS1:1': {'prbsbiterrorratio': '0', 'droppedframepercent': '0', 'droppedframecount': '0', 'reorderedframecount': '0',
                    'lateframecount': '0', 'framecount': '9', 'bitrate': '0', 'page': '2', 'prbsbiterrorcount': '0'},
            'DS1:2': {'prbsbiterrorratio': '0', 'droppedframepercent': '0', 'droppedframecount': '0', 'reorderedframecount': '0',
                    'lateframecount': '0', 'framecount': '8', 'bitrate': '7496', 'page': '1', 'prbsbiterrorcount': '0'},
            'DS1:3': {'prbsbiterrorratio': '0', 'droppedframepercent': '0', 'droppedframecount': '0', 'reorderedframecount': '0',
                    'lateframecount': '0', 'framecount': '8', 'bitrate': '0', 'page': '1', 'prbsbiterrorcount': '0'},

        Example:
            llk_stc_measure_port("resultdataset1",BitRate,FrameCount,DroppedFrameCount,ReorderedFrameCount,LateFrameCount)
        """

        llk_log_begin_proc()

        dMeasureEachSubFlow = {}
        ddMeasureAllSubFlows = {}
        iSubFlow = 0
        iRateSleepTime=0
        iPageSleepTime=1

        #Updating wait time for collecting rates, if this value is not given in the input then the test will not wait
        #for the values like bitrates
        if 'RateSleepTime' in kwargs.keys():
            iRateSleepTime = kwargs['RateSleepTime']

        #Updating wait time for page, this is necessary sleep time to be given inorder to configure the page
        if 'PageSleepTime' in kwargs.keys():
            iPageSleepTime = kwargs['PageSleepTime']

        #Total number of pages collected
        iTotalpages = self.llk_stc_send_command("stc::get "+oResultDataSet+ " -TotalPageCount")

        #Traverse through each page collected
        for page in range(1,int(iTotalpages)+1):
            self.llk_stc_send_command("stc::config "+oResultDataSet+ " -PageNumber " + str(page))
            self.llk_stc_send_command("stc::sleep "+ str(iPageSleepTime))
            llk_log_debug("The page currently configured is "+str(page))
            sResultHandles = self.llk_stc_send_command("stc::get "+oResultDataSet+ " -ResultHandleList")
            lResultHandles = sResultHandles.split()

            #timer value to check the value of bitrate periodically in between the sleep time
            iTimer = 0
            #Wait time for collecting the rates like -BitRate. This is done for each subflow
            if iRateSleepTime:
                llk_log_info("Waiting for Rates to be collected!!")
                while iTimer <= int(iRateSleepTime):
                    if int(self.llk_stc_send_command("stc::get "+ lResultHandles[0] +" -BitRate")) != 0:
                        llk_log_info("Rates are collected, exiting sleep!!")
                        break
                    else:
                        time.sleep(1)
                        iTimer += 1

            #Traverse through each handle collected in a page
            for handle in lResultHandles:
                sParentSB = self.llk_stc_send_command("stc::get "+ handle +" -parent")
                sParentSBName = self.llk_stc_send_command("stc::get "+ sParentSB +" -Name")
                sSBName = sParentSBName+":"

                dMeasureEachSubFlow['page'] = str(page)

                if args:
                    sCommand = "stc::get " + handle
                    for param in args:
                        sCommand += " -" + param

                    sCommand += " -PortUiName"
 
                    sReturnVal = self.llk_stc_send_command(sCommand)
                    # set the last value of PortUiName from '{//135.251.247.12/2/2 //2/2}' to '{//135.251.247.12/2/2'
                    sReturnVal = re.sub(' \S+$','',sReturnVal)
                    lReturnVal = sReturnVal.split()
                    iLenList = len(lReturnVal)
                    if iLenList == 1:
                        dMeasureEachSubFlow[str(param)] = sReturnVal
                    else:
                        for i in range(0,iLenList,2):
                            dMeasureEachSubFlow[str(lReturnVal[i])[1:]] = lReturnVal[i+1]
                ddMeasureAllSubFlows[sParentSB] = dMeasureEachSubFlow.copy()
                dMeasureEachSubFlow.clear()

        llk_log_end_proc()
        return ddMeasureAllSubFlows

    def llk_stc_clear_results(self,*args):
        """
        This function clears the results on the specified ports.

        Input:
            args: List of the ports whose results are to be deleted!!

        Return:
            N/A

        Example:
            llk_stc_clear_results("port1","port2","port3")
        """
        llk_log_begin_proc()

        if args:
            sCommand = "stc::perform ResultsClearAll -PortList \""
            for param in args:
                sCommand += param + " "
            sCommand += "\""
            self.llk_stc_send_command(sCommand)
            llk_log_info("Results were deleted successfully on the specified ports!!")
        else:
            llk_log_info("No port handles given to delete the results!!")

        llk_log_end_proc()

    def llk_stc_unsubscribe(self,oResultDataSet):
        """
        Input Args:
            ResultDataSet:
                ResultDataSet handle returned from the corresponding subscribe command.
                Type: handle
                Default: 0
        Return Value:
            N/A
        Example:
            llk_stc_unsubscribe(ResultDataSet1)

        """
        llk_log_begin_proc()

        self.llk_stc_send_command("stc::unsubscribe " + oResultDataSet)
        llk_log_debug("Successfully unsubscribed to the measurement!!")

        llk_log_end_proc()


    def _llk_stc_get_field_value(self,oStreamblock,sType,sField):
        """

        :return:
        """
        llk_log_begin_proc()
        oItf = self.llk_stc_send_command("stc::get " + oStreamblock + " -children-" + str(sType))
        sValue = self.llk_stc_send_command("stc::get " + oItf + " -" + sField)
        llk_log_end_proc()
        return sValue

    def _llk_stc_get_vlan_field_value(self,oStreamblock,sVlan,sField):
        """

        :param oStreamblock:
        :param sVlan:
        :return:
        """
        llk_log_begin_proc()
        oEthItf = self.llk_stc_send_command("stc::get " + oStreamblock + " -children-ethernet:ethernetii")
        oVlansItf = self.llk_stc_send_command("stc::get " + oEthItf + " -children-vlans")
        lVlanItf = str.split(str(self.llk_stc_send_command("stc::get " + oVlansItf + " -children-vlan")))
        iVlanId = int(sVlan.replace('vlan',''))
        sVlanObject = lVlanItf[iVlanId-1]
        sVlanObjectName = self.llk_stc_send_command("stc::get " + sVlanObject + " -" + sField)
        llk_log_end_proc()
        return sVlanObjectName

    def _llk_stc_get_vlan_field_name(self,oStreamblock,sVlan):
        """

        :param oStreamblock:
        :param sVlan:
        :return:
        """
        llk_log_begin_proc()
        oEthItf = self.llk_stc_send_command("stc::get " + oStreamblock + " -children-ethernet:ethernetii")
        sEthItfName = self.llk_stc_send_command("stc::get " + oEthItf + " -name")
        oVlansItf = self.llk_stc_send_command("stc::get " + oEthItf + " -children-vlans")
        lVlanItf = str.split(str(self.llk_stc_send_command("stc::get " + oVlansItf + " -children-vlan")))
        iVlanId = int(sVlan.replace('vlan',''))
        sVlanObject = lVlanItf[iVlanId-1]
        sVlanObjectName = self.llk_stc_send_command("stc::get " + sVlanObject + " -name")
        sVlanReferenceName = sEthItfName+".vlans."+sVlanObjectName
        llk_log_end_proc()
        return sVlanReferenceName

    def llk_stc_save_tcc_config(self,filePath):
        """
        Save all configuration to STC tcc file
        """       
        llk_log_begin_proc()
        self.llk_stc_send_command("stc::perform SaveToTcc -filename %s" % filePath)
        llk_log_end_proc()

    def llk_stc_save_config_file(self):
        """
        Save config file to debug
        :return:
        """
        llk_log_begin_proc()
        self.llk_stc_send_command("stc::perform saveAsXml -config system1 -filename /repo/user/abvenkat/robot-cft/debug.xml")
        llk_log_end_proc()

    def llk_stc_config_ieee1588v2Clock(self,oDevice='',oClockConfig='',**kwargs):
        """
        Config ieee1588v2ClockConfig
        """

        llk_log_begin_proc()

        if oClockConfig:
            sCommand = "stc::config " + oClockConfig
            for key in kwargs:
                sCommand = sCommand + " -" + key + " " + kwargs[key]
            self.llk_stc_send_command(sCommand)
            logger.info("Successfully configured Ieee1588v2ClockConfig")
            llk_log_end_proc()
            return oClockConfig
        else:
            sCommand = "stc::create Ieee1588v2ClockConfig -under " + str(oDevice)
            for key in kwargs:
                sCommand = sCommand + " -" + key + " " + kwargs[key]
            oItf = self.llk_stc_send_command(sCommand)
            logger.info("Successfully created Ieee1588v2ClockConfig")
            llk_log_end_proc()
            return oItf

    def llk_stc_retrive_ieee1588v2Clock_state(self,oClock):
        """
        Retrieve state of a ieee1588v2ClockConfig object.

        Args:
            oClock - ieee1588v2ClockConfig object
                Type : String
                Default : ""

        Return:
            sClockState - the state of ieee1588v2ClockConfig
                Type : String
                Default : ""
                Values: IEEE1588_STATE_NONE,IEEE1588_STATE_INITIALIZING,IEEE1588_STATE_FAULTY,IEEE1588_STATE_DISABLED,IEEE1588_STATE_LISTENING,IEEE1588_STATE_PRE_MASTER,IEEE1588_STATE_MASTER,IEEE1588_STATE_PASSIVE,IEEE1588_STATE_UNCALIBRATED	,IEEE1588_STATE_SLAVE	

        Example:
            llk_stc_retrieve_ieee1588v2Clock_state(oClock)
        """
        llk_log_begin_proc()
        sClockState = self.llk_stc_send_command("stc::get " + oClock + " -ClockState")
        llk_log_end_proc()
        return sClockState

    def llk_stc_verify_device_state(self,oDevice,configName,**kwargs):
        """
        Verify state of a device object.

        Args:
            oDevice(required) - Device object
                Type : String
            sClockState(required) - the expected state of ieee1588v2ClockConfig
                Type : String
                Values: IEEE1588_STATE_NONE,IEEE1588_STATE_INITIALIZING,IEEE1588_STATE_FAULTY,IEEE1588_STATE_DISABLED,IEEE1588_STATE_LISTENING,IEEE1588_STATE_PRE_MASTER,IEEE1588_STATE_MASTER,IEEE1588_STATE_PASSIVE,IEEE1588_STATE_UNCALIBRATED       ,IEEE1588_STATE_SLAVE

        Return:
            True or False 

        Example:
            llk_stc_verify_device_state(self,oClock,'IEEE1588_STATE_MASTER')
        """
        llk_log_begin_proc()
        sResult = False 

        oConfigObj = self.llk_stc_send_command("stc::get " + str(oDevice) + " -children-" + configName)

        if oConfigObj :
            if len(kwargs) > 1 :
                llk_log_end_proc()
                raise AssertionError("Only one expect state can be inputted:%s" % kwargs)

            for eachKey in kwargs.keys():
                sExpectState = kwargs[eachKey] 
                sCommand = "stc::get " + oConfigObj + ' -' + eachKey  
                sRealStatue = self.llk_stc_send_command(sCommand)
                sCommand = "stc::get " + oConfigObj 
                self.llk_stc_send_command(sCommand)

                if sExpectState.lower() == sRealStatue.lower() :
                    sResult = True
                    break

        """
        # DELETED
        sCommand = "stc::get project1"
        self.llk_stc_send_command(sCommand)
        sCommand = "stc::get emulateddevice1"
        self.llk_stc_send_command(sCommand)
        sCommand = "stc::get port1"
        self.llk_stc_send_command(sCommand)
        sCommand = "stc::get port2"
        self.llk_stc_send_command(sCommand)
        """
                                       
        llk_log_end_proc()
        if not sResult:
            raise AssertionError("Device state %s is not as expected %s" % (
            sRealStatue.lower(),sExpectState.lower()))

        return sResult

    def llk_stc_verify_ipv6arpnd_state(self,oDevice,configName,**kwargs):
        """
        Verify state of a device object.
        """
        llk_log_begin_proc()
        sResult = False 

        oConfigObj = self.llk_stc_send_command("stc::get " + str(oDevice) + " -Ipv6If.GatewayMacResolveState")

        if oConfigObj :
            if len(kwargs) > 1 :
                llk_log_end_proc()
                raise AssertionError("Only one expect state can be inputted:%s" % kwargs)

            for eachKey in kwargs.keys():
                sExpectState = kwargs[eachKey] 
                sCommand = "stc::get " + str(oDevice) + ' -' + configName  
                sRealStatue = self.llk_stc_send_command(sCommand)
                #sCommand = "stc::get " + oConfigObj 
                #self.llk_stc_send_command(sCommand)

                if sExpectState.lower() == sRealStatue.lower() :
                    sResult = True
                    break
                                      
        llk_log_end_proc()
        if not sResult:
            raise AssertionError("Device state %s is not as expected %s" % (
            sRealStatue.lower(),sExpectState.lower()))

        return sResult

