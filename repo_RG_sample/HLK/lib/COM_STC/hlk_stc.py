#!/usr/bin/env python

import os,sys,time,re
from ctypes import *

try:
    for lib_path in [\
    os.environ['ROBOTREPO'] +'/HLKS',
    os.environ['ROBOTREPO'] +'/LIBS/COM_STC',
    os.environ['ROBOTREPO'] +'/LIBS/DATA_TRANSLATION'] :
        if lib_path not in sys.path:
            sys.path.append(lib_path)
except Exception as inst:
    raise AssertionError("%s -> Fail to set sys.path, %s" % (__name__,inst))

import llk_stc
import hlk_traffic
from robot.libraries.BuiltIn import BuiltIn
from robot.api import logger
import data_translation
import llk_log

STC_SESSION = {}
# List of multiple measure retrive results
lMULTI_RETRIEVE_RESULTS = {} 
# List of latest measure retrieve result
lLATEST_RESULT = {}
dCLIENT_PORT = {}
dNETWORK_PORT = {}
dDevice = {}

sMEASURE_RESULT_TYPE = 'RxStreamSummaryResults'


# define protocol content
dPROTOCOL_MAP_TO_LAYER = {
  'eth':'ethii,vlan,load,frame',
  'arp':'ethii,vlan,arp,load,frame',
  'rarp':'ethii,vlan,arp,load,frame',
  'pppoe':'ethii,vlan,pppoe,load,frame',
  'mpls_ip':'ethii,vlan,mpls_ip,load,frame',
  'mpls_eth':'ethii,vlan,mpls_eth,load,frame',
  'cfm':'ethii,vlan,cfm,load,frame',
  'ccm':'ethii,vlan,ccm,load,frame',
  'lbr':'ethii,vlan,lbr,load,frame',
  'lbm':'ethii,vlan,lbm,load,frame',
  'ltm':'ethii,vlan,ltm,load,frame',
  'dhcp':'ethii,vlan,ipv4,udp,dhcp,load,frame',
  'dhcpv6':'ethii,vlan,ipv6,udp,dhcpv6,load,frame',
  'ip':'ethii,vlan,ipv4,load,frame',
  'igmp':'ethii,vlan,ipv4,igmp,load,frame',
  'igmpv2':'ethii,vlan,ipv4,igmpv2,load,frame',
  'ipv6':'ethii,vlan,ipv6,tcp,load,frame',
  'eth':'ethii,vlan,load,frame',
  'tcp':'ethii,vlan,tcp,load,frame',
  'udp':'ethii,vlan,ipv4,udp,load,frame',
  'rip':'ethii,vlan,ipv4,udp,rip,load,frame'
}

# define supported protocol layer of sending traffic
lSUPPORTED_TRAFFIC_LAYER = ['ethii','vlan','arp','rarp','pppoe','mpls_eth','mpls_ip','cfm','ccm','lbr','lbm','ltm','ipv4','ipv6','udp','tcp','dhcp','dhcpv6','igmp','igmpv2','rip','load']

# define supported protocol type of sending traffic
lSUPPORTED_TRAFFIC_PROTOCOL = ['ip', 'arp', 'rarp','dhcp','dhcpv6','cfm','ccm','lbr','lbm','ltm','pppoe','mpls_eth','mpls_ip','igmp','igmpv2','rip']

def stc_set_mesure_result_type(sType):
    global sMEASURE_RESULT_TYPE
    sMEASURE_RESULT_TYPE = sType
    
def stc_get_measure_result_type():
    global sMEASURE_RESULT_TYPE
    return sMEASURE_RESULT_TYPE 

def stc_get_port_id(sPortType,session_name="first_stc_session") :
    global dCLIENT_PORT
    global dNETWORK_PORT

    sPortId = ''
    if sPortType.lower() == 'client' :        
        sPortId = dCLIENT_PORT[session_name]['PortId']
    elif sPortType.lower() == 'network' :
        sPortId = dNETWORK_PORT[session_name]['PortId'] 

    return sPortId 

def stc_send_cmd (command,session_name="first_stc_session") :
    oResult = STC_SESSION[session_name].oLlkStc.llk_stc_send_command(command)
    return oResult

def stc_disconnect(session_name="first_stc_session"):
    """
    Unattach STC port and disconnect from STC

    Args: 
        N/A

    Returns:
        N/A

    Usage Example: 
    | stc_disconnect | 
    """
    llk_log.llk_log_info("------------------------------------------------------------")
    llk_log.llk_log_info("UNATTACH STC PORTS")
    llk_log.llk_log_info("------------------------------------------------------------")
    STC_SESSION[session_name].hlk_traffic_delete_ports()

    llk_log.llk_log_info("Disconnect STC Successfully!")

def stc_connect(**stc_info):
    """
    Connect STC and attach ports
    - *IPAddress:*     STC IP address
    - *NetworkSlot:*   STC Slot Number of network port
    - *NetworkPort:*   STC Port Number of network port
    - *ClientSlot:*    STC Slot Number of Client port
    - *ClientPort:*    STC Port Number of Client port     
    - *ExePath:*       The path of STC Automation Libs
    - *Version:*       The version of STC automation API
    - *session_name:*  The name of stc session
    - *NetworkMediaType:* The media type of network port
    - *NetworkLineSpeed:* The line speed of network port
    - *ClientMediaType:* The media type of client port
    - *ClientLineSpeed:* The line speed of client port  

    Usage Example:
    | stc_connect | IPAddress=135.251.200.1 | NetworkSlot=2 | NetworkPort=1 | ClientSlot=2 | ClientPort=2 | 
    |   | Version=4.66 | ExePath=/repo/TEST_PACKAGES/STC/4.66/SpirentTestCenter.tcl | 
    | ${stc_info} | get table line | TrafficGenData | Type=STC |  
    | stc_connect | &{stc_info} | 
    """
    if stc_info:

        stc_info = dict(map(lambda (k,v): (str(k).lower().replace('_',''), str(v)),\
        stc_info.iteritems()))

        stc_info = str(stc_info).replace("execfile","exepath")
        stc_info = str(stc_info).replace("ipaddress","ip")
        stc_info = eval(stc_info)

        llk_log.llk_log_debug("stc_info:%s" % stc_info)

        session_name = "first_stc_session"
        if "sessionname" in stc_info:
            session_name = stc_info['sessionname']

        sStcVersion = "4.66"
        if "version" in stc_info:
            sStcVersion = stc_info['version']

        sStcPath = ""
        if "exepath" in stc_info:
            sStcPath = stc_info['exepath']
       
        llk_log.llk_log_info("ver:%s,path:%s" % (sStcVersion,sStcPath))

        STC_SESSION[session_name] = hlk_traffic.hlk_traffic()

        STC_SESSION[session_name].dPortIDObjects = {}
        dNETWORK_PORT[session_name] = {}
        dCLIENT_PORT[session_name] = {}

        dNETWORK_PORT[session_name]['PortId'] = '//' + str(stc_info['ip'])+'/'+str(stc_info['networkslot'])+\
        '/'+str(stc_info['networkport'])

        dCLIENT_PORT[session_name]['PortId'] = '//' + str(stc_info['ip'])+'/'+str(stc_info['clientslot'])+\
        '/' + str(stc_info['clientport'])

        dNETWORK_PORT[session_name]['MediaType'] = stc_info['networkmediatype']
        dCLIENT_PORT[session_name]['MediaType'] = stc_info['clientmediatype']

        dNETWORK_PORT[session_name]['LineSpeed'] = stc_info['networklinespeed']
        dCLIENT_PORT[session_name]['LineSpeed'] = stc_info['clientlinespeed']

        if dNETWORK_PORT[session_name]['PortId']:
            if dNETWORK_PORT[session_name]['PortId'] not in STC_SESSION[session_name].dPortIDObjects.keys():
                STC_SESSION[session_name].dPortIDObjects[dNETWORK_PORT[session_name]['PortId']] = ''
        else:
            raise Exception("Network Port ID not found in the platform.csv file")

        if dCLIENT_PORT[session_name]['PortId']:
            if dCLIENT_PORT[session_name]['PortId'] not in STC_SESSION[session_name].dPortIDObjects.keys():
                STC_SESSION[session_name].dPortIDObjects[dCLIENT_PORT[session_name]['PortId']] = ''
        else:
            raise Exception("Client Port ID not found in the platform.csv file")

        # dynamic load libgcc_s.so.1 and libstdc++.so.6
        sStdPath = '/home/repo/LIBS/STC/' + sStcVersion + "/libgcc_s.so.1"
        stc_lib = CDLL(sStdPath)
        sStdPath = '/home/repo/LIBS/STC/' + sStcVersion + "/libstdc++.so.6"
        stc_lib = CDLL(sStdPath)
        
        # source SpirentTestCenter.tcl
        STC_SESSION[session_name].oLlkStc.llk_stc_load(sStcVersion,sStcPath)
    else:
        raise Exception("There are no entries found in stc_info!")        
    
    outputDir = stc_info.pop('outputDir','')
    if outputDir :
        STC_SESSION[session_name].oLlkStc.TRAF_OUTPUT_DIR = outputDir
    logger.info("<span style=\"color: #00009C\">port:%s</span>"%STC_SESSION[session_name].dPortIDObjects,html=True)
    # configure stc port
    duration_mode = stc_info.setdefault('durationmode','CONTINUOUS')
    burst_size = stc_info.setdefault('burstsize',1)
    STC_SESSION[session_name].hlk_traffic_config_port(portID=dNETWORK_PORT[session_name]['PortId'], burstSize=burst_size, 
    durationMode=duration_mode,speed=dNETWORK_PORT[session_name]['LineSpeed'],mediatype=dNETWORK_PORT[session_name]['MediaType'])
    STC_SESSION[session_name].hlk_traffic_config_port(portID=dCLIENT_PORT[session_name]['PortId'], burstSize=burst_size, 
    durationMode=duration_mode,speed=dCLIENT_PORT[session_name]['LineSpeed'],mediatype=dCLIENT_PORT[session_name]['MediaType'])

    STC_SESSION[session_name].hlk_traffic_connect()

def _get_protocol_parameters(protocolType,**kwargs):
    dProtocolParam = kwargs
    for eachKey in kwargs.keys():
        if protocolType not in eachKey:
            del dProtocolParam[eachKey]
        else:
            value = dProtocolParam[eachKey]
            del dProtocolParam[eachKey]
            sNewKey = re.sub("%s\." % protocolType,'', eachKey)
            dProtocolParam[sNewKey] = value

    return dProtocolParam

def stc_upstream_protocol_traffic_measure(protocol_type,stream_list,**kwargs):
    """

    To send upstream protocol traffics and measure the traffics
    
    - *protocol_type:* The protocol type of sending traffics. 'ip', 'arp', 'dhcp','ccm','pppoe' and 'igmpv2' protocols are supported. 

    - *session_name:*  The name of stc session, default is "first_stc_session"

    - *stream_list:(required)*  Stream list,input params for each stream.
 
    - *measure_list:* Stream Measure list, measure params for each stream.   

    - *filter_list:* get the value of measurement output of upstream traffics 
    
    - *expect_result:* expect result for upstream measure, split by comma ','.

    - *duration:* traffic duration time. Default is 30 seconds.
    
    - *wait_time:* the wait time between send traffic and start measure. Default is 5 seconds.   

    - *retrieve_times:* the times of retrieving measure result. Default is 5 times.
    
    - *RETURN:* a list of the measure values of all streams. the returned value items are defined in lUpFilterItems and lDownFilterItems

    Usage Examples:
    | ${dStreamIp} | Create_Dictionary | vlan.vlan1=11 | vlan.pri1=1 | vlan.vlan2=12 | vlan.pri2=2 | ethii.srcMac=11:11:11:11:11:11 | ethii.dstMac=22:22:22:22:22:22 | ipv4.destAddr=135.251.1.1 | ipv4.sourceAddr=135.251.2.2 | load.load=1 | loadunit=FRAMES_PER_SECOND	| 
    | @{FilterResult} | stc upstream protocol traffic measure | ip | ${lStreamList} | measure_list=${lMeasureList} | retrieve_times=200 | wait_time=5 | duration=10 | 		

    | ${dStreamArp} | Create_Dictionary | vlan.vlan1=11 | vlan.pri1=1 | vlan.vlan2=12 | vlan.pri2=2 | ethii.srcMac=11:11:11:11:11:11 | ethii.dstMac=22:22:22:22:22:22 | load.load=1 | loadunit=FRAMES_PER_SECOND | arp.senderPAddr=135.251.200.1 | arp.targetPAddr=135.251.200.100 | arp.senderHwAddr=00:00:02:00:00:01 | arp.targetHwAddr=00:00:00:00:00:00 | arp.hardware=0001 | arp.protocol=0800 | arp.ihAddr=6 | arp.ipAddr=4 | arp.operation=1 |              
    | @{FilterResult} | stc upstream protocol traffic measure | arp | ${lStreamList} | measure_list=${lMeasureList} | retrieve_times=200 | wait_time=5 | duration=10 | 	

    | ${dStreamPppoe} | Create_Dictionary | vlan.vlan1=11 | vlan.pri1=1 | vlan.vlan2=12 | vlan.pri2=2 | ethii.srcMac=11:11:11:11:11:11 | ethii.dstMac=22:22:22:22:22:22 | load.load=1 | loadunit=FRAMES_PER_SECOND | pppoe.code=9 | pppoe.length=10 | 
    | @{FilterResult} | stc upstream protocol traffic measure | pppoe | ${lStreamList} | measure_list=${lMeasureList} | retrieve_times=200 | wait_time=5 | duration=10	| 		

    | ${dStreamDhcp} | Create_Dictionary | vlan.vlan1=11 | vlan.pri1=1 | vlan.vlan2=12 | vlan.pri2=2 | ethii.srcMac=11:11:11:11:11:11 | ethii.dstMac=22:22:22:22:22:22 | ipv4.destAddr=135.251.1.1 | ipv4.sourceAddr=135.251.2.2 | load.load=1 | loadunit=FRAMES_PER_SECOND | udp.destPort=68 | udp.sourcePort=67 | dhcp.messageType=1 | dhcp.clientMac=00:00:02:00:00:01 | dhcp.clientAddr=135.251.200.171 | 
    | @{FilterResult} | stc upstream protocol traffic measure | dhcp | ${lStreamList} | measure_list=${lMeasureList} | retrieve_times=200 | wait_time=5 | duration=10 | 	

    | ${dStreamCcm} | Create_Dictionary | vlan.vlan1=11 | vlan.pri1=1 | vlan.vlan2=12 | vlan.pri2=2 | ethii.srcMac=11:11:11:11:11:11 | ethii.dstMac=22:22:22:22:22:22 | load.load=1 | load.loadunit=FRAMES_PER_SECOND | ccm.maepi=1 | ccm.cfmHeader.mdLevel=5 | ccm.maid.mdnf=04 | ccm.maid.smaf=02 | ccm.maid.sman=6d61323030 | ccm.maid.mdn=6d6435 | ccm.maid.padding=000000000000000000000000000000000000000000000000000000000000000000000000 | ccm.sequenceNumber=31900 | 	
    | @{FilterResult} | stc upstream protocol traffic measure | ccm | ${lStreamList} | measure_list=${lMeasureList} | retrieve_times=200 | wait_time=5 | duration=10 | 	

    | ${dStreamIgmpv2} | Create_Dictionary | vlan.vlan1=11 | vlan.pri1=1 | vlan.vlan2=12 | vlan.pri2=2 | ethii.srcMac=11:11:11:11:11:11 | ethii.dstMac=22:22:22:22:22:22 | load.load=1 | load.loadunit=FRAMES_PER_SECOND | igmpv2.groupAddress=225.0.0.2 | igmpv2.type=17	| 
    | @{FilterResult} | stc upstream protocol traffic measure | igmpv2 | ${lStreamList} | measure_list=${lMeasureList} | retrieve_times=200 | wait_time=5 | duration=10 | 	

    """
    lFilterResult = []
    lExpectResult = []
    #dKwargs = dict(map(lambda (k,v): (str(k).lower(), v), kwargs.iteritems()))

    session_name = "first_stc_session"
    if "session_name" in kwargs:
        session_name = kwargs['session_name'].lower()

    sPortId = dCLIENT_PORT[session_name]['PortId']
    lFilterResult = _stc_protocol_traffic_measure(sPortId,protocol_type,stream_list,**kwargs)
    return lFilterResult

def stc_downstream_protocol_traffic_measure(protocol_type,stream_list,**kwargs):
    """

    To send downstream protocol traffics and measure the traffics
    
    - *protocol_type:* The protocol type of sending traffics. 'ip', 'arp', 'dhcp','ccm','pppoe' and 'igmpv2' protocols are supported. 

    - *session_name:*  The name of stc session, default is "first_stc_session"

    - *stream_list:(required)*  Stream list,input params for each stream.
 
    - *measure_list:* Stream Measure list, measure params for each stream.   

    - *filter_list:* get the value of measurement output of downstream traffics 
    
    - *expect_result:* expect result for downstream measure, split by comma ','.

    - *duration:* traffic duration time. Default is 30 seconds.
    
    - *wait_time:* the wait time between send traffic and start measure. Default is 5 seconds.   

    - *retrieve_times:* the times of retrieving measure result. Default is 5 times.
    
    - *RETURN:* a list of the measure values of all streams. the returned value items are defined in lUpFilterItems and lDownFilterItems

    Usage Examples:
    | ${dStreamIp} | Create_Dictionary | vlan.vlan1=11 | vlan.pri1=1 | vlan.vlan2=12 | vlan.pri2=2 | ethii.srcMac=11:11:11:11:11:11 | ethii.dstMac=22:22:22:22:22:22 | ipv4.destAddr=135.251.1.1 | ipv4.sourceAddr=135.251.2.2 | load.load=1 | loadunit=FRAMES_PER_SECOND	| 
    | @{FilterResult} | stc downstream protocol traffic measure | ip | ${lStreamList} | measure_list=${lMeasureList} | retrieve_times=200 | wait_time=5 | duration=10 | 		

    | ${dStreamArp} | Create_Dictionary | vlan.vlan1=11 | vlan.pri1=1 | vlan.vlan2=12 | vlan.pri2=2 | ethii.srcMac=11:11:11:11:11:11 | ethii.dstMac=22:22:22:22:22:22 | load.load=1 | loadunit=FRAMES_PER_SECOND | arp.senderPAddr=135.251.200.1 | arp.targetPAddr=135.251.200.100 | arp.senderHwAddr=00:00:02:00:00:01 | arp.targetHwAddr=00:00:00:00:00:00 | arp.hardware=0001 | arp.protocol=0800 | arp.ihAddr=6 | arp.ipAddr=4 | arp.operation=1 |              
    | @{FilterResult} | stc downstream protocol traffic measure | arp | ${lStreamList} | measure_list=${lMeasureList} | retrieve_times=200 | wait_time=5 | duration=10 | 	

    | ${dStreamPppoe} | Create_Dictionary | vlan.vlan1=11 | vlan.pri1=1 | vlan.vlan2=12 | vlan.pri2=2 | ethii.srcMac=11:11:11:11:11:11 | ethii.dstMac=22:22:22:22:22:22 | load.load=1 | loadunit=FRAMES_PER_SECOND | pppoe.code=9 | pppoe.length=10 | 
    | @{FilterResult} | stc downstream protocol traffic measure | pppoe | ${lStreamList} | measure_list=${lMeasureList} | retrieve_times=200 | wait_time=5 | duration=10	| 		

    | ${dStreamDhcp} | Create_Dictionary | vlan.vlan1=11 | vlan.pri1=1 | vlan.vlan2=12 | vlan.pri2=2 | ethii.srcMac=11:11:11:11:11:11 | ethii.dstMac=22:22:22:22:22:22 | ipv4.destAddr=135.251.1.1 | ipv4.sourceAddr=135.251.2.2 | load.load=1 | loadunit=FRAMES_PER_SECOND | udp.destPort=68 | udp.sourcePort=67 | dhcp.messageType=1 | dhcp.clientMac=00:00:02:00:00:01 | dhcp.clientAddr=135.251.200.171 | 
    | @{FilterResult} | stc downstream protocol traffic measure | dhcp | ${lStreamList} | measure_list=${lMeasureList} | retrieve_times=200 | wait_time=5 | duration=10 | 	

    | ${dStreamCcm} | Create_Dictionary | vlan.vlan1=11 | vlan.pri1=1 | vlan.vlan2=12 | vlan.pri2=2 | ethii.srcMac=11:11:11:11:11:11 | ethii.dstMac=22:22:22:22:22:22 | load.load=1 | load.loadunit=FRAMES_PER_SECOND | ccm.maepi=1 | ccm.cfmHeader.mdLevel=5 | ccm.maid.mdnf=04 | ccm.maid.smaf=02 | ccm.maid.sman=6d61323030 | ccm.maid.mdn=6d6435 | ccm.maid.padding=000000000000000000000000000000000000000000000000000000000000000000000000 | ccm.sequenceNumber=31900 | 	
    | @{FilterResult} | stc downstream protocol traffic measure | ccm | ${lStreamList} | measure_list=${lMeasureList} | retrieve_times=200 | wait_time=5 | duration=10 | 	

    | ${dStreamIgmpv2} | Create_Dictionary | vlan.vlan1=11 | vlan.pri1=1 | vlan.vlan2=12 | vlan.pri2=2 | ethii.srcMac=11:11:11:11:11:11 | ethii.dstMac=22:22:22:22:22:22 | load.load=1 | load.loadunit=FRAMES_PER_SECOND | igmpv2.groupAddress=225.0.0.2 | igmpv2.type=17	| 
    | @{FilterResult} | stc downstream protocol traffic measure | igmpv2 | ${lStreamList} | measure_list=${lMeasureList} | retrieve_times=200 | wait_time=5 | duration=10 | 	

    """
    lFilterResult = []
    lExpectResult = []
    #dKwargs = dict(map(lambda (k,v): (str(k).lower(), v), kwargs.iteritems()))

    session_name = "first_stc_session"
    if "session_name" in kwargs:
        session_name = kwargs['session_name'].lower()

    sPortId = dNETWORK_PORT[session_name]['PortId']
    lFilterResult = _stc_protocol_traffic_measure(sPortId,protocol_type,stream_list,**kwargs)
    return lFilterResult

def stc_upstream_ip_traffic_measure(stream_list,**kwargs):
    """
    To send upstream ip traffics and measure the traffics

    - *session_name:*  The name of stc session, default is "first_stc_session"

    - *stream_list:(required)*  Stream list,input params for each stream.

         | Default: | None |
         | Type: | List |
         | Example: | 
         |   | ${dStream1} | create dictionary | vlan.vlan1=11 | ethii.srcMac=11:11:11:11:11:11 | load.load=5000 |
         |   | ${dStream2} | create dictionary | vlan.vlan1=12 | ethii.srcMac=11:11:11:11:11:11 | load.load=4000 | 
         |   | @{stream_list} | create list | ${dStream1} | ${dStream2} |

         | Layer Type Name | Reference Link | Comments |  
         | vlan.           | http://kms.spirentcom.com/CSC/pabtech/stc-automation-html/ethernet-Vlan.htm | configure vlan layer |
         | ethii.          | http://kms.spirentcom.com/CSC/pabtech/stc-automation-html/ethernet-EthernetII.htm | configure ethii layer | 
         | ipv4.           | http://kms.spirentcom.com/CSC/pabtech/stc-automation-html/ipv4-IPv4.htm | configure ipv4 layer | 
         | frame.          | http://kms.spirentcom.com/CSC/pabtech/stc-automation-html/StreamBlock.htm | configure frame layer | 
         | load.           | http://kms.spirentcom.com/CSC/pabtech/stc-automation-html/StreamBlockLoadProfile.htm | configure load layer | 

         Usage Examples: 

         | LOG | CONFIG TWO VLANS |
         | ${dStream1} | create dictionary | vlan.vlan1=11 | vlan.pri1=1 | vlan.vlan2=12 | vlan.pri2=2 | ethii.srcMac=11:11:11:11:11:11 | load.load=5000 |
         | @{stream_list} | create list | ${dStream1} |
        
         | LOG | CONFIG ETHII LAYER |
         | ${dStream1} | create dictionary | ethii.srcMac=11:11:11:11:11:11 | ethii.srcMac=22:22:22:11:11:11 | load.load=5000 | 
         | @{stream_list} | create list | ${dStream1} | 

         | LOG | CONFIG IPV4 LAYER |
         | ${dStream1} | create dictionary | ipv4.destAddr=192.168.1.1 | ipv4.sourceAddr=135.251.1.1 | load.load=5000 |   
         | @{stream_list} | create list | ${dStream1} | 

         | LOG | CONFIG FRAME LAYER |
         | ${dStream1} | create dictionary | frame.InsertSig=TRUE |            
         | @{stream_list} | create list | ${dStream1} |     

         | LOG | TRAFFIC WITH FIXED LENGTH frame.fixedframelength=1024, unit is byte |
         | ${dStream1} | create dictionary | frame.fixedframelength=1024 |            
         | @{stream_list} | create list | ${dStream1} |    

         | LOG | TRAFFIC WITH AUTO LENGTH.frame.framelengthmode=auto |
         | ${dStream1} | create dictionary | frame.framelengthmode=auto |            
         | @{stream_list} | create list | ${dStream1} |  

         | LOG | CONTROL TRAFFIC BITRATE |
         | ${dStream1} | create dictionary | load.load=500 | load.loadunit=BITS_PER_SECOND |         
         | @{stream_list} | create list | ${dStream1} |
        
         LoadUnit Possible Values:
         | PERCENT_LINE_RATE |
         | FRAMES_PER_SECOND | 
         | INTER_BURST_GAP	 |
         | BITS_PER_SECOND	 |
         | KILOBITS_PER_SECOND |	
         | MEGABITS_PER_SECOND |	
         | INTER_BURST_GAP_IN_MILLISECONDS |	
         | INTER_BURST_GAP_IN_NANOSECONDS  |

    - *measure_list:* Stream Measure list, measure params for each stream.
         
         | Default: | None |
         | Type: | List |
         | Reference: | http://kms.spirentcom.com/CSC/pabtech/stc-automation-html/RxStreamSummaryResults.htm | 
         | Example: |  
         |   | ${dMeasure1} | create dictionary | l1bitrate=5000 | l1bitrate_tol=5% | 
         |   | ${dMeasure2} | create dictionary | l1bitrate=4000 | l1bitrate_tol=5% |  
         |   | @{lMeasureList} | create list | ${dMeasure1} | ${dMeasure2} |
                  
         "xxx_tol" is Error tolerance rate. It means, If l1bitrate_tol=5%, the value of l1bitrate should be: 5000*(100-5)%<=l1bitrate<=5000*(100+5)%

         | LOG | CHECK IF TRAFFIC BITRATE is 5000 bits per second,tolerance is 5 percent |  
         | ${dMeasure1} | create dictionary | l1bitrate=5000 | l1bitrate_tol=5% | 
         | @{lMeasureList} | create list | ${dMeasure1} | ${dMeasure2} |

         Note: The value of "bitrate" will skip the rate of frame layer.

    - *filter_list:* get the value of measurement output of upstream traffics

         | Default: | None |
         | Type: | List |
         | Example: | 
         |   | @{filter_list} | create list | bitrate | l1bitrate |   

    - *expect_result:* expect result for upstream measure, split by comma ','. 

         | Default: | all PASS |
         | Type: | String |
         | Format: | PASS,PASS |
         | Value: | PASS,FAIL or NA. NA is to ignore result | 
    
    - *duration:* traffic duration time. Default is 30 seconds.
    - *wait_time:* the wait time between send traffic and start measure. Default is 5 seconds.

         | Unit: | second |
         | Type: | Integer |

    - *retrieve_times:* the times of retrieving measure result. Default is 5 times.

         | Default: | 5 |
         | Unit: | times |
         | Type: | Integer |

    - *RETURN:* a list of the measure values of all streams. the returned value items are defined in lUpFilterItems and lDownFilterItems

    Usage Examples:
    | LOG | SAMPLE1 |
    | stc_upstream_ip_traffic_measure | ${stream_list} | measure_list${lMeasureList} | filter_list=${lFilterItems} | duration=10 | expect_result="PASS,PASS" |  
    | LOG | GET VALUES FROM MEASURE OUTPUT | 
    | ${return_filter_item} | stc_upstream_ip_traffic_measure | ${stream_list} | measure_list=${lMeasureList} | filter_list=${lFilterItems} | duration=10 | expect_result="PASS,PASS" |  
    """ 
    lFilterResult = []
    lExpectResult = []
    #dKwargs = dict(map(lambda (k,v): (str(k).lower(), v), kwargs.iteritems()))

    session_name = "first_stc_session"
    if "session_name" in kwargs:
        session_name = kwargs['session_name'].lower()
 
    sPortId = dCLIENT_PORT[session_name]['PortId'] 
    lFilterResult = _stc_ip_traffic_measure(sPortId,stream_list,**kwargs)
    return lFilterResult

def stc_downstream_ip_traffic_measure(stream_list,**kwargs):
    """
    To send downstream ip traffics and measure the traffics

         | Default: | None |
         | Type: | List |
         | Example: | 
         |   | ${dStream1} | create dictionary | vlan.vlan1=11 | ethii.srcMac=11:11:11:11:11:11 | load.load=5000 |
         |   | ${dStream2} | create dictionary | vlan.vlan1=12 | ethii.srcMac=11:11:11:11:11:11 | load.load=4000 | 
         |   | @{stream_list} | create list | ${dStream1} | ${dStream2} |

         | Layer Type Name | Reference Link | Comments |  
         | vlan.           | http://kms.spirentcom.com/CSC/pabtech/stc-automation-html/ethernet-Vlan.htm | configure vlan layer |
         | ethii.          | http://kms.spirentcom.com/CSC/pabtech/stc-automation-html/ethernet-EthernetII.htm | configure ethii layer | 
         | ipv4.           | http://kms.spirentcom.com/CSC/pabtech/stc-automation-html/ipv4-IPv4.htm | configure ipv4 layer | 
         | frame.          | http://kms.spirentcom.com/CSC/pabtech/stc-automation-html/StreamBlock.htm | configure frame layer | 
         | load.           | http://kms.spirentcom.com/CSC/pabtech/stc-automation-html/StreamBlockLoadProfile.htm | configure load layer | 

         Usage Examples: 

         | LOG | CONFIG TWO VLANS |
         | ${dStream1} | create dictionary | vlan.vlan1=11 | vlan.pri1=1 | vlan.vlan2=12 | vlan.pri2=2 | ethii.srcMac=11:11:11:11:11:11 | load.load=5000 |
         | @{stream_list} | create list | ${dStream1} |
        
         | LOG | CONFIG ETHII LAYER |
         | ${dStream1} | create dictionary | ethii.srcMac=11:11:11:11:11:11 | ethii.srcMac=22:22:22:11:11:11 | load.load=5000 | 
         | @{stream_list} | create list | ${dStream1} | 

         | LOG | CONFIG IPV4 LAYER |
         | ${dStream1} | create dictionary | ipv4.destAddr=192.168.1.1 | ipv4.sourceAddr=135.251.1.1 | load.load=5000 |   
         | @{stream_list} | create list | ${dStream1} | 

         | LOG | CONFIG FRAME LAYER |
         | ${dStream1} | create dictionary | frame.InsertSig=TRUE |            
         | @{stream_list} | create list | ${dStream1} |     

         | LOG | TRAFFIC WITH FIXED LENGTH frame.fixedframelength=1024, unit is byte |
         | ${dStream1} | create dictionary | frame.fixedframelength=1024 |            
         | @{stream_list} | create list | ${dStream1} |    

         | LOG | TRAFFIC WITH AUTO LENGTH.frame.framelengthmode=auto |
         | ${dStream1} | create dictionary | frame.framelengthmode=auto |            
         | @{stream_list} | create list | ${dStream1} |  

         | LOG | CONTROL TRAFFIC BITRATE |
         | ${dStream1} | create dictionary | load.load=500 | load.loadunit=BITS_PER_SECOND |         
         | @{stream_list} | create list | ${dStream1} |
        
         LoadUnit Possible Values:
         | PERCENT_LINE_RATE |
         | FRAMES_PER_SECOND | 
         | INTER_BURST_GAP	 |
         | BITS_PER_SECOND	 |
         | KILOBITS_PER_SECOND |	
         | MEGABITS_PER_SECOND |	
         | INTER_BURST_GAP_IN_MILLISECONDS |	
         | INTER_BURST_GAP_IN_NANOSECONDS  |

    - *session_name:*  The name of stc session, default is "first_stc_session"

    - *measure_list:* Stream Measure list, measure params for each stream.
         
         | Default: | None |
         | Type: | List |
         | Reference: | http://kms.spirentcom.com/CSC/pabtech/stc-automation-html/RxStreamSummaryResults.htm | 
         | Example: |  
         |   | ${dMeasure1} | create dictionary | l1bitrate=5000 | l1bitrate_tol=5% | 
         |   | ${dMeasure2} | create dictionary | l1bitrate=4000 | l1bitrate_tol=5% |  
         |   | @{lMeasureList} | create list | ${dMeasure1} | ${dMeasure2} |
                  
         "xxx_tol" is Error tolerance rate. It means, If l1bitrate_tol=5%, the value of l1bitrate should be: 5000*(100-5)%<=l1bitrate<=5000*(100+5)%

         | LOG | CHECK IF TRAFFIC BITRATE is 5000 bits per second,tolerance is 5 percent |  
         | ${dMeasure1} | create dictionary | l1bitrate=5000 | l1bitrate_tol=5% | 
         | @{lMeasureList} | create list | ${dMeasure1} | ${dMeasure2} |

         Note: The value of "bitrate" will skip the rate of frame layer.

    - *filter_list:* get the value of measurement output of upstream traffics

         | Default: | None |
         | Type: | List |
         | Example: | 
         |   | @{filter_list} | create list | bitrate | l1bitrate |   

    - *expect_result:* expect result for upstream measure, split by comma ','. 

         | Default: | all PASS |
         | Type: | String |
         | Format: | PASS,PASS |
         | Value: | PASS,FAIL or NA. NA is to ignore result | 
    
    - *duration:* traffic duration time. Default is 30 seconds.
    - *wait_time:* the wait time between send traffic and start measure. Default is 5 seconds.

         | Unit: | second |
         | Type: | Integer |

    - *retrieve_times:* the times of retrieving measure result. Default is 5 times.

         | Default: | 5 |
         | Unit: | times |
         | Type: | Integer |

    - *RETURN:* a list of the measure values of all streams. the returned value items are defined in lUpFilterItems and lDownFilterItems

    Usage Examples:
    | LOG | SAMPLE1 |
    | stc_downstream_ip_traffic_measure | ${stream_list} | measure_list=${lMeasureList} | filter_list=${lFilterItems} | duration=10 | expect_result="PASS,PASS" |  
    | LOG | GET VALUES FROM MEASURE OUTPUT | 
    | ${return_filter_item} | stc_downstream_ip_traffic_measure | ${stream_list} | measure_list=${lMeasureList} | filter_list=${lFilterItems} | duration=10 | expect_result="PASS,PASS" |  
    """ 

    lFilterResult = []
    lExpectResult = []

    session_name = "first_stc_session"
    if "session_name" in kwargs:
        session_name = kwargs['session_name'].lower()

    sPortId = dNETWORK_PORT[session_name]['PortId'] 
    #dKwargs = dict(map(lambda (k,v): (str(k).lower(), v), kwargs.iteritems()))

    lFilterResult = _stc_ip_traffic_measure(sPortId,stream_list,**kwargs)
    return lFilterResult

def stc_bidirect_ip_traffic_measure(up_stream_list,down_stream_list,**kwargs):
    """
    To send upstream and downstream ip traffics and measure the traffics

    - *session_name:*  The name of stc session, default is "first_stc_session"

    - *up_stream_list:(required)*  Upstream list,input params for each stream.
    - *down_stream_list:(required)*  Downstream list,input params for each stream.

         | Default: | None |
         | Type: | List |
         | Example: | 
         |   | ${dStream1} | create dictionary | vlan.vlan1=11 | ethii.srcMac=11:11:11:11:11:11 | load.load=5000 |
         |   | ${dStream2} | create dictionary | vlan.vlan1=12 | ethii.srcMac=11:11:11:11:11:11 | load.load=4000 | 
         |   | @{up_stream_list} | create list | ${dStream1} | ${dStream2} |

    - *up_measure_list:* Upstream Measure list, measure params for each stream.
    - *down_measure_list:* Downstream Measure list, measure params for each stream.
         
         | Default: | None |
         | Type: | List |
         | Example: |  
         |   | ${dMeasure1} | create dictionary | l1bitrate=5000 | l1bitrate_tol=5% | 
         |   | ${dMeasure2} | create dictionary | l1bitrate=4000 | l1bitrate_tol=5% |  
         |   | @{up_measure_list} | create list | ${dMeasure1} | ${dMeasure2} |

    - *up_filter_list:* get the value of measurement output of upstream traffics
    - *down_filter_list:* get the value of measurement output of downstream traffics

         | Default: | None |
         | Type: | List |
         | Example: | 
         |   | @{up_filter_list} | create list | bitrate | l1bitrate |

    - *up_expect_result:* expect result for all upstream measure, split by comma ','. 
    - *down_expect_result:* expect result for all downstream measure, split by comma ','.

         | Default: | all PASS |
         | Type: | String |
         | Format: | PASS,PASS |
         | Value: | PASS,FAIL or NA. NA is to ignore result |

    - *duration:* traffic duration time. Default is 30 seconds. 

         | Default: | 30 |
         | Unit: | second |
         | Type: | Integer |

    - *wait_time:* the wait time between send traffic and start measure. Default is 5 seconds.

         | Default: | 5 |
         | Unit: | second |
         | Type: | Integer |

    - *retrieve_times:* the times of retrieving measure result. Default is 5 times.

         | Default: | 5 |
         | Unit: | times |
         | Type: | Integer |

    - *RETURN:* a list of the measure values of all streams. the returned value items are defined in lUpFilterItems and lDownFilterItems

    Usage Examples:
    | LOG | SAMPLE1 |
    | stc_bidirect_ip_traffic_measure | ${up_stream_list} | ${down_stream_list} | up_measure_list=${lUpMeasureList} | up_filter_list=${lUpFilterItems} | down_measuer_list=${lDownMeasureList} | down_filter_list=${lDownFilterItems} | duration=10 | up_expect_result="PASS,PASS" | down_expect_result="FAIL,PASS" |   
    | LOG | GET VALUES FROM MEASURE OUTPUT | 
    | ${return_filter_item} | stc_bidirect_ip_traffic_measure | ${up_stream_list} | ${down_stream_list} | up_measure_list=${lUpMeasureList} | duration=10 | up_expect_result="PASS,PASS" | down_expect_result="FAIL,PASS" |
    """

    session_name = "first_stc_session"
    if "session_name" in kwargs:
        session_name = dKwargs['session_name'].lower()

    sNetworkPortId = dNETWORK_PORT[session_name]['PortId'] 
    sClientPortId = dCLIENT_PORT[session_name]['PortId'] 
    lFilterResult = []
    dKwargs = dict(map(lambda (k,v): (str(k).lower(), v), kwargs.iteritems()))

    lUpExpectResult = []
    if "up_expect_result" in dKwargs.keys():
        lUpExpectResult = dKwargs['up_expect_result'].split(",")

    lDownExpectResult = []
    if "down_expect_result" in dKwargs.keys():
        lDownExpectResult = dKwargs['down_expect_result'].split(",")

    lUpMeasureList = []
    if "up_measure_list" in dKwargs.keys():
        lUpMeasureList = dKwargs['up_measure_list']

    lDownMeasureList = []
    if "down_measure_list" in dKwargs.keys():
        lDownMeasureList = dKwargs['down_measure_list']

    lUpFilterItems = []
    if "up_filter_list" in dKwargs.keys():
        lUpFilterItems = dKwargs['up_filter_list']

    lDownFilterItems = []
    if "down_filter_list" in dKwargs.keys():
        lDownFilterItems = dKwargs['down_filter_list']

    iLenExpect  = len(lUpExpectResult)+len(lDownExpectResult)
    iLenStream  = len(up_stream_list)+len(down_stream_list)
    iLenMeasure = len(lUpMeasureList)+len(lDownMeasureList)

    bResult = "PASS"

    if kwargs:
        dKwargs = dict(map(lambda (k,v): (str(k).lower(), v), kwargs.iteritems()))
    else:
        dKwargs = {}

    iWaitTime = 5
    if "wait_time" in dKwargs.keys():
        iWaitTime = int(dKwargs['wait_time'])

    iDuration = 30          
    if "duration" in dKwargs.keys():
        iDuration = int(dKwargs['duration'])

    iRetrieveTimes = 5
    if "retrieve_times" in dKwargs.keys():
        iRetrieveTimes = int(dKwargs['retrieve_times'])

    if int(iDuration) <= int(iWaitTime):
        raise AssertionError("duration %s must large than wait_time %s!!" % (iDuration,iWaitTime))

    fInterval = 1.0*(iDuration-iWaitTime)/iRetrieveTimes
     
    lUpExpectResult = []
    if "up_expect_result" in dKwargs.keys():
        lUpExpectResult = dKwargs['up_expect_result'].split(",")

    lDownExpectResult = []
    if "down_expect_result" in dKwargs.keys():
        lDownExpectResult = dKwargs['down_expect_result'].split(",")

    # config stream blocks
    # create a stream name with time stamp
    llk_log.llk_log_info("------------------------------------------------------------")
    llk_log.llk_log_info("CREATE UP STREAM BLOCKS")
    llk_log.llk_log_info("------------------------------------------------------------")
    lUpSBList = []
    i = 1
    for dStream in up_stream_list:
        timeStamp = time.time()
        sSBName = "UP_STREAM_%s_%s" % (i,timeStamp)
        lUpSBList.append(sSBName)
        i = i +1

        # create a streamblock 
        STC_SESSION[session_name].hlk_traffic_config_streamblock(name=sSBName,portid=sClientPortId)
        
        # attach protocol ethii
        dProtocolParam = _get_protocol_parameters(protocolType='ethii',**dStream)
        STC_SESSION[session_name].hlk_traffic_config_streamblock(name=sSBName,type="ethii",**dProtocolParam)
        
        # attach protocol ipv4
        dProtocolParam = _get_protocol_parameters(protocolType='ipv4',**dStream)
        STC_SESSION[session_name].hlk_traffic_config_streamblock(name=sSBName,type="ipv4",**dProtocolParam)
        
        # attach vlan
        dProtocolParam = _get_protocol_parameters(protocolType='vlan',**dStream)
        STC_SESSION[session_name].hlk_traffic_config_streamblock(name=sSBName,type="vlan",**dProtocolParam)

        # attach load 
        dProtocolParam = _get_protocol_parameters(protocolType='load',**dStream)
        STC_SESSION[session_name].hlk_traffic_config_streamblock(name=sSBName,type="load",**dProtocolParam)

        # config frame
        dProtocolParam = _get_protocol_parameters(protocolType='frame',**dStream)
        STC_SESSION[session_name].hlk_traffic_config_streamblock(name=sSBName,type="frame",**dProtocolParam)

    # config stream blocks
    # create a stream name with time stamp
    llk_log.llk_log_info("------------------------------------------------------------")
    llk_log.llk_log_info("CREATE DOWN STREAM BLOCKS")
    llk_log.llk_log_info("------------------------------------------------------------")
    lDownSBList = []
    i = 1
    for dStream in down_stream_list:
        timeStamp = time.time()
        sSBName = "DOWN_STREAM_%s_%s" % (i,timeStamp)
        lDownSBList.append(sSBName)
        i = i +1

        # create a streamblock 
        STC_SESSION[session_name].hlk_traffic_config_streamblock(name=sSBName,portid=sNetworkPortId)
        
        # attach protocol ethii
        dProtocolParam = _get_protocol_parameters(protocolType='ethii',**dStream)
        STC_SESSION[session_name].hlk_traffic_config_streamblock(name=sSBName,type="ethii",**dProtocolParam)
        
        # attach protocol ipv4
        dProtocolParam = _get_protocol_parameters(protocolType='ipv4',**dStream)
        STC_SESSION[session_name].hlk_traffic_config_streamblock(name=sSBName,type="ipv4",**dProtocolParam)
        
        # attach vlan
        dProtocolParam = _get_protocol_parameters(protocolType='vlan',**dStream)
        STC_SESSION[session_name].hlk_traffic_config_streamblock(name=sSBName,type="vlan",**dProtocolParam)

        # attach load 
        dProtocolParam = _get_protocol_parameters(protocolType='load',**dStream)
        STC_SESSION[session_name].hlk_traffic_config_streamblock(name=sSBName,type="load",**dProtocolParam)

        # config frame
        dProtocolParam = _get_protocol_parameters(protocolType='frame',**dStream)
        STC_SESSION[session_name].hlk_traffic_config_streamblock(name=sSBName,type="frame",**dProtocolParam)    


    llk_log.llk_log_info("------------------------------------------------------------")
    llk_log.llk_log_info("START UP AND DOWN STREAM TRAFFIC")
    llk_log.llk_log_info("------------------------------------------------------------")
    lAllSBList = lUpSBList + lDownSBList
    STC_SESSION[session_name].hlk_traffic_start_traffic(streamblock=lAllSBList)

    llk_log.llk_log_info("------------------------------------------------------------")
    llk_log.llk_log_info("START MEASURE UP AND DOWN STREAMS")
    llk_log.llk_log_info("------------------------------------------------------------")
    lMULTI_RETRIEVE_RESULTS[session_name] = []
    lLATEST_RESULT[session_name] = []
    STC_SESSION[session_name].hlk_traffic_start_measure(clearResult='FALSE',waitTime=iWaitTime,resulttype="RxStreamSummaryResults",
    filenamePrefix="RxStreamSummaryResults_file",interval=1,configtype="StreamBlock") 

    llk_log.llk_log_info("------------------------------------------------------------")
    llk_log.llk_log_info("RETRIEVE MEASURE OF UP AND DOWN STREAMS")
    llk_log.llk_log_info("------------------------------------------------------------")
    lMULTI_RETRIEVE_RESULTS[session_name] = []
    startMeasureStamp = time.time()
    stopMeasureStamp = startMeasureStamp + int(iDuration-iWaitTime)
    nowTimeStamp = time.time()
    j = 0
    while ( nowTimeStamp < stopMeasureStamp ):
        roundEndTimeStamp = nowTimeStamp + fInterval
        j = j + 1
        llk_log.llk_log_debug("**************Retrieve Times %s****************" % j )

        # create measure list 
        i = 1
        ddRetrieveMeasure = {}
        lMeasureArgs = [] 
        for eachSB in lUpSBList:
            # check if lUpMeasureList[i-1] exist
            try:
                measure = lUpMeasureList[i-1]
            except Exception as inst:
                i = i + 1
                continue
           
            # skip the emtpy measure
            if not lUpMeasureList[i-1]:
                i = i + 1 
                continue

            for eachMeasureKey in lUpMeasureList[i-1].keys():
                # skip two parameters "l1bitrate_tol" and "bitrate_tol"
                # skip the parameters which include "_tol"
                if "_tol" not in str(eachMeasureKey).lower() and eachMeasureKey not in lMeasureArgs:
                    lMeasureArgs.append(eachMeasureKey.lower())

            for eachMeasureKey in lUpFilterItems:
                if eachMeasureKey not in lMeasureArgs:
                    lMeasureArgs.append(eachMeasureKey.lower())

            i = i + 1
        
        i = 1
        for eachSB in lDownSBList:
            # check if lDownMeasureList[i-1] exist
            try:
                measure = lDownMeasureList[i-1]
            except Exception as inst:
                i = i + 1
                continue

            # skip the emtpy measure
            if not lDownMeasureList[i-1]:
                i = i + 1
                continue

            for eachMeasureKey in lDownMeasureList[i-1].keys():
                # skip two parameters "l1bitrate_tol" and "bitrate_tol"
                # skip the parameters which include "_tol"
                if "_tol" not in str(eachMeasureKey).lower() and eachMeasureKey not in lMeasureArgs: 
                    lMeasureArgs.append(eachMeasureKey.lower())

            for eachMeasureKey in lDownFilterItems:
                if eachMeasureKey not in lMeasureArgs:
                    lMeasureArgs.append(eachMeasureKey.lower())

            i = i + 1

        # retrieve measure result of all streams
        ddRetrieveMeasure = STC_SESSION[session_name].oLlkStc.llk_stc_measure_subflow(STC_SESSION[session_name].dMeasureObjects["RxStreamSummaryResults"], *lMeasureArgs)

        lAllSBlist = lUpSBList + lDownSBList

        for eachSB in lAllSBList:
            dRetrieveResult = {}
            if str(eachSB)+":0" in ddRetrieveMeasure.keys():
                sTotalStreamCnt = int(ddRetrieveMeasure[str(eachSB)+":0"]['flowcount'])
                for i in range(0,sTotalStreamCnt+1):
                    sMeasureKey = str(eachSB)+":"+str(i)
                    if sMeasureKey in ddRetrieveMeasure.keys():
                        dRetrieveResult[sMeasureKey] = ddRetrieveMeasure[sMeasureKey]
                    else:
                        llk_log_warn("Measure does not exist for the given Streamblock %s, stream count %s" % (str(sSBName),str(i)))
            else:
                llk_log_end_proc()
                raise Exception("Measure for the streamblock %s is not recorded!!" % str(sSBName))

            # add retrieved result to memory list
            lMULTI_RETRIEVE_RESULTS[session_name].append(dRetrieveResult)

            # update the latest measure result list
            if eachSB in str(lLATEST_RESULT[session_name]):
                # result exist in list
                for index in range (0,len(lLATEST_RESULT[session_name])):
                    if eachSB in str(lLATEST_RESULT[session_name][index]):
                        lLATEST_RESULT[session_name][index] = dRetrieveResult       
            else:
                # result don't exist in list and append it.
                lLATEST_RESULT[session_name].append(dRetrieveResult)

        fSleepTime=float(roundEndTimeStamp-time.time()) 
        if fSleepTime > 0 :
            time.sleep(fSleepTime)
        nowTimeStamp = time.time()

    # end of while

    llk_log.llk_log_info("------------------------------------------------------------")
    llk_log.llk_log_info("VERIFY MEASURE")
    llk_log.llk_log_info("------------------------------------------------------------")

    # verify up streamblock
    i = 1
    for eachSB in lUpSBList:
        llk_log.llk_log_info("*****************************************")
        llk_log.llk_log_info("Verify StreamBlock : %s" % eachSB)
        llk_log.llk_log_info("*****************************************")
        dArgs = {}
        try:
            # get arguments from measure list. lUpMeasureList can be empty. It also can be [${dict1},${EMPTY},${dict2}]
            dArgs = dict(map(lambda (k,v): (str(k).lower(), v), lUpMeasureList[i-1].iteritems()))
        except Exception as inst:
            llk_log.llk_log_info("Since measure is empty, so skip verify stream %s !!!" % eachSB)
            i = i + 1
            continue
        # skip the emtpy measure
        if not dArgs:
            i = i + 1 
            continue
        dArgs['streamblock']  = eachSB
        dArgs['type']         = "RxStreamSummaryResults"

        try:
            dArgs['expectResult'] = lUpExpectResult[i-1]
        except Exception as inst:
            dArgs['expectResult'] = "PASS"

        try:
            _hlk_traffic_verify_multi_measure_from_memory(**dArgs)
        except Exception as inst:
            bResult = "FAIL"
            llk_log.llk_log_error("Up Traffic Varification Failed!!!")
        i = i + 1

    # verify down streamblock
    i = 1
    for eachSB in lDownSBList:
        llk_log.llk_log_info("*****************************************")
        llk_log.llk_log_info("Verify StreamBlock : %s" % eachSB)
        llk_log.llk_log_info("*****************************************")
        dArgs = {}
        try:
            # get arguments from measure list. lUpMeasureList can be empty. It also can be [${dict1},${EMPTY},${dict2}]
            dArgs = dict(map(lambda (k,v): (str(k).lower(), v), lDownMeasureList[i-1].iteritems()))
        except Exception as inst:
            llk_log.llk_log_info("Since measure is empty, so skip verify stream %s !!!" % eachSB)
            i = i + 1
            continue
        # skip the empty measure
        if not dArgs:
            i = i + 1    
            continue
        dArgs['streamblock']  = eachSB
        dArgs['type']         = "RxStreamSummaryResults"

        try:
            dArgs['expectResult'] = lDownExpectResult[i-1]
        except Exception as inst:
            dArgs['expectResult'] = "PASS"

        try:
            _hlk_traffic_verify_multi_measure_from_memory(**dArgs)
        except Exception as inst:
            bResult = "FAIL"
            llk_log.llk_log_error("Down Traffic Varification Failed!!!")
        i = i + 1

    llk_log.llk_log_info("------------------------------------------------------------")
    llk_log.llk_log_info("STOP MEASURE")
    llk_log.llk_log_info("------------------------------------------------------------")
    STC_SESSION[session_name].hlk_traffic_stop_measure(resulttype="RxStreamSummaryResults")

    llk_log.llk_log_info("------------------------------------------------------------")
    llk_log.llk_log_info("STOP UP AND DOWN TRAFFIC")
    llk_log.llk_log_info("------------------------------------------------------------")
    STC_SESSION[session_name].hlk_traffic_stop_traffic(streamblock=lAllSBList)

    llk_log.llk_log_info("------------------------------------------------------------")
    llk_log.llk_log_info("DELETE STREAMBLOCKS")
    llk_log.llk_log_info("------------------------------------------------------------")
    STC_SESSION[session_name].hlk_traffic_delete_streamblock(streamblock="all")

    if bResult == "FAIL":
        raise StcContinueFailure ("Failed to measure IP traffic!!! ")
    else:
        return lLATEST_RESULT[session_name] 

def stc_bidirect_protocol_traffic_measure(protocol_type,up_stream_list,down_stream_list,**kwargs):
    """
    To send upstream and downstream protocol traffics and measure the traffics

    - *protocol_type:*  The protocol type of sending traffics

    - *session_name:*  The name of stc session, default is "first_stc_session"

    - *up_stream_list:(required)*  Upstream list,input params for each stream.
    - *down_stream_list:(required)*  Downstream list,input params for each stream.

         | Default: | None |
         | Type: | List |
         | Example: | 
         |   | ${dStream1} | create dictionary | vlan.vlan1=11 | ethii.srcMac=11:11:11:11:11:11 | load.load=5000 |
         |   | ${dStream2} | create dictionary | vlan.vlan1=12 | ethii.srcMac=11:11:11:11:11:11 | load.load=4000 | 
         |   | @{up_stream_list} | create list | ${dStream1} | ${dStream2} |

    - *up_measure_list:* Upstream Measure list, measure params for each stream.
    - *down_measure_list:* Downstream Measure list, measure params for each stream.
         
         | Default: | None |
         | Type: | List |
         | Example: |  
         |   | ${dMeasure1} | create dictionary | l1bitrate=5000 | l1bitrate_tol=5% | 
         |   | ${dMeasure2} | create dictionary | l1bitrate=4000 | l1bitrate_tol=5% |  
         |   | @{up_measure_list} | create list | ${dMeasure1} | ${dMeasure2} |

    - *up_filter_list:* get the value of measurement output of upstream traffics
    - *down_filter_list:* get the value of measurement output of downstream traffics

         | Default: | None |
         | Type: | List |
         | Example: | 
         |   | @{up_filter_list} | create list | bitrate | l1bitrate |

    - *up_expect_result:* expect result for all upstream measure, split by comma ','. 
    - *down_expect_result:* expect result for all downstream measure, split by comma ','.

         | Default: | all PASS |
         | Type: | String |
         | Format: | PASS,PASS |
         | Value: | PASS,FAIL or NA. NA is to ignore result |

    - *duration:* traffic duration time. Default is 30 seconds. 

         | Default: | 30 |
         | Unit: | second |
         | Type: | Integer |

    - *wait_time:* the wait time between send traffic and start measure. Default is 5 seconds.

         | Default: | 5 |
         | Unit: | second |
         | Type: | Integer |

    - *retrieve_times:* the times of retrieving measure result. Default is 5 times.

         | Default: | 5 |
         | Unit: | times |
         | Type: | Integer |

    - *RETURN:* a list of the measure values of all streams. the returned value items are defined in lUpFilterItems and lDownFilterItems

    Usage Examples:
    | ${dStreamIp} | Create_Dictionary | vlan.vlan1=11 | vlan.pri1=1 | vlan.vlan2=12 | vlan.pri2=2 | ethii.srcMac=11:11:11:11:11:11 | ethii.dstMac=22:22:22:22:22:22 | ipv4.destAddr=135.251.1.1 | ipv4.sourceAddr=135.251.2.2 | load.load=1 | loadunit=FRAMES_PER_SECOND	| 
    | ${returnFilter} | stc bidirect protocol traffic measure | ip | ${lStreamList} | ${lStreamList} | retrieve_times=20 | wait_time=1 | up_filter_list=${lFilterItems} | down_measure_list=${lMeasureList} | up_measure_list=${lMeasureList} | duration=6 | up_expect_result=PASS |up_expect_result=PASS | 


    | ${dStreamArp} | Create_Dictionary | vlan.vlan1=11 | vlan.pri1=1 | vlan.vlan2=12 | vlan.pri2=2 | ethii.srcMac=11:11:11:11:11:11 | ethii.dstMac=22:22:22:22:22:22 | load.load=1 | loadunit=FRAMES_PER_SECOND | arp.senderPAddr=135.251.200.1 | arp.targetPAddr=135.251.200.100 | arp.senderHwAddr=00:00:02:00:00:01 | arp.targetHwAddr=00:00:00:00:00:00 | arp.hardware=0001 | arp.protocol=0800 | arp.ihAddr=6 | arp.ipAddr=4 | arp.operation=1 |              
    | ${returnFilter} | stc bidirect protocol traffic measure | arp | ${lStreamList} | ${lStreamList} | retrieve_times=20 | wait_time=1 | up_filter_list=${lFilterItems} | down_measure_list=${lMeasureList} | up_measure_list=${lMeasureList} | duration=6 | up_expect_result=PASS |up_expect_result=PASS | 


    | ${dStreamPppoe} | Create_Dictionary | vlan.vlan1=11 | vlan.pri1=1 | vlan.vlan2=12 | vlan.pri2=2 | ethii.srcMac=11:11:11:11:11:11 | ethii.dstMac=22:22:22:22:22:22 | load.load=1 | loadunit=FRAMES_PER_SECOND | pppoe.code=9 | pppoe.length=10 | 
       | ${returnFilter} | stc bidirect protocol traffic measure | pppoe | ${lStreamList} | ${lStreamList} | retrieve_times=20 | wait_time=1 | up_filter_list=${lFilterItems} | down_measure_list=${lMeasureList} | up_measure_list=${lMeasureList} | duration=6 | up_expect_result=PASS |up_expect_result=PASS | 

    | ${dStreamDhcp} | Create_Dictionary | vlan.vlan1=11 | vlan.pri1=1 | vlan.vlan2=12 | vlan.pri2=2 | ethii.srcMac=11:11:11:11:11:11 | ethii.dstMac=22:22:22:22:22:22 | ipv4.destAddr=135.251.1.1 | ipv4.sourceAddr=135.251.2.2 | load.load=1 | loadunit=FRAMES_PER_SECOND | udp.destPort=68 | udp.sourcePort=67 | dhcp.messageType=1 | dhcp.clientMac=00:00:02:00:00:01 | dhcp.clientAddr=135.251.200.171 | 
    | ${returnFilter} | stc bidirect protocol traffic measure | dhcp | ${lStreamList} | ${lStreamList} | retrieve_times=20 | wait_time=1 | up_filter_list=${lFilterItems} | down_measure_list=${lMeasureList} | up_measure_list=${lMeasureList} | duration=6 | up_expect_result=PASS |up_expect_result=PASS | 

    | ${dStreamCcm} | Create_Dictionary | vlan.vlan1=11 | vlan.pri1=1 | vlan.vlan2=12 | vlan.pri2=2 | ethii.srcMac=11:11:11:11:11:11 | ethii.dstMac=22:22:22:22:22:22 | load.load=1 | load.loadunit=FRAMES_PER_SECOND | ccm.maepi=1 | ccm.cfmHeader.mdLevel=5 | ccm.maid.mdnf=04 | ccm.maid.smaf=02 | ccm.maid.sman=6d61323030 | ccm.maid.mdn=6d6435 | ccm.maid.padding=000000000000000000000000000000000000000000000000000000000000000000000000 | ccm.sequenceNumber=31900 | 	
    | ${returnFilter} | stc bidirect protocol traffic measure | ccm | ${lStreamList} | ${lStreamList} | retrieve_times=20 | wait_time=1 | up_filter_list=${lFilterItems} | down_measure_list=${lMeasureList} | up_measure_list=${lMeasureList} | duration=6 | up_expect_result=PASS |up_expect_result=PASS | 

    | ${dStreamIgmpv2} | Create_Dictionary | vlan.vlan1=11 | vlan.pri1=1 | vlan.vlan2=12 | vlan.pri2=2 | ethii.srcMac=11:11:11:11:11:11 | ethii.dstMac=22:22:22:22:22:22 | load.load=1 | load.loadunit=FRAMES_PER_SECOND | igmpv2.groupAddress=225.0.0.2 | igmpv2.type=17	| 
    | ${returnFilter} | stc bidirect protocol traffic measure | igmpv2 | ${lStreamList} | ${lStreamList} | retrieve_times=20 | wait_time=1 | up_filter_list=${lFilterItems} | down_measure_list=${lMeasureList} | up_measure_list=${lMeasureList} | duration=6 | up_expect_result=PASS |up_expect_result=PASS | 

    """
    global lSUPPORTED_TRAFFIC_LAYER
    global lSUPPORTED_TRAFFIC_PROTOCOL

    session_name = "first_stc_session"
    if "session_name" in kwargs:
        session_name = dKwargs['session_name'].lower()

    sNetworkPortId = dNETWORK_PORT[session_name]['PortId'] 
    sClientPortId = dCLIENT_PORT[session_name]['PortId'] 
    lFilterResult = []
    dKwargs = dict(map(lambda (k,v): (str(k).lower(), v), kwargs.iteritems()))

    # check protocol type
    protocol_type = protocol_type.lower()
    assert(protocol_type in lSUPPORTED_TRAFFIC_PROTOCOL), "This kind of protocol is not supported:%s" % protocol_type

    lUpExpectResult = []
    if "up_expect_result" in dKwargs.keys():
        lUpExpectResult = dKwargs['up_expect_result'].split(",")

    lDownExpectResult = []
    if "down_expect_result" in dKwargs.keys():
        lDownExpectResult = dKwargs['down_expect_result'].split(",")

    lUpMeasureList = []
    if "up_measure_list" in dKwargs.keys():
        lUpMeasureList = dKwargs['up_measure_list']

    lDownMeasureList = []
    if "down_measure_list" in dKwargs.keys():
        lDownMeasureList = dKwargs['down_measure_list']

    lUpFilterItems = []
    if "up_filter_list" in dKwargs.keys():
        lUpFilterItems = dKwargs['up_filter_list']

    lDownFilterItems = []
    if "down_filter_list" in dKwargs.keys():
        lDownFilterItems = dKwargs['down_filter_list']

    iLenExpect  = len(lUpExpectResult)+len(lDownExpectResult)
    iLenStream  = len(up_stream_list)+len(down_stream_list)
    iLenMeasure = len(lUpMeasureList)+len(lDownMeasureList)

    bResult = "PASS"

    if kwargs:
        dKwargs = dict(map(lambda (k,v): (str(k).lower(), v), kwargs.iteritems()))
    else:
        dKwargs = {}

    iWaitTime = 5
    if "wait_time" in dKwargs.keys():
        iWaitTime = int(dKwargs['wait_time'])

    iDuration = 30          
    if "duration" in dKwargs.keys():
        iDuration = int(dKwargs['duration'])

    iRetrieveTimes = 5
    if "retrieve_times" in dKwargs.keys():
        iRetrieveTimes = int(dKwargs['retrieve_times'])

    if int(iDuration) <= int(iWaitTime):
        raise AssertionError("duration %s must large than wait_time %s!!" % (iDuration,iWaitTime))

    fInterval = 1.0*(iDuration-iWaitTime)/iRetrieveTimes
     
    lUpExpectResult = []
    if "up_expect_result" in dKwargs.keys():
        lUpExpectResult = dKwargs['up_expect_result'].split(",")

    lDownExpectResult = []
    if "down_expect_result" in dKwargs.keys():
        lDownExpectResult = dKwargs['down_expect_result'].split(",")

    # config stream blocks
    # create a stream name with time stamp
    llk_log.llk_log_info("------------------------------------------------------------")
    llk_log.llk_log_info("CREATE UP STREAM BLOCKS")
    llk_log.llk_log_info("------------------------------------------------------------")
    lUpSBList = []
    i = 1
    for dStream in up_stream_list:
        timeStamp = time.time()
        sSBName = "UP_STREAM_%s_%s" % (i,timeStamp)
        lUpSBList.append(sSBName)
        i = i +1

        # create a streamblock 
        STC_SESSION[session_name].hlk_traffic_config_streamblock(name=sSBName,portid=sClientPortId)
        
        # attach the following layer
        for each_layer_type in lSUPPORTED_TRAFFIC_LAYER :
            if each_layer_type in str(dPROTOCOL_MAP_TO_LAYER[protocol_type]):
                dProtocolParam = _get_protocol_parameters(protocolType=each_layer_type,**dStream)
                STC_SESSION[session_name].hlk_traffic_config_streamblock(
                name=sSBName,streamtype=each_layer_type,**dProtocolParam) 

        # configure frame layer
        if 'frame' in dStream.keys():
            dProtocolParam = _get_protocol_parameters(protocolType='frame',**dStream)
            STC_SESSION[session_name].hlk_traffic_config_streamblock(
            name=sSBName,streamtype='frame',**dProtocolParam)

    # config stream blocks
    # create a stream name with time stamp
    llk_log.llk_log_info("------------------------------------------------------------")
    llk_log.llk_log_info("CREATE DOWN STREAM BLOCKS")
    llk_log.llk_log_info("------------------------------------------------------------")
    lDownSBList = []
    i = 1
    for dStream in down_stream_list:
        timeStamp = time.time()
        sSBName = "DOWN_STREAM_%s_%s" % (i,timeStamp)
        lDownSBList.append(sSBName)
        i = i +1

        # create a streamblock 
        STC_SESSION[session_name].hlk_traffic_config_streamblock(name=sSBName,portid=sNetworkPortId)
        
        # attach the following layer
        for each_layer_type in lSUPPORTED_TRAFFIC_LAYER :
            if each_layer_type in str(dPROTOCOL_MAP_TO_LAYER[protocol_type]):
                dProtocolParam = _get_protocol_parameters(protocolType=each_layer_type,**dStream)
                STC_SESSION[session_name].hlk_traffic_config_streamblock(
                name=sSBName,streamtype=each_layer_type,**dProtocolParam) 

        # configure frame layer
        if 'frame' in dStream.keys():
            dProtocolParam = _get_protocol_parameters(protocolType='frame',**dStream)
            STC_SESSION[session_name].hlk_traffic_config_streamblock(
            name=sSBName,streamtype='frame',**dProtocolParam)


    llk_log.llk_log_info("------------------------------------------------------------")
    llk_log.llk_log_info("START UP AND DOWN STREAM TRAFFIC")
    llk_log.llk_log_info("------------------------------------------------------------")
    lAllSBList = lUpSBList + lDownSBList
    STC_SESSION[session_name].hlk_traffic_start_traffic(streamblock=lAllSBList)

    llk_log.llk_log_info("------------------------------------------------------------")
    llk_log.llk_log_info("START MEASURE UP AND DOWN STREAMS")
    llk_log.llk_log_info("------------------------------------------------------------")
    lMULTI_RETRIEVE_RESULTS[session_name] = []
    lLATEST_RESULT[session_name] = []
    STC_SESSION[session_name].hlk_traffic_start_measure(
    clearResult='FALSE',waitTime=iWaitTime,resulttype="RxStreamSummaryResults",
    filenamePrefix="RxStreamSummaryResults_file",interval=1,configtype="StreamBlock") 

    llk_log.llk_log_info("------------------------------------------------------------")
    llk_log.llk_log_info("RETRIEVE MEASURE OF UP AND DOWN STREAMS")
    llk_log.llk_log_info("------------------------------------------------------------")
    lMULTI_RETRIEVE_RESULTS[session_name] = []
    startMeasureStamp = time.time()
    stopMeasureStamp = startMeasureStamp + int(iDuration-iWaitTime)
    nowTimeStamp = time.time()
    j = 0
    while ( nowTimeStamp < stopMeasureStamp ):
        roundEndTimeStamp = nowTimeStamp + fInterval
        j = j + 1
        llk_log.llk_log_debug("**************Retrieve Times %s****************" % j )

        # create measure list 
        i = 1
        ddRetrieveMeasure = {}
        lMeasureArgs = [] 
        for eachSB in lUpSBList:
            # check if lUpMeasureList[i-1] exist
            try:
                measure = lUpMeasureList[i-1]
            except Exception as inst:
                i = i + 1
                continue
           
            # skip the emtpy measure
            if not lUpMeasureList[i-1]:
                i = i + 1 
                continue

            for eachMeasureKey in lUpMeasureList[i-1].keys():
                # skip two parameters "l1bitrate_tol" and "bitrate_tol"
                # skip the parameters which include "_tol"
                if "_tol" not in str(eachMeasureKey).lower() and eachMeasureKey not in lMeasureArgs:
                    lMeasureArgs.append(eachMeasureKey.lower())

            for eachMeasureKey in lUpFilterItems:
                if eachMeasureKey not in lMeasureArgs:
                    lMeasureArgs.append(eachMeasureKey.lower())

            i = i + 1
        
        i = 1
        for eachSB in lDownSBList:
            # check if lDownMeasureList[i-1] exist
            try:
                measure = lDownMeasureList[i-1]
            except Exception as inst:
                i = i + 1
                continue

            # skip the emtpy measure
            if not lDownMeasureList[i-1]:
                i = i + 1
                continue

            for eachMeasureKey in lDownMeasureList[i-1].keys():
                # skip two parameters "l1bitrate_tol" and "bitrate_tol"
                # skip the parameters which include "_tol"
                if "_tol" not in str(eachMeasureKey).lower() and eachMeasureKey not in lMeasureArgs: 
                    lMeasureArgs.append(eachMeasureKey.lower())

            for eachMeasureKey in lDownFilterItems:
                if eachMeasureKey not in lMeasureArgs:
                    lMeasureArgs.append(eachMeasureKey.lower())

            i = i + 1

        # retrieve measure result of all streams
        ddRetrieveMeasure = STC_SESSION[session_name].oLlkStc.llk_stc_measure_subflow(
        STC_SESSION[session_name].dMeasureObjects["RxStreamSummaryResults"], *lMeasureArgs)

        lAllSBlist = lUpSBList + lDownSBList

        for eachSB in lAllSBList:
            dRetrieveResult = {}
            if str(eachSB)+":0" in ddRetrieveMeasure.keys():
                sTotalStreamCnt = int(ddRetrieveMeasure[str(eachSB)+":0"]['flowcount'])
                for i in range(0,sTotalStreamCnt+1):
                    sMeasureKey = str(eachSB)+":"+str(i)
                    if sMeasureKey in ddRetrieveMeasure.keys():
                        dRetrieveResult[sMeasureKey] = ddRetrieveMeasure[sMeasureKey]
                    else:
                        llk_log_warn("Measure does not exist for the given Streamblock %s, stream count %s" % (str(sSBName),str(i)))
            else:
                llk_log_end_proc()
                raise Exception("Measure for the streamblock %s is not recorded!!" % str(sSBName))

            # add retrieved result to memory list
            lMULTI_RETRIEVE_RESULTS[session_name].append(dRetrieveResult)

            # update the latest measure result list
            if eachSB in str(lLATEST_RESULT[session_name]):
                # result exist in list
                for index in range (0,len(lLATEST_RESULT[session_name])):
                    if eachSB in str(lLATEST_RESULT[session_name][index]):
                        lLATEST_RESULT[session_name][index] = dRetrieveResult       
            else:
                # result don't exist in list and append it.
                lLATEST_RESULT[session_name].append(dRetrieveResult)

        fSleepTime=float(roundEndTimeStamp-time.time()) 
        if fSleepTime > 0 :
            time.sleep(fSleepTime)
        nowTimeStamp = time.time()

    # end of while

    llk_log.llk_log_info("------------------------------------------------------------")
    llk_log.llk_log_info("VERIFY MEASURE")
    llk_log.llk_log_info("------------------------------------------------------------")

    # verify up streamblock
    i = 1
    for eachSB in lUpSBList:
        llk_log.llk_log_info("*****************************************")
        llk_log.llk_log_info("Verify StreamBlock : %s" % eachSB)
        llk_log.llk_log_info("*****************************************")
        dArgs = {}
        try:
            # get arguments from measure list. lUpMeasureList can be empty. It also can be [${dict1},${EMPTY},${dict2}]
            dArgs = dict(map(lambda (k,v): (str(k).lower(), v), lUpMeasureList[i-1].iteritems()))
        except Exception as inst:
            llk_log.llk_log_info("Since measure is empty, so skip verify stream %s !!!" % eachSB)
            i = i + 1
            continue
        # skip the emtpy measure
        if not dArgs:
            i = i + 1 
            continue
        dArgs['streamblock']  = eachSB
        dArgs['type']         = "RxStreamSummaryResults"

        try:
            dArgs['expectResult'] = lUpExpectResult[i-1]
        except Exception as inst:
            dArgs['expectResult'] = "PASS"

        try:
            _hlk_traffic_verify_multi_measure_from_memory(**dArgs)
        except Exception as inst:
            bResult = "FAIL"
            llk_log.llk_log_error("Up Traffic Varification Failed!!!")
        i = i + 1

    # verify down streamblock
    i = 1
    for eachSB in lDownSBList:
        llk_log.llk_log_info("*****************************************")
        llk_log.llk_log_info("Verify StreamBlock : %s" % eachSB)
        llk_log.llk_log_info("*****************************************")
        dArgs = {}
        try:
            # get arguments from measure list. lUpMeasureList can be empty. It also can be [${dict1},${EMPTY},${dict2}]
            dArgs = dict(map(lambda (k,v): (str(k).lower(), v), lDownMeasureList[i-1].iteritems()))
        except Exception as inst:
            llk_log.llk_log_info("Since measure is empty, so skip verify stream %s !!!" % eachSB)
            i = i + 1
            continue
        # skip the empty measure
        if not dArgs:
            i = i + 1    
            continue
        dArgs['streamblock']  = eachSB
        dArgs['type']         = "RxStreamSummaryResults"

        try:
            dArgs['expectResult'] = lDownExpectResult[i-1]
        except Exception as inst:
            dArgs['expectResult'] = "PASS"

        try:
            _hlk_traffic_verify_multi_measure_from_memory(**dArgs)
        except Exception as inst:
            bResult = "FAIL"
            llk_log.llk_log_error("Down Traffic Varification Failed!!!")
        i = i + 1

    llk_log.llk_log_info("------------------------------------------------------------")
    llk_log.llk_log_info("STOP MEASURE")
    llk_log.llk_log_info("------------------------------------------------------------")
    STC_SESSION[session_name].hlk_traffic_stop_measure(resulttype="RxStreamSummaryResults")

    llk_log.llk_log_info("------------------------------------------------------------")
    llk_log.llk_log_info("STOP UP AND DOWN TRAFFIC")
    llk_log.llk_log_info("------------------------------------------------------------")
    STC_SESSION[session_name].hlk_traffic_stop_traffic(streamblock=lAllSBList)

    llk_log.llk_log_info("------------------------------------------------------------")
    llk_log.llk_log_info("DELETE STREAMBLOCKS")
    llk_log.llk_log_info("------------------------------------------------------------")
    STC_SESSION[session_name].hlk_traffic_delete_streamblock(streamblock="all")

    if bResult == "FAIL":
        raise StcContinueFailure ("Failed to measure IP traffic!!! ")
    else:
        return lLATEST_RESULT[session_name] 


def stc_create_streamblock(sSBName,sPortId,sProtocolType,**dStream) : 
    global lSUPPORTED_TRAFFIC_LAYER

    # create a streamblock 
    session_name = dStream.setdefault('session_name','first_stc_session')
    STC_SESSION[session_name].hlk_traffic_config_streamblock(name=sSBName,portid=sPortId)
        
    # attach the following layer
    for each_layer_type in lSUPPORTED_TRAFFIC_LAYER : 
        if each_layer_type in str(dPROTOCOL_MAP_TO_LAYER[sProtocolType]):
            dProtocolParam = _get_protocol_parameters(protocolType=each_layer_type,**dStream)
            STC_SESSION[session_name].hlk_traffic_config_streamblock(
            name=sSBName,streamtype=each_layer_type,**dProtocolParam)

    # configure frame layer
    if 'frame' in dStream.keys():
        dProtocolParam = _get_protocol_parameters(protocolType='frame',**dStream)
        STC_SESSION[session_name].hlk_traffic_config_streamblock(
        name=sSBName,streamtype='frame',**dProtocolParam)

    STC_SESSION[session_name].oLlkStc.llk_stc_send_command("stc::apply")

def _stc_protocol_traffic_measure(sPortId,protocol_type,stream_list,**kwargs):

    global lSUPPORTED_TRAFFIC_LAYER
    global lSUPPORTED_TRAFFIC_PROTOCOL
    bResult = "PASS"

    # check protocol type
    protocol_type = protocol_type.lower()
    assert(protocol_type in lSUPPORTED_TRAFFIC_PROTOCOL), "This kind of protocol is not supported:%s" % protocol_type 

    if kwargs:
        dKwargs = dict(map(lambda (k,v): (str(k).lower(), v), kwargs.iteritems()))
    else:
        dKwargs = {}

    session_name = "first_stc_session"
    if "session_name" in dKwargs:
        session_name = dKwargs['session_name'].lower()

    lMeasureList = []
    if "measure_list" in dKwargs.keys():
        lMeasureList = dKwargs['measure_list']

    iWaitTime = 5
    if "wait_time" in dKwargs.keys():
        iWaitTime = int(dKwargs['wait_time'])

    iDuration = 30          
    if "duration" in dKwargs.keys():
        iDuration = int(dKwargs['duration'])

    if int(iDuration) <= int(iWaitTime):
        raise AssertionError("duration %s must large than wait_time %s!!" % (iDuration,iWaitTime))

    iRetrieveTimes = 5
    if "retrieve_times" in dKwargs.keys():
        iRetrieveTimes = int(dKwargs['retrieve_times'])

    fInterval = 1.0*(iDuration-iWaitTime)/iRetrieveTimes

    lExpectResult = []
    if "expect_result" in dKwargs.keys():
        lExpectResult = dKwargs['expect_result'].split(",")

    lFilterItems = []
    if "filter_list" in dKwargs.keys():
        lFilterItems = dKwargs['filter_list']

    # config stream blocks
    # create a stream name with time stamp
    llk_log.llk_log_info("------------------------------------------------------------")
    llk_log.llk_log_info("CREATE STREAM BLOCKS")
    llk_log.llk_log_info("------------------------------------------------------------")
    lSBList = []
    i = 1
    for dStream in stream_list:
        timeStamp = time.time()
        sSBName = "STREAM_%s_%s" % (i,timeStamp)
        lSBList.append(sSBName)
        i = i +1

        # create a streamblock 
        STC_SESSION[session_name].hlk_traffic_config_streamblock(name=sSBName,portid=sPortId)
        
        # attach the following layer
        for each_layer_type in lSUPPORTED_TRAFFIC_LAYER : 
            if each_layer_type in str(dPROTOCOL_MAP_TO_LAYER[protocol_type]):
                dProtocolParam = _get_protocol_parameters(protocolType=each_layer_type,**dStream)
                STC_SESSION[session_name].hlk_traffic_config_streamblock(
                name=sSBName,streamtype=each_layer_type,**dProtocolParam)

        # configure frame layer
        if 'frame' in dStream.keys():
            dProtocolParam = _get_protocol_parameters(protocolType='frame',**dStream)
            STC_SESSION[session_name].hlk_traffic_config_streamblock(
            name=sSBName,streamtype='frame',**dProtocolParam)

    llk_log.llk_log_info("------------------------------------------------------------")
    llk_log.llk_log_info("START TRAFFIC")
    llk_log.llk_log_info("------------------------------------------------------------")
    STC_SESSION[session_name].hlk_traffic_start_traffic(streamblock=lSBList)

    llk_log.llk_log_info("------------------------------------------------------------")
    llk_log.llk_log_info("START MEASURE")
    llk_log.llk_log_info("------------------------------------------------------------")
    lMULTI_RETRIEVE_RESULTS[session_name] = []
    lLATEST_RESULT[session_name] = []
    STC_SESSION[session_name].hlk_traffic_start_measure(
    clearResult='FALSE',waitTime=iWaitTime,resulttype="RxStreamSummaryResults",
    filenamePrefix="RxStreamSummaryResults_file",interval=1,configtype="StreamBlock") 

    llk_log.llk_log_info("------------------------------------------------------------")
    llk_log.llk_log_info("RETRIEVE MEASURE")
    llk_log.llk_log_info("------------------------------------------------------------")
    lMULTI_RETRIEVE_RESULTS[session_name] = []
    startMeasureStamp = time.time()
    stopMeasureStamp = startMeasureStamp + int(iDuration-iWaitTime)
    nowTimeStamp = time.time()
    j = 0
    while ( nowTimeStamp < stopMeasureStamp ):
        roundEndTimeStamp = nowTimeStamp + fInterval
        j = j + 1
        i = 1 
        llk_log.llk_log_debug("**************Retrieve Times %s****************" % j )

        # create measure list
        i = 1
        ddRetrieveMeasure = {}
        lMeasureArgs = []
        for eachSB in lSBList:
            # check if lMeasureList[i-1] exist
            try:
                measure = lMeasureList[i-1]
            except Exception as inst:
                i = i + 1
                continue

            # skip the emtpy measure
            if not lMeasureList[i-1]:
                i = i + 1
                continue

            for eachMeasureKey in lMeasureList[i-1].keys():
                # skip two parameters "l1bitrate_tol" and "bitrate_tol"
                # skip the parameters which include "_tol"
                if "_tol" not in str(eachMeasureKey).lower() and eachMeasureKey not in lMeasureArgs:
                    lMeasureArgs.append(eachMeasureKey.lower())

            for eachMeasureKey in lFilterItems:
                if eachMeasureKey not in lMeasureArgs:
                    lMeasureArgs.append(eachMeasureKey.lower())

            i = i + 1
            
        # retrieve measure result of all streams
        ddRetrieveMeasure = STC_SESSION[session_name].oLlkStc.llk_stc_measure_subflow(
        STC_SESSION[session_name].dMeasureObjects["RxStreamSummaryResults"], *lMeasureArgs)

        for eachSB in lSBList:
            dRetrieveResult = {}
            if str(eachSB)+":0" in ddRetrieveMeasure.keys():
                sTotalStreamCnt = int(ddRetrieveMeasure[str(eachSB)+":0"]['flowcount'])
                for i in range(0,sTotalStreamCnt+1):
                    sMeasureKey = str(eachSB)+":"+str(i)
                    if sMeasureKey in ddRetrieveMeasure.keys():
                        dRetrieveResult[sMeasureKey] = ddRetrieveMeasure[sMeasureKey]
                    else:
                        llk_log_warn("Measure does not exist for the given Streamblock %s, stream count %s" % (str(sSBName),str(i)))
            else:
                llk_log_end_proc()
                raise Exception("Measure for the streamblock %s is not recorded!!" % str(sSBName))

            # add retrieved result to memory list
            lMULTI_RETRIEVE_RESULTS[session_name].append(dRetrieveResult)

            # update the latest measure result list
            if eachSB in str(lLATEST_RESULT[session_name]):
                # result exist in list
                for index in range (0,len(lLATEST_RESULT[session_name])):
                    if eachSB in str(lLATEST_RESULT[session_name][index]):
                        lLATEST_RESULT[session_name][index] = dRetrieveResult
            else:
                # result don't exist in list and append it.
                lLATEST_RESULT[session_name].append(dRetrieveResult)

        fSleepTime=float(roundEndTimeStamp-time.time())
        if fSleepTime > 0 :
            time.sleep(fSleepTime)
        nowTimeStamp = time.time()

    llk_log.llk_log_info("------------------------------------------------------------")
    llk_log.llk_log_info("VERIFY MEASURE")
    llk_log.llk_log_info("------------------------------------------------------------")
    i = 1
    # for each stramblock, verify a measure result 
    for eachSB in lSBList:
        llk_log.llk_log_info("*****************************************")
        llk_log.llk_log_info("Verify StreamBlock : %s" % eachSB)
        llk_log.llk_log_info("*****************************************")
        dArgs = {}
        try:
            dArgs = dict(map(lambda (k,v): (str(k).lower(), v), lMeasureList[i-1].iteritems()))
        except Exception as inst:
            llk_log.llk_log_info("Skip verify stream %s since measure is empty !!!" % eachSB)
            i = i + 1
            continue
        dArgs['streamblock']  = eachSB
        dArgs['type']         = "RxStreamSummaryResults"

        try:
            dArgs['expectResult'] = lExpectResult[i-1]
        except Exception as inst:
            dArgs['expectResult'] = "PASS"

        try:
            _hlk_traffic_verify_multi_measure_from_memory(**dArgs)
        except Exception as inst:
            bResult = "FAIL"
            llk_log.llk_log_error("Traffic Varification Failed!!!")
        i = i + 1

    llk_log.llk_log_info("------------------------------------------------------------")
    llk_log.llk_log_info("STOP MEASURE")
    llk_log.llk_log_info("------------------------------------------------------------")
    STC_SESSION[session_name].hlk_traffic_stop_measure(resulttype="RxStreamSummaryResults")

    llk_log.llk_log_info("------------------------------------------------------------")
    llk_log.llk_log_info("STOP TRAFFIC")
    llk_log.llk_log_info("------------------------------------------------------------")
    STC_SESSION[session_name].hlk_traffic_stop_traffic(streamblock=lSBList)

    llk_log.llk_log_info("------------------------------------------------------------")
    llk_log.llk_log_info("DELETE STREAMBLOCKS")
    llk_log.llk_log_info("------------------------------------------------------------")
    STC_SESSION[session_name].hlk_traffic_delete_streamblock(streamblock="all")

    """
    llk_log.llk_log_info("------------------------------------------------------------")
    llk_log.llk_log_info("UNATTACH STC PORTS")
    llk_log.llk_log_info("------------------------------------------------------------")
    STC_SESSION[session_name].hlk_traffic_cleanup()
    """

    if bResult == "FAIL":
        raise StcContinueFailure ("Failed to measure IP traffic!!! ")
    else:
        return lLATEST_RESULT[session_name] 


def _stc_ip_traffic_measure(sPortId,stream_list,**kwargs):

    bResult = "PASS"

    if kwargs:
        dKwargs = dict(map(lambda (k,v): (str(k).lower(), v), kwargs.iteritems()))
    else:
        dKwargs = {}

    session_name = "first_stc_session"
    if "session_name" in dKwargs:
        session_name = dKwargs['session_name'].lower()

    lMeasureList = []
    if "measure_list" in dKwargs.keys():
        lMeasureList = dKwargs['measure_list']

    iWaitTime = 5
    if "wait_time" in dKwargs.keys():
        iWaitTime = int(dKwargs['wait_time'])

    iDuration = 30          
    if "duration" in dKwargs.keys():
        iDuration = int(dKwargs['duration'])

    if int(iDuration) <= int(iWaitTime):
        raise AssertionError("duration %s must large than wait_time %s!!" % (iDuration,iWaitTime))

    iRetrieveTimes = 5
    if "retrieve_times" in dKwargs.keys():
        iRetrieveTimes = int(dKwargs['retrieve_times'])

    fInterval = 1.0*(iDuration-iWaitTime)/iRetrieveTimes

    lExpectResult = []
    if "expect_result" in dKwargs.keys():
        lExpectResult = dKwargs['expect_result'].split(",")

    lFilterItems = []
    if "filter_list" in dKwargs.keys():
        lFilterItems = dKwargs['filter_list']

    # config stream blocks
    # create a stream name with time stamp
    llk_log.llk_log_info("------------------------------------------------------------")
    llk_log.llk_log_info("CREATE STREAM BLOCKS")
    llk_log.llk_log_info("------------------------------------------------------------")
    lSBList = []
    i = 1
    for dStream in stream_list:
        timeStamp = time.time()
        sSBName = "STREAM_%s_%s" % (i,timeStamp)
        lSBList.append(sSBName)
        i = i +1

        # create a streamblock 
        STC_SESSION[session_name].hlk_traffic_config_streamblock(name=sSBName,portid=sPortId)
        
        # attach protocol ethii
        dProtocolParam = _get_protocol_parameters(protocolType='ethii',**dStream)
        STC_SESSION[session_name].hlk_traffic_config_streamblock(name=sSBName,type="ethii",**dProtocolParam)
        
        # attach protocol ipv4
        dProtocolParam = _get_protocol_parameters(protocolType='ipv4',**dStream)
        STC_SESSION[session_name].hlk_traffic_config_streamblock(name=sSBName,type="ipv4",**dProtocolParam)
        
        # attach vlan
        dProtocolParam = _get_protocol_parameters(protocolType='vlan',**dStream)
        STC_SESSION[session_name].hlk_traffic_config_streamblock(name=sSBName,type="vlan",**dProtocolParam)

        # attach load 
        dProtocolParam = _get_protocol_parameters(protocolType='load',**dStream)
        STC_SESSION[session_name].hlk_traffic_config_streamblock(name=sSBName,type="load",**dProtocolParam)

        # config frame
        dProtocolParam = _get_protocol_parameters(protocolType='frame',**dStream)
        STC_SESSION[session_name].hlk_traffic_config_streamblock(name=sSBName,type="frame",**dProtocolParam)

    llk_log.llk_log_info("------------------------------------------------------------")
    llk_log.llk_log_info("START TRAFFIC")
    llk_log.llk_log_info("------------------------------------------------------------")
    STC_SESSION[session_name].hlk_traffic_start_traffic(streamblock=lSBList)

    llk_log.llk_log_info("------------------------------------------------------------")
    llk_log.llk_log_info("START MEASURE")
    llk_log.llk_log_info("------------------------------------------------------------")
    lMULTI_RETRIEVE_RESULTS[session_name] = []
    lLATEST_RESULT[session_name] = []
    STC_SESSION[session_name].hlk_traffic_start_measure(clearResult='FALSE',waitTime=iWaitTime,resulttype="RxStreamSummaryResults",
    filenamePrefix="RxStreamSummaryResults_file",interval=1,configtype="StreamBlock") 

    llk_log.llk_log_info("------------------------------------------------------------")
    llk_log.llk_log_info("RETRIEVE MEASURE")
    llk_log.llk_log_info("------------------------------------------------------------")
    lMULTI_RETRIEVE_RESULTS[session_name] = []
    startMeasureStamp = time.time()
    stopMeasureStamp = startMeasureStamp + int(iDuration-iWaitTime)
    nowTimeStamp = time.time()
    j = 0
    while ( nowTimeStamp < stopMeasureStamp ):
        roundEndTimeStamp = nowTimeStamp + fInterval
        j = j + 1
        i = 1 
        llk_log.llk_log_debug("**************Retrieve Times %s****************" % j )

        # create measure list
        i = 1
        ddRetrieveMeasure = {}
        lMeasureArgs = []
        for eachSB in lSBList:
            # check if lMeasureList[i-1] exist
            try:
                measure = lMeasureList[i-1]
            except Exception as inst:
                i = i + 1
                continue

            # skip the emtpy measure
            if not lMeasureList[i-1]:
                i = i + 1
                continue

            for eachMeasureKey in lMeasureList[i-1].keys():
                # skip two parameters "l1bitrate_tol" and "bitrate_tol"
                # skip the parameters which include "_tol"
                if "_tol" not in str(eachMeasureKey).lower() and eachMeasureKey not in lMeasureArgs:
                    lMeasureArgs.append(eachMeasureKey.lower())

            for eachMeasureKey in lFilterItems:
                if eachMeasureKey not in lMeasureArgs:
                    lMeasureArgs.append(eachMeasureKey.lower())

            i = i + 1
            
        # retrieve measure result of all streams
        ddRetrieveMeasure = STC_SESSION[session_name].oLlkStc.llk_stc_measure_subflow(STC_SESSION[session_name].dMeasureObjects["RxStreamSummaryResults"], *lMeasureArgs)

        for eachSB in lSBList:
            dRetrieveResult = {}
            if str(eachSB)+":0" in ddRetrieveMeasure.keys():
                sTotalStreamCnt = int(ddRetrieveMeasure[str(eachSB)+":0"]['flowcount'])
                for i in range(0,sTotalStreamCnt+1):
                    sMeasureKey = str(eachSB)+":"+str(i)
                    if sMeasureKey in ddRetrieveMeasure.keys():
                        dRetrieveResult[sMeasureKey] = ddRetrieveMeasure[sMeasureKey]
                    else:
                        llk_log_warn("Measure does not exist for the given Streamblock %s, stream count %s" % (str(sSBName),str(i)))
            else:
                llk_log_end_proc()
                raise Exception("Measure for the streamblock %s is not recorded!!" % str(sSBName))

            # add retrieved result to memory list
            lMULTI_RETRIEVE_RESULTS[session_name].append(dRetrieveResult)

            # update the latest measure result list
            if eachSB in str(lLATEST_RESULT[session_name]):
                # result exist in list
                for index in range (0,len(lLATEST_RESULT[session_name])):
                    if eachSB in str(lLATEST_RESULT[session_name][index]):
                        lLATEST_RESULT[session_name][index] = dRetrieveResult
            else:
                # result don't exist in list and append it.
                lLATEST_RESULT[session_name].append(dRetrieveResult)

        fSleepTime=float(roundEndTimeStamp-time.time())
        if fSleepTime > 0 :
            time.sleep(fSleepTime)
        nowTimeStamp = time.time()

    llk_log.llk_log_info("------------------------------------------------------------")
    llk_log.llk_log_info("VERIFY MEASURE")
    llk_log.llk_log_info("------------------------------------------------------------")
    i = 1
    # for each stramblock, verify a measure result 
    for eachSB in lSBList:
        llk_log.llk_log_info("*****************************************")
        llk_log.llk_log_info("Verify StreamBlock : %s" % eachSB)
        llk_log.llk_log_info("*****************************************")
        dArgs = {}
        try:
            dArgs = dict(map(lambda (k,v): (str(k).lower(), v), lMeasureList[i-1].iteritems()))
        except Exception as inst:
            llk_log.llk_log_info("Skip verify stream %s since measure is empty !!!" % eachSB)
            i = i + 1
            continue
        dArgs['streamblock']  = eachSB
        dArgs['type']         = "RxStreamSummaryResults"

        try:
            dArgs['expectResult'] = lExpectResult[i-1]
        except Exception as inst:
            dArgs['expectResult'] = "PASS"

        try:
            _hlk_traffic_verify_multi_measure_from_memory(**dArgs)
        except Exception as inst:
            bResult = "FAIL"
            llk_log.llk_log_error("Traffic Varification Failed!!!")
        i = i + 1

    llk_log.llk_log_info("------------------------------------------------------------")
    llk_log.llk_log_info("STOP MEASURE")
    llk_log.llk_log_info("------------------------------------------------------------")
    STC_SESSION[session_name].hlk_traffic_stop_measure(resulttype="RxStreamSummaryResults")

    llk_log.llk_log_info("------------------------------------------------------------")
    llk_log.llk_log_info("STOP TRAFFIC")
    llk_log.llk_log_info("------------------------------------------------------------")
    STC_SESSION[session_name].hlk_traffic_stop_traffic(streamblock=lSBList)

    llk_log.llk_log_info("------------------------------------------------------------")
    llk_log.llk_log_info("DELETE STREAMBLOCKS")
    llk_log.llk_log_info("------------------------------------------------------------")
    STC_SESSION[session_name].hlk_traffic_delete_streamblock(streamblock="all")

    """
    llk_log.llk_log_info("------------------------------------------------------------")
    llk_log.llk_log_info("UNATTACH STC PORTS")
    llk_log.llk_log_info("------------------------------------------------------------")
    STC_SESSION[session_name].hlk_traffic_cleanup()
    """

    if bResult == "FAIL":
        raise StcContinueFailure ("Failed to measure IP traffic!!! ")
    else:
        return lLATEST_RESULT[session_name] 

def _avg_rate_in_measure_result(session_name,sSBName):
    lRateList = ["bitrate","cellrate","framerate","l1bitrate"]
    dMeasureResult = {}
    iRateSummary = 0
    iResultLen = 0
    try: 
        for eachRate in lRateList:
            # calculate the avg rate
            iRateSummary = 0
            iResultLen   = 0

            for dEachResult in lMULTI_RETRIEVE_RESULTS[session_name]:
                # dEachResult: {'SB1:1': {'bitcount': '19'}, 'SB1:0': {'flowcount': '1'}}

                if sSBName in str(dEachResult) and eachRate in str(dEachResult) :
                    m = re.search("'%s': +'([\d|\.]+)'" % eachRate,str(dEachResult))
                    if m:
                        # skip the value 0
                        if float(m.group(1)) > 0:
                            iRateSummary = iRateSummary + float(m.group(1))
                            iResultLen = iResultLen + 1
            
            # no this kind of rate in result 
            if iResultLen <= 0:
                continue 

            fAvgRate = float(iRateSummary*1.0/iResultLen)
            index = 0 
            for index in range (0,len(lLATEST_RESULT[session_name])):
                if sSBName in str(lLATEST_RESULT[session_name][index]):
                    # update the rate value with avg 
                    sResult = str(lLATEST_RESULT[session_name][index])
                    sResult = re.sub("'%s': +'[\d|\.]+'" % eachRate, \
                    "'%s': '%s'" % (eachRate,str(fAvgRate)),str(sResult))
                    dTmpResult = eval(sResult)
                    lLATEST_RESULT[session_name][index] = dTmpResult

    except Exception as inst:
        s=sys.exc_info()
        raise StcContinueFailure("Calculate Avg Rate Failed: line:%s,exception:%s" % 
        (s[2].tb_lineno,inst))    

    index = 0
    for index in range (0,len(lLATEST_RESULT[session_name])):
        if sSBName in str(lLATEST_RESULT[session_name][index]):
            dMeasureResult = lLATEST_RESULT[session_name][index]
            break 

    return dMeasureResult

def _hlk_traffic_verify_multi_measure_from_memory(**kwargs):
    """
    Get measure result from memory and verify the parameters that were measured during the tests.

    Args:
        streamblock(Required): Name of the streamblock
            Type: string
            Default: ''

        stream: Stream number of the streamblock
            Type: Integer
            Default: All streams will be verified

        resultType(Required): Type of result measurement to be retrieved.
            Type: string
            Default: ''
            Values: RxStreamSummaryResults,TxStreamResults

        expectResult: Expect Validation Result, default value is "PASS" 
            Type: String
            Default: 'PASS'
            Values: 'PASS','FAIL'

        dExpectedMeasure(Optional, if parameters are given as arguments):
            Dictionary with key value pairs of parameters that are to be verified. This is an optional input as the
            parameters can also be given directly as input to the method.

            If both the dictionary and the parameters are given, then the common parameters would be replaced by the
            one specified as an argument to the method rather than in the dictionary.

            Note->
                Measure parameters can be given with tolerance level as well by appending _tol to the parameter.
                The tolerance level can be in either percentage or an absolute value.

                For example, if bitrate is to be measured then the input parameters can be given as
                bitrate=30, bitrate_tol=5%

                Also, a default tolerance level can also be specified by giving default_tol in percentage.
                This is applied to all the parameters for which there is no tolerance level given. If not mentioned,
                it is taken as 5%

                For checking the inequality conditions, the criteria must be menioned as a list of conditions with key
                as parameter name. This can be seen in the example 3 below.

                Parameters that can be verified depends on the resultType.

                For example, if the resultType is RxStreamSummaryResults, the following parameters are some that can be
                queried from the test data.

                BitCount, BitRate, DroppedFrameCount, FrameCount, L1BitCount, L1BitRate.

                For a complete list of measurable parameters, please refer to the Spirent Test Center documentation which
                can be found in the file path /apps/Spirent/Spirent_TestCenter_4.52/Spirent_TestCenter_Automation_Obj_Ref

    Return:
        N/A

    Example:
        1) ${dMeasureValues} =  Create_Dictionary    BitRate=30    BitRate_tol=13%    FrameCount=99    FrameCount_tol=9%

           hlk_traffic_verify_measure    streamblock=US1    stream=5    type=RxStreamSummaryResults
                                         dExpectedMeasure=${dMeasureValues}

           Note-> The key argument when providing dictionary as input should be 'dExpectedMeasure'

        2) hlk_traffic_verify_measure    streamblock=US1    stream=5    type=RxStreamSummaryResults    L1BitRate=1000000
                                         L1BitRate_tol=10    DroppedFrameCount=0    DroppedFrameCount_tol=0

        3) ${dMeasureValues} =  Create_Dictionary    BitRate=88888    BitRate_tol=5%    FrameCount=99    FrameCount_Tol=9%
                                                     droppedFrameCount=0    DroppedFrameCount_tol=1    l1Bitrate=1000000    L1bitRate_tol=10

           hlk_traffic_verify_measure    streamblock=US1    type=RxStreamSummaryResults    l1Bitrate=[<=1000010,>=99990]
                                droppedFrameCount=[>=0]    FrameCount=[!=0]    dexpectedmeasure=${dMeasureValues}
    """

    assert (kwargs.keys()), "No measure parameters given as input for hlk_traffic_retrieve_measure"
    dKwargs = dict(map(lambda (k,v): (str(k).lower(), v), kwargs.iteritems()))
    llk_log.llk_log_end_proc()
    assert ('type' in dKwargs.keys()), "Input should consist of the type of measure results to be retrieved"

    bResult = "PASS"
    sSBName = ''
    sType = ''
    sStreamIndex = ''
    lStreamIndex = []
    sTotalStreamCnt = ''
    dResultFailed = {}
    lStreamNumbers = []
    lVerifyParamRange = []
    lVerifyParamTol = []
    lRetrieveParam = set()
    sExpectResult = "PASS"

    session_name = "first_stc_session"
    if dKwargs.get('session_name'):
        session_name = dKwargs['session_name']
        del dKwargs['session_name']

    if dKwargs.get('expectresult'):
        sExpectResult = dKwargs['expectresult']
        del dKwargs['expectresult']

    if dKwargs.get('streamblock'):
        sSBName = dKwargs['streamblock']
        del dKwargs['streamblock']

    if dKwargs.get('type'):
        sType = dKwargs['type']
        del dKwargs['type']

    if dKwargs.get('stream'):
        sStreamIndex = dKwargs['stream']
        if sStreamIndex:
            if type(dKwargs['stream']) is list:
                lStreamIndex = dKwargs['stream']
            else:
                lStreamIndex = str(dKwargs['stream']).strip("[](){}").split(',')
        del dKwargs['stream']

    dExpectedMeasure = dKwargs.get('dexpectedmeasure')
    dLlkKwargs = {}
    if dExpectedMeasure:
        del dKwargs['dexpectedmeasure']
        dExpectedMeasure = dict(map(lambda (k,v): (str(k).lower(), v), dExpectedMeasure.iteritems()))

    if dExpectedMeasure and dKwargs.keys():
        for key in dKwargs.keys():
            dExpectedMeasure[str(key)] = dKwargs[key]
        dLlkKwargs = dExpectedMeasure.copy()
    elif dExpectedMeasure:
        dLlkKwargs = dExpectedMeasure.copy()
    elif dKwargs.keys():
        dLlkKwargs = dKwargs.copy()

    lVerifyParam = []
    sDefaultTol = '5%'

    if dLlkKwargs:
        if dLlkKwargs.keys():
            dLlkKwargs = dict(map(lambda (k,v): (str(k).lower(), v), dLlkKwargs.iteritems()))
        else:
            llk_log.llk_log_end_proc()
            raise Exception("No input parameters given to verify!!")

    if 'default_tol' in dLlkKwargs.keys():
        if str(dLlkKwargs['default_tol'])[-1] == '%':
            sDefaultTol = str(dLlkKwargs['default_tol'])
        else:
            llk_log.llk_log_end_proc()
            raise Exception ("The default tolerance should be given in percentage(%) units")
    else:
        llk_log.llk_log_debug("Since default_tol is not mentioned in the input arguments a value of 5% is set!!")

    for key in dLlkKwargs.keys():
        if '<' in str(dLlkKwargs[key]) or '=' in str(dLlkKwargs[key]) or '>' in str(dLlkKwargs[key]) or '!' in str(dLlkKwargs[key]):
            lRange = str(dLlkKwargs[key]).strip('[]{}()').split(';')
            lRetrieveParam.add(str(key))
            for sRange in lRange:
                lVerifyParamRange.append(str(key)+str(sRange).strip(' '))
        elif '_tol' not in str(key):
            lVerifyParam.append(str(key).strip(' '))
            lRetrieveParam.add(str(key))

    for sVerifyParam in lVerifyParam:
        if str(sVerifyParam)+'_tol' in dLlkKwargs.keys():
            lVerifyParamTol.append(dLlkKwargs[str(sVerifyParam)+'_tol'])
        else:
            lVerifyParamTol.append(sDefaultTol)

    if lRetrieveParam :
        #retrieve from memory
        try:
            ddRetrieveMeasure = _avg_rate_in_measure_result(session_name,sSBName)
        except Exception as inst:
            llk_log.llk_log_error("inst:%s" % str(inst))
    else:
        llk_log.llk_log_end_proc()
        raise Exception("No input parameters given to verify!!")

    if not sStreamIndex:
        if str(sSBName)+":0" in ddRetrieveMeasure.keys():
            sTotalStreamCnt = int(ddRetrieveMeasure[str(sSBName)+":0"]['flowcount'])
        else:
            raise Exception("Streamblock not found while retrieving measure")

    if sTotalStreamCnt:
        for i in range(1,sTotalStreamCnt+1):
            lStreamNumbers.append(i)
    elif lStreamIndex:
        lStreamNumbers = lStreamIndex
    else:
        llk_log.llk_log_end_proc()
        raise Exception("No valid stream count found for the streamblock %s " % str(sSBName))

    for sStreamNumber in lStreamNumbers:
        sSBNameStream = str(sSBName)+":"+str(sStreamNumber)
        if sSBNameStream in ddRetrieveMeasure.keys():
            dStreamMeasureValues = ddRetrieveMeasure[sSBNameStream]
            if dStreamMeasureValues:
                cnt = 0
                lResultFailed = []
                for param in lVerifyParam:
                    if dStreamMeasureValues[str(param).lower()]:
                        fExpectedValue = float(dLlkKwargs[str(param)])
                        fObtainedValue = float(dStreamMeasureValues[str(param).lower()])
                        if lVerifyParamTol[cnt][-1] == '%':
                            fToleranceValue = float(str(lVerifyParamTol[cnt]).strip('%'))/100.0
                            fAllowedTolerance = fObtainedValue*fToleranceValue
                        else:
                            fAllowedTolerance = float(str(lVerifyParamTol[cnt]))

                        if (fExpectedValue-fAllowedTolerance) <= fObtainedValue <= (fExpectedValue+fAllowedTolerance):
                            sResult = 'PASS'
                        else:
                            sResult = 'FAIL'
                            lResultFailed.append(param)

                        llk_log.llk_log_info("Measure parameter : %-15s" % str(param) + "\t" + "Expected value : %-15s" % str(fExpectedValue)
                                    + "\t" + "Obtained value : %-15s" % str(fObtainedValue) + "\t" + "Criteria : %-10s"
                                    % ("+/-"+str(lVerifyParamTol[cnt])) + "\t" + "Result : %-5s" % sResult)
                        cnt += 1
                    else:
                        llk_log.llk_log_warn("Parameter %s is not measured for streamblock %s stream %s" % (str(param),str(sSBName),str(sStreamNumber)))

                for sVerifyParamRange in lVerifyParamRange:
                    oParam = re.search(r'([A-Za-z0-9]*)[<=>!]*([0-9]*)',sVerifyParamRange)
                    sParam = oParam.group(1)
                    fExpectedValue = float(oParam.group(2))
                    fObtainedValue = float(dStreamMeasureValues[str(sParam).lower()])
                    sVerifyParamExp = str(sVerifyParamRange).replace(str(sParam),str(fObtainedValue))
                    if eval(sVerifyParamExp):
                        sResult = 'PASS'
                    else:
                        sResult = 'FAIL'
                        lResultFailed.append(sParam)

                    llk_log.llk_log_info("Measure parameter : %-20s" % str(sParam) + "\t" + "Expected value : %-20s" % str(fExpectedValue)
                                + "\t" + "Obtained value : %-20s" % str(fObtainedValue) + "\t" + "Criteria : %-30s"
                                % str(sVerifyParamRange) + "\t" + "Result : %-5s" % sResult)
                if lResultFailed:
                    dResultFailed[sSBNameStream] = lResultFailed[:]
            else:
                llk_log.llk_log_warn("No measure parameters collected for the streamblock %s" % str(sSBName)+":"+str(sStreamNumber))
        else:
            llk_log.llk_log_warn("Streamblock %s with stream %s is not measured!!" % (str(sSBName),str(sStreamNumber)))

    if dResultFailed:
        for sStream in dResultFailed.keys():
            sResultFailed = ",".join([sFailParam for sFailParam in dResultFailed[sStream]])
            llk_log.llk_log_info("Measure for Streamblock %s failed for the parameter(s) : %s" % (sStream,sResultFailed))
        llk_log.llk_log_end_proc()
        #raise StcContinueFailure("Traffic Verification Failed!!")
        bResult = "FAIL"

    llk_log.llk_log_end_proc()

    llk_log.llk_log_info("Expect Result:%s, Real Result:%s" % (sExpectResult.upper(),bResult.upper()))
    if bResult.lower() == sExpectResult.lower() or sExpectResult.upper() == 'NA':
        return lLATEST_RESULT[session_name] 
    else:
        raise StcContinueFailure("Traffic Verification Failed!!")

def stc_create_device_ipv4(lif_side,device_name,**args):

    # convert args to lower case
    args = dict(map(lambda (k,v): (str(k).lower(), str(v)),args.iteritems()))
    llk_log.llk_log_info("Input params args:%s" % args)
 
    # get session_name
    session_name = args.setdefault('session_name','first_stc_session')
    if 'session_name' in args.keys():
        args.pop('session_name')

    # get port object. for example: port1 
    port_id = ''
    if 'client' in lif_side.lower():
        port_id = dCLIENT_PORT[session_name]['PortId'] 
    elif 'network' in lif_side.lower():
        port_id = dNETWORK_PORT[session_name]['PortId']
   
    # create EmulatedDevice
    oDevice = STC_SESSION[session_name].hlk_traffic_config_device(
    name=device_name,portID=port_id)

    oIPv4if = ''
    # config IPV4If
    if 'ipv4if' in args.keys():

        # prepare param
        dArgs = {}
        paramList = args.pop('ipv4if').strip('{}[]()').split(',')
        for item in paramList:
            key,value = item.split('=')
            dArgs[key] = value

        oIPv4if = STC_SESSION[session_name].hlk_traffic_config_device(
        name=device_name,type='ipv4',**dArgs)

    # config EthIIIf
    if 'ethiiif' in args.keys():
        # prepare param
        dArgs = {}
        paramList = args.pop('ethiiif').strip('{}[]()').split(',')
        for item in paramList:
            key,value = item.split('=')
            dArgs[key] = value
 
        # config device
        STC_SESSION[session_name].hlk_traffic_config_device(
        name=device_name,type='ethii',**dArgs)
 
    # config VLANIf
    if 'vlanif' in args.keys():
        # prepare param
        dArgs = {}
        paramList = args.pop('vlanif').strip('{}[]()').split(',')
        for item in paramList:
            key,value = item.split('=')
            dArgs[key] = value

        STC_SESSION[session_name].hlk_traffic_config_device(
        name=device_name,type='vlan',**dArgs)

    # config oIPv4if as toplevelIf
    STC_SESSION[session_name].oLlkStc.llk_stc_device_toplevel_itf(
    oDevice, oIPv4if)

    # config oIPv4if as PrimaryIf 
    STC_SESSION[session_name].oLlkStc.llk_stc_device_primary_itf(
    oDevice, oIPv4if)

    # stacking device
    STC_SESSION[session_name].hlk_traffic_stack_device_protocols(device_name)

    # start device
    STC_SESSION[session_name].hlk_traffic_start_devices(device=STC_SESSION[session_name].dDevices[device_name]['oDevice'])

    logger.debug("dDevices[device_name]:%s" % 
    STC_SESSION[session_name].dDevices[device_name])
    
    return STC_SESSION[session_name].dDevices[device_name]


def stc_operate_device_ipv4(**kwargs) :
    # prepare kwargs
    kwargs = dict(map(lambda (k,v): (str(k).lower(), v), kwargs.iteritems()))

    # get session_name
    session_name = kwargs.setdefault('session_name','first_stc_session')

    # skip key 'session_name'
    if 'session_name' in kwargs.keys():
        kwargs.pop('session_name')

    # invoke hlk_traffic function
    STC_SESSION[session_name].hlk_traffic_operate_device_ipv4(**kwargs)

    return "PASS"

def stc_create_dhcp_server(lif_side,device_name,**args):
    """
    To create device dhcp server

    - *session_name:* The name of stc session, default is "first_stc_session"
    - *device_name: (required)*  The name of device dhcp server
    - *lif_side: (required)* client or network
    - *ethiiif:*  the configuration of EthIIIf Object
    - *vlanif:*  the configuration of vlanIf Object
    - *ipv4if:* the configuration of ipv4If Object. 
    - *serverConfig:* the configuration of serverConfig Object 
    - *serverDefaultPoolConfig:* the configuration of serverDefaultPoolConfig object
    - *option_xxx:* the configuration of dhcpv4ServerMsgOption object

      | Object Name | Reference Link | Comments |
      | ethiiif         | http://kms.spirentcom.com/CSC/pabtech/stc-automation-html/EthIIIf.htm | configure ethiiif object |
      | vlanif          | http://kms.spirentcom.com/CSC/pabtech/stc-automation-html/VlanIf.htm  | configure vlanif object |
      | ipv4if          | http://kms.spirentcom.com/CSC/pabtech/stc-automation-html/Ipv4If.htm | configure ipv4if object |
      | dhcpv4ServerConfig    | http://kms.spirentcom.com/CSC/pabtech/stc-automation-html/Dhcpv4ServerConfig.htm | configure dhcpv4ServerConfig object |
      | Dhcpv4ServerDefaultPoolConfig | http://kms.spirentcom.com/CSC/pabtech/stc-automation-html/Dhcpv4ServerDefaultPoolConfig.htm | configure Dhcpv4ServerDefaultPoolConfig object |
      | option_xxx | Dhcpv4ServerMsgOption | http://kms.spirentcom.com/CSC/pabtech/stc-automation-html/Dhcpv4ServerMsgOption.htm | config Dhcpv4ServerMsgOption object |

    Returns:
        - a object of dhcp server 

    Usage Example:
      | ${oDhcpServer} | stc create dhcp server	client | device_dhcp_server | session_name=first_stc | ipv4if={address=192.85.1.16,gateway=192.85.1.1} | ethiiif={sourceMac=00:00:11:22:33:44} | vlanif={vlan1=100,pri1=0,vlan2=100,pri2=0} | serverConfig=[Active=TRUE,hostName=dhcp_client1,leaseTime=600] | serverdefaultpoolconfig=[StartIpList=192.85.1.5,hostAddrCount=16,addrIncrement=2,Active=TRUE] | option_66=[msgType=OFFER,HexValue=FALSE,optionType=66,payload=135.251.200.1] | option_15=[msgType=ACK,HexValue=TRUE,optionType=15] | 
     
    """
    # convert args to lower case
    args = dict(map(lambda (k,v): (str(k).lower(), str(v)),args.iteritems()))
    llk_log.llk_log_info("Input params args:%s" % args)
 
    # get session_name
    session_name = args.setdefault('session_name','first_stc_session')
    if 'session_name' in args.keys():
        args.pop('session_name')

    # get port object. for example: port1
    port_id = ''
    if 'client' in lif_side.lower():
        port_id = dCLIENT_PORT[session_name]['PortId'] 
    elif 'network' in lif_side.lower():
        port_id = dNETWORK_PORT[session_name]['PortId']
   
    # create EmulatedDevice
    oDevice = STC_SESSION[session_name].hlk_traffic_config_device(
    name=device_name,portID=port_id)

    # config IPV4If
    if 'ipv4if' in args.keys():

        # prepare param
        dArgs = {}
        paramList = args.pop('ipv4if').strip('{}[]()').split(',')
        for item in paramList:
            key,value = item.split('=')
            dArgs[key] = value

        STC_SESSION[session_name].hlk_traffic_config_device(
        name=device_name,type='ipv4',**dArgs)

    # config EthIIIf
    if 'ethiiif' in args.keys():
        # prepare param
        dArgs = {}
        paramList = args.pop('ethiiif').strip('{}[]()').split(',')
        for item in paramList:
            key,value = item.split('=')
            dArgs[key] = value

        # config device
        STC_SESSION[session_name].hlk_traffic_config_device(
        name=device_name,type='ethii',**dArgs)
 
    # config VLANIf
    if 'vlanif' in args.keys():
        # prepare param
        dArgs = {}
        paramList = args.pop('vlanif').strip('{}[]()').split(',')
        for item in paramList:
            key,value = item.split('=')
            dArgs[key] = value

        STC_SESSION[session_name].hlk_traffic_config_device(
        name=device_name,type='vlan',**dArgs)

    # create dhcp server
    args['device'] = device_name 
    oDhcpServer = STC_SESSION[session_name].hlk_traffic_config_dhcp_server(**args)
    """        
    if 'dhcpv4serverconfig' in args.keys():
        sServerConfig = args.pop('dhcpv4serverconfig').strip('{}[]()')

    if 'dhcpv4serverdefaultpoolconfig' in args.keys() :
        sDefaultPoolConfig = args.pop('dhcpv4serverconfig').strip('{}[]()')

        oIEEE1588v2 = STC_SESSION[session_name].hlk_traffic_config_ieee1588v2(
        device_name,clockConfig='['+ sArgs +']') 
    """
    # start dhcp server 
    STC_SESSION[session_name].hlk_traffic_start_devices(device=STC_SESSION[session_name].dDevices[device_name]['oDevice'])

    logger.debug("dDevices[device_name]:%s" % 
    STC_SESSION[session_name].dDevices[device_name])
    
    return STC_SESSION[session_name].dDevices[device_name]['dhcpv4server'] 

def stc_verify_dhcp_server_state(device_name,expect_state,session_name="first_stc_session",check_time=1) :
    """
    To verify serverState of device dhcp server

    - *session_name:* The name of stc session, default is "first_stc_session"
    - *device_name: (required)*  The name of device dhcp server
    - *expect_state: (required)* The expected state of dhcp server

      | Value | Description |
      | NONE  | Servers not started |
      | UP    | Servers are UP |

    """
    llk_log.llk_log_begin_proc()
    result = "FAIL"
    check_time = int(check_time)
    if check_time > 5 :
        interval_time = int(check_time/5)
    else:
        interval_time = 1
    current_time = time.mktime(time.localtime())
    end_time = current_time + check_time

    while current_time <= end_time:
        try:
            result = STC_SESSION[session_name].hlk_traffic_verify_device_status(
            device_name,'dhcpv4ServerConfig',serverState=expect_state)
        except Exception as inst:
            llk_log.llk_log_debug("the state of device dhcp server is not as expected!,exception:%s" % inst)
        if result == "PASS":
            break
        time.sleep(interval_time)
        current_time = time.mktime(time.localtime())
    llk_log.llk_log_end_proc()
    return result

def stc_operate_dhcp_server(**kwargs) :
    # prepare kwargs
    kwargs = dict(map(lambda (k,v): (str(k).lower(), v), kwargs.iteritems()))

    # get session_name
    session_name = kwargs.setdefault('session_name','first_stc_session')

    # skip key 'session_name'
    if 'session_name' in kwargs.keys():
        kwargs.pop('session_name')

    # invoke hlk_traffic function
    STC_SESSION[session_name].hlk_traffic_operate_dhcp_server(**kwargs)

    return "PASS"


def stc_operate_dhcp_client(**kwargs) :
    # prepare kwargs
    kwargs = dict(map(lambda (k,v): (str(k).lower(), v), kwargs.iteritems()))

    # get session_name
    session_name = kwargs.setdefault('session_name','first_stc_session')

    # skip key 'session_name'
    if 'session_name' in kwargs.keys():
        kwargs.pop('session_name')

    # invoke hlk_traffic function
    STC_SESSION[session_name].hlk_traffic_operate_dhcp_client(**kwargs)

    return "PASS"


def stc_create_dhcp_client(lif_side,device_name,**args):
    """
    To create device dhcp client

    - *session_name:* The name of stc session, default is "first_stc_session"
    - *device_name: (required)*  The name of device dhcp client
    - *lif_side: (required)* client or network
    - *ethiiif:*  the configuration of EthIIIf Object
    - *vlanif:*  the configuration of vlanIf Object
    - *ipv4if:* the configuration of ipv4If Object.
    - *CircuitId:* Specify the Circuit ID field in the message sent by the relay agent. Use wildcard characters to make each circuit ID unique:

      | @s - Session index associated with the DHCP client. | 
      | @b - Block (host/router) index. | 
      | @p - Port name. | 
      | @m - MAC address of the DHCP client. | 
      | @x - Custom step setup in (start,count,step,zeropadding,stutter) format. | 
      | start - starting value. | 
      | count - number of values to generate. | 
      | step - amount to increment the start value when the start value must be stepped. | 
      | zeropadding - length the value should be padded to by prepending 0's. | 
      | stutter - number of times a value should be repeated before applying the step. | 
      | @@ - This must be used to include the "@" symbol in the string. | 
      | The EnableCircuitId attribute must be set to TRUE for this attribute to take effect. | 
  
      | Type | string | 
      | Default | circuitId_@p | 
      | Range | 1 - 128 characters | 

    - *ClientRelayAgent:* Not exposed in the GUI.

      | Type | bool | 
      | Default | FALSE | 

      Possible Values: 
      | Value | Description | 
      | TRUE | Enable Client Relay agent. | 
      | FALSE | Disable Client Relay agent. |         

    - *EnableArpServerId:* Enable or disable ARP on the Server ID.
 
      | Type | bool | 
      | Default | FALSE | 

      Possible Values:
      | Value | Description | 
      | TRUE | Enable ARP on the Server ID. | 
      | FALSE | Disable ARP on the Server ID. | 

    - *EnableAutoRetry:* Enable auto retry. Auto retry will retry sessions that fail to initially come up.

      | Type | bool | 
      | Default | FALSE | 

      Possible Values:
      | Value | Description | 
      | TRUE | Enable auto retry. Failed sessions will be retried automatically. | 
      | FALSE | Disable auto retry. | 

    - *EnableCircuitId:* Enable the circuit ID sub-option in the DHCP messages that are sent from the emulated relay agent. The circuit ID sub-option is described in RFC 3046.

      | Type | bool | 
      | Default | FALSE | 

      Possible Values:
      | Value | Description | 
      | TRUE | Include the circuit ID sub-option. | 
      | FALSE | Do not include the circuit ID sub-option. | 

    - *EnableRelayAgent:* Enables or disables the relay agent option specified in RFC 3046.

      | Type | bool |
      | Default | FALSE |

      Possible Values:
      | Value | Description |
      | TRUE | Send all client bind requests as if they were passing through a relay agent. | 
      | FALSE | Do not use a relay agent. | 

    - *EnableRemoteId:* Enable the remote ID sub-option in the DHCP messages that are sent from the emulated relay agent. The remote ID sub-option is described in RFC 3046.

      | Type | bool | 
      | Default | FALSE | 

      Possible Values:
      | Value | Description | 
      | TRUE | Include the remote ID sub-option. |
      | FALSE | Do not include the remote ID sub-option. |

    - *EnableRouterOption:* Enable the router option (option 3) specified in RFC 2132.

      | Type | bool |
      | Default | FALSE |

      Possible Values:
      | Value | Description |
      | TRUE | Enable router option. |
      | FALSE | Disable router option. |

    - *HostName:* Unique hostname of emulated client. Use wildcard characters to make each hostname unique:

      | @s - Session index associated with the DHCP client. | 
      | @b - Block (host/router) index. | 
      | @p - Port name. | 
      | @m - MAC address of the DHCP client. | 
      | @x - Custom step setup in (start,count,step,zeropadding,stutter) format. | 
      | start - starting value. | 
      | count - number of values to generate. | 
      | step - amount to increment the start value when the start value must be stepped. | 
      | zeropadding - length the value should be padded to by prepending 0's. | 
      | stutter - number of times a value should be repeated before applying the step. | 
      | @@ - This must be used to include the "@" symbol in the string. | 

      | Type | string |
      | Default | client_@p-@b-@s | 
      | Range | 1 - 32 characters | 

    - *OptionList:* A space-separated list of Option 55 numbers for the DHCP request messages on each session block. This attribute can have one or more of the following values:

      | 1 - Subnet Mask Option. | 
      | 3 - Router Option. | 
      | 6 - Domain Name Servers Option. | 
      | 15 - Domain Name Option. | 
      | 33 - Static Routes Option. | 
      | 44 - NetBIOS Name Servers Option. | 
      | 46 - NetBIOS Node Type Option. | 
      | 47 - NetBIOS Scope Option. | 
      | 51 - IP Address Lease Time Option. | 
      | 54 - Server Identifier Option. | 
      | 58 - Renewal Time (T1) Option. | 
      | 59 - Rebinding Time (T2) Option. | 
      | Tcl example: -optionList "1 6 15 33 44 51".NOTE: The correct default for this attribute is 1 6 15 33 44. | 
      | Type | u8 | 
      | Default | 1 |

    - *RelayAgentIpv4Addr:* Source IP address of the relay agent message, and the "giaddr" field in the DHCP message.

      | Type | ip | 
      | Default | 0.0.0.0 | 

    - *RelayAgentIpv4AddrMask:* IP Mask to apply to the Relay Local IP address.

      | Type | ip | 
      | Default | 255.255.0.0 | 

    - *RelayAgentIpv4AddrStep:* IP Step to be applied to the Relay Local IP address.

      | Type | ip | 
      | Default | 0.0.0.1 | 

    - *RelayClientMacAddrMask:* MAC mask that will be applied to the Relay Client MAC address.

      | Type: mac |
      | Default: 00-00-00-ff-ff-ff |

    - *RelayClientMacAddrMask:* Starting value for the MAC address.

      | Type: mac | 
      | Default: 00-10-01-00-00-01 |

    - *RelayClientMacAddrStep:* MAC step that will be applied to the Relay Client MAC address.

      | Type: mac |
      | Default: 00-00-00-00-00-01 |

    - *RelayPoolIpv4Addr:* Number of Relay Agent networks.

      | Type: ip | 
      | Default: 0.0.0.0 | 

    - *RelayPoolIpv4AddrStep:* Relay pool IPv4 address step.

      | Type: ip |
      | Default: 0.0.1.0 | 

    - *RelayServerIpv4Addr:* Destination IP address for the relay agent message.

      | Type: ip |
      | Default: 0.0.0.0 |

    - *RelayServerIpv4AddrStep:* IP Step to be applied to the Relay Server IP address.

      | Type: ip |
      | Default: 0.0.0.1 |

    - *RemoteId:* Use wildcard characters to make each remote ID unique:

      | @s - Session index associated with the DHCP client. |
      | @b - Block (host/router) index. | 
      | @p - Port name. | 
      | @m - MAC address of the DHCP client. | 
      | @x - Custom step setup in (start,count,step,zeropadding,stutter) format. | 
      | start - starting value. | 
      | count - number of values to generate. | 
      | step - amount to increment the start value when the start value must be stepped. |
      | zeropadding - length the value should be padded to by prepending 0's. | 
      | stutter - number of times a value should be repeated before applying the step. | 
      | @@ - This must be used to include the "@" symbol in the string. | 
      | The EnableRemoteId attribute must be set to TRUE for this to take effect. | 
 
      | Type | string | 
      | Default | remoteId_@p-@b-@s | 
      | Range | 1 - 128 characters | 

    - *RetryAttempts:* Number of times to retry the session after the initial failure (each retry will use a new transaction ID).

      | Type: u32 | 
      | Default: 4 | 
      | Range: 1 - 4294967295 | 

    - *UseBroadcastFlag:* Enable/disable broadcast bit in DHCP control plane packets.

      | Type: bool |
      | Default: TRUE | 

      Possible Values:
      | Value | Description |
      | TRUE | Enable broadcast bit. | 
      | FALSE | Disable broadcast bit. |

    - *UseClientMacAddrForDataplane:* Use client MAC address for dataplane.

      | Type: bool | 
      | Default: FALSE | 

      Possible Values:  
      | Value | Description | 
      | TRUE | Enable use of the client's MAC address for traffic. | 
      | FALSE | Disable use of the client's MAC address for traffic (uses the Relay Agent's MAC instead). |

    - *UsePartialBlockState:* Flag indicating partial block state as used.

      | Type: bool | 
      | Default: FALSE | 
 
      Possible Values:
      | Value | Description | 
      | TRUE | Use partial block state. | 
      | FALSE | Do not use partial block state. | 


    | Object Name | Reference Link | Comments |
    | ethiiif         | http://kms.spirentcom.com/CSC/pabtech/stc-automation-html/EthIIIf.htm | configure ethiiif object |
    | vlanif          | http://kms.spirentcom.com/CSC/pabtech/stc-automation-html/VlanIf.htm  | configure vlanif object |
    | ipv4if          | http://kms.spirentcom.com/CSC/pabtech/stc-automation-html/Ipv4If.htm | configure ipv4if object |
    | others args    | http://kms.spirentcom.com/CSC/pabtech/stc-automation-html/Dhcpv4BlockConfig.htm | configure dhcpv4BlockConfig object |
    """ 

    # convert args to lower case
    args = dict(map(lambda (k,v): (str(k).lower(), str(v)),args.iteritems()))
    llk_log.llk_log_info("Input params args:%s" % args)

    # get session_name
    session_name = args.setdefault('session_name','first_stc_session')
    if 'session_name' in args.keys():
        args.pop('session_name')

    # get port object. for example: port1
    port_id = ''
    if 'client' in lif_side.lower():
        port_id = dCLIENT_PORT[session_name]['PortId']
    elif 'network' in lif_side.lower():
        port_id = dNETWORK_PORT[session_name]['PortId']
  
    # create EmulatedDevice
    oDevice = STC_SESSION[session_name].hlk_traffic_config_device(
    name=device_name,portID=port_id)
    
    # config IPV4If
    if 'ipv4if' in args.keys():

        # prepare param
        dArgs = {}
        paramList = args.pop('ipv4if').strip('{}[]()').split(',')
        for item in paramList:
            key,value = item.split('=')
            dArgs[key] = value

        STC_SESSION[session_name].hlk_traffic_config_device(
        name=device_name,type='ipv4',**dArgs)
        
    # config EthIIIf
    if 'ethiiif' in args.keys():
        # prepare param
        dArgs = {}
        paramList = args.pop('ethiiif').strip('{}[]()').split(',')
        for item in paramList:
            key,value = item.split('=')
            dArgs[key] = value

        # config device
        STC_SESSION[session_name].hlk_traffic_config_device(
        name=device_name,type='ethii',**dArgs)
        
    # config VLANIf
    if 'vlanif' in args.keys():
        # prepare param
        dArgs = {}
        paramList = args.pop('vlanif').strip('{}[]()').split(',')
        for item in paramList:
            key,value = item.split('=')
            dArgs[key] = value

        STC_SESSION[session_name].hlk_traffic_config_device(
        name=device_name,type='vlan',**dArgs)
        
    # create dhcp server
    args['device'] = device_name
    oDhcpServer = STC_SESSION[session_name].hlk_traffic_config_dhcp_client(**args)
    
    # start dhcp client
    STC_SESSION[session_name].hlk_traffic_start_devices(device=STC_SESSION[session_name].dDevices[device_name]['oDevice'])

    logger.debug("dDevices[device_name]:%s" %
    STC_SESSION[session_name].dDevices[device_name])

    return STC_SESSION[session_name].dDevices[device_name]['oDevice']

def stc_verify_dhcp_client_state(device_name,expect_state,session_name="first_stc_session",check_time=1) :
    """
    To verify blockState of device dhcp client

    - *session_name:* The name of stc session, default is "first_stc_session"
    - *device_name: (required)*  The name of device dhcp client
    - *expect_state: (required)* The expected block state of dhcp client

      | Value | Description |
      | IDLE | Host block is not currently active (no commands have been issued). No DHCP sessions are active. This state is entered if the hosts were manually released. |
      | REQUEST | At least one session associated with the host block is in the process of being established. The hosts are either sending an initial DHCPDISCOVER broadcast message to obtain a DHCP lease, or sending a DHCPREQUEST message requesting an IP address. The messages use 255.255.255.255 as the destination address and 0.0.0.0 as the source address. The remaining hosts may be in either the Bound state or the Idle state, but not the Releasing state. | 
      | RELEASE | At least one host in the host block no longer needs to use the IP address leased to it and is in the process of sending a DHCPRELEASE message to the DHCP server. | 
      | RENEW | At least one host in the block is in the process of renewing the lease by sending another DHCPREQUEST message. Other hosts can be in the Bound or Idle state. NOTE: The Renewing state only refers to leases that were manually renewed (using the Renew DHCP Host command). It does not refer to the automatic renewal that occurs when the T1 timer has expired. | 
      | REBIND | At least one session is rebinding as the lease has expired. | 
      | AUTORENEW | At least one session is in the process of being autorenewed. | 
      | GROUPREQ | Intermediate state within the binding state. | 
      | BOUND | The DHCPDISCOVER, DHCPOFFER, DHCPREQUEST, and DHCPACK messages between the DHCP host and server have resulted in retrieving an IP address for at least one host in the block. No other hosts are requesting or releasing IP addresses, but they may be automatically renewing expired leases. |

    """
    llk_log.llk_log_begin_proc()
    result = "FAIL"
    check_time = int(check_time)
    if check_time > 5 :
        interval_time = int(check_time/5)
    else:
        interval_time = 1
    current_time = time.mktime(time.localtime())
    end_time = current_time + check_time

    while current_time <= end_time:
        try:
            result = STC_SESSION[session_name].hlk_traffic_verify_device_status(
            device_name,'dhcpv4BlockConfig',blockState=expect_state)
        except Exception as inst:
            llk_log.llk_log_debug("the state of device dhcp client is not as expected!")

        if result == "PASS":
            break
        time.sleep(interval_time)
        current_time = time.mktime(time.localtime())
    llk_log.llk_log_end_proc()
    return result

def stc_create_device_ieee1588v2(lif_side,device_name,**args):
    """
    To create device ieee1588v2 

    - *session_name:* The name of stc session, default is "first_stc_session"
    - *device_name: (required)*  The name of device ieee1588v2
    - *lif_side: (required)* client or network
    - *ethiiif:*  the configuration of EthIIIf Object
    - *vlanif:*  the configuration of vlanIf Object
    - *ipv4if:* the configuration of ipv4If Object. 
    - *ieee1588v2clockconfig* the configuration of ieee1588v2clockconfig Object 
      | Object Name | Reference Link | Comments |
      | ethiiif         | http://kms.spirentcom.com/CSC/pabtech/stc-automation-html/EthIIIf.htm | configure ethiiif object |
      | vlanif          | http://kms.spirentcom.com/CSC/pabtech/stc-automation-html/VlanIf.htm  | configure vlanif object |
      | ipv4if           | http://kms.spirentcom.com/CSC/pabtech/stc-automation-html/Ipv4If.htm | configure ipv4if object |
      | ieee1588v2clockconfig | http://kms.spirentcom.com/CSC/pabtech/stc-automation-html/Ieee1588v2ClockConfig.htm | configure ieee1588v2clockconfig object |
 
    Returns:
        - *oIEEE1588v2:* the object of ieee1588v2clockconfig 

    Usage Example:
    | ${oIEEE1588v2_master} | stc create device ieee1588v2 | network | device_ieee1588v2_master | session_name=first_stc | ipv4if={address=192.168.1.10,gateway=192.168.1.1} | ethiiif={sourceMac=00:00:11:22:33:44} | vlanif={vlan1=100,pri1=0,vlan2=100,pri2=0} | ieee1588v2clockconfig=[LogSyncInterval=-4,LogMinDelayRequestInterval=-8,StepMode=ONE_STEP,TimeSrc=INTERNAL_OSCILLATOR,ClockClass=6,EnableUnicastNegotiation=TRUE] | 
    | ${oIEEE1588v2_slave} | stc create device ieee1588v2 | client | device_ieee1588v2_client | session_name=first_stc | ipv4if={address=192.168.1.5} | ethiiif={sourceMac=00:00:11:22:33:55} | vlanif={vlan1=100,pri1=0,vlan2=100,pri2=0} | ieee1588v2clockconfig=[slaveOnly=True] |
     
    """
    # convert args to lower case
    args = dict(map(lambda (k,v): (str(k).lower(), str(v)),args.iteritems()))
    llk_log.llk_log_info("Input params args:%s" % args)
 
    # get session_name
    session_name = args.setdefault('session_name','first_stc_session')

    # get port object. for example: port1
    port_id = ''
    if 'client' in lif_side.lower():
        port_id = dCLIENT_PORT[session_name]['PortId'] 
    elif 'network' in lif_side.lower():
        port_id = dNETWORK_PORT[session_name]['PortId']
   
    # create EmulatedDevice
    oDevice = STC_SESSION[session_name].hlk_traffic_config_device(
    name=device_name,portID=port_id)

    # config IPV4If
    if 'ipv4if' in args.keys():

        # prepare param
        dArgs = {}
        paramList = args.pop('ipv4if').strip('{}[]()').split(',')
        for item in paramList:
            key,value = item.split('=')
            dArgs[key] = value

        STC_SESSION[session_name].hlk_traffic_config_device(
        name=device_name,type='ipv4',**dArgs)

    # config EthIIIf
    if 'ethiiif' in args.keys():
        # prepare param
        dArgs = {}
        paramList = args.pop('ethiiif').strip('{}[]()').split(',')
        for item in paramList:
            key,value = item.split('=')
            dArgs[key] = value

        # config device
        STC_SESSION[session_name].hlk_traffic_config_device(
        name=device_name,type='ethii',**dArgs)
 
    # config VLANIf
    if 'vlanif' in args.keys():
        # prepare param
        dArgs = {}
        paramList = args.pop('vlanif').strip('{}[]()').split(',')
        for item in paramList:
            key,value = item.split('=')
            dArgs[key] = value

        STC_SESSION[session_name].hlk_traffic_config_device(
        name=device_name,type='vlan',**dArgs)

    # create Ieee1588v2ClockConfig
    if 'ieee1588v2clockconfig' in args.keys():
        # prepare param
        sArgs = args.pop('ieee1588v2clockconfig').strip('{}[]()')

        oIEEE1588v2 = STC_SESSION[session_name].hlk_traffic_config_ieee1588v2(
        device_name,clockConfig='['+ sArgs +']') 

    # start ieee1588v2 device
    STC_SESSION[session_name].hlk_traffic_start_devices(device=oIEEE1588v2)

    return oIEEE1588v2

def stc_delete_device(device_name,session_name="first_stc_session"):
    """
    To delete device ieee1588v2

    - *session_name:* The name of stc session, default is "first_stc_session"
    - *device_name: (required)*  The name of device ieee1588v2
    """
    if device_name in STC_SESSION[session_name].dDevices.keys():
        sDevice = STC_SESSION[session_name].dDevices[device_name]['oDevice']
        STC_SESSION[session_name].oLlkStc.llk_stc_delete(sDevice)
        STC_SESSION[session_name].oLlkStc.llk_stc_send_command("stc::apply")
        STC_SESSION[session_name].dDevices.pop(device_name)
    else:
        raise AssertionError("the device %s does not exist!" % device_name)

    return "PASS"

def stc_modify_device_ieee1588v2(device_name,**args):
    """
    To modify device ieee1588v2 

    - *session_name:* The name of stc session, default is "first_stc_session"
    - *device_name: (required)*  The name of device ieee1588v2
    - *lif_side: (required)* client or network
    - *ethiiif:*  the configuration of EthIIIf Object
    - *vlanif:*  the configuration of vlanIf Object
    - *ipv4if:* the configuration of ipv4If Object. 
    - *ieee1588v2clockconfig* the configuration of ieee1588v2clockconfig Object 
      | Object Name | Reference Link | Comments |
      | ethiiif         | http://kms.spirentcom.com/CSC/pabtech/stc-automation-html/EthIIIf.htm | configure ethiiif object |
      | vlanif          | http://kms.spirentcom.com/CSC/pabtech/stc-automation-html/VlanIf.htm  | configure vlanif object |
      | ipv4if           | http://kms.spirentcom.com/CSC/pabtech/stc-automation-html/Ipv4If.htm | configure ipv4if object |
      | ieee1588v2clockconfig | http://kms.spirentcom.com/CSC/pabtech/stc-automation-html/Ieee1588v2ClockConfig.htm | configure ieee1588v2clockconfig object |
 
    Returns:
        - *oIEEE1588v2:* the object of ieee1588v2clockconfig 

    Usage Example:
    | ${oIEEE1588v2_master} | stc modify device ieee1588v2 | network | device_ieee1588v2_master | session_name=first_stc | ipv4if={address=192.168.1.10,gateway=192.168.1.1} | ethiiif={sourceMac=00:00:11:22:33:44} | vlanif={vlan1=100,pri1=0,vlan2=100,pri2=0} | ieee1588v2clockconfig=[LogSyncInterval=-4,LogMinDelayRequestInterval=-8,StepMode=ONE_STEP,TimeSrc=INTERNAL_OSCILLATOR,ClockClass=6,EnableUnicastNegotiation=TRUE] | 
    | ${oIEEE1588v2_slave} | stc modify device ieee1588v2 | client | device_ieee1588v2_client | session_name=first_stc | ipv4if={address=192.168.1.5} | ethiiif={sourceMac=00:00:11:22:33:55} | vlanif={vlan1=100,pri1=0,vlan2=100,pri2=0} | ieee1588v2clockconfig=[slaveOnly=True] |      
    """

    # convert args to lower case
    args = dict(map(lambda (k,v): (str(k).lower(), str(v)),args.iteritems()))
    llk_log.llk_log_info("Input params args:%s" % args)

    # get session_name
    session_name = args.setdefault('session_name','first_stc_session')

    # config IPV4If
    if 'ipv4if' in args.keys():

        # prepare param
        dArgs = {}
        paramList = args.pop('ipv4if').strip('{}[]()').split(',')
        for item in paramList:
            key,value = item.split('=')
            dArgs[key] = value

        STC_SESSION[session_name].hlk_traffic_config_device(
        name=device_name,type='ipv4',**dArgs)

    # config EthIIIf
    if 'ethiiif' in args.keys():
        # prepare param
        dArgs = {}
        paramList = args.pop('ethiiif').strip('{}[]()').split(',')
        for item in paramList:
            key,value = item.split('=')
            dArgs[key] = value

        # config device
        STC_SESSION[session_name].hlk_traffic_config_device(
        name=device_name,type='ethii',**dArgs)

   # config VLANIf
    if 'vlanif' in args.keys():
        # prepare param
        dArgs = {}
        paramList = args.pop('vlanif').strip('{}[]()').split(',')
        for item in paramList:
            key,value = item.split('=')
            dArgs[key] = value

        STC_SESSION[session_name].hlk_traffic_config_device(
        name=device_name,type='vlan',**dArgs)

    # modify Ieee1588v2ClockConfig
    if 'ieee1588v2clockconfig' in args.keys():

        if 'oclock' in args.keys():
           oClock = args['oclock']
        else:
           raise AssertionError("the param 'oClock' is required!")

        # prepare param
        sArgs = args.pop('ieee1588v2clockconfig').strip('{}[]()')
        
        oIEEE1588v2 = STC_SESSION[session_name].hlk_traffic_config_ieee1588v2(
        device_name,oClock=oClock,clockConfig='['+ sArgs +']')

    # apply the modification
    STC_SESSION[session_name].oLlkStc.llk_stc_send_command("stc::apply")

    return "PASS"

def stc_retrieve_device_ieee1588v2(device_name,clock_state,session_name="first_stc_session",check_time=1):
    """
    To retrieve cloclstate of device ieee1588v2

    - *session_name:* The name of stc session, default is "first_stc_session"
    - *device_name: (required)*  The name of device ieee1588v2
    - *clock_state: (required)* The expected clock state
      | Value | Description | 
      | IEEE1588_STATE_NONE | No state detected. | 
      | IEEE1588_STATE_INITIALIZING | Initializing data sets, hardware, and communication facilities. | 
      | IEEE1588_STATE_FAULTY | Multiple Pdelay_Resp messages were received. | 
      | IEEE1588_STATE_DISABLED | Does not implement the delay mechanism. | 
      | IEEE1588_STATE_LISTENING | Waiting for the announceReceiptTimeout to expire or to receive an Announce message from a master. | 
      | IEEE1588_STATE_PRE_MASTER | No Announce message was received within the required time. Allowing changes to propagate from points in the system between the local clock and possible masters visible from the port before assuming the Master state. | 
      | IEEE1588_STATE_MASTER | Operating as the master clock. | 
      | IEEE1588_STATE_PASSIVE | Not the master on the path. Not synchronizing to a master. | 
      | IEEE1588_STATE_UNCALIBRATED | One or more master ports have been detected in the domain. | 
      | IEEE1588_STATE_SLAVE | Synchronizing to the selected master port. |  
    """
    result = "FAIL"
    check_time = int(check_time)
    if check_time > 5 :
        interval_time = int(check_time/5)
    else:
        interval_time = 1
    current_time = time.mktime(time.localtime())
    end_time = current_time + check_time

    while current_time <= end_time:
        try:
            result = STC_SESSION[session_name].hlk_traffic_verify_device_status(
            device_name,'ieee1588v2clockconfig',clockstate=clock_state)
        except Exception as inst:
            llk_log.llk_log_debug("the state of device ieee1588v2 is not as expected!")

        if result == "PASS":
            break
        time.sleep(interval_time)
        current_time = time.mktime(time.localtime())
    llk_log.llk_log_end_proc()

    if result == "FAIL":
        raise AssertionError("Fail to retrieve the state of device ieee1588v2: %s" % clock_state)
    else : 
        return "PASS"


def stc_create_igmp_host(lif_side, device_name, **args):
    """
    To create device igmp host
    """
    # convert args to lower case
    args = dict(map(lambda (k, v): (str(k).lower(), str(v)), args.iteritems()))
    ##print ('args:', args)
    llk_log.llk_log_info("Input params args:%s" % args)

    # get session_name
    session_name = args.setdefault('session_name', 'first_stc_session')
    if 'session_name' in args.keys():
        args.pop('session_name')

    # get port object. for example: port1
    port_id = ''
    if 'client' in lif_side.lower():
        port_id = dCLIENT_PORT[session_name]['PortId']
    elif 'network' in lif_side.lower():
        port_id = dNETWORK_PORT[session_name]['PortId']

    # create EmulatedDevice
    oDevice = STC_SESSION[session_name].hlk_traffic_config_device(
        name=device_name, portID=port_id)
    ##print ('oDevice:', oDevice)

    # config IPV4If
    if 'ipv4if' in args.keys():

        # prepare param
        dArgs = {}
        paramList = args.pop('ipv4if').strip('{}[]()').split(',')
        for item in paramList:
            key, value = item.split('=')
            dArgs[key] = value

        STC_SESSION[session_name].hlk_traffic_config_device(
            name=device_name, type='ipv4', **dArgs)

    # config EthIIIf
    if 'ethiiif' in args.keys():
        # prepare param
        dArgs = {}
        paramList = args.pop('ethiiif').strip('{}[]()').split(',')
        for item in paramList:
            key, value = item.split('=')
            dArgs[key] = value

        # config device
        STC_SESSION[session_name].hlk_traffic_config_device(
            name=device_name, type='ethii', **dArgs)
     
    # config VLANIf
    if 'vlanif' in args.keys():
        # prepare param
        dArgs = {}
        paramList = args.pop('vlanif').strip('{}[]()').split(',')
        for item in paramList:
            key,value = item.split('=')
            dArgs[key] = value

        STC_SESSION[session_name].hlk_traffic_config_device(
        name=device_name,type='vlan',**dArgs)

   

    #if args.has_key('groupmembership'): 
    #   args['ipv4groups'] = args['groupmembership']

    ##print ('args2:', args)
    # create igmp host
    args['device'] = device_name
    logger.debug ('device_name:', device_name)
    oIgmpHost = STC_SESSION[session_name].hlk_traffic_config_igmp_host(**args)
    """
    """
    # start igmp host
    STC_SESSION[session_name].hlk_traffic_start_devices(
        device=STC_SESSION[session_name].dDevices[device_name]['oDevice'])

    logger.debug("dDevices[device_name]:%s" %
                 STC_SESSION[session_name].dDevices[device_name])

    return STC_SESSION[session_name].dDevices[device_name]['igmphost']


def stc_verify_igmp_host_state(device_name,expect_state,session_name="first_stc_session",check_time=1) :
    """
    To verify hostState of device igmp host
    -BlockState NON_MEMBER
    -BlockState MEMBER
    """
    result = "FAIL"
    check_time = int(check_time)
    if check_time > 5 :
        interval_time = int(check_time/5)
    else:
        interval_time = 1
    current_time = time.mktime(time.localtime())
    end_time = current_time + check_time

    while current_time <= end_time:
        try:
            result = STC_SESSION[session_name].hlk_traffic_verify_device_status(
            device_name,'igmphostconfig',BlockState=expect_state)
        except Exception as inst:
            llk_log.llk_log_debug("the state of igmp host is not as expected!,exception:%s" % inst)

        if result == "PASS":
            break
        time.sleep(interval_time)
        current_time = time.mktime(time.localtime())
    llk_log.llk_log_end_proc()

    if result == "FAIL":
        raise AssertionError("Fail to retrieve igmp host state: %s" % expect_state)
    else :
        return "PASS"


def stc_operate_igmp_host(**kwargs) :
    # prepare kwargs
    kwargs = dict(map(lambda (k,v): (str(k).lower(), v), kwargs.iteritems()))

    # get session_name
    session_name = kwargs.setdefault('session_name','first_stc_session')

    # skip key 'session_name'
    if 'session_name' in kwargs.keys():
        kwargs.pop('session_name')

    # invoke hlk_traffic function
    STC_SESSION[session_name].hlk_traffic_operate_igmp_host(**kwargs)

    return "PASS"

def stc_create_pppoe_client(lif_side, device_name, **args):
    """
    To create device pppoe client
    """
    # convert args to lower case
    args = dict(map(lambda (k, v): (str(k).lower(), str(v)), args.iteritems()))
    print ('args:', args)
    llk_log.llk_log_info("Input params args:%s" % args)

    # get session_name
    session_name = args.setdefault('session_name', 'first_stc_session')
    if 'session_name' in args.keys():
        args.pop('session_name')

    # get port object. for example: port1
    port_id = ''
    if 'client' in lif_side.lower():
        port_id = dCLIENT_PORT[session_name]['PortId']
    elif 'network' in lif_side.lower():
        port_id = dNETWORK_PORT[session_name]['PortId']

    # create EmulatedDevice
    oDevice = STC_SESSION[session_name].hlk_traffic_config_device(
        name=device_name, portID=port_id)
    print ('oDevice:', oDevice)

    # config IPV4If
    if 'ipv4if' in args.keys():

        # prepare param
        dArgs = {}
        paramList = args.pop('ipv4if').strip('{}[]()').split(',')
        for item in paramList:
            key, value = item.split('=')
            dArgs[key] = value

        STC_SESSION[session_name].hlk_traffic_config_device(
            name=device_name, type='ipv4', **dArgs)

    # config EthIIIf
    if 'ethiiif' in args.keys():
        # prepare param
        dArgs = {}
        paramList = args.pop('ethiiif').strip('{}[]()').split(',')
        for item in paramList:
            key, value = item.split('=')
            dArgs[key] = value

        # config device
        STC_SESSION[session_name].hlk_traffic_config_device(
            name=device_name, type='ethii', **dArgs)
     
    # config VLANIf
    if 'vlanif' in args.keys():
        # prepare param
        dArgs = {}
        paramList = args.pop('vlanif').strip('{}[]()').split(',')
        for item in paramList:
            key,value = item.split('=')
            dArgs[key] = value

        STC_SESSION[session_name].hlk_traffic_config_device(
        name=device_name,type='vlan',**dArgs)

    # config pppIf
    if 'pppif' in args.keys():
        # prepare param
        dArgs = {}
        paramList = args.pop('pppif').strip('{}[]()').split(',')
        for item in paramList:
            key, value = item.split('=')
            dArgs[key] = value

        # config device
        STC_SESSION[session_name].hlk_traffic_config_device(
            name=device_name, type='ppp', **dArgs)

    # config pppoeIf
    if 'pppoeif' in args.keys():
        # prepare param
        dArgs = {}
        paramList = args.pop('pppoeif').strip('{}[]()').split(',')
        for item in paramList:
            key, value = item.split('=')
            dArgs[key] = value

        # config device
        STC_SESSION[session_name].hlk_traffic_config_device(
            name=device_name, type='pppoe', **dArgs)


    print ('args2:', args)
    # create pppoe client
    args['device'] = device_name
    print ('device_name:', device_name)
    oPppoeClient = STC_SESSION[session_name].hlk_traffic_config_pppoe_client(**args)
    print ('oPppoeClient : ', oPppoeClient)
    """
    """
    # start pppoe client
    STC_SESSION[session_name].hlk_traffic_start_devices(
        device=STC_SESSION[session_name].dDevices[device_name]['oDevice'])

    logger.debug("dDevices[device_name]:%s" %
                 STC_SESSION[session_name].dDevices[device_name])

    return STC_SESSION[session_name].dDevices[device_name]['pppoeclient']


def stc_create_pppoe_server(lif_side, device_name, **args):
    """
    To create device pppoe server
    """
    # convert args to lower case
    args = dict(map(lambda (k, v): (str(k).lower(), str(v)), args.iteritems()))
    print ('args:', args)
    llk_log.llk_log_info("Input params args:%s" % args)

    # get session_name
    session_name = args.setdefault('session_name', 'first_stc_session')
    if 'session_name' in args.keys():
        args.pop('session_name')

    # get port object. for example: port1
    port_id = ''
    if 'client' in lif_side.lower():
        port_id = dCLIENT_PORT[session_name]['PortId']
    elif 'network' in lif_side.lower():
        port_id = dNETWORK_PORT[session_name]['PortId']

    # create EmulatedDevice
    oDevice = STC_SESSION[session_name].hlk_traffic_config_device(
        name=device_name, portID=port_id)
    print ('oDevice:', oDevice)

    # config IPV4If
    if 'ipv4if' in args.keys():

        # prepare param
        dArgs = {}
        paramList = args.pop('ipv4if').strip('{}[]()').split(',')
        for item in paramList:
            key, value = item.split('=')
            dArgs[key] = value

        STC_SESSION[session_name].hlk_traffic_config_device(
            name=device_name, type='ipv4', **dArgs)

    # config EthIIIf
    if 'ethiiif' in args.keys():
        # prepare param
        dArgs = {}
        paramList = args.pop('ethiiif').strip('{}[]()').split(',')
        for item in paramList:
            key, value = item.split('=')
            dArgs[key] = value

        # config device
        STC_SESSION[session_name].hlk_traffic_config_device(
            name=device_name, type='ethii', **dArgs)
     
    # config VLANIf
    if 'vlanif' in args.keys():
        # prepare param
        dArgs = {}
        paramList = args.pop('vlanif').strip('{}[]()').split(',')
        for item in paramList:
            key,value = item.split('=')
            dArgs[key] = value

        STC_SESSION[session_name].hlk_traffic_config_device(
        name=device_name,type='vlan',**dArgs)

    # config pppIf
    if 'pppif' in args.keys():
        # prepare param
        dArgs = {}
        paramList = args.pop('pppif').strip('{}[]()').split(',')
        for item in paramList:
            key, value = item.split('=')
            dArgs[key] = value

        # config device
        STC_SESSION[session_name].hlk_traffic_config_device(
            name=device_name, type='ppp', **dArgs)

    # config pppoeIf
    if 'pppoeif' in args.keys():
        # prepare param
        dArgs = {}
        paramList = args.pop('pppoeif').strip('{}[]()').split(',')
        for item in paramList:
            key, value = item.split('=')
            dArgs[key] = value

        # config device
        STC_SESSION[session_name].hlk_traffic_config_device(
            name=device_name, type='pppoe', **dArgs)

    print ('args2:', args)
    # create pppoe server
    args['device'] = device_name
    print ('device_name:', device_name)
    oPppoeServer = STC_SESSION[session_name].hlk_traffic_config_pppoe_server(**args)
    print ('oPppoeServer : ', oPppoeServer)

    # start pppoe server
    STC_SESSION[session_name].hlk_traffic_start_devices(
        device=STC_SESSION[session_name].dDevices[device_name]['oDevice'])

    logger.debug("dDevices[device_name]:%s" %
                 STC_SESSION[session_name].dDevices[device_name])

    return STC_SESSION[session_name].dDevices[device_name]['pppoeserver']


def stc_verify_pppoe_server_state(device_name,expect_state,session_name="first_stc_session",check_time=1) :
    """
    To verify hostState of device pppoe server
    -BlockState IDLE
    -BlockState CONNECTED
    """
    result = "FAIL"
    check_time = int(check_time)
    if check_time > 5 :
        interval_time = int(check_time/5)
    else:
        interval_time = 1
    current_time = time.mktime(time.localtime())
    end_time = current_time + check_time

    while current_time <= end_time:
        try:
            result = STC_SESSION[session_name].hlk_traffic_verify_device_status(
            device_name,'pppoeserverblockconfig',BlockState=expect_state)
        except Exception as inst:
            llk_log.llk_log_debug("the state of pppoe server is not as expected!,exception:%s" % inst)
        if result == "PASS":
            break
        time.sleep(interval_time)
        current_time = time.mktime(time.localtime())

    if result == "FAIL":
        raise AssertionError("Fail to retrieve pppoe server state: %s" % expect_state)
    llk_log.llk_log_end_proc()
    return result


def stc_operate_pppoe_server(**kwargs) :
    # prepare kwargs
    kwargs = dict(map(lambda (k,v): (str(k).lower(), v), kwargs.iteritems()))

    # get session_name
    session_name = kwargs.setdefault('session_name','first_stc_session')

    # skip key 'session_name'
    if 'session_name' in kwargs.keys():
        kwargs.pop('session_name')

    # invoke hlk_traffic function
    STC_SESSION[session_name].hlk_traffic_operate_pppoe_server(**kwargs)

    return "PASS"


def stc_verify_pppoe_client_state(device_name,expect_state,session_name="first_stc_session",check_time=1) :
    """
    To verify hostState of device pppoe client
    -BlockState IDLE
    -BlockState CONNECTED
    """
    result = "FAIL"
    check_time = int(check_time)
    if check_time > 5 :
        interval_time = int(check_time/5)
    else:
        interval_time = 1
    current_time = time.mktime(time.localtime())
    end_time = current_time + check_time

    while current_time <= end_time:
        try:
            result = STC_SESSION[session_name].hlk_traffic_verify_device_status(
            device_name,'pppoeclientblockconfig',BlockState=expect_state)
        except Exception as inst:
            llk_log.llk_log_debug("the state of pppoe client is not as expected!,exception:%s" % inst)

        if result == "PASS" :
            break
        time.sleep(interval_time)
        current_time = time.mktime(time.localtime())

    if result == "FAIL":
        raise AssertionError("Fail to retrieve pppoe client state: %s" %  expect_state)
    else :
        return "PASS"



def stc_operate_pppoe_client(**kwargs) :
    # prepare kwargs
    kwargs = dict(map(lambda (k,v): (str(k).lower(), v), kwargs.iteritems()))

    # get session_name
    session_name = kwargs.setdefault('session_name','first_stc_session')

    # skip key 'session_name'
    if 'session_name' in kwargs.keys():
        kwargs.pop('session_name')

    # invoke hlk_traffic function
    STC_SESSION[session_name].hlk_traffic_operate_pppoe_client(**kwargs)

    return "PASS"


def stc_create_dhcpv6_server(lif_side, device_name, **args):
    """
    To create device dhcpv6 server
    """
    # convert args to lower case
    args = dict(map(lambda (k, v): (str(k).lower(), str(v)), args.iteritems()))
    #print ('args:', args)
    llk_log.llk_log_info("Input params args:%s" % args)

    # get session_name
    session_name = args.setdefault('session_name', 'first_stc_session')
    if 'session_name' in args.keys():
        args.pop('session_name')

    # get port object. for example: port1
    port_id = ''
    if 'client' in lif_side.lower():
        port_id = dCLIENT_PORT[session_name]['PortId']
    elif 'network' in lif_side.lower():
        port_id = dNETWORK_PORT[session_name]['PortId']

    # create EmulatedDevice
    oDevice = STC_SESSION[session_name].hlk_traffic_config_device(
        name=device_name, portID=port_id)
    #print ('oDevice:', oDevice)

    # config IPV6If
    if 'ipv6if' in args.keys():

        # prepare param
        dArgs = {}
        paramList = args.pop('ipv6if').strip('{}[]()').split(',')
        for item in paramList:
            key, value = item.split('=')
            dArgs[key] = value

        STC_SESSION[session_name].hlk_traffic_config_device(
            name=device_name, type='ipv6', **dArgs)

    # config ipv6llif
    if 'ipv6llif' in args.keys():

        # prepare param
        dArgs = {}
        paramList = args.pop('ipv6llif').strip('{}[]()').split(',')
        for item in paramList:
            key, value = item.split('=')
            dArgs[key] = value

        STC_SESSION[session_name].hlk_traffic_config_deviceb(
            name=device_name, type='ipv6ll', **dArgs)

    # config EthIIIf
    if 'ethiiif' in args.keys():
        # prepare param
        dArgs = {}
        paramList = args.pop('ethiiif').strip('{}[]()').split(',')
        for item in paramList:
            key, value = item.split('=')
            dArgs[key] = value

        # config device
        STC_SESSION[session_name].hlk_traffic_config_device(
            name=device_name, type='ethii', **dArgs)
     
    # config VLANIf
    if 'vlanif' in args.keys():
        # prepare param
        dArgs = {}
        paramList = args.pop('vlanif').strip('{}[]()').split(',')
        for item in paramList:
            key,value = item.split('=')
            dArgs[key] = value

        STC_SESSION[session_name].hlk_traffic_config_device(
        name=device_name,type='vlan',**dArgs)
   
    #print ('args2:', args)
    # create dhcpv6 server
    args['device'] = device_name
    #print ('device_name:', device_name)
    oDhcpv6Server = STC_SESSION[session_name].hlk_traffic_config_dhcpv6_server(**args)
    #print ('oDhcpv6Server : ', oDhcpv6Server)
    
    # start dhcpv6 server
    STC_SESSION[session_name].hlk_traffic_start_devices(
        device=STC_SESSION[session_name].dDevices[device_name]['oDevice'])

    logger.debug("dDevices[device_name]:%s" %
                STC_SESSION[session_name].dDevices[device_name])

    return STC_SESSION[session_name].dDevices[device_name]['dhcpv6server']


def stc_create_dhcpv6_client(lif_side, device_name, **args):
    """
    To create device dhcpv6 client
    """
    # convert args to lower case
    args = dict(map(lambda (k, v): (str(k).lower(), str(v)), args.iteritems()))
    #print ('args:', args)
    llk_log.llk_log_info("Input params args:%s" % args)

    # get session_name
    session_name = args.setdefault('session_name', 'first_stc_session')
    if 'session_name' in args.keys():
        args.pop('session_name')

    # get port object. for example: port1
    port_id = ''
    if 'client' in lif_side.lower():
        port_id = dCLIENT_PORT[session_name]['PortId']
    elif 'network' in lif_side.lower():
        port_id = dNETWORK_PORT[session_name]['PortId']

    # create EmulatedDevice
    oDevice = STC_SESSION[session_name].hlk_traffic_config_device(
        name=device_name, portID=port_id)
    #print ('oDevice:', oDevice)

    # config IPV6If
    if 'ipv6if' in args.keys():

        # prepare param
        dArgs = {}
        paramList = args.pop('ipv6if').strip('{}[]()').split(',')
        for item in paramList:
            key, value = item.split('=')
            dArgs[key] = value

        STC_SESSION[session_name].hlk_traffic_config_device(
            name=device_name, type='ipv6', **dArgs)

    # config ipv6llif
    if 'ipv6llif' in args.keys():

        # prepare param
        dArgs = {}
        paramList = args.pop('ipv6llif').strip('{}[]()').split(',')
        for item in paramList:
            key, value = item.split('=')
            dArgs[key] = value

        STC_SESSION[session_name].hlk_traffic_config_deviceb(
            name=device_name, type='ipv6ll', **dArgs)

    # config EthIIIf
    if 'ethiiif' in args.keys():
        # prepare param
        dArgs = {}
        paramList = args.pop('ethiiif').strip('{}[]()').split(',')
        for item in paramList:
            key, value = item.split('=')
            dArgs[key] = value

        # config device
        STC_SESSION[session_name].hlk_traffic_config_device(
            name=device_name, type='ethii', **dArgs)
     
    # config VLANIf
    if 'vlanif' in args.keys():
        # prepare param
        dArgs = {}
        paramList = args.pop('vlanif').strip('{}[]()').split(',')
        for item in paramList:
            key,value = item.split('=')
            dArgs[key] = value

        STC_SESSION[session_name].hlk_traffic_config_device(
        name=device_name,type='vlan',**dArgs)
   
    #print ('args2:', args)
    # create dhcpv6 server
    args['device'] = device_name
    #print ('device_name:', device_name)
    oDhcpv6Client = STC_SESSION[session_name].hlk_traffic_config_dhcpv6_client(**args)
    #print ('oDhcpv6Client : ', oDhcpv6Client)
    
    # start dhcpv6 client
    STC_SESSION[session_name].hlk_traffic_start_devices(
        device=STC_SESSION[session_name].dDevices[device_name]['oDevice'])

    logger.debug("dDevices[device_name]:%s" %
                STC_SESSION[session_name].dDevices[device_name])

    return STC_SESSION[session_name].dDevices[device_name]['dhcpv6client']


def stc_verify_dhcpv6_server_state(device_name,expect_state,session_name="first_stc_session",check_time=1) :
    """
    To verify hostState of device dhcpv6 server
    none,up
    """
    result = "FAIL"
    check_time = int(check_time)
    if check_time > 5 :
        interval_time = int(check_time/5)
    else:
        interval_time = 1
    current_time = time.mktime(time.localtime())
    end_time = current_time + check_time

    while current_time <= end_time:
        try:
            result = STC_SESSION[session_name].hlk_traffic_verify_device_status(
            device_name,'Dhcpv6ServerConfig',ServerState=expect_state)
        except Exception as inst:
            llk_log.llk_log_debug("the state of dhcpv6 server is not as expected!,exception:%s" % inst)

        if result == "PASS" :
            break
        time.sleep(interval_time)
        current_time = time.mktime(time.localtime())

    if result == "FAIL":
        raise AssertionError("Fail to retrieve dhcpv6 server state: %s" %
        expect_state)
    else :
        return "PASS"

def stc_operate_dhcpv6_server(**kwargs) :
    # prepare kwargs
    kwargs = dict(map(lambda (k,v): (str(k).lower(), v), kwargs.iteritems()))

    # get session_name
    session_name = kwargs.setdefault('session_name','first_stc_session')

    # skip key 'session_name'
    if 'session_name' in kwargs.keys():
        kwargs.pop('session_name')

    # invoke hlk_traffic function
    STC_SESSION[session_name].hlk_traffic_operate_dhcpv6_server(**kwargs)

    return "PASS"


def stc_verify_dhcpv6_client_state(device_name,expect_state,session_name="first_stc_session",check_time=1) :
    """
    To verify hostState of device dhcpv6 client
    IDLE,REQUEST,RELEASE,RENEW,REBIND,AUTORENEW,GROUPREQ,BOUND
    """
    result = "FAIL"
    check_time = int(check_time)
    if check_time > 5 :
        interval_time = int(check_time/5)
    else:
        interval_time = 1
    current_time = time.mktime(time.localtime())
    end_time = current_time + check_time

    while current_time <= end_time:
        try:
            result = STC_SESSION[session_name].hlk_traffic_verify_device_status(
            device_name,'Dhcpv6BlockConfig',BlockState=expect_state)
        except Exception as inst:
            llk_log.llk_log_debug("the state of dhcpv6 client is not as expected!,exception:%s" % inst)

        if result == "PASS" :
            break
        time.sleep(interval_time)
        current_time = time.mktime(time.localtime())
    if result == "FAIL":
        raise AssertionError("Fail to retrieve dhcpv6 client state: %s" %
        expect_state)
    else :
        return "PASS"

def stc_verify_dhcpv6_client_pdstate(device_name,expect_state,session_name="first_stc_session",check_time=1) :
    """
    To verify hostState of device dhcpv6 client
    IDLE,REQUEST,RELEASE,RENEW,REBIND,AUTORENEW,GROUPREQ,BOUND
    """
    result = "FAIL"
    check_time = int(check_time)
    if check_time > 5 :
        interval_time = int(check_time/5)
    else:
        interval_time = 1
    current_time = time.mktime(time.localtime())
    end_time = current_time + check_time

    while current_time <= end_time:
        try:
            result = STC_SESSION[session_name].hlk_traffic_verify_device_status(
            device_name,'Dhcpv6BlockConfig',pdBlockState=expect_state)
        except Exception as inst:
            llk_log.llk_log_debug("the pdstate of dhcpv6 client is not as expected!,exception:%s" % inst)

        if result == "PASS" :
            break
        time.sleep(interval_time)
        current_time = time.mktime(time.localtime())

    if result == "FAIL":
        raise AssertionError("Fail to retrieve dhcpv6 client pdstate: %s" %
        expect_state)
    else :
        return "PASS"


def stc_operate_dhcpv6_client(**kwargs) :
    # prepare kwargs
    kwargs = dict(map(lambda (k,v): (str(k).lower(), v), kwargs.iteritems()))

    # get session_name
    session_name = kwargs.setdefault('session_name','first_stc_session')

    # skip key 'session_name'
    if 'session_name' in kwargs.keys():
        kwargs.pop('session_name')

    # invoke hlk_traffic function
    STC_SESSION[session_name].hlk_traffic_operate_dhcpv6_client(**kwargs)

    return "PASS"


def stc_create_8021x_auth(lif_side, device_name, **args):
    """
    To create device 802.1x authentication
    """
    # convert args to lower case
    args = dict(map(lambda (k, v): (str(k).lower(), str(v)), args.iteritems()))
    ##print ('args:', args)
    llk_log.llk_log_info("Input params args:%s" % args)

    # get session_name
    session_name = args.setdefault('session_name', 'first_stc_session')
    if 'session_name' in args.keys():
        args.pop('session_name')

    # get port object. for example: port1
    port_id = ''
    if 'client' in lif_side.lower():
        port_id = dCLIENT_PORT[session_name]['PortId']
    elif 'network' in lif_side.lower():
        port_id = dNETWORK_PORT[session_name]['PortId']

    # create EmulatedDevice
    oDevice = STC_SESSION[session_name].hlk_traffic_config_device(
        name=device_name, portID=port_id)
    ##print ('oDevice:', oDevice)

    # config IPV4If
    if 'ipv4if' in args.keys():

        # prepare param
        dArgs = {}
        paramList = args.pop('ipv4if').strip('{}[]()').split(',')
        for item in paramList:
            key, value = item.split('=')
            dArgs[key] = value

        STC_SESSION[session_name].hlk_traffic_config_device(
            name=device_name, type='ipv4', **dArgs)

    # config EthIIIf
    if 'ethiiif' in args.keys():
        # prepare param
        dArgs = {}
        paramList = args.pop('ethiiif').strip('{}[]()').split(',')
        for item in paramList:
            key, value = item.split('=')
            dArgs[key] = value

        # config device
        STC_SESSION[session_name].hlk_traffic_config_device(
            name=device_name, type='ethii', **dArgs)
     
    # config VLANIf
    if 'vlanif' in args.keys():
        # prepare param
        dArgs = {}
        paramList = args.pop('vlanif').strip('{}[]()').split(',')
        for item in paramList:
            key,value = item.split('=')
            dArgs[key] = value

        STC_SESSION[session_name].hlk_traffic_config_device(
        name=device_name,type='vlan',**dArgs)


    ##print ('args2:', args)
    # create 802.1x authentication
    args['device'] = device_name
    logger.debug ('device_name:', device_name)
    o8021xclient = STC_SESSION[session_name].hlk_traffic_config_8021x_auth(**args)
    """
    """
    # start 802.1x authentication
    STC_SESSION[session_name].hlk_traffic_start_devices(
        device=STC_SESSION[session_name].dDevices[device_name]['oDevice'])

    logger.debug("dDevices[device_name]:%s" %
                 STC_SESSION[session_name].dDevices[device_name])

    return STC_SESSION[session_name].dDevices[device_name]['8021xclient']

def stc_verify_8021x_auth_state(device_name,expect_state,session_name="first_stc_session",check_time=1) :
    """
    """
    result = "FAIL"
    check_time = int(check_time)
    if check_time > 5 :
        interval_time = int(check_time/5)
    else:
        interval_time = 1
    current_time = time.mktime(time.localtime())
    end_time = current_time + check_time

    while current_time <= end_time:
        try:
            result = STC_SESSION[session_name].hlk_traffic_verify_device_status(
            device_name,'Dot1xSupplicantBlockConfig',AuthState=expect_state)
        except Exception as inst:
            llk_log.llk_log_debug("the state of 802.1x authentication is not as expected!,exception:%s" % inst)

        if result == "PASS" :
            break
        time.sleep(interval_time)
        current_time = time.mktime(time.localtime())

    if result == "FAIL":
        raise AssertionError("Fail to retrieve 802.1x authentication state: %s" %
        expect_state)
    else :
        return "PASS"


def stc_operate_8021x_auth(**kwargs) :
    # prepare kwargs
    kwargs = dict(map(lambda (k,v): (str(k).lower(), v), kwargs.iteritems()))

    # get session_name
    session_name = kwargs.setdefault('session_name','first_stc_session')

    # skip key 'session_name'
    if 'session_name' in kwargs.keys():
        kwargs.pop('session_name')

    # invoke hlk_traffic function
    STC_SESSION[session_name].hlk_traffic_operate_8021x_auth(**kwargs)

    return "PASS"


def stc_create_ospfv3_router(lif_side, device_name, **args):
    """
    To create device ospfv3 router
    """
    # convert args to lower case
    args = dict(map(lambda (k, v): (str(k).lower(), str(v)), args.iteritems()))
    llk_log.llk_log_info("Input params args:%s" % args)
    # get session_name
    session_name = args.setdefault('session_name', 'first_stc_session')
    if 'session_name' in args.keys():
        args.pop('session_name')
    # get port object. for example: port1
    port_id = ''
    if 'client' in lif_side.lower():
        port_id = dCLIENT_PORT[session_name]['PortId']
    elif 'network' in lif_side.lower():
        port_id = dNETWORK_PORT[session_name]['PortId']
    
    # create EmulatedDevice
    oDevice = STC_SESSION[session_name].hlk_traffic_config_device(
        name=device_name, portID=port_id)    
    # config IPV6If
    if 'ipv6if' in args.keys():
        dArgs = {}
        paramList = args.pop('ipv6if').strip('{}[]()').split(',')
        for item in paramList:
            key, value = item.split('=')
            dArgs[key] = value

        STC_SESSION[session_name].hlk_traffic_config_device(
            name=device_name, type='ipv6', **dArgs)   
 
   # config ipv6llif
    if 'ipv6llif' in args.keys():

        # prepare param
        dArgs = {}
        paramList = args.pop('ipv6llif').strip('{}[]()').split(',')
        for item in paramList:
            key, value = item.split('=')
            dArgs[key] = value

        oDevice =STC_SESSION[session_name].hlk_traffic_config_deviceb(
            name=device_name, type='ipv6ll', **dArgs)


    # config EthIIIf
    if 'ethiiif' in args.keys():
        # prepare param
        dArgs = {}
        paramList = args.pop('ethiiif').strip('{}[]()').split(',')
        for item in paramList:
            key, value = item.split('=')
            dArgs[key] = value

        # config device
        STC_SESSION[session_name].hlk_traffic_config_device(
            name=device_name, type='ethii', **dArgs)
    
    # config VLANIf
    if 'vlanif' in args.keys():
        # prepare param
        dArgs = {}
        paramList = args.pop('vlanif').strip('{}[]()').split(',')
        for item in paramList:
            key,value = item.split('=')
            dArgs[key] = value

        STC_SESSION[session_name].hlk_traffic_config_device(
        name=device_name,type='vlan',**dArgs)

    args['device'] = device_name
    logger.debug ('device_name:', device_name)
    kospfv3router = STC_SESSION[session_name].hlk_traffic_config_ospfv3_router(**args)

    # start ospfv3_router
    STC_SESSION[session_name].hlk_traffic_start_devices(
        device=STC_SESSION[session_name].dDevices[device_name]['oDevice'])

    logger.debug("dDevices[device_name]:%s" %
                 STC_SESSION[session_name].dDevices[device_name])

    return STC_SESSION[session_name].dDevices[device_name]['ospfv3router']

def stc_create_ospfv2_router(lif_side, device_name, **args):
    """
    To create device ospfv2 router
    """
    # convert args to lower case
    args = dict(map(lambda (k, v): (str(k).lower(), str(v)), args.iteritems()))
    ##print ('args:', args)
    llk_log.llk_log_info("Input params args:%s" % args)

    # get session_name
    session_name = args.setdefault('session_name', 'first_stc_session')
    if 'session_name' in args.keys():
        args.pop('session_name')

    # get port object. for example: port1
    port_id = ''
    if 'client' in lif_side.lower():
        port_id = dCLIENT_PORT[session_name]['PortId']
    elif 'network' in lif_side.lower():
        port_id = dNETWORK_PORT[session_name]['PortId']

    # create EmulatedDevice
    oDevice = STC_SESSION[session_name].hlk_traffic_config_device(
        name=device_name, portID=port_id)
    ##print ('oDevice:', oDevice)

    # config IPV4If
    if 'ipv4if' in args.keys():

        # prepare param
        dArgs = {}
        paramList = args.pop('ipv4if').strip('{}[]()').split(',')
        for item in paramList:
            key, value = item.split('=')
            dArgs[key] = value

        STC_SESSION[session_name].hlk_traffic_config_device(
            name=device_name, type='ipv4', **dArgs)

    # config EthIIIf
    if 'ethiiif' in args.keys():
        # prepare param
        dArgs = {}
        paramList = args.pop('ethiiif').strip('{}[]()').split(',')
        for item in paramList:
            key, value = item.split('=')
            dArgs[key] = value

        # config device
        STC_SESSION[session_name].hlk_traffic_config_device(
            name=device_name, type='ethii', **dArgs)
     
    # config VLANIf
    if 'vlanif' in args.keys():
        # prepare param
        dArgs = {}
        paramList = args.pop('vlanif').strip('{}[]()').split(',')
        for item in paramList:
            key,value = item.split('=')
            dArgs[key] = value

        STC_SESSION[session_name].hlk_traffic_config_device(
        name=device_name,type='vlan',**dArgs)


    ##print ('args2:', args)
    # create ospfv2 router
    args['device'] = device_name
    logger.debug ('device_name:', device_name)
    kospfv2router = STC_SESSION[session_name].hlk_traffic_config_ospfv2_router(**args)

    # start ospfv2_router
    STC_SESSION[session_name].hlk_traffic_start_devices(
        device=STC_SESSION[session_name].dDevices[device_name]['oDevice'])

    logger.debug("dDevices[device_name]:%s" %
                 STC_SESSION[session_name].dDevices[device_name])

    return STC_SESSION[session_name].dDevices[device_name]['ospfv2router']


def stc_verify_ospfv2_router_state(device_name,expect_state,session_name="first_stc_session",check_time=1) :
    """
    """
    result = "FAIL"
    check_time = int(check_time)
    if check_time > 5 :
        interval_time = int(check_time/5)
    else:
        interval_time = 1
    current_time = time.mktime(time.localtime())
    end_time = current_time + check_time

    while current_time <= end_time:
        try:
            result = STC_SESSION[session_name].hlk_traffic_verify_device_status(
            device_name,'Ospfv2RouterConfig',AdjacencyStatus=expect_state)
        except Exception as inst:
            llk_log.llk_log_debug("the state of ospfv2 router is not as expected!,exception:%s" % inst)

        if result == "PASS" :
            break
        time.sleep(interval_time)
        current_time = time.mktime(time.localtime())

    if result == "FAIL":
        raise AssertionError("Fail to retrieve ospfv2 router state: %s" %
        expect_state)
    else :
        return "PASS"

def stc_operate_ospfv2_router(**kwargs) :
    # prepare kwargs
    kwargs = dict(map(lambda (k,v): (str(k).lower(), v), kwargs.iteritems()))

    # get session_name
    session_name = kwargs.setdefault('session_name','first_stc_session')

    # skip key 'session_name'
    if 'session_name' in kwargs.keys():
        kwargs.pop('session_name')

    # invoke hlk_traffic function
    STC_SESSION[session_name].hlk_traffic_operate_ospfv2_router(**kwargs)

    return "PASS"

def stc_verify_ospfv3_router_state(device_name,expect_state,session_name="first_stc_session",check_time=1) :
    """
    """
    result = "FAIL"
    check_time = int(check_time)
    if check_time > 5 :
        interval_time = int(check_time/5)
    else:
        interval_time = 1
    current_time = time.mktime(time.localtime())
    end_time = current_time + check_time

    while current_time <= end_time:
        try:
            result = STC_SESSION[session_name].hlk_traffic_verify_device_status(
            device_name,'Ospfv3RouterConfig',AdjacencyStatus=expect_state)
        except Exception as inst:
            llk_log.llk_log_debug("the state of ospfv3 router is not as expected!,exception:%s" % inst)

        if result == "PASS" :
            break
        time.sleep(interval_time)
        current_time = time.mktime(time.localtime())

    if result == "FAIL":
        raise AssertionError("Fail to retrieve ospfv3 router state: %s" %
        expect_state)
    else :
        return "PASS"

def stc_operate_ospfv3_router(**kwargs) :
    # prepare kwargs
    kwargs = dict(map(lambda (k,v): (str(k).lower(), v), kwargs.iteritems()))

    # get session_name
    session_name = kwargs.setdefault('session_name','first_stc_session')

    # skip key 'session_name'
    if 'session_name' in kwargs.keys():
        kwargs.pop('session_name')

    # invoke hlk_traffic function
    STC_SESSION[session_name].hlk_traffic_operate_ospfv3_router(**kwargs)

    return "PASS"

def stc_create_rip_router(lif_side, device_name, **args):
    """
    To create device rip router
    """
    # convert args to lower case
    args = dict(map(lambda (k, v): (str(k).lower(), str(v)), args.iteritems()))
    ##print ('args:', args)
    llk_log.llk_log_info("Input params args:%s" % args)

    # get session_name
    session_name = args.setdefault('session_name', 'first_stc_session')
    if 'session_name' in args.keys():
        args.pop('session_name')

    # get port object. for example: port1
    port_id = ''
    if 'client' in lif_side.lower():
        port_id = dCLIENT_PORT[session_name]['PortId']
    elif 'network' in lif_side.lower():
        port_id = dNETWORK_PORT[session_name]['PortId']

    # create EmulatedDevice
    oDevice = STC_SESSION[session_name].hlk_traffic_config_device(
        name=device_name, portID=port_id)
    ##print ('oDevice:', oDevice)

    # config IPV4If
    if 'ipv4if' in args.keys():

        # prepare param
        dArgs = {}
        paramList = args.pop('ipv4if').strip('{}[]()').split(',')
        for item in paramList:
            key, value = item.split('=')
            dArgs[key] = value

        STC_SESSION[session_name].hlk_traffic_config_device(
            name=device_name, type='ipv4', **dArgs)

    # config EthIIIf
    if 'ethiiif' in args.keys():
        # prepare param
        dArgs = {}
        paramList = args.pop('ethiiif').strip('{}[]()').split(',')
        for item in paramList:
            key, value = item.split('=')
            dArgs[key] = value

        # config device
        STC_SESSION[session_name].hlk_traffic_config_device(
            name=device_name, type='ethii', **dArgs)
     
    # config VLANIf
    if 'vlanif' in args.keys():
        # prepare param
        dArgs = {}
        paramList = args.pop('vlanif').strip('{}[]()').split(',')
        for item in paramList:
            key,value = item.split('=')
            dArgs[key] = value

        STC_SESSION[session_name].hlk_traffic_config_device(
        name=device_name,type='vlan',**dArgs)


    ##print ('args2:', args)
    # create rip router
    args['device'] = device_name
    logger.debug ('device_name:', device_name)
    kriprouter = STC_SESSION[session_name].hlk_traffic_config_rip_router(**args)

    # start rip_router
    STC_SESSION[session_name].hlk_traffic_start_devices(
        device=STC_SESSION[session_name].dDevices[device_name]['oDevice'])

    logger.debug("dDevices[device_name]:%s" %
                 STC_SESSION[session_name].dDevices[device_name])

    return STC_SESSION[session_name].dDevices[device_name]['riprouter']



def stc_verify_rip_router_state(device_name,expect_state,session_name="first_stc_session",check_time=1) :
    """
    """
    result = "FAIL"
    check_time = int(check_time)
    if check_time > 5 :
        interval_time = int(check_time/5)
    else:
        interval_time = 1
    current_time = time.mktime(time.localtime())
    end_time = current_time + check_time

    while current_time <= end_time:
        try:
            result = STC_SESSION[session_name].hlk_traffic_verify_device_status(
            device_name,'RipRouterConfig',RouterState=expect_state)
        except Exception as inst:
            llk_log.llk_log_debug("the state of rip router is not as expected!,exception:%s" % inst)

        if result == "PASS" :
            break
        time.sleep(interval_time)
        current_time = time.mktime(time.localtime())

    if result == "FAIL":
        raise AssertionError("Fail to retrieve rip router state: %s" %
        expect_state)
    else :
        return "PASS"



def stc_operate_rip_router(**kwargs) :
    # prepare kwargs
    kwargs = dict(map(lambda (k,v): (str(k).lower(), v), kwargs.iteritems()))

    # get session_name
    session_name = kwargs.setdefault('session_name','first_stc_session')

    # skip key 'session_name'
    if 'session_name' in kwargs.keys():
        kwargs.pop('session_name')

    # invoke hlk_traffic function
    STC_SESSION[session_name].hlk_traffic_operate_rip_router(**kwargs)

    return "PASS"

def stc_create_bgp_router(lif_side, device_name, **args):
    """
    To create device bgp router
    """
    # convert args to lower case
    args = dict(map(lambda (k, v): (str(k).lower(), str(v)), args.iteritems()))
    ##print ('args:', args)
    llk_log.llk_log_info("Input params args:%s" % args)

    # get session_name
    session_name = args.setdefault('session_name', 'first_stc_session')
    if 'session_name' in args.keys():
        args.pop('session_name')

    # get port object. for example: port1
    port_id = ''
    if 'client' in lif_side.lower():
        port_id = dCLIENT_PORT[session_name]['PortId']
    elif 'network' in lif_side.lower():
        port_id = dNETWORK_PORT[session_name]['PortId']

    # create EmulatedDevice
    oDevice = STC_SESSION[session_name].hlk_traffic_config_device(
        name=device_name, portID=port_id)
    ##print ('oDevice:', oDevice)

    # config IPV4If
    if 'ipv4if' in args.keys():

        # prepare param
        dArgs = {}
        paramList = args.pop('ipv4if').strip('{}[]()').split(',')
        for item in paramList:
            key, value = item.split('=')
            dArgs[key] = value

        STC_SESSION[session_name].hlk_traffic_config_device(
            name=device_name, type='ipv4', **dArgs)

    # config EthIIIf
    if 'ethiiif' in args.keys():
        # prepare param
        dArgs = {}
        paramList = args.pop('ethiiif').strip('{}[]()').split(',')
        for item in paramList:
            key, value = item.split('=')
            dArgs[key] = value

        # config device
        STC_SESSION[session_name].hlk_traffic_config_device(
            name=device_name, type='ethii', **dArgs)
     
    # config VLANIf
    if 'vlanif' in args.keys():
        # prepare param
        dArgs = {}
        paramList = args.pop('vlanif').strip('{}[]()').split(',')
        for item in paramList:
            key,value = item.split('=')
            dArgs[key] = value

        STC_SESSION[session_name].hlk_traffic_config_device(
        name=device_name,type='vlan',**dArgs)


    ##print ('args2:', args)
    # create bgp router
    args['device'] = device_name
    logger.debug ('device_name:', device_name)
    kbgprouter = STC_SESSION[session_name].hlk_traffic_config_bgp_router(**args)

    # start bgp_router
    STC_SESSION[session_name].hlk_traffic_start_devices(
        device=STC_SESSION[session_name].dDevices[device_name]['oDevice'])

    logger.debug("dDevices[device_name]:%s" %
                 STC_SESSION[session_name].dDevices[device_name])

    return STC_SESSION[session_name].dDevices[device_name]['bgprouter']



def stc_verify_bgp_router_state(device_name,expect_state,session_name="first_stc_session",check_time=1) :
    """
    """
    result = "FAIL"
    check_time = int(check_time)
    if check_time > 5 :
        interval_time = int(check_time/5)
    else:
        interval_time = 1
    current_time = time.mktime(time.localtime())
    end_time = current_time + check_time

    while current_time <= end_time:
        try:
            result = STC_SESSION[session_name].hlk_traffic_verify_device_status(
            device_name,'BgpRouterConfig',RouterState=expect_state)
        except Exception as inst:
            llk_log.llk_log_debug("the state of bgp router is not as expected!,exception:%s" % inst)

        if result == "PASS" :
            break
        time.sleep(interval_time)
        current_time = time.mktime(time.localtime())

    if result == "FAIL":
        raise AssertionError("Fail to retrieve bgp router state: %s" %
        expect_state)
    else :
        return "PASS"



def stc_operate_bgp_router(**kwargs) :
    # prepare kwargs
    kwargs = dict(map(lambda (k,v): (str(k).lower(), v), kwargs.iteritems()))

    # get session_name
    session_name = kwargs.setdefault('session_name','first_stc_session')

    # skip key 'session_name'
    if 'session_name' in kwargs.keys():
        kwargs.pop('session_name')

    # invoke hlk_traffic function
    STC_SESSION[session_name].hlk_traffic_operate_bgp_router(**kwargs)

    return "PASS"

def stc_create_synce_device(lif_side, device_name, **args):
    """
    To create SyncEth Device
    """
    # convert args to lower case
    args = dict(map(lambda (k, v): (str(k).lower(), str(v)), args.iteritems()))
    ##print ('args:', args)
    llk_log.llk_log_info("Input params args:%s" % args)

    # get session_name
    session_name = args.setdefault('session_name', 'first_stc_session')
    if 'session_name' in args.keys():
        args.pop('session_name')

    # get port object. for example: port1
    port_id = ''
    if 'client' in lif_side.lower():
        port_id = dCLIENT_PORT[session_name]['PortId']
    elif 'network' in lif_side.lower():
        port_id = dNETWORK_PORT[session_name]['PortId']

    # create EmulatedDevice
    #if device_name:
    #    print ('The device is already created')
    #else:
        oDevice = STC_SESSION[session_name].hlk_traffic_config_device(name=device_name, portID=port_id)
    ##print ('oDevice:', oDevice)

    # config IPV4If
    if 'ipv4if' in args.keys():

        # prepare param
        dArgs = {}
        paramList = args.pop('ipv4if').strip('{}[]()').split(',')
        for item in paramList:
            key, value = item.split('=')
            dArgs[key] = value

        STC_SESSION[session_name].hlk_traffic_config_device(
            name=device_name, type='ipv4', **dArgs)

    # config EthIIIf
    if 'ethiiif' in args.keys():
        # prepare param
        dArgs = {}
        paramList = args.pop('ethiiif').strip('{}[]()').split(',')
        for item in paramList:
            key, value = item.split('=')
            dArgs[key] = value

        # config device
        STC_SESSION[session_name].hlk_traffic_config_device(
            name=device_name, type='ethii', **dArgs)
     
    # config VLANIf
    if 'vlanif' in args.keys():
        # prepare param
        dArgs = {}
        paramList = args.pop('vlanif').strip('{}[]()').split(',')
        for item in paramList:
            key,value = item.split('=')
            dArgs[key] = value

        STC_SESSION[session_name].hlk_traffic_config_device(
        name=device_name,type='vlan',**dArgs)


    ##print ('args2:', args)
    # create SyncEth Device
    args['device'] = device_name
    logger.debug ('device_name:', device_name)
    osyncedevice = STC_SESSION[session_name].hlk_traffic_config_synce_device(**args)

    # start SyncEth Device
    STC_SESSION[session_name].hlk_traffic_start_devices(
        device=STC_SESSION[session_name].dDevices[device_name]['oDevice'])

    logger.debug("dDevices[device_name]:%s" %
                 STC_SESSION[session_name].dDevices[device_name])

    return STC_SESSION[session_name].dDevices[device_name]['syncedevice']


def stc_verify_synce_state(device_name,expect_state,session_name="first_stc_session",check_times=1) :
    """
    """
    result = "FAIL"
    check_times = int(check_times)
    while check_times >=0 :
        try:
            result = STC_SESSION[session_name].hlk_traffic_verify_device_status(
            device_name,'SyncEthDeviceConfig',ClockState=expect_state)
        except Exception as inst:
            llk_log.llk_log_debug("the state of synce is not as expected!,exception:%s" % inst)

        if result == "PASS" :
            break
        time.sleep(2)
        check_times -= 1

    if result == "FAIL":
        raise AssertionError("Fail to retrieve synce state: %s" %
        expect_state)
    else :
        return "PASS"

def stc_modify_synce_ql(**kwargs) :
    # prepare kwargs
    kwargs = dict(map(lambda (k,v): (str(k).lower(), v), kwargs.iteritems()))

    # get session_name
    session_name = kwargs.setdefault('session_name','first_stc_session')

    # skip key 'session_name'
    if 'session_name' in kwargs.keys():
        kwargs.pop('session_name')

    # invoke hlk_traffic function
    STC_SESSION[session_name].hlk_traffic_modify_synce_ql(**kwargs)

    return "PASS"

def stc_create_eoam_node(lif_side, device_name, **args):
    """
    To create device eoam node
    """
    # convert args to lower case
    args = dict(map(lambda (k, v): (str(k).lower(), str(v)), args.iteritems()))
    ##print ('args:', args)
    llk_log.llk_log_info("Input params args:%s" % args)

    # get session_name
    session_name = args.setdefault('session_name', 'first_stc_session')
    if 'session_name' in args.keys():
        args.pop('session_name')

    # get port object. for example: port1
    port_id = ''
    if 'client' in lif_side.lower():
        port_id = dCLIENT_PORT[session_name]['PortId']
    elif 'network' in lif_side.lower():
        port_id = dNETWORK_PORT[session_name]['PortId']

    # create EmulatedDevice
    oDevice = STC_SESSION[session_name].hlk_traffic_config_device(
        name=device_name, portID=port_id)
    ##print ('oDevice:', oDevice)

    # config IPV4If
    if 'ipv4if' in args.keys():

        # prepare param
        dArgs = {}
        paramList = args.pop('ipv4if').strip('{}[]()').split(',')
        for item in paramList:
            key, value = item.split('=')
            dArgs[key] = value

        STC_SESSION[session_name].hlk_traffic_config_device(
            name=device_name, type='ipv4', **dArgs)

    # config EthIIIf
    if 'ethiiif' in args.keys():
        # prepare param
        dArgs = {}
        paramList = args.pop('ethiiif').strip('{}[]()').split(',')
        for item in paramList:
            key, value = item.split('=')
            dArgs[key] = value

        # config device
        STC_SESSION[session_name].hlk_traffic_config_device(
            name=device_name, type='ethii', **dArgs)
     
    # config VLANIf
    if 'vlanif' in args.keys():
        # prepare param
        dArgs = {}
        paramList = args.pop('vlanif').strip('{}[]()').split(',')
        for item in paramList:
            key,value = item.split('=')
            dArgs[key] = value

        STC_SESSION[session_name].hlk_traffic_config_device(
        name=device_name,type='vlan',**dArgs)

    ##print ('args2:', args)
    # create igmp host
    args['device'] = device_name
    logger.debug ('device_name:', device_name)
    oEoamNode = STC_SESSION[session_name].hlk_traffic_config_eoam_node(**args)

    STC_SESSION[session_name].oLlkStc.llk_stc_send_command("stc::apply")

    # start eoam node
    STC_SESSION[session_name].hlk_traffic_start_devices(
        device=STC_SESSION[session_name].dDevices[device_name]['oDevice'])

    logger.debug("dDevices[device_name]:%s" %
                 STC_SESSION[session_name].dDevices[device_name])

    return STC_SESSION[session_name].dDevices[device_name]['eoamnode']

def stc_delete_eoamgenconfig(session_name="first_stc_session"):
    """
    Usage Example: 
    | stc_delete_eoamgenconfig | 
    """
    STC_SESSION[session_name].hlk_traffic_delete_eoamgenconfig()

    llk_log.llk_log_info("Delete EoamGenConfig Successfully!")

def stc_operate_device(**kwargs) :
    # prepare kwargs
    kwargs = dict(map(lambda (k,v): (str(k).lower(), v), kwargs.iteritems()))

    # get session_name
    session_name = kwargs.setdefault('session_name','first_stc_session')

    # skip key 'session_name'
    if 'session_name' in kwargs.keys():
        kwargs.pop('session_name')

    # invoke hlk_traffic function
    STC_SESSION[session_name].hlk_traffic_operate_device(**kwargs)

    return "PASS"

def stc_operate_startarpnd(**kwargs) :
    # prepare kwargs
    kwargs = dict(map(lambda (k,v): (str(k).lower(), v), kwargs.iteritems()))

    # get session_name
    session_name = kwargs.setdefault('session_name','first_stc_session')

    # skip key 'session_name'
    if 'session_name' in kwargs.keys():
        kwargs.pop('session_name')

    # invoke hlk_traffic function
    STC_SESSION[session_name].hlk_traffic_operate_startarpnd(**kwargs)

    return "PASS"

def stc_verify_ipv6_nd_state(device_name,expect_state,session_name="first_stc_session",check_times=1) :

    result = "FAIL"
    check_times = int(check_times)
    while check_times >=0 :
        try:
            result = STC_SESSION[session_name].hlk_traffic_verify_ipv6arpnd_status(
            device_name,'Ipv6If.GatewayMacResolveState',ResolveState=expect_state)
        except Exception as inst:
            llk_log.llk_log_debug("the state of ARPND is not as expected!,exception:%s" % inst)

        if result == "PASS" :
            break
        time.sleep(2)
        check_times -= 1

    if result == "FAIL":
        raise AssertionError("Fail to retrieve ARPND state: %s" %
        expect_state)
    else :
        return "PASS"

def stc_create_ipv6_device(lif_side, device_name, **args):
    """
    To create device for arp
    """
    # convert args to lower case
    args = dict(map(lambda (k, v): (str(k).lower(), str(v)), args.iteritems()))
    #print ('args:', args)
    llk_log.llk_log_info("Input params args:%s" % args)

    # get session_name
    session_name = args.setdefault('session_name', 'first_stc_session')
    if 'session_name' in args.keys():
        args.pop('session_name')

    # get port object. for example: port1
    port_id = ''
    if 'client' in lif_side.lower():
        port_id = dCLIENT_PORT[session_name]['PortId']
    elif 'network' in lif_side.lower():
        port_id = dNETWORK_PORT[session_name]['PortId']

    # create EmulatedDevice
    oDevice = STC_SESSION[session_name].hlk_traffic_config_device(
        name=device_name, portID=port_id)
    #print ('oDevice:', oDevice)

    # config IPV6If
    if 'ipv6if' in args.keys():

        # prepare param
        dArgs = {}
        paramList = args.pop('ipv6if').strip('{}[]()').split(',')
        for item in paramList:
            key, value = item.split('=')
            dArgs[key] = value

        oDevice =STC_SESSION[session_name].hlk_traffic_config_device(
            name=device_name, type='ipv6', **dArgs)

    # config ipv6llif
    if 'ipv6llif' in args.keys():

        # prepare param
        dArgs = {}
        paramList = args.pop('ipv6llif').strip('{}[]()').split(',')
        for item in paramList:
            key, value = item.split('=')
            dArgs[key] = value

        oDevice =STC_SESSION[session_name].hlk_traffic_config_deviceb(
            name=device_name, type='ipv6ll', **dArgs)

    # config EthIIIf
    if 'ethiiif' in args.keys():
        # prepare param
        dArgs = {}
        paramList = args.pop('ethiiif').strip('{}[]()').split(',')
        for item in paramList:
            key, value = item.split('=')
            dArgs[key] = value

        # config device
        oDevice =STC_SESSION[session_name].hlk_traffic_config_device(
            name=device_name, type='ethii', **dArgs)
     
    # config VLANIf
    if 'vlanif' in args.keys():
        # prepare param
        dArgs = {}
        paramList = args.pop('vlanif').strip('{}[]()').split(',')
        for item in paramList:
            key,value = item.split('=')
            dArgs[key] = value

        oDevice =STC_SESSION[session_name].hlk_traffic_config_device(
        name=device_name,type='vlan',**dArgs)

    #print ('args2:', args)
    # create dhcpv6 server
    args['device'] = device_name
    #print ('device_name:', device_name)
    oIpv6Device = STC_SESSION[session_name].hlk_traffic_config_ipv6_device(**args)
    #print ('oDhcpv6Server : ', oDhcpv6Server)

    logger.debug("dDevices[device_name]:%s" %
                STC_SESSION[session_name].dDevices[device_name])

    return oIpv6Device 

def stc_config_streamblock(**kwargs):
    # prepare kwargs
    kwargs = dict(map(lambda (k,v): (str(k).lower(), v), kwargs.iteritems()))
    
    # get session_name
    session_name = kwargs.setdefault('session_name','first_stc_session')

    # skip key 'session_name'
    if 'session_name' in kwargs.keys():
        kwargs.pop('session_name')

    # invoke hlk_traffic function
    STC_SESSION[session_name].hlk_traffic_config_streamblock(**kwargs)

    STC_SESSION[session_name].oLlkStc.llk_stc_send_command("stc::apply")

    return "PASS"

def stc_start_traffic(**kwargs):
    # prepare kwargs
    kwargs = dict(map(lambda (k,v): (str(k).lower(), v), kwargs.iteritems()))

    # get session_name
    session_name = kwargs.setdefault('session_name','first_stc_session')

    # skip key 'session_name'
    if 'session_name' in kwargs.keys():
        kwargs.pop('session_name')

    # invoke hlk_traffic function
    STC_SESSION[session_name].hlk_traffic_start_traffic(**kwargs)

    return "PASS"

def stc_stop_traffic(**kwargs):
    # prepare kwargs
    kwargs = dict(map(lambda (k,v): (str(k).lower(), v), kwargs.iteritems()))

    # get session_name
    session_name = kwargs.setdefault('session_name','first_stc_session')

    # skip key 'session_name'
    if 'session_name' in kwargs.keys():
        kwargs.pop('session_name')

    # invoke hlk_traffic function
    STC_SESSION[session_name].hlk_traffic_stop_traffic(**kwargs)

    return "PASS"

def stc_start_measure(**kwargs):
    # prepare kwargs
    kwargs = dict(map(lambda (k,v): (str(k).lower(), v), kwargs.iteritems()))

    # get session_name
    session_name = kwargs.setdefault('session_name','first_stc_session')

    # skip key 'session_name'
    if 'session_name' in kwargs.keys():
        kwargs.pop('session_name')

    # invoke hlk_traffic function
    STC_SESSION[session_name].hlk_traffic_start_measure(**kwargs)  

    return "PASS"

def stc_stop_measure(**kwargs):
    # prepare kwargs
    kwargs = dict(map(lambda (k,v): (str(k).lower(), v), kwargs.iteritems()))

    # get session_name
    session_name = kwargs.setdefault('session_name','first_stc_session')

    # skip key 'session_name'
    if 'session_name' in kwargs.keys():
        kwargs.pop('session_name')

    # invoke hlk_traffic function
    STC_SESSION[session_name].hlk_traffic_stop_measure(**kwargs) 

    return "PASS"

def stc_retrieve_measure(**kwargs):
    # prepare kwargs
    kwargs = dict(map(lambda (k,v): (str(k).lower(), v), kwargs.iteritems()))

    # get session_name
    session_name = kwargs.setdefault('session_name','first_stc_session')

    # skip key 'session_name'
    if 'session_name' in kwargs.keys():
        kwargs.pop('session_name')

    # invoke hlk_traffic function
    return STC_SESSION[session_name].hlk_traffic_retrieve_measure(**kwargs)

def stc_verify_measure_port(**kwargs):
    # prepare kwargs
    kwargs = dict(map(lambda (k,v): (str(k).lower(), v), kwargs.iteritems()))

    if 'side' not in kwargs.keys() :
        logger.error("stc_verify_measure_port-> Parameter 'side' must be inputted!")
        raise AssertionError("Parameter 'side' must be inputted!")

    # get session_name
    session_name = kwargs.setdefault('session_name','first_stc_session')

    # skip key 'session_name'
    if 'session_name' in kwargs.keys():
        kwargs.pop('session_name')

    sPortType = kwargs.pop('side')
    sPortId = ''
    if sPortType.lower() == 'client' :
        sPortId = dCLIENT_PORT[session_name]['PortId']
    elif sPortType.lower() == 'network' :
        sPortId = dNETWORK_PORT[session_name]['PortId']

    kwargs['portid'] = sPortId
    # invoke hlk_traffic function
    result = STC_SESSION[session_name].hlk_traffic_verify_measure_port(**kwargs)
    if result.lower() == 'fail' :
        raise StcContinueFailure("Verify Capture Failed!")
    else :
        return 'pass'

def stc_verify_measure(**kwargs):
    # prepare kwargs
    kwargs = dict(map(lambda (k,v): (str(k).lower(), v), kwargs.iteritems()))

    # get session_name
    session_name = kwargs.setdefault('session_name','first_stc_session')

    # skip key 'session_name'
    if 'session_name' in kwargs.keys():
        kwargs.pop('session_name')

    # invoke hlk_traffic function
    result = STC_SESSION[session_name].hlk_traffic_verify_measure(**kwargs)
    if result.lower() == 'fail' : 
        raise StcContinueFailure("Verify Capture Failed!")
    else : 
        return 'pass'

def stc_delete_streamblock(**kwargs):
    # prepare kwargs
    kwargs = dict(map(lambda (k,v): (str(k).lower(), v), kwargs.iteritems()))

    # get session_name
    session_name = kwargs.setdefault('session_name','first_stc_session')

    # skip key 'session_name'
    if 'session_name' in kwargs.keys():
        kwargs.pop('session_name')

    # invoke hlk_traffic function
    STC_SESSION[session_name].hlk_traffic_delete_streamblock(**kwargs)

    return "PASS"

def stc_config_port(**kwargs):
    # prepare kwargs
    kwargs = dict(map(lambda (k,v): (str(k).lower(), v), kwargs.iteritems()))

    # get session_name
    session_name = kwargs.setdefault('session_name','first_stc_session')

    # skip key 'session_name'
    if 'session_name' in kwargs.keys():
        kwargs.pop('session_name')

    # invoke hlk_traffic function
    STC_SESSION[session_name].hlk_traffic_config_port(**kwargs)

    return "PASS"

def stc_stop_devices(**kwargs):
    # prepare kwargs
    kwargs = dict(map(lambda (k,v): (str(k).lower(), v), kwargs.iteritems()))

    # get session_name
    session_name = kwargs.setdefault('session_name','first_stc_session')

    # skip key 'session_name'
    if 'session_name' in kwargs.keys():
        kwargs.pop('session_name')

    # invoke hlk_traffic function
    STC_SESSION[session_name].hlk_traffic_stop_devices(**kwargs)

    return "PASS"

def stc_start_capture(**kwargs) : 
    # prepare kwargs
    kwargs = dict(map(lambda (k,v): (str(k).lower(), v), kwargs.iteritems()))

    # get session_name
    session_name = kwargs.setdefault('session_name','first_stc_session')

    # skip key 'session_name'
    if 'session_name' in kwargs.keys():
        kwargs.pop('session_name')

    # invoke hlk_traffic function
    STC_SESSION[session_name].hlk_traffic_start_capture(**kwargs)

    return "PASS"

def stc_stop_capture_without_parse_pkt(**kwargs) :
    # prepare kwargs
    kwargs = dict(map(lambda (k,v): (str(k).lower(), v), kwargs.iteritems()))

    # get session_name
    session_name = kwargs.setdefault('session_name','first_stc_session')

    # skip key 'session_name'
    if 'session_name' in kwargs.keys():
        kwargs.pop('session_name')

    # invoke hlk_traffic function
    STC_SESSION[session_name].hlk_traffic_stop_capture_without_parse_pkt(**kwargs)

    return "PASS"

def stc_stop_capture(**kwargs) :
    # prepare kwargs
    kwargs = dict(map(lambda (k,v): (str(k).lower(), v), kwargs.iteritems()))

    # get session_name
    session_name = kwargs.setdefault('session_name','first_stc_session')

    # skip key 'session_name'
    if 'session_name' in kwargs.keys():
        kwargs.pop('session_name')

    # invoke hlk_traffic function
    STC_SESSION[session_name].hlk_traffic_stop_capture(**kwargs)

    return "PASS"

def stc_retrieve_capture(**kwargs) :
    # prepare kwargs
    kwargs = dict(map(lambda (k,v): (str(k).lower(), v), kwargs.iteritems()))

    # get session_name
    session_name = kwargs.setdefault('session_name','first_stc_session')

    # skip key 'session_name'
    if 'session_name' in kwargs.keys():
        kwargs.pop('session_name')

    # invoke hlk_traffic function
    return STC_SESSION[session_name].hlk_traffic_retrieve_capture(**kwargs)

def stc_verify_capture(**kwargs) :
    # prepare kwargs
    kwargs = dict(map(lambda (k,v): (str(k).lower(), v), kwargs.iteritems()))

    # get session_name
    session_name = kwargs.setdefault('session_name','first_stc_session')

    # skip key 'session_name'
    if 'session_name' in kwargs.keys():
        kwargs.pop('session_name')

    # invoke hlk_traffic function
    sResult = STC_SESSION[session_name].hlk_traffic_verify_capture(**kwargs)
    if sResult.lower() == 'fail' : 
        raise StcContinueFailure("Verify Capture Failed!")
    else : 
        return "pass"

def stc_verify_capture_without_parse_pkt(**kwargs) :
    # prepare kwargs
    kwargs = dict(map(lambda (k,v): (str(k).lower(), v), kwargs.iteritems()))

    # get session_name
    session_name = kwargs.setdefault('session_name','first_stc_session')

    # skip key 'session_name'
    if 'session_name' in kwargs.keys():
        kwargs.pop('session_name')

    # invoke hlk_traffic function
    sResult = STC_SESSION[session_name].hlk_traffic_verify_capture_without_parse_pkt(**kwargs)
    if sResult.lower() == 'fail' : 
        raise StcContinueFailure("Verify Capture Failed!")
    else : 
        return "pass"

def stc_get_measure_result (stream_index,measure_name,*lMeasureResult) :
    """
    get value of measure result. 

    - *stream_index:*     the index of stream
    - *measure_name:*     the name of measure item
    - *lMeasureResult:*   the measure result list

    Usage Example:
    | get measure result | 0 | framecount | ${FilterResult} |
    """
    try:
        measure_name = measure_name.lower()

        lMeasureResult = eval(str(lMeasureResult))
        llk_log.llk_log_debug("lMeasureResult:%s" % str(lMeasureResult))

        # get the dict of stresm index  
        dMeasResult = {}
        dMeasResult = lMeasureResult[int(stream_index)]

        llk_log.llk_log_debug("dMeasResult:%s" % dMeasResult)
        for each in dMeasResult.keys() :
            if 'page' in str(dMeasResult[each]) : 
                return dMeasResult[each][measure_name]

    except Exception as inst:
        s=sys.exc_info()
        raise AssertionError("Fail to get '%s' of stream %s, line:%s, exception:%s,\nresult:%s" 
        % (measure_name,stream_index,s[2].tb_lineno,inst,lMeasureResult) )
 
class StcContinueFailure (AssertionError) :
    ROBOT_CONTINUE_ON_FAILURE = True
