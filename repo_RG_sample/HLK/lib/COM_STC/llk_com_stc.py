import hlk_stc
#import llk_stc
#import hlk_traffic
from robot.libraries.BuiltIn import BuiltIn
from robot.api import logger
import data_translation
import llk_log

STC_SESSION = {}
# List of multiple measure retrive results
lMULTI_RETRIEVE_RESULTS = {}
# List of latest measure retrieve result
lLATEST_RESULT = {}
dCLIENT_PORT = {}
dNETWORK_PORT = {}
dDevice = {}

def llk_traffic_stc_issue_cmd (command,session_name="first_stc_session") :
    hlk_stc.stc_send_cmd(command,session_name)

def llk_traffic_stc_connect (**stc_info) :
    hlk_stc.stc_connect(**stc_info)

def llk_traffic_stc_disconnect (session_name="first_stc_session") :
    hlk_stc.stc_disconnect(session_name)

def llk_traffic_stc_start_traffic(**kwargs):
    hlk_stc.stc_start_traffic(**kwargs)

def llk_traffic_stc_stop_traffic(**kwargs):
    hlk_stc.stc_stop_traffic(**kwargs)

def llk_traffic_stc_start_measure(**kwargs):
    hlk_stc.stc_start_measure(**kwargs)

def llk_traffic_stc_stop_measure(**kwargs):
    hlk_stc.stc_stop_measure(**kwargs)

def llk_traffic_stc_retrieve_measure(**kwargs):
    hlk_stc.stc_retrieve_measure(**kwargs)

def llk_traffic_stc_verify_measure_stream(**kwargs):
    try:
        hlk_stc.stc_verify_measure(**kwargs)
        return "pass"
    except Exception as inst:
        return "fail"

def llk_traffic_stc_verify_measure_port(**kwargs):
    try:
        hlk_stc.stc_verify_measure_port(**kwargs)
        return "pass"
    except Exception as inst:
        return "fail"

def llk_traffic_stc_delete_streamblock(**kwargs):
    hlk_stc.stc_delete_streamblock(**kwargs)

def llk_traffic_stc_config_port(**kwargs):
    hlk_stc.stc_config_port(**kwargs)

def llk_traffic_stc_start_capture(**kwargs) :
    hlk_stc.stc_start_capture(**kwargs)
 
def llk_traffic_stc_stop_capture(**kwargs) :
    hlk_stc.stc_stop_capture(**kwargs)

def llk_traffic_stc_stop_capture_without_parse_pkt(**kwargs) :
    hlk_stc.stc_stop_capture_without_parse_pkt(**kwargs)

def llk_traffic_stc_retrieve_capture(**kwargs) :
    hlk_stc.stc_retrieve_capture(**kwargs)

def llk_traffic_stc_verify_capture(**kwargs) :
    try : 
        hlk_stc.stc_verify_capture(**kwargs)
        return "pass"
    except Exception as inst:
        return "fail"

def llk_traffic_stc_verify_capture_without_parse_pkt(**kwargs) :
    try : 
        hlk_stc.stc_verify_capture_without_parse_pkt(**kwargs)
        return "pass"
    except Exception as inst:
        return "fail"

def llk_traffic_stc_create_streamblock(sSBName,sPortId,sProtocolType,**dStream) : 
    hlk_stc.stc_create_streamblock(sSBName,sPortId,sProtocolType,**dStream)

def llk_traffic_stc_get_port_id(sPortType) :
    sPortId = hlk_stc.stc_get_port_id(sPortType)
    return sPortId

def llk_traffic_stc_set_mesure_result_type(sType):
    hlk_stc.stc_set_mesure_result_type(sType)

def llk_traffic_stc_get_measure_result_type():
    sType = hlk_stc.stc_get_measure_result_type()
    return sType

def llk_device_stc_ipv4host_create(**kwargs) :
 
    kwargs = dict(map(lambda (k,v): (str(k).lower(), v), kwargs.iteritems()))

    assert (kwargs.has_key('lif_side')), "Parameter 'lif_side' must be inputted!"
    assert (kwargs.has_key('device_name')), "Parameter 'device_name' must be inputted!"

    lif_side    = kwargs.pop('lif_side','')
    device_name = kwargs.pop('device_name','')

    oDevice = hlk_stc.stc_create_device_ipv4(lif_side,device_name,**kwargs)

    return oDevice

def llk_device_stc_ipv4host_operate(**kwargs) :
    hlk_stc.stc_operate_device_ipv4(**kwargs)

def llk_device_stc_ieee1588v2_create(**kwargs) :
    kwargs = dict(map(lambda (k,v): (str(k).lower(), v), kwargs.iteritems()))

    assert (kwargs.has_key('lif_side')), "Parameter 'lif_side' must be inputted!"
    assert (kwargs.has_key('device_name')), "Parameter 'device_name' must be inputted!"

    lif_side    = kwargs.pop('lif_side','')
    device_name = kwargs.pop('device_name','')

    return hlk_stc.stc_create_device_ieee1588v2(lif_side,device_name,**kwargs)

def llk_device_stc_pppoeserver_create(**kwargs) :
    kwargs = dict(map(lambda (k,v): (str(k).lower(), v), kwargs.iteritems()))

    assert (kwargs.has_key('lif_side')), "Parameter 'lif_side' must be inputted!"
    assert (kwargs.has_key('device_name')), "Parameter 'device_name' must be inputted!"

    lif_side    = kwargs.pop('lif_side','')
    device_name = kwargs.pop('device_name','')

    return hlk_stc.stc_create_pppoe_server(lif_side,device_name,**kwargs)

def llk_device_stc_pppoeclient_create(**kwargs) :
    kwargs = dict(map(lambda (k,v): (str(k).lower(), v), kwargs.iteritems()))

    assert (kwargs.has_key('lif_side')), "Parameter 'lif_side' must be inputted!"
    assert (kwargs.has_key('device_name')), "Parameter 'device_name' must be inputted!"

    lif_side    = kwargs.pop('lif_side','')
    device_name = kwargs.pop('device_name','')

    return hlk_stc.stc_create_pppoe_client(lif_side,device_name,**kwargs)

def llk_device_stc_pppoeserver_verify(**kwargs) :
    kwargs = dict(map(lambda (k,v): (str(k).lower(), v), kwargs.iteritems()))

    assert (kwargs.has_key('device_name')), "Parameter 'device_name' must be inputted!"
    assert (kwargs.has_key('expect_state')), "Parameter 'expect_state' must be inputted!"

    device_name = kwargs.pop('device_name')
    expect_state = kwargs.pop('expect_state')
    session_name = kwargs.pop('session_name','first_stc_session')
    check_time  = kwargs.pop('check_time',1)

    hlk_stc.stc_verify_pppoe_server_state(device_name,expect_state,session_name=session_name,check_time=check_time)

def llk_device_stc_pppoeclient_verify(**kwargs) :
    kwargs = dict(map(lambda (k,v): (str(k).lower(), v), kwargs.iteritems()))

    assert (kwargs.has_key('device_name')), "Parameter 'device_name' must be inputted!"
    assert (kwargs.has_key('expect_state')), "Parameter 'expect_state' must be inputted!"

    device_name = kwargs.pop('device_name')
    expect_state = kwargs.pop('expect_state')
    session_name = kwargs.pop('session_name','first_stc_session')
    check_time  = kwargs.pop('check_time',1)

    hlk_stc.stc_verify_pppoe_client_state(device_name,expect_state,session_name=session_name,check_time=check_time)

def llk_device_stc_pppoeserver_operate(**kwargs) :
    kwargs = dict(map(lambda (k,v): (str(k).lower(), v), kwargs.iteritems()))

    assert (kwargs.has_key('operation')), "Parameter 'operation' must be inputted!"

    assert (kwargs.has_key('device_name')), "Parameter 'device_name' must be inputted!"

    operation    = kwargs.pop('operation','')
    device_name = kwargs.pop('device_name','')

    hlk_stc.stc_operate_pppoe_server(device=device_name,operation=operation,**kwargs)

def llk_device_stc_pppoeclient_operate(**kwargs) :
    kwargs = dict(map(lambda (k,v): (str(k).lower(), v), kwargs.iteritems()))

    assert (kwargs.has_key('operation')), "Parameter 'operation' must be inputted!"

    assert (kwargs.has_key('device_name')), "Parameter 'device_name' must be inputted!"

    operation    = kwargs.pop('operation','')
    device_name = kwargs.pop('device_name','')

    hlk_stc.stc_operate_pppoe_client(device=device_name,operation=operation,**kwargs)

def llk_device_stc_pppoeserver_delete(**kwargs) :
    kwargs = dict(map(lambda (k,v): (str(k).lower(), v), kwargs.iteritems()))
    assert (kwargs.has_key('device_name')), "Parameter 'device_name' must be inputted!"

    session_name = kwargs.pop('session_name','first_stc_session')
    device_name = kwargs.pop('device_name')

    hlk_stc.stc_delete_device(device_name,session_name=session_name)


def llk_device_stc_pppoeclient_delete(**kwargs) :
    kwargs = dict(map(lambda (k,v): (str(k).lower(), v), kwargs.iteritems()))
    assert (kwargs.has_key('device_name')), "Parameter 'device_name' must be inputted!"

    session_name = kwargs.pop('session_name','first_stc_session')
    device_name = kwargs.pop('device_name')

    hlk_stc.stc_delete_device(device_name,session_name=session_name)

def llk_device_stc_igmphost_create(**kwargs) :
    kwargs = dict(map(lambda (k,v): (str(k).lower(), v), kwargs.iteritems()))

    assert (kwargs.has_key('lif_side')), "Parameter 'lif_side' must be inputted!"
    assert (kwargs.has_key('device_name')), "Parameter 'device_name' must be inputted!"

    lif_side    = kwargs.pop('lif_side','')
    device_name = kwargs.pop('device_name','')

    return hlk_stc.stc_create_igmp_host(lif_side,device_name,**kwargs)

def llk_device_stc_igmphost_verify(**kwargs) :
    kwargs = dict(map(lambda (k,v): (str(k).lower(), v), kwargs.iteritems()))

    assert (kwargs.has_key('device_name')), "Parameter 'device_name' must be inputted!"
    assert (kwargs.has_key('expect_state')), "Parameter 'expect_state' must be inputted!"

    device_name = kwargs.pop('device_name')
    expect_state = kwargs.pop('expect_state')
    session_name = kwargs.pop('session_name','first_stc_session')
    check_time  = kwargs.pop('check_time',1)

    hlk_stc.stc_verify_igmp_host_state(device_name,expect_state,session_name=session_name,check_time=check_time)

def llk_device_stc_igmphost_delete(**kwargs) :
    kwargs = dict(map(lambda (k,v): (str(k).lower(), v), kwargs.iteritems()))
    assert (kwargs.has_key('device_name')), "Parameter 'device_name' must be inputted!"

    session_name = kwargs.pop('session_name','first_stc_session')
    device_name = kwargs.pop('device_name')

    hlk_stc.stc_delete_device(device_name,session_name=session_name)


def llk_device_stc_igmphost_operate(**kwargs) :
    kwargs = dict(map(lambda (k,v): (str(k).lower(), v), kwargs.iteritems()))

    assert (kwargs.has_key('operation')), "Parameter 'operation' must be inputted!"

    assert (kwargs.has_key('device_name')), "Parameter 'device_name' must be inputted!"

    operation    = kwargs.pop('operation','')
    device_name = kwargs.pop('device_name','')

    hlk_stc.stc_operate_igmp_host(device=device_name,operation=operation,**kwargs)

def llk_device_stc_ieee1588v2_modify(**kwargs) :
    kwargs = dict(map(lambda (k,v): (str(k).lower(), v), kwargs.iteritems()))

    assert (kwargs.has_key('device_name')), "Parameter 'device_name' must be inputted!"

    device_name = kwargs.pop('device_name','')

    hlk_stc.stc_modify_device_ieee1588v2(device_name,**kwargs)

def llk_device_stc_ieee1588v2_verify(**kwargs) :
    kwargs = dict(map(lambda (k,v): (str(k).lower(), v), kwargs.iteritems()))

    assert (kwargs.has_key('device_name')), "Parameter 'device_name' must be inputted!"
    assert (kwargs.has_key('expect_state')), "Parameter 'expect_state' must be inputted!"

    device_name = kwargs.pop('device_name')
    expect_state = kwargs.pop('expect_state')
    session_name = kwargs.pop('session_name','first_stc_session')
    check_time  = kwargs.pop('check_time',1)

    hlk_stc.stc_retrieve_device_ieee1588v2(device_name,expect_state,session_name=session_name,check_time=check_time)

def llk_device_stc_ieee1588v2_delete(**kwargs) :
    kwargs = dict(map(lambda (k,v): (str(k).lower(), v), kwargs.iteritems()))
    assert (kwargs.has_key('device_name')), "Parameter 'device_name' must be inputted!"

    session_name = kwargs.pop('session_name','first_stc_session')
    device_name = kwargs.pop('device_name')

    hlk_stc.stc_delete_device(device_name,session_name=session_name) 


def llk_device_stc_dhcpserver_create(**kwargs):

    kwargs = dict(map(lambda (k,v): (str(k).lower(), v), kwargs.iteritems()))

    assert (kwargs.has_key('lif_side')), "Parameter 'lif_side' must be inputted!"
    assert (kwargs.has_key('device_name')), "Parameter 'device_name' must be inputted!"

    lif_side    = kwargs.pop('lif_side','')
    device_name = kwargs.pop('device_name','')

    oDevice = hlk_stc.stc_create_dhcp_server(lif_side,device_name,**kwargs)

    return oDevice


def llk_device_stc_dhcpv6server_create(**kwargs):

    kwargs = dict(map(lambda (k,v): (str(k).lower(), v), kwargs.iteritems()))

    assert (kwargs.has_key('lif_side')), "Parameter 'lif_side' must be inputted!"
    assert (kwargs.has_key('device_name')), "Parameter 'device_name' must be inputted!"

    lif_side    = kwargs.pop('lif_side','')
    device_name = kwargs.pop('device_name','')

    oDevice = hlk_stc.stc_create_dhcpv6_server(lif_side,device_name,**kwargs)

    return oDevice

def llk_device_stc_dhcpclient_create(**kwargs):

    kwargs = dict(map(lambda (k,v): (str(k).lower(), v), kwargs.iteritems()))

    assert (kwargs.has_key('lif_side')), "Parameter 'lif_side' must be inputted!"
    assert (kwargs.has_key('device_name')), "Parameter 'device_name' must be inputted!"

    lif_side    = kwargs.pop('lif_side','')
    device_name = kwargs.pop('device_name','')

    oDevice = hlk_stc.stc_create_dhcp_client(lif_side,device_name,**kwargs)

    return oDevice

def llk_device_stc_dhcpv6client_create(**kwargs):

    kwargs = dict(map(lambda (k,v): (str(k).lower(), v), kwargs.iteritems()))

    assert (kwargs.has_key('lif_side')), "Parameter 'lif_side' must be inputted!"
    assert (kwargs.has_key('device_name')), "Parameter 'device_name' must be inputted!"

    lif_side    = kwargs.pop('lif_side','')
    device_name = kwargs.pop('device_name','')

    oDevice = hlk_stc.stc_create_dhcpv6_client(lif_side,device_name,**kwargs)

    return oDevice

def llk_device_stc_dhcpserver_operate(**kwargs) :

    kwargs = dict(map(lambda (k,v): (str(k).lower(), v), kwargs.iteritems()))

    assert (kwargs.has_key('operation')), "Parameter 'operation' must be inputted!"

    assert (kwargs.has_key('device_name')), "Parameter 'device_name' must be inputted!"

    operation    = kwargs.pop('operation','')
    device_name = kwargs.pop('device_name','')

    hlk_stc.stc_operate_dhcp_server(device=device_name,operation=operation,**kwargs)

def llk_device_stc_dhcpv6server_operate(**kwargs) :

    kwargs = dict(map(lambda (k,v): (str(k).lower(), v), kwargs.iteritems()))

    assert (kwargs.has_key('operation')), "Parameter 'operation' must be inputted!"

    assert (kwargs.has_key('device_name')), "Parameter 'device_name' must be inputted!"

    operation    = kwargs.pop('operation','')
    device_name = kwargs.pop('device_name','')

    hlk_stc.stc_operate_dhcpv6_server(device=device_name,operation=operation,**kwargs)

def llk_device_stc_dhcpclient_operate(**kwargs) :

    kwargs = dict(map(lambda (k,v): (str(k).lower(), v), kwargs.iteritems()))

    assert (kwargs.has_key('operation')), "Parameter 'operation' must be inputted!"

    assert (kwargs.has_key('device_name')), "Parameter 'device_name' must be inputted!"

    operation    = kwargs.pop('operation','')
    device_name = kwargs.pop('device_name','')

    hlk_stc.stc_operate_dhcp_client(device=device_name,operation=operation,**kwargs)

def llk_device_stc_dhcpv6client_operate(**kwargs) :

    kwargs = dict(map(lambda (k,v): (str(k).lower(), v), kwargs.iteritems()))

    assert (kwargs.has_key('operation')), "Parameter 'operation' must be inputted!"

    assert (kwargs.has_key('device_name')), "Parameter 'device_name' must be inputted!"

    operation    = kwargs.pop('operation','')
    device_name = kwargs.pop('device_name','')

    hlk_stc.stc_operate_dhcpv6_client(device=device_name,operation=operation,**kwargs)

def llk_device_stc_dhcpserver_verify_state(**kwargs) :
    kwargs = dict(map(lambda (k,v): (str(k).lower(), v), kwargs.iteritems()))

    assert (kwargs.has_key('device_name')), "Parameter 'device_name' must be inputted!"
    assert (kwargs.has_key('expect_state')), "Parameter 'expect_state' must be inputted!"

    device_name = kwargs.pop('device_name')
    expect_state = kwargs.pop('expect_state')
    session_name = kwargs.pop('session_name','first_stc_session')
    check_time  = kwargs.pop('check_time',1)

    hlk_stc.stc_verify_dhcp_server_state(device_name,expect_state,session_name=session_name,check_time=check_time)

def llk_device_stc_dhcpclient_verify_state(**kwargs) :
    kwargs = dict(map(lambda (k,v): (str(k).lower(), v), kwargs.iteritems()))

    assert (kwargs.has_key('device_name')), "Parameter 'device_name' must be inputted!"
    assert (kwargs.has_key('expect_state')), "Parameter 'expect_state' must be inputted!"

    device_name = kwargs.pop('device_name')
    expect_state = kwargs.pop('expect_state')
    session_name = kwargs.pop('session_name','first_stc_session')
    check_time  = kwargs.pop('check_time',1)

    hlk_stc.stc_verify_dhcp_client_state(device_name,expect_state,session_name=session_name,check_time=check_time)


def llk_device_stc_dhcpv6server_verify(**kwargs) :
    kwargs = dict(map(lambda (k,v): (str(k).lower(), v), kwargs.iteritems()))

    assert (kwargs.has_key('device_name')), "Parameter 'device_name' must be inputted!"
    assert (kwargs.has_key('expect_state')), "Parameter 'expect_state' must be inputted!"

    device_name = kwargs.pop('device_name')
    expect_state = kwargs.pop('expect_state')
    session_name = kwargs.pop('session_name','first_stc_session')
    check_time  = kwargs.pop('check_time',1)

    hlk_stc.stc_verify_dhcpv6_server_state(device_name,expect_state,session_name=session_name,check_time=check_time)

def llk_device_stc_dhcpv6client_verify(**kwargs) :
    kwargs = dict(map(lambda (k,v): (str(k).lower(), v), kwargs.iteritems()))

    assert (kwargs.has_key('device_name')), "Parameter 'device_name' must be inputted!"
    assert (kwargs.has_key('expect_state')), "Parameter 'expect_state' must be inputted!"

    device_name = kwargs.pop('device_name')
    expect_state = kwargs.pop('expect_state')
    session_name = kwargs.pop('session_name','first_stc_session')
    check_time  = kwargs.pop('check_time',1)

    hlk_stc.stc_verify_dhcpv6_client_state(device_name,expect_state,session_name=session_name,check_time=check_time)
    hlk_stc.stc_verify_dhcpv6_client_pdstate(device_name,expect_state,session_name=session_name,check_time=check_time)


def llk_device_stc_dhcpserver_delete(**kwargs) :
    kwargs = dict(map(lambda (k,v): (str(k).lower(), v), kwargs.iteritems()))
    assert (kwargs.has_key('device_name')), "Parameter 'device_name' must be inputted!"

    session_name = kwargs.pop('session_name','first_stc_session')
    device_name = kwargs.pop('device_name')

    hlk_stc.stc_delete_device(device_name,session_name=session_name)

def llk_device_stc_dhcpclient_delete(**kwargs) :
    kwargs = dict(map(lambda (k,v): (str(k).lower(), v), kwargs.iteritems()))
    assert (kwargs.has_key('device_name')), "Parameter 'device_name' must be inputted!"

    session_name = kwargs.pop('session_name','first_stc_session')
    device_name = kwargs.pop('device_name')

    hlk_stc.stc_delete_device(device_name,session_name=session_name)

def llk_device_stc_dhcpv6server_delete(**kwargs) :
    kwargs = dict(map(lambda (k,v): (str(k).lower(), v), kwargs.iteritems()))
    assert (kwargs.has_key('device_name')), "Parameter 'device_name' must be inputted!"

    session_name = kwargs.pop('session_name','first_stc_session')
    device_name = kwargs.pop('device_name')

    hlk_stc.stc_delete_device(device_name,session_name=session_name)

def llk_device_stc_dhcpv6client_delete(**kwargs) :
    kwargs = dict(map(lambda (k,v): (str(k).lower(), v), kwargs.iteritems()))
    assert (kwargs.has_key('device_name')), "Parameter 'device_name' must be inputted!"

    session_name = kwargs.pop('session_name','first_stc_session')
    device_name = kwargs.pop('device_name')

    hlk_stc.stc_delete_device(device_name,session_name=session_name)

def llk_device_stc_8021x_create(**kwargs):
    kwargs = dict(map(lambda (k,v): (str(k).lower(), v), kwargs.iteritems()))

    assert (kwargs.has_key('lif_side')), "Parameter 'lif_side' must be inputted!"
    assert (kwargs.has_key('device_name')), "Parameter 'device_name' must be inputted!"

    lif_side    = kwargs.pop('lif_side','')
    device_name = kwargs.pop('device_name','')

    oDevice = hlk_stc.stc_create_8021x_auth(lif_side,device_name,**kwargs)

    return oDevice

def llk_device_stc_8021x_verify(**kwargs):
    kwargs = dict(map(lambda (k,v): (str(k).lower(), v), kwargs.iteritems()))

    assert (kwargs.has_key('device_name')), "Parameter 'device_name' must be inputted!"
    assert (kwargs.has_key('expect_state')), "Parameter 'expect_state' must be inputted!"

    device_name = kwargs.pop('device_name')
    expect_state = kwargs.pop('expect_state')
    session_name = kwargs.pop('session_name','first_stc_session')
    check_time  = kwargs.pop('check_time',1)

    hlk_stc.stc_verify_8021x_auth_state(device_name,expect_state,session_name=session_name,check_time=check_time)

def llk_device_stc_8021x_operate(**kwargs) :

    kwargs = dict(map(lambda (k,v): (str(k).lower(), v), kwargs.iteritems()))

    assert (kwargs.has_key('operation')), "Parameter 'operation' must be inputted!"

    assert (kwargs.has_key('device_name')), "Parameter 'device_name' must be inputted!"

    operation    = kwargs.pop('operation','')
    device_name = kwargs.pop('device_name','')

    hlk_stc.stc_operate_8021x_auth(device=device_name,operation=operation,**kwargs)

def llk_device_stc_8021x_delete(**kwargs) :
    kwargs = dict(map(lambda (k,v): (str(k).lower(), v), kwargs.iteritems()))
    assert (kwargs.has_key('device_name')), "Parameter 'device_name' must be inputted!"

    session_name = kwargs.pop('session_name','first_stc_session')
    device_name = kwargs.pop('device_name')

    hlk_stc.stc_delete_device(device_name,session_name=session_name)


def llk_device_stc_ospfv2_create(**kwargs):
    kwargs = dict(map(lambda (k,v): (str(k).lower(), v), kwargs.iteritems()))

    assert (kwargs.has_key('lif_side')), "Parameter 'lif_side' must be inputted!"
    assert (kwargs.has_key('device_name')), "Parameter 'device_name' must be inputted!"

    lif_side    = kwargs.pop('lif_side','')
    device_name = kwargs.pop('device_name','')

    oDevice = hlk_stc.stc_create_ospfv2_router(lif_side,device_name,**kwargs)

    return oDevice

def llk_device_stc_ospfv3_create(**kwargs):
    kwargs = dict(map(lambda (k,v): (str(k).lower(), v), kwargs.iteritems()))

    assert (kwargs.has_key('lif_side')), "Parameter 'lif_side' must be inputted!"
    assert (kwargs.has_key('device_name')), "Parameter 'device_name' must be inputted!"

    lif_side    = kwargs.pop('lif_side','')
    device_name = kwargs.pop('device_name','')

    oDevice = hlk_stc.stc_create_ospfv3_router(lif_side,device_name,**kwargs)

    return oDevice

def llk_device_stc_ospfv2_verify(**kwargs):
    kwargs = dict(map(lambda (k,v): (str(k).lower(), v), kwargs.iteritems()))

    assert (kwargs.has_key('device_name')), "Parameter 'device_name' must be inputted!"
    assert (kwargs.has_key('expect_state')), "Parameter 'expect_state' must be inputted!"

    device_name = kwargs.pop('device_name')
    expect_state = kwargs.pop('expect_state')
    session_name = kwargs.pop('session_name','first_stc_session')
    check_time  = kwargs.pop('check_time',1)

    hlk_stc.stc_verify_ospfv2_router_state(device_name,expect_state,session_name=session_name,check_time=check_time)


def llk_device_stc_ospfv3_verify(**kwargs):
    kwargs = dict(map(lambda (k,v): (str(k).lower(), v), kwargs.iteritems()))

    assert (kwargs.has_key('device_name')), "Parameter 'device_name' must be inputted!"
    assert (kwargs.has_key('expect_state')), "Parameter 'expect_state' must be inputted!"

    device_name = kwargs.pop('device_name')
    expect_state = kwargs.pop('expect_state')
    session_name = kwargs.pop('session_name','first_stc_session')
    check_time  = kwargs.pop('check_time',1)

    hlk_stc.stc_verify_ospfv3_router_state(device_name,expect_state,session_name=session_name,check_time=check_time)

def llk_device_stc_ospfv2_operate(**kwargs):
    kwargs = dict(map(lambda (k,v): (str(k).lower(), v), kwargs.iteritems()))

    assert (kwargs.has_key('operation')), "Parameter 'operation' must be inputted!"

    assert (kwargs.has_key('device_name')), "Parameter 'device_name' must be inputted!"

    operation    = kwargs.pop('operation','')
    device_name = kwargs.pop('device_name','')

    hlk_stc.stc_operate_ospfv2_router(device=device_name,operation=operation,**kwargs)


def llk_device_stc_ospfv3_operate(**kwargs):
    kwargs = dict(map(lambda (k,v): (str(k).lower(), v), kwargs.iteritems()))

    assert (kwargs.has_key('operation')), "Parameter 'operation' must be inputted!"

    assert (kwargs.has_key('device_name')), "Parameter 'device_name' must be inputted!"

    operation    = kwargs.pop('operation','')
    device_name = kwargs.pop('device_name','')

    hlk_stc.stc_operate_ospfv3_router(device=device_name,operation=operation,**kwargs)

def llk_device_stc_ospfv2_delete(**kwargs):
    kwargs = dict(map(lambda (k,v): (str(k).lower(), v), kwargs.iteritems()))
    assert (kwargs.has_key('device_name')), "Parameter 'device_name' must be inputted!"

    session_name = kwargs.pop('session_name','first_stc_session')
    device_name = kwargs.pop('device_name')

    hlk_stc.stc_delete_device(device_name,session_name=session_name)


def llk_device_stc_ospfv3_delete(**kwargs):
    kwargs = dict(map(lambda (k,v): (str(k).lower(), v), kwargs.iteritems()))
    assert (kwargs.has_key('device_name')), "Parameter 'device_name' must be inputted!"

    session_name = kwargs.pop('session_name','first_stc_session')
    device_name = kwargs.pop('device_name')

    hlk_stc.stc_delete_device(device_name,session_name=session_name)

def llk_device_stc_rip_create(**kwargs):
    kwargs = dict(map(lambda (k,v): (str(k).lower(), v), kwargs.iteritems()))

    assert (kwargs.has_key('lif_side')), "Parameter 'lif_side' must be inputted!"
    assert (kwargs.has_key('device_name')), "Parameter 'device_name' must be inputted!"

    lif_side    = kwargs.pop('lif_side','')
    device_name = kwargs.pop('device_name','')

    oDevice = hlk_stc.stc_create_rip_router(lif_side,device_name,**kwargs)
    return oDevice


def llk_device_stc_bgp_create(**kwargs):
    kwargs = dict(map(lambda (k,v): (str(k).lower(), v), kwargs.iteritems()))

    assert (kwargs.has_key('lif_side')), "Parameter 'lif_side' must be inputted!"
    assert (kwargs.has_key('device_name')), "Parameter 'device_name' must be inputted!"

    lif_side    = kwargs.pop('lif_side','')
    device_name = kwargs.pop('device_name','')

    oDevice = hlk_stc.stc_create_bgp_router(lif_side,device_name,**kwargs)
    return oDevice


def llk_device_stc_rip_verify(**kwargs):
    kwargs = dict(map(lambda (k,v): (str(k).lower(), v), kwargs.iteritems()))

    assert (kwargs.has_key('device_name')), "Parameter 'device_name' must be inputted!"
    assert (kwargs.has_key('expect_state')), "Parameter 'expect_state' must be inputted!"

    device_name = kwargs.pop('device_name')
    expect_state = kwargs.pop('expect_state')
    session_name = kwargs.pop('session_name','first_stc_session')
    check_time  = kwargs.pop('check_time',1)

    hlk_stc.stc_verify_rip_router_state(device_name,expect_state,session_name=session_name,check_time=check_time)

def llk_device_stc_bgp_verify(**kwargs):
    kwargs = dict(map(lambda (k,v): (str(k).lower(), v), kwargs.iteritems()))

    assert (kwargs.has_key('device_name')), "Parameter 'device_name' must be inputted!"
    assert (kwargs.has_key('expect_state')), "Parameter 'expect_state' must be inputted!"

    device_name = kwargs.pop('device_name')
    expect_state = kwargs.pop('expect_state')
    session_name = kwargs.pop('session_name','first_stc_session')
    check_time  = kwargs.pop('check_time',1)

    hlk_stc.stc_verify_bgp_router_state(device_name,expect_state,session_name=session_name,check_time=check_time)


def llk_device_stc_rip_operate(**kwargs):
    kwargs = dict(map(lambda (k,v): (str(k).lower(), v), kwargs.iteritems()))

    assert (kwargs.has_key('operation')), "Parameter 'operation' must be inputted!"

    assert (kwargs.has_key('device_name')), "Parameter 'device_name' must be inputted!"

    operation    = kwargs.pop('operation','')
    device_name = kwargs.pop('device_name','')

    hlk_stc.stc_operate_rip_router(device=device_name,operation=operation,**kwargs)

def llk_device_stc_bgp_operate(**kwargs):
    kwargs = dict(map(lambda (k,v): (str(k).lower(), v), kwargs.iteritems()))

    assert (kwargs.has_key('operation')), "Parameter 'operation' must be inputted!"

    assert (kwargs.has_key('device_name')), "Parameter 'device_name' must be inputted!"

    operation    = kwargs.pop('operation','')
    device_name = kwargs.pop('device_name','')

    hlk_stc.stc_operate_bgp_router(device=device_name,operation=operation,**kwargs)

def llk_device_stc_rip_delete(**kwargs):
    kwargs = dict(map(lambda (k,v): (str(k).lower(), v), kwargs.iteritems()))
    assert (kwargs.has_key('device_name')), "Parameter 'device_name' must be inputted!"

    session_name = kwargs.pop('session_name','first_stc_session')
    device_name = kwargs.pop('device_name')

    hlk_stc.stc_delete_device(device_name,session_name=session_name)


def llk_device_stc_bgp_delete(**kwargs):
    kwargs = dict(map(lambda (k,v): (str(k).lower(), v), kwargs.iteritems()))
    assert (kwargs.has_key('device_name')), "Parameter 'device_name' must be inputted!"

    session_name = kwargs.pop('session_name','first_stc_session')
    device_name = kwargs.pop('device_name')

    hlk_stc.stc_delete_device(device_name,session_name=session_name)


