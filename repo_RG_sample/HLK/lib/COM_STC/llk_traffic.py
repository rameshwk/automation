import re,os
import xml.etree.ElementTree as ET
from robot.libraries.BuiltIn import BuiltIn
from robot.api import logger
# Path to tshark exe file
#-----modified----------
sTsharkExe = "/usr/sbin/tshark"
from llk_log import *

try:
    TRAF_OUTPUT_DIR = BuiltIn().replace_variables('${OUTPUTDIR}') + "/TRAFFIC"
except Exception as inst:
    TRAF_OUTPUT_DIR = "/tmp/TRAFFIC"

def llk_traffic_pcap_to_stat(sPcapFileName,**dVerifyParam):
    """
    Function to parse the pcap file and get statstics result

    Args:
        sPcapFilename: Name of the pcap file to be parsed
        dVerifyParam: a dict of validation parameters.
    Return:
        (int) the count of matched packet

    Example:
        llk_traffic_pcap_to_stat("capture.pcap",ip.dst='192.168.0.1',eth.dst='00:00:00:00:00:01')
    """

    iPktCount = 0
    llk_log_begin_proc()
    try: 
        sFilePrefix = sPcapFilename[:-5]
        sStatFilename = sFilePrefix + ".stat"
    except Exception as inst:
        # need test
        s = 'a'

    if sPdmlFilepath == "":
        sPdmlFilenamepath = os.path.join(TRAF_OUTPUT_DIR,sPdmlFilename)
    else:
        if not os.path.exists(sPdmlFilepath):
            os.makedirs(sPdmlFilepath)
        sPdmlFilenamepath = os.path.join(sPdmlFilepath,sPdmlFilename)

    sCmd = sTsharkExe + " -r " + os.path.join(TRAF_OUTPUT_DIR,sPcapFilename) + " -T pdml > " + sPdmlFilenamepath

    llk_log_debug("llk OS EXEC : %s" % sCmd)
    llk_log_info("log OS EXEC : %s" % sCmd)
    print("print OS EXEC : %s" % sCmd)
    #os.system(sTsharkExe + " -r " + os.path.join(TRAF_OUTPUT_DIR,sPcapFilename) + " -T pdml > " + sPdmlFilenamepath)
    os.system(sCmd)

    llk_log_end_proc()


    return iPktCount


def llk_traffic_pcap_to_pdml(sPcapFilename, sPdmlFilepath="",ta_protocol=''):
    """
    Function to parse the pcap file and generate a pdml file.

    Args:
        sPcapFilename: Name of the pcap file to be parsed
        sPdmlFilepath: The folder path where the parsed pcap files i.e. pdml files are to be stored
    Return:
        N/A

    Example:
        llk_traffic_pcap_to_pdml("capture.pcap",/home1/user/robot-mso/LOG)
    """

    llk_log_begin_proc()

    sFilePrefix = sPcapFilename[:-5]
    sPdmlFilename = sFilePrefix + ".pdml"

    if sPdmlFilepath == "":
        sPdmlFilenamepath = os.path.join(sPdmlFilepath,sPdmlFilename)
    else:
        if not os.path.exists(sPdmlFilepath):
            os.makedirs(sPdmlFilepath)
        sPdmlFilenamepath = os.path.join(sPdmlFilepath,sPdmlFilename)

    sFilterProt = ''
    if ta_protocol :
        sFilterProt = ' -R ' + ta_protocol + ''

    sCmd = sTsharkExe + " -r " + os.path.join(sPdmlFilepath,sPcapFilename) + sFilterProt + " -T pdml > " + sPdmlFilenamepath

    llk_log_debug("llk OS EXEC : %s" % sCmd)
    llk_log_info("log OS EXEC : %s" % sCmd)
    print("print OS EXEC : %s" % sCmd)
    #os.system(sTsharkExe + " -r " + os.path.join(sPdmlFilepath,sPcapFilename) + " -T pdml > " + sPdmlFilenamepath)
    os.system(sCmd)

    llk_log_end_proc()

def _get_acronym(sText) :
    sAcronym = ''
    lContent = sText.split(" ")
    for each in lContent : 
        sAcronym = sAcronym + each[0].upper()

    return sAcronym.lower()

def llk_traffic_parse_pdml(sPdmlFilename, sPdmlFilepath=""):
    """
    Function to parse the pcap file and return a list of dictionaries each corresponding to a packet.

    Args:
        sPcapFilename: Name of the pcap file to be parsed
        sPdmlFilepath: The folder path where the parsed pcap files i.e. pdml files are to be stored
    Return:
        ldParsePkt : List of dictionaries with each dictionary containing the key value paris as fields parsed from the
                    packet. This is explained in detail below.

        The returned list consists of dictionaries each corresponding to a packet parsed.
        The fields parsed from the packet are stored as key value paris in the dictionary.

        The 'key' holds the attribute value 'name' of the field. If the field is nested, then the attribute value of 'name'
        will be the string concatenation of parent field's attribute value 'name' and child fields attribute 'name' with the
        separator as '__'(double underscore). This is done since some keys might have the same attribute value of 'name'.
        The 'value' holds the attribute value 'show'.

        Generally, the key value pair would be 'name': 'show'

        Example for the fields:
        Single field:
        "<field name="arp.opcode" showname="Opcode: reply (2)" size="2" pos="20" show="2" value="0002"/>"
        In this case ,the key value pair stored is ---> 'arp.opcode': '2'

        Nested field:
        <field name="eth.fcs" showname="Frame check sequence: 0x23dfff92 [correct]" size="4" pos="124" show="601882514" value="23dfff92">
            <field name="eth.fcs_good" showname="FCS Good: True" size="4" pos="124" show="1" value="23dfff92"/>
        In this case, the first line is the parent field and the second line is the nested/child field.

        The key value pair for the parent field is ---> 'eth.fcs': '601882514'
        The key value pair for the child field is ---> 'eth.fcs__eth.fcs_good': '1'

    Example for the usage of function:
        llk_traffic_parse_pcap_to_pdml("capture.pdml",/home1/user/robot-mso/LOG)
    """

    llk_log_begin_proc()

    #list of dictionaries each corresponding to a parsed packet
    ldParsePkt = []
    #dictionary containing the parsed packet fields
    dParsePkt = {}
    #Current packet number that is being parsed
    iCurrPktCnt = 0
    sPdmlFilenamepath = ""

    if sPdmlFilepath == "":
        sPdmlFilenamepath = os.path.join(TRAF_OUTPUT_DIR,sPdmlFilename)
    else:
        if os.path.exists(sPdmlFilepath):
            sPdmlFilenamepath = os.path.join(sPdmlFilepath,sPdmlFilename)
        else:
            raise Exception("The specifed path does not exist!! Please specify the correct path to the pdml file")

    #Read the pdml file to create a XML tree
    try:
        sXmlTree = ET.parse(sPdmlFilenamepath)
        sXmlRoot = sXmlTree.getroot()
    except IOError, e:
        raise Exception("PDML file could not be opened!!")

    for sPacket in sXmlRoot.iter("packet"):
        sFrameNumber = sPacket.find("./proto[@name='frame']/field[@name='frame.number']").attrib["show"]
        #llk_log_trace("Frame number " + sFrameNumber + " :" + sPdmlFilename)
        #llk_log_trace(ET.tostring(sPacket, method="html"))

    # Parse the captured packets in the pdml file
    # Extracting all the 'packet' fields from the pdml file
    if sXmlRoot.findall('packet') is not None:
        #llk_log_info("Parsing packets")
        for packet in sXmlRoot.findall('packet'):
            iCurrPktCnt += 1
            iVlanCnt = 1
            #Extracting all the 'fields' and nested fields to be parsed from the packet
            sFiledPath = "./proto/field"
            while packet.findall(sFiledPath):
                #llk_log_info("Parsing the fields inside the packet %d" % iCurrPktCnt)

                for field in packet.findall(sFiledPath):
                    if field.find('field') is not None:                    
                        #parent field attribute values
                        tag_attr = field.get('name')
                        fieldval_show = field.get('show')
                        parent_name = field.get('parent_name')
                        # if field name is ''
                        if not tag_attr :
                            m = re.search('(.*):',fieldval_show)
                            if m :
                                tag_attr = m.group(1)
                                tag_attr = tag_attr.replace(' ','_').lower()
                            else:
                                tag_attr = _get_acronym(fieldval_show)
                       
                            # remove the left of = 
                            m = re.search("=",tag_attr)
                            if m :
                                left,right = tag_attr.split("=")
                                tag_attr = right

                            # add '!!' to mark it as empty name which is parsed by tshark
                            tag_attr = '!!' + tag_attr

                        #fieldval_value = field.get('value')
                        #dParsePkt[tag_attr] = list((fieldval_show,fieldval_value))
                        
                        # if tag_attr exist, give it a array index
                        if dParsePkt.has_key(tag_attr) :
                            logger.trace("tag_attr exist:%s,value:%s" % (tag_attr,fieldval_show))
                            index = 0
                            key_name = tag_attr
                            # get the current index
                            while dParsePkt.has_key(key_name) :
                                index = index + 1
                                # set while checkd key_name
                                key_name = '[' + str(index) +  ']' + tag_attr

                                if index == 1 :
                                    #if tag_attr exist, it means it should be a member of list. the first child with the same name of list should has parent.
                                    # research the all children of parent node to find out whose name equal tag_attr
                                    for uncle_field in packet.findall(sFiledPath) :
                                        if uncle_field.get('name') == tag_attr :
                                            for cousin_field in uncle_field.findall('field') :  
                                                cousin_field.set('parent_name',tag_attr)                                

                            # quit while, and find index, then set new tag_attr
                            tag_attr = '[' + str(index) + ']' + tag_attr 

                            # if tag_attr exist, it means it should be a member of list. it'a all children should keep it's path
                            for child_field in field.findall('field'):
                                child_field.set('parent_name',tag_attr)                           
                                                               

                        # update the filed name in xml, because it maybe used for the compare of checking tag_attr exist
                        field.set('name',tag_attr)

                        # add this filed information in returned dict dParsePkt
                        dParsePkt[tag_attr] = fieldval_show

                    else:
                        #Extracting the normal fields
                        field_key = field.get('name')
                        fieldval_show = field.get('show')
                        parent_name = field.get('parent_name')                     

                        # if field name is ''
                        if not field_key :
                            m = re.search('(.*):',fieldval_show)
                            if m :
                                field_key = m.group(1)
                                field_key = field_key.replace(' ','_').lower()
                            else:
                                field_key = _get_acronym(fieldval_show)

                            # remove the left of = 
                            m = re.search("=",field_key)
                            if m :
                                 left,right = field_key.split("=")
                                 field_key = right

                            # add '!!' to mark it as empty name which is parsed by tshark
                            field_key = '!!' + field_key

                        if parent_name :
                            field_key = parent_name + '__' + field_key

                        #fieldval_value = field.get('value')
                        #Update the dictionary with the key value pairs obtained from normal fields
                        if 'vlan' in str(field_key):
                            sNewVlanKey = 'vlan'+str(iVlanCnt)
                            field_key = field_key.replace('vlan',sNewVlanKey)
                            if field_key in dParsePkt.keys():
                                iVlanCnt += 1
                                field_key = field_key.replace(sNewVlanKey,'vlan'+str(iVlanCnt))
                        #dParsePkt[field_key] = list((fieldval_show,fieldval_value))
                        dParsePkt[field_key.lower()] = fieldval_show

                # extend the filed path for next round
                sFiledPath = sFiledPath + '/field'

            else:
                llk_log_debug("No fields found in the packet!!")

            #update the list with the dictionary
            ldParsePkt.append(dParsePkt.copy())

            # print dParsePkt for trace debug
            for each in sorted(dParsePkt) :
                logger.trace("%s:%s" % (each,dParsePkt[each]))

            dParsePkt.clear()

    else:
        llk_log_info("No packets found in the pdml file!!")

    #Printing the list of dictionaries of packets on the log
    llk_log_trace(ldParsePkt)

    llk_log_end_proc()

    return ldParsePkt
