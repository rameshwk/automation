#coding=utf-8
import os
import sys
import re
import xml.dom.minidom
import string
import time
from Excute_CMD import ExcuteCMD

sys.path.append(os.environ.get('ROBOTREPO')+'/LIBS/COM/COM_CLI')
sys.path.append(os.environ.get('ROBOTREPO')+'/HLK/lib')


def _digui(node, node_list, nodename):
    print node
    print "Current Node Name is : %s" %node.nodeName
    if node.childNodes:
        print "%s has child Nodes" % node.nodeName
        for child_node in node.childNodes:
            print child_node.nodeName
        for child_node in node.childNodes:
            print child_node
            print "Current Child Node Name is : %s" % child_node.nodeName
            if child_node.nodeType == node.ELEMENT_NODE:
                print "Before connection string, the node_name is : %s" % node.nodeName
                node_name = nodename
                a = node_name
                node_name += child_node.nodeName
                print "After connect string, the node_name is : %s" % node_name
                node_list.append(node_name)
                if child_node.childNodes:
                    new_node_name = node_name
                    print new_node_name
                else:
                    new_node_name = a
                #print node_list
                print "this node's parent node is %s" %child_node.parentNode.nodeName
                print "Node_Name rechanged to the init value is : %s" % node_name
                print "*"*100
                print "*"*100
                _digui(child_node, node_list, new_node_name)
            else:
                print "This node is not ELEMENT_NODE, can not apppend in to node list!!!"
    else:
        print "This node have no child node!!!"
    return node_list


def _return_childNodes(node, node_list):
    print node
    print "Current Node Name is : %s" %node.nodeName
    if node.childNodes:
        print "%s has child Nodes" % node.nodeName
        for child_node in node.childNodes:
            if child_node.nodeType == node.ELEMENT_NODE:
                node_list.append(child_node)
                _return_childNodes(child_node, node_list)
    else:
        print "This node have no child node!!!"
    return node_list

def del_last_row_of_xml(path, filename):
    xmlfile = os.path.join(path, filename)
    f = open(xmlfile,'r')
    fr = f.readlines()
    fr.pop()
    print fr
    print len(fr)
    f.close()
    fs = open(xmlfile, 'w+')
    fs.writelines(fr)
    fs.close()

def get_all_nodes_from_datamodel(path, filename):
    nodelist = []
    new_node_name = ""
    datamodel_file = os.path.join(path, filename)
    dom = xml.dom.minidom.parse(datamodel_file)
    root = dom.documentElement
    b = _digui(root, nodelist, root.nodeName)
    for i in b:
        print i
    print len(b)

    passwordnodes = []

    for i in b:
        m = re.match(r'.*\.$', i)
        n = re.match(r'.*vsaInfo.*', i)
        if m:
            print i
            print "this node is not a tr69 node, remove it."
        elif n:
            print "this node is under node vsaInfo, remove it. "
        else:
            passwordnodes.append(i)

    new_all_nodes = []
    for i in passwordnodes:
        i = i + '\n'
        new_all_nodes.append(i)
    f = open('/tftpboot/nodelist.txt', 'w+')
    f.writelines(new_all_nodes)
    f.close()
    return passwordnodes


def return_node_from_preconfigurtaion_file(path, filename, opid):
    nodelist = []
    datamodel_file = os.path.join(path, filename)
    dom = xml.dom.minidom.parse(datamodel_file)
    root = dom.documentElement
    aaa = root.getElementsByTagName(opid)
    print aaa[0].nodeName
    print aaa[0].parentNode
    b = _return_childNodes(aaa[0], nodelist)
    print '$' * 100
    print "get nodes finished, start get dburi!!!!"
    print '$' * 100
    print b
    keyvalue_dict = {}

    for j in b:
        if j.hasAttribute("dburi"):
            print j
            a = j.getAttribute("dburi")
            print "%s have duri is %s" %(j, a)
            m = re.match(r'^InternetGatewayDevice.*\.\w+$', a)
            n = re.match(r'^[^InternetGatewayDevice.].*\w+$', a)
            if m:
                d = j.getAttribute("v")
                key_value = a + '=' + d + '\r\n'
                print key_value
                keyvalue_dict.setdefault(j.getAttribute("dburi"), j.getAttribute("v"))

            elif n:
                print "%s duri is not have InternetGatewayDevice." % j
                print j.getAttribute("dburi")
                if j.parentNode:
                    print j.parentNode.nodeName
                    print j.parentNode.getAttribute("dburi")
                    node = str(j.parentNode.getAttribute("dburi")) + str(j.getAttribute("dburi"))
                    key_value = node + "=" + j.getAttribute("v")
                    print key_value
                    keyvalue_dict.setdefault(node, j.getAttribute("v"))

            else:
                print j.getAttribute("dburi")
                print "the node %s is not useful." % j.getAttribute("dburi")
                if j.parentNode:
                    print j.parentNode
                    
    print "log the tr69nodes in the preconfiguration_global.xml:"
    for key in keyvalue_dict:
        print '%s = %s' %(key, keyvalue_dict[key])
    print len(keyvalue_dict)
    return keyvalue_dict

def return_node_value_from_cfgfile(path, filename):
    if path:
        path = str(path)
    if filename:
        filename = str(filename)
        target_filename = 'target_' + str(filename)
    cmd = 'cat ' + path + '/' + filename + '| grep -a "^InternetGatewayDevice." > ' + path + '/' + target_filename
    print "cmd is %s" % cmd
    os.system(cmd)

    file = path + '/' + target_filename
    nodes_dict = {}
    if not os.path.exists(file):
        raiseError("ATC_ERROR: " + file + " does not exist!!!")
    else:
        f = open(file, 'r')
        fr = f.readlines()
        print fr
        print len(fr)
        for i in fr:
            print len(i)
            if i.endswith('\r\n'):
                j = i[:-2]
            else:
                j = i[:-1]

            print j.split('=', 1)
            nodes_dict.setdefault(j.split('=', 1)[0], j.split('=', 1)[1])
        f.close()
    print nodes_dict
    return nodes_dict


def raiseError(Message=None):
    raise Exception,  Message

if __name__ == '__main__':
    return_node_value_from_cfgfile('/root', 'CFGTEST002')
    return_node_from_preconfigurtaion_file('/tftpboot', 'preconfiguration_global.xml', 'DUXX.')

