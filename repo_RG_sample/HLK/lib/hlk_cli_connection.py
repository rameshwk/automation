import time,inspect
from robot.api import logger
import data_translation
import cli_command
   

def connect_isam (tablename = "ISAMData",session_name="first_cli_session") :
    """
        connect isam by build up cli session based on data of ISAMData
    
    """
    tablename = tablename.encode("ascii")
    tabledata = data_translation.get_global_table(tablename)
    entry = tabledata[0]
    logger.info("Module:%s, Keyword:%s -> isam info:%s " \
    % (__name__,inspect.stack()[0][3],str(entry)))

    isamip = entry['IP'].encode("ascii")
    try :
        username = entry['Username'].encode("ascii")

    except Exception as inst:
        username = entry['UserName'].encode("ascii")
    firstpassword = entry['FirstPassword'].encode("ascii")
    secondpassword = entry['SecondPassword'].encode("ascii")

    re_connect = 0
    while re_connect < 11:
        try:
            cli_command.connect_cli(isamip,username=username,\
            first_password=firstpassword, second_password=secondpassword,\
            session_name=session_name)
        except Exception as inst:
            time.sleep(5)
            re_connect += 1
            if re_connect == 11:
                raise AssertionError("Module:%s, Keyword:%s -> \
                can not build up cli session: %s" \
                % (__name__,inspect.stack()[0][3], inst)) 
            continue
        else:    
            logger.info("Module:%s, Keyword:%s -> connect isam success " \
            % (__name__,inspect.stack()[0][3]))
            return "pass"

def disconnect_isam (session_name="first_cli_session") :
    """
       disconnect isam by close cli session
    """
    try:
        cli_command.disconnect_cli(session_name=session_name)
    except:
        raise AssertionError("Module:%s, Keyword:%s -> \
        fail to close isam connection" \
        % (__name__,inspect.stack()[0][3]))         
 
    else :
        logger.info("Module:%s, Keyword:%s -> disconnect isam success " \
        % (__name__,inspect.stack()[0][3]))
    return "pass"

