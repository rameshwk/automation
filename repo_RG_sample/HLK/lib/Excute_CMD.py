import subprocess
def ExcuteCMD(cmd):
    "Excute shell command ,return code and output"
    cmd = cmd
    print 'CMD is ### ' + cmd + ' ###'
    rc = 1
    content = ""
    # print (datetime.datetime.now())
    p = subprocess.Popen(cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

    p.wait()

    rc = p.returncode
    print 'rc is ' + str(rc)

    content = p.stdout.read().strip()
    if content:
        print ">" + content + "<"

    if p.returncode == 0:
        # print 'ATC_INFO : "' + cmd + '" Excute SUCESS!'
        rc = True
    else:
        print 'return code :' + str(rc)
        print 'ATC_WARNING : "' + cmd + '" Excute FAIL!'
        rc = False
    p.stdout.close()
    return rc, content
