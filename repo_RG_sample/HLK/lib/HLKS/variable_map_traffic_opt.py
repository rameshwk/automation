dSendPacketMapping =  {
'control_mode': {'STC':'control_mode','PCTA':'control_mode'},
'packet_count': {'STC':'burst_size','PCTA':'packet_count'},
'frame_len' : {'STC':'frame.FixedFrameLength','PCTA':'frame_len'},
'bit_rate' : {'STC':'load.load','PCTA':'bit_rate'},
'burst_rate' : {'STC':'burst_rate','PCTA':'burst_rate'},
'burst_size' : {'STC':'burst_size','PCTA':'burst_size'},
'mac_src': {'STC':'ethii.srcMac','PCTA':'mac_src'},
'mac_dest': {'STC':'ethii.dstMac','PCTA':'mac_dest'},
'eth_type': {'STC':'ethii.type','PCTA':'eth_type'},
'vlan1_id': {'STC':'vlan.vlan1','PCTA':'vlan1_id'},
'vlan2_id': {'STC':'vlan.vlan2','PCTA':'vlan2_id'},
'vlan3_id': {'STC':'vlan.vlan3','PCTA':'vlan3_id'},
'vlan1_user_prior': {'STC':'vlan.pri1','PCTA':'vlan1_user_prior'},
'vlan2_user_prior': {'STC':'vlan.pri2','PCTA':'vlan2_user_prior'},
'vlan3_user_prior': {'STC':'vlan.pri3','PCTA':'vlan3_user_prior'},
'vlan1_tpid': {'STC':'vlan.tpid1','PCTA':'vlan1_tpid'},
'vlan2_tpid': {'STC':'vlan.tpid2','PCTA':'vlan2_tpid'},
'vlan3_tpid': {'STC':'vlan.tpid3','PCTA':'vlan3_tpid'},
'vlan1_cfi': {'STC':'vlan.cfi1','PCTA':'x'},
'vlan2_cfi': {'STC':'vlan.cfi2','PCTA':'x'},
'vlan3_cfi': {'STC':'vlan.cfi3','PCTA':'x'},
'ip_dest': {'STC':'ipv4.destAddr' ,'PCTA':'ip_dest'},
'ip_src': {'STC':'ipv4.sourceAddr' ,'PCTA':'ip_src'},
'dhcpv6_ip_dest': {'STC':'ip.destAddr' ,'PCTA':'ip_dest'},
'dhcpv6_ip_src': {'STC':'ip.sourceAddr' ,'PCTA':'ip_src'},
'ipv6_dest': {'STC':'ipv6.destAddr' ,'PCTA':'ip_dest'},
'ipv6_src': {'STC':'ipv6.sourceAddr' ,'PCTA':'ip_src'},
'tcp_dest_port': {'STC':'tcp.destPort' ,'PCTA':'tcp_dest_port'},
'tcp_src_port': {'STC':'tcp.sourcePort' ,'PCTA':'tcp_src_port'},
'udp_dst_port': {'STC':'udp.dstport' ,'PCTA':'dst_port'},
'udp_src_port': {'STC':'udp.srcport' ,'PCTA':'src_port'},
'opcodecfm':{'STC':'cfm.opcode' ,'PCTA':'opcodecfm'},
'lbtid':{'STC':'cfm.lbtid' ,'PCTA':'transactionid'},
'original_mac': {'STC':'cfm.OrigMAC','PCTA':'x'},
'target_mac': {'STC':'cfm.TargetMAC', 'PCTA':'x'},
'ltmttl': {'STC':'cfm.LTMTTL', 'PCTA':'x'},
'mdlevel':{'STC':'cfm.cfmheader.mdlevel' ,'PCTA':'mdlevel'},
'maepi':{'STC':'cfm.maepi' ,'PCTA':'maepi'},
'mdnf':{'STC':'cfm.maid.mdnf' ,'PCTA':'mdnf'},
'smaf':{'STC':'cfm.maid.smaf' ,'PCTA':'smaf'},
'sman':{'STC':'cfm.maid.sman' ,'PCTA':'sman'},
'mdn':{'STC':'cfm.maid.mdn' ,'PCTA':'mdn'},
'sequencenumber':{'STC':'cfm.sequencenumber' ,'PCTA':'sequencenumber'},
'padding':{'STC':'cfm.maid.padding' ,'PCTA':'padding'},
'prot_addr_dest':{'STC':'prot_addr_dest' ,'PCTA':'prot_addr_dest'},
'prot_addr_src':{'STC':'prot_addr_src' ,'PCTA':'prot_addr_src'},
'arp_rarp_mode':{'STC':'arp_rarp_mode' ,'PCTA':'arp_rarp_mode'},
'opcode':{'STC':'arp.operation' ,'PCTA':'opcode'},
'hw_addr_dest':{'STC':'arp.targetHwAddr' ,'PCTA':'hw_addr_dest'},
'hw_addr_src':{'STC':'arp.senderHwAddr' ,'PCTA':'hw_addr_src'},
'hw_type':{'STC':'arp.hardware' ,'PCTA':'hw_type'},
'igmp_type':{'STC':'igmp.type' ,'PCTA':'igmp_type'},
'igmp_version':{'STC':'igmp.version' ,'PCTA':'igmp_version'},
'nb_of_group_records':{'STC':'nb_of_group_records' ,'PCTA':'nb_of_group_records'},
'igmp_grp_addr':{'STC':'igmp.groupAddress' ,'PCTA':'igmp_grp_addr'},
'group_record_list':{'STC':'group_record_list' ,'PCTA':'group_record_list'},
'bootp_dhcp':{'STC':'dhcp.bootp_dhcp' ,'PCTA':'bootp_dhcp'},
'bootp_type':{'STC':'bootp_type' ,'PCTA':'bootp_type'},
'dhcp_message_type':{'STC':'dhcp.messageType' ,'PCTA':'dhcp_message_type'},
'chaddr':{'STC':'chaddr' ,'PCTA':'chaddr'},
'transaction_id':{'STC':'dhcp.xid' ,'PCTA':'transaction_id'},
'dhcp_options':{'STC':'dhcp.options' ,'PCTA':'dhcp_options'},
'opt82_agent_circuit_id':{'STC':'opt82_agent_circuit_id' ,'PCTA':'opt82_agent_circuit_id'},
'opt82_agent_remote_id':{'STC':'opt82_agent_remote_id' ,'PCTA':'opt82_agent_remote_id'},
'opt82_vendor_specific_info':{'STC':'opt82_vendor_specific_info' ,'PCTA':'opt82_vendor_specific_info'},
'icmp_type':{'STC':'' ,'PCTA':'icmp_type'},
'icmp_code':{'STC':'' ,'PCTA':'icmp_code'},
'dhcpv6_udp_dest_port': {'STC':'dhcpv6.destPort' ,'PCTA':'udp_dest_port'},
'dhcpv6_udp_src_port': {'STC':'dhcpv6.sourcePort' ,'PCTA':'udp_src_port'},
'dhcpv6_tcp_dest_port': {'STC':'dhcpv6.destPort' ,'PCTA':'tcp_dest_port'},
'dhcpv6_tcp_src_port': {'STC':'tcp_src_port' ,'PCTA':'tcp_src_port'},
'rip_version':{'STC':'rip.version' ,'PCTA':'rip_version'},
'rip_routing_domain':{'STC':'rip_routing_domain' ,'PCTA':'rip_routing_domain'},
'rip_entry_list':{'STC':'rip_entry_list' ,'PCTA':'rip_entry_list'},
'rip_command':{'STC':'rip.command' ,'PCTA':'rip_command'},
'fcs':{'STC':'rip.fsc' ,'PCTA':'fcs'},
'code':{'STC':'pppoe.code' ,'PCTA':'code'},
'ver':{'STC':'pppoe.version' ,'PCTA':'ver'},
'type':{'STC':'eth.type' ,'PCTA':'type'},
'pppoetype':{'STC':'pppoe.type' ,'PCTA':'type'},
'ac_name_tag':{'STC':'pppoe.acname.Name' ,'PCTA':'ac_name_tag'},
'ac_name_len':{'STC':'pppoe.acname.length' ,'PCTA':'ac_name_len'},
'ac_name_val':{'STC':'pppoe.acname.value' ,'PCTA':'ac_name_val'},
'pppoa_encaps_type':{'STC':'' ,'PCTA':'pppoa_encaps_type'},
'session_id':{'STC':'' ,'PCTA':'session_id'},
'information':{'STC':'' ,'PCTA':'information'},
'accm_option':{'STC':'' ,'PCTA':'accm_option'},
'accm_val':{'STC':'' ,'PCTA':'accm_val'},
'ap_option':{'STC':'' ,'PCTA':'ap_option'},
'acfc_option':{'STC':'' ,'PCTA':'acfc_option'},
'pfc_option':{'STC':'' ,'PCTA':'pfc_option'},
'qp_option':{'STC':'' ,'PCTA':'qp_option'},
'seq_nbr':{'STC':'' ,'PCTA':'seq_nbr'},
'atm_clp':{'STC':'' ,'PCTA':'atm_clp'},
'atm_clp_list':{'STC':'' ,'PCTA':'atm_clp_list'},
'atm_payload':{'STC':'' ,'PCTA':'atm_payload'},
'atm_payload_list':{'STC':'' ,'PCTA':'atm_payload_list'},
'atm_pti':{'STC':'' ,'PCTA':'atm_pti'},
'atm_pti_list':{'STC':'' ,'PCTA':'atm_pti_list'},
'atm_vci':{'STC':'' ,'PCTA':'atm_vci'},
'atm_vci_list':{'STC':'' ,'PCTA':'atm_vci_list'},
'atm_vpi':{'STC':'' ,'PCTA':'atm_vpi'},
'atm_vpi_list':{'STC':'' ,'PCTA':'atm_vpi_list'},
'mpls_bos':{'STC':'mpls.sBit' ,'PCTA':'mpls_bos'},
'mpls_bos_list':{'STC':'mpls_bos_list' ,'PCTA':'mpls_bos_list'},
'mpls_exp':{'STC':'mpls.exp' ,'PCTA':'mpls_exp'},
'mpls_exp_list':{'STC':'mpls_exp_list' ,'PCTA':'mpls_exp_list'},
'mpls_label':{'STC':'mpls.label' ,'PCTA':'mpls_label'},
'mpls_label_list':{'STC':'mpls_label_list' ,'PCTA':'mpls_label_list'},
'mpls_ttl':{'STC':'mpls.ttl' ,'PCTA':'mpls_ttl'},
'mpls_ttl_list':{'STC':'mpls_ttl_list' ,'PCTA':'mpls_ttl_list'},
'ip_protocol' :{'STC':'','PCTA':'ip_protocol'},
'pwach' :{'STC':'','PCTA':'pwach'},
'pwmcw_flags' :{'STC':'','PCTA':'pwmcw_flags'},
'pwach_version' :{'STC':'','PCTA':'pwach_version'},
'pwmcw_frg' :{'STC':'','PCTA':'pwmcw_frg'},
'pwmcw_seq_num' :{'STC':'','PCTA':'pwmcw_seq_num'},
'eap_id' :{'STC':'','PCTA':'eap_id'},
'file' :{'STC':'','PCTA':'file'},
'data_source_type' :{'STC':'','PCTA':'data_source_type'},
'key_desc_type' :{'STC':'','PCTA':'key_desc_type'},
}

def get_mapping():
    global dSendPacketMapping
    return dSendPacketMapping
