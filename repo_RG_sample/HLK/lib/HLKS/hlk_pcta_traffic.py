import os,sys,time,re,hashlib
from robot.libraries.BuiltIn import BuiltIn
from robot.api import logger

try:	
    for lib_path in [os.environ['ROBOTREPO'] +'/HLK/lib/COM_PCTA',\
    os.environ['ROBOTREPO'] +'/HLK/lib/HLKS'] :
        if lib_path not in sys.path:
            sys.path.append(lib_path)
except Exception as inst:
    raise AssertionError("%s -> Fail to set sys.path, %s" % (__name__,inst))

import hlk_mac_address_convertion
import hlk_ip_address_convertion
import pcta_command

### MODE:normal/debug
TRAFFIC_MODE = "normal"
TRAFFIC_DEBUG = False
PRODUCT_INFO = {}
MODULE_NAME = None
product = ""

try:
    product = BuiltIn().get_variable_value('${PRODUCT}',None)
except Exception as inst:
    logger.debug("Fail to get environment variable of PRODUCT")
    product = ""
if product:
    try:
        sys.path.append(os.environ.get('ROBOTREPO')+'/ULKS/'+product.upper())
        module = 'ulk_trace_'+product.lower()
        MODULE_NAME = __import__(module)
    except Exception as inst:
        logger.warn(inst)
#else :
#    product = ""

def _get_unique_mac(vlan1,vlan2):
    mac = "000000123456"
    if vlan1 != "" and vlan2 != "" and vlan1 != "x" and vlan2 != "x":
        mac=hlk_mac_address_convertion.get_aid_unique_mac("1/1/1/1/1/1/1/%s/%s" % (vlan2,vlan1))
    return mac

def _ip_convert_decimal_to_hex(ip):
    keyword="_ip_convert_decimal_to_hex"
    logger.debug("%s:%s" \
    % (__name__,keyword))
    
    ip_hex=""
    ip_list=ip.split(".")
    for single_ip in ip_list:
        hex_ip=hex(int(single_ip))
        hex_ip=hex_ip.replace("0x","")
        hex_ip=hex_ip.rjust(2,"0")
        ip_hex=ip_hex+hex_ip
    return ip_hex

def _time_convert_to_hex(time,len=8):
    keyword="_time_convert_to_hex"
    logger.debug("%s:%s" \
    % (__name__,keyword))
    
    hex_time=hex(int(time))
    hex_time=hex_time.replace("0x","")
    hex_time=hex_time.rjust(len,"0")
    return hex_time

def _get_vlan_list(vlan):
    keyword="_get_vlan_list"
    logger.debug("%s:%s" \
    % (__name__,keyword))

    vlan_list=str(vlan).split(",")
    vlan_list.reverse()
    return vlan_list
        
def _get_param_list(send_param,capt_param,default_send_param,\
                    default_capt_param,client_vlan,network_vlan,**args):
    keyword="_get_param_list"
    logger.debug("%s:%s" \
    % (__name__,keyword))
    vlan_style = args.setdefault("vlan_style","new")
 
    if send_param!="":
        send_param_list=send_param.split(",")
    else:
        send_param_list=[]
    if capt_param!="":
        capt_param_list=capt_param.split(",")
    else:
        capt_param_list=[]
    for single_default_send_param in default_send_param:
        key,value=single_default_send_param.split("=")
        if key not in send_param:
            send_param_list.append(single_default_send_param)
    i=1
    for single_vlan in client_vlan:
        if single_vlan != "" and single_vlan != "x":
            if vlan_style == 'new':
                send_param_list.append("vlan%s_id=%s" % (i,single_vlan)) 
            else:
                if i == 1:
                    send_param_list.append("c_vlan_id=%s" % (single_vlan))
                if i == 2:
                    send_param_list.append("s_vlan_id=%s" % (single_vlan))
            i+=1   
    for single_default_capt_param in default_capt_param:
        key,value=single_default_capt_param.split("=")
        if key not in capt_param:
            capt_param_list.append(single_default_capt_param)

    # delete the items which value equals 'x'. that means user want to ignore it in capture options.
    for each in capt_param_list :
        try : 
            key,value=each.split("=")
            if str(value) == 'x' :
                capt_param_list.remove(each)
        except Exception as inst:
            raise AssertionError("Parameter format is wrong, please check: %s" % each)

    i=1
    for single_vlan in network_vlan:
        if single_vlan == "x":
            single_vlan = 65535
        if single_vlan != "":
            if vlan_style == "new":
                capt_param_list.append("vlan%s_id=%s" % (i,single_vlan))
            else:
                if i == 1:
                    capt_param_list.append("c_vlan_id=%s" % (single_vlan))
                if i == 2:
                    capt_param_list.append("s_vlan_id=%s" % (single_vlan))
            i+=1  
    return send_param_list,capt_param_list

def _send_traffic_logic(send_param,capt_param,expect_result,traffic_times=2,delay_time=1,send_delay=0):

    global TRAFFIC_MODE
    traffic_times=int(traffic_times)
    
    if TRAFFIC_MODE == "normal":
        for i in range(traffic_times) :
            inst = ""
            try:
                pcta_command.pcta_check_traffic \
                ("send_options=%s" % send_param,"capt_options=%s" % capt_param,\
                "exp_result=%s" % expect_result,"delay_time=%s" % delay_time,"capt_reserve=true","send_delay=%s" % send_delay)
            except Exception as inst:
                logger.info("time %s,exception: %s" % (i+1,inst))

            if str(inst) == "" :
                return "PASS"
            elif "no packets found" in str(inst) or "expect/verified is" in str(inst) :  
                logger.debug("sleep 1 second and try again")               
                time.sleep(1)
            else :
                raise PctaContinueError("pcta function broken, please check") 

        status,info=get_traffic_debug_status()
        traffic_debug("before_traffic")
        if status:
            try:              
                pcta_command.pcta_check_traffic \
                ("send_options=%s" % send_param,"capt_options=%s" % capt_param,\
                 "exp_result=%s" % expect_result,"delay_time=%s" % delay_time,"capt_reserve=true","send_delay=%s" % send_delay)
            except:
                logger.info("debug traffic failed")
            else :
                logger.info ("debug traffic passed")
            traffic_debug("after_traffic") 
        raise PctaContinueError("failed to get expect traffic for %s times" % str(traffic_times))

    if TRAFFIC_MODE == "debug":
        traffic_times = 5
        i = 1
        status = "pass"
        while i < traffic_times:  
            traffic_debug("before_traffic") 
            try:              
                pcta_command.pcta_check_traffic("send_options=%s" % send_param,"capt_options=%s" % capt_param,\
                                          "exp_result=%s" % expect_result,"delay_time=%s" % delay_time,"capt_reserve=true","send_delay=%s" % send_delay)
            except Exception as inst:
                status = "fail"
                logger.info("Time %s: traffic check failed! \nexception: %s" % (traffic_times,inst))
                traffic_debug("after_traffic") 
            i+=1
        if status == "fail":
            raise AssertionError("fail to check traffic") 

def pcta_generate_group_records(record_type,src_address_list,igmp_grp_addr):
    record_type_dict = { 
                         "MODE_IS_INCLUDE": "01",
                         "MODE_IS_EXCLUDE": "02",
                         "CHANGE_TO_INCLUDE": "03",
                         "CHANGE_TO_EXCLUDE": "04",
                         "ALLOW": "05",
                         "BLOCK": "06"                      
        }
    
    # group_record_list combined with record_type + source_address_number + multicast_address + source_addresses
    if record_type_dict.has_key(record_type) :
         group_record_list = record_type_dict[record_type]
    else :
         group_record_list = record_type.zfill(2)

    addr_split_list = []
    addr_split_list.extend( igmp_grp_addr.split(".") )
    chg_src_address_list = re.sub(" +",".",src_address_list.lstrip("{").rstrip("}"))  
    if src_address_list == '{}':
        src_address_num = 0
    else:
        src_address_num = len (src_address_list.strip().split(" "))
        addr_split_list.extend( chg_src_address_list.split("."))
    src_address_num = hex (src_address_num).lstrip('0x')
    src_address_num = src_address_num.zfill(4)    
    group_record_list = group_record_list + "00" + src_address_num
    
    for part in addr_split_list :
        part = int (part)
        part = hex ( part )
        part = part.lstrip('0x')
        part = part.zfill(2)
        group_record_list = group_record_list + part
    return group_record_list

def enable_traffic_debug(**product_info):
    global TRAFFIC_DEBUG,PRODUCT_INFO
    TRAFFIC_DEBUG=True
    PRODUCT_INFO=product_info

def disable_traffic_debug():
    global TRAFFIC_DEBUG,PRODUCT_INFO
    TRAFFIC_DEBUG=False

def get_traffic_debug_status():
    return (TRAFFIC_DEBUG,PRODUCT_INFO)
    
def traffic_debug(debugTiming):
    traffic_debug,product_info=get_traffic_debug_status()
    if traffic_debug:

        try:
            logger.info("Traffic Debug Start: %s****************" % debugTiming)
            MODULE_NAME.traffic_debug(debugTiming,**PRODUCT_INFO)

        except Exception as inst:
            logger.info("Can not execute traffic debug! Exception: %s" % inst)

def send_end2end_ethernet_traffic(expect_result,send_vlan="",capt_vlan="",send_param="",capt_param="",comments="",traffic_times=10):
    """
    specific traffic mode: sending and capturing port are same (default is user side)
    
    - *expect_result:* PASS/FAIL
    - *send_vlan:* vlan used when you send the packet. Example: 100 or 200,100 or 200,100,...,100
    - *capt_vlan:* vlan to be compared when you captured
    - *send_param:* useful if want to specify a particular parameter value to be sent
    - *capt_param:* to specify particular parameter values other than vlan to be captured
    - *comments:* description for test target of this traffic
    - *traffic_times:* times to re-try if traffic failed at first time

    | send_end2end_ethernet_traffic | pass | 11 | 12 | send_param=mac_src=000000123456,mac_dest=000000222222 |
    | | capt_param=mac_src=000000123456 |

    """
    keyword="send_end2end_ip_traffic"
    logger.debug("%s:%s" % (__name__,keyword))  
      
    send_vlan_list=_get_vlan_list(send_vlan)  
    capt_vlan_list=_get_vlan_list(capt_vlan)

    send_mac=_get_unique_mac(send_vlan_list[0],send_vlan_list[-1])
    capt_mac=_get_unique_mac(capt_vlan_list[0],capt_vlan_list[-1])
    if capt_mac == send_mac:
        capt_mac=000000654321

    default_send_param=["ta_protocol=IP","side=user","eth_type=ETH","ip_src=192.168.1.10",\
                       "ip_dest=192.168.1.11","mac_src=%s" % send_mac,"mac_dest=%s" % capt_mac,"packet_count=11"]
    default_capt_param=["ta_protocol=IP","side=user","mac_src=%s" % send_mac,"mac_dest=%s" % capt_mac]
    param_list=_get_param_list(send_param,capt_param,default_send_param,\
                                   default_capt_param,send_vlan_list,capt_vlan_list)

   ### start send traffic logic
    _send_traffic_logic(param_list[0],param_list[1],expect_result,traffic_times,delay_time=2)

def send_upstream_ipv6_traffic(expect_result,client_vlan="",network_vlan="",send_param="",capt_param="",comments="",traffic_times=3,delay_time=1):
    """
    - *expect_result:* PASS/FAIL
    - *client_vlan:* vlan used when you send the packet. Example: 100 or 200,100 or 200,100,...,100
    - *network_vlan:* vlan to be compared when you captured.
    - *send_param:* specify a particular parameter value to be sent.
    - *capt_param:* useful if want to specify a particular parameter value to be compared. 
    - *comments:* description for test target of this traffic
    - *traffic_times:* times to re-try if traffic failed at first time   
    - *delay_time:* time to sleep before stop capture (seconds)
     
    | send_upstream_ipv6_traffic | FAIL | 11 | 12 | send_param=ip_src=2001::1,mac_dest=000000222222 |
    |   | capt_param=ip_src=2001::1 |  
    | send_upstream_ipv6_traffic | PASS | 11 | 12 | send_param=ip_src=2001::1 | delay_time =5 |
    |   | traffic_times=4 |     
    """
    keyword="send_upstream_ipv6_traffic"
    logger.debug("%s:%s" % (__name__,keyword))  
      
    client_vlan_list=_get_vlan_list(client_vlan)  
    network_vlan_list=_get_vlan_list(network_vlan)
    client_mac=_get_unique_mac(client_vlan_list[0],client_vlan_list[-1])

    ip_src = "2000::1"
    ip_dest = "2222::3"
    ip_src_full = hlk_ip_address_convertion.convert_to_mib_ipv6_str(ip_src,":")
    ip_dest_full = hlk_ip_address_convertion.convert_to_mib_ipv6_str(ip_dest,":")

    default_send_param=["ta_protocol=IPV6","side=user","eth_type=ETH","mac_src=%s" % client_mac,"packet_count=5",
                        "ip_src=%s" % ip_src_full,"ip_dest=%s" % ip_dest_full]
    default_capt_param=["ta_protocol=IPV6","side=network","mac_src=%s" % client_mac]

    # need update ipv6 address since PCTA do not support abbreviation
    ret = re.search("ip_src=((\w|\:)*)",send_param)
    if ret:
        ipv6_orig = ret.group(1)
        ipv6_full = hlk_ip_address_convertion.convert_to_mib_ipv6_str(ipv6_orig,":")
        send_param = re.sub(ipv6_orig,ipv6_full,send_param)
    ret = re.search("ip_dest=((\w|\:)*)",send_param)
    if ret:
        ipv6_orig = ret.group(1)
        ipv6_full = hlk_ip_address_convertion.convert_to_mib_ipv6_str(ipv6_orig,":")
        send_param = re.sub(ipv6_orig,ipv6_full,send_param)
    
    param_list=_get_param_list(send_param,capt_param,default_send_param,\
                                   default_capt_param,client_vlan_list,network_vlan_list)

   ### start send traffic logic
    _send_traffic_logic(param_list[0],param_list[1],expect_result,traffic_times,delay_time)
     
def send_downstream_ipv6_traffic(expect_result,network_vlan="",client_vlan="",send_param="",capt_param="",comments="",traffic_times=2,delay_time=1,send_delay=0):
    """
    - *expect_result:* PASS/FAIL
    - *client_vlan:* vlan used when you send the packet. Example: 100 or 200,100 or 200,100,...,100
    - *network_vlan:* vlan to be compared when you captured.
    - *send_param:* specify a particular parameter value to be sent.
    - *capt_param:* specify a particular parameter value to be compared. 
    - *comments:* description for test target of this traffic
    - *traffic_times:* times to re-try if traffic failed at first time   
    - *delay_time:* time to sleep before stop capture (seconds)
    - *send_delay:* time delay between start capture and send packets
        
    | send_downstream_ipv6_traffic | FAIL | 11 | 12 | send_param=ip_src=2001::1,mac_dest=000000222222 |
    |   | capt_param=ip_src=2001::1 |  
    | send_downstream_ipv6_traffic | PASS | 11 | 12 | send_param=ip_src=2001::1 | delay_time =5 |
    |   | traffic_times=4 |      
    
    """
    keyword="send_downstream_ipv6_traffic"
    logger.debug("%s:%s" % (__name__,keyword))  

    client_vlan_list =_get_vlan_list(client_vlan)  
    network_vlan_list =_get_vlan_list(network_vlan)
    client_mac =_get_unique_mac(client_vlan_list[0],client_vlan_list[-1])

    ip_src = "2000::3"
    ip_dest = "2000::1"
    ip_src_full = hlk_ip_address_convertion.convert_to_mib_ipv6_str(ip_src,":")
    ip_dest_full = hlk_ip_address_convertion.convert_to_mib_ipv6_str(ip_dest,":")

    default_send_param=["ta_protocol=IPV6","side=network","eth_type=ETH","mac_dest=%s" % client_mac,"packet_count=3",
                        "ip_src=%s" % ip_src_full,"ip_dest=%s" % ip_dest_full]
    default_capt_param=["ta_protocol=IPV6","side=user","mac_dest=%s" % client_mac]
    # need update ipv6 address since PCTA do not support abbreviation
    ret = re.search("ip_src=((\w|\:)*)",send_param)
    if ret:
        ipv6_orig = ret.group(1)
        ipv6_full = hlk_ip_address_convertion.convert_to_mib_ipv6_str(ipv6_orig,":")
        send_param = re.sub(ipv6_orig,ipv6_full,send_param)
    ret = re.search("ip_dest=((\w|\:)*)",send_param)

    if ret:
        ipv6_orig = ret.group(1)
        ipv6_full = hlk_ip_address_convertion.convert_to_mib_ipv6_str(ipv6_orig,":")
        send_param = re.sub(ipv6_orig,ipv6_full,send_param)

    param_list=_get_param_list(send_param,capt_param,default_send_param,\
                                   default_capt_param,network_vlan_list,client_vlan_list)

    ### start send traffic logic
    _send_traffic_logic(param_list[0],param_list[1],expect_result,traffic_times,delay_time,send_delay)

def send_upstream_ip_traffic(expect_result,client_vlan="",network_vlan="",send_param="",capt_param="",comments="",traffic_times=2,delay_time=1):
    """
    - *expect_result:*  PASS/FAIL
    - *client_vlan:* vlan used when you send the packet. Example: 100 or 200,100 or 200,100,...,100
    - *network_vlan:* vlan to be compared when you captured.
    - *send_param:* to specify particular parameter values other than vlan to be sent
    - *capt_param:* to specify particular parameter values other than vlan to be captured 
    - *comments:* description for test target of this traffic
    - *traffic_times:* times to re-try if traffic failed at first time   
    - *delay_time:* time to sleep before stop capture (seconds)
        
    | send_upstream_ip_traffic | FAIL | 11 | 12 | send_param=ip_src=135.531.55.66,mac_dest=000000222222 |
    |   | capt_param=ip_src=135.531.55.66 |  
    | send_upstream_ip_traffic | PASS | 11 | 12 | send_param=ip_src=135.531.55.66 | delay_time =5 |
    |   | traffic_times=4 |      
    """
    keyword="send_upstream_ip_traffic"
    logger.debug("%s:%s" % (__name__,keyword))  
      
    client_vlan_list=_get_vlan_list(client_vlan)  
    network_vlan_list=_get_vlan_list(network_vlan)
    client_mac=_get_unique_mac(client_vlan_list[0],client_vlan_list[-1])

    default_send_param=["ta_protocol=IP","side=user","eth_type=ETH","mac_src=%s" % client_mac,"frame_len=1050","packet_count=5"]
    default_capt_param=["ta_protocol=IP","side=network","mac_src=%s" % client_mac]
    param_list=_get_param_list(send_param,capt_param,default_send_param,\
                                   default_capt_param,client_vlan_list,network_vlan_list)

   ### start send traffic logic
    _send_traffic_logic(param_list[0],param_list[1],expect_result,traffic_times,delay_time)
     
def send_downstream_ip_traffic(expect_result,network_vlan="",client_vlan="",send_param="",capt_param="",comments="",traffic_times=2,delay_time=1,send_delay=0):
    """
    - *expect_result:*  PASS/FAIL
    - *client_vlan:* vlan used when you send the packet. Example: 100 or 200,100 or 200,100,...,100
    - *network_vlan:* vlan to be compared when you captured.
    - *send_param:* to specify particular parameter values other than vlan to be sent
    - *capt_param:* to specify particular parameter values other than vlan to be captured
    - *comments:* description for test target of this traffic
    - *traffic_times:* times to re-try if traffic failed at first time  
    - *delay_time:* time to sleep before stop capture (seconds)
    - *send_delay:* time delay between start capture and send packets
        
    | send_downstream_ip_traffic | FAIL | 11 | 12 | send_param=ip_src=135.531.55.66,mac_dest=000000222222 |
    |   | capt_param=ip_src=135.531.55.66 |  
    | send_downstream_ip_traffic | PASS | 11 | 12 | send_param=ip_src=135.531.55.66 | delay_time =5 |
    |   | traffic_times=4 | 
    """
    keyword="send_downstream_ip_traffic"
    logger.debug("%s:%s" % (__name__,keyword))  

    client_vlan_list=_get_vlan_list(client_vlan)  
    network_vlan_list=_get_vlan_list(network_vlan)
    client_mac=_get_unique_mac(client_vlan_list[0],client_vlan_list[-1])

    default_send_param=["ta_protocol=IP","side=network","eth_type=ETH","mac_dest=%s" % client_mac,"frame_len=1050","packet_count=3"]
    default_capt_param=["ta_protocol=IP","side=user","mac_dest=%s" % client_mac]
    param_list=_get_param_list(send_param,capt_param,default_send_param,\
                                   default_capt_param,network_vlan_list,client_vlan_list)
    
    ### start send traffic logic
    _send_traffic_logic(param_list[0],param_list[1],expect_result,traffic_times,delay_time,send_delay)
 
def send_upstream_rarp_traffic(expect_result,ip_req,arp_type,client_vlan="", \
                              network_vlan="",send_param="",capt_param="",comments="",traffic_times=2,delay_time=1):
    """
    - *expect_result:*  PASS/FAIL
    - *ip_req:* ip address required for arp_request. Example: 1.1.1.1
    - *arp_type:* arp type. Exapme: arp_request/arp_reply
    - *client_vlan:* vlan used when you send the packet. Example: 100 or 200,100 or 200,100,...,100
    - *network_vlan:* vlan to be compared when you captured.
    - *send_param:* to specify particular parameter values other than vlan to be sent
    - *capt_param:* to specify particular parameter values other than vlan to be captured
    - *comments:* description for test target of this traffic
    - *traffic_times:* times to re-try if traffic failed at first time
    - *delay_time:* time to sleep before stop capture (seconds)
        
    | send_upstream_rarp_traffic | PASS | 1.1.1.1 | arp_request | 11 | 12 |  
    | send_upstream_rarp_traffic | PASS | 1.1.1.1 | arp_request | 11 | 12 | delay_time =5 |
    |   | traffic_times=4 |    
    """
    keyword="send_upstream_arp_traffic"
    logger.debug("%s:%s" % (__name__,keyword))  
 
    ip_src="2.2.2.2"
    arp={'arp_request':{'opcode':1,'mac_dest':'ffffffffffff','hw_addr_dest':'000000000000'},\
         'arp_reply':{'opcode':2,'mac_dest':'000000222222','hw_addr_dest':'000000222222'}}

    client_vlan_list=_get_vlan_list(client_vlan)  
    network_vlan_list=_get_vlan_list(network_vlan)
    client_mac=_get_unique_mac(client_vlan_list[0],client_vlan_list[-1])

    default_send_param=["ta_protocol=ARP","side=user","mac_src=%s" % client_mac,"prot_addr_src=%s" % \
                    ip_src,"mac_dest=%s" % arp[arp_type]['mac_dest'],"opcode=%s" % arp[arp_type]['opcode'],\
                    "hw_addr_src=%s" % client_mac,"hw_addr_dest=%s" % arp[arp_type]['hw_addr_dest'], \
                    "prot_addr_dest=%s" % ip_req,"packet_count=5"]
    default_capt_param=["ta_protocol=ARP","side=network","mac_src=%s" % client_mac, "mac_dest=%s" % \
                    arp[arp_type]['mac_dest'],"opcode=%s" % arp[arp_type]['opcode']]
    param_list=_get_param_list(send_param,capt_param,default_send_param,\
                               default_capt_param,client_vlan_list,network_vlan_list)
   
    ### start send traffic logic
    _send_traffic_logic(param_list[0],param_list[1],expect_result,traffic_times,delay_time)
 
def send_downstream_rarp_traffic(expect_result,ip_req,arp_type,network_vlan, \
                              client_vlan="",send_param="",capt_param="",comments="",traffic_times=2,delay_time=1):
    """
    - *expect_result:*  PASS/FAIL
    - *ip_req:* ip address required for arp_request. Example: 1.1.1.1
    - *arp_type:* arp type. Exapme: arp_request/arp_reply
    - *client_vlan:* vlan used when you send the packet. Example: 100 or 200,100 or 200,100,...,100
    - *network_vlan:* vlan to be compared when you captured.
    - *send_param:* to specify particular parameter values other than vlan to be sent
    - *capt_param:* to specify particular parameter values other than vlan to be captured
    - *comments:* description for test target of this traffic
    - *traffic_times:* times to re-try if traffic failed at first time   
    - *delay_time:* time to sleep before stop capture (seconds)
     
    | send_downstream_rarp_traffic | PASS | 1.1.1.1 | arp_reply | 11 | 12 |  
    | send_downstream_rarp_traffic | PASS | 1.1.1.1 | arp_reply | 11 | 12 | delay_time =5 |
    |   | traffic_times=4 | 
    """
    keyword="send_downstream_arp_traffic"
    logger.debug("%s:%s" % (__name__,keyword))  
    ip_src="2.2.2.2"
  
    client_vlan_list=_get_vlan_list(client_vlan)  
    network_vlan_list=_get_vlan_list(network_vlan)
    client_mac=_get_unique_mac(client_vlan_list[0],client_vlan_list[-1])
 
    arp={'arp_request':{'opcode':1,'mac_dest':'ffffffffffff','hw_addr_dest':'000000000000'},\
         'arp_reply':{'opcode':2,'mac_dest':client_mac,'hw_addr_dest':client_mac}}

    default_send_param=["ta_protocol=ARP","side=network","mac_dest=%s" % arp[arp_type]['mac_dest'],\
                     "prot_addr_src=%s" % ip_src,"opcode=%s" % arp[arp_type]['opcode'],"hw_addr_dest=%s" % \
                        arp[arp_type]['hw_addr_dest'],"prot_addr_dest=%s" % ip_req,"packet_count=2"]
    default_capt_param=["ta_protocol=ARP","side=user","mac_dest=%s" % arp[arp_type]['mac_dest'], \
                        "opcode=%s" % arp[arp_type]['opcode']]
    param_list=_get_param_list(send_param,capt_param,default_send_param,\
                                   default_capt_param,network_vlan_list,client_vlan_list) 

    ### start send traffic logic
    _send_traffic_logic(param_list[0],param_list[1],expect_result,traffic_times,delay_time)    

def send_upstream_dhcp_traffic(expect_result,ip_require,message_type,client_vlan="", \
                              network_vlan="",send_param="",capt_param="",comments="",client_mac="",traffic_times=2,delay_time=1):
    """    
    - *expect_result:*  PASS/FAIL
    - *ip_require:* ip address required for dhcp. Example: 1.1.1.1
    - *message_type:* dhcp message type. Exapme: discover/offer/request/ack/renew/release/
    - *client_vlan:* vlan used when you send the packet. Example: 100 or 200,100 or 200,100,...,100
    - *network_vlan:* vlan to be compared when you captured.
    - *send_param:* to specify particular parameter values other than vlan to be sent
    - *capt_param:* to specify particular parameter values other than vlan to be captured
    - *comments:* description for test target of this traffic
    - *traffic_times:* times to re-try if traffic failed at first time
    - *delay_time:* time to sleep before stop capture (seconds)
     
    | send upstream dhcp traffic | PASS | 0.0.0.0 | discovery | 11 | 12 | 
    |   | send_param=mac_src=000000600000 | capt_param=mac_src=000000600000 | traffic_times=4 | 
    
    """
    keyword="send_upstream_dhcp_traffic"
    logger.debug("%s:%s" % (__name__,keyword))  

    ip_hex=_ip_convert_decimal_to_hex(ip_require)
  
    client_vlan_list=_get_vlan_list(client_vlan)  
    network_vlan_list=_get_vlan_list(network_vlan)

    if client_mac == "":
            client_mac=_get_unique_mac(client_vlan_list[0],client_vlan_list[-1])

    m=re.search("mac_src=(\w+)",send_param)
    if m:
        client_mac=m.group(1)
   
    dhcp={'discovery':{'opcode':1,'dhcp_options':'370501060f212cff','ip_src':'0.0.0.0',\
                     'ip_dest':'255.255.255.255','mac_dest':'ffffffffffff','ciaddr':'0.0.0.0'},\
          'request':{'opcode':3,'dhcp_options':'3604020202023204'+ip_hex+'370501060f212cff','ip_src':'0.0.0.0',\
                   'ip_dest':'255.255.255.255','mac_dest':'ffffffffffff','ciaddr':'0.0.0.0'},\
          'renew':{'opcode':3,'dhcp_options':'3604020202023204'+ip_hex+'370501060f212cff','ip_src':ip_require,\
                  'ip_dest':'2.2.2.2','mac_dest':'000000222222','ciaddr':ip_require},\
          'decline':{'opcode':4,'dhcp_options':'3604020202023204'+ip_hex+'370501060f212cff',\
                   'ip_src':'0.0.0.0','ip_dest':'255.255.255.255','mac_dest':'ffffffffffff','ciaddr':'0.0.0.0'},\
          'release':{'opcode':7,'dhcp_options':'360402020202','ip_src':ip_require,'ip_dest':'2.2.2.2',\
                    'mac_dest':'000000222222','ciaddr':ip_require},\
          'rebind':{'opcode':3,'dhcp_options':'3604020202023204'+ip_hex+'370501060f212cff',\
                  'ip_src':'0.0.0.0','ip_dest':'255.255.255.255','mac_dest':'ffffffffffff','ciaddr':ip_require}}

    default_send_param=["ta_protocol=DHCP","side=user","bootp_dhcp=DHCP","bootp_type=BOOT_REQUEST",\
                            "mac_src=%s" % client_mac,"mac_dest=%s" % dhcp[message_type]['mac_dest'],\
                            "dhcp_message_type=%s" % dhcp[message_type]['opcode'],"client_addr=%s" % \
                            dhcp[message_type]['ciaddr'],"chaddr=%s" % client_mac,"ip_dest=%s" % \
                            dhcp[message_type]['ip_dest'],"ip_src=%s" % dhcp[message_type]['ip_src'],\
                            "dhcp_options=%s" % dhcp[message_type]['dhcp_options'],"message_count=5"]
    default_capt_param=["ta_protocol=DHCP","side=network","mac_src=%s" % client_mac, "mac_dest=%s" % \
                            dhcp[message_type]['mac_dest']]
    param_list=_get_param_list(send_param,capt_param,default_send_param,\
                                   default_capt_param,client_vlan_list,network_vlan_list)
  
    ### start send traffic logic
    _send_traffic_logic(param_list[0],param_list[1],expect_result,traffic_times,delay_time)

def send_downstream_dhcp_traffic(expect_result,ip_require,message_type,network_vlan="", \
     client_vlan="",lease_time=3600,send_param="",capt_param="",comments="",client_mac="",traffic_times=2):
    """
    - *expect_result:*  PASS/FAIL
    - *ip_require:* ip address required for dhcp. Example: 1.1.1.1
    - *message_type:* dhcp message type. Exapme: discover/offer/request/ack/renew/release/
    - *client_vlan:* vlan used when you send the packet. Example: 100 or 200,100 or 200,100,...,100
    - *network_vlan:* vlan to be compared when you captured.
    - *lease_time: * dhcp session lease time
    - *send_param:* to specify particular parameter values other than vlan to be sent
    - *capt_param:* to specify particular parameter values other than vlan to be captured
    - *comments:* description for test target of this traffic
    - *traffic_times:* times to re-try if traffic failed at first time
    - *delay_time:* time to sleep before stop capture (seconds)
     
    | send dwonstream dhcp traffic | PASS | 1.1.1.1 | offer | 11 | 12 | 
    |   | send_param=mac_src=000000600000 | capt_param=mac_src=000000600000 | traffic_times=4 | 
    """
    keyword="send_downstream_dhcp_traffic"
    logger.debug("%s:%s" % (__name__,keyword))  

    ip_hex=_ip_convert_decimal_to_hex(ip_require)
    lease_time=_time_convert_to_hex(lease_time)
    dhcp_options='3604020202023304'+lease_time+'ff'
    mac_src='000000222222'
    ip_src='2.2.2.2'

    client_vlan_list=_get_vlan_list(client_vlan)  
    network_vlan_list=_get_vlan_list(network_vlan)

    if client_mac == "":
        client_mac=_get_unique_mac(client_vlan_list[0],client_vlan_list[-1])

    dhcp={'offer':{'opcode':2,'ip_dest':'255.255.255.255','mac_dest':'ffffffffffff'},\
          'ack':{'opcode':5,'ip_dest':'255.255.255.255','mac_dest':'ffffffffffff'},\
          'nak':{'opcode':6,'ip_dest':'255.255.255.255','mac_dest':'ffffffffffff'},\
          'renewack':{'opcode':5,'ip_dest':ip_require,'mac_dest':client_mac}}

    default_send_param=["ta_protocol=DHCP","side=network","bootp_dhcp=DHCP","bootp_type=BOOT_REPLY",\
                            "mac_src=%s" % mac_src,"mac_dest=%s" % dhcp[message_type]['mac_dest'],\
                            "dhcp_message_type=%s" % dhcp[message_type]['opcode'],"client_addr=0.0.0.0",\
                            "next_serv_addr=%s" % ip_src,"relay_agent_addr=0.0.0.0","your_addr=%s" % ip_require,\
                            "chaddr=%s" % client_mac,"chaddr_lineup=false","ip_dest=%s" % \
                            dhcp[message_type]['ip_dest'],"ip_src=%s" % ip_src,\
                            "dhcp_options=%s" % dhcp_options,"message_count=2"]
    default_capt_param=["ta_protocol=DHCP","side=user","mac_src=%s" % mac_src, "mac_dest=%s" % \
                            dhcp[message_type]['mac_dest']]
    param_list=_get_param_list(send_param,capt_param,default_send_param,\
                                   default_capt_param,network_vlan_list,client_vlan_list)
   
    ### start send traffic logic
    _send_traffic_logic(param_list[0],param_list[1],expect_result,traffic_times)

def send_upstream_dhcpv6_traffic (expect_result,ip_require,message_type,client_vlan="", \
                              network_vlan="",send_param="",capt_param="",comments="",client_mac="",traffic_times=2,delay_time=1,xid='1'):
    """
    - *expect_result:*  PASS/FAIL
    - *ip_require:* ip address required for dhcp. Example: 1.1.1.1
    - *message_type:* dhcp message type. Exapme: discover/offer/request/ack/renew/release/
    - *client_vlan:* vlan used when you send the packet. Example: 100 or 200,100 or 200,100,...,100
    - *network_vlan:* vlan to be compared when you captured.
    - *send_param:* to specify particular parameter values other than vlan to be sent
    - *capt_param:* to specify particular parameter values other than vlan to be captured
    - *comments:* description for test target of this traffic
    - *traffic_times:* times to re-try if traffic failed at first time
    - *delay_time:* time to sleep before stop capture (seconds)
    - *xid:* transaction id
     
    | send upstream dhcp traffic | PASS | :: | solicit | 11 | 12 | 
    |   | send_param=mac_src=000000600000 | capt_param=mac_src=000000600000 | traffic_times=4 | 
    
    """
    keyword="send_upstream_dhcpv6_traffic"
    logger.debug("%s:%s" % (__name__,keyword))  
    xid_hex = _time_convert_to_hex(int(xid),6) 
    client_vlan_list=_get_vlan_list(client_vlan)  
    network_vlan_list=_get_vlan_list(network_vlan)
    option_iana = ""
    logger.debug("mac is %s" %client_mac)
    if client_mac == "":
        client_mac=_get_unique_mac(client_vlan_list[0],client_vlan_list[-1])

    m=re.search("mac_src=(\w+)",send_param)
    if m:
        client_mac=m.group(1)
    try:
        relay,message_type=message_type.split("-")
    except:
        relay = message_type  
 
    ip_src = hlk_ip_address_convertion.get_ipv6_linklocal_addr(client_mac)
    ip_req_list = ip_require.split(",")
    index = 0
    for ip_req in ip_req_list:
        index_hex = _time_convert_to_hex(index)
        try:
            ip_req,prefix_len = ip_req.split("/")
            ip_req_hex = hlk_ip_address_convertion.convert_to_mib_ipv6_str(ip_req)
            prefix_len_hex = _time_convert_to_hex(prefix_len,2)
            option_iana = option_iana+'00190029'+index_hex+'0000000000000000001a0019'+'0000000000000000'+prefix_len_hex+ip_req_hex
            option_iana_len = 45
        except:
            ip_req_hex = hlk_ip_address_convertion.convert_to_mib_ipv6_str(ip_req)
            option_iana = option_iana+'00030028'+index_hex+'000000000000000000050018'+ip_req_hex+'0000000000000000'
            option_iana_len = 44
        index += 1
        len_iana = index * option_iana_len

    option_elapseTime = "000800020000"
    len_relay = 42 + len_iana
    len_relay_solicit = len_relay - 14
    len_relay_hex = _time_convert_to_hex(len_relay,4)
    len_relay_solicit_hex = _time_convert_to_hex(len_relay_solicit,4)

    dhcp={'solicit':{'opcode':1,'src_port':546,'dhcp_options':'0001000e0001000100000000'+client_mac+option_elapseTime+option_iana},\
          'rapidSolicit':{'opcode':1,'src_port':546,'dhcp_options':'0001000e0001000100000000'+client_mac+option_elapseTime+option_iana+'000e0000'},\
          'request':{'opcode':3,'src_port':546,'dhcp_options':'0001000e0001000100000000'+client_mac+'0002000a00030001000000222222'+option_elapseTime+option_iana},\
          'release':{'opcode':8,'src_port':546,'dhcp_options':'0001000e0001000100000000'+client_mac+'0002000a00030001000000222222'+option_elapseTime+option_iana},\
          'renew':{'opcode':5,'src_port':546,'dhcp_options':'0001000e0001000100000000'+client_mac+'0002000a00030001000000222222'+option_elapseTime+option_iana},\
          'rebind':{'opcode':6,'src_port':546,'dhcp_options':'0001000e0001000100000000'+client_mac+'0002000a00030001000000222222'+option_elapseTime+option_iana},\
          'infoRequest':{'opcode':11,'src_port':546,'dhcp_options':'0001000e0001000100000000'+client_mac+'0006000400170018'},\
          'decline':{'opcode':9,'src_port':546,'dhcp_options':'0001000e0001000100000000'+client_mac+'0002000a00030001000000222222'+option_iana},\
          'relayedSolicit':{'opcode':12,'src_port':547,'dhcp_options':'0009'+len_relay_solicit_hex+'01' + xid_hex + '0001000e0001000100000000'+client_mac+option_elapseTime+option_iana},\
          'relayedRequest':{'opcode':12,'src_port':547,'dhcp_options':'0009'+len_relay_hex+'03' + xid_hex + '0001000e0001000100000000'+client_mac+'0002000a00030001000000222222'+option_elapseTime+option_iana},\
          'relayedRelease':{'opcode':12,'src_port':547,'dhcp_options':'0009'+len_relay_hex+'08' + xid_hex + '0001000e0001000100000000'+client_mac+'0002000a00030001000000222222'+option_elapseTime+option_iana},\
          'relay':{'opcode':12}}

    default_send_param=["ta_protocol=DHCPV6","side=user","udp_src_port=%s" % dhcp[message_type]['src_port'],"hop_count=0",\
                            "mac_src=%s" % client_mac,"mac_dest=333300010002",\
                            "dhcp_message_type=%s" % dhcp[message_type]['opcode'],
                            "ip_dest=ff02:0:0:0:0:0:1:2","ip_src=%s" % ip_src,"peer_addr=%s" % ip_src,\
                            "dhcp_options=%s" % dhcp[message_type]['dhcp_options'],"message_count=2"]
    if not relay == 'relay' :
        default_send_param.append("transaction_id=%s" %xid) 
    logger.debug('client_mac is%s' %client_mac)
    logger.debug('dhcpoptionis%s' %dhcp[message_type]['dhcp_options'])
    logger.debug('send param:%s' %str(default_send_param))
    default_capt_param=["ta_protocol=DHCPV6","side=network","mac_src=%s" % client_mac,"mac_dest=333300010002"]
    param_list=_get_param_list(send_param,capt_param,default_send_param,\
                                   default_capt_param,client_vlan_list,network_vlan_list)
  
    ### start send traffic logic
    _send_traffic_logic(param_list[0],param_list[1],expect_result,traffic_times,delay_time)

def send_downstream_dhcpv6_traffic(expect_result,ip_require,message_type,network_vlan="", \
                              client_vlan="",lease_time=3600,send_param="",capt_param="",comments="",client_mac="",traffic_times=2,delay_time=1,xid='1'):
    """
    - *expect_result:*  PASS/FAIL
    - *ip_require:* ip address required for dhcp. Example: 1.1.1.1
    - *message_type:* dhcp message type. Exapme: discover/offer/request/ack/renew/release/
    - *client_vlan:* vlan used when you send the packet. Example: 100 or 200,100 or 200,100,...,100
    - *network_vlan:* vlan to be compared when you captured.
    - *lease_time: * dhcp session lease time
    - *send_param:* to specify particular parameter values other than vlan to be sent
    - *capt_param:* to specify particular parameter values other than vlan to be captured
    - *comments:* description for test target of this traffic
    - *traffic_times:* times to re-try if traffic failed at first time
    - *delay_time:* time to sleep before stop capture (seconds)
    - *xid:* transaction id to match request and reply
     
    | send dwonstream dhcp traffic | PASS | 2002::2 | advertise | 11 | 12 | 
     
    """
    keyword="send_downstream_dhcpv6_traffic"
    logger.debug("%s:%s" % (__name__,keyword))  

    logger.debug("mac is %s" %client_mac)
    mac_src='000000222222'
    option_iana=""
    client_vlan_list=_get_vlan_list(client_vlan)  
    network_vlan_list=_get_vlan_list(network_vlan)
    if client_mac == "":
        client_mac=_get_unique_mac(client_vlan_list[0],client_vlan_list[-1])

    ip_src = hlk_ip_address_convertion.get_ipv6_linklocal_addr(mac_src)
    ip_dest = hlk_ip_address_convertion.get_ipv6_linklocal_addr(client_mac)  
    t1 = int(lease_time) / 2
    t2 = int(lease_time) * 3 / 4
    lease_time = _time_convert_to_hex(lease_time)
    t1 = _time_convert_to_hex(t1)
    t2 = _time_convert_to_hex(t2)

    xid_hex = _time_convert_to_hex(int(xid),6)
    ip_req_list = ip_require.split(",")
    index = 0
    len_iana = 0
    for ip_req in ip_req_list:
        index_hex = _time_convert_to_hex(index)
        try:
            ip_req,prefix_len = ip_req.split("/")
            ip_req_hex = hlk_ip_address_convertion.convert_to_mib_ipv6_str(ip_req)
            prefix_len_hex = _time_convert_to_hex(prefix_len,2)
            option_iana = option_iana+'00190029'+index_hex+t1+t2+'001a0019'+t2+lease_time+prefix_len_hex+ip_req_hex
            option_iana_len = 45
        except:
            ip_req_hex = hlk_ip_address_convertion.convert_to_mib_ipv6_str(ip_req)
            option_iana = option_iana+'00030028'+index_hex+t1+t2+'00050018'+ip_req_hex+t2+lease_time
            option_iana_len = 44
        index += 1 
        len_iana = len_iana + option_iana_len
    option_dns = "00170010"+hlk_ip_address_convertion.convert_to_mib_ipv6_str("1000::35")
    try:
        relay,message=message_type.split("-")
    except:
        relay=message_type
    len_relay = 36 + len_iana
    len_relay_rapid = len_relay + 4
    len_relay_hex = _time_convert_to_hex(len_relay,4)
    len_relay_rapid_hex = _time_convert_to_hex(len_relay_rapid,4)
    #client_mac='001094000001'
    #mac_src='001094000002'
    dhcp={'advertise':{'opcode':2,'dest_port':546,'dhcp_options':'0001000e0001000100000000'+client_mac+'0002000a00030001'+mac_src+option_iana},\
          'reply':{'opcode':7,'dest_port':546,'dhcp_options':'0001000e0001000100000000'+client_mac+'0002000a00030001'+mac_src+option_iana},\
          'infoReply':{'opcode':7,'dest_port':546,'dhcp_options':'0001000e0001000100000000'+client_mac+'0002000a00030001'+mac_src+option_dns},\
          'rapidReply':{'opcode':7,'dest_port':546,'dhcp_options':'0001000e0001000100000000'+client_mac+'0002000a00030001'+mac_src+option_iana+'000e0000'},\
          'relay-infoReply':{'opcode':13,'dest_port':547,'dhcp_options':'0009003807' + xid_hex + '0001000e0001000100000000'+client_mac+'0002000a00030001'+mac_src+option_dns},\
          'relay-reply':{'opcode':13,'dest_port':547,'dhcp_options':'0009'+len_relay_hex+'07' + xid_hex + '0001000e0001000100000000'+client_mac+'0002000a00030001'+mac_src+option_iana},\
          'relay-rapidReply':{'opcode':13,'dest_port':547,'dhcp_options':'0009'+len_relay_rapid_hex+'07' + xid_hex + '0001000e0001000100000000'+client_mac+'0002000a00030001'+mac_src+option_iana+'000e0000'},\
          'relay-advertise':{'opcode':13,'dest_port':547,'dhcp_options':'0009'+len_relay_hex+'02' + xid_hex + '0001000e0001000100000000'+client_mac+'0002000a00030001'+mac_src+option_iana}}

    default_send_param=["ta_protocol=DHCPV6","side=network","peer_addr=%s" % ip_dest,"hop_count=0",\
                            "mac_src=%s" % mac_src,"mac_dest=%s" % client_mac,\
                            "dhcp_message_type=%s" % dhcp[message_type]['opcode'],\
                            "dhcp_options=%s" % dhcp[message_type]['dhcp_options'],\
                            "ip_dest=%s" % ip_dest,"ip_src=%s" % ip_src,"udp_src_port=547",\
                            "udp_dest_port=%s" % dhcp[message_type]['dest_port'],"message_count=2"]
    if not relay == 'relay' :
        default_send_param.append("transaction_id=%s" %xid) 
    default_capt_param=["ta_protocol=DHCPV6","side=user","mac_src=%s" % mac_src, "mac_dest=%s" % client_mac]
    param_list=_get_param_list(send_param,capt_param,default_send_param,\
                                   default_capt_param,network_vlan_list,client_vlan_list)
   
    ### start send traffic logic
    _send_traffic_logic(param_list[0],param_list[1],expect_result,traffic_times,delay_time)


def send_upstream_icmpv6_traffic(expect_result,ip_require,message_type,client_vlan="", \
                              network_vlan="",send_param="",capt_param="",comments="",traffic_times=2,delay_time=1):
    """
    - *expect_result:* PASS/FAIL
    - *ip_require:* ipv6 address 
    - *message_type:* icmpv6 message type. Exapme: free_neighbor_solicit/neighbor_solicit/neighbor_advertise/router_solicit/router_advertise
    - *client_vlan:* vlan used when you send the packet. Example: 100 or 200,100 or 200,100,...,100
    - *network_vlan:* vlan to be compared when you captured.
    - *send_param:* specify a particular parameter value to be sent.
    - *capt_param:* specify a particular parameter value to be compared. 
    - *delay_time:* time to sleep before stop capture (seconds)
    
    | send upstream icmpv6 traffic | PASS | 3000:1 |
    
    """
    keyword="send_upstream_icmpv6_traffic"
    logger.debug("%s:%s" % (__name__,keyword))  
  
    client_vlan_list=_get_vlan_list(client_vlan)  
    network_vlan_list=_get_vlan_list(network_vlan)

    client_mac=_get_unique_mac(client_vlan_list[0],client_vlan_list[-1])

    m=re.search("mac_src=(\w+)",send_param)
    if m:
        client_mac=m.group(1)
    mac_dest = "000000222222"

    if message_type == "free_neighbor_solicit":
        ip_src = hlk_ip_address_convertion.convert_to_mib_ipv6_str("::",":")
    else :
        ip_src = hlk_ip_address_convertion.get_ipv6_linklocal_addr(client_mac)

    ip_dest_solicit = hlk_ip_address_convertion.get_ipv6_solicit_node_addr(ip_require)
    ip_dest_linklocal = hlk_ip_address_convertion.get_ipv6_linklocal_addr(mac_dest)
    mac_dest_solicit = hlk_ip_address_convertion.get_ipv6_mcast_mac(ip_require)
    target_addr = hlk_ip_address_convertion.convert_to_mib_ipv6_str(ip_require,":")

    icmpv6={'free-neighbor-solicit':{'type':135,'ip_dest':ip_dest_solicit,'mac_dest':mac_dest_solicit,'target_addr':target_addr,'options':0,'flag':0},\
          'neighbor-solicit':{'type':135,'ip_dest':ip_dest_solicit,\
                              'mac_dest':mac_dest_solicit,'target_addr':target_addr,'options':'0101'+client_mac,'flag':0},
          'neighbor-advertise':{'type':136,'ip_dest':ip_dest_linklocal,'mac_dest':mac_dest,\
                                'target_addr':target_addr,'options':'0201'+client_mac,'flag':"6"},
          'router-solicit':{'type':133,'ip_dest':ip_dest_solicit,\
                              'mac_dest':mac_dest_solicit,'target_addr':target_addr,'options':'0101'+client_mac,'flag':6},
          'router-advertise':{'type':134,'ip_dest':ip_dest_linklocal,'mac_dest':mac_dest,\
                                'target_addr':target_addr,'options':'0101'+client_mac,'flag':"c0"},
          'router-redirect':{'type':137,'ip_dest':ip_dest_solicit,'mac_dest':mac_dest,\
                                'target_addr':target_addr,'options':'0408'+client_mac,'flag':"c0"}
}

    default_send_param=["ta_protocol=ICMPV6","side=user","target_addr=%s" % icmpv6[message_type]['target_addr'],\
                            "mac_src=%s" % client_mac,"mac_dest=%s" % icmpv6[message_type]['mac_dest'],\
                            "icmpv6_type=%s" % icmpv6[message_type]['type'],"icmpv6_flags=%s" % icmpv6[message_type]['flag'],\
                            "ip_dest=%s" % icmpv6[message_type]['ip_dest'],"ip_src=%s" % ip_src,\
                            "icmpv6_options=%s" % icmpv6[message_type]['options'],"message_count=2"]
    default_capt_param=["ta_protocol=ICMPV6","side=network","mac_src=%s" % client_mac,"mac_dest=%s" % icmpv6[message_type]['mac_dest'],\
                        "icmpv6_message_type=%s" % icmpv6[message_type]['type']]

    # need update ipv6 address since PCTA do not support abbreviation
    ret = re.search("ip_src=((\w|\:)*)",send_param)
    if ret:
        ipv6_orig = ret.group(1)
        ipv6_full = hlk_ip_address_convertion.convert_to_mib_ipv6_str(ipv6_orig,":")
        send_param = re.sub(ipv6_orig,ipv6_full,send_param)
    ret = re.search("ip_dest=((\w|\:)*)",send_param)
    if ret:
        ipv6_orig = ret.group(1)
        ipv6_full = hlk_ip_address_convertion.convert_to_mib_ipv6_str(ipv6_orig,":")
        send_param = re.sub(ipv6_orig,ipv6_full,send_param)

    param_list=_get_param_list(send_param,capt_param,default_send_param,\
                                   default_capt_param,client_vlan_list,network_vlan_list)
  
    ### start send traffic logic
    _send_traffic_logic(param_list[0],param_list[1],expect_result,traffic_times,delay_time)


def send_downstream_icmpv6_traffic(expect_result,ip_require,message_type,network_vlan="", \
                              client_vlan="",send_param="",capt_param="",comments="",traffic_times=2,delay_time=1):
    """
    - *expect_result:* PASS/FAIL
    - *ip_require:* ipv6 address for neighbor discovery. 
    - *message_type:* icmpv6 message type. Exapme: free_neighbor_solicit/neighbor_solicit/neighbor_advertise/router_solicit/router_advertise
    - *client_vlan:* vlan used when you send the packet. Example: 100 or 200,100 or 200,100,...,100
    - *network_vlan:* vlan to be compared when you captured.
    - *send_param:* useful if want to specify a particular parameter value to be sent. Example: send_param=mac_src=000000123456,mac_dest=000000222222,...
    - *capt_param:* useful if want to specify a particular parameter value to be compared.  
    - *delay_time:* time to sleep before stop capture (seconds) 
    
    | send downstream icmpv6 traffic | PASS | 2000::1 | neighbor-solicit | 11,12 | 13 | 
    |   | comments=send downstream icmpv6 traffic expect pass |

    """
    keyword="send_downstream_icmpv6_traffic"
    logger.debug("%s:%s" % (__name__,keyword))  

    mac_src='000000222222'
    client_vlan_list=_get_vlan_list(client_vlan)  
    network_vlan_list=_get_vlan_list(network_vlan)
    client_mac=_get_unique_mac(client_vlan_list[0],client_vlan_list[-1])

    if message_type == "free_neighbor_solicit":
        ip_src = hlk_ip_address_convertion.convert_to_mib_ipv6_str("::",":")
    else :
        ip_src = hlk_ip_address_convertion.get_ipv6_linklocal_addr(mac_src)

    ip_dest_solicit = hlk_ip_address_convertion.get_ipv6_solicit_node_addr(ip_require)
    ip_dest_linklocal = hlk_ip_address_convertion.get_ipv6_linklocal_addr(client_mac)
    mac_dest_solicit = hlk_ip_address_convertion.get_ipv6_mcast_mac(ip_require)
    target_addr = hlk_ip_address_convertion.convert_to_mib_ipv6_str(ip_require,":")

    icmpv6={'free-neighbor-solicit':{'type':135,'ip_dest':ip_dest_solicit,'mac_dest':mac_dest_solicit,'target_addr':target_addr,'options':0,'flag':0},\
          'neighbor-solicit':{'type':135,'ip_dest':ip_dest_solicit,\
                              'mac_dest':mac_dest_solicit,'target_addr':target_addr,'options':'0101'+mac_src,'flag':0},
          'neighbor-advertise':{'type':136,'ip_dest':ip_dest_linklocal,'mac_dest':client_mac,\
                                'target_addr':target_addr,'options':'0201'+mac_src,'flag':"6"},
          'router-solicit':{'type':133,'ip_dest':ip_dest_solicit,\
                              'mac_dest':mac_dest_solicit,'target_addr':target_addr,'options':'0101'+mac_src,'flag':0},
          'router-advertise':{'type':134,'ip_dest':ip_dest_linklocal,'mac_dest':client_mac,\
                                'target_addr':target_addr,'options':'0304'+'3f:0e:20::/64','flag':"c0"},
          'router-redirect':{'type':137,'ip_dest':ip_dest_linklocal,'mac_dest':client_mac,\
                                'target_addr':target_addr,'options':'0408'+mac_src,'flag':"c0"}
}

    default_send_param=["ta_protocol=ICMPV6","side=network","target_addr=%s" % icmpv6[message_type]['target_addr'],\
                            "mac_src=%s" % mac_src,"mac_dest=%s" % icmpv6[message_type]['mac_dest'],\
                            "icmpv6_type=%s" % icmpv6[message_type]['type'],"icmpv6_flags=%s" % icmpv6[message_type]['flag'],\
                            "ip_dest=%s" % icmpv6[message_type]['ip_dest'],"ip_src=%s" % ip_src,\
                            "icmpv6_options=%s" % icmpv6[message_type]['options'],"message_count=2"]
    default_capt_param=["ta_protocol=ICMPV6","side=user","mac_src=%s" % mac_src,"mac_dest=%s" % icmpv6[message_type]['mac_dest'],\
                        "icmpv6_message_type=%s" % icmpv6[message_type]['type']]

    # need update ipv6 address since PCTA do not support abbreviation
    ret = re.search("ip_src=((\w|\:)*)",send_param)
    if ret:
        ipv6_orig = ret.group(1)
        ipv6_full = hlk_ip_address_convertion.convert_to_mib_ipv6_str(ipv6_orig,":")
        send_param = re.sub(ipv6_orig,ipv6_full,send_param)
    ret = re.search("ip_dest=((\w|\:)*)",send_param)
    if ret:
        ipv6_orig = ret.group(1)
        ipv6_full = hlk_ip_address_convertion.convert_to_mib_ipv6_str(ipv6_orig,":")
        send_param = re.sub(ipv6_orig,ipv6_full,send_param)

    param_list=_get_param_list(send_param,capt_param,default_send_param,\
                                   default_capt_param,network_vlan_list,client_vlan_list)
   
    ### start send traffic logic
    _send_traffic_logic(param_list[0],param_list[1],expect_result,traffic_times,delay_time)


def send_upstream_ethernet_traffic(expect_result,client_vlan="",network_vlan="",send_param="",capt_param="",comments="",traffic_times=2,delay_time=1):
    """
    - *expect_result:*  PASS/FAIL
    - *client_vlan:* vlan used when you send the packet. Example: 100 or 200,100 or 200,100,...,100
    - *network_vlan:* vlan to be compared when you captured.
    - *send_param:* to specify particular parameter values other than vlan to be sent
    - *capt_param:* to specify particular parameter values other than vlan to be captured
    - *comments:* description for test target of this traffic
    - *traffic_times:* times to re-try if traffic failed at first time
    - *delay_time:* time to delay before stop capture (seconds)
    
    | send upstream ethernet traffic | PASS | 11,12 | 13 | 

    """
    keyword="send_upstream_ethernet_traffic"
    logger.debug("%s:%s" % (__name__,keyword))  
    
    client_vlan_list=_get_vlan_list(client_vlan)  
    network_vlan_list=_get_vlan_list(network_vlan)
    client_mac=_get_unique_mac(client_vlan_list[0],client_vlan_list[-1])
 
    default_send_param=["ta_protocol=ETH","side=user","mac_src=%s" % client_mac,"frame_count=5"]
    default_capt_param=["ta_protocol=ETH","side=network","mac_src=%s" % client_mac]

    param_list=_get_param_list(send_param,capt_param,default_send_param,\
                                   default_capt_param,client_vlan_list,network_vlan_list)

    ### start send traffic logic
    _send_traffic_logic(param_list[0],param_list[1],expect_result,traffic_times,delay_time)

def send_downstream_ethernet_traffic(expect_result,network_vlan="",client_vlan="",send_param="",capt_param="",comments="",traffic_times=2,delay_time=1):
    """
    - *expect_result:*  PASS/FAIL
    - *client_vlan:* vlan used when you send the packet. Example: 100 or 200,100 or 200,100,...,100
    - *network_vlan:* vlan to be compared when you captured.
    - *send_param:* to specify particular parameter values other than vlan to be sent
    - *capt_param:* to specify particular parameter values other than vlan to be captured
    - *comments:* description for test target of this traffic
    - *traffic_times:* times to re-try if traffic failed at first time
    - *delay_time:* time to sleep before stop capture (seconds)
    
    | send_downstream_ethernet_traffic | PASS | 11,12 | 13 | send_param=mac_src=000000123456,mac_dest=000000222222 |
    
    """
    keyword="send_downstream_ethernet_traffic"
    logger.debug("%s:%s" % (__name__,keyword))  

    client_vlan_list=_get_vlan_list(client_vlan)  
    network_vlan_list=_get_vlan_list(network_vlan)
    client_mac=_get_unique_mac(client_vlan_list[0],client_vlan_list[-1])
  
    default_send_param=["ta_protocol=ETH","side=network","mac_dest=%s" % client_mac,"frame_count=2"]
    default_capt_param=["ta_protocol=ETH","side=user","mac_dest=%s" % client_mac]

    param_list=_get_param_list(send_param,capt_param,default_send_param,\
                                   default_capt_param,network_vlan_list,client_vlan_list)

    ### start send traffic logic
    _send_traffic_logic(param_list[0],param_list[1],expect_result,traffic_times,delay_time)

def send_upstream_pppoe_traffic(expect_result,message_type,\
client_vlan="",network_vlan="",send_param="",capt_param="",comments="",traffic_times=2,delay_time=1):
    """
    - *expect_result:* PASS/FAIL
    - *message_type:* pppoe,ppp,ppp-lcp or ppp-ipcp
    - *client_vlan:* vlan used when you send the packet. Example: 100 or 200,100 or 200,100,...,100
    - *network_vlan:* vlan to be compared when you captured.
    - *send_param:* specify a particular parameter value to be sent. 
    - *capt_param:* useful if want to specify a particular parameter value to be compared. 
    - *comments:* description for test target of this traffic
    - *traffic_times:* times to re-try if traffic failed at first time
    - *delay_time:* time to sleep before stop capture (seconds)
    
    | send upstream pppoe traffic | PASS | pppoe | 11,12 | 13 | comments=send upstream pppoe traffic expect to passed |

    """
    keyword="send_upstream_pppoe_traffic"
    logger.debug("%s:%s %s %s %s %s" % (__name__,keyword,expect_result,message_type,network_vlan,client_vlan)) 
    
    client_vlan_list=_get_vlan_list(client_vlan)  
    network_vlan_list=_get_vlan_list(network_vlan)
    client_mac=_get_unique_mac(client_vlan_list[0],client_vlan_list[-1])
    
    pppoe={'ppp':{'protocol':'PPP'},\
          'ppp-lcp':{'protocol':'PPP_LCP'},\
          'ppp-ipcp':{'protocol':'PPP_IPCP'},\
          'pppoe':{'protocol':'PPPOE'}}

    default_send_param=["ta_protocol=%s" % pppoe[message_type]['protocol'],"side=user","mac_src=%s" % client_mac,\
                        "mac_dest=000000222222"]
    default_capt_param=["ta_protocol=%s" % pppoe[message_type]['protocol'],"side=network",\
                        "mac_dest=000000222222","mac_src=%s" % client_mac]

    param_list=_get_param_list(send_param,capt_param,default_send_param,\
                                   default_capt_param,client_vlan_list,network_vlan_list)

    ### start send traffic logic
    _send_traffic_logic(param_list[0],param_list[1],expect_result,traffic_times,delay_time)

def send_downstream_pppoe_traffic(expect_result,message_type,network_vlan="",client_vlan="",send_param="",capt_param="",comments="",traffic_times=2,delay_time=1):
    """
    - *expect_result:* PASS/FAIL
    - *message_type:* pppoe,ppp,ppp-lcp or ppp-ipcp
    - *client_vlan:* vlan used when you send the packet. Example: 100 or 200,100 or 200,100,...,100
    - *network_vlan:* vlan to be compared when you captured.
    - *send_param:* specify a particular parameter value to be sent.
    - *capt_param:* useful if want to specify a particular parameter value to be compared. 
    - *comments:* description for test target of this traffic
    - *traffic_times:* times to re-try if traffic failed at first time
    - *delay_time:* time to sleep before stop capture (seconds)
    
    | send downstream pppoe traffic | PASS | ppp | 11,12 | 13 | comments=send downstream ppp traffic expect to passed |

    """
    keyword="send_downstream_pppoe_traffic"
    logger.debug("%s:%s %s %s %s %s" % (__name__,keyword,expect_result,message_type,network_vlan,client_vlan))  

    client_vlan_list=_get_vlan_list(client_vlan)  
    network_vlan_list=_get_vlan_list(network_vlan)
    client_mac=_get_unique_mac(client_vlan_list[0],client_vlan_list[-1])
    
    pppoe={'ppp':{'protocol':'PPP'},\
          'ppp-lcp':{'protocol':'PPP_LCP'},\
          'ppp-ipcp':{'protocol':'PPP_IPCP'},\
          'pppoe':{'protocol':'PPPOE'}}

    default_send_param=["ta_protocol=%s" % pppoe[message_type]['protocol'],"side=network",\
                        "mac_src=000000222222","mac_dest=%s" % client_mac]
    default_capt_param=["ta_protocol=%s" % pppoe[message_type]['protocol'],"side=user",\
                        "mac_src=000000222222","mac_dest=%s" % client_mac]

    param_list=_get_param_list(send_param,capt_param,default_send_param,\
                                   default_capt_param,network_vlan_list,client_vlan_list)

    ### start send traffic logic
    _send_traffic_logic(param_list[0],param_list[1],expect_result,traffic_times,delay_time)

def send_upstream_udp_traffic(expect_result,client_vlan="",network_vlan="",send_param="",capt_param="",comments="",traffic_times=2,delay_time=1):
    """
    - *expect_result:*  PASS/FAIL
    - *client_vlan:* vlan used when you send the packet. Example: 100 or 200,100 or 200,100,...,100
    - *network_vlan:* vlan to be compared when you captured.
    - *send_param:* to specify particular parameter values other than vlan to be sent
    - *capt_param:* to specify particular parameter values other than vlan to be captured
    - *comments:* description for test target of this traffic
    - *traffic_times:* times to re-try if traffic failed at first time
    - *delay_time:* time to sleep before stop capture (seconds)    
    
    | send upstream udp traffic | PASS | 11,12 | 13 |
    
    """
    keyword="send_upstream_udp_traffic"
    logger.debug("%s:%s" % (__name__,keyword))  
      
    client_vlan_list=_get_vlan_list(client_vlan)  
    network_vlan_list=_get_vlan_list(network_vlan)
    client_mac=_get_unique_mac(client_vlan_list[0],client_vlan_list[-1])

    default_send_param=["ta_protocol=UDP","side=user","eth_type=ETH","mac_src=%s" % client_mac,"message_count=5"]
    default_capt_param=["ta_protocol=UDP","side=network","mac_src=%s" % client_mac]
    param_list=_get_param_list(send_param,capt_param,default_send_param,\
                                   default_capt_param,client_vlan_list,network_vlan_list)

   ### start send traffic logic
    _send_traffic_logic(param_list[0],param_list[1],expect_result,traffic_times,delay_time)
    
def send_downstream_udp_traffic (expect_result,network_vlan="",client_vlan="",send_param="",capt_param="",comments="",traffic_times=2,delay_time=1,send_delay=0):
    """
    - *expect_result:*  PASS/FAIL
    - *client_vlan:* vlan used when you send the packet. Example: 100 or 200,100 or 200,100,...,100
    - *network_vlan:* vlan to be compared when you captured.
    - *send_param:* to specify particular parameter values other than vlan to be sent
    - *capt_param:* to specify particular parameter values other than vlan to be captured
    - *comments:* description for test target of this traffic
    - *traffic_times:* times to re-try if traffic failed at first time
    - *delay_time:* time to sleep before stop capture (seconds)    
    - *send_delay:* time delay between start capture and send packets
    
    | send downstream udp traffic | PASS | 11,12 | 13 | send_param=ip_src=1.1.1.2 |
    
    """
    keyword="send_downstream_udp_traffic"
    logger.debug("%s:%s" % (__name__,keyword))  

    client_vlan_list=_get_vlan_list(client_vlan)  
    network_vlan_list=_get_vlan_list(network_vlan)
    client_mac=_get_unique_mac(client_vlan_list[0],client_vlan_list[-1])

    default_send_param=["ta_protocol=UDP","side=network","eth_type=ETH","mac_dest=%s" % client_mac,"message_count=3"]
    default_capt_param=["ta_protocol=UDP","side=user","mac_dest=%s" % client_mac]
    param_list=_get_param_list(send_param,capt_param,default_send_param,\
                                   default_capt_param,network_vlan_list,client_vlan_list)
    
    ### start send traffic logic
    _send_traffic_logic(param_list[0],param_list[1],expect_result,traffic_times,delay_time,send_delay)
    
    
def send_upstream_tcp_traffic(expect_result,client_vlan="",network_vlan="",send_param="",capt_param="",comments="",traffic_times=2,delay_time=1):
    """
    - *expect_result:*  PASS/FAIL
    - *client_vlan:* vlan used when you send the packet. Example: 100 or 200,100 or 200,100,...,100
    - *network_vlan:* vlan to be compared when you captured.
    - *send_param:* to specify particular parameter values other than vlan to be sent
    - *capt_param:* to specify particular parameter values other than vlan to be captured
    - *comments:* description for test target of this traffic
    - *traffic_times:* times to re-try if traffic failed at first time
    - *delay_time:* time to sleep before stop capture (seconds)    
    
    | send upstream tcp traffic | PASS | 11,12 | 13 | send_param=ip_src=1.1.1.2 | capt_param=ip_src=1.1.1.2 |
    
    """
    keyword="send_upstream_tcp_traffic"
    logger.debug("%s:%s" % (__name__,keyword))  
      
    client_vlan_list=_get_vlan_list(client_vlan)  
    network_vlan_list=_get_vlan_list(network_vlan)
    client_mac=_get_unique_mac(client_vlan_list[0],client_vlan_list[-1])

    default_send_param=["ta_protocol=TCP","side=user","eth_type=ETH","mac_src=%s" % client_mac,"packet_count=5"]
    default_capt_param=["ta_protocol=TCP","side=network","mac_src=%s" % client_mac]
    param_list=_get_param_list(send_param,capt_param,default_send_param,\
                                   default_capt_param,client_vlan_list,network_vlan_list)

   ### start send traffic logic
    _send_traffic_logic(param_list[0],param_list[1],expect_result,traffic_times,delay_time)
    
def send_downstream_tcp_traffic(expect_result,network_vlan="",client_vlan="",send_param="",capt_param="",comments="",traffic_times=2,delay_time=1):
    """
    - *expect_result:*  PASS/FAIL
    - *client_vlan:* vlan used when you send the packet. Example: 100 or 200,100 or 200,100,...,100
    - *network_vlan:* vlan to be compared when you captured.
    - *send_param:* to specify particular parameter values other than vlan to be sent
    - *capt_param:* to specify particular parameter values other than vlan to be captured
    - *comments:* description for test target of this traffic
    - *traffic_times:* times to re-try if traffic failed at first time
    - *delay_time:* time to sleep before stop capture (seconds)    
    
    | send downstream tcp traffic | PASS | 11,12 | 13 |
    """
    keyword="send_downstream_tcp_traffic"
    logger.debug("%s:%s" % (__name__,keyword))  

    client_vlan_list=_get_vlan_list(client_vlan)  
    network_vlan_list=_get_vlan_list(network_vlan)
    client_mac=_get_unique_mac(client_vlan_list[0],client_vlan_list[-1])

    default_send_param=["ta_protocol=TCP","side=network","eth_type=ETH","mac_dest=%s" % client_mac,"packet_count=3"]
    default_capt_param=["ta_protocol=TCP","side=user","mac_dest=%s" % client_mac]
    param_list=_get_param_list(send_param,capt_param,default_send_param,\
                                   default_capt_param,network_vlan_list,client_vlan_list)
    
    ### start send traffic logic
    _send_traffic_logic(param_list[0],param_list[1],expect_result,traffic_times,delay_time)

def send_upstream_rip_traffic(expect_result,rip_version,rip_type,client_vlan="",network_vlan="",send_param="",capt_param="",comments="",traffic_times=3,delay_time=1):
    """
    - *expect_result:*  PASS/FAIL
    - *rip_version:*  v1/v2
    - *rip_type:*  request/response
    - *client_vlan:* vlan used when you send the packet. Example: 100 or 200,100 or 200,100,...,100
    - *network_vlan:* vlan to be compared when you captured.
    - *send_param:* to specify particular parameter values other than vlan to be sent
    - *capt_param:* to specify particular parameter values other than vlan to be captured
    - *comments:* description for test target of this traffic
    - *traffic_times:* times to re-try if traffic failed at first time 
    - *delay_time:* time to sleep before stop capture (seconds) 
    
    | send upstream rip traffic | PASS | v1 | response | 11 | 12,13 |
    
    
    """
    keyword="send_upstream_rip_traffic"
    logger.debug("%s:%s" % (__name__,keyword))  
      
    client_vlan_list=_get_vlan_list(client_vlan)  
    network_vlan_list=_get_vlan_list(network_vlan)
    client_mac=_get_unique_mac(client_vlan_list[0],client_vlan_list[-1])

    if rip_type == 'request':
        rip_command = 'REQ'
        rip_command_capt = 1
    else:
        rip_command = 'RES'
        rip_command_capt = 2

    rip={'v1':{'version':'VER1','ip_dest':'255.255.255.255','mac_dest':'ffffffffffff'},\
          'v2':{'version':'VER2','ip_dest':'224.0.0.9','mac_dest':'01005e000009'}}

    default_send_param=["ta_protocol=RIP","side=user","eth_type=ETH","mac_src=%s" % client_mac,"mac_dest=%s" % \
                        rip[rip_version]['mac_dest'],"ip_dest=%s" % rip[rip_version]['ip_dest'], "rip_version=%s" % \
                        rip[rip_version]['version'],"rip_command=%s" % rip_command, \
                        "rip_entry_list=0002000005050505ffffff00010101030200000000000000010002","message_count=5"]
    default_capt_param=["ta_protocol=RIP","side=network","mac_src=%s" % client_mac,"rip_command=%s" % rip_command_capt]
    param_list=_get_param_list(send_param,capt_param,default_send_param,\
                                   default_capt_param,client_vlan_list,network_vlan_list)
    rep_send_param_list=[]
    rep_capt_param_list=[]
    for single_param in param_list[0]:
        single_param=re.sub("vlan1","c_vlan",single_param)
        single_param=re.sub("vlan2","s_vlan",single_param)
        rep_send_param_list.append(single_param)
    for single_param in param_list[1]:
        single_param=re.sub("vlan1","c_vlan",single_param)
        single_param=re.sub("vlan2","s_vlan",single_param)
        rep_capt_param_list.append(single_param)

   ### start send traffic logic
    _send_traffic_logic(rep_send_param_list,rep_capt_param_list,expect_result,traffic_times,delay_time)
    
def send_downstream_rip_traffic(expect_result,rip_version,rip_type,network_vlan="",client_vlan="",send_param="",capt_param="",comments="",traffic_times=2,delay_time=1):
    """
    - *expect_result:*  PASS/FAIL
    - *rip_version:*  v1/v2
    - *rip_type:*  request/response
    - *client_vlan:* vlan used when you send the packet. Example: 100 or 200,100 or 200,100,...,100
    - *network_vlan:* vlan to be compared when you captured.
    - *send_param:* to specify particular parameter values other than vlan to be sent
    - *capt_param:* to specify particular parameter values other than vlan to be captured
    - *comments:* description for test target of this traffic
    - *traffic_times:* times to re-try if traffic failed at first time
    - *delay_time:* time to sleep before stop capture (seconds)
    
    | send downstream rip traffic | PASS | v1 | response | 11,12 | 13 | comments=downstream of rip expect pass | 
    
    """
    keyword="send_downstream_rip_traffic"
    logger.debug("%s:%s" % (__name__,keyword))  

    client_vlan_list=_get_vlan_list(client_vlan)  
    network_vlan_list=_get_vlan_list(network_vlan)

    if rip_type == 'request':
        rip_command = 'REQ'
        rip_command_capt = 1
    else:
        rip_command = 'RES'
        rip_command_capt = 2

    rip={'v1':{'version':'VER1','ip_dest':'255.255.255.255','mac_dest':'ffffffffffff'},\
          'v2':{'version':'VER2','ip_dest':'224.0.0.9','mac_dest':'01005e000009'}}

    default_send_param=["ta_protocol=RIP","side=network","eth_type=ETH","mac_src=000000222222","mac_dest=%s" % \
                        rip[rip_version]['mac_dest'],"ip_dest=%s" % rip[rip_version]['ip_dest'], "rip_version=%s" % \
                        rip[rip_version]['version'],"rip_command=%s" % rip_command, \
                        "rip_entry_list=0002000005050505ffffff00010101030200000000000000010002","message_count=5"]
    default_capt_param=["ta_protocol=RIP","side=user","mac_src=000000222222","rip_command=%s" % rip_command_capt]
    param_list=_get_param_list(send_param,capt_param,default_send_param,\
                               default_capt_param,network_vlan_list,client_vlan_list)

    rep_send_param_list=[]
    rep_capt_param_list=[]
    for single_param in param_list[0]:
        single_param=re.sub("vlan1","c_vlan",single_param)
        single_param=re.sub("vlan2","s_vlan",single_param)
        rep_send_param_list.append(single_param)
    for single_param in param_list[1]:
        single_param=re.sub("vlan1","c_vlan",single_param)
        single_param=re.sub("vlan2","s_vlan",single_param)
        rep_capt_param_list.append(single_param)
    
    ### start send traffic logic
    _send_traffic_logic(rep_send_param_list,rep_capt_param_list,expect_result,traffic_times,delay_time)

def get_rip_entry(ip_address,metric=1,subnet='0.0.0.0',next_hop='0.0.0.0'):
    ip_dest=_ip_convert_decimal_to_hex(ip_address)
    net_mask=_ip_convert_decimal_to_hex(subnet)
    next_hop=_ip_convert_decimal_to_hex(next_hop)
    metric=hex(int(metric))
    metric=metric.replace("0x","")
    metric=metric.rjust(2,"0")
    return '00020000'+ip_dest+net_mask+next_hop+metric+'0000000000000001'     


def send_downstream_igmpv2_traffic (client_vlan,network_vlan,igmp_grp_addr,igmp_type,**params) :

    """
    - *client_vlan:* vlan used when you send the packet. Example: "100" or "200,100" or "200,100,300"
    - *network_vlan:* vlan to be compared when you captured
    - *igmp_grp_addr:* Multicast group ip    
    - *igmp_type:* IGMP type, "11" for GMP or GSQ
    - *send_param:* to specify particular parameter values other than vlan to be sent
    - *capt_param:* to specify particular parameter values other than vlan to be captured   
    - *comments:* description for test target of this traffic
    - *traffic_times:* times to re-try if traffic failed at first time 
    - *delay_time:* times to delay before stop capture     
    - *expect_result:*  PASS/FAIL, default is PASS 
    
    | send_downstream_igmpv2_traffic | 100,200 | 300 | 0.0.0.0 | 11 | comments="send igmp GMQ message" |
    | send_downstream_igmpv2_traffic | 100,200 | 300 | 224.205.155.11 | 11 | send_param=mac_src=000102030405,ip_src=10.1.1.2 |
    |   | capt_param=mac_src=000102030405,ip_grp_addr=224.205.155.11 | expect_result=PASS | traffic_times = 1 | 
    |   | comments="send igmp GSQ message" |    

    """
    keyword="send_downstream_igmpv2_traffic"
    logger.debug ("%s: %s,%s,%s,%s, %s" % (keyword,client_vlan,network_vlan,igmp_grp_addr,igmp_type,str(params)))
    
    traffic_times = params.setdefault('traffic_times',3)
    delay_time    = params.setdefault('delay_time',1)
    expect_result = params.setdefault('expect_result','PASS') 
    send_param    = params.setdefault('send_param','') 
    capt_param    = params.setdefault('capt_param','')
	
    client_vlan_list=_get_vlan_list(client_vlan)
    network_vlan_list=_get_vlan_list(network_vlan)
    client_mac=_get_unique_mac(client_vlan_list[0],client_vlan_list[-1])

    default_send_param = ["ta_protocol=IGMP","igmp_version=V2","side=network",\
                        "mac_src="+client_mac,"igmp_grp_addr="+igmp_grp_addr,\
                        "igmp_type="+igmp_type]                  
    if "mac_dest=" not in send_param :
        mac_dest = _create_mac_dest_by_group_ip(igmp_grp_addr)    
        default_send_param.append ("mac_dest="+mac_dest)    
        
    if "ip_dest=" not in send_param :
        if igmp_grp_addr == "0.0.0.0" :
            ip_dest = "224.0.0.1"
        else :
            ip_dest = igmp_grp_addr
        default_send_param.append("ip_dest="+ip_dest)
                        						
    default_capt_param = ["ta_protocol=IGMP","side=user"]
    param_list = _get_param_list(send_param,capt_param,default_send_param,\
                                   default_capt_param,client_vlan_list,network_vlan_list)

    _send_traffic_logic(param_list[0],param_list[1],expect_result,traffic_times,delay_time)
    

def send_upstream_igmpv2_traffic (client_vlan,network_vlan,igmp_grp_addr,igmp_type,**params) :

    """
    - *client_vlan:* vlan used when you send the packet. Example: "100" or "200,100" or "200,100,300"
    - *network_vlan:* vlan to be compared when you captured
    - *igmp_grp_addr:* Multicast group ip    
    - *igmp_type:* IGMP type, "16" for join, "17" for leave  
    - *send_param:* to specify particular parameter values other than vlan to be sent
    - *capt_param:* to specify particular parameter values other than vlan to be captured   
    - *comments:* description for test target of this traffic
    - *traffic_times:* times to re-try if traffic failed at first time 
    - *delay_time:* times to delay before stop capture     
    - *expect_result:*  PASS/FAIL, default is PASS
     
    | send_upstream_igmpv2_traffic | 100,200 | 300 | 224.205.155.11 | 17 | comments="send igmp leave message" |
    | send_upstream_igmpv2_traffic | 100,200 | 300 | 224.205.155.11 | 16 | send_param=mac_src=000102030405,ip_src=10.1.1.2 |
    |   | capt_param=mac_src=000102030405,ip_grp_addr=224.205.155.11 | delay_time= 5 | traffic_times = 4 | 
    |   | comments="send igmp join message" |    

    """
    keyword="send_upstream_igmpv2_traffic"
    logger.debug ("%s: %s,%s,%s,%s, %s" % (keyword,client_vlan,network_vlan,igmp_grp_addr,igmp_type,str(params)))
    
    traffic_times = params.setdefault('traffic_times',3)
    delay_time    = params.setdefault('delay_time',1)
    expect_result = params.setdefault('expect_result','PASS') 
    send_param    = params.setdefault('send_param','') 
    capt_param    = params.setdefault('capt_param','')
	
    client_vlan_list=_get_vlan_list(client_vlan)
    network_vlan_list=_get_vlan_list(network_vlan)
    client_mac=_get_unique_mac(client_vlan_list[0],client_vlan_list[-1])

    default_send_param = ["ta_protocol=IGMP","igmp_version=V2","side=user",\
                        "mac_src="+client_mac,"igmp_grp_addr="+igmp_grp_addr,\
                        "igmp_type="+igmp_type]                  
    if "mac_dest=" not in send_param :
        if igmp_type == "16" :
            mac_dest = _create_mac_dest_by_group_ip(igmp_grp_addr)
        if igmp_type == "17" :
            mac_dest = _create_mac_dest_by_group_ip("224.0.0.2")     
        default_send_param.append ("mac_dest="+mac_dest)    
        
    if "ip_dest=" not in send_param :
        if igmp_type == "16" :
            ip_dest = igmp_grp_addr
        if igmp_type == "17" :
            ip_dest = "224.0.0.2"
        default_send_param.append("ip_dest="+ip_dest)
                        						
    default_capt_param = ["ta_protocol=IGMP","side=network"]
    param_list = _get_param_list(send_param,capt_param,default_send_param,\
                                   default_capt_param,client_vlan_list,network_vlan_list)

    _send_traffic_logic(param_list[0],param_list[1],expect_result,traffic_times,delay_time)
       
def _create_mac_dest_by_group_ip(igmp_grp_addr)  :
    if igmp_grp_addr == "0.0.0.0" :
        return "01005e000001"
    mac_dest="01005e"
    ip_list = igmp_grp_addr.split(".")
    field1 = int(ip_list[1]) % 128
    field1 = hex(field1).lstrip('0x')
    field1 = field1.zfill(2)
    field2 = int(ip_list[2])
    field2 = hex(field2).lstrip('0x')
    field2 = field2.zfill(2)
    field3 =  int(ip_list[3])      
    field3 = hex(field3).lstrip('0x')
    field3 = field3.zfill(2)
    mac_dest = mac_dest+field1+field2+field3
    return mac_dest       


def send_downstream_igmpv3_traffic (client_vlan,network_vlan,igmp_grp_addr,nb_of_sources,src_address_list="{0.0.0.0}",**params) :

    """
    - *client_vlan:* vlan used when you send the packet. Example: "100" or "200,100" or "200,100,300"
    - *network_vlan:* vlan to be compared when you captured
    - *igmp_grp_addr:* igmp group ip, 0.0.0.0 for GSQ 
    - *nb_of_sources:* number of source addresses, 0 for GQ and GSQ 
    - *src_address_list:* the multicast server ip address list
    - *send_param:* to specify particular parameter values other than vlan to be sent
    - *capt_param:* to specify particular parameter values other than vlan to be captured   
    - *comments:* description for test target of this traffic
    - *traffic_times:* times to re-try if traffic failed at first time 
    - *delay_time:* times to delay before stop capture     
    - *expect_result:*  PASS/FAIL, default is PASS 
    
    | send_downstream_igmpv3_traffic | 100,200 | 300 | 0.0.0.0 | 0 | 
    |   | send_param=qrv=2,qqic=125 | comments="send igmp GQ message" |
    | send_downstream_igmpv3_traffic | 100,200 | 300 | 225.1.1.4 | 2 |
    |   | {192.2.3.2 2.2.2.2} | send_param=igmp_max_resp_time=50,ip_src=10.1.1.2 | capt_param=mac_src=ip_grp_addr=225.1.1.4 | expect_result=FAIL | traffic_times = 1 | comments="send igmp GSSQ message" |    
    """
    keyword="send_downstream_igmpv3_traffic"
    logger.debug ("%s: %s,%s,%s,%s,%s,%s" % (keyword,client_vlan,network_vlan,igmp_grp_addr,nb_of_sources,src_address_list,str(params)))
    
    traffic_times = params.setdefault('traffic_times',3)
    delay_time    = params.setdefault('delay_time',1)
    expect_result = params.setdefault('expect_result','PASS') 
    send_param    = params.setdefault('send_param','') 
    capt_param    = params.setdefault('capt_param','')    
         	
    client_vlan_list=_get_vlan_list(client_vlan)
    network_vlan_list=_get_vlan_list(network_vlan)
    client_mac=_get_unique_mac(client_vlan_list[0],client_vlan_list[-1])

    if igmp_grp_addr == "0.0.0.0" :
        ip_dest = "224.0.0.1"
        mac_dest = "01005e000001"
    else :
        ip_dest = igmp_grp_addr
        mac_dest = _create_mac_dest_by_group_ip(igmp_grp_addr)
        
    default_send_param=["ta_protocol=IGMP","side=network","mac_src=%s" % client_mac, "mac_dest=%s" % mac_dest,\
                        "ip_dest=%s" % ip_dest, "igmp_version=V3", "igmp_type=11","igmp_grp_addr=%s" % igmp_grp_addr, \
                        "nb_of_sources=%s" % nb_of_sources]   
    if str(nb_of_sources) != "0" :       
        default_send_param.append ( "src_address_list=%s" % src_address_list )             
  			
    default_capt_param=["ta_protocol=IGMP","side=user"]
    param_list=_get_param_list(send_param,capt_param,default_send_param,\
				default_capt_param,client_vlan_list,network_vlan_list)

    # start send traffic logic
    _send_traffic_logic(param_list[0],param_list[1],expect_result,traffic_times,delay_time)    
    
    
def send_upstream_igmpv3_traffic (*args,**params) :
    """
    - *client_vlan:* vlan used when you send the packet. Example: 100 or 200,100 or 200,100,...,100
    - *network_vlan:* vlan to be compared when you captured.
    - *record_type:* record type in group record,1~6
    - *src_address_list:* Multicast source ip
    - *igmp_grp_addr:* Multicast group ip
    - *send_param:* to specify particular parameter values other than vlan to be sent
    - *capt_param:* to specify particular parameter values other than vlan to be captured
    - *comments:* description for test target of this traffic
    - *traffic_times:* times to re-try if traffic failed at first time
    - *delay_time:* times to delay before stop capture
    
    | send_upstream_igmpv3_traffic | 100,200 | 300 | 5 | {1.1.1.1 2.2.2.2} | 225.1.1.4 | send_param=ip_src=1.1.1.2 |
    | send_upstream_igmpv3_traffic | 100,200 | 300 | 6 | {192.2.3.2} | 225.1.1.4 | expect_result=FAIL |       
    
    """

    keyword="send_upstream_igmpv3_traffic"
    logger.debug ("%s: %s,%s" % (keyword,str(args),str(params)))
    legacy_expect_result = None
    legacy_ip_src = None
    if len(args) == 7 :
        legacy_expect_result = args[0]
        record_type =  args[1]
        legacy_ip_src = args[2]
        src_address_list = args[3]
        igmp_grp_addr = args[4]
        client_vlan = args[5]
        network_vlan = args[6]
    elif len(args) == 5:
        client_vlan = args[0]
        network_vlan = args[1]     
        record_type =  args[2]     
        src_address_list = args[3]
        igmp_grp_addr = args[4]  
    else :
        raise AssertionError ("accept only 5 position params: \
        record_type,src_address_list, igmp_grp_addr,client_vlan,network_vlan")
             
    traffic_times = params.setdefault('traffic_times',3)
    delay_time    = params.setdefault('delay_time',1)
    send_param    = params.setdefault('send_param','') 
    capt_param    = params.setdefault('capt_param','')    
         	
    client_vlan_list=_get_vlan_list(client_vlan)
    network_vlan_list=_get_vlan_list(network_vlan)
    client_mac=_get_unique_mac(client_vlan_list[0],client_vlan_list[-1])
    expect_result = params.setdefault('expect_result','PASS')
    group_record_list = pcta_generate_group_records(record_type,src_address_list,igmp_grp_addr)

    default_send_param=["ta_protocol=IGMP","side=user","mac_src=%s" % client_mac, "mac_dest=01005e000016",\
                        "ip_dest=224.0.0.22", "igmp_version=V3", "igmp_type=22", \
                        "nb_of_group_records=1", "group_record_list=%s" % group_record_list]
    if legacy_ip_src :
        default_send_param.append ("ip_src=%s" % legacy_ip_src )      
					
    default_capt_param=["ta_protocol=IGMP","side=network"]
    param_list=_get_param_list(send_param,capt_param,default_send_param,\
				default_capt_param,client_vlan_list,network_vlan_list)

   ### start send traffic logic
    _send_traffic_logic(param_list[0],param_list[1],expect_result,traffic_times,delay_time)
        
 
def send_downstream_multicast_traffic(expect_result,src_address,igmp_grp_addr,network_vlan="", client_vlan="",send_param="",capt_param="",comments="",traffic_times=2,delay_time=1):
    """
    - *expect_result:*  PASS/FAIL
    - *src_address:* Multicast source ip
    - *igmp_grp_addr:* Multicast group ip
    - *client_vlan:* vlan used when you send the packet. Example: 100 or 200,100 or 200,100,...,100
    - *network_vlan:* vlan to be compared when you captured.
    - *send_param:* to specify particular parameter values other than vlan to be sent
    - *capt_param:* to specify particular parameter values other than vlan to be captured
    - *comments:* description for test target of this traffic
    - *traffic_times:* times to re-try if traffic failed at first time
    
    | send_downstream_multicast_traffic | PASS | 2.2.2.2 | 225.1.1.4 | 100 | 200 |
    | send_downstream_multicast_traffic | FAIL | 2.2.2.2 | 225.1.1.4 | 100 | 200 | 
    
    """

    keyword="send_downstream_multicast_traffic"
    logger.debug("%s:%s %s %s %s %s %s %s %s" % \
    (__name__,keyword,expect_result,src_address,igmp_grp_addr,client_vlan,network_vlan,\
    str(send_param),str(capt_param)))

    client_vlan_list = _get_vlan_list(client_vlan)
    network_vlan_list = _get_vlan_list(network_vlan)
    client_mac = _get_unique_mac(client_vlan_list[0],client_vlan_list[-1])
    mac_dest = _create_mac_dest_by_group_ip(igmp_grp_addr)

    default_send_param=["ta_protocol=IP","side=network","eth_type=ETH",\
        "ip_src=%s" % src_address,"ip_dest=%s" % igmp_grp_addr,"mac_dest=%s" \
        % mac_dest,"frame_len=1019","packet_count=10"]
    default_capt_param=["ta_protocol=IP","side=user","mac_dest=%s" % mac_dest,"packet_count=10"]
    param_list=_get_param_list(send_param,capt_param,default_send_param,\
                                   default_capt_param,network_vlan_list,client_vlan_list)

    ### start send traffic logic
    _send_traffic_logic(param_list[0],param_list[1],expect_result,traffic_times,delay_time)

def send_upstream_8021x_traffic(message_type,client_vlan,**args) :

    """    
    - *expect_result:*  PASS/FAIL
    - *message_type:* EAP message type. Example: start/response_identity/response_md5/logoff
    - *username:* EAP md5 username
    - *password:* EAP md5 password
    - *client_vlan:* vlan used when you send the packet. Example: 100 or 200,100 or 200,100,...,100
    - *network_vlan:* vlan to be compared when you captured.
    - *send_param:* to specify particular parameter values other than vlan to be sent
    - *capt_param:* to specify particular parameter values other than vlan to be captured
    - *comments:* description for test target of this traffic
    - *traffic_times:* times to re-try if traffic failed at first time
    - *delay_time:* time to sleep before stop capture (seconds)
     
    | send upstream 8021x traffic | response_identity | onos | alacatel | 1 |
    |   | send_param=mac_src=000000600000 | capt_param=mac_src=000000600000 | traffic_times=4 | 
    
    """
    keyword="send_upstream_8021x_traffic"
    logger.debug("%s:%s" % (__name__,keyword))  
    username = args.setdefault('username','')
    password = args.setdefault('password','')
    eap_id = args.setdefault('eap_id',0)
    challenge = args.setdefault('challenge','')
    send_param=args.setdefault('send_param','')
    capt_param=args.setdefault('capt_param','')
    comments=args.setdefault('comments','')
    traffic_times= args.setdefault('traffic_times',1)
    delay_time=args.setdefault('delay_time',3)
    expect_result = args.setdefault('expect_result','PASS')
    vlan_style = args.setdefault('vlan_style','new')
    md5_password = ''
    client_vlan_list=_get_vlan_list(client_vlan)  
    client_mac=_get_unique_mac(client_vlan_list[0],client_vlan_list[-1])
    
    if password != '':
        md5_password = pcta_create_md5_key(eap_id,password,challenge)
        md5_password = '10'+md5_password
        
    DOT1X_DATA = {'response_identity':{'packet_type':'00','eap_code':'02','eap_data_type':'01','data':username},\
      'response_md5':{'packet_type':'00','eap_code':'02','eap_data_type':'04','data':md5_password}}
            #'start':{'packet_type':'01'},\         
             #     'logoff':{'packet_type':'02'}}
              #    'response_identity':{'packet_type':'00','eap_code':'02','eap_data_type':'01','data':username},\
               #   'response_md5':{'packet_type':'00','eap_code':'02','eap_data_type':'04','data':password}}      
  
    default_send_param=[]
    if message_type == 'response_identity' or message_type == 'response_md5': 
      default_send_param=["ta_protocol=8021X","side=user","packet_type=%s" \
      % DOT1X_DATA[message_type]['packet_type'],"mac_src=%s" % client_mac,"mac_dest=0180c2000003", "version=1",\
      "eap_id = %s" % eap_id, "eap_code=%s" % DOT1X_DATA[message_type]['eap_code'],"eap_data_type=%s" \
      % DOT1X_DATA[message_type]['eap_data_type'], "data=%s" % DOT1X_DATA[message_type]['data']]

    if message_type == 'start':
      default_send_param=["ta_protocol=8021X","side=user","packet_type=01" ,"mac_src=%s" \
                         %client_mac,"mac_dest=0180c2000003", "version=1"]
    if message_type == 'logoff':
      default_send_param=["ta_protocol=8021X","side=user","packet_type=02" ,"mac_src=%s" \
                         %client_mac,"mac_dest=0180c2000003", "version=1"]

    default_capt_param=["ta_protocol=8021X","side=user","mac_dest=%s" % client_mac]

    param_list=_get_param_list(send_param,capt_param,default_send_param,\
                                   default_capt_param,client_vlan_list,client_vlan_list,vlan_style=vlan_style)
    if message_type != 'logoff':
        _send_traffic_logic(param_list[0],param_list[1],expect_result,traffic_times,delay_time)
    else:
        _send_traffic_logic(param_list[0],'x',expect_result,traffic_times,delay_time)
def _str2byte(s):
    base='0123456789ABCDEF'
    i=0
    s = s.upper()
    s1=''
    if len(s) == 1:
        b1=base.find('0')
        b2=base.find(s[0])
        if b1 == -1 or b2 == -1:
            return None
        s1+=chr((b1 << 4)+b2)           
    else:
     while i < len(s):
        c1=s[i]
        c2=s[i+1]
        i+=2
        b1=base.find(c1)
        b2=base.find(c2)
        if b1 == -1 or b2 == -1:
            return None
        s1+=chr((b1 << 4)+b2)
    return s1

def pcta_create_md5_key (eap_id,password="alcatel",chall_key="b1b604671e9346947906dd2b5768a628") :
    eap_id = _str2byte(eap_id)
    m2 = hashlib.md5()   
    m2.update(eap_id) 
    m2.update(password)
    m2.update(_str2byte(chall_key))
    res = m2.hexdigest()
    logger.debug ( "md5 key created:" +str(res) )
    return res 	
	
class PctaContinueError (AssertionError) :
    ROBOT_CONTINUE_ON_FAILURE = True
    
