
dMeasPacketMapping =  {
'side': {'STC':'side','PCTA':'side'},
'control_mode': {'STC':'control_mode','PCTA':'control_mode'},
'packet_count': {'STC':'FrameCount','PCTA':'packet_count'},
'packets_lost':{'STC':'droppedFrameCount','PCTA':'packets_lost'},
'packet_loss_ratio':{'STC':'DroppedFramePercent','PCTA':'packet_loss_ratio'},
'last_received_seq_nbr' : {'STC':'lastSeqNum','PCTA':'last_received_seq_nbr'},
'bit_rate' : {'STC':'bitrate','PCTA':'bit_rate_good'},
'l1_bit_rate' : {'STC':'l1bitrate','PCTA':'x'},
'total_bit_rate' : {'STC':'totalbitrate','PCTA':'x'},
'mac_src': {'STC':'x','PCTA':'mac_src'},
'mac_dest': {'STC':'x','PCTA':'mac_dest'},
'vlan1_id': {'STC':'x','PCTA':'vlan1_id'},
'vlan2_id': {'STC':'x','PCTA':'vlan2_id'},
'vlan1_user_prior': {'STC':'x','PCTA':'vlan1_user_prior'},
'vlan2_user_prior': {'STC':'x','PCTA':'vlan2_user_prior'},
'ip_dest': {'STC':'x','PCTA':'ip_dest'},
'ip_src': {'STC':'x' ,'PCTA':'ip_src'},
'port_name':{'STC':'port_name' ,'PCTA':''},
'max_jitter':{'STC':'maxJitter' ,'PCTA':''},
}

def get_mapping():
    global dMeasPacketMapping
    return dMeasPacketMapping

