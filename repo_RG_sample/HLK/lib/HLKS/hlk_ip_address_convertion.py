import re 
#from robot.api import logger 

def convert_to_pcta_ipv6_addr(input):
    tmp_ipv6 = ""
    tmp_ipv6 = convert_to_mib_ipv6_str(input,":")
    pcta_ipv6 = ""
    index = 0

    ipv6_list = tmp_ipv6.split(":")
    for each in ipv6_list:
        index = index + 1
        each_hex = str(hex(int(each,16)))
        each_hex = each_hex.replace("0x","")

        if index < len(ipv6_list):
            pcta_ipv6 = pcta_ipv6 + each_hex + ":"
        else:
            pcta_ipv6 = pcta_ipv6 + each_hex

    if ":0:0" in pcta_ipv6 :
        search_str = ':0'
        m = 1 
        while m:
            search_str = search_str + ':0'
            m = re.search(search_str,pcta_ipv6)

        search_str = search_str[0:-2]
        print("search_str:%s" % search_str)
        pcta_ipv6 = re.sub(search_str,':',pcta_ipv6)

    return pcta_ipv6
     
        
def convert_to_mib_ipv6_str(input,split_symbol=""):
    """
      convert the input ipv6 address to the format of mib ipv6 str
    """
    output_str = ""
    str_mib_ipv6 = ['0000','0000','0000','0000',\
                    '0000','0000','0000','0000']
    
    left_list = [] 
    right_list = [] 
    colon_list = [] 
    left_list = input

    #if it's already a ipv6 mib str, return it directly.
    colon_index = input.find(':')
    if colon_index < 0:
        return input

    colon_index = input.find('::')

    #if ipv6 address has char '::', then split it into two list, left_list and right_list. 
    if colon_index > 0:
        colon_list = input.lower().split("::")
        left_list = colon_list[0]
        right_list = colon_list[1]
#        logger.info("colon_list:%s" % (colon_list)) 
#        logger.info("left_list:%s" % (left_list))

        #for the right_list, fill it in str_mib_ipv6 from the end
        right_list = right_list.split(':')
        right_index = 7 - len(right_list) + 1
        for right_sub in right_list:
#            logger.debug("right_sub:%s" % (right_sub))
            tmp_str = right_sub
            tmp_str = tmp_str.rjust(4,'0')
            str_mib_ipv6[right_index] = tmp_str
#            logger.info("str_mib_ipv6[%s]:%s" % (right_index,str_mib_ipv6[right_index]))
            right_index = right_index + 1

    #for the left_list, fill it in str_mib_ipv6 from the begin
    left_index = 0
    left_list = left_list.split(':')

    for left_sub in left_list:
        tmp_str = left_sub
#        logger.debug("left_sub:%s" % (left_sub))
        tmp_str = tmp_str.rjust(4,'0')
        str_mib_ipv6[left_index] = tmp_str
#        logger.info("str_mib_ipv6[%s]:%s" % (left_index,str_mib_ipv6[left_index]))
        left_index = left_index + 1 
 
    #convert list to str 
    output_index = 0
    for out_str in str_mib_ipv6:
#        output_str = output_str + str(str_mib_ipv6[output_index])
        if output_index != 7:
            output_str = output_str + str(str_mib_ipv6[output_index]) + split_symbol
        else:
            output_str = output_str + str(str_mib_ipv6[output_index])
        output_index = output_index + 1   
    
    #logger.info("return:%s" % (output_str))    
    return output_str 

def get_ipv6_linklocal_addr(mac_addr):
    
    # remove the most common macaddr delimiters, dots, dashes, etc.
    # cast the hex string to an base-16 int for math safety.
    # use xor of 02 on the 2nd most significant hex char.
    # Remove/slice the '0x' off the begining from using hex().

    #sample: 00:22:22:22:22:44
    m = mac_addr.replace(":", "")

    ###################################################
    #add FFFE in the middle, and add fe80 in the head.
    #              ____         __ __
    #result sample:fe80::0022:22ff:fe22:2244
    ###################################################
    m =  'fe80::%s:%sff:fe%s:%s' % (m[:4],m[4:6],m[6:8],m[8:12])

    ###################################################
    #invert the 15th significant bit.
    #                      _
    #result sample: fe80  0222 22ff fe22 2244
    ###################################################
    m = m.replace(":","")
    m = hex(int(m,16)^0x200000000000000)[2:]

    m = '%s::%s:%s:%s:%s' %(m[:4],m[4:8],m[8:12],m[12:16],m[16:20])
    
    m = convert_to_mib_ipv6_str(m,":")
    return m

def get_ipv6_solicit_node_addr(ip_addr):
    ip_hex = convert_to_mib_ipv6_str(ip_addr)
    
    part1 = ip_hex[26:28]
    part2 = ip_hex[28:32]
    
    solicit_node_addr = "ff02:0:0:0:0:1:ff"+part1+":"+part2

    return solicit_node_addr

def get_ipv6_mcast_mac(ip_addr):
    ip_hex = convert_to_mib_ipv6_str(ip_addr)
    
    part1 = ip_hex[26:32]
    mac = "3333ff"+part1

    return mac
 
def get_ipv6_addr(ipv6_prefix,interface_id):
    ipv6_prefix = convert_to_mib_ipv6_str(ipv6_prefix,":")
    ipv6_addr = ipv6_prefix+"::"+interface_id
    ipv6_addr = convert_to_mib_ipv6_str(ipv6_addr,":")

    return ipv6_addr

def get_ipv6_subnet(ipv6_addr,subnet_mask):
    ipv6_subnet   = ""
    ipv6_addr     = convert_to_mib_ipv6_str(ipv6_addr,":")
    subnet_length = 128/16-int(subnet_mask)/16

    #remove the last subnets
    for i in range (1,subnet_length+1):
        ipv6_addr = ipv6_addr.rsplit(":",1)[0]
        print("ip = %s" % ipv6_addr)

    ipv6_subnet = ipv6_addr + "::" + "/" + str(subnet_mask)
    return ipv6_subnet    

def ipv6_to_int(ip):
    hex_value = convert_to_mib_ipv6_str(ip)
    hex_value = "0x"+hex_value
    print("hex:%s" % hex_value)
    int_value = long(eval(hex_value))        
    return int_value

def int_to_ipv6(int_ip):

    ipv6_ip = ""
    hex_str = str(hex(int_ip))
    print("HEX:%s" % hex_str)
    hex_str = hex_str.replace("0x","") 
    hex_str = hex_str.replace("L","") 
  
    for i in (7,6,5,4,3,2,1):
        index_start = 4*i
        index_end   = index_start + 4
        ipv6_ip     = ":" + hex_str[index_start:index_end] + ipv6_ip

    index_start = 0
    index_end   = 4
    ipv6_ip     = hex_str[index_start:index_end] + ipv6_ip 

    return ipv6_ip

def ipv4_to_int(ip):
    ip_list = ip.split(".")
    return (int(ip_list[0]) << 24) + (int(ip_list[1]) << 16) + (int(ip_list[2]) << 8) + int(ip_list[3])     

def get_ipv4_mask(ip1,ip2):
    m = 0xFFFFFFFF ^ ipv4_to_int(ip1) ^ ipv4_to_int(ip2)
    m = [(m & (0xFF << (8*n))) >> 8*n for n in (3,2,1,0)]
    for i in range(4):
        if m[i] != 255:
            m[i] = 0
    net_mask = str(m[0]) + "." + str(m[1]) + "." + str(m[2]) + "." + str(m[3])
    return net_mask

def ipv4_netmask_str2len(ip):

    list = ip.split(".")

    hexStr = ""
    for each in list:
        hex_tmp = hex(int(each))
        str_tmp = str(hex_tmp).replace("0x","")
        hexStr = hexStr + str_tmp

    hexStr = "0x" + hexStr
    binValue = bin(eval(hexStr))
    binStr = str(binValue).replace("0b","")

    mask = 0
    for each in binStr:
        if each == "1":
            mask = mask +1

    return mask
 
def ipv4_netmask_len2str(mask_len):
    mask_len = 32 - int(mask_len) - 1
    i_mask = 1
    for i in range(mask_len):
        i_mask = (i_mask << 1) ^ 1
    i_mask = 0xFFFFFFFF ^ i_mask
    m = [(i_mask & (0xFF << (8*n))) >> 8*n for n in (3,2,1,0)]
    net_mask = str(m[0]) + "." + str(m[1]) + "." + str(m[2]) + "." + str(m[3])
    return net_mask 

def convert_ipv6_syntax(ipv6):

    """
      convert ipv6 syntax from 2000:0000:0000:0000:0000:0000:0000:0002 to 20:00:00:00:00:00:00:00:00:00:00:00:00:00:00:02
    """
    ipv6=ipv6.replace(":","")
    ipv6=ipv6.lower()

    ipv61=ipv6[0:2]+":"+ipv6[2:4]+":"+ipv6[4:6]+":"+ipv6[6:8]+":"+ipv6[8:10]+":"+ipv6[10:12]+":"+ipv6[12:14]+":"+ipv6[14:16]+":"+ipv6[16:18]+":"+ipv6[18:20]+":"+ipv6[20:22]+":"+ipv6[22:24]+":"+ipv6[24:26]+":"+ipv6[26:28]+":"+ipv6[28:30]+":"+ipv6[30:32]

    return(ipv61)

