import os,sys,time,re,hashlib
from robot.libraries.BuiltIn import BuiltIn
from robot.api import logger
import logging


try:
    for lib_path in [os.environ['ROBOTREPO'] +'/HLK/lib/COM_PCTA',\
    os.environ['ROBOTREPO'] +'/HLK/lib/COM_MSG',\
    os.environ['ROBOTREPO'] +'/HLK/lib/COM_STC',\
    os.environ['ROBOTREPO'] +'/HLK/lib/HLKS'] :
        if lib_path not in sys.path:
            sys.path.append(lib_path)
except Exception as inst:
    raise AssertionError("%s -> Fail to set sys.path, %s" % (__name__,inst))

import hlk_mac_address_convertion
import hlk_ip_address_convertion
import llk_com_pcta
import llk_com_stc 

import variable_map_traffic_opt
import variable_map_capture_opt
import variable_map_measure_opt

import msg_command
import pcta_command

# DELETED, set to empty
TA_TYPE = ''
TA_STATUS = {}
sTRAF_OUTPUT_DIR = ''

try:
    sTRAF_OUTPUT_DIR = BuiltIn().replace_variables('${OUTPUTDIR}') + "/TRAFFIC"
except Exception as inst:
    sTRAF_OUTPUT_DIR = '/tmp/TRAFFIC'


sLOG_CASE_NAME = ''

def hlk_traffic_setup(*lTAList) :
    """
    setup the output directory and connect PCTA or STC

    usage Example:
    | hlk traffic setup | @{TAToolList} | new_traff_cmd_log=True |
    | hlk traffic setup | @{TAToolList} |

    - *new_traff_cmd_log:* if true, new log file is generated
    - *lTAToolList:* a list variable which contains the dictionary variables of pcta info and stc info.

       | &{stc_info} | get table line | TrafficGenData | type=STC |
       | &{pcta_info} | get table line | TrafficGenData | type=PCTA |
       | @{TAToolList} | create list | ${pcta_info} | ${stc_info} |

    - *RETURN VALUE:* none or raise error

    """
    keyword_name = "hlk_traffic_setup"
    logger.debug("%s-> %s" % (keyword_name,str(lTAList)))

    global TA_STATUS
    global TA_TYPE
    global sTRAF_OUTPUT_DIR

    # pop out the output directory
    taList = {}
    for each in lTAList:
        if 'outputDir' in each :
            print each
            sTRAF_OUTPUT_DIR = each['outputDir']
            taList = list(lTAList)
            taList.remove(each)
            lTAList = eval(str(taList))
    lTAList = eval(str(lTAList)) 
    sTrafCmdLogPath = str(sTRAF_OUTPUT_DIR) + '/ta_traffic_cmd.log'
    if not os.path.exists(sTRAF_OUTPUT_DIR):
        os.makedirs(sTRAF_OUTPUT_DIR)

    #os.system('rm -rf %s' % sTrafCmdLogPath)
    open (sTrafCmdLogPath,'w')

    logger_ta_cmd(logName='ta_traffic_cmd',logPath=sTrafCmdLogPath)
    logger_cmd_ta = logging.getLogger("ta_traffic_cmd")

    for each in lTAList:
        try:
            if 'new_traff_cmd_log' in str(each) :
                key,value = each.split('=')
                if value.lower() == 'true' : 
                    continue

            # convert the key to lower case
            each = dict(map(lambda (k,v): (str(k).lower(), str(v)),\
            each.iteritems()))

            # trigger connect function
            if 'type' in each :
                # only deal with PCTA and STC
                if each['type'].lower() in ['pcta','stc'] :
                    if each.has_key('available') and each['available'].lower() == 'false':
                        # if it is 'not_available', only set it's status 
                        TA_STATUS[each['type'].upper()] = 'not_available'
                    else:
                        # if it is not 'not_available', trigger llp connect function
                        each['outputDir'] = sTRAF_OUTPUT_DIR
                        llp_connect_cmd = 'llk_com_%s.llk_traffic_%s_connect(**each)' % \
                        (each['type'].lower(),each['type'].lower())
                        eval(llp_connect_cmd)

                        # add its status to TA_STATUS
                        TA_STATUS[each['type'].upper()] = 'connect_pass'
                else:
                    logger.warn("Can not support this kind of TA:%s" % each['type'])
              
        except Exception as inst:
            s = sys.exc_info()      
            logging.warn("****************lineno:%s\nException:%s" % (s[2].tb_lineno,inst))      
            #logger.warn("Fail to connect %s, lineno:%s,\nexception:%s" % \
            #(each['type'],s[2].tb_lineno,inst))
            TA_STATUS[each['type'].upper()] = 'connect_fail'

    # set default TA_TYPE,PATC has the highest priority
    if 'PCTA' in TA_STATUS and TA_STATUS['PCTA'] == 'connect_pass' : 
        TA_TYPE = 'PCTA'
    elif 'STC' in TA_STATUS and TA_STATUS['STC'] == 'connect_pass': 
        TA_TYPE = 'STC'
    else: 
        TA_TYPE = ''

    logger.info('TA_STATUS:%s' % TA_STATUS )
    logger.info('TA_TYPE:%s' % TA_TYPE)  

    if not TA_STATUS:
        raise AssertionError("There is no any PCTA or STC is avaiable!\n%s" % str(lTAList)) 
      
def hlk_traffic_cleanup():
    """
    disconenct the traffic generators of PCTA or STC which is already connected by hlk_traffic_setup

    usage Example:
    | hlk traffic cleanup | |
     - *RETURN VALUE:* none or raise error

    """
    global TA_STATUS
    for each in TA_STATUS:
        try: 
            llp_disconnect_cmd = 'llk_com_%s.llk_traffic_%s_disconnect()' % (each.lower(),each.lower())
            eval(llp_disconnect_cmd)
        except Exception as inst: 
            logger.info("Fail to disconnect %s" % each)
 
def hlk_traffic_set_ta_type(sTA_type):
    """
    set TA type to PCTA or STC

    usage Example:
    | hlk traffic set ta type | sTA_type |

    - *sTA_type:* the type of traffic generator, Its value is STC or PCTA
    - *RETURN VALUE:* none or raise error

    """
    global TA_STATUS
    global TA_TYPE

    if sTA_type.upper() in TA_STATUS and TA_STATUS[sTA_type.upper()] == 'connect_pass':
        TA_TYPE = sTA_type.upper()
    else:
        raise AssertionError("Fail to change TA type from '%s' to '%s': \n'%s' is not in current available TA list:%s" % (TA_TYPE,sTA_type,sTA_type,TA_STATUS))

    logger.info("Change TA_TYPE to : %s" % TA_TYPE)

def check_pcta_capbility (**aArgs) :
    bStatus = True

    if 'bitrate' in str(aArgs) : 
        if not llk_com_pcta.llk_traffic_pcta_get_cmd('IP','start') : 
            bStatus = False

    return bStatus

def _convert_vlan_id(**dict_arg):
    try : 
        dNewVlan = {}
        # select all vlan related key from dict_arg
        for key in dict_arg.keys() :
            m = re.search('^vlan(\d+)_',key)
            if m :
                dNewVlan[key] = dict_arg[key]
                dict_arg.pop(key)

        # get the max vlan id
        iMaxVlanId = 0
        for key in dNewVlan :
            m = re.search('^vlan(\d+)_',key)
            if m : 
                iCurVlanId = m.group(1)
                if int(iCurVlanId) > int(iMaxVlanId) :
                    iMaxVlanId = int(iCurVlanId)

        # create the vlan id map 
        dVlanIdxMap = {}
        for each in range (1,iMaxVlanId+1):
            dVlanIdxMap[each] = iMaxVlanId+1-int(each)
    
        # convert the vlan id
        for key in dNewVlan :
            m = re.search('^vlan(\d+)_',key)
            if m : 
                iCurVlanId = m.group(1)
                newVlanId = dVlanIdxMap[int(iCurVlanId)]
                newKey = re.sub(iCurVlanId,str(newVlanId),key)
        
                #if int(iCurVlanId) > int(iMaxVlanId) :
                #    iMaxVlanId = int(iCurVlanId)
                dict_arg[newKey] = dNewVlan[key]

    except Exception as inst:
        s = sys.exc_info()
        logger.error("_convert_vlan_id->Exception:%s, lineno:%s" % (inst,s[2].tb_lineno))    

    return dict_arg 

def hlk_traffic (**args) :
    """
    send out, capture and verify stream block per TA type

    usage Example:
    | hlk traffic | us_send_format=${dUStream1} | ds_send_format=${lDStreamList} | us_capt_opts=${dUCapture1} | ds_capt_opts=${lDCaptureList} | us_meas_opts=${dUMeasure1} | ds_meas_opts=${lDMeasureList} |send_delay=3 | capture_delay=2 | measure_duration=5 |

    - *us_send_format:* upstream block info dictionary, containing ta_protocol, parameters in the packet
    - *ds_send_format:* downstream blocks dictinary, containing ta_protocol, parameters in the packet
    - *us_capt_opts:* upstream capture parameters, if it is not agreed with that in packet, capture failed.
    - *ds_capt_opts:* downstream capture parameters, if it is not agreed with that in packet, capture failed.
    - *us_meas_opts:* upstream measure parameters, to judge wehther the packet is acceptable when jitter.
    - *ds_meas_opts:* downstream measure dictinary, to judge wehther the packet is acceptable when jitter.
    |       - *control_mode:* its range is start_capture_only  stop_capture_only start_stop_capture verify_capture_only start_traffic_only stop_traffic_only start_stop_traffic start_measure_only stop_measure_only start_stop_measure verify_measure_only. usage example:
    |       | ${dUStream1} | Create_Dictionary | stream_name=U1 | ta_protocol=eth | vlan1_user_prior=1 | vlan1_id=10 | vlan2_id=18 | mac_src=00:00:00:00:66:00 | frame_len=1050 | type=809B | control_mode=start_traffic_only |
    |       | ${dUCapture1} | create dictionary | stream_name=U1 | packet_count=>=1 | mac_src=00:00:00:00:66:00 | vlan1_id=10 | vlan2_id=18 | vlan1_user_prior=1 | control_mode=start_stop_capture |
    |       | ${dUMeasure2} | create dictionary | stream_name=U2 | bit_rate=100 | bit_rate_tol=5% | control_mode=verify_measure_only |
    - *send_delay:* delay time(seconds) before sending packet
    - *capture_delay:* delay time(seconds) before capturing packet
    - *measure_duration:* delay time(seconds) before measure packet
    - *save_stc_cfg:* whether to save STC configuration, and it may be file name without path for example
    |save_stc_cfg=True|save_stc_cfg=False|save_stc_cfg=stc_configure_file_name.tcc|    
    - *RETURN VALUE:* Null or raise error

    """

    keyword_name = "hlk_traffic"
    global sLOG_CASE_NAME
    aArgs = args
    logger.debug("%s-> %s" % (keyword_name,aArgs))

    logger_cmd_ta = logging.getLogger("ta_traffic_cmd")

    try :
        sTestName = BuiltIn().replace_variables('${TEST NAME}')    
        # it's a new case
        if sLOG_CASE_NAME != sTestName :
            sLOG_CASE_NAME = sTestName
            logger_cmd_ta.info('***Case Name:*** ' + sLOG_CASE_NAME + '\r\n')     

    except Exception as inst:
        sLOG_CASE_NAME = 'pythonTest'
    logger_cmd_ta.debug('\r\nhlk_traffic: %s\r\n' % str(args))
    global TA_TYPE
    sTA_TYPE_NOW = TA_TYPE
    """
    if TA_TYPE == 'PCTA' : 
        if not check_pcta_capbility (**aArgs) : 
            # try to convert to STC
            if TA_STATUS['STC'] == 'connect_pass' : 
                sTA_TYPE_NOW = 'STC'
                logger.info("Convert TA to STC!")
            else:
                raise AssertionError("hlk_traffic: PCTA can't support this kind of traffic.\n%s" % aArgs)
    """

    hlk_cmd = "hlk_traffic_%s(**args)" % sTA_TYPE_NOW.lower()
    logger.info("Trigger %s" % hlk_cmd)
    try : 
        eval(hlk_cmd)
    except Exception as inst:
        raise continueFailure("\n%s" % inst)

def _convert_key_to_PCTA_or_STC_format(format_type,**dArgs) :  
    global TA_TYPE
    dFormatKey = {}
    dNewFormat = {}
    if format_type == 'send_traffic' : 
        dFormatKey = variable_map_traffic_opt.get_mapping()
    elif format_type == 'capt_opts' : 
        dFormatKey = variable_map_capture_opt.get_mapping()
    elif format_type == 'meas_opts' : 
        dFormatKey = variable_map_measure_opt.get_mapping()
    
    try:        
        sKey = '' 
        for key in dArgs :  
            value = dArgs[key]
            key = key.lower()

            # for PCTA, convert 00:00:00:99:00:00 to 000000990000
            if TA_TYPE == 'PCTA' and str(key) in ['mac_src','mac_dest','hw_addr_dest','hw_addr_src']:
                value = value.replace(':','')

            if TA_TYPE == 'PCTA' and str(key) in ['ipv6_src','ipv6_dest','dhcpv6_ip_dest','dhcpv6_ip_src'] and format_type == 'capt_opts':
                value = hlk_ip_address_convertion.convert_to_pcta_ipv6_addr(value)
                 
            if TA_TYPE == 'STC' and str(key) in ['code'] and format_type == 'capt_opts':
                value = str(int(dArgs['code'],16))

            if key in ['ta_protocol','expect_result','exp_result']:  
                # don't convert it's key name
                sKey = key
            elif '_tol' in key :                 
                sKey = dFormatKey[re.sub('_tol','',key)][TA_TYPE]
                sKey = sKey + '_tol'                
            else :
                # convert it's key name to TA_TYPE key
                sKey = dFormatKey[key][TA_TYPE]

            if sKey == '' :
                raise AssertionError("Fail to convert % to % format" % (key,TA_TYPE))
            elif sKey != 'x' :  
                dNewFormat[sKey] = value
            # if sKey == 'x', skip it

    except Exception as inst:
        s = sys.exc_info()
        raise AssertionError("_convert_key_to_PCTA_or_STC_format->Fail to convert format keys,format_type:%s, line:%s, \
        exception:%s\nargs:%s" % (format_type,s[2].tb_lineno,inst,dArgs))

    return dNewFormat
    
def _convert_format_args(arg_name,format_type,**args):
    lArgList = []
    dReturnArg = {}

    try:  

        sFormat_args = ''
        # get value of args when key=arg_name
        if arg_name in args.keys():
            sFormat_args = args.pop(arg_name)
        else : 
            return None

        logger.debug("init sFormat_args:%s" % sFormat_args)

        # get content from input args and convert it to list
        format_args = {}   
        try : 
            # if sFormat_args is dict or list         
            format_args = eval(sFormat_args)     
        except Exception as inst:
            # if the format of sFormat_args is : {[(a=b,c=d,e=f,h>g)]}            
            for each in str(sFormat_args).strip('[]{}()').split(',') : 
                """
                >=   =>=  :>=
                =>=  ==>= =>= :>=
                >    => :>
                """
                # deal with the charactor '>' '<' and '!'
                for each_char in ['>','<','!'] :
                    if each_char in str(each) :
                        each = each.replace(each_char,'='+each_char)

                each = each.replace('==','=')
                each = each.replace('=',':',1)
                k,v = each.split(':')
                format_args[k] = v                  

        logger.debug('format_args:%s' % format_args)   
        #logger.debug("type:%s" % type(format_args))

        # set lArgList as list
        if 'dict' in str(type(format_args)).lower() : 
            # if type of format_args is dist               
            lArgList.append(format_args)
        elif type(format_args) == list :
            # if type of format_args is list
            lArgList = format_args
        else : 
            raise AssertionError("Invalid input argement format, only dict or list is available. \n%s" % format_args)

        if not 'port_meas_opts' in arg_name : 
            # set stream_name as the key of dict: 
            # Ex:{u'D2': {u'vlan.vlan2': u'12', u'vlan.vlan1': u'22'}, u'D1': {u'vlan.vlan2': u'12'}}
            for each in lArgList : 
                if not each.has_key('stream_name') :
                    raise AssertionError("'stream_name' is required in %s:\n%s" % (arg_name,each))
                stream_name = each.pop('stream_name')        
                #convert key to PCTA or STC format
                d_new_format = _convert_key_to_PCTA_or_STC_format(format_type,**each)                
                dReturnArg[stream_name] = d_new_format
        else :
            # only deal with us_port_meas_opts and ds_port_meas_opts
            for each in lArgList : 
                #convert key to PCTA or STC format
                d_new_format = _convert_key_to_PCTA_or_STC_format(format_type,**each)                
                dReturnArg[arg_name] = d_new_format           

    except Exception as inst:
        s = sys.exc_info()
        raise AssertionError("Fail to convert format arguments, line:%s, Exception:%s\nformat_args:%s" % \
        (s[2].tb_lineno,inst,format_args))

    logger.debug('_convert_format_args->%s,%s\n%s' % (arg_name,type(format_args),dReturnArg))
    return dReturnArg

def hlk_traffic_pcta(**args) :
    keyword_name = "hlk_traffic_pcta"
    aArgs = args
    logger.debug("%s-> %s" % (keyword_name,aArgs))
    
    sResult = 'pass'
    sFailInfo = '\n'
    ta_protocol = ''

    global sLOG_CASE_NAME
    global sTRAF_OUTPUT_DIR

    # check input args
    assert (aArgs.keys()), "No any input arguments given !"
 
    # convert all input key to lower()
    aArgs = dict(map(lambda (k,v): (str(k).lower(), str(v)), aArgs.iteritems()))

    send_delay       = aArgs.setdefault('send_delay',0)
    capture_delay    = aArgs.setdefault('capture_delay',0)
    measure_delay    = aArgs.setdefault('measure_delay',0)
    measure_duration = aArgs.setdefault('measure_duration',30)
    pcap_file_name   = aArgs.setdefault('save_pcap_file','')

    # reset pcap_file_name according to its value 'true', 'false', 'xxx'
    if pcap_file_name :
        if pcap_file_name.lower() == 'true' :  
            pcap_file_name = sLOG_CASE_NAME + time.strftime("_%m-%d-%Y_%H-%M-%S") + '.pcap'
        elif pcap_file_name.lower() == 'false' :
            pcap_file_name = ''

    if pcap_file_name : 
        # start tshark to capture and save to file
        # tshark -i eth0 -w xxx.pcap &

        # get the dict of pcta_info
        pcta_info = pcta_command.get_pcta_info()

        network_pcap_file = 'network_' + pcta_info['network_nic'] + '_' + pcap_file_name 
        client_pcap_file  = 'client_'  + pcta_info['client_nic']  + '_' + pcap_file_name

        msg_command.open_msg_session(ip=pcta_info['ip'],port=str(pcta_info['port']),username=pcta_info['username'],password=pcta_info['password'],prompt='.*\].*\$|.*\].*%|.*\].*#')

        msg_command.start_capture_msg(pcta_info['network_nic'], msg_path='/tmp',file_name=network_pcap_file)
        msg_command.start_capture_msg(pcta_info['client_nic'],  msg_path='/tmp',file_name=client_pcap_file)        

    # parse arguments from input **args
    dUsSendFormat = {}
    if "us_send_format" in aArgs.keys():
        dUsSendFormat = _convert_format_args(arg_name='us_send_format',format_type='send_traffic',**aArgs)

    dDsSendFormat = {}
    if "ds_send_format" in aArgs.keys():
        dDsSendFormat = _convert_format_args(arg_name='ds_send_format',format_type='send_traffic',**aArgs)

    dUsCaptOptions = {}
    if "us_capt_opts" in aArgs.keys():
        dUsCaptOptions = _convert_format_args(arg_name='us_capt_opts',format_type='capt_opts',**aArgs)

    dDsCaptOptions = {}
    if "ds_capt_opts" in aArgs.keys():
        dDsCaptOptions = _convert_format_args(arg_name='ds_capt_opts',format_type='capt_opts',**aArgs)  
  
    dUsMeasOptions = {}
    if "us_meas_opts" in aArgs.keys():
        dUsMeasOptions = _convert_format_args(arg_name='us_meas_opts',format_type='meas_opts',**aArgs)

    dDsMeasOptions = {}
    if "ds_meas_opts" in aArgs.keys():
        dDsMeasOptions = _convert_format_args(arg_name='ds_meas_opts',format_type='meas_opts',**aArgs)

    try :
        dUsSendDistId = {}
        dDsSendDistId = {} 
        sUStartType = ''
        sDStartType = ''
        bStart = 'false' 
        # start measure or capture for Upstream
        if dUsMeasOptions or 'bit_rate' in str(dUsSendFormat) or 'burst_' in str(dUsSendFormat) :
            sUStartType = 'measure'
        else :
            sUStartType = 'capture'
        
        if (sUStartType == 'capture') and ( 'verify_capture_only' not in str(dUsCaptOptions)) \
           and ( 'stop_capture_only' not in str(dUsCaptOptions)) :
            bStart = 'true'
        if (sUStartType == 'measure') and ( 'verify_measure_only' not in str(dUsMeasOptions)) \
           and ( 'stop_measure_only' not in str(dUsMeasOptions)) :                        
            bStart = 'true'         
        if bStart == 'true' :
            for each in sorted(dUsSendFormat) :
                protocol_type = dUsSendFormat[each]['ta_protocol'].upper() 
                try :            
                    cmd_exec = "llk_com_pcta.llk_traffic_pcta_start_%s (side='network',ta_protocol=protocol_type)" % sUStartType
                    eval(cmd_exec)
                except Exception as inst :
                    # if start_measure is not support, change to start_capture
                    if sUStartType == 'measure' :
                        sUStartType = 'capture'
                        cmd_exec = "llk_com_pcta.llk_traffic_pcta_start_%s (side='network',ta_protocol=protocol_type)" % sUStartType
                        eval(cmd_exec)
                    else :
                        raise AssertionError("Fail to start_%s of %s protocol" % (sUStartType,ta_protocol))
                   
	# start measure or capture for Downstream	
        if dDsMeasOptions or 'bit_rate' in str(dDsSendFormat) or 'burst_' in str(dDsSendFormat) :
            sDStartType = 'measure'
        else :
            sDStartType = 'capture'
  
        bStart = 'false' 
        if (sUStartType == 'capture') and ( 'verify_capture_only' not in str(dDsCaptOptions)) \
           and ( 'stop_capture_only' not in str(dDsCaptOptions)) :
            bStart = 'true'
        if (sUStartType == 'measure') and ( 'verify_measure_only' not in str(dDsMeasOptions)) \
           and ( 'stop_measure_only' not in str(dDsMeasOptions)) :
            bStart = 'true'
        if bStart == 'true':
            for each in sorted(dDsSendFormat) :
                protocol_type = dDsSendFormat[each]['ta_protocol'].upper()
                try :            
                    cmd_exec = "llk_com_pcta.llk_traffic_pcta_start_%s (side='user',ta_protocol=protocol_type)" % sDStartType
                    eval(cmd_exec)
                except Exception as inst :
                    # if start_measure is not support, change to start_capture
                    if sDStartType == 'measure' :
                        sDStartType = 'capture'
                        cmd_exec = "llk_com_pcta.llk_traffic_pcta_start_%s (side='user',ta_protocol=protocol_type)" % sDStartType
                        eval(cmd_exec)
                    else :
                        raise AssertionError("Fail to start_%s of %s protocol" % (sDStartType,ta_protocol))

        sUSendType = ''
        sDSendType = ''

        logger.info("sleep %s seconds before sending traffic!" % send_delay )
        time.sleep(int(send_delay))

        # send upstream traffic    
        if 'burst_' in str(dUsSendFormat) :
            sUSendType = 'bursty'
        elif 'bit_rate' in str(dUsSendFormat) :
            sUSendType = 'start'
        elif dUsMeasOptions :
            sUSendType = 'start'
        else : 
            sUSendType = 'send'
 
        if ('stop_traffic_only' not in str(dUsSendFormat)) : 
            for each in sorted(dUsSendFormat) :
                dUsSendFormat[each].pop('control_mode','')
                protocol_type = dUsSendFormat[each].pop('ta_protocol').upper()
                dUsSendDistId[each] = {}
                dUsSendDistId[each]['protocol'] = protocol_type
                logger.info("send upstream traffic:%s" % dUsSendFormat[each])
                dSendFormat = {}
                try:
                    dSendFormat = _convert_vlan_id(**dUsSendFormat[each]) 
                    dUsSendDistId[each]['dist_id'] = llk_com_pcta.llk_traffic_pcta_send_traffic(side='user',\
                    send_type=sUSendType,ta_protocol=protocol_type,**dSendFormat)  	
                except Exception as inst:
                    s = sys.exc_info()
                    logger.error("hlk_traffic_pcta-> Exception: %s, lineno: %s" % (inst,s[2].tb_lineno))
                    raise AssertionError("Fail to send %s protocol traffic with %s mode, please check the arguments:%s" % (protocol_type,sUSendType,dSendFormat)) 

                msg = "Sent Upstream Traffic %s: %s" % (each,dUsSendFormat[each])
                logger.info("<span style=\"color: #0000FF\">" + msg + "</span>",html=True)

        time.sleep(0.5)
					
        # send downstream traffic   
        if 'burst_' in str(dDsSendFormat) :
            sDSendType = 'bursty'
        elif 'bit_rate' in str(dDsSendFormat) :
            sDSendType = 'start'
        elif dDsMeasOptions :
            sDSendType = 'start'
        else : 
            sDSendType = 'send' 
     
        if ('stop_traffic_only' not in str(dDsSendFormat)) : 
            for each in sorted(dDsSendFormat) :
                dDsSendFormat[each].pop('control_mode','')
                protocol_type = dDsSendFormat[each].pop('ta_protocol').upper()
                dDsSendDistId[each] = {}
                dDsSendDistId[each]['protocol'] = protocol_type
                logger.info("send upstream traffic:%s" % dDsSendFormat[each])
                dSendFormat = {}
                try :
                    dSendFormat = _convert_vlan_id(**dDsSendFormat[each]) 
                    dDsSendDistId[each]['dist_id'] = llk_com_pcta.llk_traffic_pcta_send_traffic(side='network',\
                    send_type=sDSendType,ta_protocol=protocol_type,**dSendFormat) 
                except Exception as inst :
                    raise AssertionError("Fail to send %s protocol traffic with %s mode, please check the arguments:%s" % (protocol_type,sDSendType,dSendFormat))

                msg = "Sent Downstream Traffic %s: %s" % (each,dDsSendFormat[each])
                logger.info("<span style=\"color: #0000FF\">" + msg + "</span>",html=True)
 
        if dUsMeasOptions or ('bit_rate' in str(dUsSendFormat)) or ('burst_' in str(dUsSendFormat))  or dDsMeasOptions or ('bit_rate' in str(dDsSendFormat)) or ('burst_' in str(dDsSendFormat)) :
            logger.info("sleep %s seconds before start measure traffic!" % measure_duration)
            time.sleep(int(measure_duration))
       
        lUsPackets = []
        # stop capture upstream     
        bStop = 'false'
        if (sUStartType == 'capture') and ( 'verify_capture_only' not in str(dUsCaptOptions)) \
           and ( 'start_capture_only' not in str(dUsCaptOptions)) :
            bStop = 'true'
        if (sUStartType == 'measure') and ( 'verify_measure_only' not in str(dUsMeasOptions)) \
           and ( 'start_measure_only' not in str(dUsMeasOptions)) :
            bStop = 'true'
        if bStop == 'true': 
            for each in sorted(dUsSendFormat) :
                packets = []            
                #if dUsMeasOptions or 'bit_rate' in str(dUsSendFormat) or 'burst_' in str(dUsSendFormat)  :  
                if sUStartType == 'measure' :       
                    if 'start_traffic_only' not in str(dUsSendFormat) :
                        llk_com_pcta.llk_traffic_pcta_stop_cmd (side='user',stop_type='traffic',\
                        dist_id=dUsSendDistId[each]['dist_id'],ta_protocol=dUsSendDistId[each]['protocol'])
                    try : 
                        packets = llk_com_pcta.llk_traffic_pcta_stop_cmd (side='network',stop_type='measure',\
                        ta_protocol=dUsSendDistId[each]['protocol'],dist_id='1')
                    except Exception as inst:
                        logger.warn("Failed to stop measure, exception:%s" % inst)
                        packets = []
                else :
                    try : 
                        packets = llk_com_pcta.llk_traffic_pcta_stop_cmd (side='network',stop_type='capture',\
                        ta_protocol=dUsSendDistId[each]['protocol'],dist_id='1')
                    except Exception as inst:
                        logger.warn("Failed to stop capture, exception:%s" % inst)
                        packets = []
                if packets : 
                    lUsPackets = lUsPackets + packets

        lDsPackets = []		
        # stop capture downstream      
        bStop = 'false'
        if (sUStartType == 'capture') and ( 'verify_capture_only' not in str(dDsCaptOptions)) \
           and ( 'start_capture_only' not in str(dDsCaptOptions)) :
            bStop = 'true'
        if (sUStartType == 'measure') and ( 'verify_measure_only' not in str(dDsMeasOptions)) \
           and ( 'start_measure_only' not in str(dDsMeasOptions)) :
            bStop = 'true'
        if bStop == 'true':
            for each in sorted(dDsSendFormat) :
                packets = []
                #if dDsMeasOptions or 'bit_rate' in str(dDsSendFormat) or 'burst_' in str(dDsSendFormat)  :   
                if sDStartType == 'measure' :                 
                    if 'start_traffic_only' not in str(dDsSendFormat) :
                        llk_com_pcta.llk_traffic_pcta_stop_cmd (side='network',stop_type='traffic',\
                        dist_id=dDsSendDistId[each]['dist_id'],ta_protocol=dDsSendDistId[each]['protocol'])
                    try : 
                        packets = llk_com_pcta.llk_traffic_pcta_stop_cmd (side='user',stop_type='measure',\
                        ta_protocol=dDsSendDistId[each]['protocol'])
                    except Exception as inst:
                        packets = []
                else :
                    try : 
                        packets = llk_com_pcta.llk_traffic_pcta_stop_cmd (side='user',stop_type='capture',\
                        ta_protocol=dDsSendDistId[each]['protocol'])
                    except Exception as inst:
                        packets = []
                if packets: 
                    lDsPackets = lDsPackets + packets

        # stop capture pcap file
        if pcap_file_name :            
            network_pcap_file = 'network_' + pcta_info['network_nic'] + '_' + pcap_file_name 
            client_pcap_file  = 'client_'  + pcta_info['client_nic']  + '_' + pcap_file_name
            msg_command.stop_capture_msg(pcta_info['network_nic'], msg_path='/tmp',file_name=network_pcap_file)
            msg_command.stop_capture_msg(pcta_info['client_nic'],  msg_path='/tmp',file_name=client_pcap_file)  
            msg_command.scp_pcap_file_to_local('/tmp/'+ network_pcap_file, sTRAF_OUTPUT_DIR + '/' + network_pcap_file)
            msg_command.scp_pcap_file_to_local('/tmp/'+ client_pcap_file,  sTRAF_OUTPUT_DIR + '/' + client_pcap_file)
            msg_command.close_msg_session()

        if dUsMeasOptions and ('start_measure_only' not in str(dUsMeasOptions) \
           and 'stop_measure_only' not in str(dUsMeasOptions)\
           and 'start_stop_measure' not in str(dUsMeasOptions)) :
            logger.info("\n=======================Verify Measure Upstream======================================")
            # verify Upstream measure
            dUsMeasOptions[each].pop('control_mode','')
            for each in sorted(dUsMeasOptions) :  
                try:               
                    result = llk_com_pcta.llk_traffic_pcta_verify_measure (each,compdict=dUsMeasOptions[each],results=lUsPackets)
                except Exception as inst:
                    result = 'fail'
                    logger.warn ("Verify Measure Failed:%s,exception:%s" % (each,inst)) 
         
                if result.lower() == 'fail' : 
                    sResult = 'fail'
                    sFailInfo = sFailInfo + 'Measure Verified failed: Upstream %s \n' % each 

        if dDsMeasOptions and ('start_measure_only' not in str(dDsMeasOptions) \
           and 'stop_measure_only' not in str(dDsMeasOptions)\
           and 'start_stop_measure' not in str(dDsMeasOptions)) :
            logger.info("\n=======================Verify Measure Downstream====================================")
            # verify downstream measure
            dDsMeasOptions[each].pop('control_mode','')
            for each in sorted(dDsMeasOptions) :  
                try :             
                    result = llk_com_pcta.llk_traffic_pcta_verify_measure (each,compdict=dDsMeasOptions[each],results=lDsPackets) 
                except Exception as inst:
                    result = 'fail'
                    logger.warn ("Verify Measure Failed:%s,exception:%s" % (each,inst))
               
                if result.lower() == 'fail' : 
                    sResult = 'fail'
                    sFailInfo = sFailInfo + 'Measure Verified failed: Downstream %s \n' % each

        # verify captured upstream traffic 
        if ('start_capture_only' not in str(dUsCaptOptions) \
            and 'stop_catpure_only' not in str(dUsCaptOptions)\
            and 'start_stop_catpure' not in str(dUsCaptOptions)) : 
            logger.info("\n=======================Verify Capture Upstream======================================")
            dUsCaptOptions.pop('control_mode','')
            for each in sorted(dUsCaptOptions) :
                dCompare = {}
                dUsCaptOptions[each] = _convert_vlan_id(**dUsCaptOptions[each]) 
                # convert packet_count to packets_tot if heavey traffic,set default_tol as 0
                if dUsMeasOptions or 'bit_rate' in str(dUsSendFormat) or 'burst_' in str(dUsSendFormat) :  
                    dUsCaptOptions[each]['default_tol'] = 0         
                    dUsCaptOptions = eval(str(dUsCaptOptions).replace('packet_count','packets_tot'))
                    sVerifyCmd = "llk_com_pcta.llk_traffic_pcta_verify_measure (each,compdict=dUsCaptOptions[each],\
                    results=lUsPackets)"
                else :
                    sVerifyCmd = "llk_com_pcta.llk_traffic_pcta_verify_capture (each,compdict=dUsCaptOptions[each],\
                    packets=lUsPackets)"

                try : 
                    result = eval(sVerifyCmd)
                except Exception as inst:
                    result = 'fail'  
                    logger.warn ("Verify Capture Failed:%s,exception:%s" % (each,inst))
                if result.lower() == 'fail' : 
                    sResult = 'fail'
                    sFailInfo = sFailInfo + 'Capture Verified failed: Upstream %s \n' % each

        # verify captured downstream traffic 
        if ('start_capture_only' not in str(dDsCaptOptions) \
            and 'stop_catpure_only' not in str(dDsCaptOptions)\
            and 'start_stop_catpure' not in str(dDsCaptOptions)) : 
            logger.info("\n=======================Verify Capture Downstream====================================")
            dDsCaptOptions.pop('control_mode','')
            for each in sorted(dDsCaptOptions) :
                dCompare = {}
                dDsCaptOptions[each] = _convert_vlan_id(**dDsCaptOptions[each]) 
                # convert packet_count to packets_tot if heavey traffic,set default_tol as 0
                if dDsMeasOptions or 'bit_rate' in str(dDsSendFormat) or 'burst_' in str(dDsSendFormat) :  
                    dDsCaptOptions[each]['default_tol'] = 0         
                    dDsCaptOptions = eval(str(dDsCaptOptions).replace('packet_count','packets_tot'))
                    sVerifyCmd = "llk_com_pcta.llk_traffic_pcta_verify_measure (each,compdict=dDsCaptOptions[each],\
                    results=lDsPackets)"
                else :
                    sVerifyCmd = "llk_com_pcta.llk_traffic_pcta_verify_capture (each,compdict=dDsCaptOptions[each],\
                    packets=lDsPackets)"

                try : 
                    result = eval(sVerifyCmd)
                except Exception as inst:
                    logger.warn ("Verify Capture Failed:%s,exception:%s" % (each,inst))
                    result = 'fail'                  
                if result.lower() == 'fail' : 
                    sResult = 'fail'
                    sFailInfo = sFailInfo + 'Capture Verified failed: Downstream %s \n' % each

        # set function to fail 
        if sResult == 'fail' : 
            raise continueFailure(sFailInfo)
  
    except Exception as inst:
        s = sys.exc_info()
        raise AssertionError("%s-> line:%s, exception:%s" % (keyword_name,s[2].tb_lineno,inst))			

def hlk_traffic_stc(**args) :
    keyword_name = "hlk_traffic_stc"
    aArgs = args
    logger.debug("%s-> %s" % (keyword_name,aArgs))
    global sTRAF_OUTPUT_DIR
    sResult = ''
    sFailInfo = ''

    # store the stream_name of sent packet
    lSentPacketInfo = []

    # check input args
    assert (aArgs.keys()), "No any input arguments given !"
 
    # convert all input key to lower()
    aArgs = dict(map(lambda (k,v): (str(k).lower(), str(v)), aArgs.iteritems()))

    send_delay       = aArgs.setdefault('send_delay',0)
    capture_delay    = aArgs.setdefault('capture_delay',0)
    measure_delay    = aArgs.setdefault('measure_delay',0)
    measure_duration = aArgs.setdefault('measure_duration',30)
    save_stc_cfg     = aArgs.setdefault('save_stc_cfg','')
    # parse arguments from input **args
    dUsSendFormat = {}
    if "us_send_format" in aArgs.keys():
        dUsSendFormat = _convert_format_args(arg_name='us_send_format',format_type='send_traffic',**aArgs)

    dDsSendFormat = {}
    if "ds_send_format" in aArgs.keys():
        dDsSendFormat = _convert_format_args(arg_name='ds_send_format',format_type='send_traffic',**aArgs)

    dUsCaptOptions = {}
    if "us_capt_opts" in aArgs.keys():
        dUsCaptOptions = _convert_format_args(arg_name='us_capt_opts',format_type='capt_opts',**aArgs)

    dDsCaptOptions = {}
    if "ds_capt_opts" in aArgs.keys():
        dDsCaptOptions = _convert_format_args(arg_name='ds_capt_opts',format_type='capt_opts',**aArgs)  
  
    dUsMeasOptions = {}
    if "us_meas_opts" in aArgs.keys():
        dUsMeasOptions = _convert_format_args(arg_name='us_meas_opts',format_type='meas_opts',**aArgs)

    dDsMeasOptions = {}
    if "ds_meas_opts" in aArgs.keys():
        dDsMeasOptions = _convert_format_args(arg_name='ds_meas_opts',format_type='meas_opts',**aArgs)

    dUsPortMeasOptions = {}
    if "us_port_meas_opts" in aArgs.keys():
        dUsPortMeasOptions = _convert_format_args(arg_name='us_port_meas_opts',format_type='meas_opts',**aArgs)

    dDsPortMeasOptions = {}
    if "ds_port_meas_opts" in aArgs.keys():
        dDsPortMeasOptions = _convert_format_args(arg_name='ds_port_meas_opts',format_type='meas_opts',**aArgs)

    try : 
        sClientPort = llk_com_stc.llk_traffic_stc_get_port_id('client')
        sNetworkPort = llk_com_stc.llk_traffic_stc_get_port_id('network')

        # change client port mode to Burst, except loadload, others are all burst stream packet, for example, packet_count, burst_size, burst_rate
        if 'load.load' not in str(dUsSendFormat) :
            for each in sorted(dUsSendFormat) :
                #if 'burst_size' not in dUsSendFormat[each] and 'burst_rate' not in dUsSendFormat[each] and 'load.load' not in dUsSendFormat[each] :
                #    dUsSendFormat[each].setDefault('burst_size',1)

                iBurstSize = dUsSendFormat[each].pop('burst_size',1)
                iBurstRate = dUsSendFormat[each].pop('burst_rate',1)                                 
                llk_com_stc.llk_traffic_stc_config_port(portID=sClientPort,\
                durationMode='BURSTS',schedulingMode='RATE_BASED',burstSize=iBurstSize,duration=iBurstRate)

        # change network port mode to Burst
        if 'load.load' not in str(dDsSendFormat) :
            for each in sorted(dDsSendFormat) :
                iBurstSize = dDsSendFormat[each].pop('burst_size',1)
                iBurstRate = dDsSendFormat[each].pop('burst_rate',1)                  
                llk_com_stc.llk_traffic_stc_config_port(portID=sNetworkPort,\
                durationMode='BURSTS',schedulingMode='RATE_BASED',burstSize=iBurstSize,duration=iBurstRate)

        # create upstream traffic        
        for each in sorted(dUsSendFormat) :
            lSentPacketInfo.append(each) 
            protocol_type = dUsSendFormat[each].pop('ta_protocol').lower()
            logger.info("create upstream traffic:%s" % dUsSendFormat[each])
            # set load unit
            if dUsSendFormat[each].has_key('load.load') : 
                dUsSendFormat[each]['load.loadunit'] = 'BITS_PER_SECOND'
            #dUsSendFormat[each].setDefault('burst_size',1)
            #dUsSendFormat[each].setdefault('load.load','1000')
            #dUsSendFormat[each].setdefault('load.loadunit','BITS_PER_SECOND')
            llk_com_stc.llk_traffic_stc_create_streamblock(sSBName=each,\
            sPortId=sClientPort,sProtocolType=protocol_type,**dUsSendFormat[each])       	
					
        # create downstream traffic        
        for each in sorted(dDsSendFormat) :
            lSentPacketInfo.append(each) 
            protocol_type = dDsSendFormat[each].pop('ta_protocol').lower()
            if dDsSendFormat[each].has_key('load.load') : 
                dDsSendFormat[each]['load.loadunit'] = 'BITS_PER_SECOND'
            #dDsSendFormat[each].setdefault('load.load','1000')
            #dDsSendFormat[each].setdefault('load.loadunit','BITS_PER_SECOND')
            logger.info("create downstream traffic:%s" % dDsSendFormat[each])
            llk_com_stc.llk_traffic_stc_create_streamblock(sSBName=each,\
            sPortId=sNetworkPort,sProtocolType=protocol_type,**dDsSendFormat[each])  


        # start capture
        # not 'verify_capture_only' and 'stop_capture_only'
        if ( dUsCaptOptions or dDsCaptOptions ) \
           and ( 'verify_capture_only' not in str(dUsCaptOptions)+str(dDsCaptOptions)) \
           and ( 'stop_capture_only' not in str(dUsCaptOptions)+str(dDsCaptOptions)) :
            llk_com_stc.llk_traffic_stc_start_capture(portID='all')


        # start traffic
        # not 'stop_traffic_only'
        if ( dUsSendFormat or dDsSendFormat ) \
           and ( 'stop_traffic_only' not in str(dUsSendFormat)+str(dDsSendFormat)) :
            logger.info("sleep %s seconds before sending traffic!" % send_delay )
            time.sleep(int(send_delay))
            llk_com_stc.llk_traffic_stc_start_traffic(streamblock='all')


        # start measure port 
        if  ( dUsPortMeasOptions or dDsPortMeasOptions) \
            and ('verify_measure_only' not in str(dUsPortMeasOptions)+str(dDsPortMeasOptions)) \
            and ('stop_measure_only' not in str(dUsPortMeasOptions)+str(dDsPortMeasOptions)) :
            llk_com_stc.llk_traffic_stc_start_measure(clearResults='FALSE',waitTime=4,\
            resulttype='analyzerportresults',configtype='analyzer')

        # get measure result type
        sMeasureResultType = llk_com_stc.llk_traffic_stc_get_measure_result_type()

        # start measure traffic
        if ( dUsMeasOptions or dDsMeasOptions ) \
            and ( 'verify_measure_only' not in str(dUsMeasOptions)+str(dDsMeasOptions)) \
            and ( 'stop_measure_only' not in str(dUsMeasOptions)+str(dDsMeasOptions)) :
            llk_com_stc.llk_traffic_stc_start_measure(clearResults='FALSE',waitTime=4,\
            resulttype=sMeasureResultType,configtype='StreamBlock')	
        
        # verify measure
        if dUsMeasOptions or dDsMeasOptions :
            logger.info("sleep %s seconds before start measure traffic!" % measure_duration)
            time.sleep(int(measure_duration))

        if dUsMeasOptions and ('start_measure_only' not in str(dUsMeasOptions) \
           and 'stop_measure_only' not in str(dUsMeasOptions)\
           and 'start_stop_measure' not in str(dUsMeasOptions)) :
            # verify upstream measure
            for each in sorted(dUsMeasOptions) :  
                dUsMeasOptions[each].pop('control_mode','')
                logger.info("=========================================================================================")
                logger.info("Verify Measure Upstream Streamblock %s" % each)
                result = ''
                for i in range (0,10) : 
                    try : 
                        result = llk_com_stc.llk_traffic_stc_verify_measure_stream(streamblock=each,\
                        type=sMeasureResultType,**dUsMeasOptions[each]) 
                        if result.lower() == 'pass' :
                            break	
                    except Exception as inst:
                        logger.info("Verify Measure Upstream Streamblock %s second time!\nException:%s" % (each,inst))
                        time.sleep(0.5)                        
                if result == 'fail' :
                    sResult = 'fail'
                    sFailInfo = sFailInfo + 'Measure Verified failed: Upstream %s \n' % each
                logger.info("=========================================================================================")

        if dDsMeasOptions and ('start_measure_only' not in str(dDsMeasOptions) \
           and 'stop_measure_only' not in str(dDsMeasOptions)\
           and 'start_stop_measure' not in str(dDsMeasOptions)) :
            # verify downstream measure
            for each in sorted(dDsMeasOptions) :  
                dDsMeasOptions[each].pop('control_mode','')
                logger.info("=========================================================================================")
                logger.info("Verify Measure Downstream Streamblock %s" % each)
                result = ''
                for i in range (0,10) : 
                    try : 
                        result = llk_com_stc.llk_traffic_stc_verify_measure_stream(streamblock=each,\
                        type=sMeasureResultType,**dDsMeasOptions[each])
                        if result.lower() == 'pass' : 
                            break
                    except Exception as inst:
                        logger.info("Verify Measure Downstream Streamblock %s second time!\nException:%s" % (each,inst))
                        time.sleep(0.5)                      
                if result == 'fail' :
                    sResult = 'fail'
                    sFailInfo = sFailInfo + 'Measure Verified failed: Downstream %s \n' % each
                    
                logger.info("========================================================================================")	

        if dUsPortMeasOptions and ('start_measure_only' not in str(dUsPortMeasOptions) \
           and 'stop_measure_only' not in str(dUsPortMeasOptions)\
           and 'start_stop_measure' not in str(dUsPortMeasOptions)) :
            # verify upstream (network) port measure
            for each in sorted(dUsPortMeasOptions) :  
                dUsPortMeasOptions[each].pop('control_mode','')
                logger.info("=========================================================================================")
                logger.info("Verify Measure Upstream Network Port %s" % each)
                logger.info("Verify Condition: %s" % dUsPortMeasOptions[each])
                result = ''
                for i in range (0,10) : 
                    try : 
                        result = llk_com_stc.llk_traffic_stc_verify_measure_port(type='analyzerportresults',**dUsPortMeasOptions[each]) 
                        if result.lower() == 'pass' :
                            break	
                    except Exception as inst:
                        logger.info("Verify Measure Upstream Network Port %s second time!\nException:%s" % (each,inst))
                        time.sleep(0.5)                        
                if result == 'fail' :
                    sResult = 'fail'
                    sFailInfo = sFailInfo + 'Measure Port Verified failed: Port %s \n' % each
                logger.info("=========================================================================================")

        if dDsPortMeasOptions and ('start_measure_only' not in str(dDsPortMeasOptions) \
           and 'stop_measure_only' not in str(dDsPortMeasOptions)\
           and 'start_stop_measure' not in str(dDsPortMeasOptions)) : 
            # verify downstream (client) port measure
            for each in sorted(dDsPortMeasOptions) :  
                dDsPortMeasOptions[each].pop('control_mode','')
                logger.info("=========================================================================================")
                logger.info("Verify Measure Downstream Client Port %s" % each)
                logger.info("Verify Condition: %s" % dDsPortMeasOptions[each])
                result = ''
                for i in range (0,10) : 
                    try : 
                        result = llk_com_stc.llk_traffic_stc_verify_measure_port(type='analyzerportresults',**dDsPortMeasOptions[each])
                        if result.lower() == 'pass' : 
                            break
                    except Exception as inst:
                        logger.info("Verify Measure Downstream Client Port %s second time!\nException:%s" % (each,inst))
                        time.sleep(0.5)                      
                if result == 'fail' :
                    sResult = 'fail'
                    sFailInfo = sFailInfo + 'Measure Port Verified failed: Port %s \n' % each
                    
                logger.info("========================================================================================")

        # stop measure
        if dUsMeasOptions or dDsMeasOptions : 
            llk_com_stc.llk_traffic_stc_stop_measure(resulttype=sMeasureResultType)
        if dUsPortMeasOptions or dDsPortMeasOptions : 
            llk_com_stc.llk_traffic_stc_stop_measure(resulttype='analyzerportresults')

        # stop capture
        # not 'verify_capture_only' and 'start_capture_only'
        if ( dUsCaptOptions or dDsCaptOptions ) \
           and ('verify_capture_only' not in str(dUsCaptOptions)+str(dDsCaptOptions)) \
           and ('start_capture_only' not in str(dUsCaptOptions)+str(dDsCaptOptions)):
            logger.info("sleep %s seconds before stop capture traffic!" % capture_delay)
            time.sleep(int(capture_delay))
            llk_com_stc.llk_traffic_stc_stop_capture_without_parse_pkt(portID='all')	

        # verify captured traffic  
        # not 'start_capture_only' and 'stop_capture_only' and 'start_stop_capture' 
        if ('start_capture_only' not in str(dUsCaptOptions) \
           and 'stop_catpure_only' not in str(dUsCaptOptions)\
           and 'start_stop_catpure' not in str(dUsCaptOptions)) : 
            for each in sorted(dUsCaptOptions) :
                logger.info("======================================================================================")
                logger.info("Verify Capture Upstream Streamblock %s" % each)
                # delete control_mode
                dUsCaptOptions[each].pop('control_mode','')
                logger.info('Verify Condition: %s' % dUsCaptOptions[each])
                result = llk_com_stc.llk_traffic_stc_verify_capture_without_parse_pkt(portID=sNetworkPort,verifyParam=dUsCaptOptions[each])        
                logger.info("======================================================================================")
                if result == 'fail' :
                    sResult = 'fail'
                    sFailInfo = sFailInfo + 'Capture Verified failed: Upstream %s \n' % each

        # not 'start_capture_only' and 'stop_capture_only' and 'start_stop_capture'
        if ('start_capture_only' not in str(dDsCaptOptions) \
           and 'stop_capture_only' not in str(dDsCaptOptions)\
           and 'start_stop_catpure' not in str(dDsCaptOptions)) :
            for each in sorted(dDsCaptOptions) :
                logger.info("======================================================================================")
                logger.info("Verify Capture Downstream Streamblock %s" % each)
                # delete control_mode
                dDsCaptOptions[each].pop('control_mode','')
                logger.info('Verify Condition: %s' % dDsCaptOptions[each])
                result = llk_com_stc.llk_traffic_stc_verify_capture_without_parse_pkt(portID=sClientPort,verifyParam=dDsCaptOptions[each])        
                logger.info("======================================================================================")
                if result == 'fail' : 
                    sResult = 'fail'
                    sFailInfo = sFailInfo + 'Capture Verified failed: Downstream %s \n' % each

        # stop sending traffic
        if ( dUsSendFormat or dDsSendFormat ) \
           and ( 'start_traffic_only' not in str(dUsSendFormat)+str(dDsSendFormat)) :
            llk_com_stc.llk_traffic_stc_stop_traffic(streamblock='all') 
        
        # Add option save_stc_cfg to hlk_traffic
        stc_cfg_filename = ''
        if save_stc_cfg.lower() == '' or save_stc_cfg.lower() == 'false' :
            logger.info("No need to store stc configure file. \n")
        elif save_stc_cfg.lower() == 'true' : 
            stc_cfg_filename = sTRAF_OUTPUT_DIR + '/' + sLOG_CASE_NAME + time.strftime("_%m-%d-%Y_%H-%M-%S") + '.tcc' 
        else :
            stc_cfg_filename = sTRAF_OUTPUT_DIR +  '/' + save_stc_cfg
        logger.info("The stc cfg file name is :%s" % stc_cfg_filename)
        if stc_cfg_filename :
            stc_cfg_cmd = 'stc::perform SaveToTcc -filename %s' % stc_cfg_filename
            llk_com_stc.llk_traffic_stc_issue_cmd(stc_cfg_cmd)

        # delete streamblocks
        # not 'keep_stream'
        if (dUsSendFormat or dDsSendFormat) \
           and ('keep_stream' not in str(dUsSendFormat)+str(dDsSendFormat)) : 
            llk_com_stc.llk_traffic_stc_delete_streamblock(streamblock='all')	

        # change durationMode to continous 
        if 'burst_' in dUsSendFormat :
            llk_com_stc.llk_traffic_stc_config_port(portID=sClientPort,\
            durationMode='CONTINUOUS', burstSize=1)

        if 'burst_' in dDsSendFormat :
            llk_com_stc.llk_traffic_stc_config_port(portID=sNetworkPort,\
            durationMode='CONTINUOUS', burstSize=1)

        # set function to fail
        if sResult == 'fail' :
            raise continueFailure(sFailInfo)
  
    except Exception as inst:
        s = sys.exc_info()
        raise AssertionError("%s-> line:%s, exception:\n%s" % (keyword_name,s[2].tb_lineno,inst))  

def hlk_device(device_type,action_type,**args) :
    """
    operate device, creating, verifying, operating and deleting per device type

    usage Example:
    | hlk device | device_type=dhcp_server | action_type=create | **args |
    | hlk device | device_type=dhcp_server | action_type=create | lif_side=network | device_name=ipv4_device | ipv4if={address=192.85.1.16,gateway=192.85.1.1} | ethiiif={sourceMac=00:00:11:22:33:44} | vlanif={vlan1=100,pri1=0,vlan2=101,pri2=0} | serverConfig=[Active=TRUE,hostName=dhcp_server1,leaseTime=600] | serverdefaultpoolconfig=[StartIpList=192.85.1.5,hostAddrCount=16,addrIncrement=2,Active=TRUE] | option_66=[msgType=OFFER,HexValue=FALSE,payload=135.251.200.1] |
    | hlk device | device_type=igmp_host | action_type=verify | device_name | expect_state | session_name="first_stc_session" | check_times=1 |
    | hlk device | device_type=device_pppoe_server | action_type=delete | session_name=first_stc_session |
    | hlk device | device_type=igmp_host | action_type=create | lif_side=client | device_name=device_igmp_host | ipv4if={address=192.85.1.16,gateway=192.85.1.1} | ethiiif={sourceMac=00:00:11:22:33:44} | hostconfig={Active=TRUE,version=igmp_v3} | groupmembership={Active=TRUE,FilterMode=INCLUDE,UserDefinedSources=TRUE} | multicastsource={StartIpList=1.0.0.2,PrefixLength=32,NetworkCount=1,AddrIncrement=1,Active=TRUE}       | multicastgroups={StartIpList=225.0.0.1,PrefixLength=32,NetworkCount=1,AddrIncrement=1,Active=TRUE} | vlanif={vlan1=100,pri1=0}        | session_name=first_stc_session |
    | hlk device | device_type=pppoe_server | action_type=create | lif_side=network | device_name=device_pppoe_server | ipv4if={address=192.85.1.15,gateway=192.85.1.1} | ethiiif={sourceMac=00:00:11:22:33:55} | vlanif={vlan1=100,pri1=0,vlan2=101,pri2=0} | pppif={Active=TRUE} | pppoeif={PeerMacAddrList=00:10:94:00:00:05,SessionResolver=PppoeProtocol} | serverconfig={Active=TRUE,Protocol=PPPOE} | emulationmode={EmulationType=server,Active=TRUE} | pppoeserverpool={Ipv4PeerPoolAddr=192.0.1.0,StartIpList=192.0.1.0} | session_name=first_stc_session |
    | hlk device | device_type=dhcpv6_server | action_type=create | lif_side=network | device_name=device_dhcpv6_server | ethiiif={sourceMac=00:00:11:22:33:55} | ipv6if={address=2001::5 ,gateway=2001::1} | vlanif={vlan1=100,pri1=0,vlan2=101,pri2=0} | ipv6llif={Address=fe80::1} | serverconfig={Active=TRUE,EmulationMode=DHCPV6} | serverdefaultaddrpool={HostAddrStep=::1,StartIpList=2000::8,NetworkCount=1} | serverdefaultprefixpool={ServerPrefixStep=::,StartIpList=2000::,NetworkCount =200} | session_name=first_stc_session |
    | hlk device | device_type=8021x_auth | action_type=create | lif_side=client | device_name=device_8021x_client | ipv4if={address=135.251.200.194,gateway=135.251.200.199} | ethiiif={sourceMac=00:00:11:22:33:44} | 8021xconfig={Active=TRUE,EapAuthMethod=MD5} | md5={UserId=cliuser1,Password=password1!} | session_name=first_stc_session |
    | hlk device | device_type=ieee1588v2 | action_type=create | lif_side=client | device_name=device_ieee1588v2_master | ipv4if={address=192.168.1.10,gateway=192.168.1.1} | ethiiif={sourceMac=00:00:11:22:33:44} | vlanif={vlan1=100,pri1=0,vlan2=100,pri2=0} | ieee1588v2clockconfig=[LogSyncInterval=-4,LogMinDelayRequestInterval=-8,StepMode=ONE_STEP,TimeSrc=INTERNAL_OSCILLATOR,ClockClass=6,EnableUnicastNegotiation=TRUE] |
    | hlk device | device_type=ospfv2_router | action_type=create | lif_side=client | device_name=device_ospfv2_router | ipv4if={address=192.85.1.16,gateway=192.85.1.1} | ethiiif={sourceMac=00:00:11:22:33:44} | vlanif={vlan1=100,pri1=0,vlan2=101,pri2=0} | routerconfig={Active=TRUE,AreaId=0.0.0.0} | extroutecount={RouteCountPerRouter=1} | extrouteaddr={StartIpList=21.0.0.0,NetworkCount=1} | session_name=first_stc_session |
    | hlk device | device_type=rip_router | action_type=create | lif_side=client | device_name=device_rip_router | ipv4if={address=192.85.1.16,gateway=192.85.1.1} | ethiiif={sourceMac=00:00:11:22:33:44} | vlanif={vlan1=100,pri1=0,vlan2=101,pri2=0} | routerconfig={Active=TRUE,DutIpv4Addr=224.0.0.9,RipVersion=V2} | riprouteparams={Metric=2} | riprouteaddr={StartIpList=21.0.0.0,NetworkCount=3} | session_name=first_stc_session |
    | hlk device | device_type=bgp_router | action_type=create | lif_side=client | device_name=device_bgp_router | ipv4if={address=192.85.1.16,gateway=192.85.1.1} | ethiiif={sourceMac=00:00:11:22:33:44} | vlanif={vlan1=100,pri1=0,vlan2=101,pri2=0} | routerconfig={Active=TRUE,Dutasnum=200,asnum=100} | bgprouteparams={nexthop=192.85.1.16,aspath=100} | bgprouteaddr={StartIpList=21.0.0.0,NetworkCount=3} | session_name=first_stc_session |
    | hlk device | device_type=ipv4_arp | action_type=create | lif_side=network | device_name=device_ipv4_arp | ipv4if={address=192.85.1.16,gateway=192.85.1.1} | ethiiif={sourceMac=00:00:11:22:33:44} | vlanif={vlan1=100,pri1=0,vlan2=101,pri2=0} |
    - *device_type:* device type for various protocol
    - *action_type:* action type including create, verify, operate and delete.
    - *lif_side:* To mark the device's lif side is eserside or network
    - *device_name:* The name of the device
    - *RETURN VALUE:* NETCONF_OBJ[session_name] or raise error

    """

    try:
        llk_device_cmd = 'llk_com_%s.llk_device_%s_%s_%s(**args)' % (TA_TYPE.lower(),TA_TYPE.lower(),device_type,action_type)
        return eval(llk_device_cmd)
    except Exception as inst:
        raise AssertionError("Fail to trigger %s, exception:%s" % (llk_device_cmd,inst))

class logger_ta_cmd():

    def __init__(self,logName,logPath) :
        self.logger = logging.getLogger(logName)
        self.logger.setLevel(logging.DEBUG)

        fh = logging.FileHandler(logPath,mode="a",encoding="UTF-8")
        formatter = logging.Formatter('%(asctime)s - %(filename)s - %(message)s' );
        fh.setFormatter(formatter);
        fh.setLevel(logging.DEBUG)
        self.logger.addHandler(fh)

    def getLog(self):
        return self.logger

class continueFailure (AssertionError) :
    ROBOT_CONTINUE_ON_FAILURE = True
