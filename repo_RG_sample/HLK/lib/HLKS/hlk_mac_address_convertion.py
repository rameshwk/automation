import re 
import random
from robot.api import logger 

def generate_random_l2cp_mac():
    """
    To generate the Random L2CP Mac within the Limit 
    01:80:C2:00:00:00 to 01:80:C2:00:00:2F
    INPUT ARGUMENT : NONE 
    Return Value : L2CP_MAC
    Author :                    Comments :
    Beno K Immanuel             Developed  
    """   
    r = random.randint;
    l2cp_mac =  "01:80:C2:00:00:%02X" % r(0,0x2f);   
    logger.debug("L2CP MAC : %s " %l2cp_mac)
    return l2cp_mac

def generate_mcast_mac(ip,**prot):
	""" Generate mcast ip

     	Example:
	generate_mcast_mac '224.0.0.5'
	
	Return A mac :01005e000005 
	"""
	lip = ip.split('.')
	bip2 = str.replace(bin(int(lip[1])),'0b','')
	bip3 = str.replace(bin(int(lip[2])),'0b','')
	bip4 = str.replace(bin(int(lip[3])),'0b','')	
	#bip2 = str('0')+str(bip2[1:])
	bip2 = str.zfill(bip2,8)
	bip3 = str.zfill(bip3,8)
	bip4 = str.zfill(bip4,8)
	bip2 = str('0')+str(bip2[1:])
	
	bmac1 = str('0b')+bip2[:4]
	bmac2 = str('0b')+bip2[4:]
	bmac3 = str('0b')+bip3[:4]
	bmac4 = str('0b')+bip3[4:]
	bmac5 = str('0b')+bip4[:4]
	bmac6 = str('0b')+bip4[4:]
	
	hmac1 = str.replace(hex(eval(bmac1)),'0x','')
	hmac2 = str.replace(hex(eval(bmac2)),'0x','')
	hmac3 = str.replace(hex(eval(bmac3)),'0x','')
	hmac4 = str.replace(hex(eval(bmac4)),'0x','')
	hmac5 = str.replace(hex(eval(bmac5)),'0x','')
	hmac6 = str.replace(hex(eval(bmac6)),'0x','')
	mac_result = str('01005e')+str(hmac1)+str(hmac2)+str(hmac3)+str(hmac4)+str(hmac5)+str(hmac6)
	logger.debug("MCAST MAC : %s " %mac_result)
	return mac_result
    
def generate_igmpv3_group_record_list(ip,**prot):
	""" Generate group record list with type of igmp v3 asm/ssm-join/leave

     	Example:
	generate_igmpv3_group_record_list '225.1.1.1' 'type=v3ssm-join'
	
	Return A mac :05000001e1010101
	"""
	lip = ip.split('.')
	bip2 = str.replace(bin(int(lip[1])),'0b','')
	bip3 = str.replace(bin(int(lip[2])),'0b','')
	bip4 = str.replace(bin(int(lip[3])),'0b','')	
	#bip2 = str('0')+str(bip2[1:])
	bip2 = str.zfill(bip2,8)
	bip3 = str.zfill(bip3,8)
	bip4 = str.zfill(bip4,8)
	bip2 = str('0')+str(bip2[1:])
	
	bmac1 = str('0b')+bip2[:4]
	bmac2 = str('0b')+bip2[4:]
	bmac3 = str('0b')+bip3[:4]
	bmac4 = str('0b')+bip3[4:]
	bmac5 = str('0b')+bip4[:4]
	bmac6 = str('0b')+bip4[4:]
	
	hmac1 = str.replace(hex(eval(bmac1)),'0x','')
	hmac2 = str.replace(hex(eval(bmac2)),'0x','')
	hmac3 = str.replace(hex(eval(bmac3)),'0x','')
	hmac4 = str.replace(hex(eval(bmac4)),'0x','')
	hmac5 = str.replace(hex(eval(bmac5)),'0x','')
	hmac6 = str.replace(hex(eval(bmac6)),'0x','')
	if prot.has_key('type') and (prot['type'] in ['v3asm-join','v3asm-leave','v3ssm-join','v3ssm-leave']):
		if   'v3asm-join'  == prot['type'] : strHead = '04000000e1'
		elif 'v3asm-leave' == prot['type'] : strHead = '03000000e1'
		elif 'v3ssm-join'  == prot['type'] : strHead = '05000001e1'
		elif 'v3ssm-leave' == prot['type'] : strHead = '06000001e1'
	else:
		raise AssertionError("need to specify one param of type in: 'v3asm-join','v3asm-leave','v3ssm-join','v3ssm-leave'")
	mac_result = str(strHead)+str(hmac1)+str(hmac2)+str(hmac3)+str(hmac4)+str(hmac5)+str(hmac6)
	logger.debug( "group record list with %s : %s" % (prot['type'], mac_result) )
	return mac_result
    
def mac_with_seperator_to_hex (mac,seperator=":"):
    """ 
    To convert the dot mac format to without colon format 
    Input Argument : mac value 
    Return value   : mac value without colon
    Convert " 00:01:02:03:04:05 " to "000102030405"
    Author :                    Comments :
    Beno K Immanuel             Developed     
    """ 
    mac_val = re.sub(seperator,"",mac.strip())
    logger.debug("CONVERTED MAC : %s " %mac_val)
    return mac_val      
         
def mac_hex_add_seperator (mac,seperator=":"):
    """ 
    To convert the dot mac format to without colon format 
    Input Argument : mac value 
    Return value   : mac value without colon
    Convert " 00:01:02:03:04:05 " to "000102030405"
    Author :                    Comments :
    Beno K Immanuel             Developed     
    """ 
    mac_val = mac[:2]+seperator+mac[2:4]+seperator+mac[4:6]+seperator+mac[6:8]+seperator+mac[8:10]+seperator+mac[10:12]
    logger.debug("MAC : %s " %mac_val)
    return mac_val     

def get_aid_unique_mac (aid,stream_num=1,oui_num=1):
    '''
        get a unique mac based on aid,stream_num,onu_num
        
        aid should be formated like iRack/iShelf/iSlot/iPon/iOnt/iCard/iPort/iSvlan/iCvlan
    '''   
    keyword="get_aid_unique_mac"
    logger.debug("Module:%s, Function:%s -> aid=%s,stream_num=%s oui_num=%s" \
    % (__name__,keyword,aid,stream_num,oui_num))
    if "/" in aid :
        splitlst = aid.split('/')
        if len(splitlst) == 9 :
            (iRack,iShelf,iSlot,iPon,iOnt,iCard,iPort,iSvlan,iCvlan) = aid.split('/')
            #iSlot = 1
            if iSvlan == '65535':
                iSvlan = 0
            if iCvlan == '65535':
                iCvlan = 0
            tmpoui_num = int(oui_num) - 1
            tmpstream_num = int(stream_num) - 1
            sum = int(iSlot) + int(iPon) + int(iOnt) + int(iCard) + int(iPort) 
            uniquemac = "%02x%03x%03x%03x%1x" % (tmpoui_num,sum,int(iSvlan),int(iCvlan),tmpstream_num)
            logger.debug("Unique Mac : %s" % uniquemac)
            return uniquemac
    elif "-" in aid :
        splitlst = aid.split('-')
        if len(splitlst) == 10 :
            (sType,iRack,iShelf,iSlot,iPon,iOnt,iCard,iPort,iSvlan,iCvlan) = aid.split('-')
            if iSvlan == '65535':
                iSvlan = 0
            if iCvlan == '65535':
                iCvlan = 0
            tmpoui_num = int(oui_num) - 1
            tmpstream_num = int(stream_num) - 1
            sum = int(iSlot) + int(iPon) + int(iOnt) + int(iCard) + int(iPort) 
            uniquemac = "%02x%03x%03x%03x%1x" % (tmpoui_num,sum,int(iSvlan),int(iCvlan),tmpstream_num)
            logger.debug("Unique Mac : %s" % uniquemac)
            return uniquemac
    else :
        return None

def convert_mac_syntax(mac,mode="normal_to_cisco"):
    """
      convert mac syntax from ef:00:12:33:fe:fe to ef00.1233.fefe
    """
    mac=mac.replace(":","")
    mac=mac.replace(".","")
    mac=mac.lower()
    
    if mode == "normal_to_cisco":
        mac1=mac[0:4]+"."+mac[4:8]+"."+mac[8:12]
    elif mode == "cisco_to_normal":
        mac1=mac[0:2]+":"+mac[2:4]+":"+mac[4:6]+":"+mac[6:8]+":"+mac[8:10]+":"+mac[10:12]
    elif mode == "any_to_raw":
        mac1=mac
    return(mac1)

def hex2dec(string_num):
    return str(int(string_num.upper(), 16))

def convert_mac_hex_to_dec(mac):
    """
      convert mac format from 00:0e:00:0e:00:0e -> 0.14.0.14.0.14
    """

    mac_list=mac.split(':')

    mac1=hex2dec(mac_list[0])+"."+hex2dec(mac_list[1])+"."+hex2dec(mac_list[2])+"."+hex2dec(mac_list[3])+"."+hex2dec(mac_list[4])+"."+hex2dec(mac_list[5])

    return(mac1)

