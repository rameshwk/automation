import os
import sys
import pexpect
import re
import subprocess
import time
sys.path.append(os.environ.get('ROBOTREPO')+'/LIBS/COM/COM_CLI')
sys.path.append(os.environ.get('ROBOTREPO')+'/HLK/lib')
from Excute_CMD import ExcuteCMD
from optparse import OptionParser

class hlk_Wifi:
    
    def __init__(self):
        """"""
        self.cfg_file = '/tmp/wifi_config_file'
        self.security_mode = ''
        self.pc = ""
        self.library_path = os.getenv('U_PATH_LIBRARY')
        self.logincmd = ""
        self.wificard = ""
        self.ipaddr = ""
        
    def raiseError(self, Message=None):
        raise Exception, Message

    def Get_Wlan_Card_Name(self):
        """
        _Name_:Get wifi interface Name
        
        _Source_:Wifi
        
        _Arguments_:[]

        Use this keyword to get Wireless Interface's name 
        
        _Examples_:
        
        | ${Interface}= | Get wifi interface Name |
        | Log           | ${Interface}       |

        """

        cmd = 'ifconfig -a |grep -o "^ *wlan[0-9][0-9]* *"|awk ' + "'{print $1}'"
        rc, result = ExcuteCMD(cmd)
        if rc:
            data = result.strip()
        else:
            data = None
            self.raiseError("ATC_ERROR: Do NOT have wifi interface !!!")
        return data

    def Up_Wlan_Interface(self):
        """
        _Name_: Up Wlan Interface
        
        _Source_: Wifi
        
        _Arguments_:[]

        Use this keyword to UP Wlan Interface
        
        _Examples_:

        | Up Wlan Interface |

        """

        wlan = self.Get_Wlan_Card_Name()
        if ExcuteCMD(cmd='ifconfig |grep "^ *' + wlan + '"')[0]:
            pass
        else:
            cmd = 'ifconfig ' + wlan + ' up'
            if ExcuteCMD(cmd)[0]:
                pass
            else:
                self.raiseError("ATC_ERROR: " + cmd + " run Fail!")
        return True


    def readfile(self, file, filter=''):
        filter = filter
        match_flag = False
        try:
            file = open(file, 'r')
        except Exception, e:
            print e
            return False
        for line in file:
            print line,
            if filter:
                m = re.findall(filter, line)
                if m:
                    match_flag = True
        file.close()
        return match_flag


    def Scan_SSID(self, ssid='', bssid='', expect='true'):
        
        """
        _Name_:Scan SSID
        
        _Source_:Wifi
        
        _Arguments_:[ ssid | bssid |expect ]
            
        ssid is the ssid's name
        bssid is the ssid's bssid
        expect is used to do Positive and negative test.
        when expect is true, wlan interface should can scan the ssid succeed, if not, the test is failed
        then when expect is false, wlan interface should can not scan ssid, if not, the test is failed 

        _Exmaples_:

        | Scan SSID | ssid=prince2g | bssid=10:9f:a9:70:01:03 |expect=true |
        | Scan SSID | prince2g      | 10:9f:a9:70:01:03       |true        |
        | Scan SSID | ssid=prince2g | bssid=10:9f:a9:70:01:03 |expect=false|
        | Scan SSID | prince2g      | 10:9f:a9:70:01:03       |false       |

        """

        wlan = self.Get_Wlan_Card_Name()
        ssid = str(ssid)
        bssid = str(bssid)
        expect = str(expect).lower()

        self.Up_Wlan_Interface()

        library_path = os.getenv('U_PATH_LIBRARY')

        if ExcuteCMD("lsusb|egrep -i 'Atheros Communications|linksys'")[0]:
            type = 'tplink'
            cmd = 'iw dev ' + wlan + ' scan |grep "' + ssid + '" -B8' + '|grep -i ' + bssid
        elif ExcuteCMD("lsusb|grep -i netgear")[0]:
            type = 'netgear'
            cmd = library_path + '/' + type + '/' + 'wlx86 -i ' + wlan + ' scan && sleep 5 && ' + library_path + '/' + type + '/' + 'wlx86 -i ' + wlan + ' scanresults |grep "' + ssid + '" -A13' + '|grep -i ' + bssid
        else:
            self.raiseError('ATC_ERROR: Unknown wifi interface Type!')

        num = 0
        ttt = 6
        for i in range(ttt):

            if ExcuteCMD(cmd)[0]:
                num += 1
                print 'ATC_INFO: Scan_SSID Successs, can Scan SSID ' + ssid + ' ' + str(num) + ' times!!!'
            else:
                print 'ATC_WARNING: Maybe this SSID is not stable, Scan_SSID RUN FAIL!'
            time.sleep(10)

        print 'expect = ' + expect

        if expect == 'true':
            if num >= 3:
                print 'ATC_INFO: Scan_SSID Successs, Can Scan SSID ' + ssid
                return True
            else:
                self.raiseError("ATC_ERROR: Positive test, Can NOT Scan SSID " + ssid)

        else:
            if num > 0:
                self.raiseError("ATC_ERROR: Negative test, Can Scan SSID " + ssid)
            else:
                print 'ATC_INFO: Scan_SSID Success, can not Scan SSID' + ssid
                return True

    def Start_WPASupplicant(self):
        
        """
        _Name_:Start WPASupplicant
        
        _Source_:Wifi
        
        _Arguments_:[]

        This keyword is used to start wifi tool wpa_supplicant

        _Exmaples_:

        | Start WPASupplicant |

        """
        wlan = self.Get_Wlan_Card_Name()
        os.system('killall wpa_supplicant')
        time.sleep(1)
        if not os.path.exists('/tmp/wifi_config_file'):
            self.raiseError("ATC_ERROR: wifi config file " + self.cfg_file + " does not exist!!!")
        os.system('rm -f /tmp/wpa_supplicant_output')
        cmd = 'nohup wpa_supplicant -d -f /tmp/wpa_supplicant_output -i ' + wlan + ' -c ' + self.cfg_file + ' -B 2>&1 & '
        print "ATC_INFO cmd is " + str(cmd)
        os.system(cmd)
        time.sleep(1)
        if ExcuteCMD('ps aux |grep -v grep |grep wpa_supplicant')[0]:
            print 'ATC_INFO: Start_WPASupplicant RUN SUCCESS SUCCESS!'
            print "sleep 10s"
            time.sleep(10)
            return True
        else:
            ExcuteCMD('cat /tmp/wpa_supplicant_output')
            self.raiseError("ATC_ERROR:Start wpa_supplicant Fail!")

    def Check_WPASupplicant(self, expect='true'):

        """
        _Name_:Check WPASupplciant
        
        _Source_:Wifi
        
        _Arguments_:[ expect ]
            
        expect is used to do Positive and negative test.
        when expect is true, wlan interface should can scan the ssid succeed, if not, the test is failed
        then when expect is false, wlan interface should can not scan ssid, if not, the test is failed 
        If not set this argument,itsdefault value is true

        _Exmaples_:

        | Check WPASupplciant |true    |
        | Check WPASupplciant |expect=false|
        | Check WPASupplciant | 

        """

        time.sleep(10)
        cmd = 'wpa_cli status'
        expect = str(expect).lower()
        num = 0


        if expect == 'true':
            for i in range(12):
                print 'sleep 5 seconds'
                time.sleep(5)

                rc, result = ExcuteCMD(cmd)

                if rc:
                    m = re.findall(r'wpa_state=COMPLETED', result)
                    if m:
                        num += 1
                        print 'ATC_INFO: wpa_state is COMPLETED ' + str(num) + ' times!'

                        if num >= 3:
                            print 'ATC_INFO: num >=3,Check_WPASupplicant Postive Test Successs!!!! wpa_state is COMPLETED'
                            return True
                        else:
                            print 'ATC_INFO: num <3,Need continue to check wpa_state, run Check_WPASupplicant again!!!'

                    else:
                        print 'ATC_INFO: wpa_state is not COMPLETED'

                else:
                    print "ATC_ERROR: rc != 0, Check WPASupplicant RUN FAIL!!!"
            ExcuteCMD('wpa_cli term')
            ExcuteCMD('sync')
            f = os.popen('cat /tmp/wpa_supplicant_output')
            for i in f:
                print str(i).strip()
            f.close()
            self.raiseError("ATC_ERROR: Positive Test,Can NOT Connect SSID!")

        else:
            for i in range(10):
                print 'sleep 5 seconds'
                time.sleep(5)
                rc, result = ExcuteCMD(cmd)
                if rc:
                    m = re.findall(r'wpa_state=COMPLETED', result)
                    if m:
                        print 'ATC_ERROR: wpa_state is COMPLETED'
                        self.raiseError("ATC_ERROR: Negative Test, Can Connect SSID!")
                    else:
                        print 'ATC_INFO: wpa_state is not COMPLETED'
                else:
                    print "ATC_ERROR: rc != 0, Check WPASupplicant RUN FAIL!!!"

            print "ATC_INFO: num = 0,Check_WPASupplicant Negative Test Successs!!!! wpa_state is not COMPLETED"
            return True

    def Generate_Config_file(self, ssid='', bssid='', type='', key=''):
        """
        _Name_:Generate Config file
        
        _Source_:Wifi
        
        _Arguments_:[ ssid | bssid | type | key ]
        
        ssid is DUT's SSID Name
        bssid is DUT's BSSID Name
        type is DUT's wireless encryption method, the value as follows:
        none
        wep-open-1
        wep-open-2
        wep-open-3
        wep-open-4
        wep-shard-1
        wep-shard-2
        wep-shard-3
        wep-shard-4
        wpa-aes
        wpa-tkip
        wpa-both
        wpa2-aes
        wpa2-tkip
        wpa2-both
        wpawpa2-aes
        wpawpa2-tkip
        wpawpa2-both
        key is DUT SSID's password
      
        Generate Wifi Config file for Wireless Card
        -Give this Keyword the 4 arguments of a AP: ssid, bssid, type and key
        -The Case will Generate the Config file named wifi_config_file for wpa_supplicant

        _Examples_:

        | Generate Config file | ssid=prince2g | bssid=10:9f:a9:70:01:03 | type=WPA-AES   |   key=1234567890   |
        | Generate Config file | ssid=prince2g | bssid=10:9f:a9:70:01:03 | type=WEP-SHARED-1  |   key=1234567890   |
        | Generate Config file | prince2g | 10:9f:a9:70:01:03 | WPA-AES   |  1234567890   |
        | Generate Config file | prince2g | 10:9f:a9:70:01:03 | WEP-SHARED-1   |  1234567890   |
        | Generate Config file | ssid=prince2g | type=WPA-AES   |   key=1234567890   |
        | Generate Config file | ssid=prince2g | type=WEP-SHARED-1  |   key=1234567890   |
        | Generate Config file | prince2g | WPA-AES   |  1234567890   |
        | Generate Config file | prince2g | WEP-SHARED-1   |  1234567890   |

       
       """
        ssid = str(ssid)
        bssid = str(bssid)
        type = str(type).lower()
        key = str(key)
        try:
            file = open(self.cfg_file, 'w')
        except Exception, e:
            print e
            return False

        file.writelines('ctrl_interface=/var/run/wpa_supplicant' + os.linesep)
        file.writelines('eapol_version=1' + os.linesep)
        file.writelines('ap_scan=1' + os.linesep)
        file.writelines('fast_reauth=1' + os.linesep)
        file.writelines('network={' + os.linesep)
        file.writelines('    ssid=\"' + ssid + '\"' + os.linesep)

        if bssid:
            file.writelines('    bssid=' + bssid + os.linesep)
        else:
            print "Not need bssid in the config file."
        
        if type.startswith('wep'):
            print "type is start with wep"
            m = re.match(r'(\w+)-(\w+)-([1-4])', type)

            if m:
                print m.group(1)
                print m.group(2)
                print m.group(3)

                if m.group(2) == 'open':
                    print "method is open"
                    print "key's length is %d" % len(key)

                    if len(key) in [5, 13]:
                        print "Method is ASCII"
                        file.writelines('    scan_ssid=1' + os.linesep)
                        file.writelines('    key_mgmt=NONE' + os.linesep)
                        file.writelines('    priority=5' + os.linesep)
                        file.writelines('    wep_key' + str(int(m.group(3)) - 1) + '=\"' + key + '\"' + os.linesep)
                        file.writelines('    wep_tx_keyidx=' + str(int(m.group(3)) - 1) + os.linesep)
                        file.writelines('    auth_alg=OPEN' + os.linesep)
                        file.writelines('}' + os.linesep)
                        file.close()

                    elif len(key) in [10, 26]:
                        print "Method is HEX"
                        file.writelines('    scan_ssid=1' + os.linesep)
                        file.writelines('    key_mgmt=NONE' + os.linesep)
                        file.writelines('    priority=5' + os.linesep)
                        file.writelines('    wep_key' + str(int(m.group(3)) - 1) + '=' + key + os.linesep)
                        file.writelines('    wep_tx_keyidx=' + str(int(m.group(3)) - 1) + os.linesep)
                        file.writelines('    auth_alg=OPEN' + os.linesep)
                        file.writelines('}' + os.linesep)
                        file.close()
                    else:
                        self.raiseError("The length of Key is %d, it should be 5, 10, 13 or 26" % len(key))

                elif m.group(2) == 'shared':
                    print "method is shared"
                    print "key's length is %d" % len(key)
                    if len(key) in [5, 13]:
                        print "Method is ASCII"
                        file.writelines('    scan_ssid=1' + os.linesep)
                        file.writelines('    key_mgmt=NONE' + os.linesep)
                        file.writelines('    priority=5' + os.linesep)
                        file.writelines('    wep_key' + str(int(m.group(3)) - 1) + '=\"' + key + '\"' + os.linesep)
                        file.writelines('    wep_tx_keyidx=' + str(int(m.group(3)) - 1) + os.linesep)
                        file.writelines('    auth_alg=SHARED' + os.linesep)
                        file.writelines('}' + os.linesep)
                        file.close()

                    elif len(key) in [10, 26]:
                        print "Method is HEX"
                        file.writelines('    scan_ssid=1' + os.linesep)
                        file.writelines('    key_mgmt=NONE' + os.linesep)
                        file.writelines('    priority=5' + os.linesep)
                        file.writelines('    wep_key' + str(int(m.group(3)) - 1) + '=' + key + os.linesep)
                        file.writelines('    wep_tx_keyidx=' + str(int(m.group(3)) - 1) + os.linesep)
                        file.writelines('    auth_alg=SHARED' + os.linesep)
                        file.writelines('}' + os.linesep)
                        file.close()
                    else:
                        self.raiseError("The length of Key is %d, it should be 5, 10, 13 or 26" % len(key))
                else:
                    self.raiseError("wep should be open or shared!!!")

                self.readfile(self.cfg_file)
                return True

            else:
                self.raiseError("wep method wrong!!!")
            
        elif type == 'wpa-psk':
            file.writelines('    scan_ssid=1' + os.linesep)
            file.writelines('    key_mgmt=WPA-PSK' + os.linesep)
            file.writelines('    priority=5' + os.linesep)
            file.writelines('    pairwise=CCMP' + os.linesep)
            file.writelines('    proto=WPA RSN' + os.linesep)
            lenkey = len(key)
            if lenkey >= 64:
                file.writelines('    psk=' + key + '' + os.linesep)
            else:
                file.writelines('    psk=\"' + key + '\"' + os.linesep)
            file.writelines('}' + os.linesep)
            file.close()
            self.readfile(self.cfg_file)
            return True

        elif type == 'wpa-aes':
#             file.writelines('    bssid=' + bssid + os.linesep)
            file.writelines('    scan_ssid=1' + os.linesep)
            file.writelines('    key_mgmt=WPA-PSK' + os.linesep)
            file.writelines('    priority=5' + os.linesep)
            file.writelines('    pairwise=CCMP' + os.linesep)
            file.writelines('    proto=WPA' + os.linesep)
            lenkey = len(key)
            if lenkey >= 64:
                file.writelines('    psk=' + key + '' + os.linesep)
            else:
                file.writelines('    psk=\"' + key + '\"' + os.linesep)
            file.writelines('}' + os.linesep)
            file.close()
            self.readfile(self.cfg_file)
            return True

        elif type == 'wpa-tkip':
            file.writelines('    scan_ssid=1' + os.linesep)
            file.writelines('    key_mgmt=WPA-PSK' + os.linesep)
            file.writelines('    priority=5' + os.linesep)
            file.writelines('    pairwise=TKIP' + os.linesep)
            file.writelines('    proto=WPA' + os.linesep)
            lenkey = len(key)
            if lenkey >= 64:
                file.writelines('    psk=' + key + '' + os.linesep)
            else:
                file.writelines('    psk=\"' + key + '\"' + os.linesep)
            file.writelines('}' + os.linesep)
            file.close()
            self.readfile(self.cfg_file)
            return True

        elif type == 'wpa-both':
            file.writelines('    scan_ssid=1' + os.linesep)
            file.writelines('    key_mgmt=WPA-PSK' + os.linesep)
            file.writelines('    priority=5' + os.linesep)
            file.writelines('    pairwise=TKIP CCMP' + os.linesep)
            file.writelines('    proto=WPA' + os.linesep)
            lenkey = len(key)
            if lenkey >= 64:
                file.writelines('    psk=' + key + '' + os.linesep)
            else:
                file.writelines('    psk=\"' + key + '\"' + os.linesep)
            file.writelines('}' + os.linesep)
            file.close()
            self.readfile(self.cfg_file)
            return True

        elif type == 'wpa2-aes':
            file.writelines('    scan_ssid=1' + os.linesep)
            file.writelines('    key_mgmt=WPA-PSK' + os.linesep)
            file.writelines('    priority=5' + os.linesep)
            file.writelines('    pairwise=CCMP' + os.linesep)
            file.writelines('    proto=RSN' + os.linesep)
            lenkey = len(key)
            if lenkey >= 64:
                file.writelines('    psk=' + key + '' + os.linesep)
            else:
                file.writelines('    psk=\"' + key + '\"' + os.linesep)
            file.writelines('}' + os.linesep)
            file.close()
            self.readfile(self.cfg_file)
            return True

        elif type == 'wpa2-tkip':
            file.writelines('    scan_ssid=1' + os.linesep)
            file.writelines('    key_mgmt=WPA-PSK' + os.linesep)
            file.writelines('    priority=5' + os.linesep)
            file.writelines('    pairwise=TKIP' + os.linesep)
            file.writelines('    proto=RSN' + os.linesep)
            lenkey = len(key)
            if lenkey >= 64:
                file.writelines('    psk=' + key + '' + os.linesep)
            else:
                file.writelines('    psk=\"' + key + '\"' + os.linesep)
            file.writelines('}' + os.linesep)
            file.close()
            self.readfile(self.cfg_file)
            return True

        elif type == 'wpa2-both':
            file.writelines('    scan_ssid=1' + os.linesep)
            file.writelines('    key_mgmt=WPA-PSK' + os.linesep)
            file.writelines('    priority=5' + os.linesep)
            file.writelines('    pairwise=TKIP CCMP' + os.linesep)
            file.writelines('    proto=RSN' + os.linesep)
            lenkey = len(key)
            if lenkey >= 64:
                file.writelines('    psk=' + key + '' + os.linesep)
            else:
                file.writelines('    psk=\"' + key + '\"' + os.linesep)
            file.writelines('}' + os.linesep)
            file.close()
            self.readfile(self.cfg_file)
            return True

        elif type == "wpawpa2-tkip":
            file.writelines('    scan_ssid=1' + os.linesep)
            file.writelines('    key_mgmt=WPA-PSK' + os.linesep)
            file.writelines('    priority=5' + os.linesep)
            file.writelines('    pairwise=TKIP' + os.linesep)
            file.writelines('    proto=WPA RSN' + os.linesep)
            lenkey = len(key)
            if lenkey >= 64:
                file.writelines('    psk=' + key + '' + os.linesep)
            else:
                file.writelines('    psk=\"' + key + '\"' + os.linesep)
            file.writelines('}' + os.linesep)
            file.close()
            self.readfile(self.cfg_file)
            return True

        elif type == "wpawpa2-aes":
            file.writelines('    scan_ssid=1' + os.linesep)
            file.writelines('    key_mgmt=WPA-PSK' + os.linesep)
            file.writelines('    priority=5' + os.linesep)
            file.writelines('    pairwise=CCMP' + os.linesep)
            file.writelines('    proto=WPA RSN' + os.linesep)
            lenkey = len(key)
            if lenkey >= 64:
                file.writelines('    psk=' + key + '' + os.linesep)
            else:
                file.writelines('    psk=\"' + key + '\"' + os.linesep)
            file.writelines('}' + os.linesep)
            file.close()
            self.readfile(self.cfg_file)
            return True

        elif type == "wpawpa2-both":
            file.writelines('    scan_ssid=1' + os.linesep)
            file.writelines('    key_mgmt=WPA-PSK' + os.linesep)
            file.writelines('    priority=5' + os.linesep)
            file.writelines('    pairwise=TKIP CCMP' + os.linesep)
            file.writelines('    proto=WPA RSN' + os.linesep)
            lenkey = len(key)
            if lenkey >= 64:
                file.writelines('    psk=' + key + '' + os.linesep)
            else:
                file.writelines('    psk=\"' + key + '\"' + os.linesep)
            file.writelines('}' + os.linesep)
            file.close()
            self.readfile(self.cfg_file)
            return True

        elif type == "none":
            file.writelines('    scan_ssid=1' + os.linesep)
            file.writelines('    key_mgmt=NONE' + os.linesep)
            file.writelines('    priority=5' + os.linesep)
            file.writelines('}' + os.linesep)
            file.close()
            self.readfile(self.cfg_file)
            return True

        else:
            self.raiseError("ATC_ERROR:  wifi security type error,Can NOT Create right wifi config file!")


    def connect_AP(self, ssid='', bssid='', type='', key='', expect='true'):
        """
        _Name_:connect AP
        
        _Source_:Wifi
        
        _Arguments_:[ ssid | bssid | type | key | expect ]

        ssid is DUT's SSID Name
        
        bssid is DUT's BSSID Name
        
        type is DUT's wireless encryption method, the value as follows:
        none
        wep-open
        wep-shard
        wpa-aes
        wpa-tkip
        wpa-both
        wpa2-aes
        wpa2-tkip
        wpa2-both
        wpawpa2-aes
        wpawpa2-tkip
        wpawpa2-both
        
        key is DUT SSID's password
        
        expect is used to do Positive and negative test.
        when expect is true, wlan interface should can connect the ssid succeed, if not, the test is failed
        then when expect is false, wlan interface should can not connect ssid, if not, the test is failed 
        If not set this argument,itsdefault value is true

        This keyword is used to check Wireless Card can connect Wireless AP or not

        -Give this Keyword the 4 arguments of a AP: ssid, bssid, type and key

        -Then Case Word will scan SSID Exist or not ,and generate a Wifi config file
        If  Scan SSID Succeed  and config file is Wright, it will invoke wireless
        Tool wpa_supplicant to connect the AP and check the wireless status with
        Wireless Tool wpa_cli.

        -When wpa_state is "COMPLETED", the Keyword run succeed, otherwise it fails.


        _Examples_:

        | connect AP | ssid=roy2g | bssid=10:9f:a9:70:01:03 | type=WPA-AES   |   key=1234567890   |

        | connect AP | ssid=roy2g | bssid=10:9f:a9:70:01:03 | type=WPA2-TKIP   |   key=1234567890   | expect=true |

        | connect AP | roy2g      | 10:9f:a9:70:01:03       | WPAWPA2-BOTH     |   1234567890       | false       |

        """
        ssid = str(ssid)
        bssid = str(bssid)
        type = str(type)
        key = str(key)
        self.Up_Wlan_Interface()
        self.Generate_Config_file(ssid=ssid, bssid=bssid, type=type, key=key)
        self.Start_WPASupplicant()
        self.Check_WPASupplicant(expect=expect)

    def Assign_IP_Address_For_WLAN(self, interface='', ip='', negative=False):
        """
        Assign Ip for wlan card,default is dhclient
        Examples:
        | Assign_IP_Address_For_WLAN |
        | Assign_IP_Address_For_WLAN | wlan2 |
        | Assign_IP_Address_For_WLAN | wlan2 | 192.168.1.150|
        """
        interface = str(interface)
        ExcuteCMD('killall dhclient')
        time.sleep(1)
        ExcuteCMD('route del default')
        time.sleep(1)
        ExcuteCMD('ip addr flush dev eth2')
        time.sleep(1)
        time.sleep(1)
        if ip:
            print "config static ip"
            m = re.match(r'^ *(([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3})\.[0-9]{1,3}) *$', ip)
            if m:
                ip = m.group(1)
                net = m.group(2)
                ExcuteCMD('ifconfig ' + interface + ' ' + ip + ' up')
        else:
            "get ip via dhclient"
            fff = '/var/lib/dhclient/dhclient.leases'
            if os.path.exists(fff):
                os.remove(fff)
            for i in range(3):
                ExcuteCMD('dhclient -r ' + interface)
                time.sleep(1)   
                ExcuteCMD('dhclient -v ' + interface + ' -lf ' + fff)
                flag = False
                f = open(fff, 'r')
                data = f.readlines()
                f.close()
                for item in data:
                    print item,
                    m = re.match(r'option +routers +(([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3})\.[0-9]{1,3})', item.strip())
                    if m:
                        flag = True
                        net = m.group(2)
                if flag:
                    break
                time.sleep(20)

            if flag:
                # dhclient pass
                if negative:
                    print 'ATC_ERROR : dhclient PASS,Negative Test FAIL FAIL!'
                    return False
                else:
                    pass
            else:
                # dhclient fail
                if negative:
                    print 'AT_INFO : dhclient FAIL,Negative Test PASS PASS!'
                    return True
                else:
                    self.raiseError("ATC_ERROR : dhclient -v " + interface + " FAIL FAIL!")

        f = os.popen('route -n')
        data = f.readlines()
        f.close()
        s = '^ *' + net + '\.0  +0.0.0.0  +.*'
        for item in data:
            m = re.match(s, item)
            if m:
                iface = item.split()[-1]
                if iface.lower() != interface.lower():
                    cmd = 'ip addr flush dev ' + iface
                    print cmd
                    os.system(cmd)

        f = os.popen('ifconfig')
        data = f.readlines()
        f.close()
        for item in data:
            print item,

        f = os.popen('route -n')
        data = f.readlines()
        f.close()
        for item in data:
            print item,
        return True


    def Fun_Ping_Lan(self, interface='',ip='', count='100', expect='true'):
        """
        Ping one IP Address from LAN
        |Fun_Ping_Lan|
        |Fun_Ping_Lan|wlan0|
        """
        if interface:
            interface = str(interface)
        else:
            interface = ''
        if ip:
            ip = str(ip)
            print ip

        if count:
            count = int(count)
        else:
            count = 100
        if expect:
            expect = str(expect).lower()
        else:
            expect = 'true'

        cmd = 'ping ' + ip + ' -c 1'

        if interface:
            cmd += ' -I ' + interface

        if expect == 'true':
            for i in range(count):
                rc, content = ExcuteCMD(cmd)
                if rc:
                    print 'AT_INFO : ping LAN Success!'
                    return True
                time.sleep(1)
            self.raiseError("ATC_ERROR : ping LAN FAIL!")

        elif expect == 'false':
            for i in range(count):
                rc, content = ExcuteCMD(cmd)
                if rc:
                    print 'AT_INFO : ping LAN Success!'
                    self.raiseError("ATC_ERROR : Negative Test should not Ping Success!")
                time.sleep(1)
            return False


    def Restore_LAN_PC_IP_Address(self, interface=None, ip='', gw=""):
        """
        Restore LAN PC IP Address
        Examples:
        | Restore_LAN_PC_IP_Address |
        """
        if interface:
            interface = interface
            if interface == 'eth1':
                self.raiseError('ATC_ERRPR : Error interface, eth1 is wan interface!')
            elif interface == 'eth2':
                interface = 'eth2'
            else:
                self.raiseError('ATC_ERRPR : Error interface!')
        else:
            interface = 'eth2'

        m = re.match(r'^ *(([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3})\.[0-9]{1,3}) *$', ip)
        net = m.group(2)

        f = os.popen('route -n')
        data = f.readlines()
        f.close()
        s = '^ *' + net + '\.0  +0.0.0.0  +.*'

        for item in data:
            m = re.match(s, item)
            if m:
                iface = item.split()[-1]
                if iface.lower() != interface.lower():
                    cmd = 'ip addr flush dev ' + iface
                    print cmd
                    os.system(cmd)

        wificd = self.Get_Wlan_Card_Name()
        cmd = ['killall wpa_supplicant', 'killall dhclient', 'route del default',
               'ifconfig ' + interface + ' ' + ip + ' up', 'route add default gw ' + gw]
        if wificd:
            cmd.insert(3, 'ip addr flush dev ' + wificd)
        for i in cmd:
            ExcuteCMD(i)

        f = os.popen('ifconfig')
        data = f.readlines()
        f.close()
        for item in data:
            print item,

        f = os.popen('route -n')
        data = f.readlines()
        f.close()
        for item in data:
            print item


def parseCommandLine():
    usage = ""
    parser = OptionParser(usage=usage)

    parser.add_option("-s", "--ssid", dest="ssid", default="",
                            help="ssid name")
    parser.add_option("-b", "--bssid", dest="bssid", default="",
                            help="bssid name")

    parser.add_option("-t", "--type", dest="type", default="",
                            help="security type")

    parser.add_option("-k", "--key", dest="key", default="",
                            help="password key")

    parser.add_option("-e", "--expect", dest="expect", default='True',
                            help="expect")
    (options, args) = parser.parse_args()
    return options


if __name__ == '__main__':
    wifi = hlk_Wifi()
# #     opts = parseCommandLine()
    wifi.connect_AP(ssid="ALHN-9C89", bssid="f4:9e:ef:b5:39:cd", type="wpawpa2-aes", key="1545604361", expect="true")
    WLAN_Interface = wifi.Get_Wlan_Card_Name()
    wifi.Assign_IP_Address_For_WLAN(interface=WLAN_Interface, ip="192.168.1.64")
    wifi.Fun_Ping_Lan(interface=WLAN_Interface, ip='192.168.1.254', count='10')
    wifi.Restore_LAN_PC_IP_Address(interface='eth2', ip='192.168.1.119', gw='10.18.90.254')




