"""
  convert setup info in ltb file and .csv file to global table

  a adaption layer to finish the data format translation.\
  so that the other functions can free from detail dealing with setup data and format.
  check global table was provided too.
"""

import re, sys, os, inspect
#from robot.api import logger
         
def create_global_tables(CsvFile="None"): 
    """
     to convert setup data in relative .csv file to global dictionaries or table,
     update the the global table based on input ltb data
    """

    if CsvFile == "None":
        CsvFile = os.environ["SETUPFILENAME"]

    if not os.path.isfile(CsvFile) :
        raise AssertionError("Module:%s, Keyword:%s -> %s is not a file!" \
        % (__name__,inspect.stack()[0][3],str(CsvFile)))
    else :       
        print "Data file is : %s" % CsvFile

    CsvData = _strip_csv_data(CsvFile)
    lCsvTable = []
    TableStart = 'No'

    for line in CsvData:
        if TableStart == 'No':
            CheckTableStart = re.search('TABLE (.*) Start', line)
            if CheckTableStart:
                TableStart = 'Yes'
                TableName = CheckTableStart.group(1)
                TableRow = []
        else:
            CheckTableEnd = re.search('TABLE (.*) End', line)
            if CheckTableEnd:
                TableStart = 'No'
                TableDict = _list_to_dict_convert(TableRow)
                TableNameDict = TableName
                globals()[TableNameDict] = TableDict
                print "%s: %s" % (TableName,globals()[TableNameDict])
            else:
                TableRow.append(line)

    return
    
def print_global_table(table_name):
    print "Table Name: ", table_name
    print globals()[table_name]
    return

def get_global_table(table_name):
    """
        get the content in the input table
    """
    try :
        print "table %s: %s" % (table_name,str(globals()[table_name]))
        return globals()[table_name]
    except KeyError :
        print "No table named %s found" % table_name
        return {}
      

def set_global_table(table_name, data):
    globals()[table_name] = data


def update_global_table(table_name,data,line_num=None):
    """
        update or add columns for table according to table_content
        
        if no table_name found, it will be added   
        instance: update_global_table(table_name="ONTData",data={"AidCli":"1/1/8"})       
    """
    try :
        type(globals()[table_name])
    except NameError :
        print "No table named %s found, it will be added" % table_name
        globals()[table_name].append(data)
        return
        
    if line_num or line_num == 0:
        globals()[table_name][int(line_num)].update(data)
    else :
        for line in globals()[table_name]:
            line.update(data)
            
            
    return

def get_table_line(table_name,search_condition=None,matched_line_index=None):
    """
        get specific line with search condition and No. of matched lines from table
        
        search_condition is a string with format "key1=value1,key2=value2" 
        if there are no matched_line_index, return the first line of all matched,
        or return the required line
    """

    try :
        totalTableList = globals()[table_name]
    except Exception as inst:
        print "can not get table name %s, exception: %s" % (table_name,inst)
        return {}
    
    if not matched_line_index :
        matched_line_index = 0
    else :
        matched_line_index = int(matched_line_index)

    if not search_condition :
        return totalTableList[matched_line_index]
     
    matched_lines = []
                
    conditionList = search_condition.split(',')
    for lineDict in totalTableList :

        for condition in conditionList :
            try:
                key,value = condition.split('=',1)
            except IndexError :
                raise AssertionError("Module:%s, Keyword:%s -> \
                seach_condition format error for '%s'" \
                % (__name__,inspect.stack()[0][3],condition))
            if not (lineDict.has_key(key) and re.search(value,lineDict[key])):
                lineMatch = False
                break  # break the inner condition loop          
        else:
            matched_lines.append(lineDict)
            lineMatch = True
            
    if len(matched_lines) > matched_line_index :
        return matched_lines[matched_line_index]
    else :
        msg = "no any line matched the search condition '"\
        +search_condition+"' in table "+table_name
        print msg
        return {}
  

def get_table_lines(table_name,search_condition=None):
    """
        get all matched table lines with search condition and No. of matched lines from table
        
        search_condition is a string with format "key1=value1,key2=value2" 
    """

    try :
        totalTableList = globals()[table_name]
    except Exception as inst:
        print "can not get table name %s, exception: %s" % (table_name,inst)
        return {}

    if not search_condition :
        return totalTableList
     
    matched_lines = []
                
    conditionList = search_condition.split(',')
    for lineDict in totalTableList :

        for condition in conditionList :
            try:
                key,value = condition.split('=',1)
            except IndexError :
                raise AssertionError("Module:%s, Keyword:%s -> \
                seach_condition format error for '%s'" \
                % (__name__,inspect.stack()[0][3],condition))
            if not (lineDict.has_key(key) and re.search(value,lineDict[key])):
                lineMatch = False
                break  # break the inner condition loop          
        else:
            matched_lines.append(lineDict)
            lineMatch = True
            
    return matched_lines
  
def _strip_csv_data(csvfile):
    with open(csvfile, 'rb') as f:
        lContent = f.readlines()
        ProcessedCsvData = []
        for line in lContent:
            line = re.sub('([,]+([\\r]|[\\n])+|([\\r]|[\\n])+)','',line)
            if len(line) > 0 :
                ProcessedCsvData.append(line)
    return ProcessedCsvData

   
def _list_to_dict_convert(ListTable):
    lstdata = ListTable
    outputarr = []

    headerstr = lstdata[0]
    headerlst = headerstr.split(',')           
    headerlstlen = len(headerlst)
    lenlstdata = len(lstdata)
    i = 1
    while i < lenlstdata:
        tmp = re.sub(r'".*?,.*?"',lambda m:m.group(0).replace(",","@@$$@@"),lstdata[i]).split(',')
        tmparr = "@@##@@".join(tmp).replace("@@$$@@",",").split("@@##@@")
        #tmparr = lstdata[i].split(',')
        if headerlstlen != len(tmparr) :
            print "data gotten can not map columns:"+str(tmparr)
            i += 1
            continue      
        tmpdict = {}
        j = 0
        while j < headerlstlen:
            jkey = headerlst[j]
            jval = tmparr[j]
            tmpdict[jkey] = jval
            j += 1 
        outputarr.append(tmpdict)
        i += 1
    return outputarr


