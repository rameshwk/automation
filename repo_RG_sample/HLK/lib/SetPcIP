#!/bin/bash
# Author: Alfred ZHANG  2017-4-27 
# ---- Functions ----
function echo_error(){
    if [ -n $2 ];then
	echo "Error:  Line-$2 :--$1" 1>&2
    else
	echo "Error:  Line-${BASH_LINENO[$i]} :--$1" 1>&2
    fi

    echo ""
    echo $usage
    echo ""
}

function check_rc() {
    line=${BASH_LINENO[$i]}
    if [ $2 != 0 ]; then
	if [[ $3 =~ [0-9]+ ]];then
	    line=$3
	fi
	echo_error "$1 retrun $2" $line
	exit $2
    fi

    echo "Successful run --- $1"
}

function exec_cmd() {
    line=${BASH_LINENO[$i]}
    if [ -z "$1" ];then
	exit 128
    fi
    awk -v max=$COLUMNS 'BEGIN {while (z++<max) printf "=";print}' -f
    echo "| CMD--->: $1"
    awk -v max=$COLUMNS 'BEGIN {while (z++<max) printf "="; print}' -f
    out=`eval $1`
    # Check expect result, if the second argument is used, then it will be treat as RE .
    if [ -n "$2" ];then
	local pt=$2
	printf  "......%s ..... %s\n" "$pt" "$out"
	if [[ $out =~ $pt ]];
	then
	    echo "Result match the pattern... $pt"
	else
	    echo_error "The output do not match expect pattern $pt ... "  $line
	    exit 3

	fi
    fi

    echo  $out

    check_rc "$1" $?
}

#---- MAIN -----

echo "Init Starting ..... "
orig=/etc/sysconfig/network-scripts/ifcfg-eth2 
inpattern=([0-9]{1,3}.){3}[0-9]{1,3}
usage="Usage: SetLanIP 19.1.1.1 eth2 . The firest arg is ipv4 address, the second arg is the target NIC (Network Interface Card)  "

IP_OR_BAK=NULL
NIC=eth2
FLAG_BAKUP=0

if [ $BASH_ARGC != "2" ]
then
    echo $usage
    exit 128
fi

if [ -z $2 ]
then
    echo ""
    echo_error "The target NIC can not be empty ... "
    exit 128
else 
    NIC=$2
    exec_cmd "ifconfig $NIC"

fi

orig=/etc/sysconfig/network-scripts/ifcfg-$NIC
bak=$orig.bak


if [ -z $1 ]
then
    echo ""
    echo "Line-$LINENO :-- Error: The target NIC can not be empty ... " 1>&2
    echo $usage
    exit 128
else
    IP_OR_BAK=$1
fi

 
if [[ $IP_OR_BAK =~ $inpattern ]]; then 
    echo ""
    echo "Set Lan IP to be $1 ... "
    buf=$(ifconfig $NIC)
    echo $IP_OR_BAK
    if [[ $buf =~ $IP_OR_BAK ]];then
        echo "Current IP is already the target ip. So exit with code 0."
	exit 0
    fi
else
    echo "$IP_OR_BAK it is an invalid IPv4 address. But, maybe it's an backup file, let's find out... " 
    if [ -f $IP_OR_BAK ];then
	echo "Ooooh, yes!"
	bak= $IP_OR_BAK
	orig= `echo $bak | awk 'BEGIN {FS=".bak"} {print $1}'`
	echo "Line-$LINENO :-- orig is $orig ..."
	echo "bak is $bak"        
	FLAG_BAKUP=1
    else 
	echo "Line-$LINENO :-- Error: $1 is not exit, so it cannot be an backup file for ifcfg. That means you cannot restore it !" 1>&2
	echo $usage
	exit 128
    fi
fi

echo "Init finished ... "
echo ""
echo "Step 1. --- Create a backup file for $orig, named $bak"

if [ $FLAG_BAKUP != 1 ]; then
    exec_cmd "cp -f $orig  $bak"
    echo ""
    echo "Step 2. --- Create new configure file for ETH2 "
    exec_cmd "awk -v ip=\"$IP_OR_BAK\"  'BEGIN {FS=\"=\"} \$1 ~ /IPADDR.*/ {\$2=ip} {print \$1 \"=\" \$2}' $bak 1> $orig"
else
    tmp=/tmp/nic_bak
    exec_cmd "cp -f $orig $tmp"
    exec_cmd "cp -f $bak $orig"
    exec_cmd "cp -f $tmp $bak"
fi 

echo $out
echo ""
echo "Step 3. --- Restart $NIC to active the new configuration"
exec_cmd "ifdown $NIC"
exec_cmd "ifup  $NIC"
sleep 3s
exec_cmd "ifconfig $NIC" ".*UP BROADCAST RUNNING.*"

echo ""
echo "Check by echo ...."
exec_cmd "ls -l /etc/sysconfig/network-scripts/ifcfg-$NIC*"

echo ""
echo "Echo the New Configuration Context ----- "
exec_cmd "cat /etc/sysconfig/network-scripts/ifcfg-$NIC"
