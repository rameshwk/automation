import os, sys
try:	
    lib_path = os.environ['ROBOTREPO'] +'/LIBS/COM_PCTA'
    if lib_path not in sys.path:
        sys.path.append(lib_path)
except Exception as inst:
    raise AssertionError("%s -> Fail to set sys.path, %s" % (__name__,inst))


import re, time, sys
import pcta_command
import data_translation
from robot.api import logger
from hlk_data_conversion import list_to_dict
from copy import deepcopy

network_side_traffic = []
user_side_traffic = []

def pcta_connect (*args):
    """
        connect to pcta by build up pcta session based on data of TrafficGenData  
    """
    keyword_name = "pcta_connect"
    tablename = "TrafficGenData"
    tabledata = data_translation.get_global_table(tablename)
    for entry in tabledata:
        if entry['Type'].encode("ascii") == 'PCTA':
            break
 
    logger.debug("%s:%s -> pcta info:%s " \
    % (__name__,keyword_name,str(entry)))
    
    ipAdress = entry['IPAddress']
    try:
        username = entry['Username']
    except Exception:
        username = entry['UserName']
    password = entry['Password']
    execFile = entry['ExePath']
    
    try :
        clientSlot = entry['ClientSlot']
    except Exception:
        clientSlot = "1"
    else :
        if clientSlot == "x" :
            clientSlot = "1"
            
    try :    
        networkSlot = entry['NetworkSlot']
    except Exception:  
        networkSlot =  "2"
    else :
        if networkSlot == "x" :
            networkSlot = "2"  
                      
    try:
        pcta_command.open_pcta_session\
        (ipAdress, username, password, execFile, \
        networkSlot=networkSlot,clientSlot=clientSlot)
    except Exception as inst:
        raise AssertionError("%s:%s -> \
        can not build up pcta session, exception:%s" \
        % (__name__,keyword_name,inst)) 
    else:    
        logger.info("connect pcta success ")     

def pcta_disconnect () :
    """
       disconnect pcta by close pcta session
    """
    keyword_name = "pcta_disconnect"
    try:
        pcta_command.close_pcta_session()
    except:
        raise AssertionError("%s:%s -> fail to close pcta connection" \
        % (__name__,keyword_name))         
 
    else :
        logger.info("disconnect pcta connection success " )


def pcta_send_traffic(*args) :
    """
    send packets on pcta
    """
    keyword_name = "pcta_send_traffic"      
    aArgs = list_to_dict(args)
    logger.debug("%s:%s -> %s" % (__name__,keyword_name,aArgs))
 
    # Get the send type command for the mentioned protocol
    if not aArgs.has_key('ta_protocol') or aArgs['ta_protocol']== "x" :
        raise AssertionError("%s:%s-> fail to get 'ta_protocol' in '%s'" \
        % (__name__,keyword_name,str(args)))
    
    if aArgs.has_key('send_type') and aArgs['send_type']!='x' :
        send_type = aArgs['send_type']   
    else :
        send_type = 'send'
    try :    
        command_name = pcta_command.get_pcta_command(aArgs['ta_protocol'],send_type)
    except Exception as inst :
        raise AssertionError("%s:%s-> fail to get pcta command,exception: %s" \
        % (__name__,keyword_name,inst))  
            
    # Set the lif side for the user/network in both capt & send sides
    if aArgs.has_key('lif') and aArgs['lif'] != "x" :
        sendSideOpt = "lif=" + aArgs['lif']  
    elif aArgs.has_key('side') :      
        pcta_info =  pcta_command.get_pcta_info()      
        if 'user' in aArgs['side'] :
            sendSideOpt = "lif="+pcta_info['client_slot']
        elif 'network' in aArgs['side'] :
            sendSideOpt = "lif="+pcta_info['network_slot']
    else :
        raise AssertionError("%s:%s-> fail to get 'side' in '%s'" \
        % (__name__,keyword_name,str(args))) 
  
    # PCTA command to send traffic
    options = []
    options.append(sendSideOpt)
    
    for arg in args :
        arg = arg.encode("ascii")
        if 'ta_protocol=' in arg or 'side=' in arg or 'send_type=' in arg :
            continue
        else :
            m = re.search('((c|s)?_?vlan[1-4]?)_id',arg)
            if m :
                if '65535' not in arg :
                    options.append(arg)
                    options.append(m.group(1)+'=true')           
                else :
                    continue
            else :
                options.append(arg)

    try :
        command_options = pcta_command.pcta_command_options(*options)
        full_command = command_name +" " + command_options + "\n"  
    except Exception as inst :
        raise AssertionError("%s:%s-> fail to create pcta command" \
        % (__name__,keyword_name)) 
    
    (status,return_info) = pcta_command.send_pcta_command(full_command)       
    if "pass" != status.lower() :
        raise AssertionError("%s:%s-> fail to send pcta command '%s'" \
        % (__name__,keyword_name,full_command))
    
    return "pass"

def pcta_receive_traffic(*args) :
    """
    stop the capture process of pcta and validate the captured packets
    """
    keyword_name = "pcta_receive_traffic"
    aArgs = list_to_dict(args)
    
    # Set the lif side for the user/network in both capt side
    if aArgs.has_key('lif') and aArgs['lif'] != "x" :
        logger.debug("%s:%s -> lif exists in input" % (__name__,keyword_name))
        captSideOpt = "lif=" + aArgs['lif']
    elif aArgs.has_key ('side') and aArgs['side'] in ["user","network"] :
        try :
            pcta_info = pcta_command.get_pcta_info()   
        except Exception as Inst :
            raise AssertionError("%s:%s -> fail to get pcta info " \
            % (__name__,keyword_name)) 

        if 'user' in aArgs['side'] :
            captSideOpt = "lif="+pcta_info['client_slot']
            aArgs['lif'] = pcta_info['client_slot']
        elif 'network' in aArgs['side'] :
            captSideOpt = "lif="+pcta_info['network_slot'] 
            aArgs['lif'] = pcta_info['network_slot'] 
        else :
            raise AssertionError("%s:%s -> unexpected 'side' option:'%s' " \
            % (__name__,keyword_name,str(aArgs['side'])))            
    else :
          raise AssertionError("%s:%s ->'side' option not exists in '%s' " \
          % (__name__,keyword_name,str(args)))   

    if aArgs.has_key('dist_id') and int(aArgs['dist_id']) <= 8 :  
        distOpt = "dist_id=" + str(aArgs['dist_id'])
    else :
        raise AssertionError("%s:%s-> get unexpected dist_id from input '%s'" \
        % (__name__,keyword_name,str(args))) 
    
    # Get the capture & send type commands for the mentioned protocol
    try:
        captcommand = pcta_command.get_pcta_command \
        (aArgs['ta_protocol'],'stop_capture')
    except Exception :
        raise AssertionError("%s:%s-> can not get capture stop command for '%s'" \
        % (__name__,keyword_name,aArgs['ta_protocol']))
  
    if aArgs.has_key('exp_res'):
        exp_res = aArgs['exp_res']
    else :
        exp_res = "pass"

    #Input Dict to compare received packet values
    inputDict = deepcopy(aArgs)
    if aArgs.has_key('side'):
        del inputDict['side']
    del inputDict['lif']
    del inputDict['ta_protocol']
    if aArgs.has_key('exp_res'):
        del inputDict['exp_res']
    if aArgs.has_key('dist_id'):
        del inputDict['dist_id']
    if aArgs.has_key('capt_reserve'):
        del inputDict['capt_reserve']
    
    logger.debug ("InputDict to compare : %s" % inputDict)

    # Send Stop capture command
    captOptList = []  
    captOptList.append(captSideOpt)
    captOptList.append(distOpt)

    command_options = pcta_command.pcta_command_options(*captOptList)
    full_command = captcommand +" " + command_options + "\r" 
  
    # TimeDelay before stop capture
    time.sleep(1)

    try:   
        (status,return_info) = pcta_command.send_pcta_command(full_command)
    except Exception as inst:
        raise AssertionError("%s:%s -> fail to stop capture" \
        % (__name__,keyword_name,inst))
 
    if '_received_l' not in return_info :
        raise AssertionError("%s:%s -> pcta return abnormal value: %s" \
        % (__name__,keyword_name,return_info))
        
    # Validation part
    logger.info("Conditions To Filter Received Packets:\n %s" % inputDict)
    if '_received_l {}' in return_info :
        msg = "No packets found, and expect result is " + exp_res.lower()
        if exp_res.lower() == "fail" :
            logger.info (msg)
            return "pass"
        else :
            try :
                logger.error (msg)
            except Exception as inst:
                logger.info ("<i>"+msg+'</i>',html=True)
            return "fail"
    
    packets = _packet_convertor (return_info)
    if aArgs.has_key('capt_reserve') and "true" == aArgs['capt_reserve'] :
        global user_side_traffic, network_side_traffic
        if 'user' in aArgs['side'] :
            user_side_traffic = packets
        elif 'network' in aArgs['side'] :
            network_side_traffic = packets
    
    validateret = _validate_packets(inputDict,packets)
    if "pass" == validateret :
        return exp_res.lower()
    else :
        if exp_res.lower() == "fail" :
            return "pass"
        else :
            return "fail"             

def pcta_capture_traffic(*args) :
    """
    To capture the packet captured in the interface
    on client or network and to process the packet 
    
    Input : Pcta options :
    
    |   options                 |   Explantion                              |
    |   lif                     |   lif=1 or lif=2                          |
    |   side                    |   user  or network                        |
    |   dist_id                 |   capture particular dist_id              |
    |   ta_protocol             |   protocol value as (IP,ARP..etc )        |
    |   exp_res                 |   expected result is pass or fail         |
    
    output : captured packets( in List of dicts)
    
    Author :
    Beno K Immanuel   Added pcta_capture_traffic to get the captured packets
    
    """
    packets =[]
    keyword_name = "pcta_capture_traffic"
    aArgs = list_to_dict(args)

    logger.debug("%s:%s->Input dict: %s" 
    % (__name__,keyword_name,aArgs))
    
    # Set the lif side for the user/network in both capt side
    if aArgs.has_key('lif') and aArgs['lif'] != "x" :
        logger.debug("%s:%s -> lif exists in input" % (__name__,keyword_name))
        captSideOpt = "lif=" + aArgs['lif']
    elif aArgs.has_key ('side') and aArgs['side'] in ["user","network"] :
        pcta_info = pcta_command.get_pcta_info()      
        if 'user' in aArgs['side'] :
            captSideOpt = "lif="+pcta_info['client_slot']
            aArgs['lif'] = pcta_info['client_slot']
        elif 'network' in aArgs['side'] :
            captSideOpt = "lif="+pcta_info['network_slot'] 
            aArgs['lif'] = pcta_info['network_slot'] 
        else :
            raise AssertionError("%s:%s -> unexpected 'side' option:'%s' " \
            % (__name__,keyword_name,str(aArgs['side'])))            
    else :
          raise AssertionError("%s:%s ->'side' option not exists in '%s' " \
          % (__name__,keyword_name,str(args)))

    if aArgs.has_key('dist_id') and int(aArgs['dist_id']) <= 8 :  
        distOpt = "dist_id=" + str(aArgs['dist_id'])
    else :
        raise AssertionError("%s:%s -> can not get correct dist_id from input '%s'" \
        % (__name__,keyword_name,str(args)))    
    # Get the capture & send type commands for the mentioned protocol
    try:
        captcommand = pcta_command.get_pcta_command(aArgs['ta_protocol'],'stop_capture')
    except Exception :
        raise AssertionError("%s:%s -> can not get capture stop command for %s" \
        % (__name__,keyword_name,aArgs['ta_protocol']))
  
    if aArgs.has_key('exp_res'):
        exp_res = aArgs['exp_res']
    else :
        exp_res = "pass"

    #Input Dict to compare received packet values
    inputDict = aArgs
    if aArgs.has_key('side'):
        del inputDict['side']
    del inputDict['lif']
    del inputDict['ta_protocol']
    if aArgs.has_key('exp_res'):
        del inputDict['exp_res']
    if aArgs.has_key('dist_id'):
        del inputDict['dist_id']
    logger.debug ("InputDict to compare : %s" % inputDict)

    # Send Stop capture command
    captOptList = []  
    captOptList.append(captSideOpt)
    captOptList.append(distOpt)

    command_options = pcta_command.pcta_command_options(*captOptList)
    full_command = captcommand +" " + command_options + "\r" 
  
    # TimeDelay before stop capture
    time.sleep(1.5)

    try:   
        (status,return_info) = pcta_command.send_pcta_command(full_command)
    except Exception as inst:
        raise AssertionError("%s:%s -> fail to stop capture" \
        % (__name__,keyword_name,inst))
 
    if '_received_l' not in return_info :
        raise AssertionError("pcta return abnormal value: %s" % return_info)
        
    # Validation part
    if '_received_l {}' in return_info :
        msg = "No packets found, and expect result is " + exp_res.lower()
        if exp_res.lower() == "fail" :
            logger.info (msg)
            return []
        else :
            raise AssertionError(msg)
    else :

        try:
            packets = _packet_convertor(return_info)
        except Exception as inst:
            raise AssertionError("%s:%s -> fail to convert packets" \
            % (__name__,keyword_name,inst))
        
        
        #add for filter matched packets
        if inputDict != {}:
            try:
                packets=_filter_packets(inputDict,packets)
            except Exception as inst:
                raise AssertionError("%s:%s -> fail to filter packets" \
                % (__name__,keyword_name,inst))
        
        if packets == [] :
            msg = "No packets Matched, and expect result is " + exp_res.lower()
            if exp_res.lower() == "fail":           
                logger.info (msg)
            else :
                raise AssertionError(msg)
        else :
            msg = "Packets Matched, and expect result is " + exp_res.lower()
            if exp_res.lower() == "pass":           
                logger.info (msg)
            else :
                raise AssertionError(msg)
            
    return packets

  
def pcta_check_traffic(*args) :
    """
    single direction traffic check, the traffic can be expected as pass or fail
  
    example:
    pcta_check_traffic \
    capt_options='ta_protocol=ip, side=user, mac_dest=000000000011, \
      packet_count=5' \
    send_options='ta_protocol=IP, lif=2, \
      vlan1_id=100, vlan1_user_prior=2,  \
      mac_dest=000000000022, mac_src=000000000011, \
      ip_dest=2.2.2.2, ip_src=1.1.1.1, \
      packet_count=5' \
    delay_time='3' \
    exp_result='pass'

    """
    keyword_name = "pcta_check_traffic"
    logger.debug("%s:%s ->  %s" \
    % (__name__,keyword_name, str(args)))
 
    aArgs = list_to_dict(args)

    if aArgs.has_key('exp_result') and aArgs['exp_result']!= "x" :
        pass
    else:
        aArgs['exp_result']="pass"   
    result = "fail"
    
    # 1 start capture packets
    if aArgs.has_key('capt_options') and aArgs['capt_options']!= "x" :
        list_capt_options = []
        for item in aArgs['capt_options'].lstrip('[').strip(']').split(',') :
            try :
                m = re.search("\s*u'(.*)'", item)
                item = m.group(1)
            except Exception as inst :
                pass
            finally :    
                list_capt_options.append(item)

        try :
            tuple_capt_options = tuple(list_capt_options)
            dist_id = pcta_start_capture(*tuple_capt_options)
        except Exception as inst :
            raise PctaContinueError("%s:%s -> \
            fail to start capture, exception: %s" \
            % (__name__,keyword_name,inst))

    # 2 send packets
    if aArgs.has_key('send_options') and aArgs['send_options']!= "x" :
        list_send_options = []
        for item in aArgs['send_options'].lstrip('[').strip(']').split(',') :
            try :
                m = re.search("\s*u'(.*)'", item)
                item = m.group(1)
            except Exception as inst :
                pass
            finally :    
                list_send_options.append(item)
        try:
            tuple_send_options = tuple(list_send_options)
            result = pcta_send_traffic(*tuple_send_options)
        except Exception as inst:
            raise PctaContinueError("%s:%s -> \
            fail to send traffic, exception: %s" \
            % (__name__,keyword_name,inst))

    if aArgs.has_key('delay_time') :
        time.sleep(int(aArgs['delay_time'].strip('s')))
  
    # 3 stop capture and check packets
    if aArgs.has_key('capt_options') and aArgs['capt_options']!= "x" : 
        # let stop capture process know the distance id of start capture and expect result
        list_capt_options.append('dist_id='+str(dist_id))
        list_capt_options.append('exp_res='+aArgs['exp_result'])
        
        if aArgs.has_key('capt_reserve') and 'true' == aArgs['capt_reserve'] :
            list_capt_options.append('capt_reserve=true')
        
        tuple_capt_options = tuple(list_capt_options)    
        try: 
            result = pcta_receive_traffic(*tuple_capt_options)
        except Exception as inst:
            raise PctaContinueError("%s:%s -> exception in pcta_receive_traffic: %s" \
            % (__name__,keyword_name,inst))  
   
    if result == "fail":
        raise PctaContinueError("%s:%s -> not gotten expect traffic" \
        % (__name__,keyword_name)) 
               
    return result

def pcta_start_capture (*args) :
    """
    starting the capture process of pcta
    """
    keyword_name = "pcta_start_capture"

    aArgs = list_to_dict(args)
    logger.debug("%s:%s -> %s" % (__name__,keyword_name,aArgs))
 
    # Get the capture & send type commands for the mentioned protocol
    if aArgs.has_key('ta_protocol') and aArgs['ta_protocol']!= "x" :
        captcommand = pcta_command.get_pcta_command(aArgs['ta_protocol'],'start_capture')
    else :
        raise AssertionError("%s:%s -> ta_protocol value not exists in '%s'" \
        % (__name__,keyword_name,str(args)))
        
    # Set the lif side for the user/network in both capt side
    if aArgs.has_key('lif') and aArgs['lif'] != "x" :
        logger.debug("%s:%s -> lif exists in input" % (__name__,keyword_name))
        captSideOpt = "lif=" + aArgs['lif']
    elif aArgs.has_key ('side') and aArgs['side'] in ["user","network"] :
        pcta_info = pcta_command.get_pcta_info()      
        if 'user' in aArgs['side'] :
            captSideOpt = "lif="+pcta_info['client_slot']
            aArgs['lif'] = pcta_info['client_slot']
        elif 'network' in aArgs['side'] :
            captSideOpt = "lif="+pcta_info['network_slot'] 
            aArgs['lif'] = pcta_info['network_slot'] 
        else :
            raise AssertionError("%s:%s -> unexpected 'side' option:'%s' " \
            % (__name__,keyword_name,str(aArgs['side'])))            
    else :
          raise AssertionError("%s:%s ->'side' option not exists in '%s' " \
          % (__name__,keyword_name,str(args)))
    options = []
    options.append(captSideOpt)

    # Send Start capture command
    try :
        command_options = pcta_command.pcta_command_options(*options)
        full_command = captcommand +" " + command_options + "\n"
        (status,return_info) = pcta_command.send_pcta_command(full_command)
    except Exception as inst :
         raise AssertionError("%s:%s-> fail to create/send command, exception: %s" \
         % (__name__,keyword_name,inst)) 
                
    if "pass" == status.lower() :
        chkdist = re.search('-dist_id\s*(\d*)', return_info)
        dist_id = chkdist.group(1)  
        if not (1 <= int(dist_id) <= 8 ) :
            raise AssertionError("%s:%s-> return abnormal dist_id '%s'!" \
            % (__name__,keyword_name,str(dist_id)))
        logger.debug ("capture started with dist_id = %s" % str(dist_id))
        return dist_id
    else :
         raise AssertionError("%s:%s -> failed command: %s" \
         % (__name__,keyword_name,full_command))


def _packet_convertor (input='') :
    """
    convert packet content from string to dictionary list
    """
    packetschk = re.search('{(.*)}' , input)
    matchedcont = packetschk.group(1)

    matchedcont = matchedcont.lstrip('{')
    matchedcont = matchedcont.rstrip('}')
    matchedcont = matchedcont.strip(' ')

    packetlst = matchedcont.split('} {')

    packetscount = len(packetlst)

    packets = []
  
    for pkt in packetlst :
        pkt=" "+pkt
        
        #add for dealing with negative value. replace "-" with "####".
        pat="-\d+"
        pat1='[a-zA-Z]'
        flag_ne=0
        tmp1=re.search(pat,pkt)
        if tmp1 != None :
            list_1=pkt.split(" ")
            list_1_new=[]
            for i,item in enumerate(list_1) :
                if re.match(pat,item) and not re.search(pat1,item):
                    flag_ne=1 
                    list_1[i]=re.sub("-","####",item)
                    #print item
            pkt=" ".join(list_1)
        #add for dealing with negative value.replace "-" with "####".
        
        splitlst = pkt.split(' -')
        del splitlst[0]
        optdict = {}
        for optpair in splitlst :
            optpair = optpair.strip(' ');
            
            #add for dealing with negative value. recover "-".
            if flag_ne == 1 and "####" in optpair :
                optpair=optpair.replace("####","-")
            else :
                pass
            #add for dealing with negative value. recover "-".
            
            if "\"" in optpair :
                optpair = optpair.replace("\s*"," ")
                packetsplit = optpair.split(' "')
                key = packetsplit[0]
                value = packetsplit[1]
                optdict[key] = value
            else :
                mobj = re.match(r'(\w+)\ (.*)',optpair)
                key = mobj.group(1)
                value = mobj.group(2)
                if "{" in value and "}" in value :
                    value = re.sub('{','',value)
                    value = re.sub('}','',value)
                    value = value.strip()
                    if key != 'message' :
                        value = value.split(' ')
                optdict[key] = value
	
        packets.append(optdict)

    return packets


def _validate_packets (compdict={},packets={}) :
    """
    comparing packets content with condition in comdict
    """
    keyword_name = "_validate_packets"

    recv_packet_len = len(packets)
    msg = str(recv_packet_len)+ " Packet(s) Received"
    logger.info(msg)
    matched_packets = 0

    if compdict.has_key('packet_count'):
        dontcare_scenario = 0
        expect_packets = int(compdict['packet_count'])
        del compdict['packet_count']
    else :
        expect_packets = 1
        dontcare_scenario = 1

    packet_no = 1
    for packet in packets :
        if compdict.has_key('opt82_agent_remote_id_hex') :
            if packet.has_key('dhcp_options') :
                if packet['dhcp_options'].find(compdict['opt82_agent_remote_id_hex']) >= 0:
                    packet['opt82_agent_remote_id_hex'] = \
                    compdict['opt82_agent_remote_id_hex']
                else:
                    logger.debug("%s:%s -> no opt82_agent_remote_id_hex in captured packet: %s" 
                    % (__name__,keyword_name,str(packet_no)))
            else:
                logger.debug("%s:%s -> no dhcp_options in captured packet: %s" 
                % (__name__,keyword_name,str(packet_no)))
   
        flag = 1
        for key in compdict.keys() : 
            if packet.has_key(key) :
                if key == 'cfm_flags' :
                    packet[key] = str(int(packet[key],16))
                    compdict[key] = str(int(compdict[key]))

                if type(packet[key]) is list :
                    if compdict[key] in packet[key] :
                        continue
                    else :
                        flag = 0
                        msg = str(packet[key])+" not matched with "+compdict[key]\
                        +" for option: "+key + " in Packet No." + str(packet_no)
                        try :
                            logger.error (msg)
                        except Exception as inst:
                            logger.info ("<i>"+msg+'</i>',html=True)

                elif packet[key] == compdict[key] :
                    continue
                else :
                    flag = 0
                    msg = str(packet[key])+" not matched with "+compdict[key]\
                    +" for option: "+key + " in Packet No." + str(packet_no)
                    try :
                        logger.error (msg)
                    except Exception as inst:
                        logger.info ("<i>"+msg+'</i>',html=True)
                      
            else :
                flag = 0
                msg = "No value for option "+key+" in Packet No. "+ str(packet_no)
                try :
                    logger.error (msg)
                except Exception as inst:
                    logger.info ("<i>"+msg+'</i>',html=True)                 
                break

        if flag == 1 :
            matched_packets = matched_packets + 1
            msg ="Packet No." + str(packet_no) + " Matched"
            logger.info (msg)
        else :
            msg ="Packet No." + str(packet_no) + " Not Matched"
            logger.info (msg)

        packet_no = packet_no + 1
        
    msg = "Expect Packet(s) Count: %s, Matched Packet(s) count : %s" \
    % (str(expect_packets),str(matched_packets))
    logger.info (msg)
    if dontcare_scenario :
        if matched_packets >= expect_packets :
            return "pass"
        else :
            return "fail" 
    else :
        if matched_packets == expect_packets :
            return "pass"
        elif matched_packets > expect_packets :
            logger.info("Matched Packet(s) %s is more than Expected %s Packet(s)" \
            % (str(matched_packets),str(expect_packets)))
            return "fail"       
        else :
            return "fail" 

def get_traffic_capture(*args):
    
    """
    return specified field's value from captured packets on user side or network side
    
    Example 
    | ${packet1}= | get_traffic_capture    | side="user"            | ip_src=192.168.1.254 | get_field=mac_dest,vlan1_id | #will get captured packets on user    side |
    | Comment     | ${packet1['mac_dest']} | ${packet1['vlan1_id']} | | | |
    | ${packet2}= | get_traffic_capture    | side="network"         | ip_src=10.18.81.186  | get_field=mac_dest,vlan1_id | #will get captured packets on network side |
    | Comment     | ${packet2['mac_dest']} | ${packet2['vlan1_id']} | | | |
    
    """
    global user_side_traffic, network_side_traffic
    keyword_name = "get_traffic_capture"
    logger.debug("%s:%s ->  %s" % (__name__,keyword_name, str(args)))
 
    aArgs = list_to_dict(args)
    result = {}
    packets = []
    if aArgs.has_key('side') and aArgs['side'].lower() in ["user", "network"] :
        if 'user' == aArgs['side'] : 
            packets = user_side_traffic
        elif 'network' == aArgs['side'] : 
            packets = network_side_traffic
    else :
        raise AssertionError("we should specify argu: side, and should be 'side=user' or 'side=network'")
    
    try:
        fields = aArgs['get_field'].split(",")
    except Exception as inst:
        fields = []
    
    in_option = deepcopy(aArgs)
    try:
        del in_option['side']
        del in_option['get_field']
    except:
        pass
    if not in_option: 
        return result
    
    for packet in packets :
        #each captured packet
        bMatch = True
        logger.info("in_option.keys:%s" % in_option.keys())
        for option in in_option.keys() :
            #each input option
            temp = packet.has_key(option) and packet[option] or "nonExist"
            if temp != in_option[option] :
                bMatch = False
        if bMatch :
            if not fields:
                result = deepcopy(packet)
            for field in fields :
                #each field which should be gotten
                result[field] = packet.has_key(field) and packet[field] or ""
            break
    
    return result

def pcta_check_traffic_bidirect(*args) :
    """
    bi direction traffic check, the traffic can be expected as pass or fail

    Example 
    | pcta_check_traffic_bidirect | us_send_options="side=user,ta_protocol=CFM" | us_capt_options='ta_protocol=ip,side=network' | ds_capt_options="side=user" |
    |          ...           |         exp_result="us_pass_ds_pass"        |
    
    """
    keyword_name = "pcta_check_traffic_bidirect"
    logger.debug("%s:%s ->  %s" \
    % (__name__,keyword_name, str(args)))
 
    aArgs = list_to_dict(args)

    if aArgs.has_key('exp_result') and aArgs['exp_result']!= "x" :
        pass
    else:
        aArgs['exp_result']="us_pass"
    exp_result_us="pass"
    exp_result_ds="pass"
    exp_result_old="pass"
    if "us" in aArgs['exp_result'] or "ds" in aArgs['exp_result'] :
        exp_tmp_l=aArgs['exp_result'].split("_")
        for i,exp in enumerate(exp_tmp_l) :
            tmp_len=len(exp_tmp_l)
            if exp == "us" and i != tmp_len :
                exp_result_us = exp_tmp_l[i+1]
            elif exp == "ds" and i != tmp_len :
                exp_result_ds = exp_tmp_l[i+1]
            else :
                continue
    else :
        exp_result_old = aArgs['exp_result']
    
    result = "fail"
    
    # 1 start capture packets
    #list_capts=[]
    list_capt_old=[]
    list_capt_us=[]
    list_capt_ds=[]
    if aArgs.has_key('capt_options') and aArgs['capt_options']!= "x" :
        list_capt_old=_start_capt_option(aArgs['capt_options'])
        
    if aArgs.has_key('us_capt_options') and aArgs['us_capt_options']!= "x" :
        list_capt_us=_start_capt_option(aArgs['us_capt_options'])

    if aArgs.has_key('ds_capt_options') and aArgs['ds_capt_options']!= "x" :
        list_capt_ds=_start_capt_option(aArgs['ds_capt_options'])

    if aArgs.has_key('capt_reserve') and 'true' == aArgs['capt_reserve'] :
        if aArgs.has_key('capt_options') and aArgs['capt_options']!= "x" :
            list_capt_old.append('capt_reserve=true')

        if aArgs.has_key('us_capt_options') and aArgs['us_capt_options']!= "x" :
            list_capt_us.append('capt_reserve=true')

        if aArgs.has_key('ds_capt_options') and aArgs['ds_capt_options']!= "x" :
            list_capt_ds.append('capt_reserve=true')
    
    # 2 send packets
    if aArgs.has_key('send_options') and aArgs['send_options']!= "x" :
        _send_traffic(aArgs['send_options'])
        
    if aArgs.has_key('us_send_options') and aArgs['us_send_options']!= "x" :
        _send_traffic(aArgs['us_send_options'])    
    
    if aArgs.has_key('ds_send_options') and aArgs['ds_send_options']!= "x" :
         _send_traffic(aArgs['ds_send_options'])


    if aArgs.has_key('delay_time') :
        time.sleep(int(aArgs['delay_time'].strip('s')))
  
    # 3 stop capture and check packets
    result_old=""
    result_us=""
    result_ds=""
    if aArgs.has_key('capt_options') and aArgs['capt_options']!= "x" :
        list_capt_old.append('exp_res='+exp_result_old)
        tuple_capt_options = tuple(list_capt_old)
        try: 
            result_old = pcta_receive_traffic(*tuple_capt_options)
        except Exception as inst:
            raise PctaContinueError("%s:%s -> exception in pcta_receive_traffic: %s" \
        % (__name__,keyword_name,inst)) 
    if aArgs.has_key('us_capt_options') and aArgs['us_capt_options']!= "x" :
        list_capt_us.append('exp_res='+exp_result_us)
        tuple_capt_options = tuple(list_capt_us)
        try: 
            result_us = pcta_receive_traffic(*tuple_capt_options)
        except Exception as inst:
            raise PctaContinueError("%s:%s -> exception in pcta_receive_traffic: %s" \
        % (__name__,keyword_name,inst))
    if aArgs.has_key('ds_capt_options') and aArgs['ds_capt_options']!= "x" :   
        list_capt_ds.append('exp_res='+exp_result_ds)
        tuple_capt_options = tuple(list_capt_ds)
        try: 
            result_ds = pcta_receive_traffic(*tuple_capt_options)
        except Exception as inst:
            raise PctaContinueError("%s:%s -> exception in pcta_receive_traffic: %s" \
        % (__name__,keyword_name,inst))

    if result_us == "fail" or result_ds == "fail" or result_old == "fail" :
        result="fail"
        raise PctaContinueError("%s:%s -> not gotten expect traffic" \
        % (__name__,keyword_name)) 
    else :
        result="pass"              
    return result



def _start_capt_option(capt_info):
    keyword_name="_start_capt_option"
    list_capt_options = []
    for item in capt_info.lstrip('[').strip(']').split(',') :
        try :
            m = re.search("\s*u'(.*)'", item)
            item = m.group(1)
        except Exception as inst :
            pass
        finally :    
            list_capt_options.append(item)

    try :
        tuple_capt_options = tuple(list_capt_options)
        dist_id = pcta_start_capture(*tuple_capt_options)
        list_capt_options.append('dist_id='+str(dist_id))
    except Exception as inst :
        raise PctaContinueError("%s:%s -> \
        fail to start capture, exception: %s" \
        % (__name__,keyword_name,inst))
    return list_capt_options



def _send_traffic(send_info):
    keyword_name="_send_traffic"
    list_send_options=[]
    for item in send_info.lstrip('[').strip(']').split(',') :
        try :
            m = re.search("\s*u'(.*)'", item)
            item = m.group(1)
        except Exception as inst :
            pass
        finally :    
            list_send_options.append(item)
    try:
        tuple_send_options = tuple(list_send_options)
        result = pcta_send_traffic(*tuple_send_options)
    except Exception as inst:
        raise PctaContinueError("%s:%s -> \
        fail to send traffic, exception: %s" \
        % (__name__,keyword_name,inst))


def _filter_packets(compdict={},packets={}) :
    """
    comparing packets content with condition in comdict
    """
    keyword_name = "_validate_packets"
    recv_packet_len = len(packets)
    msg = str(recv_packet_len)+ " Packet(s) Received"
    logger.info(msg)
    matched_packets = []

    if compdict.has_key('packet_count'):
        dontcare_scenario = 0
        expect_packets = int(compdict['packet_count'])
        del compdict['packet_count']
    else :
        expect_packets = 1
        dontcare_scenario = 1
    
    for packet in packets :
        flag = 1
        if compdict.has_key('opt82_agent_remote_id_hex') :
            if packet.has_key('dhcp_options') :
                if packet['dhcp_options'].find(compdict['opt82_agent_remote_id_hex']) >= 0:
                    packet['opt82_agent_remote_id_hex'] = \
                    compdict['opt82_agent_remote_id_hex']
                else:
                    continue
            else:
                continue
        for key in compdict.keys() : 
            if packet.has_key(key) :
                if key == 'cfm_flags' :
                    packet[key] = str(int(packet[key],16))
                    compdict[key] = str(int(compdict[key]))

                if type(packet[key]) is list :
                    if compdict[key] in packet[key] :
                        continue
                    else :
                        flag = 0
                        break

                elif packet[key] == compdict[key] :
                    continue
                else :
                    flag = 0
                    break
                      
            else :
                flag = 0
                break
        
        if flag == 1 :
            matched_packets.append(packet)
        else :
            continue
    match_packet_len = len(matched_packets)
    msg1 = str(match_packet_len)+ " Packet(s) Matched"
    logger.info(msg1)
    return matched_packets

 
class PctaContinueError (AssertionError) :
    ROBOT_CONTINUE_ON_FAILURE = True
    
