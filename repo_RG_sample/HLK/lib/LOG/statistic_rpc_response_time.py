#!/usr/bin/env python
"""
    statistic how long each rpc spend in output.xml
    
    usage:
        statistic_rpc_response_time.py --output_file_dir /root/DPU_CI_LOG/201611231249 --record_file /root/DPU_CI_LOG/201611231249/statistic_time.txt
      
    return:
        a result file named statistic_time.txt will be gotten, include the total number of rpcs,the total time of all rpcs and the average time of each rpc
      
    History:
        Nov 23th 2016: designed by zhoujin

"""
import os, re, sys, getopt, time

TOTAL_NUMBER_RPCS = ""
TOTAL_TIME_RPCS = ""
AVER_TIME_RPC = ""


if __name__ == '__main__' :

    import logging
    result_pass ='statistic time success'
    result_fail ='statistic time fail'

    try:
        opts, args = getopt.getopt \
        (sys.argv[1:],"ho:",["output_file_dir=","help"])  
    except getopt.GetoptError:
        print ("illegal options: %s" % str(sys.argv[1:]))
        sys.exit(result_fail)

    if "-h" in sys.argv[1:] or "--help" in sys.argv[1:] :
        print ("Usage:")
        print ("./statistic_rpc_response_time.py --output_file_dir /root/DPU_CI_LOG/201611231249 ")   
        sys.exit(0) 
    
    recordFile = "statistic_time.txt"
    for opt, value in opts:
        if "-o" == opt or "--output_file_dir" == opt:
            outputDir = value.rstrip ("/")

    print ("check output_file_dir is a directory ...")    
    if not os.path.isdir(outputDir):
        print ("output_file_dir %s is not a directory!" % (outputDir))
        sys.exit(result_fail)

    createfile_cmd='touch ' + outputDir + '/' + recordFile
    if os.system(createfile_cmd) :
        logging.info ("%s fail." % (createfile_cmd))            
        sys.exit('create record file fail')      
    
    output_file = outputDir + "/output.xml"
    record_file = outputDir + "/statistic_time.txt"
    try :
        lines = open(output_file,'r').readlines()
        flen = len(lines) - 1
        rpc_number = 0
        rpcs_time = 0.0
        file_object = open(record_file,'a+')
        for i in range(flen) :
            if "NETCONF response time" in lines[i] :
                rpc_number = rpc_number + 1                
                file_object.writelines(lines[i])             
                data = re.search("NETCONF response time: (\d+.\d+) seconds",lines[i])                           
                rpcs_time = rpcs_time + float(data.group(1))               
        aver_time = rpcs_time/rpc_number
        total_rpcs_number_line = "Total rpcs number is " + str(rpc_number) + "\n"
        total_rpcs_time_line = "Total rpcs time is " + str(rpcs_time) + " " + "seconds" + "\n"
        average_rpc_time_line = "Average time of each rpc is " + str(aver_time) + " " + "seconds" + "\n"
        file_object.writelines(total_rpcs_number_line) 
        file_object.writelines(total_rpcs_time_line)
        file_object.writelines(average_rpc_time_line)            
        file_object.close( )
        print ("Total rpcs number is %s" % (rpc_number))
        print ("Total rpcs time is %s seconds" % (rpcs_time))
        print ("Average time of each rpc is %s seconds" % (aver_time)) 
        sys.exit(result_pass)
    except Exception as inst:
        print ("create report file FAIL,get:%s" % inst) 
        sys.exit(result_fail)

 
