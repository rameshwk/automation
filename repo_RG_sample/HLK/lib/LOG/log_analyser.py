#!/usr/local/bin/python2.7

"""
log_analyser.py usage details



The script performs the following functionalities ( each functionality is independent of each other)

 1) Analyses the input logs at the test case level.This functionality compares two or more Robot Framework output files and creates a
	report where possible differences between test case statuses in each file are highlighted. usage is comparing earlier test results with newer ones to find out possible
	status changes and added test cases.For this functionality the following are the options:

 Options:
 
 -F --functionality **	  Specifies the type of functionality of the log to be analysed (testcase)
 
 -r --report file         HTML report file (created from the input files).
                          Default is 'robotdiff.html'.

 -n --name name *         Custom names for test runs. If this option is used,
                          it must be used as many times as there are input
                          files. By default test run names are got from the
                          input file names.

 -t --title title         Title for the generated diff report. The default
                          title is 'Test Run Diff Report'.
 

 --help                   Print this usage instruction.

 
 Examples:
  log_analyser.py -F testcase output1.xml output2.xml output3.xml 
  log_analyser.py -F testcase --name Env1 --name Env2 smoke1.xml smoke2.xml
	

 2) Analyses two different logs ( new run and old run) and predicts the type of issue (NEW,IDENTICAL,NON-IDENTICAL,UNPREDICTABLE) for the failed test cases
 	in the new runs by comparing with the old ones. This would return a csv file containg the failure test case names,issue type and the failed keywords
	
	-------------------------------------------------------------------------------------------------------------------------
	ISSUE TYPE		 |	DESCRIPTION
	-------------------------------------------------------------------------------------------------------------------------
	1) NEW			 |	To represent test cases failed in new runs alone
					 |
	2) IDENTICAL	 |	To represent the test case(s) that are failed in both the runs and the occurance of the failure(s) 
					 |	is in the same key word(s)
					 |
	3) NON-IDENTICAL |	To represent the test cases that are failed in both runs and the occurance of the failure are
					 |	due to different key words
					 |
	4) UNPREDICTABLE |  To represent failed test cases(s) that are been modified in the new run 		
	--------------------------------------------------------------------------------------------------------------------------

 Options:
	
	-F --functionality	**	 Specifies the type of functionality of the log to be analysed (logdiff)
	
	-N --newrunlog ** 		 Specifies the new run log file in XML format. [ not HTML ] 

	-O --oldrunlog **	  	 Specifies the old run log file in XML format. [ not HTML]
	
 	-L --logdir		    	 This is an optional argument to specify the path where the 
							 output file has to be saved. If this option is not mentioned
					 		 then the output file is stored in the current working dir path.
	
	--help					 To print this help message

 EXAMPLE:
 log_analyser.py -F logdiff -N output.xml -O output.xml 
 log_analyser.py --functionality logdiff --newrunlog output.xml --oldrunlog output.xml --logdir /home/logs/output.csv

NOTE : In case if no comparison is needed and the log details of the new run alone is required in that case 
 EXAMPLE :	
	log_analyser.py -F logdiff -N output.xml
 This would return the complete details of the log file along with the test suit,test case,key word and its 
 respective status in a csv file.


Options that can be specified multiple times are marked with an asterisk (*).
 Options that are marked with two asterisk (**) are mandatory
"""


import re,sys,os,itertools,datetime,commands,logging,math,time,copy,inspect
import csv,pprint

"""
FUNCTION NAME : cli_argument_selection

DESCRIPTION   : This function processes the command line arguments that are given as input to the post_processing.py
				script and returns as a dictionary to the 'main_post_processing' function

RETURN VALUE  :  returns as a dictionary ['dDict_cli_details'] to the 'main_post_processing' function
"""
def cli_argument_selection():
	dDict_cli_details = {}
	for temp in sys.argv :
		if temp=='-newrunlog' or temp=='-N':
			iIndexTemp = sys.argv.index(temp)
			dDict_cli_details.update({'newRunLog': sys.argv[iIndexTemp + 1]}) 
		if temp == '-oldrunlog' or temp == '-O':
			iIndexTemp = sys.argv.index(temp)
			dDict_cli_details.update({'oldRunLog': sys.argv[iIndexTemp + 1]})
		if temp == 'logdir' or temp == '-L':
			iIndexTemp = sys.argv.index(temp)
			dDict_cli_details.update({'logdir': sys.argv[iIndexTemp + 1]})
	return dDict_cli_details		


"""
FUNCTION NAME : xml_to_csv_file_conversion

INPUT ARGUMENT : The dictionary containing the processed output of the funcition cli_argument_selection

DESCRIPTION   : This file is used to convert the input xml file into csv file. This function uses Robot framework's
				built in script for this process. This will create csv file named as newRun.csv and oldRun.csv depending on the command line inputs

RETURN VALUE  : Returns the path where the created csv file(s) are stored to the 'main_post_processing' function.
"""


def xml_to_csv_file_conversion(args):
	loc = commands.getoutput('locate times2csv.py')
	sCurrentPath = os.getcwd()
	if args.has_key('newRunLog'):
		sNewRun_csv = sCurrentPath+"/newRun.csv"
		#The format is times2csv output.xml output.csv
		os.system(loc+" " +args['newRunLog']+" "+sNewRun_csv)
	if args.has_key('oldRunLog'):
		sOldRun_csv = sCurrentPath+"/oldRun.csv"
		os.system(loc+" " +args['oldRunLog']+" "+sOldRun_csv) 
	return sCurrentPath


"""
FUNCTION NAME : get_failed_tests_kw

INPUT ARGUMENT : The csv file name and/or with path

DESCRIPTION	  : This fuction parses the csv file and returns a list of dict(s) with  the failed test case name(s) and keywords if any
				
RETURN VALUE  : lFailedKwComplete
"""

def get_failed_tests_kw(csvFilePath):
	lFailedTests = []
	lFailedKeyWords = ""
	lFailedKwComplete = []
	flag_testCase = 0
	dFailed_dict = {}
	sFailedKeyWords = ""
	with open(csvFilePath,'r') as csvfile:
		fp = csv.reader(csvfile)
		count = 0
		for row in fp:
					
			if '|- Test' in row[0] and flag_testCase == 1 :
				flag_testCase = 0
				dFailed_dict = {'FAILED TEST CASE':sFailedTestCase,'FAILED KEY WORD(s)':sFailedKeyWords}
				lFailedKwComplete.append(dFailed_dict)
				sFailedKeyWords = ""
				count = 0
			if '|- Test' in row[0] and 'FAIL' in row[2] :
				sFailedTestCase = row[1]
				lFailedTests.append(row[1])
				flag_testCase = 1
									
			if '|- Kw' in row[0] and 'FAIL' in row[2] and flag_testCase == 1:
				if count > 0:
					sFailedKeyWords=sFailedKeyWords+"; "+row[1]
				else:
					sFailedKeyWords = row[1]
				count = 1
	
	if len(lFailedTests) > 0 :
		return lFailedKwComplete

"""
FUNCTION NAME : csv_file_writer
DESCRIPTION   : To write the input contents into a csc file
"""

def csv_file_writer(lList_newruns,dDictOut,lFieldNames):
	if dDictOut.has_key('logdir'):
		sLoc = dDictOut['logdir']
	else:
		sCurrentPath = os.getcwd()
		sLoc = sCurrentPath+"/output.csv"
	csv_file = open(sLoc,'w+')
	csvwriter= csv.DictWriter(csv_file,delimiter=",",fieldnames=lFieldNames,dialect='excel')
	csvwriter.writeheader()
	csvwriter.writerows(lList_newruns)
	print " The output file is available in the path : "+sLoc


"""
FUNCTION NAME(s) : get_issue_type_old_failure and get_reason_run
DESCRIPTION      : To find failure reason(s) for the test cases which are failed in both runs ( new and old) 


"""
def get_issue_type_old_failure(test_case_name):
	sCurrentPath = os.getcwd()
	sLoc_newRun = sCurrentPath+"/newRun.csv"
	sLoc_oldRun = sCurrentPath+"/oldRun.csv"
	lLoc = [sLoc_newRun,sLoc_oldRun]
	count_loop = 0
	lList_newRun_kws = []
	lList_newRun_status = []
	lList_oldRun_kws = []
	lList_oldRun_status = []
	for temp_loc in lLoc:
		flag_test_case = 0
		count_loop += 1
		with open(temp_loc,'r') as csvfile:
			fp_new = csv.reader(csvfile)
			 
			for row in fp_new:
				if '|- Test' in row[0] and test_case_name == row[1]:
					flag_test_case = 1
					continue
				if flag_test_case == 1:
					#the for loop breaks if the next suit or test case occurs in the log file the loop will break
					if '|- Test' in row[0] or 'Suite' in row[0]:
						break
					#checking for all possible values between two test cases like keyword,for,tear down etc
					if 'Kw' in row[0] or 'For' in row[0] or 'Teardown' in row[0] or 'SetUp' in row[0]:
						if count_loop == 1:
							
							lList_newRun_kws.append(row[1])
							lList_newRun_status.append(row[2])
							
						elif count_loop == 2:
							lList_oldRun_kws.append(row[1])
							lList_oldRun_status.append(row[2])
	## to check for  ISSUE TYPE by comparing the key words and it's respective status
	# 1) Check if there are changes in the test case ---> returns UNPREDICTABLE
	# 2) Check if there are no changes in the two test cases of new and old runs and then check if the status are different ---> return NON-IDENTICAL
	# 3) Else if both the key words are identical and also the status of failure is also at the same instance ----> return IDENTICAL
	if 	set(lList_newRun_kws) != set(lList_oldRun_kws) :
		return 'UNPREDICTABLE'
	elif set(lList_newRun_status) != set(lList_oldRun_status):					
		return 'NON-IDENTICAL'
	else:
		return 'IDENTICAL'			
							



def get_reason_run(lList_new_run,lList_old_run):
	
	lList_new_tests_failed = []
	lList_old_tests_failed = []
	lTest_case_issue = []   #to gather all the test cases,keywords and its issues
	for temp_newRun in lList_new_run:
		lList_new_tests_failed.append(temp_newRun.get('FAILED TEST CASE'))
	print " The test cases failed in latest/new run : %s " %lList_new_tests_failed
	for temp_oldRun in lList_old_run:
		lList_old_tests_failed.append(temp_oldRun.get('FAILED TEST CASE'))
	print "The test cases failed in old run %s " %lList_old_tests_failed
	lMatched_test_case = []
	lUnmatched_test_case = []
	## segregation of test cases based on new and old issues.lMatched_test_case referes to failed test cases in
	## both the runs. The lUnmatched_test_case refers to failed test cases in new run alone [NEW ISSUES]
	for sTestCase in lList_new_tests_failed:
		#sMatched_test_case = set(lList_new_tests_failed) & set(lList_old_tests_failed)
		if sTestCase in lList_old_tests_failed:
			lMatched_test_case.append(sTestCase)
		else:
			lUnmatched_test_case.append(sTestCase)
	print "Test case(s) with new issues %s " %lUnmatched_test_case
	print "Test Case(s) with old issues %s " %lMatched_test_case
	
	### Tagging unmatched test cases as the ones with new issues
	if len(lUnmatched_test_case) > 0:
		
		for temp in lList_new_run:
			if temp['FAILED TEST CASE'] in lUnmatched_test_case:
				temp.update({'ISSUE TYPE' : 'NEW'})
				lTest_case_issue.append(temp)
		
	### Tagging matched test cases with IDENTICAL/NON-IDENTICAL/UNPREDICTABLE
	for testCaseName in  lMatched_test_case:
		sIssue_type = get_issue_type_old_failure(testCaseName)
		for temp in lList_new_run:
			if temp['FAILED TEST CASE'] in lMatched_test_case:
				temp.update({'ISSUE TYPE' : sIssue_type})
				lTest_case_issue.append(temp)
	return lTest_case_issue			

"""
FUNCTION NAME : test_case_diff
DESCRIPTION   : Analyses the input logs at the test case level.This functionality compares two or more Robot Framework output files and creates a
				report where possible differences between test case statuses in each file are highlighted. usage is comparing earlier test results with newer ones to find out possible
				status changes and added test cases.

"""


def test_case_diff(args):
	loc = commands.getoutput('locate robotdiff.py')
	sArg_details = ""
	for temp in args:
		sArg_details = sArg_details + " "+ temp
		os.system(loc+sArg_details)
	
	
def main_logdiff_processing():
	dDictOut = cli_argument_selection()
	sCurrPath = xml_to_csv_file_conversion(dDictOut)	
	if dDictOut.has_key('newRunLog'):
		fileLoc = sCurrPath+"/newRun.csv"
		lList_new_run_details = get_failed_tests_kw(fileLoc)
		#pprint.pprint(lList_new_run_details)
	if dDictOut.has_key('oldRunLog'): 
		#do the cmp work here
		fileLoc = sCurrPath+"/oldRun.csv"
		lList_old_run_details = get_failed_tests_kw(fileLoc)
		lList_reason_run = get_reason_run(lList_new_run_details,lList_old_run_details)
		## writing contents to a csv file
		fieldNames = ['FAILED TEST CASE','ISSUE TYPE','FAILED KEY WORD(s)']
		csv_file_writer(lList_reason_run,dDictOut,fieldNames)
		
	else:
		if lList_new_run_details != 'NONE':
			fieldNames = ['FAILED TEST CASE','FAILED KEY WORD(s)']
			csv_file_writer(lList_new_run_details,dDictOut,fieldNames)
		
		else:
			print "No Failed Test cases found in the given log !! "
		
		
						 	
if __name__=='__main__':
	
	if  2 <= len(sys.argv) <= 3 or '--help' in sys.argv :
		print __doc__
		sys.exit(1)
	else :
		flag_argCheck = 0
		for temp in sys.argv[1:]:
			if '-F' in temp or '--functionality' in temp:
				flag_argCheck = 1
				iIndexTemp = sys.argv.index(temp)
				if sys.argv[iIndexTemp + 1] == 'testcase':
					test_case_diff(sys.argv[3:])
				elif sys.argv[iIndexTemp + 1] == 'logdiff':
					main_logdiff_processing()
		if flag_argCheck == 0:
			print "Missing -F or --functionality option which is mandatory"
			print "The following is the usage options "
			print __doc__
			sys.exit(1)
		
		
