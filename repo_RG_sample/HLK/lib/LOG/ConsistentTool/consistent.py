import sys, os
from ConsistentTool.common import unic
from ConsistentTool.xmlresult import ExecutionResult, Result, get_fail_output
from ConsistentTool.jsbuilder import JsModelBuilder
from ConsistentTool.writer import LogWriter, ReportWriter, TraceWriter, RebotSettings, FailLogWriter

class ResultWriter(object):
    def __init__(self, *sources):
        self._sources = sources
        input_directory = os.path.dirname(os.path.abspath(self._sources[0]))
        self.FILEPATH = {
            'NT_TRACE': os.path.join(input_directory, 'nt_error_record.txt'),
            'LT_TRACE': os.path.join(input_directory, 'lt_error_record.txt'),
            'ALARM': os.path.join(input_directory, 'alarm_record.txt'),
            'VARIABLE': os.path.join(input_directory, 'robot_variable.txt')
        }
        os.environ['FILEPATH'] = str(self.FILEPATH)

    def write_results(self, settings=None, **options):
        settings = settings or RebotSettings(options)
        results = Results(settings, *self._sources)
        if settings.results:
            res = results.db_results
            return res
        if settings.log:
            config = dict(settings.log_config, minLevel=results.js_result.min_level)
            self._write_log(results.js_result, settings.log, config)
        if settings.report:
            # results.js_result.remove_data_not_needed_in_report()
            self._write_report(results.js_result, settings.report, settings.report_config)
        if settings.fail_log:
            failXML = get_fail_output(*self._sources)
            if failXML != None:
                failResults = Results(settings, failXML)
                config = dict(settings.log_config, minLevel=failResults.js_result.min_level)
                self._write_fail_log(failResults.js_result, settings.fail_log, config)
        return results.return_code

    def _write_log(self, js_result, path, config):
        self._write('Log', LogWriter(js_result).write, path, config)

        if os.path.exists(self.FILEPATH['NT_TRACE']):
            NT_TRACE_HTML=os.path.join(os.path.dirname(path), os.path.basename(self.FILEPATH['NT_TRACE']).replace('.txt', '.html'))
            self._write('NT_Trace', TraceWriter(self.FILEPATH['NT_TRACE']).write, NT_TRACE_HTML, 'trace')
        if os.path.exists(self.FILEPATH['LT_TRACE']):
            LT_TRACE_HTML=os.path.join(os.path.dirname(path), os.path.basename(self.FILEPATH['LT_TRACE']).replace('.txt', '.html'))
            self._write('LT_Trace', TraceWriter(self.FILEPATH['LT_TRACE']).write,LT_TRACE_HTML, 'trace')
        if os.path.exists(self.FILEPATH['ALARM']):
            ALARM_HTML=os.path.join(os.path.dirname(path), os.path.basename(self.FILEPATH['ALARM']).replace('.txt', '.html'))
            self._write('Alarm_Info', TraceWriter(self.FILEPATH['ALARM']).write, ALARM_HTML, 'alarm')

    def _write_report(self, js_result, path, config):
        self._write('Report', ReportWriter(js_result).write, path, config)

    def _write_fail_log(self, js_result, path, config):
        self._write('Fail_Log', FailLogWriter(js_result).write, path, config)

    def _write(self, name, writer, path, *args):
        try:
            writer(path, *args)
        except Exception as err:
            print (err.message)
        except EnvironmentError as err:
            print ("Writing %s file '%s' failed: %s: %s" % (name.lower(), path, err.strerror, unic(err.filename)))
        else:
            print (name, path)


class Results(object):

    def __init__(self, settings, *sources):
        self._settings = settings
        self._sources = sources

        if len(sources) == 1 and isinstance(sources[0], Result):
            self._result = sources[0]
            self._prune = False
            self.return_code = self._result.return_code
        else:
            self._result = None
            self._prune = True
            self.return_code = -1
        self._js_result = None
        self._id_result = None
        self._results = None

    @property
    def result(self):
        if self._result is None:
            include_keywords = bool(self._settings.log or self._settings.output)
            self._result = ExecutionResult(*self._sources, include_keywords=include_keywords)
            self._result.configure(self._settings.status_rc,
                                   self._settings.suite_config,
                                   self._settings.statistics_config)
            self.return_code = self._result.return_code

        return self._result

    @property
    def js_result(self):
        if self._js_result is None:
            builder = JsModelBuilder(log_path=self._settings.log,
                                     split_log=self._settings.split_log,
                                     prune_input_to_save_memory=self._prune)
            self._js_result = builder.build_from(self.result)
            if self._prune:
                self._result = None
        return self._js_result

    @property
    def db_results(self):
        if self._results is None:
            builder = JsModelBuilder(log_path=self._settings.log, split_log=False, prune_input_to_save_memory=False)
            self._results = builder.get_results(self.result)
        return self._results

if __name__ == "__main__":
    options = {}
    settings = RebotSettings(options)
    datasources = sys.argv[1]
    rc = ResultWriter(datasources).write_results(settings)
    if rc < 0:
        raise AssertionError('No outputs created.')
