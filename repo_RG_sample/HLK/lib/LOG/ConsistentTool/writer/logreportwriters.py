#  Copyright 2008-2015 Nokia Solutions and Networks
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

from os.path import basename, splitext, sep
from ConsistentTool.common import is_string, file_writer
from .htmlfilewriter import HtmlFileWriter, ModelWriter
from .jswriter import JsResultWriter, SplitLogWriter
from .trancehandle import TraceFileHandle


LOG = 'rebot/log.html'
REPORT = 'rebot/report.html'
FailLOG = 'rebot/fail_log.html'


class _LogReportWriter(object):

    def __init__(self, js_model):
        self._js_model = js_model

    def _write_file(self, path, config, template):
        outfile = file_writer(path) \
            if is_string(path) else path  # unit test hook
        with outfile:
            model_writer = RobotModelWriter(outfile, self._js_model, config)
            writer = HtmlFileWriter(outfile, model_writer)
            writer.write(template)


class LogWriter(_LogReportWriter):

    def write(self, path, config):
        self._write_file(path, config, LOG)
        if self._js_model.split_results:
            self._write_split_logs(splitext(path)[0])

    def _write_split_logs(self, base):
        for index, (keywords, strings) in enumerate(self._js_model.split_results):
            index += 1  # enumerate accepts start index only in Py 2.6+
            self._write_split_log(index, keywords, strings, '%s-%d.js' % (base, index))

    def _write_split_log(self, index, keywords, strings, path):
        with file_writer(path) as outfile:
            writer = SplitLogWriter(outfile)
            writer.write(keywords, strings, index, basename(path))


class ReportWriter(_LogReportWriter):

    def write(self, path, config):
        self._write_file(path, config, REPORT)


class FailLogWriter(LogWriter):

    def write(self, path, config):
        self._write_file(path, config, FailLOG)
        if self._js_model.split_results:
            self._write_split_logs(splitext(path)[0])


class RobotModelWriter(ModelWriter):

    def __init__(self, output, model, config):
        self._output = output
        self._model = model
        self._config = config

    def write(self, line):
        JsResultWriter(self._output).write(self._model, self._config)


class CustomJsResultWriter(JsResultWriter):
    _trace_time = 'trace_time'
    _trace_msg = 'trace_msg'
    _alarm_time = 'alarm_time'
    _alarm_msg = 'alarm_msg'

    def _output_var(self, key):
        return '%s["%s"]' % (self._output_attr, key)

    def _write_trace_time(self, trace_time):
        #self._write(self._start_block, postfix='', separator=False)
        self._write_json('%s = ' % self._output_var(self._trace_time), trace_time, separator=False)
        self._write(self._end_block, postfix='', separator=False)

    def _write_trace_info(self, trace_info):
        self._write(self._start_block, postfix='', separator=False)
        self._write_json('%s = ' % self._output_var(self._trace_msg), trace_info, separator=False)
        self._write(self._end_block, postfix='', separator=False)

    def _write_alarm_time(self, alarm_time):
        self._write_json('%s = ' % self._output_var(self._alarm_time), alarm_time, separator=False)
        self._write(self._end_block, postfix='', separator=False)

    def _write_alarm_info(self, alarm_info):
        self._write(self._start_block, postfix='', separator=False)
        self._write_json('%s = ' % self._output_var(self._alarm_msg), alarm_info, separator=False)
        self._write(self._end_block, postfix='', separator=False)

    def write(self, result, tag):
        self._start_output_block()
        if tag == 'trace':
            self._write_trace_time(result.get_trace_timestamp)
            self._write_trace_info(result.get_trace_log)
        elif tag == 'alarm':
            self._write_alarm_time(result.get_alarm_timestamp)
            self._write_alarm_info(result.get_alarm_log)


class CustomRobotModelWriter(RobotModelWriter):
    def write(self, line):
        CustomJsResultWriter(self._output).write(self._model, self._config)

    @property
    def get_result(self):
        time = ''
        log = ''
        if self._config == 'trace':
            time = self._model.get_trace_timestamp
            log = self._model.get_trace_log
        elif self._config == 'alarm':
            time = self._model.get_alarm_timestamp
            log = self._model.get_alarm_log
        return dict(zip(time, log))


class TraceWriter(object):

    def __init__(self, trace_file):
        self._trace_file = trace_file

    def _write_file(self, path, config, template):
        outfile = file_writer(path) \
            if is_string(path) else path
        with outfile:
            trace_handle = TraceFileHandle(self._trace_file)
            model_writer = CustomRobotModelWriter(outfile, trace_handle, config)
            writer = HtmlFileWriter(outfile, model_writer)
            writer.write(template)

    def write(self, path, config):
        template = 'rebot/' + path.split(sep)[-1].split('.')[0] + '.html'
        self._write_file(path, config, template)

    def get(self, config):
        trace_handle = TraceFileHandle(self._trace_file)
        model = CustomRobotModelWriter(None, trace_handle, config)
        return model.get_result
