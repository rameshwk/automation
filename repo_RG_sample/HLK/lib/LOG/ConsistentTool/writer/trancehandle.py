import os, re
import time, socket

beginpattern = re.compile('\*\* ERROR RECORD \*\*')
endpattern = re.compile('\*\* END \*\*')
timepattern = re.compile('^time\W+: \d+/\d+/\d+ \d+:\d+:\d+')
alarmpattern = re.compile('.* major alarm occurred for .*')


def timeConvert(strfTime):
    # Try to convert the string format time to UTC time, such as "05/09/17 03:44:16.675570" to according UTC time.
    try:
        timezone_gap = time.timezone
        if strfTime.find('.') > -1:
            strf_head = strfTime.split('.')[0]
            strf_end = strfTime.split('.')[1]
        else:
            strf_head = strfTime
            strf_end = str(0) * 3

        if strfTime.find('/') > -1:
            utc_time = time.mktime(time.strptime(strf_head, "%d/%m/%y %H:%M:%S"))
        else:
            utc_time = time.mktime(time.strptime(strf_head, "%Y%m%d %H:%M:%S"))
        utc_head = int(utc_time) - timezone_gap
        convert_time = str(utc_head) + strf_end[0:3]

    except Exception as e:
        raise AssertionError("Failed to converted the time to UTC time for the exception: %s" % e)
    return int(convert_time)


class TraceFileHandle(object):
    def __init__(self, traceFile):
        self.traceFile = traceFile
        if not os.path.exists(self.traceFile):
            raise AssertionError("Fail to find trace file: %s" % self.traceFile)

    @property
    def get_trace_timestamp(self):
        time_list = []
        try:
            with open(self.traceFile, 'rb') as fileHandle:
                line = fileHandle.readline()
                while line:
                    if timepattern.search(line):
                        strftime = re.search('^time\W+: (\d+/\d+/\d+ \d+:\d+:\d+\.\d+)', line).group(1)
                        time_list.append(timeConvert(strftime))
                    line = fileHandle.readline()
        except Exception as e:
            raise AssertionError("Failed to get timestamp for exception: %s" % e)
        return time_list

    @property
    def get_trace_log(self):
        list_b = []
        list_e = []
        trace_info = []
        try:
            with open(self.traceFile, 'rb') as fileHandle:
                line = fileHandle.readline()
                while line:
                    if beginpattern.search(line):
                        index_b = fileHandle.tell()
                        list_b.append(index_b)
                    if endpattern.search(line):
                        index_e = fileHandle.tell()
                        list_e.append(index_e)
                    line = fileHandle.readline()

                for i in range(len(list_b)):
                    length = list_e[i] - list_b[i]
                    fileHandle.seek(list_b[i])
                    content = fileHandle.read(length)
                    trace_info.append(content)
        except Exception as e:
            raise AssertionError("Fail to get trace log by exception: %s" % e)
        return trace_info

    @property
    def get_alarm_timestamp(self):
        time_list = []
        try:
            with open(self.traceFile, 'rb') as fileHandle:
                line = fileHandle.readline()
                while line:
                    if alarmpattern.search(line):
                        strftime = re.search('^(\d+/\d+/\d+ \d+:\d+:\d+) major alarm .*', line).group(1)
                        time_list.append(timeConvert(strftime))
                    line = fileHandle.readline()
        except Exception as e:
            raise AssertionError("Failed to get timestamp for exception: %s" % e)
        return time_list

    @property
    def get_alarm_log(self):
        msg_list = []
        try:
            with open(self.traceFile, 'rb') as fileHandle:
                line = fileHandle.readline()
                while line:
                    if alarmpattern.search(line):
                        alarm_msg = re.search('^(\d+/\d+/\d+ \d+:\d+:\d+) (major alarm .*)', line).group(2)
                        msg_list.append(alarm_msg)
                    line = fileHandle.readline()
        except Exception as e:
            raise AssertionError("Failed to get alarm message for exception: %s" % e)
        return msg_list

    @property
    def get_env_log(self):
        build = release = csinfo = shelf = nt = oam = hostip = ltb = pybotcmd = ''
        try:
            with open(self.traceFile, 'rb') as fileHandle:
                line = fileHandle.readline()
                while line:
                    if line.find("SHELF =") > -1:
                        shelf = line.split('=')[1].lstrip()
                    if line.find("NT =") > -1:
                        nt = line.split('=')[1].lstrip()
                    if line.find("BUILD =") > -1:
                        build = line.split('=')[1].lstrip()
                    if line.find("RELEASE =") > -1:
                        release = line.split('=')[1].lstrip()
                    if line.find("ISAMIP =") > -1:
                        oam = line.split('=')[1].lstrip()
                    if line.find("CS_Rev =") > -1:
                        csinfo = line.split('=')[1].lstrip()
                    if line.find("LTB =") > -1:
                        ltb = line.split('=')[1].lstrip()
                    if line.find("PYBOT =") > -1:
                        pybotcmd = line.split('=')[1].lstrip()
                    line = fileHandle.readline()
            try:
                hostip = socket.gethostbyname(socket.gethostname())
            except Exception as inst:
                print "Blocked by the exception of host config issue: %s, failed to get host IP" % inst
                hostip = ''
        except Exception as e:
            raise AssertionError("Failed to get environment info for exception: %s" % e)
        return [build, release, csinfo, shelf, nt, oam, hostip, ltb, pybotcmd]
