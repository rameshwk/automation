function addNodeISAM(parentId, nodeId, nodeLable, position,width) {
	var panel = d3.select("#" + parentId);
	panel.append('div').style('width',width).style('height','430px')
		.style('position','absolute')
		.style('top',position.y).style('left',position.x)
		.style('border','3px #000000 solid').style('white-space','pre-wrap').style('font-size','18px')
		.attr('align','center')
		.attr('id',nodeId).classed('node',true)
		.text(nodeLable);

  return jsPlumb.getSelector('#' + nodeId)[0];

}

function addNodeISAMTitle(parentId, nodeId, nodeLable, position) {
	var panel = d3.select("#" + parentId);
	panel.append('div').style('width','250px').style('height','80px')
		.style('position','absolute')
		.style('top',position.y).style('left',position.x)
		.style('border','3px FFFFFF solid').style('white-space','pre-wrap').style('font-size','18px')
		.attr('align','center')
		.attr('id',nodeId).classed('node',true)
		.text(nodeLable);

  return jsPlumb.getSelector('#' + nodeId)[0];

}

function addNodeNT(parentId, nodeId, nodeLable, position) {
	var panel = d3.select("#" + parentId);
	panel.append('div').style('width','40px').style('height','150px')
		.style('position','absolute')
		.style('top',position.y).style('left',position.x)
		.style('border','3px #EEEE00 solid').style('white-space','pre-wrap').style('font-size','13px')
		.attr('align','center')
		.attr('id',nodeId).classed('node',true)
		.text(nodeLable);

  return jsPlumb.getSelector('#' + nodeId)[0];

}

function addNodeLT(parentId, nodeId, nodeLable, position) {
  var panel = d3.select("#" + parentId);
  panel.append('div').style('width','40px').style('height','320px')
    .style('position','absolute').style('font-size','13px')
    .style('top',position.y).style('left',position.x).style('white-space','pre-wrap')
    .style('border','3px #00FF00 solid').attr('align','center')
    .attr('id',nodeId).classed('node',true)
    .text(nodeLable);
	
  return jsPlumb.getSelector('#' + nodeId)[0];
}

function addNodeONT(parentId, nodeId, nodeLable, position) {
	var panel = d3.select("#" + parentId);
	panel.append('div').style('width','50px').style('height','25px')
		.style('position','absolute')
		.style('top',position.y).style('left',position.x)
		.style('border','3px #9999FF solid').style('font-size','12px')
		.attr('align','center')
		.attr('id',nodeId).classed('node',true)
		.text(nodeLable);

  return jsPlumb.getSelector('#' + nodeId)[0];

}

function addNodeSTC(parentId, nodeId, nodeLable, position) {
	var panel = d3.select("#" + parentId);
	panel.append('div').style('width','120px').style('height','30px')
		.style('position','absolute')
		.style('top',position.y).style('left',position.x)
		.style('border','3px #FF88C2 solid').style('font-size','20px')
		.attr('align','center')
		.attr('id',nodeId).classed('node',true)
		.text(nodeLable);

  return jsPlumb.getSelector('#' + nodeId)[0];

}

function addNode(parentId, nodeId, nodeLable, position,color) {
	var panel = d3.select("#" + parentId);
	panel.append('div').style('width','50px').style('height','20px')
		.style('position','absolute')
		.style('top',position.y).style('left',position.x)
		.style('border','3px '+color+' solid').style('font-size','10px')
		.attr('align','center')
		.attr('id',nodeId).classed('node',true)
		.text(nodeLable);

  return jsPlumb.getSelector('#' + nodeId)[0];

}

function addSwitch(parentId, nodeId, nodeLable, position,color,width) {
	var panel = d3.select("#" + parentId);
	panel.append('div').style('width',width).style('height','50px')
		.style('position','absolute')
		.style('top',position.y).style('left',position.x)
		.style('border','3px '+color+' solid').style('font-size','20px')
		.attr('align','center')
		.attr('id',nodeId).classed('node',true)
		.text(nodeLable);

  return jsPlumb.getSelector('#' + nodeId)[0];

}

function addDotNode(parentId, nodeId, nodeLable, position,color) {
	var panel = d3.select("#" + parentId);
	panel.append('div').style('width','120px').style('height','15px')
		.style('position','absolute')
		.style('top',position.y).style('left',position.x)
		.style('border','3px '+color+' solid').style('font-size','15px')
		.attr('align','center')
		.attr('id',nodeId).classed('node',true)
		.text(nodeLable);

  return jsPlumb.getSelector('#' + nodeId)[0];

}

function addPorts(instance, node, ports, type) {
  var number_of_ports = ports.length; 
  var i = 0;
  var height = $(node).height();  //Note, jquery does not include border for height
  var y_offset = 1 / ( number_of_ports + 1); 
  var y = 0;

  for ( ; i < number_of_ports; i++ ) {
    var anchor = [0,0,0,0];
    var paintStyle = { radius:5, fillStyle:'#FF0000' };
    var isSource = false, isTarget = false;
    if ( type === 'output' ) {
      anchor[0] = 1;
      paintStyle.fillStyle = '#FF0000';
      isSource = true;
    } else {
      isTarget =true;
    }

    anchor[1] = y + y_offset;
    y = anchor[1];

    instance.addEndpoint(node, {
      uuid:node.getAttribute("id") + "-" + ports[i],
      paintStyle: paintStyle,
      anchor:anchor,
      maxConnections:-1,
      isSource:isSource,
      isTarget:isTarget
    });
  }
}

function connectPorts(instance, node1, port1, node2 , port2) {
  var color = "gray";
  var arrowCommon = { foldback:0.8, fillStyle:color, width:5 },
  // use three-arg spec to create two different arrows with the common values:
  overlays = [
    [ "Arrow", { location:0.8 }, arrowCommon ],
    [ "Arrow", { location:0.2, direction:-1 }, arrowCommon ]
  ];

  var uuid_source = node1.getAttribute("id") + "-" + port1;  //node1-in2
  var uuid_target = node2.getAttribute("id") + "-" + port2;  //node2-in

  instance.connect({uuids:[uuid_source, uuid_target]});
}

jsPlumb.ready(function() {
    console.log("jsPlumb is ready to use");

    //Initialize JsPlumb
    var color = "#E8C870";
    var instance = jsPlumb.getInstance({
      // notice the 'curviness' argument to this Bezier curve.  the curves on this page are far smoother
      // than the curves on the first demo, which use the default curviness value.      
      Connector : [ "Bezier", { curviness:50 } ],
      DragOptions : { cursor: "pointer", zIndex:2000 },
      PaintStyle : { strokeStyle:color, lineWidth:3 },
      EndpointStyle : { radius:5, fillStyle:color },
      HoverPaintStyle : {strokeStyle:"#7073EB" },
      EndpointHoverStyle : {fillStyle:"#7073EB" },
      Container:"flow-panel"
    });      

    instance.doWhileSuspended(function() {

      // declare some common values:
      var arrowCommon = { foldback:0.8, fillStyle:color, width:5 },
      // use three-arg spec to create two different arrows with the common values:
      overlays = [
        [ "Arrow", { location:0.8 }, arrowCommon ],
        [ "Arrow", { location:0.2, direction:-1 }, arrowCommon ]
      ];
	 
	  var node = addNode('flow-panel','ISAMInfo', 'ISAM', {x:'10px',y:'100px'},'#000000');
	  var node = addNode('flow-panel','NTInfo', 'NT', {x:'10px',y:'140px'},'#EEEE00');
	  var node = addNode('flow-panel','LTInfo', 'LT', {x:'10px',y:'180px'},'#00FF00');
	  var node = addNode('flow-panel','ONTInfo', 'ONT', {x:'10px',y:'220px'},'#9999FF');
	  var node = addNode('flow-panel','STCInfo', 'STC/PCTA', {x:'10px',y:'260px'},'#FF88C2');
	  var node = addNode('flow-panel','SEITCHInfo', 'SWITCH', {x:'10px',y:'300px'},'#9400D3');
	  
	  //switch
	  var ltLength = window.lt.length;
	  if(ltLength<2){
		  tempStep = 0;
	  }else{
		  tempStep = (ltLength-2)*350;
	  }
	  
	  var switchNode = addSwitch('flow-panel','switchInfo', 'switch', {x:'500px',y:'90px'},'#9400D3',
							700+tempStep+'px');
	  addPorts(instance, switchNode, ['port1','port2'],'input');
	  
	  //OLT
	  var oltNode = addNodeISAM('flow-panel','isam', '',{x:'300px',y:'180px'},900+tempStep+'px');
	  addPorts(instance, oltNode, ['port1','port2'],'input');
	  
	  var oltTitleNode = addNodeISAMTitle('flow-panel','isamTitleInfo', 'ISAM\n'+'ip:'+window.ip+'\n'+'shelf_type:'+window.shelf_type, {x:'300px',y:'185px'},'#9400D3');
	  
	  //OLT left
	  var nodeOLTLeft1 = addDotNode('flow-panel','nodeOLTLeft1', window.stcPort, {x:'100px',y:'320px'});
	  addPorts(instance, nodeOLTLeft1, ['port1'],'output');
	  var nodeOLTLeft2 = addDotNode('flow-panel','nodeOLTLeft2', window.pctaPort, {x:'0px',y:'460px'});
	  addPorts(instance, nodeOLTLeft2, ['port1'],'output');
	  
	  //STC
	  var stcNode = addNodeSTC('flow-panel','stc', 'stc', {x:'300px',y:'75px'});
	  addPorts(instance, stcNode, ['port1'],'input');
	  addPorts(instance, stcNode, ['port2'],'output');
	  var nodeSTCLeft = addDotNode('flow-panel','nodeSTCLeft', '', {x:'120px',y:'90px'});
	  addPorts(instance, nodeSTCLeft, ['port1'],'input');
	  connectPorts(instance, stcNode, 'port1', nodeSTCLeft, 'port1');
	  
	  //PCTA
	  var pctaNode = addNodeSTC('flow-panel','pcta', 'pcta', {x:'300px',y:'130px'});
	  addPorts(instance, pctaNode, ['port1'],'input');
	  addPorts(instance, pctaNode, ['port2'],'output');
	  var nodePCTALeft = addDotNode('flow-panel','nodePCTALeft', '', {x:'215px',y:'150px'});
	  addPorts(instance, nodePCTALeft, ['port1'],'input');
	  connectPorts(instance, pctaNode, 'port1', nodePCTALeft, 'port1');
	  
	  //OLT left connect to OLT and PCTA left
	  connectPorts(instance, oltNode, 'port1', nodeOLTLeft1, 'port1');
	  connectPorts(instance, oltNode, 'port2', nodeOLTLeft2, 'port1');
	  connectPorts(instance, nodeOLTLeft1, 'port1', nodePCTALeft, 'port1');
	  connectPorts(instance, nodeOLTLeft2, 'port1', nodeSTCLeft, 'port1');
	  
	  //STC and PCTA connect to switch
	  connectPorts(instance, switchNode, 'port1', stcNode, 'port2');
	  connectPorts(instance, switchNode, 'port2', pctaNode, 'port2');
	  
	  //NT
	  $.each(window.nt,function(i,item){
		  var ntStep = 170*i;
		  var ntNode = addNodeNT('flow-panel','nt', ''+item[1]+'\n'+item[0], {x:'370px',y:(270+ntStep)+'px'});
	  });
	 
	 //LT
	  $.each(window.lt,function(i,item){
		  var ltStep = 360*i;
		  var ltNode = addNodeLT('flow-panel','lt'+item[0], 'lt'+item[0]+":"+'\n'+item[1]+'\n'+'\n'+'\n'+'\n'+'\n'+'\n'
									+'\n'+'\n'+'\n'+'\n'+'\n'+'\n'+'\n'+'\n'+'\n'+'...', 
								{x:(430+ltStep)+'px',y:'270px'});
		  addPorts(instance, ltNode, ['port1','port2','port3','port4','port5'],'output');

		  if(item.length-2){
			  var ontArray = item[item.length-1];
			  if(ontArray.length > 5){
				  ontArray = ontArray.slice(0,5);
			  }
			  $.each(ontArray,function(j,item){
				  var ontStep = 55*j;
				  var nodeStep = 30*j;
				  var nodeONT = addNodeONT('flow-panel','lt'+(i+1)+'-'+'ont'+(j+1), 
									'port'+item, {x:(530+ltStep)+'px',y:(300+ontStep)+'px'});
				  addPorts(instance,nodeONT,['port1'],'input');
				  addPorts(instance,nodeONT,['port2'],'output');
				  
				  var port = 'port' + (j+1);
				  connectPorts(instance, ltNode, port, nodeONT, 'port1');
				  
				  var node = addDotNode('flow-panel','lt'+(i+1)+'-'+'node'+(j+1), '', 
				                          {x:(630+ltStep+nodeStep)+'px',y:(310+ontStep)+'px'});
				  addPorts(instance, node, ['port1'],'input');
				  
				  var nodeSwitch = addDotNode('flow-panel','lt'+(i+1)+'-'+'nodeSwitch'+(j+1), '', 
				                          {x:(630+ltStep+nodeStep)+'px',y:'140px'});
				  addPorts(instance, nodeSwitch, ['port1'],'input');
				  
				  connectPorts(instance, node, 'port1', nodeONT, 'port2');
				  connectPorts(instance, node, 'port1', nodeSwitch, 'port1');

			  });
		  }	
			
	  });
	  
	  //instance.draggable($('.node'));
	  
	  /*$.each($('svg'),function(i,item){
		  if(item.id != "undefined"){
			$(item).parent().attr('title',$(item).attr('id'));  
		  }  
	  }); */

    });
    
});