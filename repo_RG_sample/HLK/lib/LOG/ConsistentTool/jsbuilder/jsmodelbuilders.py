#  Copyright 2008-2015 Nokia Solutions and Networks
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

import os, re, copy, datetime
from ConsistentTool.writer import timeConvert, TraceWriter
from .jsbuildingcontext import JsBuildingContext
from .jsexecutionresult import JsExecutionResult
from time import timezone

LEVELS = {
  'NONE'  : 6,
  'FAIL'  : 5,
  'ERROR' : 4,
  'WARN'  : 3,
  'INFO'  : 2,
  'DEBUG' : 1,
  'TRACE' : 0,
}


class JsModelBuilder(object):

    def __init__(self, log_path=None, split_log=False,
                 prune_input_to_save_memory=False):
        if log_path != None:
            self.directPath = os.path.dirname(log_path)
        else:
            self.directPath = ''
        self._context = JsBuildingContext(log_path, split_log, prune_input_to_save_memory)
        self._result = JsBuildingContext(log_path, split_log, prune_input_to_save_memory)
        self._failed_kw = JsBuildingContext(log_path, split_log, prune_input_to_save_memory)

    def get_results(self, result_from_xml):
        suites = GetAllCases(self._context).build(result_from_xml.suite)
        for suite in suites:
            suite['alarmInfo'] = ''
            suite['traceInfo'] = ''

        # input_directory = os.environ.get('FILEPATH')
        # self.FILEPATH = {
        #     'nt_trace': os.path.join(input_directory, 'nt_error_record.txt'),
        #     'lt_trace': os.path.join(input_directory, 'lt_error_record.txt'),
        #     'alarm': os.path.join(input_directory, 'alarm_record.txt')}
        self.FILEPATH = eval(os.environ.get('FILEPATH'))
        if os.path.exists(self.FILEPATH['ALARM']):
            alarms = TraceWriter(self.FILEPATH['ALARM']).get('alarm')
            for suite in suites:
                start = int(suite['startTime'])
                end = start + int(suite['escapeTime'])
                for time in alarms.keys():
                    if start <= int(time) <= end:
                        suite['alarmInfo'] += '\nAlarm_Info: %s' % alarms[time]
        else:
            print "WARNING: No Alarm record file: %s found!" % self.FILEPATH['ALARM']

        if os.path.exists(self.FILEPATH['NT_TRACE']):
            nt_traces = TraceWriter(self.FILEPATH['NT_TRACE']).get('nt_trace')
            for suite in suites:
                start = int(suite['startTime'])
                end = start + int(suite['escapeTime'])
                for time in nt_traces.keys():
                    if start <= int(time) <= end:
                        suite['traceInfo'] += '\nNT_Trace: %s' % nt_traces[time]
        else:
            print "WARNING: No NT trace record file: %s found!" % self.FILEPATH['NT_TRACE']

        if os.path.exists(self.FILEPATH['LT_TRACE']):
            lt_traces = TraceWriter(self.FILEPATH['LT_TRACE']).get('lt_trace')
            for suite in suites:
                start = int(suite['startTime'])
                end = start + int(suite['escapeTime'])
                for t in lt_traces.keys():
                    if start <= int(t) <= end:
                        suite['traceInfo'] += '\nLT_Trace: %s' % lt_traces[t]
        else:
            print "WARNING: No LT trace record file: %s found!" % self.FILEPATH['LT_TRACE']
        timezone_gap = timezone
        for suite in suites:
            suite['ATCTags'] = ' '.join(suite['ATCTags'])
            escapeTime = int(suite['escapeTime'])
            if escapeTime == 0:
                escapeTime = 0
            elif 0 < escapeTime <= 1000:
                escapeTime = 1
            else:
                escapeTime = int(escapeTime/1000)
            suite['escapeTime'] = datetime.timedelta(0, escapeTime)
            suite['startTime'] = datetime.datetime.fromtimestamp(int(suite['startTime']/1000)+timezone_gap).strftime("%Y-%m-%d %H:%M:%S")
        return suites

    @property
    def get_id_list(self):
        id_list = []
        id_strings = self._context.strings
        pattern = re.compile('^\*s1-')
        id_list.append('s1')
        for id_num in id_strings:
            if pattern.match(id_num):
                id_num = pattern.sub('s1-', id_num)
                if not re.search('-k\d+$', id_num):
                    id_list.append(id_num)
        return id_list

    def get_failed_kw(self, suite):
        kwid_list = []
        kwname_list = []
        failed_kw = FailedSuiteBuilder(self._failed_kw).build(suite)
        kw_strings = self._failed_kw.strings
        kw_pattern = re.compile('\d+:\d+')
        star_tag = re.compile('^\**')
        kw_list = kw_pattern.findall(str(failed_kw))
        for item in kw_list:
            index_id = int(item.split(":")[0])
            index_name = int(item.split(":")[1])
            kw_id = star_tag.sub('', kw_strings[index_id])
            kw_name = star_tag.sub('', kw_strings[index_name]) \
                if star_tag.match(kw_strings[index_name]) else kw_strings[index_name]
            kwid_list.append(str(kw_id))
            kwname_list.append(str(kw_name))
        kws_dict = {'kwid':kwid_list, 'kwname':kwname_list}
        return kws_dict

    def build_from(self, result_from_xml):
        # Statistics must be build first because building suite may prune input.
        # TODO: Enhance performance without deepcopy
        original_suite = copy.deepcopy(result_from_xml.suite)
        stat=StatisticsBuilder().build(result_from_xml.statistics)
        suite=SuiteBuilder(self._context).build(result_from_xml.suite)
        errs=ErrorsBuilder(self._context).build(result_from_xml.errors)
        strs=self._context.strings
        bmillis=self._context.basemillis
        splits=self._context.split_results
        mil=self._context.min_level
        sid=self.get_id_list
        fkw=self.get_failed_kw(original_suite)
        return JsExecutionResult(statistics=stat,suite=suite,errors=errs,strings=strs,basemillis=bmillis,suite_id=sid,\
                                 split_results=splits,min_level=mil,failed_kwid=fkw['kwid'],failed_kwname=fkw['kwname'])


class _Builder(object):
    _statuses = {'FAIL': 0, 'PASS': 1, 'NOT_RUN': 2}

    def __init__(self, context):
        self._context = context
        self._string = self._context.string
        self._html = self._context.html
        self._timestamp = self._context.timestamp

    def _get_status(self, item):
        model = (self._statuses[item.status],
                 self._timestamp(item.starttime),
                 item.elapsedtime)
        msg = getattr(item, 'message', '')
        if not msg:
            return model
        elif msg.startswith('*HTML*'):
            msg = self._string(msg[6:].lstrip(), escape=False)
        else:
            msg = self._string(msg)
        return model + (msg,)

    def _build_keywords(self, kws, split=False):
        splitting = self._context.start_splitting_if_needed(split)
        model = tuple(self._build_keyword(k) for k in kws)
        return model if not splitting else self._context.end_splitting(model)


class GetAllCases(_Builder):

    def __init__(self, context):
        _Builder.__init__(self, context)
        self._build_suite = self.build
        self.all_cases = []

    def build(self, suite):
        with self._context.prune_input(suite.suites, suite.tests, suite.keywords):
            suitelist = tuple(self._build_suite(s) for s in suite.suites)
            tag = list(self._yield_metadata(suite))
            for test in suite.tests:
                tname = suite.name+'.'+test.name
                failure = ''
                if len(test.tags) == 0:
                    test.tags = tag
                if test.status != "PASS":
                    msg = 'ERROR MSG: ' + test.message
                    for kw in test.keywords:
                        if kw.status == "FAIL":
                            failure = msg + '\nFAILED KW: ' + kw.name
                            break
                    #         for m in kw.messages:
                    #             if m.level == 'FAIL':
                    #                 failure = kw.name+":"+m.message

                case_info = {'ATCName':tname,'testResult':test.status,'ATCDesc':test.doc,'ATCTags':test.tags, \
                             'startTime':timeConvert(test.starttime),'escapeTime':test.elapsedtime,'errorInfo':failure}
                other_info = {'jobNum':'', 'jobName':'', 'frClassify':'', 'frNewOrOld':'', 'frId':'', 'frStatus':'', 'frDesc':'', \
                              'runOnObjType':'', 'runOnObjName':'', 'rerunResult':'', 'rerunError':'', 'failAnalyzer':''}
                case_info.update(other_info)
                self.all_cases.append(case_info)
        return self.all_cases

    def _yield_metadata(self, suite):
        for name, value in suite.metadata.items():
            yield self._string(name)
            yield self._html(value)


class SuiteBuilder(_Builder):

    def __init__(self, context):
        _Builder.__init__(self, context)
        self._build_suite = self.build
        self._build_test = TestBuilder(context).build
        self._build_keyword = KeywordBuilder(context).build

    def build(self, suite):
        with self._context.prune_input(suite.suites, suite.tests, suite.keywords):
            stats = self._get_statistics(suite)  # Must be done before pruning
            suiteid = self._string(suite.id)
            name = self._string(suite.name, attr=True)
            source = self._string(suite.source)
            relative = self._context.relative_source(suite.source)
            doc = self._html(suite.doc)
            tag = tuple(self._yield_metadata(suite))
            status = self._get_status(suite)
            suitelist = tuple(self._build_suite(s) for s in suite.suites)
            testlist = tuple(self._build_test(t) for t in suite.tests)
            kwlist = tuple(self._build_keyword(k, split=True) for k in suite.keywords)
        return (name,source,relative,doc,tag,status,suitelist,testlist,kwlist,stats)

    def _yield_metadata(self, suite):
        for name, value in suite.metadata.items():
            yield self._string(name)
            yield self._html(value)

    def _get_statistics(self, suite):
        stats = suite.statistics  # Access property only once
        return (stats.all.total,
                stats.all.passed,
                stats.critical.total,
                stats.critical.passed)


class TestBuilder(_Builder):

    def __init__(self, context):
        _Builder.__init__(self, context)
        self._build_keyword = KeywordBuilder(context).build

    def build(self, test):
        with self._context.prune_input(test.keywords):
            testid = self._string(test.id)
            return (self._string(test.name, attr=True),
                    self._string(test.timeout),
                    int(test.critical),
                    self._html(test.doc),
                    tuple(self._string(t) for t in test.tags),
                    self._get_status(test),
                    self._build_keywords(test.keywords, split=True))


class KeywordBuilder(_Builder):
    _types = {'kw': 0, 'setup': 1, 'teardown': 2, 'for': 3, 'foritem': 4}

    def __init__(self, context):
        _Builder.__init__(self, context)
        self._build_keyword = self.build
        self._build_message = MessageBuilder(context).build

    def build(self, kw, split=False):
        with self._context.prune_input(kw.messages, kw.keywords):
            return (self._types[kw.type],
                    self._string(kw.kwname, attr=True),
                    self._string(kw.libname, attr=True),
                    self._string(kw.timeout),
                    self._html(kw.doc),
                    self._string(', '.join(kw.args)),
                    self._string(', '.join(kw.assign)),
                    self._string(', '.join(kw.tags)),
                    self._get_status(kw),
                    self._build_keywords(kw.keywords, split),
                    tuple(self._build_message(m) for m in kw.messages))


class MessageBuilder(_Builder):

    def build(self, msg):
        if msg.level in ('WARN','ERROR'):
            self._context.create_link_target(msg)
        self._context.message_level(msg.level)
        return self._build(msg)

    def _build(self, msg):
        return (self._timestamp(msg.timestamp),
                LEVELS[msg.level],
                self._string(msg.html_message, escape=False))


class StatisticsBuilder(object):

    def build(self, statistics):
        return (self._build_stats(statistics.total),
                self._build_stats(statistics.tags),
                self._build_stats(statistics.suite))

    def _build_stats(self, stats):
        return tuple(stat.get_attributes(include_label=True,
                                         include_elapsed=True,
                                         html_escape=True)
                     for stat in stats)


class ErrorsBuilder(_Builder):

    def __init__(self, context):
        _Builder.__init__(self, context)
        self._build_message = ErrorMessageBuilder(context).build

    def build(self, errors):
        with self._context.prune_input(errors.messages):
            return tuple(self._build_message(msg) for msg in errors)


class ErrorMessageBuilder(MessageBuilder):

    def build(self, msg):
        model = self._build(msg)
        link = self._context.link(msg)
        return model if link is None else model + (link,)


class SuiteIDBuilder(SuiteBuilder):
    def __init__(self, context):
        _Builder.__init__(self, context)
        self._build_suite = self.build
        self._build_test = TestIDBuilder(context).build

    def build(self, suite):
        with self._context.prune_input(suite.suites, suite.tests, suite.keywords):
            return (self._string(suite.id),
                    tuple(self._build_suite(s) for s in suite.suites),
                    tuple(self._build_test(t) for t in suite.tests))


class TestIDBuilder(TestBuilder):
    def __init__(self, context):
        _Builder.__init__(self, context)

    def build(self, test):
        with self._context.prune_input(test.keywords):
            return self._string(test.id)


class FailedSuiteBuilder(SuiteBuilder):
    def __init__(self, context):
        _Builder.__init__(self, context)
        self._build_suite = self.build
        self._build_test = FailedTestBuilder(context).build
        self._build_keyword = FailedKeywordBuilder(context).build

    def build(self, suite):
        with self._context.prune_input(suite.suites, suite.tests, suite.keywords):
            if self._get_status(suite)[0] != 1:
                return (tuple(self._build_suite(s) for s in suite.suites),
                        tuple(self._build_test(t) for t in suite.tests),
                        tuple(self._build_keyword(k, split=True) for k in suite.keywords))


class FailedTestBuilder(TestBuilder):
    def __init__(self, context):
        _Builder.__init__(self, context)
        self._build_keyword = FailedKeywordBuilder(context).build

    def build(self, test):
        with self._context.prune_input(test.keywords):
            if self._get_status(test)[0] != 1:
                return self._build_keywords(test.keywords, split=True)


class FailedKeywordBuilder(KeywordBuilder):
    def __init__(self, context):
        _Builder.__init__(self, context)
        self._build_keyword = self.build

    def build(self, kw, split=False):
        #with self._context.prune_input(kw.messages, kw.keywords):
        with self._context.prune_input(kw.keywords):
            if self._get_status(kw)[0] != 1:
                kw_id = self._string(kw.id)
                kw_name = self._string(kw.kwname, attr=True)
                kw_info = str(kw_id) + ':' + str(kw_name)
                return (kw_info,
                        # self._string(kw.libname, attr=True),
                        self._build_keywords(kw.keywords, split))
