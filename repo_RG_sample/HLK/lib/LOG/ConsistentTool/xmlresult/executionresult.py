#  Copyright 2008-2015 Nokia Solutions and Networks
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

from .statistics import Statistics
from .model import TestSuite
from .xmllogger import XmlLogger
from .executionerrors import ExecutionErrors

class OutputWriter(XmlLogger):

    def __init__(self, output):
        XmlLogger.__init__(self, output, generator='Rebot')

    def start_message(self, msg):
        self._write_message(msg)

    def visit_keyword(self, kw):
        self.start_keyword(kw)
        for child in kw.children:
            child.visit(self)
        self.end_keyword(kw)

    def close(self):
        self._writer.end('robot')
        self._writer.close()

    def end_result(self, result):
        self.close()

class Result(object):
    def __init__(self, source=None, root_suite=None, errors=None):
        self.source = source
        #: Hierarchical execution results as a
        #: :class:`~.result.model.TestSuite` object.
        self.suite = root_suite or TestSuite()
        self.errors = errors or ExecutionErrors()
        self.generated_by_robot = True
        self._status_rc = True
        self._stat_config = {}

    @property
    def statistics(self):

        return Statistics(self.suite, **self._stat_config)

    @property
    def return_code(self):

        if self._status_rc:
            return min(self.suite.statistics.critical.failed, 250)
        return 0

    def configure(self, status_rc=True, suite_config=None, stat_config=None):

        if suite_config:
            self.suite.configure(**suite_config)
        self._status_rc = status_rc
        self._stat_config = stat_config or {}

    def save(self, path=None):
        """Save results as a new output XML file.
        """
        self.visit(OutputWriter(path or self.source))

    def visit(self, visitor):

        visitor.visit_result(self)

    def handle_suite_teardown_failures(self):
        """Internal usage only."""
        if self.generated_by_robot:
            self.suite.handle_suite_teardown_failures()

    def handle_suite_setup_failures(self):
        """Internal usage only."""
        if self.generated_by_robot:
            self.suite.handle_suite_setup_failures()
