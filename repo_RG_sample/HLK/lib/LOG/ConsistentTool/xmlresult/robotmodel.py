#  Copyright 2008-2015 Nokia Solutions and Networks
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

from itertools import chain
from operator import attrgetter
from ConsistentTool.common import html_escape, setter, SetterAwareType, is_string, NormalizedDict, unic
from .tags import Tags
from .visitor import SuiteVisitor
from .filter import Filter, EmptySuiteRemover

def with_metaclass(meta, *bases):
    """Create a base class with a metaclass."""
    # This requires a bit of explanation: the basic idea is to make a
    # dummy metaclass for one level of class instantiation that replaces
    # itself with the actual metaclass.
    class metaclass(type):
        def __new__(cls, name, this_bases, d):
            return meta(name, bases, d)
    return type.__new__(metaclass, 'temporary_class', (), {})

def seq2str(sequence, quote="'", sep=', ', lastsep=' and '):
    """Returns sequence in format 'item 1', 'item 2' and 'item 3'"""
    quote_elem = lambda string: quote + unic(string) + quote
    if not sequence:
        return ''
    if len(sequence) == 1:
        return quote_elem(sequence[0])
    elems = [quote_elem(s) for s in sequence[:-2]]
    elems.append(quote_elem(sequence[-2]) + lastsep + quote_elem(sequence[-1]))
    return sep.join(elems)

class ModelObject(with_metaclass(SetterAwareType, object)):
    __slots__ = []

    def __unicode__(self):
        return self.name

    def __repr__(self):
        return repr(str(self))


class ItemList(object):
    __slots__ = ['_item_class', '_common_attrs', '_items']

    def __init__(self, item_class, common_attrs=None, items=None):
        self._item_class = item_class
        self._common_attrs = common_attrs
        self._items = ()
        if items:
            self.extend(items)

    def create(self, *args, **kwargs):
        return self.append(self._item_class(*args, **kwargs))

    def append(self, item):
        self._check_type_and_set_attrs(item)
        self._items += (item,)
        return item

    def _check_type_and_set_attrs(self, *items):
        common_attrs = self._common_attrs or {}
        for item in items:
            if not isinstance(item, self._item_class):
                raise TypeError("Only %s objects accepted, got %s."
                                % (self._item_class.__name__,
                                   item.__class__.__name__))
            for attr in common_attrs:
                setattr(item, attr, common_attrs[attr])
        return items

    def extend(self, items):
        self._items += self._check_type_and_set_attrs(*items)

    def insert(self, index, item):
        self._check_type_and_set_attrs(item)
        items = list(self._items)
        items.insert(index, item)
        self._items = tuple(items)

    def index(self, item, *start_and_end):
        return self._items.index(item, *start_and_end)

    def clear(self):
        self._items = ()

    def visit(self, visitor):
        for item in self:
            item.visit(visitor)

    def __iter__(self):
        return iter(self._items)

    def __getitem__(self, index):
        if not isinstance(index, slice):
            return self._items[index]
        items = self.__class__(self._item_class)
        items._common_attrs = self._common_attrs
        items.extend(self._items[index])
        return items

    def __setitem__(self, index, item):
        if isinstance(index, slice):
            self._check_type_and_set_attrs(*item)
        else:
            self._check_type_and_set_attrs(item)
        items = list(self._items)
        items[index] = item
        self._items = tuple(items)

    def __len__(self):
        return len(self._items)

    def __unicode__(self):
        return u'[%s]' % ', '.join(unicode(item) for item in self)


class SuiteConfigurer(SuiteVisitor):

    def __init__(self, name=None, doc=None, metadata=None, set_tags=None,
                 include_tags=None, exclude_tags=None, include_suites=None,
                 include_tests=None, empty_suite_ok=False):
        self.name = name
        self.doc = doc
        self.metadata = metadata
        self.set_tags = set_tags or []
        self.include_tags = include_tags
        self.exclude_tags = exclude_tags
        self.include_suites = include_suites
        self.include_tests = include_tests
        self.empty_suite_ok = empty_suite_ok

    @property
    def add_tags(self):
        return [t for t in self.set_tags if not t.startswith('-')]

    @property
    def remove_tags(self):
        return [t[1:] for t in self.set_tags if t.startswith('-')]

    def visit_suite(self, suite):
        self._set_suite_attributes(suite)
        self._filter(suite)
        suite.set_tags(self.add_tags, self.remove_tags)

    def _set_suite_attributes(self, suite):
        if self.name:
            suite.name = self.name
        if self.doc:
            suite.doc = self.doc
        if self.metadata:
            suite.metadata.update(self.metadata)

    def _filter(self, suite):
        name = suite.name
        suite.filter(self.include_suites, self.include_tests,
                     self.include_tags, self.exclude_tags)
        if not (suite.test_count or self.empty_suite_ok):
            self._raise_no_tests_error(name)

    def _raise_no_tests_error(self, suite):
        selectors = '%s %s' % (self._get_test_selector_msgs(),
                               self._get_suite_selector_msg())
        msg = "Suite '%s' contains no tests %s" % (suite, selectors.strip())
        raise AssertionError(msg.strip() + '.')

    def _get_test_selector_msgs(self):
        parts = []
        for explanation, selector in [('with tags', self.include_tags),
                                      ('without tags', self.exclude_tags),
                                      ('named', self.include_tests)]:
            if selector:
                parts.append(self._format_selector_msg(explanation, selector))
        return seq2str(parts, quote='')

    def _format_selector_msg(self, explanation, selector):
        if len(selector) == 1 and explanation[-1] == 's':
            explanation = explanation[:-1]
        return '%s %s' % (explanation, seq2str(selector, lastsep=' or '))

    def _get_suite_selector_msg(self):
        if not self.include_suites:
            return ''
        return self._format_selector_msg('in suites', self.include_suites)

class Metadata(NormalizedDict):

    def __init__(self, initial=None):
        NormalizedDict.__init__(self, initial, ignore='_')

    def __setitem__(self, key, value):
        if not is_string(key):
            key = unic(key)
        if not is_string(value):
            value = unic(value)
        NormalizedDict.__setitem__(self, key, value)

    def __unicode__(self):
        return u'{%s}' % ', '.join('%s: %s' % (k, self[k]) for k in self)

class TagSetter(SuiteVisitor):

    def __init__(self, add=None, remove=None):
        self.add = add
        self.remove = remove

    def start_suite(self, suite):
        return bool(self)

    def visit_test(self, test):
        test.tags.add(self.add)
        test.tags.remove(self.remove)

    def visit_keyword(self, keyword):
        pass

    def __nonzero__(self):
        return bool(self.add or self.remove)


class Message(ModelObject):
    """A message created during the test execution.

    Can be a log message triggered by a keyword, or a warning or an error
    that occurred during parsing or test execution.
    """
    __slots__ = ['message', 'level', 'html', 'timestamp', '_sort_key']

    def __init__(self, message='', level='INFO', html=False, timestamp=None,
                 parent=None):
        #: The message content as a string.
        self.message = message
        #: Severity of the message. Either ``TRACE``, ``DEBUG``, ``INFO``,
        #: ``WARN``, ``ERROR``, or ``FAIL``. The latest one is only used with
        #: keyword failure messages.
        self.level = level
        #: ``True`` if the content is in HTML, ``False`` otherwise.
        self.html = html
        #: Timestamp in format ``%Y%m%d %H:%M:%S.%f``.
        self.timestamp = timestamp
        self._sort_key = -1
        #: The object this message was triggered by.
        self.parent = parent

    @setter
    def parent(self, parent):
        if parent and parent is not getattr(self, 'parent', None):
            self._sort_key = getattr(parent, '_child_sort_key', -1)
        return parent

    @property
    def html_message(self):
        """Returns the message content as HTML."""
        return self.message if self.html else html_escape(self.message)

    def visit(self, visitor):
        """:mod:`Visitor interface <robot.model.visitor>` entry-point."""
        visitor.visit_message(self)

    def __unicode__(self):
        return self.message


class Messages(ItemList):
    __slots__ = []

    def __init__(self, message_class=Message, parent=None, messages=None):
        ItemList.__init__(self, message_class, {'parent': parent}, messages)

    def __setitem__(self, index, item):
        old = self[index]
        ItemList.__setitem__(self, index, item)
        self[index]._sort_key = old._sort_key


class Keyword(ModelObject):
    """Base model for a single keyword.

    Extended by :class:`robot.running.model.Keyword` and
    :class:`robot.result.model.Keyword`.
    """
    __slots__ = ['_name', 'doc', 'args', 'assign', 'timeout', 'type',
                 '_sort_key', '_next_child_sort_key']
    KEYWORD_TYPE = 'kw'         #: Normal keyword :attr:`type`.
    SETUP_TYPE = 'setup'        #: Setup :attr:`type`.
    TEARDOWN_TYPE = 'teardown'  #: Teardown :attr:`type`.
    FOR_LOOP_TYPE = 'for'       #: For loop :attr:`type`.
    FOR_ITEM_TYPE = 'foritem'   #: Single for loop iteration :attr:`type`.
    keyword_class = None        #: Internal usage only.
    message_class = Message     #: Internal usage only.

    def __init__(self, name='', doc='', args=(), assign=(), tags=(),
                 timeout=None, type=KEYWORD_TYPE):
        self.parent = None
        self._name = name
        self.doc = doc
        self.args = args      #: Keyword arguments as a list of strings.
        self.assign = assign  #: Assigned variables as a list of strings.
        self.tags = tags
        self.timeout = timeout
        #: Keyword type as a string. The value is either :attr:`KEYWORD_TYPE`,
        #: :attr:`SETUP_TYPE`, :attr:`TEARDOWN_TYPE`, :attr:`FOR_LOOP_TYPE` or
        #: :attr:`FOR_ITEM_TYPE` constant defined on the class level.
        self.type = type
        self.messages = None
        self.keywords = None
        self._sort_key = -1
        self._next_child_sort_key = 0

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, name):
        self._name = name

    @setter
    def parent(self, parent):
        """Parent test suite, test case or keyword."""
        if parent and parent is not self.parent:
            self._sort_key = getattr(parent, '_child_sort_key', -1)
        return parent

    @property
    def _child_sort_key(self):
        self._next_child_sort_key += 1
        return self._next_child_sort_key

    @setter
    def tags(self, tags):
        """Keyword tags as a :class:`~.model.tags.Tags` object."""
        return Tags(tags)

    @setter
    def keywords(self, keywords):
        """Child keywords as a :class:`~.Keywords` object."""
        return Keywords(self.keyword_class or self.__class__, self, keywords)

    @setter
    def messages(self, messages):
        """Messages as a :class:`~.model.message.Messages` object."""
        return Messages(self.message_class, self, messages)

    @property
    def children(self):
        """Child :attr:`keywords` and :attr:`messages` in creation order."""
        # It would be cleaner to store keywords/messages in same `children`
        # list and turn `keywords` and `messages` to properties that pick items
        # from it. That would require bigger changes to the model, though.
        return sorted(chain(self.keywords, self.messages),
                      key=attrgetter('_sort_key'))

    @property
    def id(self):
        """Keyword id in format like ``s1-t3-k1``.

        See :attr:`TestSuite.id <robot.model.testsuite.TestSuite.id>` for
        more information.
        """
        if not self.parent:
            return 'k1'
        return '%s-k%d' % (self.parent.id, self.parent.keywords.index(self)+1)

    def visit(self, visitor):
        """:mod:`Visitor interface <robot.model.visitor>` entry-point."""
        visitor.visit_keyword(self)

class Keywords(ItemList):
    """A list-like object representing keywords in a suite, a test or a keyword.

    Possible setup and teardown keywords are directly available as
    :attr:`setup` and :attr:`teardown` attributes.
    """
    __slots__ = []

    def __init__(self, keyword_class=Keyword, parent=None, keywords=None):
        ItemList.__init__(self, keyword_class, {'parent': parent}, keywords)

    @property
    def setup(self):
        """Keyword used as the setup or ``None`` if no setup."""
        return self[0] if (self and self[0].type == 'setup') else None

    @property
    def teardown(self):
        """Keyword used as the teardown or ``None`` if no teardown."""
        return self[-1] if (self and self[-1].type == 'teardown') else None

    @property
    def all(self):
        """Iterates over all keywords, including setup and teardown."""
        return self

    @property
    def normal(self):
        """Iterates over normal keywords, omitting setup and teardown."""
        kws = [kw for kw in self if kw.type not in ('setup', 'teardown')]
        return Keywords(self._item_class, self._common_attrs['parent'], kws)

    def __setitem__(self, index, item):
        old = self[index]
        ItemList.__setitem__(self, index, item)
        self[index]._sort_key = old._sort_key


class TestCase(ModelObject):
    """Base model for a single test case.

    Extended by :class:`robot.running.model.TestCase` and
    :class:`robot.result.model.TestCase`.
    """
    __slots__ = ['parent', 'name', 'doc', 'timeout']
    keyword_class = Keyword  #: Internal usage only

    def __init__(self, name='', doc='', tags=None, timeout=None):
        self.parent = None      #: Parent suite.
        self.name = name        #: Test case name.
        self.doc = doc          #: Test case documentation.
        self.timeout = timeout  #: Test case timeout.
        self.tags = tags
        self.keywords = None

    @setter
    def tags(self, tags):
        """Test tags as a :class:`~.model.tags.Tags` object."""
        return Tags(tags)

    @setter
    def keywords(self, keywords):
        """Keywords as a :class:`~.Keywords` object.

        Contains also possible setup and teardown keywords.
        """
        return Keywords(self.keyword_class, self, keywords)

    @property
    def id(self):
        """Test case id in format like ``s1-t3``.

        See :attr:`TestSuite.id <robot.model.testsuite.TestSuite.id>` for
        more information.
        """
        if not self.parent:
            return 't1'
        return '%s-t%d' % (self.parent.id, self.parent.tests.index(self)+1)

    @property
    def longname(self):
        """Test name prefixed with the long name of the parent suite."""
        if not self.parent:
            return self.name
        return '%s.%s' % (self.parent.longname, self.name)

    def visit(self, visitor):
        """:mod:`Visitor interface <robot.model.visitor>` entry-point."""
        visitor.visit_test(self)

class TestCases(ItemList):
    __slots__ = []

    def __init__(self, test_class=TestCase, parent=None, tests=None):
        ItemList.__init__(self, test_class, {'parent': parent}, tests)

    def _check_type_and_set_attrs(self, *tests):
        tests = ItemList._check_type_and_set_attrs(self, *tests)
        for test in tests:
            for visitor in test.parent._visitors:
                test.visit(visitor)
        return tests


class TestSuite(ModelObject):
    """Base model for single suite.

    Extended by :class:`robot.running.model.TestSuite` and
    :class:`robot.result.model.TestSuite`.
    """
    __slots__ = ['parent', 'source', '_name', 'doc', '_my_visitors']
    test_class = TestCase    #: Internal usage only.
    keyword_class = Keyword  #: Internal usage only.

    def __init__(self, name='', doc='', metadata=None, source=None):
        self.parent = None  #: Parent suite. ``None`` with the root suite.
        self._name = name
        self.doc = doc  #: Test suite documentation.
        self.metadata = metadata
        self.source = source  #: Path to the source file or directory.
        self.suites = None
        self.tests = None
        self.keywords = None
        self._my_visitors = []

    @property
    def _visitors(self):
        parent_visitors = self.parent._visitors if self.parent else []
        return self._my_visitors + parent_visitors

    @property
    def name(self):
        """Test suite name. If not set, constructed from child suite names."""
        return self._name or ' & '.join(s.name for s in self.suites)

    @name.setter
    def name(self, name):
        self._name = name

    @property
    def longname(self):
        """Suite name prefixed with the long name of the parent suite."""
        if not self.parent:
            return self.name
        return '%s.%s' % (self.parent.longname, self.name)

    @setter
    def metadata(self, metadata):
        """Free test suite metadata as a dictionary."""
        return Metadata(metadata)

    @setter
    def suites(self, suites):
        """Child suites as a :class:`~.TestSuites` object."""
        return TestSuites(self.__class__, self, suites)

    @setter
    def tests(self, tests):
        """Tests as a :class:`~.TestCases` object."""
        return TestCases(self.test_class, self, tests)

    @setter
    def keywords(self, keywords):
        """Suite setup and teardown as a :class:`~.Keywords` object."""
        return Keywords(self.keyword_class, self, keywords)

    @property
    def id(self):
        """An automatically generated unique id.

        The root suite has id ``s1``, its child suites have ids ``s1-s1``,
        ``s1-s2``, ..., their child suites get ids ``s1-s1-s1``, ``s1-s1-s2``,
        ..., ``s1-s2-s1``, ..., and so on.

        The first test in a suite has an id like ``s1-t1``, the second has an
        id ``s1-t2``, and so on. Similarly keywords in suites (setup/teardown)
        and in tests get ids like ``s1-k1``, ``s1-t1-k1``, and ``s1-s4-t2-k5``.
        """
        if not self.parent:
            return 's1'
        return '%s-s%d' % (self.parent.id, self.parent.suites.index(self)+1)

    @property
    def test_count(self):
        """Number of the tests in this suite, recursively."""
        return len(self.tests) + sum(suite.test_count for suite in self.suites)

    def set_tags(self, add=None, remove=None, persist=False):
        """Add and/or remove specified tags to the tests in this suite.

        :param add: Tags to add as a list or, if adding only one,
            as a single string.
        :param remove: Tags to remove as a list or as a single string.
            Can be given as patterns where ``*`` and ``?`` work as wildcards.
        :param persist: Add/remove specified tags also to new tests added
            to this suite in the future.
        """
        setter = TagSetter(add, remove)
        self.visit(setter)
        if persist:
            self._my_visitors.append(setter)

    def filter(self, included_suites=None, included_tests=None,
               included_tags=None, excluded_tags=None):
        """Select test cases and remove others from this suite.

        Parameters have the same semantics as ``--suite``, ``--test``,
        ``--include``, and ``--exclude`` command line options. All of them
        can be given as a list of strings, or when selecting only one, as
        a single string.

        Child suites that contain no tests after filtering are automatically
        removed.

        Example::

            suite.filter(included_tests=['Test 1', '* Example'],
                         included_tags='priority-1')
        """
        self.visit(Filter(included_suites, included_tests,
                          included_tags, excluded_tags))

    def configure(self, **options):
        """A shortcut to configure a suite using one method call.

        :param options: Passed to
            :class:`~robot.model.configurer.SuiteConfigurer` that will then
            set suite attributes, call :meth:`filter`, etc. as needed.
        """
        self.visit(SuiteConfigurer(**options))

    def remove_empty_suites(self):
        """Removes all child suites not containing any tests, recursively."""
        self.visit(EmptySuiteRemover())

    def visit(self, visitor):
        """:mod:`Visitor interface <robot.model.visitor>` entry-point."""
        visitor.visit_suite(self)


class TestSuites(ItemList):
    __slots__ = []

    def __init__(self, suite_class=TestSuite, parent=None, suites=None):
        ItemList.__init__(self, suite_class, {'parent': parent}, suites)
