#  Copyright 2008-2015 Nokia Solutions and Networks
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

from .visitor import SuiteVisitor
from .xmlelementhandlers import XmlElementHandler
from .executionresult import Result
from ConsistentTool.common import is_string
from io import BytesIO

import pdb
_ERROR = 'No valid ElementTree XML parser module found'

try:
    from xml.etree import cElementTree as ET
except ImportError:
    try:
        import cElementTree as ET
    except ImportError:
        try:
            from xml.etree import ElementTree as ET
        except ImportError:
            try:
                from elementtree import ElementTree as ET
            except ImportError:
                raise ImportError(_ERROR)


class ETSource(object):

    def __init__(self, source):
        self._source = source
        self._opened = None

    def __enter__(self):
        self._opened = self._open_source_if_necessary()
        return self._opened or self._source

    def __exit__(self, exc_type, exc_value, exc_trace):
        if self._opened:
            self._opened.close()

    def __unicode__(self):
        if self._source_is_file_name():
            return self._source
        if hasattr(self._source, 'name'):
            return self._source.name
        return '<in-memory file>'

    def _source_is_file_name(self):
        return is_string(self._source) \
                and not self._source.lstrip().startswith('<')

    def _open_source_if_necessary(self):
        if self._source_is_file_name() or not is_string(self._source):
            return None
        return BytesIO(self._source.encode('UTF-8'))

def ExecutionResult(*sources, **options):
    """Factory method to constructs :class:`~.executionresult.Result` objects.

    :param sources: Path(s) to the XML output file(s).
    :param options: Configuration options.
        Using ``merge=True`` causes multiple results to be combined so that
        tests in the latter results replace the ones in the original. Other
        options are passed directly to the :class:`ExecutionResultBuilder`
        object used internally.
    :returns: :class:`~.executionresult.Result` instance.

    Should be imported by external code via the :mod:`robot.api` package.
    See the :mod:`robot.result` package for a usage example.
    """
    if not sources:
        raise AssertionError('One or more data source needed.')
    return _single_result(sources[0], options)


def _single_result(source, options):
    ets = ETSource(source)
    try:
        return ExecutionResultBuilder(ets, **options).build(Result(source))
    except IOError as err:
        error = err.strerror
    except:
        error = "get_error_message"
    raise AssertionError("Reading XML source failed: %s" % (error))


class ExecutionResultBuilder(object):
    """Builds :class:`~.executionresult.Result` objects based on output files.

    Instead of using this builder directly, it is recommended to use the
    :func:`ExecutionResult` factory method.
    """

    def __init__(self, source, include_keywords=True):
        """
        :param source: Path to the XML output file to build
            :class:`~.executionresult.Result` objects from.
        :param include_keywords: Boolean controlling whether to include
            keyword information in the result or not. Keywords are
            not needed when generating only report.
        """
        self._source = source \
            if isinstance(source, ETSource) else ETSource(source)
        self._include_keywords = include_keywords

    def build(self, result):
        # Parsing is performance optimized. Do not change without profiling!
        handler = XmlElementHandler(result)
        with self._source as source:
            self._parse(source, handler.start, handler.end)
        result.handle_suite_teardown_failures()
        result.handle_suite_setup_failures()
        if not self._include_keywords:
            result.suite.visit(RemoveKeywords())
        return result

    def _parse(self, source, start, end):
        context = ET.iterparse(source, events=('start', 'end'))
        if not self._include_keywords:
            context = self._omit_keywords(context)
        for event, elem in context:
            if event == 'start':
                start(elem)
            else:
                end(elem)
                elem.clear()

    def _omit_keywords(self, context):
        omitted_kws = 0
        for event, elem in context:
            # Teardowns aren't omitted to allow checking suite teardown status.
            omit = elem.tag == 'kw' and elem.get('type') != 'teardown'
            start = event == 'start'
            if omit and start:
                omitted_kws += 1
            if not omitted_kws:
                yield event, elem
            elif not start:
                elem.clear()
            if omit and not start:
                omitted_kws -= 1

    # def _get_matcher(self, matcher_class, flattened):
    #     matcher = matcher_class(flattened)
    #     return matcher.match, bool(matcher)


class RemoveKeywords(SuiteVisitor):

    def start_suite(self, suite):
        suite.keywords = []

    def visit_test(self, test):
        test.keywords = []
