#!/usr/bin/env python
# -*- coding: UTF-8 -*-

import os, re
from xml.etree import ElementTree as ET


def get_all_nodes(tree, path):
    nodes_list = []
    sub_path = path
    while True:
        sub_nodes = tree.findall(sub_path)
        if len(sub_nodes) == 0:
            break
        else:
            nodes_list.extend(sub_nodes)
            sub_path += '/%s' % path
    return nodes_list


def if_match(node, attr_map):
    for key in attr_map:
        if node.get(key) != attr_map.get(key):
            return False
    return True


def get_node_by_attrib(nodelist, attr_map):
    result_nodes = []
    for node in nodelist:
        if if_match(node, attr_map):
            result_nodes.append(node)
    return result_nodes


def change_node_properties(nodelist, attr_map, is_delete=False):
    for node in nodelist:
        for key in attr_map:
            if is_delete:
                if key in node.attrib:
                    del node.attrib[key]
            else:
                # print node.attrib[key]  # CROP for debug
                node.set(key, attr_map.get(key))
                # print node.attrib[key]  # CROP for debug


def get_node_elem_text(nodelist, element, dest_str):
    result_nodes = []
    for node in nodelist:
        element_obj = node.find(element)
        if element_obj is not None:
            element_info = element_obj.text
            if element_info and re.search(dest_str, element_info):
                result_nodes.append(node)
    return result_nodes


def set_node_elem_attrib(nodelist, element, attr_map):
    for node in nodelist:
        element_nodes = node.findall(element)
        if len(element_nodes) != 0:
            change_node_properties(element_nodes,attr_map)


def create_node(tag, property_map, content):
    element = ET.Element(tag, property_map)
    element.text = content
    return element


def add_child_node(nodelist, element):
    for node in nodelist:
        node.append(element)


def write_xml(tree, out_path):
    tree.write(out_path, encoding="utf-8",xml_declaration=True)


def output_handle(input="output.xml",output="output.xml"):
    update_tag = False
    backup_file = os.path.join(os.path.dirname(os.path.abspath(input)), "output_backup.xml")
    try:
        tree = ET.parse(input)
    except Exception, e:
        raise AssertionError("Error: Can not parse xml file: output.xml for Exception: %s" % e)
    suite_list = get_all_nodes(tree, "suite")
    # Handler for teardown failed suites:
    teardown_fail_suites = get_node_elem_text(suite_list, "status", "Suite teardown failed:")
    if len(teardown_fail_suites) != 0:
        update_tag = True
        set_node_elem_attrib(teardown_fail_suites, "status", {"status":"PASS"})
        for matched_suite in teardown_fail_suites:
            matched_kws = matched_suite.findall("kw")
            matched_teardowns = get_node_by_attrib(matched_kws,{"type":"teardown"})
            set_node_elem_attrib(matched_teardowns, "status", {"status":"PASS"})

    # Handler for setup failed suites:
    setup_fail_suites = get_node_elem_text(suite_list, "status", "Suite setup failed:")
    if len(setup_fail_suites) != 0:
        update_tag = True
        # tags_nod = create_node("tags", {}, "")
        # tag_node = create_node("tag", {}, "Setup_Fail")
        # tags_nod.append(tag_node)
        for matched_suite in setup_fail_suites:
            matched_tests = matched_suite.findall("test")
            set_node_elem_attrib(matched_tests, "status", {"status": "NOT_RUN"})
            # set_node_elem_attrib(matched_tests, "status", {"critical": "no"})
            # # Add tag for NOT_RUN cases
            # for test in matched_tests:
            #     test_tags = test.find("tags")
            #     if test_tags is None:
            #         test.append(tags_nod)
            #     else:
            #         test_tags.append(tag_node)
    if update_tag:
        write_xml(tree, output)
        os.rename(input, backup_file)


def search_node_by_elem_attr(node, element, attr_map):
    res = False
    element_obj = node.find(element)
    for key in attr_map:
        if element_obj is not None:
            value = element_obj.attrib[key]
            # if value and value.upper() == attr_map[key].upper():
            if value.upper() == attr_map[key].upper():
                res = True
            else:
                res = False
                break
    if res:
        return node
    else:
        return None


def get_parent_node(tree):
    parent_map = dict((c, p) for p in tree.getiterator() for c in p)
    return parent_map


def get_fail_output(input="output.xml", output="fail_output.xml"):
    try:
        tree = ET.parse(input)
    except Exception, e:
        raise AssertionError("Error: Can not parse xml file: output.xml for Exception: %s" % e)
    pass_num = 0
    tests_num = 0
    parent_map = get_parent_node(tree)
    suite_list = get_all_nodes(tree, "suite")
    for suite in suite_list:
        tests = suite.findall("test")
        tests_cn = len(tests)
        if tests_cn != 0:
            tests_num += tests_cn
            fail_cn = 0
            for test in tests:
                pass_test = search_node_by_elem_attr(test, "status", {"status":"PASS"})
                if pass_test is not None:
                    suite.remove(pass_test)
                    pass_num += 1
                else:
                    fail_cn += 1
            # Check and delete all empty suites after cases filtered
            if fail_cn == 0:
                parent_suite = parent_map[suite]
                parent_suite.remove(suite)
                if not parent_suite.findall('suite'):
                    parent_map[parent_suite].remove(parent_suite)
    if tests_num != pass_num:
        write_xml(tree, output)
    else:
        return None
    return os.path.join(os.path.abspath(os.getcwd()), output)
