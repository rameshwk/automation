#  Copyright 2008-2015 Nokia Solutions and Networks
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

import re
from itertools import chain
from .visitor import SuiteVisitor
from ConsistentTool.common import NormalizedDict
from .stats import CombinedTagStat, CriticalTagStat, TagStat, SuiteStat, TotalStat
from .tags import SingleTagPattern, TagPatterns


class Statistics(object):
    """Container for total, suite and tag statistics.

    Accepted parameters have the same semantics as the matching command line
    options.
    """
    def __init__(self, suite, suite_stat_level=-1, tag_stat_include=None,
                 tag_stat_exclude=None, tag_stat_combine=None, tag_doc=None,
                 tag_stat_link=None):
        total_builder = TotalStatisticsBuilder()
        suite_builder = SuiteStatisticsBuilder(suite_stat_level)
        tag_builder = TagStatisticsBuilder(suite.criticality, tag_stat_include,
                                           tag_stat_exclude, tag_stat_combine,
                                           tag_doc, tag_stat_link)
        suite.visit(StatisticsBuilder(total_builder, suite_builder, tag_builder))
        #: Instance of :class:`~robot.model.totalstatistics.TotalStatistics`.
        self.total = total_builder.stats
        #: Instance of :class:`~robot.model.suitestatistics.SuiteStatistics`.
        self.suite = suite_builder.stats
        #: Instance of :class:`~robot.model.tagstatistics.TagStatistics`.
        self.tags = tag_builder.stats

    def visit(self, visitor):
        visitor.visit_statistics(self)


class StatisticsBuilder(SuiteVisitor):

    def __init__(self, total_builder, suite_builder, tag_builder):
        self._total_builder = total_builder
        self._suite_builder = suite_builder
        self._tag_builder = tag_builder

    def start_suite(self, suite):
        self._suite_builder.start_suite(suite)

    def end_suite(self, suite):
        self._suite_builder.end_suite()

    def visit_test(self, test):
        self._total_builder.add_test(test)
        self._suite_builder.add_test(test)
        self._tag_builder.add_test(test)

    def visit_keyword(self, kw):
        pass


class TagStatistics(object):
    """Container for tag statistics."""

    def __init__(self, critical_stats, non_critical_stats, combined_stats):
        #: Dictionary, where key is the name of the tag as a string and value
        #: is an instance of :class:`~robot.model.stats.TagStat`.
        self.tags = NormalizedDict(ignore='_')
        #: List of :class:`~robot.model.stats.CriticalTagStat` objects.
        self.critical = critical_stats
        #: List of :class:`~robot.model.stats.CriticalTagStat` objects.
        self.non_critical = non_critical_stats
        #: List of :class:`~robot.model.stats.CombinedTagStat` objects.
        self.combined = combined_stats

    def visit(self, visitor):
        visitor.visit_tag_statistics(self)

    def __iter__(self):
        crits = self._get_critical_and_non_critical_matcher()
        tags = [t for t in self.tags.values() if t.name not in crits]
        return iter(sorted(chain(self.critical, self.non_critical,
                                 self.combined, tags)))

    def _get_critical_and_non_critical_matcher(self):
        crits = [stat for stat in self.critical + self.non_critical
                 if isinstance(stat.pattern, SingleTagPattern)]
        return NormalizedDict([(unicode(stat.pattern), None) for stat in crits],
                              ignore='_')


class TagStatisticsBuilder(object):

    def __init__(self, criticality=None, included=None, excluded=None,
                 combined=None, docs=None, links=None):
        self._included = TagPatterns(included)
        self._excluded = TagPatterns(excluded)
        self._info = TagStatInfo(docs, links)
        self.stats = TagStatistics(
            self._info.get_critical_stats(criticality),
            self._info.get_critical_stats(criticality, critical=False),
            self._info.get_combined_stats(combined)
        )

    def add_test(self, test):
        self._add_tags_to_statistics(test)
        self._add_to_critical_and_combined_statistics(test)

    def _add_tags_to_statistics(self, test):
        for tag in test.tags:
            if self._is_included(tag):
                if tag not in self.stats.tags:
                    self.stats.tags[tag] = self._info.get_stat(tag)
                self.stats.tags[tag].add_test(test)

    def _is_included(self, tag):
        if self._included and not self._included.match(tag):
            return False
        return not self._excluded.match(tag)

    def _add_to_critical_and_combined_statistics(self, test):
        stats = self.stats
        for stat in stats.critical + stats.non_critical + stats.combined:
            if stat.match(test.tags):
                stat.add_test(test)


class TagStatInfo(object):

    def __init__(self, docs=None, links=None):
        self._docs = [TagStatDoc(*doc) for doc in docs or []]
        self._links = [TagStatLink(*link) for link in links or []]

    def get_stat(self, tag):
        return TagStat(tag, self.get_doc(tag), self.get_links(tag))

    def get_critical_stats(self, criticality, critical=True):
        if not criticality:
            return []
        tag_patterns = (criticality.critical_tags
                        if critical else criticality.non_critical_tags)
        return [self._get_critical_stat(p, critical) for p in tag_patterns]

    def _get_critical_stat(self, pattern, critical):
        name = unicode(pattern)
        return CriticalTagStat(pattern, name, critical, self.get_doc(name),
                               self.get_links(name))

    def get_combined_stats(self, combined=None):
        return [self._get_combined_stat(*comb) for comb in combined or []]

    def _get_combined_stat(self, pattern, name=None):
        name = name or pattern
        return CombinedTagStat(pattern, name, self.get_doc(name),
                               self.get_links(name))

    def get_doc(self, tag):
        return ' & '.join(doc.text for doc in self._docs if doc.match(tag))

    def get_links(self, tag):
        return [link.get_link(tag) for link in self._links if link.match(tag)]


class TagStatDoc(object):

    def __init__(self, pattern, doc):
        self._matcher = TagPatterns(pattern)
        self.text = doc

    def match(self, tag):
        return self._matcher.match(tag)


class TagStatLink(object):
    _match_pattern_tokenizer = re.compile('(\*|\?+)')

    def __init__(self, pattern, link, title):
        self._regexp = self._get_match_regexp(pattern)
        self._link = link
        self._title = title.replace('_', ' ')

    def match(self, tag):
        return self._regexp.match(tag) is not None

    def get_link(self, tag):
        match = self._regexp.match(tag)
        if not match:
            return None
        link, title = self._replace_groups(self._link, self._title, match)
        return link, title

    def _replace_groups(self, link, title, match):
        for index, group in enumerate(match.groups()):
            placefolder = '%%%d' % (index+1)
            link = link.replace(placefolder, group)
            title = title.replace(placefolder, group)
        return link, title

    def _get_match_regexp(self, pattern):
        pattern = '^%s$' % ''.join(self._yield_match_pattern(pattern))
        return re.compile(pattern, re.IGNORECASE)

    def _yield_match_pattern(self, pattern):
        for token in self._match_pattern_tokenizer.split(pattern):
            if token.startswith('?'):
                yield '(%s)' % ('.'*len(token))
            elif token == '*':
                yield '(.*)'
            else:
                yield re.escape(token)


class SuiteStatistics(object):
    """Container for suite statistics."""

    def __init__(self, suite):
        #: Instance of :class:`~robot.model.stats.SuiteStat`.
        self.stat = SuiteStat(suite)
        #: List of :class:`~robot.model.testsuite.TestSuite` objects.
        self.suites = []

    def visit(self, visitor):
        visitor.visit_suite_statistics(self)

    def __iter__(self):
        yield self.stat
        for child in self.suites:
            for stat in child:
                yield stat


class SuiteStatisticsBuilder(object):

    def __init__(self, suite_stat_level):
        self._suite_stat_level = suite_stat_level
        self._stats_stack = []
        self.stats = None

    @property
    def current(self):
        return self._stats_stack[-1] if self._stats_stack else None

    def start_suite(self, suite):
        self._stats_stack.append(SuiteStatistics(suite))
        if self.stats is None:
            self.stats = self.current

    def add_test(self, test):
        self.current.stat.add_test(test)

    def end_suite(self):
        stats = self._stats_stack.pop()
        if self.current:
            self.current.stat.add_stat(stats.stat)
            if self._is_child_included():
                self.current.suites.append(stats)

    def _is_child_included(self):
        return self._include_all_levels() or self._below_threshold()

    def _include_all_levels(self):
        return self._suite_stat_level == -1

    def _below_threshold(self):
        return len(self._stats_stack) < self._suite_stat_level


class TotalStatistics(object):
    """Container for total statistics."""

    def __init__(self):
        #: Instance of :class:`~robot.model.stats.TotalStat` for critical tests.
        self.critical = TotalStat('Critical Tests')
        #: Instance of :class:`~robot.model.stats.TotalStat` for all the tests.
        self.all = TotalStat('All Tests')

    def visit(self, visitor):
        visitor.visit_total_statistics(self)

    def __iter__(self):
        return iter([self.critical, self.all])

    @property
    def message(self):
        """String representation of the statistics.

        For example::

            2 critical tests, 1 passed, 1 failed
            2 tests total, 1 passed, 1 failed
        """
        ctotal, cend, cpass, cfail = self._get_counts(self.critical)
        atotal, aend, apass, afail = self._get_counts(self.all)
        return ('%d critical test%s, %d passed, %d failed\n'
                '%d test%s total, %d passed, %d failed'
                % (ctotal, cend, cpass, cfail, atotal, aend, apass, afail))

    def _get_counts(self, stat):
        ending = 's' if stat.total != 1 else ''
        return stat.total, ending, stat.passed, stat.failed


class TotalStatisticsBuilder(SuiteVisitor):

    def __init__(self, suite=None):
        self.stats = TotalStatistics()
        if suite:
            suite.visit(self)

    def add_test(self, test):
        self.stats.all.add_test(test)
        if test.critical:
            self.stats.critical.add_test(test)

    def visit_test(self, test):
        self.add_test(test)

    def visit_keyword(self, kw):
        pass