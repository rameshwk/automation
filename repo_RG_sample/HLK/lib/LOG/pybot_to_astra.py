#!/usr/bin/env python

import os,sys,pprint,itertools,re,commands,shutil,time,urllib2,logging

"""
  This script is responsible for a consolidated single tms file (for both APME & Robot) to update in ABC page.   

  http://aww.sh.bel.alcatel.be/tools/dslam/bm/buildlog/cgi-bin/TI_filter.cgi
  http://aww.sh.bel.alcatel.be/tools/Logs/ETSI/ATCSuppressed.txt
  http://aww.sh.bel.alcatel.be/tools/Logs/ETSI/ATCSuspended.txt
"""
def update_test_run_time(line_with_status):

    #sTestCaseRunTime = re.search(' endtime="(.*?)"( | critical="\w+" )starttime=\"(.*)\">',line_with_status)
    #sParseStart = sTestCaseRunTime.group(3).split(" ")
    sParseStart = re.search(' starttime="(.*?)"',line_with_status).group(1).split(" ")
    sStartTime = sParseStart[1]
    sStartDay =  sParseStart[0]
    #sParseEnd = sTestCaseRunTime.group(1).split(" ")   
    sParseEnd = re.search(' endtime="(.*?)"',line_with_status).group(1).split(" ") 
    sEndTime = sParseEnd[1]
    sEndDay = sParseEnd[0]
    fGapDaySecs = (float(sEndDay) - float(sStartDay)) * 86400
    count = 0

    for temp in sStartTime,sEndTime:
        count += 1
        lTimeParts = [float(temp1) for temp1 in temp.split(':')]
        fTotalSecs = (lTimeParts[0] * 60 + lTimeParts[1]) * 60 + lTimeParts[2]
        if count == 1:
            fStartTime_secs = fTotalSecs
    fTotalSecs = fTotalSecs + fGapDaySecs
    fTestCaseTime_sec = fTotalSecs - fStartTime_secs
    fTestCaseTime_sec = round(fTestCaseTime_sec,0)
    totalSecs, sec = divmod(fTestCaseTime_sec, 60)
    hr, min = divmod(totalSecs, 60)
    run_time =  "%02d:%02d:%02d" % (hr, min, sec)
    return run_time

def parse_supress_file (release,lt,platform_type="TARGET",shelf=None,test_file=None,url="http://aww.sh.bel.alcatel.be/tools/Logs/ETSI/ATCSuppressed.txt") :
    """
    return like [("ATC1_NAME,ATC5_NAME","ALU000000"),("ATC_2","ALU00003334")]
    """
 
    release_regex = "R" + release.replace(".","\.")     
    if lt == "" :
        lt_regex = "(\S+)"
    else :
        lt_regex = "(x|X|"+lt+")"
    platform_type_regex = "(x|X|"+platform_type+")"
    shelf_regex = "(x|X|"+shelf+")"
    supress_list = []
    try:
        with open(test_file,'r') as fileHandler:
            test_case_list = fileHandler.readlines()
    except Exception as inst :
        test_case_list = None
 
    try :
        webPage = urllib2.urlopen(url, timeout=10)
    except Exception as inst : 
        print "  --No supress source file in '%s'" % url
        return supress_list	        

    txt_content = webPage.read() 
    for line in txt_content.split("\n") :
        m = re.search ("(.*)\s+"+release_regex+"\s+(\S+)\s+"+lt_regex+"\s+"+platform_type_regex+"\s+"+shelf_regex,line)

        if not m :
            continue
        else :
            this_test_list = m.group(1).split(',')
            fr = m.group(2)
            for this_test in this_test_list :
                test_name = this_test.strip(" ").lstrip(" ")
                if test_case_list and test_name+"\n" in test_case_list :
                    supress_list.append ((test_name,fr))

    return supress_list

def get_atc_suspend_info (release,lt,suspend_file) :
    """
    return like [("ATC1_NAME","ALU000000"),("ATC_2","ALU00003334")]
    """
    release = "R" + release.lstrip('r').lstrip('R')
    if lt == "" :
        lt_regex = "LT=([^,]+)"
    else :
        lt_regex = "LT=(None|"+lt+")"
    suspend_list = []
    try:
        with open(suspend_file,'r') as fileHandler:
            data = fileHandler.readlines()
    except Exception as inst :  
        print ("  --failed to open file '%s',suspend will be skipped. \nexception:'%s' " \
        % (suspend_file,inst))
        return suspend_list
 
    for line in data :
        #RELEASE=R5.7,LT=None,ATC=DPU_ETH_TRAFFIC_14,FR=ALU02361565,SHELF=X
        m = re.search (lt_regex+",ATC=(\S+),FR=([^\s,]+)",line)
        if not m :
            continue
        else :
            test_name = m.group(2)
            fr = m.group(3)
            suspend_list.append ((test_name,fr))

    return suspend_list

def get_release_build_info_from_history_file ():

    #to get the release values from ".apmetoastra.cfg" file 
    sUserPath=commands.getoutput('echo $HOME')
    sPath_apmetoastraCfg=sUserPath+"/.apmetoastra.cfg"
    file_release=open(sPath_apmetoastraCfg,"r")
    lReleaseData=file_release.readlines()
    for lLine in lReleaseData:
	if "set aConfig(NE_RLS)" in lLine:
	    release=re.search("set aConfig\(NE_RLS\) \"(.*)\"",lLine)
	    release=release.group(1)
	    continue
	if "set aConfig(NE_SW_BUILD)" in lLine:
	    build=re.search("set aConfig\(NE_SW_BUILD\) \"(.*)\"",lLine)
	    build=build.group(1)
	    break
    file_release.close()
    return (release,build)

def get_info_from_robot_input_file (input_file):

    output_dict = {}
    try:
        file_handle = open(input_file,"r")
        lRoboVariableData = file_handle.readlines()
    except Exception as inst :
        print input_file + " could not be read, exception:" + str(inst)
        return output_dict

    for lLine in lRoboVariableData:
        m = re.search("\s*(\S+)\s*=\s*(\S+)\s*",lLine)
	if m :
            key,value = m.groups()
            output_dict[key] = value
		
    file_handle.close()
    return output_dict

def _get_test_result (need_second_error_info,step_info,test_result,lLine):
    result=test_result
    if "<status" in lLine:
        result = re.search(' status\="(PASS|FAIL)"',lLine)
        if result.group(1)=="PASS" :
            result = "OK;;"	
        else:  
            if re.search("arent suite setup failed:\n",lLine):
                result = "NOT_RUN;;"
            else:
                step_info = step_info.strip(",")
                if step_info != "" :       
                    result = "TI;" + step_info + ":"
                else :
                    result = "TI;0:"
        error_info = re.search("status=\"FAIL\".*?>([^\r\n<]+)",lLine)

        if error_info :     
            this_error = error_info.group(1)
            this_error = this_error.replace("&lt;","<").replace("&gt;",">")
            result = result + this_error.replace(";",".").replace(":"," ") 
            if not re.search("</status>",lLine) :
                 need_second_error_info = True
            else :
                need_second_error_info = False 
                result = result +";"                           
        return (need_second_error_info,result)

    # second error info exist in next line        
    if need_second_error_info :
        second_error_info = re.sub("</status>","",lLine)
        second_error_info = re.sub("\r|\n"," ",second_error_info)
        second_error_info = second_error_info.replace("&lt;","<").replace("&gt;",">")
        
        result = result + second_error_info.replace(";",".").replace(":"," ")
        result = result.rstrip("\.").rstrip(",")
        if re.search("</status>",lLine) :
            need_second_error_info = False
            result = result +";"

    return (need_second_error_info,result)


def parse_xml_file_for_test_result (log_file,xml_file,lt=""):
    """
    parse robot output file output.xml to get test result
    """

    dir_name = os.path.dirname(xml_file)

    test_time = 0
    test_summary_begin = False
    test_case_begin = False
    setup_begin = False
    teardown_begin = False
    step_begin = False
    need_second_error_info = False
    test_result = ""
    test_lines = []
    setup_depth = 0
    teardown_depth = 0

    for lLine in open(xml_file,"r") :
        ##########################################
        # get test case info
        ##########################################
	#to get the test case status (pass/fail)
	
	if test_case_begin :				
            (need_second_error_info,test_result) = _get_test_result (need_second_error_info,step_info,test_result,lLine)		
	    if "<status " in lLine:
                status_line = lLine

            if "</test>" in lLine :
                test_case_begin = False
                test_run_time = update_test_run_time(status_line)                
                test_case_info = test_line.group(1)\
                +";"+lt+";;;;;;"+test_result+test_run_time+";"+log_file+";;"
                test_lines.append(test_case_info)
            
            # get step keyword info
	    #if '<kw ' in lLine and ' type="teardown"' not in lLine and ' type="setup"' not in lLine:
	    if '<kw ' in lLine :
                step_line = re.search ('<kw (.*)name="([^"]+)".*>',lLine)
                step_name = step_line.group(1) ;# not be used till now
                step_begin = True
                step_depth = step_depth + 1
                continue
           
            if step_begin :
                """
                if '<kw ' in lLine :
                    step_depth = step_depth + 1
                """
                if "</kw>" in lLine :
                    step_depth = step_depth - 1
	        if "<status " in lLine:
                    status_line = lLine
                    m = re.search (' status="(PASS|FAIL)"',status_line)
                    step_result = m.group(1)
                if step_depth == 0 :
                    step_begin = False
                    step_run_time = update_test_run_time(status_line)                
                    step_no = step_no + 1
                    if step_result == "FAIL" :
                        step_info = step_info+str(step_no)+","  
	            continue
	    
	# to get the test case nemonic
	if "<test " in lLine and not test_case_begin :
            test_line = re.search("\<test .* name\=\"(.*)\"",lLine)
            test_case_begin = True
            step_depth = 0
            step_no = 0
            step_info = ""
            continue

        ###########################################
        # get setup keyword info
        ###########################################
	if '<kw' in lLine and 'type="setup"' in lLine and not test_case_begin :
            setup_line = re.search (' name="([^"]+)".*',lLine)
            setup_name = setup_line.group(1)
            setup_begin = True
            setup_depth = setup_depth + 1
            continue

        if setup_begin :
            if "<kw " in lLine :
                setup_depth = setup_depth + 1
                continue
            if "</kw>" in lLine :
                setup_depth = setup_depth - 1
	    if "<status " in lLine:
                status_line = lLine
                m = re.search (" status=\"(PASS|FAIL)\" ",status_line)
                if m.group(1)== "PASS" : 
                    setup_result = "OK"
                else :
                    setup_result = "TI"
            if setup_depth == 0 :
                setup_begin = False
                setup_run_time = update_test_run_time(status_line)                
                setup_info = "setup:"+setup_name\
                +";"+lt+";;;;;;"+setup_result+";;"+setup_run_time+";"+log_file+";;"
                test_lines.append(setup_info)                

        ###########################################
        # get teardown keyword info
        ###########################################
	if"<kw " in lLine and 'type="teardown"' in lLine and not test_case_begin :
            teardown_line = re.search (' name="([^"]+)".*>',lLine)
            teardown_name = teardown_line.group(1)
            teardown_begin = True
            teardown_depth = teardown_depth + 1
            continue

        if teardown_begin :
            if "<kw " in lLine :
                teardown_depth = teardown_depth + 1
                continue
            if "</kw>" in lLine :
                teardown_depth = teardown_depth - 1
	    if "<status " in lLine:
                status_line = lLine
                m = re.search (" status=\"(\w+)\" ",status_line)
                if m.group(1)== "PASS" : 
                    teardown_result = "OK"
                else :
                    teardown_result = "TI"
            if teardown_depth == 0 :
                teardown_begin = False
                teardown_run_time = update_test_run_time(status_line)                
                teardown_info = "teardown:"+teardown_name\
                +";"+lt+";;;;;;"+teardown_result+";;"+teardown_run_time+";"+log_file+";;"
                test_lines.append(teardown_info)

	#to get the run time 
	if "<status " in lLine:         
            #<status status="FAIL" endtime="20141118 09:29:42.551" starttime="20141118 09:23:14.051"></status>
            run_time = re.search("endtime=\"(.*)\"\s+starttime=\"(.*)\"",lLine)
	
	if re.search("<total>",lLine):
            test_summary_begin =  True
            continue
			
	if test_summary_begin and re.search(">All Tests<",lLine):
            num = re.search("\<stat fail\=\"(.*)\" pass\=\"(.*)\".*",lLine)
            print "  robot statistic fail test: " + num.group(1)
            print "  robot statistic pass test: " + num.group(2)

            sto = re.search("(\d{4})(\d{2})(\d{2})\s+(\d{2}):(\d{2}):(\d{2})",run_time.group(1))
            sta = re.search("(\d{4})(\d{2})(\d{2})\s+(\d{2}):(\d{2}):(\d{2})",run_time.group(2))

            start_time = time.strptime(sta.group(1)+sta.group(2)+sta.group(3)+" "+\
                         sta.group(4)+":"+sta.group(5)+":"+sta.group(6),"%Y%m%d %H:%M:%S")
            stop_time  = time.strptime(sto.group(1)+sto.group(2)+sto.group(3)+" "+\
                         sto.group(4)+":"+sto.group(5)+":"+sto.group(6),"%Y%m%d %H:%M:%S")
            test_time = int(time.mktime(stop_time) - time.mktime(start_time))
            continue 
        
    return (test_lines,test_time)

def merge_test_lines(suspend_lines,supress_lines,test_lines,test_file) :
    final_lines = []
    for test_res in test_lines :
        supressed = False
        for this_line in supress_lines :
            supressed_test = this_line[0]
            fr = this_line[1]
            if supressed_test in test_res :

                update_res = re.sub (";TI;[^;]+;",";SUP;"+fr+";",test_res)
                final_lines.append (update_res)
                supressed = True
                break
        if not supressed :
            final_lines.append (test_res)
    line_template = test_lines[0]
   
    test_list_order = []

    for thisATC in open(test_file,'r') :
         test_list_order.append(thisATC.strip("\n"))

    for this_line in suspend_lines :
        atc = this_line[0]
        fr =  this_line[1]
        line_list = line_template.split(";")
        
        m = re.search ("(;[^;]+.html.*)",line_template)
        new_line = atc+";"+line_list[1]+";;;;;;SUS;"+fr+";00:00:00"+ m.group(1)
        thisATCIndex = test_list_order.index(atc)

        for thisItem in final_lines :
            m = re.search("([^;]+);",thisItem)
            if "setup:" in m.group(1) or "teardown:" in m.group(1) :
                continue
            else :
                execATC = m.group(1)
            execATCIndex = test_list_order.index(execATC)
            if execATCIndex > thisATCIndex :
                targetIndex = final_lines.index(thisItem)
                final_lines.insert (targetIndex,new_line)
                break
        else :
            final_lines.append (new_line)

    return final_lines


def get_log_and_relative_hw (log_dir,command_file="pybotcmd.txt",log="log.html",output="output.xml",lt="") :
    file_list = []
    try:
        file_handle = open(log_dir+command_file,"r")
    except Exception as inst :
        print "  could not open pybotcmd.txt file"
        file_list.append ("log=log.html,output=output.xml,lt=")
        return file_list
   
    pybot_cmd_data = file_handle.readlines()
    for line in pybot_cmd_data :
        if "pybot " not in line :
            continue
        m1 = re.search ("--log\s+(\S+).*--output\s+(\S+)",line)
        m2 = re.search ("--variable\s+LT:(\S+)",line)
        if m1 :
            log = m1.group(1)
            output = m1.group(2)
        else :
            log = "log.html"
            output = "output.xml"
        if m2 :
            lt = m2.group(1)
        else :
            lt = ""
        file_list.append ("log=%s,output=%s,lt=%s" % (log,output,lt))
            
    return file_list                            

if __name__ == '__main__' :
    """
    need files under input directory :
      robot_variable.txt
      pybotcmd.py
      TEST_CASE.txt
      *_ATC_SUSPEND_FILTERED.txt

    depend on following web to get supress test:
      http://aww.sh.bel.alcatel.be/tools/Logs/ETSI/ATCSuppressed.txt
    """
    if os.path.isdir(sys.argv[1]) :
        input_path = sys.argv[1].rstrip("/") +"/"
    else :
        input_path = os.path.dirname(sys.argv[1]) +"/"

    robot_variable_file = input_path + "robot_variable.txt"

    test_info_dict = get_info_from_robot_input_file (robot_variable_file)
    try :
        shelf = test_info_dict['SHELF']
    except :
        shelf = None  
              
    try :
        sCoverage = test_info_dict['COVERAGE']
    except :
        sCoverage = None
    if sCoverage.upper() == "HOST" :
        platform_type = "HOST"
    else :
        platform_type = "TARGET" 
    try :
        product = test_info_dict['PRODUCT'] 
    except :
        product = ""
    try :
        build = test_info_dict['BUILD'] 
    except :
        build = "999"
    try :
        release = test_info_dict['RELEASE']
    except :
        (release,build) = get_release_build_info_from_history_file ()

    log_files = get_log_and_relative_hw (input_path,command_file="pybotcmd.txt")
    total_test_lines = []
    total_run_time = 0
    output = ''

    for file_info in log_files :
        print "\n  filter: " + file_info
        m = re.search ("log=(\S+),output=(\S+),lt=(.*)",file_info)
        if not m :
            print "  could not parse '%s', will skip this robot run." % file_info
            continue
        log = m.group(1)
        output = m.group(2)
        lt = m.group(3)

        suspend_lines = get_atc_suspend_info (release,lt,input_path+product+"_ATC_SUSPEND_FILTERED.txt")
        print "  suspended ATC:"        
        for atc,fr in suspend_lines :
            print "    "+atc+": "+fr
        supress_lines = parse_supress_file (release,lt,platform_type,shelf,test_file=input_path+"TEST_CASE.txt",\
                        url="http://aww.sh.bel.alcatel.be/tools/Logs/ETSI/ATCSuppressed.txt")
        print "  supressed ATC:"
        for atc,fr in supress_lines :
            print "    "+atc+": "+fr 
       
        test_lines,run_time = parse_xml_file_for_test_result (log,input_path+output,lt)
        test_in_this_log = merge_test_lines(suspend_lines,supress_lines,test_lines,test_file=input_path+"TEST_CASE.txt")

        total_test_lines.extend(test_in_this_log)
        print "  run time : " + str(run_time) + " seconds"
        total_run_time = total_run_time + int(run_time)    

    file_tms = open(input_path+"ATDD_focus.tms","w+")  
    file_tms_precond =  open(input_path+"ATDD_focus_precond.tms","w+")
    for this_line in total_test_lines :
        this_line = release+";"+sCoverage+";;;"+this_line+build+"\n"
        file_tms_precond.write(this_line)
        if not ( re.search(";setup:", this_line) or re.search(";teardown:", this_line)) :
            file_tms.write(this_line)
    file_tms.close()
    file_tms_precond.close()    
    
    file_run_time = open(input_path+"robot_runtime.txt","w+")
    file_run_time.write(str(total_run_time)+"\n")
    file_run_time.close()
    
    try:
        from ConsistentTool import RebotSettings, ResultWriter
        prefix = ''
        postfix = ''
        # Create the log files in temp file to avoid the log files corrupted issue if previous files are covered.
        tmpdir = os.path.join(input_path,'tmp')
        # Loop added for LOOP option and rerunfailed modules with multi output xml files
        for file in os.listdir(input_path):
            if re.search('.*output.*.xml', file):
                if not file.startswith('output'):
                    prefix = file.split('_')[0] + '_'
                if file.endswith('_rerun.xml'):
                    postfix = '_rerun'
                logfile = prefix + 'log' + postfix + '.html'
                faillogfile = prefix + 'fail_log' + postfix + '.html'
                reportfile = prefix + 'report' + postfix + '.html'
                options = {'outputdir': tmpdir, 'log': logfile, 'report': reportfile, 'fail_log':faillogfile}
                # options = {'results': True}
                settings = RebotSettings(options)
                rc = ResultWriter(os.path.join(input_path,file)).write_results(settings)
                if rc < 0:
                    raise AssertionError('No outputs created.')
                else:
                    for file in os.listdir(tmpdir):
                        dst = file.replace('\\tmp\\', '\\')
                        os.rename(os.path.join(tmpdir, file), os.path.join(input_path, dst))
                    os.rmdir(tmpdir)

    except Exception as inst:
        print " No consistent tool found for consistent log/report creating. Exception: %s" % inst

