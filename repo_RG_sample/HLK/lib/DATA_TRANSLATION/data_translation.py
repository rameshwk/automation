"""
  convert setup info in ltb file and .csv file to global table

  a adaption layer to finish the data format translation.\
  so that the other functions can free from detail dealing with setup data and format.
  check global table was provided too.
"""

import re, sys, os, inspect
#from robot.api import logger
         
def create_global_tables(SetupFile="None", YamlFile="None"): 
    """
    to convert setup data in relative .csv or .yaml file to global dictionaries or table,
    update the the global table based on input ltb data
    Also could support format conversion from csv to yaml
    - *SetupFile:* the path of relative .csv or .yaml file
    
    Usage Example:
    | create global tables | %{SETUPFILENAME} | %{YamlFileName}
    """

    if SetupFile == "None":
        SetupFile = os.environ["SETUPFILENAME"]

    if not os.path.isfile(SetupFile) :
        raise AssertionError("%s:%s -> %s is not a file!" \
        % (__name__,inspect.stack()[0][3],str(SetupFile)))
    if SetupFile.endswith('.yaml'):
        import yaml
        yamldic = {}
        with open(SetupFile,'r') as f:
            yamldic = yaml.load(f)
            pop_list=parse_dict_pop(yamldic,[])
            for attr in pop_list:
                yamldic.update(attr)

            for item in yamldic.keys():
                globals()[item] = yamldic[item]
                #print "%s: %s" % (item, globals()[item])
    else:
        CsvData = _strip_csv_data(SetupFile)
        yamldic = {}
        TableStart = 'No'

        for line in CsvData:
            if TableStart == 'No':
                CheckTableStart = re.search('TABLE (.*) Start', line)
                if CheckTableStart:
                    TableStart = 'Yes'
                    TableName = CheckTableStart.group(1)
                    TableRow = []
            else:
                CheckTableEnd = re.search('TABLE (.*) End', line)
                if CheckTableEnd:
                    TableStart = 'No'
                    TableDict = _list_to_dict_convert(TableRow)
                    TableNameDict = TableName
                    if YamlFile == "None":
                        globals()[TableNameDict] = TableDict
                    #print "%s: %s" % (TableName,globals()[TableNameDict])
                    else:
                        yamldic[TableNameDict] = TableDict
                else:
                    TableRow.append(line)
        if YamlFile != "None":
            import yaml
            with open(YamlFile, 'w') as f:
                yaml.dump(yamldic, stream=f)
    return

def get_table_names(SetupFile):
    table_names=[]
    if SetupFile == "None":
        SetupFile = os.environ["SETUPFILENAME"]

    if not os.path.isfile(SetupFile) :
        raise AssertionError("%s:%s -> %s is not a file!" \
        % (__name__,inspect.stack()[0][3],str(SetupFile)))
    if SetupFile.endswith('.yaml'):
        import yaml
        yamldic = {}
        with open(SetupFile,'r') as f:
            yamldic = yaml.load(f)
            pop_list=parse_dict_pop(yamldic,[])
            for attr in pop_list:
                yamldic.update(attr)
            table_names = yamldic.keys()
    else:
        CsvData = _strip_csv_data(SetupFile)
        for line in CsvData:
            CheckTableStart = re.search('TABLE (.*) Start', line)
            if CheckTableStart:
                TableName = CheckTableStart.group(1)
                table_names.append(TableName)
    return table_names

def print_global_table(table_name):
    """
    print relative global table info
    - *table_name:* the table name

    Usage Example: 
    | print_global_table | ServerTableData |  

    """
    print "Table Name: ", table_name
    print globals()[table_name]
    return

def get_global_table(table_name):
    """
    get the content of the input table
    - *table_name:* the table name
    - *RETURN_VALUE:* the content of the given table 

    Usage Example:     
    | ${table_info} | get_global_table | ServerTableData | 
 
    """
    try :
        print "table %s: %s" % (table_name,str(globals()[table_name]))
        return globals()[table_name]
    except KeyError :
        print "No table named %s found" % table_name
        return {}
      

def set_global_table(table_name, data):
    """
    set data to global variable table_name
    - *table_name:* the table name
    - *data: * the data will be set for global variable table_name   

    Usage Example:
    | set global table | table_name="ONTData" | data={"AidCli":"1/1/8"} |    
    """
    globals()[table_name] = data


def update_global_table(table_name,data,line_num=None):
    """
    update or add columns for table according to table_content
        
    if no table_name global variable found, it will be added   
   
    - *table_name:* the table name
    - *data: * the data will be set for global variable table_name   
    - *line_num:* the line num of updated table

    Usage Example:
    | update_global_table | table_name=ONTData | data={"AidCli":"1/1/8"} |    
    """
    try :
        type(globals()[table_name])
    except NameError :
        print "No table named %s found, it will be added" % table_name
        globals()[table_name].append(data)
        return
        
    if line_num or line_num == 0:
        globals()[table_name][int(line_num)].update(data)
    else :
        for line in globals()[table_name]:
            line.update(data)
            
    return

def get_table_lines (table_name,search_condition=None):
    """
    get specific lines with search condition and return matched list 
    - *table_name:* name of the table
    - *search_condition:* a string with format "key1=value1,key2=value2", default is None
    
    Usage Example:
    | ${line_list} | get_table_lines | SwitchVlanMapData |
    
    """

    try :
        totalTableList = globals()[table_name]
    except Exception as inst:
        #print "can not get table name %s, exception: %s" % (table_name,inst)
        return []
    

    if not search_condition :
        return totalTableList
     
    matched_lines = []
                
    conditionList = search_condition.split(',')
    for lineDict in totalTableList :

        for condition in conditionList :
            try:
                key,value = condition.split('=',1)
            except IndexError :
                raise AssertionError("%s-> \
                seach_condition format error for '%s'" \
                % (__name__,condition))
            if not (lineDict.has_key(key) and re.search(value,lineDict[key])):
                lineMatch = False
                break  # break the inner condition loop          
        else:
            matched_lines.append(lineDict)
            
    return matched_lines

def get_table_line (table_name,search_condition=None,matched_line_index=None):
    """
    get specific line with search condition and No. of matched lines from table 
    - *table_name:* name of the table
    - *search_condition:* a string with format "key1=value1,key2=value2", default is None
    - *matched_line_index:* if there are no matched_line_index, return the first line of all matched,
        or return the required line 
    - *RETURN_VALUE:* the info of matched table line.
    
    Usage Example:
    | ${line_info} | get_table_line | SwitchVlanMapData | matched_line_index=0 |
    """

    try :
        totalTableList = globals()[table_name]
    except Exception as inst:
        print "can not get table name %s, exception: %s" % (table_name,inst)
        return {}
    
    if not matched_line_index :
        matched_line_index = 0
    else :
        matched_line_index = int(matched_line_index)

    if not search_condition :
        return totalTableList[matched_line_index]
     
    matched_lines = []
                
    conditionList = search_condition.split(',')
    for lineDict in totalTableList :

        for condition in conditionList :
            try:
                key,value = condition.split('=',1)
            except IndexError :
                raise AssertionError("%s-> \
                seach_condition format error for '%s'" \
                % (__name__,condition))
            if not (lineDict.has_key(key) and re.search(value,lineDict[key])):
                lineMatch = False
                break  # break the inner condition loop          
        else:
            matched_lines.append(lineDict)
            lineMatch = True
            
    if len(matched_lines) > matched_line_index :
        return matched_lines[matched_line_index]
    else :
        msg = "no any line matched the search condition '"\
        +search_condition+"' in table "+table_name
        #print msg
        return {}
        
  
def _strip_csv_data(csvfile):
    with open(csvfile, 'rb') as f:
        lContent = f.readlines()
        ProcessedCsvData = []
        for line in lContent:
            line = re.sub('([,]+([\\r]|[\\n])+|([\\r]|[\\n])+)','',line)
            if len(line) > 0 :
                ProcessedCsvData.append(line)
    return ProcessedCsvData

   
def _list_to_dict_convert(ListTable):
    lstdata = ListTable
    outputarr = []

    headerstr = lstdata[0]
    headerlst = headerstr.split(',')           
    headerlstlen = len(headerlst)
    lenlstdata = len(lstdata)
    i = 1
    while i < lenlstdata:
        tmp = re.sub(r'".*?,.*?"',lambda m:m.group(0).replace(",","@@$$@@"),lstdata[i]).split(',')
        tmparr = "@@##@@".join(tmp).replace("@@$$@@",",").split("@@##@@")
       
        if headerlstlen != len(tmparr) :
            print "data gotten can not map columns:"+str(tmparr)
            i += 1
            continue      
        tmpdict = {}
        j = 0
        while j < headerlstlen:
            jkey = headerlst[j]
            jval = tmparr[j]
            tmpdict[jkey] = jval
            j += 1 
        outputarr.append(tmpdict)
        i += 1
    return outputarr


def _grand_dict_pop(setup_dic):
  pop_list=[]
  merge_dic={}
  try:
    if isinstance(setup_dic, dict):
      for key in setup_dic.keys():
        son=setup_dic[key]
        if isinstance(son, dict):
          for skey in son.keys():
            grand=son[skey]
            if isinstance(grand, dict) or isinstance(grand, list):
              pop_list.append({skey:son.pop(skey)})
        else:
          for item in son:
            if isinstance(item, dict):
              for skey in item.keys():
                grand=item[skey]
                if isinstance(grand, dict) or isinstance(grand, list):
                  #print "PROMPTED Dic: %s\n" % item.pop(skey)
                  pop_list.append({skey:item.pop(skey)})
    if pop_list != []:
      for pop_dic in pop_list:
        for k, v in pop_dic.items():
          if isinstance(v,list):
            merge_dic.setdefault(k,[]).extend(v)
          elif isinstance(v, dict):
            merge_dic.setdefault(k,[]).append(v)
  except Exception as inst:
    raise AssertionError("%s-> fail to parse the dict by exception: %s" % (__name__,inst))
  return merge_dic


def _is3Ddict(setup_dic):
  res=False
  try:
    if isinstance(setup_dic, dict):
      for key in setup_dic.keys():
        son=setup_dic[key]
        if isinstance(son, dict):
          for skey in son.keys():
            grand=son[skey]
            if isinstance(grand, dict) or isinstance(grand, list):
              res=True
        else:
          for item in son:
            if isinstance(item, dict):
              for skey in item.keys():
                grand=item[skey]
                if isinstance(grand, dict) or isinstance(grand, list):
                  res=True
  except Exception as inst:
    raise AssertionError("%s-> fail to check dict by exception: %s" % (__name__,inst))
  return res


def parse_dict_pop(setup_dic,pop_dics):
  pop_dic = _grand_dict_pop(setup_dic)
  if _is3Ddict(pop_dic):
    sub_pop = parse_dict_pop(pop_dic,pop_dics)
  pop_dics.append(pop_dic)
  return pop_dics

