import os,re,sys,time
import telnetlib
import datetime
from robot.api import logger
from robot.libraries.BuiltIn import BuiltIn

try :
    import pexpect
except ImportError :
    sys.path.append(os.environ.get('ROBOTREPO')+'/TOOLS/pexpect-2.3_1')
    import pexpect

try:
    lib_path = os.environ['ROBOTREPO'] +'/TOOLS/lib/python'
    if lib_path not in sys.path:
        sys.path.append(lib_path)
except Exception as inst:
    raise AssertionError("%s -> Fail to set sys.path,exception: %s" \
    % (__name__,inst))
    
class com_cli(object) :
    def __init__(self,ip='127.0.0.1',port='0',username='isadmin',\
    password='isamcli!',default_password='i$@mad-',prompt='isadmin>',timeout='3600s',command_prompt=None,transport_protocol="telnet") :
        self.hostIp=ip.encode("ascii")
        self.cliPort=port.encode("ascii")
        self.cliUser=username.encode("ascii")
        self.cliPassword=password.encode("ascii")
        self.cliDefPassword=default_password.encode("ascii")
        self.cliPrompt=prompt.encode("ascii")
        self.cliClient=""
        self.cliTimeout=timeout.rstrip("s")
        self.session_alive = False
        if command_prompt :
            self.cmdPrompt= command_prompt.encode("ascii")
        else :
            self.cmdPrompt=self.cliPrompt 

        keyword_name = "__init__"

        if self.hostIp == "127.0.0.1" :
            msg = "IP error! please given correct ip"
            raise AssertionError("%s:%s -> %s" \
            % (__name__,keyword_name,msg))        

        if self.cliPort == '0' and self.transport_protocol == "ssh" :
            self.cliPort = '22'
        elif self.cliPort == '0' :
            self.cliPort = '23'

        connect_info = {}
        connect_info['hostIp'] = self.hostIp
        connect_info['cliPort'] = self.cliPort
        connect_info['cliUser'] = self.cliUser
        connect_info['cliPassword'] = self.cliPassword
        connect_info['cliDefPassword'] = self.cliDefPassword
        connect_info['cliPrompt'] = self.cliPrompt
        connect_info['cliTimeout'] = self.cliTimeout
        connect_info['cmdPrompt'] = self.cmdPrompt
        connect_info['sessionAlive'] = self.session_alive

        if transport_protocol == 'ssh':
            self.cliClient = cliSSHClient(**connect_info)
        else:
            self.cliClient = cliTelnetClient(**connect_info)

    def open_cli(self):
        self.cliClient.open()
        self.session_alive = self.cliClient.session_alive

    def _write_to_session(self,message,timeout=None) :
        logger.debug("write:"+message+"\n")
        self.cliClient.write(message)
        if not timeout :
           timeout = 2 
        returnMsg = self.cliClient.read_until(".*",timeout)
        logger.debug("gotten return:"+returnMsg)
        return returnMsg
                                        
    def _any_item_matched(self,item_list,target_string):
        for item in item_list :
            if item in target_string :
                return True
        else :
            return False
              
                       
    def close_cli(self):
        keyword_name = "close_cli"
        try:
            self.cliClient.write("logout\r\n")
            #logger.info("CLI CMD >> logout")
            logger.info("<span style=\"color: #00008B\">"+"CLI CMD >> logout"+"</span>",html=True)
            returnTmp = self.cliClient.read_until(".*",0.1)  
            logger.info("CLI REPLY << "+returnTmp)
            time.sleep(1)
            self.cliClient.close()
            if self.cliClient.logHandler:
                self.cliClient.logHandler.close()
        except Exception as inst:
            raise AssertionError("%s:%s -> fail to close cli session, exception: %s" \
            % (__name__,keyword_name,inst))
        else:
            return "pass"

    def send_command(self,cli_command,timeout=0,prompt=None):
        keyword_name = "send_command"
        command =cli_command.encode("ascii")
        timeout = int(timeout)
        if not prompt :
            prompt = self.cmdPrompt
        else :
            prompt = prompt.encode("ascii")

        # if user did not assign timeout, use the global CLITIMEOUT
        if timeout == 0:
            try:
                timeout = int(os.environ['CLITIMEOUT'])
            except:
                timeout = 10 

        if not self.session_alive : 
            self.open_cli()
        
        #Cleanup the buffer in try block
        returnTmp = ""
        try:
            returnTmp = self.cliClient.read_until(".*",timeout*1.0/1000) 
        except Exception as inst:
            logger.debug("%s:%s-> need re-open cli \n expection: %s" \
            % (__name__,keyword_name,inst))
            self.open_cli()

        logger.debug ("info in buffer before send cli:\n %s" \
        % (returnTmp))
        self.session_alive = self.cliClient.session_alive
        # Write the command in the telnet session using try block
        try:
            wret = self.cliClient.write(command+"\r")                       
        except Exception as inst:             
            logger.debug("<span style=\"color: #CD4F39\">"+"written: %s\\r, gotten exception: %s" % (command,inst)+"</span>",html=True)          
            try :
                wret = self.cliClient.write(command+"\r")
            except Exception as inst:     
                raise AssertionError("to re-write '%s\\r', gotten exception:\n %s" \
                % (command,inst))              
 
        # Read buffer at least 5 times to capture the command and its output 
        alarmCnt = 0 
        readBuffer = ""
        chkcmd = re.sub('\s+','',command)
        chkcmd = re.sub('\?','',chkcmd)
        i = 0
        for readBufferCnt in range (5) :
            try:             
                returnTmp = self.cliClient.read_until(prompt,timeout,command)
            except Exception as inst:
                s=sys.exc_info()
                self.session_alive = False
                raise AssertionError("to read buffer, line:%s, gotten exception: '%s'" % (s[2].tb_lineno,inst)) 
            logger.debug ("gotten: %s" % returnTmp)
            readBuffer = readBuffer + returnTmp   
            chkreturn = re.sub('\?','',returnTmp)    
                
            if chkcmd in re.sub('\s+','',chkreturn) and re.search(prompt,returnTmp) :  
                break
            else :
                # Enhance the match pattern to fix FR ALU02410506
                if command.lstrip(" ").startswith("info ") or command.lstrip(" ").startswith("show "):
                    # cleanup the buffer in try block
                    time.sleep(1)
                    self.cliClient.read_until(".*",2)
                    logger.debug("Retry to send cli command, cleanup the buffer with read_until %s time!!" % readBufferCnt)                
                    # send command again
                    self.cliClient.write(command+"\r")
                    self.cliClient.write("\r")                     
                    if ("alarm occurred" in returnTmp) or ("alarm cleared" in returnTmp):
                        alarmCnt= alarmCnt + 1

        if  chkcmd not in re.sub('\s+','',readBuffer) :
             logger.info("<span style=\"color: #FFFF00\">command did not exist in output</span>",html=True)
             logger.debug("output: %s" % str(readBuffer))                         
        if alarmCnt > 4 :        
            raise AssertionError("gotten more than 4 alarm occur" )                             
        if readBuffer == "" :
            self.session_alive = False
            raise AssertionError("send command '%s', gotten no output" % command)
        if not re.search(prompt,readBuffer) :
            raise AssertionError("no expected prompt '%s' after send '%s' \ngotten '%s'" \
            % (prompt,command,readBuffer))

        return readBuffer


    def send_group_command(self,cli_command,prompt_number=1,timeout=0):
    
        command =cli_command.encode("ascii").rstrip('\r')
        timeout = int(timeout)
        prompt = ('.*' + self.cliPrompt) * int(prompt_number)

        timeout = (timeout or  60)

        if not self.session_alive : 
            self.open_cli()

        #1, Cleanup the buffer in try block
        try:            
            returnTmp = self.cliClient.read_until(".*",0.001)
        except Exception as inst:
            logger.debug("need re-open cli \n exception: %s" % inst)
            try :
                open_cli()
            except CliFatalError as inst :
                raise CliFatalError("failed to re-open cli before send group cli\n \
                exception:%s"  % inst) 
        
        #2, send command      
        for i in range(3) :
            try:          
                self.cliClient.write(command+"\r")
            except Exception as inst:
                logger.debug("<span style=\"color: #CD4F39\">"+"written: %s\\r, gotten exception: %s" % (command,inst)+"</span>",html=True)
                logger.debug("to re-open cli session for re-write")  
                self.open_cli()
                logger.debug("re-opened cli session")  
                continue
            else:
                break
        else :
            raise AssertionError("%s-> failed to send command '%s'" \
            % (__name__,command))
              
        #3, get response
        fullResponse = ""
        for i in range (3) :
            try :
                cmdResponse = self.cliClient.read_until(prompt,timeout,command)
            except Exception as inst:
                raise AssertionError("%s-> gotten exception when read buffer: %s" \
                % (__name__,inst))
            else :
                fullResponse = fullResponse + cmdResponse
                                
            if ("alarm occurred" in cmdResponse) or ("alarm cleared" in cmdResponse):
                self.cliClient.write("\r")
            else :
                break
        else :
            logger.info("<span style=\"color: #FFFF00\">\
            gotten more than 3 alarm info during executing "+command+"</span>",\
            html=True) 
           
        return  fullResponse


    def reboot_connect_back (self,max_wait_time) :
            
        returnTmp = ""
        self.session_alive = False
        currentTime = time.mktime(time.localtime())
        endTime = currentTime + max_wait_time
        logger.info ("Will continue to reboot before the end of timeout: %d" % max_wait_time)
        while currentTime <= endTime and self.session_alive == False:         
            time.sleep(5)
            currentTime = time.mktime(time.localtime())
            try:
                self.cliClient.write("\r\n")     
                returnTmp = self.cliClient.read_until(".*",5)         
                logger.debug("while expect 'break':\n write '\\r\\n', gotten '%s'" % returnTmp)                 
                # For cli connect from craft port, no exception occurred when telnet disconnected, below handle is added for it.
                if "CLI(C) or a TL1 login(T)" in returnTmp:
                    logger.debug ("to open cli session after reboot")
                    self.open_cli()
                    return returnTmp
            except Exception as inst:                                                        
                logger.debug ("gotten expected exception for reset\n '%s'" % inst)
                time.sleep(15)
                logger.debug ("to open cli session after reboot")
                self.open_cli()                      
                return returnTmp

        if currentTime >= endTime :
            raise AssertionError("SUT didn't reset in %s seconds" % str(max_wait_time) )

    def capture_cli_log(self,log_file_name,timestamp) :
        if log_file_name:
            self.cliClient.logFileName = log_file_name

        if not timestamp :
            self.cliClient.logTimestamp = "no"
        elif timestamp.lower() == 'no' :
            self.cliClient.logTimestamp = "no"
        else:
            self.cliClient.logTimestamp = "yes"

        logger.info("Save cli log to %s" % log_file_name) 
        self.cliClient.logHandler = open (log_file_name,'w')

    def stop_capture_cli(self) :
        """
        stop capture cli log and return cli log list

        INPUT:  N/A

        RETURN: 
                a list of cli log
        """
        self.cliClient.logTimestamp = "no"
        self.cliClient.logHandler.close()
        self.cliClient.logHandler=None

        lCliLog = [] 
        logger.info("Getting cli log from %s" % self.cliClient.logFileName)

        logHandler = open (self.cliClient.logFileName,'rb')
        lCliLog = logHandler.readlines()
        logHandler.close()

        self.cliClient.logFileName = ""

        slog = ""
        for eachLine in lCliLog:
            slog = slog + str(eachLine)

        return slog

    def connect_status_check(self,timeout=3):
        return self.cliClient.connect_status_check(timeout)


class cliSSHClient ():
    def __init__(self,**connect_info):
        self.ip = connect_info['hostIp'] 
        self.port = connect_info['cliPort'] 
        self.username = connect_info['cliUser'] 
        self.password = connect_info['cliPassword'] 
        self.defPassword = connect_info['cliDefPassword'] 
        self.prompt = connect_info['cliPrompt']
        self.session_alive = connect_info['sessionAlive']
        self.timeout = connect_info['cliTimeout'] 
        self.cmdPrompt = connect_info['cmdPrompt'] 
        self.logFileName = ""
        self.logTimestamp = None
        self.logHandler = ""
      
        #transport and channel are the object variables which will have the transport & channel objects.
        self.interface = ""

    def open(self):

        # ssh -l auto -p 22 135.251.200.132
        cmd_ssh      = "ssh -l "+self.username+" "+self.ip +" -p "+self.port + "\n"
        currentTime      = time.mktime(time.localtime())
        endTime          = currentTime + 10

        self.prompt = ".*>|#|\$|%"
        dict_log         = {}
        dict_log['type'] = 'info'
        fLogin = "FAIL"
        recv = ""
        is_quit = "FAIL"
        is_fatal_error = "FALSE"
 
        while currentTime <= endTime :
            try :
                self.interface = pexpect.spawn(cmd_ssh,timeout=20)
                logger.info(cmd_ssh)
                iPasswordCount = 0
                while 1:
                    recv = ""
                    send_cmd = ""
                    expect_value = self.interface.expect(['[\s\S]+',pexpect.EOF,pexpect.TIMEOUT])
                    if expect_value == 0:
                        recv = self.interface.after
                        logger.info("recv:%s" % recv)
                    else:
                        recv = self.interface.after
                        logger.info("recv timeout:%s" % recv)
                        break

                    # checking return information
                    if "Login" in recv:
                        send_cmd = self.username
                    elif "enter new password" in recv:
                        send_cmd = self.password
                    elif "re-enter new password" in recv:
                        send_cmd = self.password
                    elif "assword:" in recv:
                        send_cmd = self.password
                        if iPasswordCount == 0 :
                            send_cmd = self.password
                            iPasswordCount += 1
                        elif iPasswordCount == 1 :
                            send_cmd = self.defPassword
                            iPasswordCount += 1
                        else :
                            logger.debug("to write '%s', after got 3+ times \
                            of 'password' prompt" % self.password)
                            send_cmd = self.password
                    elif "Are you sure you want to continue connecting" in recv:
                        send_cmd = "yes"
                    elif self.cmdPrompt in recv:
                        fLogin = "PASS"
                        break
                    elif "Permission denied" in recv or "Connection Refused!" in recv or "Unable to connect\
                    " in recv or "closed by remote host" in recv or "Connection closed by" in recv:
                        is_fatal_error = "TRUE"
                        break
                    elif 'Host key verification failed' in recv:
                        if self.port == "22" :
                            cmd = "ssh-keygen -R " + self.ip
                        else :
                            cmd = "ssh-keygen -R [" + self.ip + "]:" + self.port
                        logger.error("exec '" + cmd + "' to clean inconsistent public key" )
                        res = pexpect.run ( cmd )
                        logger.info(str(res))
                        self.interface.close()
                        logger.error("inconsistent public key cleaned, please retry")
                        time.sleep(2)
                        break 
                    #elif '\r' == recv or '\n' == recv or '\r\n' == recv :
                    #    send_cmd = "\r"
                    else:
                        continue 

                    logger.info("send:%s" % send_cmd)
                    self.interface.sendline(send_cmd) 

            except Exception as inst :
                error_log = "Fail to exec 'pexpect.spawn(%s)', exception: %s\n" % (cmd_ssh,inst)
                logger.error(error_log)
                self.interface.close()
                time.sleep(10)
                currentTime = time.mktime(time.localtime())
                continue
            else :
                logger.info(self.interface.after )

            if fLogin == "PASS":
                self.session_alive = True
                break
                
            if is_fatal_error == "TRUE":
                break

            time.sleep(2)
            currentTime = time.mktime(time.localtime())
        #end while
        if fLogin == "FAIL":
            logger.error("raise assertion error with received info to :%s" % recv)
            self.session_alive = False
            raise AssertionError ("fatal to ssh CLI:%s\n" % recv)     

    def close(self):
        self.interface.terminate() # to avoid gui pop up when close()
        self.interface.close()

    def write(self,cmd):
        self.interface.sendline(cmd)

    def write_cli_log_to_file(self,result):
        if result == '\r' or result == '\n' or result == '\r\n':
            return

        if self.logHandler:
            if self.logTimestamp != "no":
                now = datetime.datetime.now() 
                self.logHandler.writelines(str(now) + '\r\n')

            self.logHandler.writelines(result + '\r\n' )

    def read_until(self,prompt,timeout=3,command=None):
        prompt_exp     = ""
        cmdVal         = 0
        prompt_exp_all = ""
        prompt_exp_one = '.'
    
        if command:
            if len(command)>21:
                command = command[0:19]
            command = command.strip('\r').strip('\n').strip('\\r').strip('\\n')
            prompt_exp_all = '[\s\S]*' + command + '[\s\S]*' + prompt + '[\s\S]*'
        else :
            # didn't send any command and expect prompt is '.*' means nothing received is OK
            prompt_exp_all = '.*'

        try :
            recv = ""

            cmdVal = self.interface.expect([prompt_exp_all,
            pexpect.EOF,pexpect.TIMEOUT],timeout=int(3))

            if cmdVal > 0:
                logger.debug("recv timeout!!")
                #recv = recv + self.interface.before
               
                while 1:
                    secVal = self.interface.expect(['[\s\S]+',
                    pexpect.EOF,pexpect.TIMEOUT],timeout=int(3))
                    
                    if secVal == 0:
                        data = self.interface.after
                        recv = recv + data
                    elif secVal > 0:
                        logger.debug("recv nothing!!")
                        break
            elif cmdVal == 0:
                data = self.interface.after
                recv = recv + data

            self.write_cli_log_to_file(recv)
            return recv

        except Exception as inst :
            raise AssertionError ("Fail to recv response of cmd '%s', exception: %s" % 
            (prompt_exp, str(inst)))

    def connect_status_check(self,timeout=3):
        currentTime = time.mktime(time.localtime())
        expect_value = 0
        endTime = currentTime + timeout
        while currentTime <= endTime and expect_value == 0:
            time.sleep(.1)
            currentTime = time.mktime(time.localtime())
            try:
                self.interface.sendline("\r\n")
                expect_value = self.interface.expect(pexpect.EOF,timeout=3)
                if expect_value != 0:
                    return True
            except Exception as inst:
                logger.debug ("gotten expected exception for connect status check\n '%s'" % inst)
                return True
        else:
             return False


class cliTelnetClient ():
    def __init__(self,**connect_info):
        self.client = ""
        self.hostIp = connect_info['hostIp']
        self.cliPort = connect_info['cliPort']
        self.cliUser = connect_info['cliUser']
        self.cliPassword = connect_info['cliPassword']
        self.cliDefPassword = connect_info['cliDefPassword']
        self.cliPrompt = connect_info['cliPrompt']
        self.cliTimeout = connect_info['cliTimeout']
        self.cmdPrompt = connect_info['cmdPrompt'] 
        self.session_alive = connect_info['sessionAlive']
        self.logFileName = ""
        self.logTimestamp = None
        self.logHandler = ""

    def _write_to_session(self,message,timeout=None,prompt=".*") :
        logger.debug("write:"+message+"\n")
        self.client.write(message)
        if not timeout :
           timeout = 2
        returnMsg = self.client.read_until(prompt,timeout)
        logger.debug("gotten return:"+returnMsg)
        return returnMsg

    def _any_item_matched(self,item_list,target_string):
        for item in item_list :
            if item in target_string :
                return True
        else :
            return False

    def open(self) :
        keyword_name = "open_cli"
        re_connectTime = 0
        iPasswordCount = 0
        non_recover_error = ["No such user","error",
        "Connection Refused!","Unable to connect to remote host",
        "Internal processing error","This terminal is locked for ",
        "Connection reset by peer"]

        # if cli login failed in 1 hour, raise fatal error to stop robot run
        currentTime = time.mktime(time.localtime())
        endTime = currentTime + int(self.cliTimeout)
        while currentTime <= endTime :
            currentTime = time.mktime(time.localtime())
            try:
                re_connectTime += 1
                self.client = telnetlib.Telnet(self.hostIp,self.cliPort,1)
                logger.info("create telnet: self client:%s" % self.client )
            except Exception as inst:
                logger.debug ("gotten exception for telnetlib.telnet : %s" % inst)
                logger.debug ("cli telnet failed, try it the %d(th) time"  % re_connectTime)
                time.sleep(30)
                continue
            logger.debug("telnet object is self.client=%s" % self.client)

            returnTmp = " "
            try :
                returnTmp = self.client.read_until(".*",5)
            except Exception as inst :
                logger.debug("first read failed for this telnet to %s \n exception:%s" % (self.hostIp,inst))
                if int(self.cliTimeout) < 30:
                    time.sleep(int(self.cliTimeout))
                else:
                    time.sleep(30)
                continue
            logger.debug("get return:%s\n" % returnTmp)
            promptCount = 0
            while self.cliPrompt not in returnTmp and currentTime <= endTime :
                currentTime = time.mktime(time.localtime())
                promptCount = promptCount + 1
                if promptCount > 30 :
                    self.client.close()
                    break
                if "login:" in returnTmp or "username:" in returnTmp :
                    returnTmp = self._write_to_session(self.cliUser+"\r",2)
                    #continue
                elif "enter new password:" in returnTmp :
                    returnTmp = self._write_to_session(self.cliPassword+"\r",3,"e-enter  password:")

                elif "re-enter  password:" in returnTmp :
                    returnTmp = self._write_to_session(self.cliPassword+"\r",2)
                elif "assword:" in returnTmp:
                    if iPasswordCount == 0 :
                        returnTmp = self._write_to_session(self.cliPassword+"\r",2)
                        iPasswordCount += 1
                    elif iPasswordCount == 1 :
                        returnTmp = self._write_to_session(self.cliDefPassword+"\r",3,prompt="new password:")
                        iPasswordCount += 1
                    else :
                        logger.debug("to write '%s', after got 3+ times \
                        of 'password' prompt" % self.cliPassword)
                        returnTmp = self._write_to_session(self.cliPassword+"\r",2)
                elif "CLI(C) or a TL1 login(T)" in returnTmp:
                    returnTmp = self._write_to_session("C\r\n",3)
                    continue
                elif "<" in returnTmp or returnTmp.strip() == "" :
                    time.sleep(2)
                    returnTmp = self._write_to_session("\r\n",3)
                    continue
                elif "Login incorrect" in returnTmp and "Connection closed" in returnTmp :
                    logger.debug("get return:%s\n" % returnTmp)
                    self.client.close()
                    time.sleep(1)
                    raise AssertionError("fail to telnet cli, not gotten expected prompt\n return:"+returnTmp)
                elif self._any_item_matched(non_recover_error,returnTmp) :
                    logger.debug("get return:%s\n" % returnTmp)
                    self.client.close()
                    raise AssertionError ("fatal to telnet CLI\n get error:%s" % returnTmp)
                else:
                    logger.debug("get return:%s\n" % returnTmp)
                    logger.info("<span style=\"color: #FFFF00\">gotten unexpected return</span>",html=True)
                    self.client.close()
                    time.sleep(1)
                    break

            logger.info("CLI REPLY << %s" % returnTmp)

            if self.cliPrompt in returnTmp :
                logger.debug("Telnet CLI success with last prompt: %s"  % returnTmp)
                self.session_alive = True
                #ALU1923183 - Set the terminal-timeout as '0' to avoid the terminal timeout
                sessionRet = self._write_to_session("environment terminal-timeout timeout:0 \n",2)
                logger.debug("gotten return '%s'" % sessionRet)
                return "pass"

            logger.info("CLI REPLY << %s" % returnTmp)
            if self.session_alive == False and re_connectTime > 10:
                self.client.close()
                self.session_alive = False
                raise AssertionError("fail to telnet cli more than 10 times, not gotten expected prompt\n return:"+returnTmp)

        if currentTime >= endTime :
            raise CliFatalError ("%s:%s-> fatal to telnet CLI in %d seconds" \
            % (__name__,keyword_name,int(self.cliTimeout)))

    def close(self):
        self.client.close()

    def write(self,cmd):
        self.client.write(cmd) 

    def read_until(self,prompt,timeout=0,command=None):
        returnTmp = self.client.read_until(prompt,timeout)
        if returnTmp:
            self.write_cli_log_to_file(returnTmp)
        return returnTmp

    def write_cli_log_to_file(self,result):
        if result == '\r' or result == '\n' or result == '\r\n':
            return

        if self.logHandler:
            if self.logTimestamp != "no":
                now = datetime.datetime.now()
                self.logHandler.writelines(str(now) + '\r\n')

            self.logHandler.writelines(result + '\r\n' )

    def connect_status_check(self,timeout=10):
        currentTime = time.mktime(time.localtime())
        connection = False
        endTime = currentTime + timeout
        while currentTime <= endTime:
            time.sleep(.5)
            currentTime = time.mktime(time.localtime())
            try:
                res = self.client.read_until(".*",timeout*1.0/5)
                if res != None:
                    connection = True
                    break
            except Exception as inst:
                logger.debug ("gotten expected exception for connect status check\n '%s'" % inst)
                connection = False 
        return connection


class CliFatalError (RuntimeError) :
    ROBOT_EXIT_ON_FAILURE = True
