import os,re,sys,itertools,pprint

NO_XML_TYPE_CLI_KEY = ['port','service','filter','route','cable','cm','cmts']

dMapping = { \

    'parse_cli_show_type_01' : [\
        'show service sdp',\
        'show service sap-using sap',\
        'show service fdb-mac'\
        ],\
    'parse_cli_show_type_02' : [\
        'show cable',\
        'show router rip database'\
        ],\
    'parse_cli_show_type_05' : [\
        'show port '\
        ],\
    'parse_cli_info_configure_type_01' : [\
        'info configure cm',\
        'info configure cmts'\
        ],\
}

def specific_cli_parser_map (show_command):
    """
        map the particular CLI show command to the respective parser
        
        INPUT PARAMETERS :
        show_command : it contains the CLI show command
       

    """
    find_out = False

    for k,v in dMapping.iteritems():

        for command_item in v :
            if command_item in show_command :
                find_out = True
                break
        if find_out :
            return k
    if not find_out :
        return None
               
def parse_cli_show_type_01(input_data):

    """
    parse cli output, which have following charactors:
      column names is multiple lines, 
      item content is multiple lines, 
      return only one item
    
    parse cli output with following format:
    ====================================================================
    Service Access Points Using Port nt-a:eth:1:0
    ====================================================================
    PortId                          SvcId      Ing.    Egr.   Adm  Opr             
                                               Fltr    Fltr                        
    --------------------------------------------------------------------
    nt-a:eth:1:0                    3          none    none   Up   Up  
    --------------------------------------------------------------------
    Number of SAPs : 1
    --------------------------------------------------------------------
    ====================================================================
    """
    d = {}
    l = []
    data_list = input_data.split("\n")
    
    title_start = False
    column_start = False
    content_start = False    
    is_first_column_line = True
    is_first_content_line = True 
    double_line_no = 0
    single_line_no = 0    
       
    for data_line in data_list :
        if "==========" in data_line :
            double_line_no += 1    
        if "----------" in data_line :
            single_line_no += 1    
                           
        if double_line_no == 1 :
            title_start = True
        elif double_line_no == 2 :  
            column_start = True
            title_start = False
        else :
            title_start = False
            column_start = False
            content_start = False
            
        if single_line_no == 1 :
            content_start = True
            column_start = False
        elif single_line_no >= 2 :
            content_start = False 
            column_start = False
            
        if "======" in data_line  or "------" in data_line :
            continue    
                     
        if title_start : 
            d['title'] = data_line.strip()           
        if column_start :
             if is_first_column_line :
                 col_name_list = _get_item_list_from_str(data_line) 
                 col_pos_list = _get_position_list_from_str(data_line,col_name_list)
                 is_first_column_line = False
             else :
                 new_col_list = _get_items_list_by_position_list (data_line, col_pos_list) 
                 for i in range(0,len(col_name_list)) :
                     col_name_list[i] = col_name_list[i].strip()+new_col_list[i].strip() 
                               
        if content_start :
            if is_first_content_line :
                content_list = _get_item_list_from_str(data_line) 
                is_first_content_line = False
            else :
                 new_content_list=_get_items_list_by_position_list (data_line, col_pos_list)
                 for i in range(0,len(content_list)) :
                     content_list[i] = content_list[i].strip()+\
                     new_content_list[i].strip()
            d = dict(itertools.izip(col_name_list,content_list))
    l = l.append(d)
            
    if len(l) == 1 :
        return d
    elif len(l) > 1 :
        return l
    else :
        return None

 
def parse_cli_show_type_02(input_data):
    """

    parse cli output, which have following charactors:
      column names is multiple lines, 
      item content is single line, 
      return multiple items
    
    parse cli output with following format:

    CLI# show cable modem
    ==============================================================
    Cable Modem
    ==============================================================
    MAC            IP              I/F         MAC       Prim   RxPwr  Timing   Num 
    Address        Address                     State     Sid    (db)   Offset   CPE 
    --------------------------------------------------------------
    e030.05ce.5fd8 10.10.10.201    C1/4/0/1    online    1      32.1   13       0   
    e030.05ce.5fc8 10.10.10.101    C1/5/0/1    online    1      32.1   13       0 
    ============================================================== 

    """
    
    l= []
    d = {}
    data_list = input_data.split("\n")
    
    title_start = False
    column_start = False
    content_start = False    
    is_first_column_line = True
    is_first_content_line = True 
    double_line_no = 0
    single_line_no = 0    
       
    for data_line in data_list :

        if data_line.strip() == "" :
            continue

        if "==========" in data_line :
            double_line_no += 1    

        if "----------" in data_line :
            single_line_no += 1    
                           
        if double_line_no == 1 :
            title_start = True
        elif double_line_no == 2 :  
            column_start = True
            title_start = False
        else :
            title_start = False
            column_start = False
            content_start = False
            
        if single_line_no == 1 :
            if double_line_no == 3 :
                content_start = False
            else :
               content_start = True
            column_start = False              
        elif single_line_no >= 2 :
            content_start = False 
            column_start = False
            
        if "======" in data_line  or "------" in data_line :
            continue 
         
        if title_start : 
            d['title'] = data_line.strip()           
        if column_start :
             if is_first_column_line :
                 col_name_list = _get_item_list_from_str(data_line) 
                 col_pos_list = _get_position_list_from_str(data_line,col_name_list)
                 is_first_column_line = False
             else :
                 try:
                     new_col_list = _get_items_list_by_position_list (data_line, col_pos_list) 
                 except Exception as inst :
                     logger.debug("exception is "+inst)
                     return l
                 for i in range(0,len(col_name_list)) :
                     col_name_list[i] = col_name_list[i].strip()+" "+new_col_list[i].strip() 
                 
        if content_start :
            content_list = _get_items_list_by_position_list (data_line, col_pos_list)
            d = dict(itertools.izip(col_name_list,content_list))
            l.append(d)  
                  
    return l


def parse_cli_show_type_03(input_data):
    """
    parse cli output, which have following charactors:
      column names is single line with line break, 
      item content is single line with line break, 
      return single item   

    Sample Input : show router XXX static-route 

    ===============================================================================
         Static Route Table (Service: 4000)  Family: IPv4
   ===============================================================================
          Prefix                                        Tag         Met    Pref Type Act 
    Next Hop                                    Interface
    -------------------------------------------------------------------------------
        2.2.2.2/32                                    0           1      5    NH   Y   
            20.20.20.1                          vlan3300
           
    -------------------------------------------------------------------------------
         No. of Static Routes: 1
    ===============================================================================    
    """

    d = {}
    total_column = ""
    total_content = ""

    title_start = False
    column_start = False
    content_start = False    
    double_line_no = 0
    single_line_no = 0 
       
    dataList = input_data.split("\n")      
    for lLine in dataList :
        if "==========" in lLine :
            double_line_no += 1    
            continue
        if "----------" in lLine :
            single_line_no += 1    
            continue  
                           
        if double_line_no == 1 :
            title_start = True
        elif double_line_no == 2 :  
            column_start = True
            title_start = False
        else :
            title_start = False
            column_start = False
            
        if single_line_no == 1 :
            content_start = True
            column_start = False
        elif single_line_no >= 2 :
            content_start = False 
            column_start = False
            
        if title_start : 
            d['title'] = lLine.strip()           
        if column_start :
             total_column = total_column + " " + lLine                              
        if content_start :
             total_content = total_content + " " + lLine
             
    col_name_list = _get_item_list_from_str(total_column)
    content_list = _get_item_list_from_str(total_content)                                        
    d = dict(itertools.izip(col_name_list,content_list))  
    return d


def parse_cli_show_type_04(input_data):

    """    
    parse cli output, which have following charactors:
      cli output with format "TYPE : VALUE"
      return single item
      
    SAMPLES OF HANDLED COMMANDS:
     show filter mac <id> ; show router ospf area detail ;show router ospf area detail 
     show router ospf status;

    SAMPLE INPUT:

    ===============================================================================
     Mac Filter
    ===============================================================================
          Filter Id   : 1                                Applied         : Yes
          Scope       : Template                         Def. Action     : Forward
          Entries     : 1                                Type            : normal
          Description : (Not Specified)
    -------------------------------------------------------------------------------
          Filter Match Criteria : Mac
    -------------------------------------------------------------------------------
          Entry       : 1                                FrameType       : Unwn
          Description : (Not Specified)            
          Log Id      : n/a                              
          Src Mac     : 00:00:00:00:00:0a ff:ff:ff:ff:ff:ff
          Dest Mac    :                                    
          Dot1p       : Undefined                        Ethertype       : Undefined
          DSAP        : Undefined                        SSAP            : Undefined
          Snap-pid    : Undefined                        ESnap-oui-zero  : Undefined
          Match action: Drop                             
          Ing. Matches: 5 pkts (520 bytes)
          Egr. Matches: 0 pkts
       
    ===============================================================================
    """
    d = {}
    data_list = input_data.split("\n")
       
    for data_line in data_list :
        if data_line.strip() == "" :
            continue
        for item in re.findall("(\S+?)\s*:\s*(\S*)(\s+|\s*$)",data_line) :
            key,value,x = item
            d[key]= value
    return d


def parse_cli_info_configure_type_01(input_data):
    """
    # SAMPLE OUTPUT
    typ:isadmin># info configure cm classifier c_encp_ds

    configure cm
        classifier c_encp_ds
            priority 0
            s-vlanid-dot1ad 20
            vpn-id EVPL-1
            cmim 0x40000000
        exit
    exit
    """
    print "****parse_cli_info_configure_type_01****"
    l = []
    d = {}
    data_list = input_data.split("\n")
       
    for data_line in data_list :
        m = re.search("^\s+(\S+)\s+(\S+)",data_line)
        if m:
            d[m.group(1)] = m.group(2)
    l.append(d)
    return l
 
def parse_cli_show_type_05(input_data):

    """
    parse cli output, which have following charactors:
      cli output with format both "TYPE : VALUE" and detail lines
      the detail lines have format like :
          ==================
          xxx
          ==================
                      xx  xx
          ------------------
          xxx         xx xx
          ==================
          
      return single item
      
    parse cli output with following format:

    leg:isadmin># show port nt-a:xfp:1 

    ===============================================================================
    Ethernet Interface
    ===============================================================================
    Description        : SFP Plus Port
    Interface          : nt-a:xfp:1                 Oper Speed       : 100 mbps
    Link-level         : Ethernet                   Config Speed     : 10 Gbps
    Admin State        : up                         Oper Duplex      : full
    Oper State         : up                         Config Duplex    : full
    Physical Link      : Yes                        MTU              : 2044
    IfIndex            : 35848192                   Hold time up     : 0 seconds
    Last State Change  : 01/04/1970 11:56:58        Hold time down   : 0 seconds
    Last Cleared Time  : N/A                        

    Configured Mode    : access                     Encap Type       : 802.1q
    Dot1Q Ethertype    : 0x8100                     
    Auto-negotiate     : true                       MDI/MDX          : unknown
    use-vlan-dot1q-ety*: No                         
    Egress Rate        : Default                    
    Egress Burst       : Default                    

    Configured Address : ac:9c:e4:cc:92:ca
    Hardware Address   : ac:9c:e4:cc:92:ca
    Cfg Alarm          : 
    Alarm Status       : 
    Category           : regular                    
    LoopbackMode       : none                       LoopbackVlan     : 0
    Remark             : disbled                    
    State Change Count : 1                          

    ===============================================================================
    Traffic Statistics
    ===============================================================================
                                                       Input                 Output
    -------------------------------------------------------------------------------
    Octets                                             59005                  64834
    Packets                                              812                    560
    Errors                                                 0                      0
    ===============================================================================
    * indicates that the corresponding row element may have been truncated.

    ===============================================================================
    Port Statistics
    ===============================================================================
                                                   Input                 Output
    -------------------------------------------------------------------------------
    Unicast Packets                                      811                    550
    Multicast Packets                                      0                      9
    Broadcast Packets                                      1                      1
    Discards                                               0                      0
    Unknown Proto Discards                                 0                       
    ===============================================================================

    """
    l = []
    d = {}
    sum_d = {}
    detail_d = {}

    for item in re.findall("([^:=\n]+):\s(.*?)(\s{2,})",input_data) :
        key,value,x = item
        key = key.strip()
        value = value.strip()
        sum_d[key] = value
    
    for item in re.findall \
    ("[=]{3,}\s+(.*?)\s+[=]{3,}\s+(\w+)\s{2,}(\w+)\s+[-]{3,}\s+([\w\s]+)\s+[=]{3,}",input_data) :

        title, column1,column2,contents = item
        
        detail_d= {}
        for elem in contents.split("\n") :
            if elem.strip() == "" :
                continue
            m = re.search("\s*((\w+\s{1})+)[\s]{2,}(\w*)[\s]{2,}(\w*)",elem)
            if m.group(3).strip() :               
                detail_d[m.group(1),column1] = m.group(3).strip() 
            else :
                detail_d[m.group(1),column1] = ""          
            if m.group(4) :    
                detail_d[m.group(1),column2] = m.group(4)
            else :
                detail_d[m.group(1),column2] = ""
                
        d[title] = detail_d
        detail_d = {}
        
    d.update(sum_d)
    l.append(d)
    return l
       
def _get_items_list_by_position_list (str_line, pos_list) :
    item_list = []
    for i in range(0,len(pos_list)) :
        item_start_pos = pos_list[i]

        if (i+1) < len(pos_list) :
            item_end_pos = pos_list[i+1]
            item_list.append ( str_line[item_start_pos:item_end_pos].strip() ) 
        else :
            item_list.append ( str_line[item_start_pos:].strip() )
 
    return item_list

def _get_item_list_from_str(str_line):
    str_line = str_line + " "
    item_list = []
    for s in re.findall("((\S+\s{1})+)(\s{2,}|\s*$)",str_line) :
        item,x,x = s
        item_list.append(item.strip())
    return item_list

def _get_position_list_from_str(str_line,refer_list):
    position_list = []
    position = 0
    str_len  = 0
    for item in refer_list :
        position = str_line.find(item,position+str_len)
        position_list.append(position)
        str_len = len(item)
    return position_list
    
    
