import os
import re
import sys
import time
import com_cli
import specific_parsers
from robot.api import logger
from datetime import datetime

CLI_SESSION = {}

def connect_cli (ip,port='23',username="isadmin",first_password="i$@mad-", \
second_password="isamcli!", session_name="first_cli_session",prompt="isadmin>",timeout="3600s",command_prompt=None,transport_protocol=None) :
    """ 
    build up the cli session for sending cli command
   
    usage example :
    | connect cli | 135.251.200.133 | 23 | isadmin | second_password=isamcli! | 
      
    - *ip:* ip address
    - *port:* port number for this connectin
    - *username:* username for create cli session  
    - *first_password:* the default password of cli session 
    - *second_password:* modified password of cli session     
    - *session_name:* the name of target cli session
    - *prompt:* meeting it in command output indicate command executed correctly 
    
    """
    keyword_name = "connect_cli"
    logger.debug("%s:%s -> \
    ip=%s,port=%s,username=%s,first_password=%s,second_password=%s,session_name=%s" \
    % (__name__,keyword_name,ip,port,username,first_password,second_password,session_name))

    try :
        CLI_SESSION[session_name] = com_cli.com_cli(ip,port=port,\
        username=username,password=second_password,\
        default_password=first_password,prompt=prompt,\
        timeout=timeout,command_prompt=command_prompt,transport_protocol=transport_protocol)
    except CliFatalError as inst :
        if session_name == "first_cli_session":
            raise CliFatalError("fatal cli of exception: %s" % inst)
        else:
            raise AssertionError("failed cli of exception: %s" % inst)
    except AssertionError as inst :
        raise AssertionError("fail to init cli session\n exception: %s" % inst)
    
    try :     
        CLI_SESSION[session_name].open_cli()
    except CliFatalError as inst :
        if session_name == "first_cli_session":
            raise CliFatalError("fatal to open cli of exception: %s" % inst)
        else:
            raise AssertionError("failed to open cli of exception: %s" % inst)
    except AssertionError as inst :
        raise AssertionError("fail to open cli session\n exception: %s" % inst)     
    else :
        logger.debug("%s:%s-> cli session created: %s of %s " \
        % (__name__,keyword_name,session_name,str(CLI_SESSION)))
    
    return CLI_SESSION[session_name]


def disconnect_cli (session_name="first_cli_session") :
    """
    close cli session
      
    usage example :
    | disconnect cli |   
    | disconnect cli | second_cli_session |
    
    - *session_name:* the name of target cli session     
    """
    keyword_name = "disconnect_cli"
    logger.debug("%s:%s -> session_name=%s" \
    % (__name__,keyword_name,session_name))
    try:
        # CLI_SESSION[session_name].close_cli()
        if CLI_SESSION == {} or CLI_SESSION[session_name].session_alive == False or CLI_SESSION[session_name].connect_status_check() == False:
            logger.info("%s -> No CLI connected before disconnected it. " % __name__)
        else:
            CLI_SESSION[session_name].close_cli()
            CLI_SESSION.pop(session_name)
    except Exception as inst:
        raise AssertionError("fail to close cli session\n exception: %s" % inst)         
 
    else :
        logger.debug("%s:%s -> cli session '%s' closed " \
        % (__name__,keyword_name,session_name))
    
    return "PASS"

def get_cli_output (command,timeout=0,session_name="first_cli_session",prompt=None,expect_item=None):
    """
    parse 'info' or 'show' type cli command, support only single instance
    
    usage example : 
    | get cli output | show equipment slot nt detail |      
    | get cli output | show equipment slot nt detail | expect_item=slot(\\S+) |
        
    - *command:* the command to be executed
    - *timeout:* the max waiting time (seconds) if no prompt gotten in output
    - *session_name:* the name of target cli session
    - *prompt:* meeting it in command output indicate command executed correctly
 
    """

    keyword_name = "get_cli_output"
    logger.debug("%s:%s -> %s,timeout=%s,session_name=%s" \
    % (__name__,keyword_name,command,str(timeout),session_name))
     
    command = command.rstrip("\n").strip()  
    list_cmd =command.split()
    
    if (list_cmd[0] == 'show' and list_cmd[1] in specific_parsers.NO_XML_TYPE_CLI_KEY) \
    or (list_cmd[0] == 'info' and list_cmd[1] == 'configure' and list_cmd[2] \
    in specific_parsers.NO_XML_TYPE_CLI_KEY)  :
        logger.debug("%s:%s -> command only support specific parser" % (__name__,keyword_name))
        parser_type = "specific"
    else :
        parser_type = "xml"
        if not re.search(" xml", command) :
            command = str(command) + " xml"
  
        if not re.search(" detail", command) :
            command = str(command) + " detail"

    cliobj = CLI_SESSION[session_name]
    try :
        res = cliobj.send_command(command,timeout=timeout,prompt=prompt)
    except CliFatalError as inst :
        if session_name == "first_cli_session":
            raise CliFatalError("fatal cli '%s'\n exception:%s" % (command,inst))
        else:
            raise AssertionError("failed cli '%s' \n exception: %s" % (command,inst))
   
    #logger.info("CLI CMD >> %s" % command)    
    logger.info("<span style=\"color: #00008B\">"+"CLI CMD >> %s" % command+"</span>",html=True)
    status = _check_error(res)
    logger.info("CLI REPLY << %s " % res)
    if status == "FAIL" :
        raise AssertionError("failed cli '%s' \nexception: %s"\
        % (command,res))
        
    if expect_item :
        found = _one_partern_in_multi_line (expect_item, res)
        if not found :
            raise AssertionError("not found '%s' in output: %s"\
            % (expect_item,res))
        else :
            return found

    if parser_type == "specific" :
        parser_procedure = specific_parsers.specific_cli_parser_map(command)
        parser_procedure = "specific_parsers."+parser_procedure
        if parser_procedure :
            instance_list = eval(parser_procedure+'(res)')
            return instance_list[0]
        else :
            return res
    elif parser_type == "xml" :
        instance_list = _parse_xml_cli_response (res)
        return instance_list[0]
    else :
        return res


def get_cli_output_raw (command,timeout=0, session_name="first_cli_session",prompt=None):
    """
    get raw output of cli command without error check
    
    usage example : 
    | get cli output raw | debug-command error-info nt statistics |
           
    - *command:* the command to be executed
    - *timeout:* the max waiting time (seconds) if no prompt gotten in output
    - *session_name:* the name of target cli session
    - *prompt:* meeting it in command output indicate command executed correctly 
      
    """
    keyword_name = "get_cli_output_raw"
    logger.debug("%s:%s -> %s,timeout=%s,session_name=%s" \
    % (__name__,keyword_name,command,str(timeout),session_name)) 
     
    cliobj = CLI_SESSION[session_name]
    try :
        res = cliobj.send_command(command,timeout=int(timeout),prompt=prompt)
    except CliFatalError as inst :
        if session_name == "first_cli_session":
            raise CliFatalError("fatal cli '%s' \n exception: %s" % (command,inst))
        else:
            raise AssertionError("failed cli '%s' \n exception: %s" % (command,inst))

    #logger.info("CLI CMD >> %s" % command)
    logger.info("<span style=\"color: #00008B\">"+"CLI CMD >> %s" % command+"</span>",html=True)
    logger.info("CLI REPLY << %s" % res)
        
    return res
          
def send_cli_commands_in_file(*args):
	
    """ 
      send cli commands in a file
	
      The arguments available are : ['sleep','filename']

    """
    keyword_name = "send_cli_commands_in_file"
    if not aArgs.has_key('session_name') :
        session_name = "first_cli_session"
        
    if not aArgs.has_key('filename') or aArgs['filename'] == "None" :
        raise AssertionError("filename is required")
	
    filename = aArgs['filename']
    if not os.path.isfile(filename) :
        raise AssertionError("'%s' is not a file" % filename)		
    try:
        fin = open(filename,"r")
    except:
        raise AssertionError("could not open file '%s'" % filename)
		
    if fin:
        status = []
        data = fin.readlines()
    for line in data :
        if line == "":
            continue
        command = line  
        try :
            res = CLI_SESSION[session_name].send_command(command)
        except CliFatalError as inst :
            if session_name == "first_cli_session":
                raise CliFatalError("fatal cli '%s' \n exception: %s" % (command,inst))
            else:
                raise AssertionError("failed cli '%s' \n exception: %s" % (command,inst))
    
        if aArgs.has_key('sleep'):
            time.sleep(aArgs['sleep'])

        stat = _check_error(res)
        status.append(stat)
        if stat == "FAIL" :
            logger.debug ("failed command:" + command)
					
    if "FAIL" in status :
        raise AssertionError("fail to execute all commands in %s" % filename)						
    else:
        logger.info("%s:%s -> successfully configued all commands " \
        % (__name__,keyword_name))

def start_capture_cli_log(log_file_name="cli.log",session_name="first_cli_session",timestamp=None):
    """
    capture cli log and save it in file. 
    
    usage example:
    | start capture cli log | cli.log | 
    | start capture cli log | cli.log | second_cli_session | 
  
    - *log_file_name:* the name of cli log file
    - *session_name:* the name of target cli session 
    """
    keyword_name = "start_capture_cli_log"
    logger.debug("%s:%s-> \
    log_file_name='%s',session_name=%s" \
    % (__name__,keyword_name,log_file_name,session_name ))

    try:
        cliobj = CLI_SESSION[session_name]
    except Exception as inst:
        raise AssertionError("no cli session found \n exception:%s" % inst)

    try:    
        cliobj.capture_cli_log(log_file_name,timestamp)
        cliobj.send_command("echo start_capture_cli")
    except Exception as inst :
        s = sys.exc_info()
        raise AssertionError("Fail to capture cli log, lineno:%s, exception: %s" % (s[2].tb_lineno,inst))

def stop_capture_cli_log(session_name="first_cli_session"):
    keyword_name = "stop_capture_cli_log"
    logger.debug("%s:%s-> \
    session_name=%s" \
    % (__name__,keyword_name,session_name ))

    try:
        cliobj = CLI_SESSION[session_name]
    except Exception as inst:
        raise AssertionError("no cli session found \n exception:%s" % inst)

    try: 
        cliobj.send_command("echo stop_capture_cli_1",timeout=10)
        cliobj.send_command("echo stop_capture_cli_2",timeout=10) 
        sCliLog = cliobj.stop_capture_cli()
        return sCliLog
    except Exception as inst :
        s = sys.exc_info()
        raise AssertionError("Fail to stop capture cli log, lineno:%s, exception: %s" % (s[2].tb_lineno,str(inst)))

def send_cli_command (command,timeout=0, session_name="first_cli_session",\
expect_result="PASS",prompt=None,prompt_number=1) :
    """
    send cli command via cli session indicated by session_name
    
    usage example :
    | send cli command | configure equipment slot lt:1/1/2 planned-type fplt-a |
    | send cli command | show equipment slot lt:1/1/2 | session_name=second_cli_session |
    
    - *command:* the command to be executed
    - *timeout:* the max waiting time if the command did not return normally
    - *session_name:* the name of target cli session
    - *prompt:* meeting it in command return indicate command executed correctly    
        
    """
    keyword_name = "send_cli_command"  
    logger.debug("%s:%s-> \
    command='%s',session_name=%s,timeout=%s,expect_result=%s,prompt='%s'" \
    % (__name__,keyword_name,command,session_name,\
    str(timeout),expect_result,prompt))
    try:
        cliobj = CLI_SESSION[session_name]
    except Exception as inst:
        raise AssertionError("no cli session found \n exception:%s" % inst)   
        
    try:     
        #logger.info("CLI CMD >> %s" % command)
        logger.info("<span style=\"color: #00008B\">"+"CLI CMD >> %s" % command+"</span>",html=True)
        if prompt_number == 1 :
            res = cliobj.send_command(command,timeout=timeout,prompt=prompt)
        else :
            res = cliobj.send_group_command(command,prompt_number=prompt_number,timeout=timeout)
    except CliFatalError as inst :
        if session_name == "first_cli_session":
            raise CliFatalError("fatal cli '%s' \n exception: %s" % (command,inst))
        else:
            raise AssertionError("failed cli '%s' \n exception: %s" % (command,inst))
    except AssertionError as inst :     
        raise AssertionError("failed cli '%s' \n exception: %s" % (command,inst))
      
    status = _check_error(res)
    logger.info("CLI REPLY << %s " % (res)) 
    if status != expect_result.upper() : 
        raise AssertionError("failed cli '%s' \n return:'%s'"  % (command,res))
        
    return "PASS",res
    
def send_cli_reboot_command (command,timeout=0, session_name="first_cli_session",\
    prompt=None,reboot_num=1,reboot_time=900):
    """
    send cli command which will cause SUT reboot
    
    usage example :
    | send cli reboot command | admin equipment system reboot with-linked-db | reboot_time=30000 |
    
    - *command:* the command to be executed
    - *timeout:* the max waiting time if the command did not return normally
    - *session_name:* the name of target cli session
    - *prompt:* meeting it in command return indicate command executed correctly
    - *reboot_num:*  the max trying number when send reboot command encounter error
    - *reboot_time:* time to keep trying for DUT connection come back after reboot
             
    """

    keyword_name = "send_cli_reboot_command"  
    logger.debug("%s:%s-> command=%s,session_name=%s,timeout=%s" \
    % (__name__,keyword_name,command,session_name,str(timeout)))
    try:
        cliobj = CLI_SESSION[session_name]
    except Exception as inst:
        raise AssertionError("no cli session found\n exception:%s" % inst)
        
    for i in range (int(reboot_num)) :   
        try:
            #logger.info("CLI CMD >> %s" % command)
            logger.info("<span style=\"color: #00008B\">"+"CLI CMD >> %s" % command+"</span>",html=True)
            res = cliobj.send_command(command,timeout=timeout,prompt=prompt)
        except CliFatalError as inst :
            if session_name == "first_cli_session":
                raise CliFatalError("fatal cli '%s' \n exception: %s" % (command,inst))
            else:
                raise AssertionError("failed cli '%s' \n exception: %s" % (command,inst))
        except AssertionError as inst:
            raise AssertionError("failed cli '%s'\n exception:%s" \
            % (command,inst))              
        status = _check_error(res)
        logger.info("CLI REPLY << %s " % (res)) 
        if status == "FAIL" :
            logger.debug("failed to send command for the %s time,waiting 1 minutes" \
            % str(i))
            time.sleep (60)
        else :
            break
            
    if status == "FAIL" :         
        raise AssertionError("failed cli '%s' after '%s' trying" % (command,str(reboot_num)))
        
    logger.debug("expect SUT reset by '%s'" % command)   
     
    try :
        res = cliobj.reboot_connect_back(float(reboot_time))
    except AssertionError as inst :
        raise AssertionError ("failed to reboot and re-connect cli \n\
                exception:%s" % inst)
    except CliFatalError as inst :
        if session_name == "first_cli_session":
            raise CliFatalError("fatal to reboot and re-connect cli \n\
                exception:%s" % inst)
        else:
            raise AssertionError("fatal to reboot and re-connect cli \n\
                exception:%s" % inst)
    return res  
    
    

def check_cli_command_xml (command,**params):
    """
    to check if specific contents in the command output
    
    usage example :
    | check cli command xml |  info configure mcast detail | check_time=30 | grp-ip-addr=224.1.1.1 |
    
    - *command:*     the retrieve command to be executed
    - *check_time:*  duration time to check output
    - *expect_result:* "PASS" or "FAIL", default is "PASS"
    - *timeout:*     the max waiting time if the command did not return normally
    - *prompt:*      meeting it in command return indicate command executed correctly
    - **=*:*         any items as match conditions in output

    
    """
    keyword="check_cli_command_xml"
    logger.debug("%s:%s %s %s" % (__name__,keyword,command,str(params)))
    command = command.rstrip("\n").strip()
    command = re.sub (" xml","", command) + " xml"
    
    check_time = params.setdefault('check_time', 0)
    timeout = params.setdefault('timeout', 0)   
    expect_result = params.setdefault('expect_result','PASS')
    expect_result = expect_result.upper()
    session_name = params.setdefault('session_name',"first_cli_session")
    prompt = params.setdefault('prompt',None)
    for key in ["check_time","timeout","expect_result","session_name","prompt"] :
        if params.has_key( key ) : params.pop (key)
    
    check_time = int(check_time)
    if check_time > 5 :
        interval_time = int(check_time/5)
    else:      
        interval_time = 1
    current_time = time.mktime(time.localtime())
    end_time = current_time + check_time
    
    while current_time <= end_time:                      
        search_result="PASS"

        cliobj = CLI_SESSION[session_name]
        try :
            res = cliobj.send_command(command,timeout=timeout,prompt=prompt)
        except CliFatalError as inst :
            if session_name == "first_cli_session":
                raise CliFatalError("fatal cli '%s' \n exception: %s" % (command,inst))
            else:
                raise AssertionError("failed cli '%s' \n exception: %s" % (command,inst))
   
        #logger.info("CLI CMD >> %s" % command)
        logger.info("<span style=\"color: #00008B\">"+"CLI CMD >> %s" % command+"</span>",html=True)
        logger.info("CLI REPLY << %s " % res)
        status = _check_error(res)
        if status == "FAIL" :
            if expect_result.upper() != "FAIL":
                raise AssertionError("failed cli '%s' \nexception: %s"\
                % (command,res))
            else :
                return "PASS"
        try :
            lInfo = _parse_xml_to_exact_list (res)
        except AssertionError as inst:    
            if expect_result.upper() == "FAIL":
                return "PASS"
            raise AssertionError("failed cli '%s'\n exception: '%s'" % (command, inst)) 
        
        for elem in params.keys() :
            for info in lInfo :
                if elem+"="+str(params[elem])+"@@$$@@" in info :
                    break
            else :
                logger.info('<span style="color: #FF8C00">not get expected "'\
                +str(params[elem])+' for "'+elem+'"</span>',html=True) 
                search_result = "FAIL"

        if search_result == expect_result.upper():
            return "PASS"
                 
        time.sleep(interval_time)
        current_time = time.mktime(time.localtime())
                 
    if search_result != expect_result.upper():
        raise AssertionError("expect %s/'%s' for '%s'\n actual is '%s'" \
        % (expect_result,str(params),command,info))  

        
def _parse_xml_to_exact_list (res) :
    """
        parse xml format cli command response
    """       
    lData = res.split('\n')
    
    l = []
    prefix = ""   
    node_list = []
    inst_id = 0
    inst_id_list = ["","","",""]
    command = ""
    for lLine in lData:
        

        if "<hierarchy name=" in lLine:
            try:
              m = re.search("<hierarchy name=\"([^ ]+)\" +type=.*>",lLine)         
            except Exception as inst :
              raise AssertionError("failed to get command body \nexception: %s" % inst)
            command = command + " " + m.group(1)
            continue
            
        if "<hier-id name" in lLine :
            m = re.search("<hier-id name=\"([^ ]+)\".*>(.*)</hier-id>",lLine)
            if not m : continue
            param = m.group(1)
            value = m.group(2)
            #prefix = prefix + param + ":" + value + "_"
            command = command + " " + value 
                     
        if "<node name=" in lLine:
            try:  
                m = re.search("<node name=\"([^ ]+)\"(.*)>",lLine)
                node = m.group(1) + "_"
                node_list.append (node)
                prefix = prefix + node
            except Exception as inst : 
                raise AssertionError("failed to parse node in line '%s'\n exception: %s" \
                % (lLine,inst))
            continue 
            
        if "</node>" in lLine:
            node = node_list.pop(-1)
            prefix,x = prefix.rsplit( node )
            continue 
                     
        if "<instance" in lLine:
            inst_id = inst_id + 1        
            continue

        if "</instance>" in lLine:
            inst_id = inst_id - 1
            if prefix != "" :
                prefix,x = prefix.rsplit( inst_id_list[inst_id] )
            inst_id_list[inst_id]=""
            continue
            
        for key_name in ['res-id','hier-id','parameter','info']:               
            m = re.search("<"+key_name+" name=\"([^ ]+)\".*>(.*)</"+key_name+">",lLine)
            if not m : continue
            param = m.group(1)
            value = m.group(2)
            if key_name == 'res-id' : 
                list_num = inst_id -1               
                inst_id_list[list_num] = inst_id_list[list_num] + param + ":" + value + "_"
                prefix = prefix + param + ":" + value + "_"
            prefix = prefix.lstrip("_") 
            if prefix != "" :   
                l.append(prefix+"_"+param+"="+value+"@@$$@@")
            else :
                l.append (param+"="+value+"@@$$@@")
            
    logger.debug ("cli: " + command)        
    logger.debug ("parse result:" + str(l))
    return l

       
def check_cli_command_multi_line_regexp(command, *args):
    """
    to check if specific contents in the command output, the contents can cross multiple lines
    
    usage example :
    | check cli command multi line regexp | show equipment slot lt:1/1/2 detail | oper-status\\s:\\s+enabled | check_time=300 |
    | ${planned_type}= | check cli command multi line regexp | show equipment slot lt:1/1/2 detail |
    |  | oper-status\\s:\\s+enabled | planned-type +: +(\\S+) |
    
    - *command:*     the retrieve command to be executed
    - * .* :*        regular expression to as search partern in command response
    - *check_time:*   duration time to check output every 5 seconds | 
    - *expect_result:*  "PASS" or "FAIL" 
    - *session_name:*   the name of target cli session 
    - *RETURN_VALUE:*  if matched and expect 'PASS', return the match result;
                if not matched and expect 'FAIL', return 'False';
                else,raise error 
              
    chunyagu developed at Oct 21th 2013
    """
    keyword_name = 'check_cli_command_multi_line_regexp'
    logger.debug("command=%s, args=%s" % (command, str(args)))
    input_dict = {}
    input_dict['check_time'] = 0
    input_dict['timeout'] = 0
    input_dict['expect_result'] = "PASS"
    input_dict['session_name'] = "first_cli_session"
    
    search_list = []
    for elem in args:
        try : 
            key_word,expect_value = elem.split("=") 
        except Exception as inst :
            if elem != '':
                search_list.append(elem)
        else :
            input_dict[key_word] = expect_value
            
    check_time = int(input_dict['check_time'])        
    if check_time > 5 :
        interval_time = int(check_time/5)
    else :
        interval_time = 1   

    current_time = time.mktime(time.localtime())
    end_time = current_time + check_time

    while current_time <= end_time :
        try:
            info = get_cli_output_raw \
            (command,session_name=input_dict['session_name'],timeout=input_dict['timeout'])
        except CliFatalError as inst :
            if input_dict['session_name'] == "first_cli_session":
                raise CliFatalError("fatal cli '%s' \n exception: %s" % (command,inst))
            else:
                raise AssertionError("failed cli '%s' \n exception: %s" % (command,inst))
        except AssertionError as inst:    
            raise AssertionError("failed cli '%s' \n exception: %s" \
            % (command, inst)) 
        info = info.replace(command,"")
        
        search_result = "PASS"
        found = _all_items_in_multi_line (search_list, info)        
        if not found :
            search_result = "FAIL"
            logger.debug("not gotten '%s' in output" % str(search_list))
            logger.info("expect_result:%s, search_Result:%s" \
            % (input_dict['expect_result'], search_result)) 
        if search_result == input_dict['expect_result'].upper():
            return found
        else :
            time.sleep(interval_time)
            current_time = time.mktime(time.localtime())           
            continue
    else :        
        raise AssertionError("no '%s' in cli '%s' output"\
        % (str(search_list),command))
   

def check_cli_command_regexp (command,*args) :
    """
    to check if specific contents in the command output, the contents should keep in same line
   
    usage example :
    | check cli command regexp | show cable modem | enabled | check_time=300 |
    | ${modem_ip}= | check cli command regexp | show cable modem | ${cpe_mac} +(\\S+) |    
    
    - *command:*     the retrieve command to be executed
    - * .* :*        regular expression to as search partern in command response
    - *check_time:*   duration time to check output every 5 seconds | 
    - *expect_result:*  "PASS" or "FAIL" 
    - *session_name:*   the name of target cli session 
    - *RETURN_VALUE:*  if matched and expect 'PASS', return the match result;
                if not matched and expect 'FAIL', return 'False';
                else,raise error 
                 
    chunyagu developed at Oct 21th 2013
    """
    keyword_name = 'check_cli_command_regexp'
    logger.debug("command=%s, args=%s" % (command, str(args)))
    input_dict = {}
    input_dict['check_time'] = 0
    input_dict['timeout'] = 0
    input_dict['expect_result'] = "PASS"
    input_dict['session_name'] = "first_cli_session"
    
    search_list = []
    for elem in args:
        try : 
            key_word,expect_value = elem.split("=") 
        except Exception as inst :
            search_list.append(elem)
        else :
            input_dict[key_word] = expect_value
            
    check_time = int(input_dict['check_time'])        
    if check_time > 5 :
        interval_time = int(check_time/5)
    else :
        interval_time = 1   

    current_time = time.mktime(time.localtime())
    end_time = current_time + check_time

    while current_time <= end_time :
        try:
            info = get_cli_output_raw \
            (command,session_name=input_dict['session_name'],timeout=input_dict['timeout'])
        except CliFatalError as inst :
            if input_dict['session_name'] == "first_cli_session":
                raise CliFatalError("fatal cli '%s' \n exception: %s" % (command,inst))
            else:
                raise AssertionError("failed cli '%s' \n exception: %s" % (command,inst))
        except AssertionError as inst:    
            raise AssertionError("failed cli '%s'\n exception: %s" % (command, inst)) 
        info = info.replace(command,"")        
        search_result = "PASS"
        found = _all_items_in_one_line (search_list, info)
        logger.debug("found: %s" % (found))
        if not found :
            search_result = "FAIL"
            logger.debug("not gotten '%s' in output" % str(search_list))
            logger.info("expect_result:%s, search_result:%s" \
            % (input_dict['expect_result'], search_result)) 
            
        if search_result == input_dict['expect_result'].upper():
            return found
        else :
            time.sleep(interval_time)
            current_time = time.mktime(time.localtime()) 
            continue
    else :        
        raise AssertionError("'%s' not in cli '%s' output" % (search_list,command))   

def _all_items_in_multi_line (item_list, lines) :

    found = True  
    command,lines = lines.split("\n",1)
    res_list = []

    for item in item_list : 
        res = re.search(item,lines,re.DOTALL) 
        if not res :
            logger.debug("not found : %s" % item)
            found = False
            break
        else :
            res_list.append(res)        
          
    if found :
        matched_list =[]
        for res in res_list :            
            matched = res.groups()   
            if  len(matched) > 0 :
                matched_list.extend ( list(matched) )    
        if len(matched_list) == 0 :            
            matched = res.group(0)
            matched_list = matched 
        if  len(matched_list) == 1 : 
            matched_list = matched_list[0]
    else :
         matched_list = False 
 
    return matched_list 

def _one_partern_in_multi_line (item, lines) :

    found = True  
    command,lines = lines.split("\n",1)

    
    res = re.search(item,lines,re.DOTALL) 
    if not res :
        logger.debug("not found : %s" % item)
        found = False
    else :
        logger.debug("found : %s" % item)            
    if found :
        matched = res.groups()       
        if  len(matched) == 0 :
            matched = res.group(0)
        elif len(matched) == 1 :
            matched = matched[0]
        else :
            matched = list(matched)
    else :
        matched = False      

    return matched 


def _all_items_in_one_line (item_list, lines) :
    
    line_list = lines.split('\n') 

    for line in line_list[1:] :
        found = True 
        for item in item_list :
            res = re.search(item,line,re.DOTALL)
            if not res :
                found = False
                break
        if found :
            break           
    if found :
        matched = res.groups()       
        if   len(matched) == 0 :
            matched = res.group(0)
        elif len(matched) == 1 :
            matched = matched[0]
        else :
            matched = list(matched)
    else :
        matched = False
    
    return matched
     
     
def _check_error(data):
    """
        check if failure info existing in the input data
    """
    return_val = "PASS"
    error_syntax = '.*Error :.*|.*Error:.*|.*ERROR:.*|.*Error,.*'
    error_syntax = error_syntax + '|.*Warning :.*|.*Can\'t.*|.*Invalid .*|.*MINOR:.*'   
    error_syntax = error_syntax + '|.*Command making ambiguity.*|.*command is not complete.*'
    error_syntax = error_syntax + '|.*invalid token.*|.*unexpected token.*'
    error_syntax = error_syntax + '|.*Internal processing error.*'
    error_syntax = error_syntax + '|.*Resources Temporarily Unavailable.*'
    filter_data = re.sub('major alarm .*Invalid .*>','',data)
    val = re.search(error_syntax,filter_data)
    if val :
        return_val = "FAIL"
        logger.debug("Gotten failure info in command response: %s" % val.group(0))

    return  return_val


def _parse_xml_cli_response (cli_response):

    """
        parse xml format cli command response

    """       
    lData = cli_response.split('\n')
    
    d = {}
    l = []
    cmd = ""
    
    if re.search("<instance",cli_response) :
        no_instance = False
    else :
        no_instance = True
        
    flag_instance = 0
    
    node = ""
    
    for lLine in lData:
        if "<hierarchy name=" in lLine:
          try:
            m = re.search("<hierarchy name=\"([^ ]+)\" +type=.*>",lLine)
            cmd = cmd + m.group(1) + " "           
          except Exception as inst :
            raise AssertionError("failed to get command body \nexception: %s" % inst)
          continue
          
        if "<instance" in lLine:
            d['command'] = cmd.strip()
            flag_instance = flag_instance + 1
            continue

        if "</instance>" in lLine:
            flag_instance = flag_instance - 1
            l.append(d)
            d = {}
            continue

        if "<node name=" in lLine:
            try:  
                m = re.search("<node name=\"([^ ]+)\"(.*)>",lLine)
                sub_node = m.group(1) + "_"
                node = node + sub_node
            except Exception as inst : 
                raise AssertionError("failed to parse node in line '%s'\n exception: %s" \
                % (lLine,inst))
            continue 
                      
        if "</node>" in lLine:
            node = node.strip(sub_node)
            continue

        if flag_instance > 0 or no_instance :
            
            for key_name in ['res-id','hier-id','parameter','info']:
                if "<"+key_name+" name=" in lLine :
                    param_dict = _get_parameter_in_xml_line(lLine,key_name,node)
                    d.update(param_dict)

        if len(l) == 0 and no_instance :
            l.append(d)

    logger.debug("parse result:%s" % str(l))
    return l
  
def _get_parameter_in_line (lLine,key_name,prefix,inst_id) :
    if "<"+key_name+" name=" in line :
        try:  
            m = re.search("<"+key_name+" name=\"([^ ]+)\".*>(.*)</"+key_name+">",line)
            param = node + "_" + m.group(1)
            value = m.group(2)
            return (param,value,inst_id)
        except Exception as inst : 
            raise AssertionError("failed to parse cli item in line '%s' \n exception: %s" \
            % (line,inst))
        return param_dict
    else :
        return (None,None)   
        
def _get_parameter_in_xml_line(line,key_name,node_name) :

    if "<"+key_name+" name=" in line :
        param_dict = {}
        try:  
            m = re.search("<"+key_name+" name=\"([^ ]+)\".*>(.*)</"+key_name+">",line)
            param_dict[node_name+m.group(1)] = m.group(2)
        except Exception as inst : 
            raise AssertionError("failed to parse cli item in line '%s' \n exception: %s" \
            % (line,inst))
        return param_dict
    else :
        return {}


class CliFatalError (RuntimeError) :
    ROBOT_EXIT_ON_FAILURE = True
    
"""
following keywords are going to be obsoleted
"""    
def get_cli_output_multi_items (command,timeout=0,\
session_name="first_cli_session",prompt=None):
    """
    *OBSOLETE*
    parse 'info' or 'show' type cli command,support multiple instancse in command response.
    
    - *command:* the command to be executed
    
    - *timeout:* the max waiting time if the command did not return normally
    
    - *session_name:* the name of target cli session
    
    - *prompt:* meeting it in command return indicate command executed correctly
     
    - *return:*  expected content as a list, or raise error  
    """

    keyword_name = "get_cli_output_multi_items"
    logger.debug("%s:%s-> %s,timeout=%s,session_name=%s" \
    % (__name__,keyword_name,command,str(timeout),session_name))
    
    command = command.rstrip("\n").strip()  
    list_cmd = command.split()
    if list_cmd[0] == 'show' and list_cmd[1] in specific_parsers.NO_XML_TYPE_CLI_KEY :
        logger.debug("%s:%s-> command only support specific parser" \
        % (__name__,keyword_name))
        parser_type = "specific"

    else :
        parser_type = "xml"
        if not re.search(" xml", command) :
            command = str(command) + " xml"
  
        if not re.search(" detail", command) :
            command = str(command) + " detail"
          
    cliobj = CLI_SESSION[session_name]
    try :
        res = cliobj.send_command(command,timeout=timeout,prompt=prompt)
    except CliFatalError as inst :
        if session_name == "first_cli_session":
            raise CliFatalError("fatal cli '%s' \n exception: %s" % (command,inst))
        else:
            raise AssertionError("failed cli '%s' \n exception: %s" % (command,inst))

    #logger.info("CLI CMD >> %s" % command)
    logger.info("<span style=\"color: #00008B\">"+"CLI CMD >> %s" % command+"</span>",html=True)
    status = _check_error(res)
    logger.info("CLI REPLY << %s" % (res))
    if status == "FAIL" :
        raise AssertionError("failed cli '%s'\n exception: %s"\
        % (command,res))
    
    if parser_type == "specific" :
        parser_procedure = specific_parsers.specific_cli_parser_map(command)
        parser_procedure = 'specific_parsers.' + parser_procedure
        if parser_procedure :
            return eval(parser_procedure+'(res)')
        else :
            return res
    elif parser_type == "xml" :
        return _parse_xml_cli_response (res)
    else :
        return res


def check_cli_command (command,check_time,expect_result,*args):
    """
    *OBSOLETE*
    to check if specific contents in the command output, the contens should be in same item
    
    - *command:* the retrieve command to be executed
    - *check_time:* duration time to check output
    - *expect_result:* "PASS" or "FAIL"
    - *args:*    some param to specific like command execution timeout,session_name,command prompt
    - *return:*  if search result is same to expect result, return 'PASS'; else, raise error     
    """
    keyword="check_cli_command"
    logger.debug("%s:%s"  % (__name__,keyword))

    key_word=""
    expect_value=""

    try:
        times = int(check_time)/2
    except Exception as inst:      
        times = 0
    
    session_name="first_cli_session"
    prompt=None
    timeout=0
    try:
        largs=args[0]
        largs.capitalize()
        largs=args
    except:
        largs=args[0]
    for elem in largs:
        try:
            key_word,expect_value=elem.split("=") 
        except:
            logger.debug("elem value can not be split by '=': %s" % elem)
        if key_word=="prompt":
            prompt=expect_value
        elif key_word=="session_name":
            session_name=expect_value
        elif key_word=="timeout":
            timeout=expect_value       
    i=0    
    while i <= times:
        result="PASS"
        try:
            info=get_cli_output(command,session_name=session_name,\
            prompt=prompt,timeout=timeout)
        except CliFatalError as inst :
            if session_name == "first_cli_session":
                raise CliFatalError("fatal cli '%s' \n exception: %s" % (command,inst))
            else:
                raise AssertionError("failed cli '%s' \n exception: %s" % (command,inst))
        except AssertionError as inst:    
            if expect_result.upper() == "FAIL":
                return "PASS"
            raise AssertionError("failed cli '%s'\n exception: '%s'" \
            % (command, inst)) 
    
        for elem in largs:
            if "!=" in elem:
                key_word,expect_value=elem.split("!=")
                if key_word not in info or info[key_word] == expect_value :
                    result = "FAIL"
                    break
            elif "=" in elem:
                key_word,expect_value=elem.split("=")
                if key_word=="prompt" or key_word=="session_name" or key_word=="timeout":
                    continue
                if key_word not in info or info[key_word] != expect_value :
                    result="FAIL"
                    break
        if result == "PASS" and result == expect_result.upper():
            return "PASS"
        time.sleep(2)
        i+=1
    
    if result != expect_result.upper():
        raise AssertionError("expect %s/'%s' for '%s'\n actual is '%s'" \
        % (expect_result,args,command,info)) 
              
            
def check_cli_command_multi_outputs(command,search_condition,check_time,\
expect_result,*args):
    """
    *OBSOLETE*
    
    to check if specific content in the command output 
    
    - *command:* the retrieve command to be executed
    
    - *search_condition:* search string with format aa=bb
    
    - *check_time:* duration time to check output
    
    - *expect_result:* "PASS" or "FAIL"
    
    - *args:*    some param to specific like command execution timeout,session_name,command prompt 
 
    - *return:*  if search result is same to expect result, return 'PASS';
                else, raise error 
                 
    """
    keyword="check_cli_command_multi_outputs"
    logger.info("%s:%s" % (__name__,keyword))
    
    session_name="first_cli_session"
    prompt=None
    timeout=0
    try:
        largs=args[0]
        largs.capitalize()
        largs=args
    except:
        largs=args[0]
    for elem in largs:
        key_word,expect_value=elem.split("=") 
        if key_word=="prompt":
            prompt=expect_value
        elif key_word=="session_name":
            session_name=expect_value
        elif key_word=="timeout":
            timeout=expect_value

    check_time = int(check_time)
    if check_time > 5 :
        interval_time = int(check_time/5)
    else:      
        interval_time = 1
    current_time = time.mktime(time.localtime())
    end_time = current_time + check_time
    result = "FAIL"
    while current_time <= end_time:   
        try:
            info=get_cli_output_multi_items(command,session_name=session_name,\
            prompt=prompt,timeout=timeout)
        except CliFatalError as inst :
            if session_name == "first_cli_session":
                raise CliFatalError("fatal cli '%s' \n exception: %s" % (command,inst))
            else:
                raise AssertionError("failed cli '%s' \n exception: %s" % (command,inst))
        except AssertionError as inst:
            if expect_result.upper() == "FAIL":
                return "PASS"    
            raise AssertionError("failed cli '%s'\n exception: %s" \
            % (command, inst)) 
        
        try:
            key,value = search_condition.split('=',1)
        except:
            raise AssertionError("search_condition format should be aa=xx") 
        for item in info :  
            result="PASS" 
            for elem in largs:
                if "!=" in elem:
                    key_word,expect_value=elem.split("!=")
                    if key not in item or item[key] != value or \
                    key_word not in item or item[key_word] == expect_value:
                        result="FAIL"
                        break
                else:
                    key_word,expect_value=elem.split("=")
                    if key_word=="prompt" or key_word=="session_name" \
                    or key_word=="timeout":
                        continue
                    if key not in item or item[key] != value or \
                    key_word not in item or item[key_word] != expect_value:
                        result="FAIL"
                        break   
            if result == "PASS":
                if result != expect_result.upper():
                    break      
                else:
                    return "PASS" 
        time.sleep(interval_time)
        current_time = time.mktime(time.localtime())

    if result != expect_result.upper(): 
        raise AssertionError("expect %s/'%s' for '%s'\n actual is '%s'" \
        % (expect_result,args,command,info)) 
