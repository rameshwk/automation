import os,sys,time,re
import telnetlib
from robot.api import logger

TELNET_SESSION = {}

def connect_telnet (*session_info,**connect_info) :
    """
    Build up the telnet session for sending command to target 
    - *session_info[0]:* string, default value is "first_telnet_session"
    connect_info: 
    - *ip:* ip address
    - *port:* netconf server port
    - *username:* users defined in netconf aaa
    - *password:* password of above user
    - *prompt:*   the last prompt of netconf cli session
    - *RETURN:*   TELNET_SESSION[session_name] or raise Exception
    
    usage example
    | connect_telnet | &session_info_dict |
    | connect_telnet | session_name=another_telnet_session | ip=127.0.0.1 | port=23 | username='admin' | password='admin'| prompt='.*#' |  
    """    

    keyword_name = 'connect_telnet' 
    logger.debug ("%s:%s-> %s"  % (__name__,keyword_name,str(connect_info)))
    try :
        session_name = session_info[0]
    except Exception as inst :
        session_name = "first_telnet_session"
        
    ip =           connect_info.setdefault('ip','127.0.0.1')
    port =         connect_info.setdefault('port','23')
    username =     connect_info.setdefault('username','admin')
    password =     connect_info.setdefault('password','admin')
    prompt =       connect_info.setdefault('prompt','.*#')
    try:
        TELNET_SESSION[session_name] = telnet_com (ip,port,username,password,prompt)
    except Exception as inst:
        raise AssertionError("%s:%s -> fail to init telnet object: %s" \
        % (__name__,keyword_name,inst))    
            
    try:
        TELNET_SESSION[session_name].open_telnet ()
    except Exception as inst:
        raise AssertionError("%s:%s -> fail to open telnet session: %s" \
        % (__name__,keyword_name,inst))         
	           
    logger.info ("%s:%s -> success to create telnet session: '%s'" \
    % (__name__,keyword_name,str(TELNET_SESSION)))
    
    return TELNET_SESSION[session_name]
    

def disconnect_telnet (session_name="first_telnet_session",comment="disconnect this telnet session") :
    """ 
    Disconnect the connected telnet session
    - *session_name:* string, default value is "first_netconf_cli_session"
    - *comment:*  describe the purpose of this session
    
    usage example
    | disconnect_telnet |
    | disconnect_telnet | "first_netconf_cli_session" |

    """
    keyword_name = 'disconnect_telnet'
    logger.debug ("%s:%s -> session_name=%s" % (__name__,keyword_name,session_name))
    try:
        TELNET_SESSION[session_name].close_telnet()
    except Exception as inst:
        raise AssertionError("%s:%s -> fail to close telnet session\n exception: %s" \
        % (__name__,keyword_name,inst))          
    else :
        TELNET_SESSION.pop(session_name)
        logger.debug("%s:%s -> telnet session '%s' closed" % (__name__,keyword_name,session_name))
    return 


def telnet_send_command (command,timeout=None,prompt=None,session_name="first_telnet_session") :
    """
    send command by telnet session and return the output without any check
    - *command:* commond executed in telenet session
    - *session_name:* string, default value is "first_netconf_cli_session"
    - *prompt:* prompt to identify command end
    - *timeout:* waiting time (seconds) before check return prompt, default value is 0
 
    usage example:
    | $output= | telnet_send_command | pwd | 
    """
    keyword_name = 'telnet_send_command'
    logger.debug ("%s:%s -> command=%s" % (__name__,keyword_name,command))

         
    try:
        res = TELNET_SESSION[session_name].send_command(command,timeout=timeout,prompt=prompt)
    except Exception as inst:
        raise AssertionError( "%s:%s-> failed command '%s', exception :\n %s" \
        % (__name__,keyword_name,command,inst) )
    else :
	logger.info("SSH REPLY << %s" % (res))  
    return res
  
    
def telnet_check_command  (command,*output_regexp,**params):
    """
    to check if specific contents in the command output.
    - *command:* command sent by telnet session 
    - *output_regexp:* regular expression list,the match regexp to command output
    params:
    - *session_name:* string, default value is "first_telnet_session"
    - *prompt:* prompt to identify command end
    - *timeout:* waiting time (seconds) before check return prompt, default value is 0
    - *check_time: * how long time (seconds) before return not found
    - *expect_result:* PASS or FAIL, default value is "PASS" 
     
    usage example:
    | telnet check command | ls -la | temp.txt | \.bashrc |
    | @{date_of_file} | telnet check command | ls -la ~ | \ +(\\S+)\ +(\\d+)\ +(\\d+) +\\.bashrc |
    """

    session_name = params.setdefault('session_name',"first_telnet_session")
    timeout = params.setdefault('timeout', 0)
    check_time = params.setdefault('check_time', 0)
    expect_result = params.setdefault('expect_result',"PASS").upper()
    prompt = params.setdefault('prompt', None)

    keyword_name = 'telnet_check_command'
    logger.debug ("TELNET CMD >> %s " % command)
                
    if check_time > 5 :
        interval_time = int(check_time/5)
    else :
        interval_time = 1   

    current_time = time.mktime(time.localtime())
    end_time = current_time + check_time

    while current_time <= end_time : 
        try:
            info = TELNET_SESSION[session_name].send_command(command,timeout=timeout,prompt=prompt)
        except AssertionError as inst:    
            raise AssertionError("failed command '%s' \n exception: %s" \
            % (command, inst))
        logger.info("TELNET REPLY << %s" % (info))
        search_result = "PASS"
        if not output_regexp :
            return [info]
        found = _check_all_items_return_last (output_regexp, info)
        if not found :
            search_result = "FAIL"
            logger.debug("not gotten '%s' in output" % str(output_regexp))
            logger.info("expect_result:%s, search_result:%s" \
            % (expect_result, search_result)) 
            
        if search_result == expect_result :
            return found
        else :
            time.sleep(interval_time)
            current_time = time.mktime(time.localtime())           
            continue
    else :        
        raise AssertionError("no '%s' in command '%s' output"\
        % (output_regexp,command))

def telnet_check_command_interact  (command,output_regexp,**params):
    """
    to check if specific contents in the command output.
    - *command:* command sent by telnet session 
    - *output_regexp:* regular expression list,the match regexp to command output
    params:
    - *session_name:* string, default value is "first_telnet_session"
    - *prompt:* prompt to identify command end
    - *prompt_interact:* prompt interactive to identify command continue
    - *timeout:* waiting time (seconds) before check return prompt, default value is 0
    - *check_time: * how long time (seconds) before return not found
    - *expect_result:* PASS or FAIL, default value is "PASS" 
     
    usage example:
    | telnet check command | ls -la | temp.txt | \.bashrc |
    | @{date_of_file} | telnet check command | ls -la ~ | \ +(\\S+)\ +(\\d+)\ +(\\d+) +\\.bashrc |
    """
    
    session_name = params.setdefault('session_name',"first_telnet_session")
    timeout = params.setdefault('timeout', 0)
    check_time = params.setdefault('check_time', 0)
    expect_result = params.setdefault('expect_result',"PASS").upper()
    prompt = params.setdefault('prompt', None)
    prompt_interact = params.setdefault('prompt_interact', None)

    keyword_name = 'telnet_check_command'
    logger.debug ("TELNET CMD >> %s session_name is:%s" % (command,session_name))

    if check_time > 5 :
        interval_time = int(check_time/5)
    else :
        interval_time = 1   

    current_time = time.mktime(time.localtime())
    end_time = current_time + check_time

    while current_time <= end_time : 
        try:
            info = TELNET_SESSION[session_name].exec_command_interact(command,timeout=timeout,prompt=prompt,prompt_interact = prompt_interact)
        except AssertionError as inst:   
            raise AssertionError("failed command '%s' \n exception: %s" \
            % (command, inst))
        info_interact = info
        logger.debug("getoutput '%s' in telnet" % info)
        while info.find(prompt) == -1:
            try:
                info = TELNET_SESSION[session_name].exec_command_interact(cmd='c',timeout=timeout,prompt=prompt,prompt_interact = prompt_interact)
                info_interact = info_interact + info
                logger.debug("getoutput '%s' in telnet" % info)
            except AssertionError as inst:  
                raise AssertionError("failed command '%s' \n exception: %s" \
                % (command, inst))
                break

        logger.info("TELNET REPLY << %s" % (info_interact))
        search_result = "PASS"
        if not output_regexp :
            return [info_interact]

        found = _check_all_items_return_last (output_regexp, info_interact)
        if not found :
            search_result = "FAIL"
            logger.debug("not gotten '%s' in output" % str(output_regexp))
            logger.info("expect_result:%s, search_result:%s" \
            % (expect_result, search_result)) 
            
        if search_result == expect_result :
            logger.info("TELNET REPLY find match << %s" % (output_regexp))
            return found
        else :
            time.sleep(interval_time)
            current_time = time.mktime(time.localtime())           
            continue
    else :        
        raise AssertionError("no '%s' in command '%s' output"\
        % (output_regexp,command))
 

class telnet_com (object) :
    def __init__(self,ip='127.0.0.1',port='23',username='alcatel',\
    password='alcatel123',prompt='*',root_password=None) :
    
        self.ip = ip.encode("ascii")
        self.port = port.encode("ascii")
        self.username = username.encode("ascii")
        self.password = password.encode("ascii")
        if root_password :
	    self.root_password = root_password.encode("ascii")
	else :
	    self.root_password = None
	self.prompt = prompt.encode("ascii")

    def _exec_command(self,command,expectPrompt,message="execute command",timeout=5):

        returnTmp = ""
        try:
            self.interface.write(command+"\n")
            logger.debug("write:'%s', expect:'%s'" % (command,expectPrompt))
            returnTmp = self.interface.read_until(expectPrompt,int(timeout))
            logger.debug("get return: \n%s" % returnTmp)
        except Exception as inst:
            msg = "fail to " + message
            logger.debug("%s->%s,exception:%s" % (__name__,msg,inst))
        return returnTmp

                          	
    def open_telnet(self):

        re_telnetTime = 0

        while re_telnetTime < 5:
            try:                                
                self.interface = telnetlib.Telnet(self.ip,self.port,1)
            except Exception as inst:
                if re_telnetTime == 10:
                    raise AssertionError("%s-> Can't telnet after 10 tries, exception:%s" \
                    % (__name__,inst))                   
                self.interface.close()
                re_telnetTime += 1
                logger.info("%s-> Try to telent the %d(th) time" % (__name__,re_telnetTime))
                time.sleep(5)      
                continue         
            else:
                break
               
        msg = "get login prompt"
        expectPrompt = "ogin:"
        res = self._exec_command("",expectPrompt,msg,15)    
        if expectPrompt not in res :
            self.interface.close()
            raise AssertionError ("unexpected response:\n %s" % res)                   
   
        msg = "input username"
        expectPrompt = "assword:"
        res = self._exec_command(self.username,expectPrompt,msg,10)             
        if expectPrompt not in res :
            self.interface.close()
            raise AssertionError ("unexpected response after given username:\n %s" % res)  
            
        msg = "input password"
        expectPrompt = self.prompt      
        returnInfo = self._exec_command(self.password,expectPrompt,msg)             
        if "ogin:" in returnInfo:
            returnInfo = self._exec_command \
                (self.username,"assword: ","input self.username again",10)
            if "assword:" in returnInfo :
                returnInfo = self._exec_command(self.password,expectPrompt,msg,10)
            else:
                self.interface.close()
                raise AssertionError("%s -> fail to %s again" %(__name__,msg))
                
        if expectPrompt not in returnInfo :
            raise AssertionError ( "fail to telnet:\n not gotten expected prompt '%s'" % expectPrompt)
        
        if not self.root_password :
            return
 
        run_sudo = "sudo cd /etc"
        returnInfo = self._exec_command(run_sudo,"$","execute sudo",5)   
        if "$" not in returnInfo:
            if "password for" in returnInfo :
                returnInfo = self._exec_command(self.password,expectPrompt,"input sudo password",5) 
            if not re.search("$", returnInfo):               
                self.interface.close()
                raise AssertionError("%s-> fail to execute telnet process"  %__name__) 

      
    def close_telnet(self):
        try:        
            self.interface.write("logout\n")
            time.sleep(1)
            self.interface.close()
        except Exception as inst:
            msg = "Telnet can't been closed"
            raise AssertionError("%s-> %s, exception: %s" \
            % (__name__,msg, inst))
                
    
    def send_command(self,cmd,timeout=None,prompt="*"):
        command = cmd + "\n"
        try:
            self.interface.write(command.encode('ascii'))
        except Exception as inst:
            msg ="Pls check whether telnet is opened correctly"
            raise AssertionError("%s-> %s, exception: %s"  % (__name__,msg,inst))
        if timeout :
            current_timeout = timeout
        else :
            current_timeout = 5
        if prompt :
            current_prompt = prompt
        else :
            current_prompt= self.prompt
        return self.interface.read_until(current_prompt,current_timeout)

    def exec_command_interact(self,cmd,timeout=None,prompt="*",prompt_interact=None):
        command = cmd + "\n"

        try:
            self.interface.write(command.encode('ascii'))
        except Exception as inst:
            msg ="Pls check whether telnet is opened correctly"
            raise AssertionError("%s-> %s, exception: %s"  % (__name__,msg,inst))
        if timeout :
            current_timeout = timeout
        else :
            current_timeout = 5
        if prompt :
            current_prompt = prompt
        else :
            current_prompt= self.prompt
        logger.info("send_command << %s and %s" % (current_prompt,prompt_interact))  
        prompt_list = [prompt]
        if not prompt == None:
            prompt_list = [prompt_interact,prompt]
        res = self.interface.expect(prompt_list)
        return res[2]

def _check_all_items_return_last (search_list, lines) :

    found = True  
    for item in search_list : 
        item = re.sub(" +"," +",item).strip () 
        res = re.search(item,lines,re.DOTALL) 
        if not res :
            logger.debug("not found : %s" % item)
            found = False
            break
        else :
            logger.debug("found : %s" % item)            
    if found : 
        matched = res.groups()       
        if len(matched) == 0 :
            matched = res.group(0)
    else :
        matched = False  
    return matched 
    


