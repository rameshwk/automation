import time,inspect
from robot.api import logger
import data_translation
import tl1_command

def connect_isam_tl1(tablename = "ISAMData") :
    """ 
      establish TL1 session as the data given in Setup Datafile 
    """
    tablename = tablename.encode("ascii")
    tabledata = data_translation.get_global_table(tablename)
    entry = tabledata[0]
    
    logger.debug("%s-> isam info:%s " % (__name__,str(entry)))
 
    isamip = entry['IP'].encode("ascii")
    username = entry['TL1Username'].encode("ascii")   
    password = entry['TL1Password'].encode("ascii")
    
    try:
        tl1_command.connect_tl1(isamip,username=username,password=password)
    except Exception as inst:
        raise AssertionError("%s -> can not connect tl1, exception: %s" \
        % (__name__,inst)) 
    else:    
        logger.info("connect isam tl1 success" )
    return "pass" 

def disconnect_isam_tl1() :
    try:
        tl1_command.disconnect_tl1()
    except Exception as inst:
        raise AssertionError("%s -> fail to disconnect tl1, exception: %s" \
        % (__name__, inst)) 
    
    logger.info("disconnect isam tl1 success " )
    return "pass" 
    
