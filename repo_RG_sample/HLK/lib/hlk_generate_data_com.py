from robot.api import logger

def dns_payload_value(url):
    ''' 
    This keyword is to generate dns payload value with an url string
    Example 
    | dns_payload_value | atc.wlpd |
    '''
    payload_value = "186d01000001000000000000"
    dns_address = url.split(".")
    urlHex = ""
    length1  = len(dns_address)
    for j in range(length1):
        dns_part = dns_address[j]
        length2 = len(dns_part)
        urlHex = urlHex + "%02x" % length2
        for i in range(length2):
            c = dns_part[i]
            urlHex = urlHex + "%02x" % ord(c)
            
    payload_value = payload_value + urlHex + "0000010001"
    #logger.debug("new dns_payload_value: %s with url %s" % (payload_value, url))
    return payload_value
