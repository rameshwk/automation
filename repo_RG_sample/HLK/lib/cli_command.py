import os
import re
import sys
import time
import com_cli
import specific_parsers
from robot.api import logger
from datetime import datetime

try :
    import CLI_parsers
except Exception as inst :
    sys.path.append(os.environ['ROBOTREPO'] +'/HLK/lib/PARSERS')
    import CLI_parsers  

CLI_SESSION = {}

def connect_cli (ip,port='23',username="isadmin",first_password="i$@mad-", \
second_password="isamcli!", session_name="first_cli_session",prompt="isadmin>",timeout="3600s") :
    """ 
    build up the cli session for sending cli command
    
    - *ip:* ip address
    
    - *port:* port number for this connectin
    
    - *username:* username for create cli session 
    
    - *first_password:* the default password of cli session
    
    - *second_password:* modified password of cli session
           
    - *session_name:* the name of created cli session
       
    - *prompt:* meeting it in command return indicate command executed correctly
     
    - *return:*  session name or raise error   	
    """
    keyword_name = "connect_cli"
    logger.debug("%s:%s -> \
    ip=%s,port=%s,username=%s,first_password=%s,second_password=%s,session_name=%s" \
    % (__name__,keyword_name,ip,port,username,first_password,second_password,session_name))

    try :
        CLI_SESSION[session_name] = com_cli.com_cli(ip,port=port,\
        username=username,password=second_password,default_password=first_password,prompt=prompt,timeout=timeout)
    except CliFatalError as inst :
        raise CliFatalError("fatal to init cli session\n exception: %s" % inst) 
    except AssertionError as inst :
        raise AssertionError("fail to init cli session\n exception: %s" % inst)
    
    try :     
        CLI_SESSION[session_name].open_cli()
    except CliFatalError as inst:
        raise CliFatalError("fatal to open cli session\n exception: %s" % inst)    
    except AssertionError as inst :
        raise AssertionError("fail to open cli session\n exception: %s" % inst)     
    else :
        logger.debug("%s:%s-> cli session created: %s of %s " \
        % (__name__,keyword_name,session_name,str(CLI_SESSION)))
    
    return CLI_SESSION[session_name]


def disconnect_cli (session_name="first_cli_session") :
    """
    close cli session

    - *session_name:* the name of target cli session

    - *return:*  pass or raise error       
    """
    keyword_name = "disconnect_cli"
    logger.debug("%s:%s -> session_name=%s" \
    % (__name__,keyword_name,session_name))
    try:
        CLI_SESSION[session_name].close_cli()
    except Exception as inst:
        raise AssertionError("fail to close cli session\n exception: %s" % inst)         
 
    else :
        logger.debug("%s:%s -> cli session '%s' closed " \
        % (__name__,keyword_name,session_name))
    
    CLI_SESSION.pop(session_name)
    return "pass"

def get_cli_output (command,timeout=0,session_name="first_cli_session",prompt=None):
    """
   parse 'info' or 'show' type cli command, support only single instance.
        
    - *command:* the command to be executed
    
    - *timeout:* the max waiting time if the command did not return normally
    
    - *session_name:* the name of target cli session
    
    - *prompt:* meeting it in command return indicate command executed correctly
     
    - *return:*  expected content as a dictionary, or raise error  
    """

    keyword_name = "get_cli_output"
    logger.debug("%s:%s -> %s,timeout=%s,session_name=%s" \
    % (__name__,keyword_name,command,str(timeout),session_name))
     
    command = command.rstrip("\n").strip()  
    list_cmd =command.split()
    
    if (list_cmd[0] == 'show' and list_cmd[1] in specific_parsers.NO_XML_TYPE_CLI_KEY) \
    or (list_cmd[0] == 'info' and list_cmd[1] == 'configure' and list_cmd[2] \
    in specific_parsers.NO_XML_TYPE_CLI_KEY)  :
        logger.debug("%s:%s -> command only support specific parser" % (__name__,keyword_name))
        parser_type = "specific"
    else :
        parser_type = "xml"
        if not re.search(" xml", command) :
            command = str(command) + " xml"
  
        if not re.search(" detail", command) :
            command = str(command) + " detail"

    cliobj = CLI_SESSION[session_name]
    try :
        res = cliobj.send_command(command,timeout=timeout,prompt=prompt)
    except CliFatalError as inst :
        raise CliFatalError("fatal cli '%s'\n exception:%s" \
        % (command,inst))
   
    logger.info("CLI CMD >> %s" % command)
    status = _check_error(res)
    logger.info("CLI REPLY << %s " % res)
    if status == "fail" :
        raise AssertionError("failed cli '%s' \nexception: %s"\
        % (command,res))

    if parser_type == "specific" :
        parser_procedure = specific_parsers.specific_cli_parser_map(command)
        parser_procedure = "specific_parsers."+parser_procedure
        if parser_procedure :
            instance_list = eval(parser_procedure+'(res)')
            return instance_list[0]
        else :
            return res
    elif parser_type == "xml" :
        instance_list = _parse_xml_cli_response (res)
        return instance_list[0]
    else :
        return res

def get_cli_output_raw (command,timeout=0, session_name="first_cli_session",prompt=None):
    """
    get raw output of cli command
    
    - *command:* the command to be executed
    
    - *timeout:* the max waiting time if the command did not return normally
    
    - *session_name:* the name of target cli session
    
    - *prompt:* meeting it in command return indicate command executed correctly
     
    - *return:*  expected content as a string, or raise error          
    """
    keyword_name = "get_cli_output_raw"
    logger.debug("%s:%s -> %s,timeout=%s,session_name=%s" \
    % (__name__,keyword_name,command,str(timeout),session_name))
    
    command = command.rstrip("\n").strip()  
     
    cliobj = CLI_SESSION[session_name]
    try :
        res = cliobj.send_command(command,timeout=int(timeout),prompt=prompt)
    except CliFatalError as inst :
        raise CliFatalError("fatal cli '%s'\n exception:%s" \
        % (command,inst))

    logger.info("CLI CMD >> %s" % command)
    logger.info("CLI REPLY << %s" % (res))
    
    status = _check_error(res)
    if status == "fail" :
        raise AssertionError("failed cli '%s'\n exception: %s"\
        % (command,res))
        
    return res

def get_cli_output_multi_items (command,timeout=0,\
session_name="first_cli_session",prompt=None):
    """
    parse 'info' or 'show' type cli command,support multiple instancse in command response.
    
    - *command:* the command to be executed
    
    - *timeout:* the max waiting time if the command did not return normally
    
    - *session_name:* the name of target cli session
    
    - *prompt:* meeting it in command return indicate command executed correctly
     
    - *return:*  expected content as a list, or raise error  
    """

    keyword_name = "get_cli_output_multi_items"
    logger.debug("%s:%s-> %s,timeout=%s,session_name=%s" \
    % (__name__,keyword_name,command,str(timeout),session_name))
    
    command = command.rstrip("\n").strip()  
    list_cmd = command.split()
    if list_cmd[0] == 'show' and list_cmd[1] in specific_parsers.NO_XML_TYPE_CLI_KEY :
        logger.debug("%s:%s-> command only support specific parser" \
        % (__name__,keyword_name))
        parser_type = "specific"

    else :
        parser_type = "xml"
        if not re.search(" xml", command) :
            command = str(command) + " xml"
  
        if not re.search(" detail", command) :
            command = str(command) + " detail"
          
    cliobj = CLI_SESSION[session_name]
    try :
        res = cliobj.send_command(command,timeout=timeout,prompt=prompt)
    except CliFatalError as inst :
        raise CliFatalError("fatal cli '%s'\n exception:%s" \
        % (command,inst))

    logger.info("CLI CMD >> %s" % command)
    status = _check_error(res)
    logger.info("CLI REPLY << %s" % (res))
    if status == "fail" :
        raise AssertionError("failed cli '%s'\n exception: %s"\
        % (command,res))
    
    if parser_type == "specific" :
        parser_procedure = specific_parsers.specific_cli_parser_map(command)
        parser_procedure = 'specific_parsers.' + parser_procedure
        if parser_procedure :
            return eval(parser_procedure+'(res)')
        else :
            return res
    elif parser_type == "xml" :
        return _parse_xml_cli_response (res)
    else :
        return res

          
def send_cli_commands_in_file(*args):
	
    """ 
      send cli commands in a file
	
      The arguments available are : ['sleep','filename','retry','pattern','out']
      sleep    - Sleep to wait for the command response from the NE \n 	
      filename - File containing CLI commands that needs to send to NE \n
      retry 	 - It is used to retry for a command for no of times .. 
            ex : retry=2  it will executed cmd -- twice \n
      pattern  - It will search for the user specified pattern in the output \n
      out      - "yes"  or "no" to return output of the command 
      - Developed by Beno K Immanuel
    """
    keyword_name = "send_cli_commands_in_file"
    if not aArgs.has_key('session_name') :
        session_name = "first_cli_session"
        
    if not aArgs.has_key('filename') or aArgs['filename'] == "None" :
        raise AssertionError("filename is required")
	
    filename = aArgs['filename']
    if not os.path.isfile(filename) :
        raise AssertionError("'%s' is not a file" % filename)		
    try:
        fin = open(filename,"r")
    except:
        raise AssertionError("could not open file '%s'" % filename)
		
    if fin:
        status = []
        data = fin.readlines()
    for line in data :
        if line == "":
            continue
        command = line  
        try :
            res = CLI_SESSION[session_name].send_command(command)
        except CliFatalError as inst :
            raise CliFatalError("fatal cli '%s'\n exception:%s" \
            % (command,inst))
	    
        if aArgs.has_key('sleep'):
            time.sleep(aArgs['sleep'])
	      
            if aArgs.has_key('pattern') and aArgs['pattern'] != "None" :		
                pattern_check = re.search(pattern,res)
            if pattern_check:
                logger.info("%s:%s -> %s Pattern Matched:%s" \
                % (__name__,keyword_name,pattern,res))
        stat = _check_error(res)
        status.append(stat)
					
    if "fail" in status :
        raise AssertionError("fail to execute all commands in %s" % filename)							
    else:
        logger.info("%s:%s -> successfully configued all commands " \
        % (__name__,keyword_name))


def check_cli_command(command,check_time,expect_result,*args):
    """
    to check if specific contents in the command output, the contens should be in same item
    
    - *command:* the retrieve command to be executed
    
    - *check_time:* duration time to check output
    
    - *expect_result:* "pass" or "fail"
    
    - *args:*    some param to specific like command execution timeout,session_name,command prompt

     | timeout |       the max waiting time if the command did not return normally |
     | session_name | the name of target cli session |  
     | prompt | meeting it in command return indicate command executed correctly |
     | aa=bb | match conditions |
     
    - *return:*  if search result is same to expect result, return 'pass'; else, raise error     
    """
    keyword="check_cli_command"
    logger.debug("%s:%s" \
    % (__name__,keyword))

    key_word=""
    expect_value=""

    try:
        times = int(check_time)/2
    except Exception as inst:      
        times = 0
    
    session_name="first_cli_session"
    prompt=None
    timeout=0
    try:
        largs=args[0]
        largs.capitalize()
        largs=args
    except:
        largs=args[0]
    for elem in largs:
        try:
            key_word,expect_value=elem.split("=") 
        except:
            logger.debug("elem value can not be split by '=': %s" % elem)
        if key_word=="prompt":
            prompt=expect_value
        elif key_word=="session_name":
            session_name=expect_value
        elif key_word=="timeout":
            timeout=expect_value       
    i=0    
    while i <= times:
        result="pass"
        try:
            info=get_cli_output(command,session_name=session_name,\
            prompt=prompt,timeout=timeout)
        except CliFatalError as inst :
            raise CliFatalError("fatal cli '%s'\n exception:%s" \
            % (command,inst))
        except AssertionError as inst:    
            if expect_result == "fail":
                return "pass"
            raise AssertionError("failed cli '%s'\n exception: '%s'" \
            % (command, inst)) 
    
        for elem in largs:
            if "!=" in elem:
                key_word,expect_value=elem.split("!=")
                if key_word not in info or info[key_word] == expect_value :
                    result="fail"
                    break
            elif "=" in elem:
                key_word,expect_value=elem.split("=")
                if key_word=="prompt" or key_word=="session_name" or key_word=="timeout":
                    continue
                if key_word not in info or info[key_word] != expect_value :
                    result="fail"
                    break
        if result == "pass" and result == expect_result:
            return "pass"
        time.sleep(2)
        i+=1
    
    if result != expect_result:
        raise AssertionError("expect %s/'%s' for '%s'\n actual is '%s'" \
        % (expect_result,args,command,info))     

def check_cli_command_multi_outputs(command,search_condition,check_time,\
expect_result,*args):
    """
    to check if specific content in the command output 
    
    - *command:* the retrieve command to be executed
    
    - *search_condition:* search string with format aa=bb
    
    - *check_time:* duration time to check output
    
    - *expect_result:* "pass" or "fail"
    
    - *args:*    some param to specific like command execution timeout,session_name,command prompt 
    
     | timeout    |  the max waiting time if the command did not return normally | 
     | session_name | the name of target cli session |  
     | prompt | meeting it in command return indicate command executed correctly | 
     
    - *return:*  if search result is same to expect result, return 'pass';
                else, raise error 
                 
    """
    keyword="check_cli_command_multi_outputs"
    logger.info("%s:%s" % (__name__,keyword))
    
    session_name="first_cli_session"
    prompt=None
    timeout=0
    try:
        largs=args[0]
        largs.capitalize()
        largs=args
    except:
        largs=args[0]
    for elem in largs:
        key_word,expect_value=elem.split("=") 
        if key_word=="prompt":
            prompt=expect_value
        elif key_word=="session_name":
            session_name=expect_value
        elif key_word=="timeout":
            timeout=expect_value

    check_time = int(check_time)
    if check_time > 5 :
        interval_time = int(check_time/5)
    else:      
        interval_time = 1
    current_time = time.mktime(time.localtime())
    end_time = current_time + check_time
    result = "fail"
    while current_time <= end_time:   
        try:
            info=get_cli_output_multi_items(command,session_name=session_name,\
            prompt=prompt,timeout=timeout)
        except CliFatalError as inst :
            raise CliFatalError("fatal cli '%s'\n exception:%s" \
            % (command,inst))
        except AssertionError as inst:
            if expect_result == "fail":
                return "pass"    
            raise AssertionError("failed cli '%s'\n exception: %s" \
            % (command, inst)) 
        
        try:
            key,value = search_condition.split('=',1)
        except:
            raise AssertionError("search_condition format should be aa=xx") 
        for item in info :  
            result="pass" 
            for elem in largs:
                if "!=" in elem:
                    key_word,expect_value=elem.split("!=")
                    if key not in item or item[key] != value or \
                    key_word not in item or item[key_word] == expect_value:
                        result="fail"
                        break
                else:
                    key_word,expect_value=elem.split("=")
                    if key_word=="prompt" or key_word=="session_name" \
                    or key_word=="timeout":
                        continue
                    if key not in item or item[key] != value or \
                    key_word not in item or item[key_word] != expect_value:
                        result="fail"
                        break   
            if result == "pass":
                if result != expect_result:
                    break      
                else:
                    return "pass" 
        time.sleep(interval_time)
        current_time = time.mktime(time.localtime())

    if result != expect_result: 
        raise AssertionError("expect %s/'%s' for '%s'\n actual is '%s'" \
        % (expect_result,args,command,info)) 
       
def check_cli_command_multi_line_regexp(command, *args):
    """
    to check if specific contents in the command output, the contents can cross multiple lines
    
    - *command:* the retrieve command to be executed
    
    - *args:*    the regexp string for search 
    
     | check_time   | duration time to check output every 5 seconds | 
     | expect_result | "pass" or "fail" | 
     | session_name | the name of target cli session |
     |              | regular express |
      
    - *return:*  if matched and expect 'pass', return the match result;
                if not matched and expect 'fail', return 'False';
                else,raise error 
                 
    chunyagu developed at Oct 21th 2013
    """
    keyword_name = 'check_cli_command_multi_line_regexp'
    logger.debug("command=%s, args=%s" % (command, str(args)))
    input_dict = {}
    input_dict['check_time'] = 0
    input_dict['timeout'] = 0
    input_dict['expect_result'] = "pass"
    input_dict['session_name'] = "first_cli_session"
    
    search_list = []
    for elem in args:
        try : 
            key_word,expect_value = elem.split("=") 
        except Exception as inst :
            if elem != '':
                search_list.append(elem)
        else :
            input_dict[key_word] = expect_value
            
    check_time = int(input_dict['check_time'])        
    if check_time > 5 :
        interval_time = int(check_time/5)
    else :
        interval_time = 1   

    current_time = time.mktime(time.localtime())
    end_time = current_time + check_time

    while current_time <= end_time :
        try:
            info = get_cli_output_raw \
            (command,session_name=input_dict['session_name'],timeout=input_dict['timeout'])
        except CliFatalError as inst :
            raise CliFatalError("fatal cli '%s'\n exception:%s" \
            % (command,inst))
        except AssertionError as inst:    
            raise AssertionError("failed cli '%s' \n exception: %s" \
            % (command, inst)) 
        info = info.replace(command,"")
        
        search_result = "pass"
        found = _all_items_in_multi_line (search_list, info)
        if not found :
            search_result = "fail"
            logger.debug("not gotten '%s' in output" % str(search_list))
            logger.info("expect_result:%s, search_Result:%s" \
            % (input_dict['expect_result'], search_result)) 
        if search_result == input_dict['expect_result']:
            return found
        else :
            time.sleep(interval_time)
            current_time = time.mktime(time.localtime())           
            continue
    else :        
        raise AssertionError("no '%s' in cli '%s' output"\
        % (str(search_list),command))
   

def check_cli_command_regexp (command,*args) :
    """
    to check if specific contents in the command output, the contents should keep in same line
    
    - *command:* the retrieve command to be executed
    
    - *args:*    the regexp string for search  

     | check_time   | duration time to check output every 5 seconds | 
     | expect_result | "pass" or "fail" | 
     | session_name | the name of target cli session |
     |              | regular express |
       
    - *return:*  if matched and expect 'pass', return the match result;
                if not matched and expect 'fail', return 'False';
                else,raise error 
                 
    chunyagu developed at Oct 21th 2013
    """
    keyword_name = 'check_cli_command_regexp'
    logger.debug("command=%s, args=%s" % (command, str(args)))
    input_dict = {}
    input_dict['check_time'] = 0
    input_dict['timeout'] = 0
    input_dict['expect_result'] = "pass"
    input_dict['session_name'] = "first_cli_session"
    
    search_list = []
    for elem in args:
        try : 
            key_word,expect_value = elem.split("=") 
        except Exception as inst :
            search_list.append(elem)
        else :
            input_dict[key_word] = expect_value
            
    check_time = int(input_dict['check_time'])        
    if check_time > 5 :
        interval_time = int(check_time/5)
    else :
        interval_time = 1   

    current_time = time.mktime(time.localtime())
    end_time = current_time + check_time

    while current_time <= end_time :
        try:
            info = get_cli_output_raw \
            (command,session_name=input_dict['session_name'],timeout=input_dict['timeout'])
        except CliFatalError as inst :
            raise CliFatalError("fatal cli '%s'\n exception:%s" \
            % (command,inst))
        except AssertionError as inst:    
            raise AssertionError("failed cli '%s'\n exception: %s" % (command, inst)) 
        info = info.replace(command,"")        
        search_result = "pass"
        found = _all_items_in_one_line (search_list, info)
        logger.debug("found: %s" % (found))
        if not found :
            search_result = "fail"
            logger.debug("not gotten '%s' in output" % str(search_list))
            logger.info("expect_result:%s, search_result:%s" \
            % (input_dict['expect_result'], search_result)) 
            
        if search_result == input_dict['expect_result']:
            return found
        else :
            time.sleep(interval_time)
            current_time = time.mktime(time.localtime()) 
            continue
    else :        
        raise AssertionError("'%s' not in cli '%s' output" % (search_list,command))   

def _all_items_in_multi_line (item_list, lines) :

    found = True  
    command,lines = lines.split("\n",1)

    for item in item_list : 
        res = re.search(item,lines,re.DOTALL) 
        if not res :
            logger.debug("not found : %s" % item)
            found = False
            break
        else :
            logger.debug("found : %s" % item)            
    if found :
        matched = res.groups()       
        if  len(matched) == 0 :
            matched = res.group(0)
        elif len(matched) == 1 :
            matched = matched[0]
        else :
            matched = list(matched)
    else :
        matched = False      
        
    #logger.debug("all items to find in output: %s" % item_list) 
    return matched 


def _all_items_in_one_line (item_list, lines) :
    
    line_list = lines.split('\n') 

    for line in line_list[1:] :
        found = True 
        for item in item_list :
            res = re.search(item,line,re.DOTALL)
            if not res :
                found = False
                break
        if found :
            break           
    if found :
        matched = res.groups()       
        if   len(matched) == 0 :
            matched = res.group(0)
        elif len(matched) == 1 :
            matched = matched[0]
        else :
            matched = list(matched)
    else :
        matched = False
    
    return matched
     
     
def _check_error(data):
    """
        check if failure info existing in the input data
    """
    return_val = "pass"
    error_syntax = '.*Error :.*|.*Error:.*|.*ERROR:.*|.*Error,.*'
    error_syntax = error_syntax + '|.*Warning :.*|.*Can\'t.*|.*Invalid.*|.*MINOR:.*'   
    error_syntax = error_syntax + '|.*Command making ambiguity.*|.*command is not complete.*'
    error_syntax = error_syntax + '|.*invalid token.*|.*unexpected token.*'
    error_syntax = error_syntax + '|.*Internal processing error.*'
    error_syntax = error_syntax + '|.*Resources Temporarily Unavailable.*'
    val = re.search(error_syntax,data)
    if val :
        return_val = "fail"
        logger.debug("Gotten failure info in command response: %s" % val.group(0))

    return  return_val


def _parse_xml_cli_response (cli_response):

    """
        parse xml format cli command response

    """       
    lData = cli_response.split('\n')
    
    d = {}
    l = []
    cmd = ""
    
    if re.search("<instance",cli_response) :
        no_instance = False
    else :
        no_instance = True
        
    flag_instance = 0
    
    node = ""
    
    for lLine in lData:
        if "<hierarchy name=" in lLine:
          try:
            m = re.search("<hierarchy name=\"([^ ]+)\" +type=.*>",lLine)
            cmd = cmd + m.group(1) + " "           
          except Exception as inst :
            raise AssertionError("failed to get command body \nexception: %s" % inst)
          continue
          
        if "<instance" in lLine:
            d['command'] = cmd.strip()
            flag_instance = flag_instance + 1
            continue

        if "</instance>" in lLine:
            flag_instance = flag_instance - 1
            l.append(d)
            d = {}
            continue

        if "<node name=" in lLine:
            try:  
                m = re.search("<node name=\"([^ ]+)\"(.*)>",lLine)
                sub_node = m.group(1) + "_"
                node = node + sub_node
            except Exception as inst : 
                raise AssertionError("failed to parse node in line '%s'\n exception: %s" \
                % (lLine,inst))
            continue 
                      
        if "</node>" in lLine:
            node = node.strip(sub_node)
            continue

        if flag_instance > 0 or no_instance :
            
            for key_name in ['res-id','hier-id','parameter','info']:
                if "<"+key_name+" name=" in lLine :
                    param_dict = _get_parameter_in_xml_line(lLine,key_name,node)
                    d.update(param_dict)

        if len(l) == 0 and no_instance :
            l.append(d)

    logger.debug("parse result:%s" % str(l))
    return l
  

def _get_parameter_in_xml_line(line,key_name,node_name) :

    if "<"+key_name+" name=" in line :
        param_dict = {}
        try:  
            m = re.search("<"+key_name+" name=\"([^ ]+)\".*>(.*)</"+key_name+">",line)
            param_dict[node_name+m.group(1)] = m.group(2)
        except Exception as inst : 
            raise AssertionError("failed to parse cli item in line '%s' \n exception: %s" \
            % (line,inst))
        return param_dict
    else :
        return {}


def cli_command_failure_check(command,reply,session_name):

    """
    keyword defined by chennai team
    #Name               : cli_command_failure_check
    #Keyword Objective  : Check the reason for the cli_command_failure
    #Input Parameters   : command,cli_command_reply,session_name
    #Output Parameters  : NONE
    #Output Information : rtrv-cond-all & rtrv-errorlog-all & info of configure command
    #                     Auto TI Analysis report details will be added.
    #Designer           : Mahalakshmi Venkatraman (Mahalakshmi.Venkatraman@alcatel-lucent.com)
    #Date               : 04-Sep-2013
    #Comments           : Keyword will be used to find the cli_command_failure
    """
    
    CLI_ANALYSIS_REPORT = ""
    CLI_ANALYSIS_REPORT = "CONFIGURATION FAILED"
    logger.error("<font color='blue'>#################################################################</font>" ,html=True)
    logger.error("<font color='blue'>######Auto TI Analysis Report Start######...</font>" ,html=True)
    logger.error("<font color='blue'>=================================================================</font>" ,html=True)
    logger.error("<b><font color='blue'>######Auto TI Analysis Failure Type : CONFIGURATION FAILED :##### </font></b>" ,html=True)
    logger.error("<font color='blue'>=================================================================</font>" ,html=True)
    logger.error("<font color='blue'>Failed Command : %s </font>" % (command),html=True)
    logger.error("<font color='blue'>Failed Command output : %s </font>" % (reply),html=True)          
    logger.error("<font color='blue'>=================================================================</font>" ,html=True)
    logger.error("<b><font color='blue'>#####Auto TI Analysis Report : Type = CONFIGURATION : %s#####</font><b>" % (CLI_ANALYSIS_REPORT),html=True)
    logger.error("<font color='blue'>=================================================================</font>" ,html=True)
    logger.error("<font color='blue'>#####Auto TI Analysis Report End#####...</font>",html=True)
    logger.error("<font color='blue'>#################################################################</font>" ,html=True)

def send_cli_command (command,timeout=0, session_name="first_cli_session",\
expect_result="pass",prompt=None,prompt_number=1) :
    """
    send cli command via cli session indicated by session_name
    
    - *command:* the command to be executed

    - *timeout:* the max waiting time if the command did not return normally
        
    - *session_name:* the name of target cli session
    
    - *prompt:* meeting it in command return indicate command executed correctly
     
    - *return:*  if executed result is same to expect result, return 'pass';
                else, raise error    
        
    """
    keyword_name = "send_cli_command"  
    logger.debug("%s:%s-> \
    command='%s',session_name=%s,timeout=%s,expect_result=%s,prompt='%s'" \
    % (__name__,keyword_name,command,session_name,\
    str(timeout),expect_result,prompt))
    try:
        cliobj = CLI_SESSION[session_name]
    except Exception as inst:
        raise AssertionError("no cli session found \n exception:%s" % inst)   
        
    try:     
        logger.info("CLI CMD >> %s" % command)
        if prompt_number == 1 :
            res = cliobj.send_command(command,timeout=timeout,prompt=prompt)
        else :
            res = cliobj.send_group_command(command,prompt_number=prompt_number,timeout=timeout)
    except CliFatalError as inst :
        raise CliFatalError("fatal cli '%s' \n exception: %s" % (command,inst))
    except AssertionError as inst :     
        raise AssertionError("failed cli '%s' \n exception: %s" % (command,inst))
      
    status = _check_error(res)
    logger.info("CLI REPLY << %s " % (res)) 
    if status != expect_result : 
        raise AssertionError("failed cli '%s' \n return:'%s'"  % (command,res))
        
    return "pass",res

    
def send_cli_reboot_command (command,timeout=0, session_name="first_cli_session",\
expect_result="pass",prompt=None):
    """
    send cli command which will cause SUT reboot
    
    - *command:* the command to be executed

    - *timeout:* the max waiting time if the command did not return normally
        
    - *session_name:* the name of target cli session
    
    - *prompt:* meeting it in command return indicate command executed correctly \n
     
    - *return:*  if executed result is same to expect result, return 'pass';
                else, raise error       
        
    """

    keyword_name = "send_cli_reboot_command"  
    logger.debug("%s:%s-> command=%s,session_name=%s,timeout=%s,expect_result=%s" \
    % (__name__,keyword_name,command,session_name,str(timeout),expect_result))
    try:
        cliobj = CLI_SESSION[session_name]
    except Exception as inst:
        raise AssertionError("no cli session found\n exception:%s" % inst)
        
    try:     
        logger.info("CLI CMD >> %s" % command)
        res = cliobj.send_command(command,timeout=timeout,prompt=prompt,expect_reboot=True)
    except CliFatalError as inst :
        raise CliFatalError("fatal cli '%s'\n exception:%s" \
        % (command,inst))
    except AssertionError as inst:
        raise AssertionError("failed cli '%s'\n exception:%s" \
        % (command,inst))
      
    status = _check_error(res)
    logger.info("CLI REPLY << %s " % (res)) 
    if status != expect_result : 
        raise AssertionError("failed cli '%s'" % command)
        
    return "pass",res  
 
    
def show_command_failure_check(cmd,reply,errtype,parse_data=None,validate_error=None):

    """
    keyword defined by chennai team
    Check the reason for the show_command validation

    Name               : show_command_failure_check
    Keyword Objective  : Check the reason for the show_command validation
    Input Parameters   : command,command_reply,errtype,parse_data,validate_error,session_name
    Output Parameters  : NONE
    Output Information : Print the errtype & failed scenario in the parse/validation
                         Auto TI Analysis report details will be added.
    Designer           : Mahalakshmi Venkatraman (Mahalakshmi.Venkatraman@alcatel-lucent.com)
    Date               : 04-Sep-2013
    Comments           : Keyword will be used to find the tl1_command_failure

    Sample Output :
    17:37:42.922 	ERROR 	Module:cli_command, Keyword:show_command_failure_check : Auto TI Analysis Report Start...
    17:37:42.924 	ERROR 	Module:cli_command, Keyword:show_command_failure_check : Failure Type : SHOW COMMAND VALIDATION FAILED
    17:37:42.924 	ERROR 	<font color='red'>show Command : >>>show vlan shub-fdb 75 <<< </font>
    17:37:42.924 	ERROR 	Expected val :>>> mac : 00:00:01:00:00:03 Retrieved val :>>> mac : 00:00:01:00:00:01 is not matched Expected val :>>> bridge-port : network:3 Retrieved val :>>> bridge-port : lt:1/1/3 is not matched Expected val :>>> mac : 00:00:01:00:00:03 Retrieved val :>>> mac : 00:00:01:00:00:02 is not matched Expected val :>>> mac : 00:00:01:00:00:03 Retrieved val :>>> mac : 00:00:10:00:80:d1 is not matched Expected val :>>> bridge-port : network:3 Retrieved val :>>> bridge-port : lt:1/1/3 is not matched
    17:37:42.925 	ERROR 	<font color='red'>Expected value is not matched with the retrieved val </font>
    17:37:42.925 	ERROR 	Auto TI Analysis Report : VALUE NOT MATCHED FOR THE GIVEN CRITERIA
    17:37:42.925 	ERROR 	Auto TI Analysis Report End...

    """

    SHOW_ANALYSIS_REPORT = ""
    logger.error("<font color='blue'>#################################################################</font>" ,html=True)
    logger.error("<font color='blue'>######Auto TI Analysis Report Start######...</font>" ,html=True)
    logger.error("<font color='blue'>=================================================================</font>" ,html=True)
    logger.error("<b><font color='blue'>######Auto TI Analysis Failure Type : SHOW COMMAND VALIDATION FAILED :##### </font></b>" ,html=True)
    logger.error("<font color='blue'>=================================================================</font>" ,html=True)
    logger.error("<font color='blue'>show Command : >>>%s<<< </font>" % cmd,html=True)
    if "MAPPER" in errtype :
        logger.error("<font color='blue'>SUITABLE PARSER FUNCTION FOR THE GIVEN CLI COMMAND IN 'mapper.py' IS NOT AVAILABLE </font>",html=True)
        SHOW_ANALYSIS_REPORT += "MAPPING FAILED.<br> "
    if "PARSE" in errtype :
        logger.error("<font color='blue'>PARSING FAILED FOR THE GIVEN SHOW COMMAND OUTPUT </font>",html=True)
        SHOW_ANALYSIS_REPORT += "PARSING FAILED. <br>"
    if "VALIDATEKEY" in errtype :        	
        logger.error("<font color='blue'>Input argument supplied to validate is incorrect </font>",html=True)	
        SHOW_ANALYSIS_REPORT += "SUPPLIED INPUT KEY NOT VALID. <br>"
    if "VALIDATEVALUE" in errtype :        	
        logger.error("<font color='blue'>Expected value is not matched with the retrieved val </font>",html=True)	
        SHOW_ANALYSIS_REPORT += "VALUE NOT MATCHED FOR THE GIVEN CRITERIA. <br>"
    if not SHOW_ANALYSIS_REPORT:
        SHOW_ANALYSIS_REPORT = "SHOW COMMAND FAILED. <br>"
        logger.error ( "<font color='blue'>%s command failed </font>" % (cmd),html=True)
        
    if "VALIDATEKEY" in errtype or "VALIDATEVALUE" in errtype :
        logger.error(validate_error)
        
    logger.error("<font color='blue'>=================================================================</font>" ,html=True)
    logger.error("<b><font color='blue'>#####Auto TI Analysis Report : Type = SHOWCOMMAND :#####<br>%s</font></b>" % (SHOW_ANALYSIS_REPORT),html=True)
    logger.error("<font color='blue'>=================================================================</font>" ,html=True)
    logger.error("<font color='blue'>#####Auto TI Analysis Report End#####...</font>",html=True)
    logger.error("<font color='blue'>#################################################################</font>" ,html=True)
    
def retrive_parsed_output(*resp):	
    """
    keyword defined by chennai team for specific reason
    """       
    for val in resp:
        print " RET ",val

def validate_parsed_data(**aArgs):
    """
    keyword defined by chennai team
    TO VALIDATE THE PARSED DATA FROM THE SHOW OR INFO COMMANDS
    INPUT : validate {} 
    Return value : pass/fail 
    Support the Single List of dict validation
    Support the Multiple List of dict validation
    Author :
    Beno K Immanuel             Developed
    Beno K Immanuel             Added support to handle Multiple Lists of Parsed dicts
    """
    
    global parsed_out	
    input_dict = aArgs
    errorlog = ""
    errtype = ""	
    inp_key =  input_dict.keys()
    keyunavail = 0 
    validate_flag = 0
    status = "fail"
    param_count = 0
    parsed_len = 0 
    mat_count = 0    
    inputfield_len =len(inp_key)
    parsed_datalen = len(parsed_out)
    if parsed_datalen > 1:
        for key_in in inp_key :
            for val in parsed_out:
                if val.has_key(key_in):
                    if re.search(input_dict[key_in],val[key_in]):
                        logger.info("<font size='3'><b>Expected val : %s : %s >>> Retrieved val : %s : %s <<< is matched </b></font>" % (key_in,input_dict[key_in],key_in,val[key_in]),html=True)
                        mat_count+=1
                    else:
                        logger.info("Expected val !!!!:>>> %s : %s Retrieved val :>>> %s : %s is not matched" % (key_in,input_dict[key_in],key_in,val[key_in]))
                        errorlog += "Expected val !!!!!:>>> " + key_in + " : " + input_dict[key_in] +  " Retrieved val :>>> " + key_in + " : " + val[key_in] + " is not matched\n" 
                        validate_flag = 1
        if int(inputfield_len) <= mat_count:
            return "pass"
    else:
        for val in parsed_out:
            validate_flag = 0
            keyunavail = 0
            for key_in in inp_key :
                if val.has_key(key_in) :
                    if re.search(input_dict[key_in],val[key_in]):
                        logger.info("Expected val : %s : %s >>> Retrieved val : %s : %s <<< is matched" % (key_in,input_dict[key_in],key_in,val[key_in]))
                    else:
                        logger.error("Expected val :>>> %s : %s Retrieved val :>>> %s : %s is not matched" % (key_in,input_dict[key_in],key_in,val[key_in]))
                        errorlog += "Expected val :>>> " + key_in + " : " + input_dict[key_in] +  " Retrieved val :>>> " + key_in + " : " + val[key_in] + " is not matched\n" 
                        validate_flag = 1
                else:
                    keyunavail =1 				
                    logger.error("Invalid Key value:%s "% (key_in))
                    errorlog += "Invalid Key value: " + key_in + "\n"
            if keyunavail != 1 and validate_flag != 1 :            
                return "pass"
            else :
                continue

    if keyunavail ==1 :
        status = "fail"
        logger.error("Input argument supplied to validate is incorrect ")
        errtype += ' VALIDATEKEY '
    if validate_flag ==1 :
        status = "fail"
        errtype += ' VALIDATEVALUE '
        logger.error("Expected value is not matched with the retrieved val ")
        
    if keyunavail == 1 or validate_flag == 1 :        
        show_command_failure_check(cmd=CLI_parsers.showcmd,reply=None,errtype=errtype,parse_data=parsed_out,validate_error=errorlog)	
    return status


def update_parsed_data(*args):
    """
    keyword defined by chennai team for specific reason
    """
    global 	parsed_out
    parsed_out = args


class CliFatalError (RuntimeError) :
    ROBOT_EXIT_ON_FAILURE = True
	
