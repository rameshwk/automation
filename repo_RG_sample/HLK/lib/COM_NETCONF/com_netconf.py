import os,re,subprocess,logging,sys,time
import paramiko,ssl,select,socket
from xml.dom.minidom import parseString
from lxml import etree
from multiprocessing import Process
logger = logging.getLogger("com_netconf")

NETCONF_OBJ = {}
BASE_NS_1_0 = "urn:ietf:params:xml:ns:netconf:base:1.0"
PARTIAL_NS_1_0 = "urn:ietf:params:xml:ns:netconf:partial-lock:1.0"
NOTIF_NS_1_0 = "urn:ietf:params:xml:ns:netconf:notification:1.0"
WITH_DEFAULTS_NS = "urn:ietf:params:xml:ns:yang:ietf-netconf-with-defaults"
HELLO_XML = '<?xml version="1.0" encoding="UTF-8"?><hello xmlns="urn:ietf:params:xml:ns:netconf:base:1.0"><capabilities><capability>urn:ietf:params:netconf:base:1.0</capability></capabilities></hello>'
ATTR_DICT ={'operation':BASE_NS_1_0}
XMLNS_MAP = {}
YANG_INFO = {}
RPC_XML_INFO = {}

def connect_netconf(**connect_info):
    """ 
    Build up the NETCONF session
    
    usage Example:
    | connect netconf | &session_info_dict |
    | connect netconf | username=admin | password=admin | session_name=second_session |
    |   | transport_protocol=tls |
    | connect netconf | &DPU_info | transport_protocol=tls |
        
    - *ip:* ip address of netconf SSH server / of TLS client
    - *port:* port number of the netconf SSH server / of tls client
    - *username:* username of the netconf SSH server, default value 'admin'
    - *password:* password of the netconf SSH server, default value 'admin'
    - *ca_cert:* CA certificate file, this is for TLS connection
    - *keyfile:* private key file, this is for TLS connection
    - *certfiles:* certificate file, this is for TLS connection
    - *ciphers:* cipher suites set, this is for TLS connection
    - *session_name:* session name of the connection, default value 'first_netconf_session'.
    - *timeout:* connect retry timeout, default value is 10 mins  
    - *transport_protocol:* transport protocol used for netconf session, support ssh/tls. default value is ssh
    - *RETURN VALUE:* NETCONF_OBJ[session_name] or raise error

    """
    global YANG_INFO
    global XMLNS_MAP
    ret_hello = ''
    ret_yang_lib = ''
    keyword = "connect_netconf"
 
    logger.debug("%s:%s -> %s" % (__name__,keyword,str(connect_info)))
    session_name = connect_info.setdefault('session_name','first_netconf_session')
    transport_protocol = connect_info.setdefault('transport_protocol','ssh')
    RPC_XML_INFO[session_name] = {}
   
    
    if transport_protocol == 'tls':
        session_name = connect_info.setdefault('session_name','tls_netconf_session')
        NETCONF_OBJ[session_name] = tls_netconf(**connect_info) 
    else:
        NETCONF_OBJ[session_name] = ssh_netconf(**connect_info) 

    ret_hello = NETCONF_OBJ[session_name].netconf_connect()

    if YANG_INFO.has_key(session_name) == False:
        # set YANG_INFO from hello message
        l_yang_info_hello,XMLNS_MAP[session_name] = _collect_yang_info_from_hello (ret_hello)

        # get yang-library message
        ret_yang_lib = netconf_get("modules-state{ietf-yang-library}/module",session_name=session_name) 

        # collect yang info from yang-library message and update,XMLNS_MAP[session_name]
        l_yang_info_yang_lib,d_yang_xmlns_map = _collect_yang_info_from_yang_library(ret_yang_lib) 
        len_yang_lib = len(l_yang_info_yang_lib)

        # merge two namespace lists: l_yang_info_hello and l_yang_info_yang_lib
        YANG_INFO[session_name] = _merge_two_list(l_yang_info_hello,l_yang_info_yang_lib)
        logger.debug("Merged list(len:%s): yang_info_hello(len:%s) and yang_info_library(len:%s)" 
        % (len(YANG_INFO[session_name]),len(l_yang_info_hello),len_yang_lib))

        # merge two xmlns map: d_yang_xmlns_map and d_hello_xmlns_map
        XMLNS_MAP[session_name].update(d_yang_xmlns_map)
        logger.debug("XMLNS_MAP:%s" % XMLNS_MAP[session_name])

    return ret_hello,ret_yang_lib

def disconnect_netconf (session_name='first_netconf_session') :
    """ 
    Disconnect the NETCONF session 

    Usage Example:
    | disconnect netconf | 
    | disconnect netconf | session_name=partial_lock_session |     
    - *session_name:* session name of the connection, default value 'first_netconf_session' 
     
    """
    keyword = 'disconnect_netconf'
    logger.debug ("%s:%s-> %s" % (__name__,keyword,session_name))

    NETCONF_OBJ[session_name].netconf_disconnect()
    return

def set_netconf_mgmt_param (session_name='first_netconf_session',**mgmt_param):
    NETCONF_OBJ[session_name].mgmt_param = mgmt_param
    
def netconf_plan_rpc(command_index,*command_elements,**params):
    """
    Edit-rpc operation for netconf 

    Usage Example:
    | netconf plan rpc | interfaces{ietf-interfaces}/interface | type{bbfift:bbf-if-type} bbfift:xdsl |
    | netconf_plan_rpc | entity/{ietf-entity}/physical-entity | name entity_1 | serial-num 123456 |
    |   | admin-state unlocked | class{bbf-entity-extension}{ianaent:iana-entity} ianaent:chassis |
    |   | parent-rel-pos{bbf-entity-extension} 1 |
    | netconf_rpc |
    
    - *command_index:* interface for target nodes
    - *command_elements:* leafs and values for the most inner container
    - *source:* source data base for edit-config operation, default value is 'running'
    - *default_operation:* default operation, value can be merge/replace/none
    - *error_option:* error option, value can be stop-on-error/continue-on-error/rollback-on-error
    - *test_option:* test option, value can be test-then-set/set/test-only
    - *session_name:* session name of the connection, default value 'first_netconf_session' 
    - *timeout:* the max-time to get response of this rpc message (second)
    """
    keyword = "netconf_plan_rpc"
    logger.debug ("%s:%s-> command_index='%s' command_elements='%s' params='%s'" \
    % (__name__,keyword,command_index,str(command_elements),str(params)))

    # set default value
    source = params.setdefault('source','running')
    default_operation = params.setdefault('default_operation',None)
    error_option = params.setdefault('error_option',None)
    test_option = params.setdefault('test_option',None)
    session_name = params.setdefault('session_name','first_netconf_session')

    config_xml = _get_filter_xml(command_index,*command_elements,mgmt_param=NETCONF_OBJ[session_name].mgmt_param,session_name=session_name)
    config_xml = NETCONF_OBJ[session_name].xml_post_operation('edit-config',config_xml)
   
    if RPC_XML_INFO[session_name].has_key('root') :
        # not the first RPC info
        # append config_xml as a child of 'config' tag
        rpc_info = RPC_XML_INFO[session_name]['config'].append(etree.fromstring(config_xml))
        
    else :
        # the first RPC info
        # create a root of xml
        root = etree.Element('edit-config')
        target = etree.SubElement(root,'target')
        running = etree.SubElement(target,'running')
        config = etree.SubElement(root,'config')

        # append default_operation, error_option and test_option
        if default_operation :
            do = etree.SubElement(root,'default_operation')
            do.text = default_operation
        if error_option :
            eo = etree.SubElement(root,'error_option')
            eo.text = error_option
        if test_option:
            to = etree.SubElement(root,'test_option')
            to.text = test_option

        # append config_xml as a child of 'config' tag
        config.append(etree.fromstring(config_xml))
        RPC_XML_INFO[session_name]['config'] = config
        RPC_XML_INFO[session_name]['root'] = root

    logger.debug("RPC_XML_INFO:%s" % RPC_XML_INFO[session_name])


def netconf_edit_config(command_index,*command_elements,**params):
    """
    Edit-config operation for netconf 

    Usage Example:
    | netconf edit config | interfaces{ietf-interfaces}/interface | type{bbfift:bbf-if-type} bbfift:xdsl |
    | netconf edit config | interfaces{ietf-interfaces}/interface{operation:replace} | name new-int |
    |   | port-type{bbf-l2-forwarding}/port-type user-port |
    | netconf_edit_config | a/b[name xx]/c/d | leaf 1 | leaf 2 |
    | netconf_edit_config | entity/{ietf-entity}/physical-entity | name entity_1 | serial-num 123456 |
    |   | admin-state unlocked | class{bbf-entity-extension}{ianaent:iana-entity} ianaent:chassis |
    |   | parent-rel-pos{bbf-entity-extension} 1 |
    | netconf edit config | interfaces{ietf-interfaces}/interface{operation:delete} | name network_itf_2 |
    |   | expect_result=FAIL |			
    
    - *command_index:* interface for target nodes
    - *command_elements:* leafs and values for the most inner container
    - *source:* source data base for edit-config operation, default value is 'running'
    - *default_operation:* default operation, value can be merge/replace/none
    - *error_option:* error option, value can be stop-on-error/continue-on-error/rollback-on-error
    - *test_option:* test option, value can be test-then-set/set/test-only
    - *expect_result:* default value 'PASS'
    - *session_name:* session name of the connection, default value 'first_netconf_session' 
    - *timeout:* the max-time to get response of this rpc message (second)
    - *edit_mode:* config:send rpc directly; plan:plan rpc in advance
    """
    keyword = "netconf_edit_config"
    logger.debug ("%s:%s-> command_index='%s' command_elements='%s' params='%s'" \
    % (__name__,keyword,command_index,str(command_elements),str(params)))

    # set default value
    source = params.setdefault('source','running')
    default_operation = params.setdefault('default_operation',None)
    error_option = params.setdefault('error_option',None)
    test_option = params.setdefault('test_option',None)
    expect_result = params.setdefault('expect_result','PASS')
    expect_result = expect_result.upper()
    session_name = params.setdefault('session_name','first_netconf_session')
    edit_mode = params.setdefault('edit_mode','config')

    if edit_mode == 'plan':
        netconf_plan_rpc(command_index,*command_elements,**params)
        return

    config_xml = _get_filter_xml(command_index,*command_elements,mgmt_param=NETCONF_OBJ[session_name].mgmt_param,session_name=session_name)
    config_xml = NETCONF_OBJ[session_name].xml_post_operation('edit-config',config_xml)
    
    option_dict = {'default_operation':default_operation,\
                       'error_option':error_option, \
                       'test_option':test_option}    
    option_string = ""
    for option,value in option_dict.items():
        if value:
            option_string += ",%s='%s'" % (option,value) 

    cmd_string = "edit_config(target='%s'%s,config='%s')" % (source,option_string,config_xml)
   
    ret = NETCONF_OBJ[session_name].netconf_operation(cmd_string,**params)

    # check if the return is correct
    result,message = NETCONF_OBJ[session_name].check_result(ret)
    if (result == expect_result == 'FAIL' and message == 'RPCError') or (result == expect_result == 'PASS'):
        return ret
    try:
        ret = etree.tostring(etree.fromstring(ret),pretty_print=True)
    except:
        logger.debug("Can not parse return to xml format")

    raise AssertionError("NETCONF edit-config '%s' \n %s" % (message,ret))


def netconf_get (command_index=None,*command_elements,**params) :
    """           
    get states for specific nodes or system
    
    usage example:
        | netconf_get | output_flatpair=name subtending-itf,vlan-id 4 | output_regexp=type.*user_port | timeout=5 |
        | netconf_get | interfaces{ietf-interfaces}/interface | output_flatpair=name subtending-itf,vlan-id 4 |            
        | netconf_get | interfaces{ietf-interfaces}/interface | name subtending-itf | output_regexp=enabled.*true |
        | netconf_get | interfaces{ietf-interfaces}/interface | type{bbfift:bbf-if-type} bbfift:xdsl |        
    
    - *command_index:* interface for target nodes. If command_index=None, get states for whole system                      
    - *command_elements:* contents for filter
    - *session_name:* string, default value is "first_netconf_session"
    - *output_regexp:* the exact match regexp to command output
    - *output_flatpair:* the xml key value pair match
    - *check_time: * how long time (seconds) before return not found
    - *expect_result:* PASS or FAIL, default value is "PASS" 
    - *with_defaults: with-defaults value, default is None
    - *timeout:* the max-time to get response of this rpc message (second)  
    """
    keyword = "netconf_get"
    logger.debug ("%s:%s-> get %s %s %s" % (__name__,keyword,command_index,str(command_elements),str(params)))

    
    check_time = params.setdefault('check_time', 0)
    expect_result = params.setdefault('expect_result','PASS')
    expect_result = expect_result.upper()
    session_name = params.setdefault('session_name',"first_netconf_session")
    output_regexp = params.setdefault('output_regexp',None)
    output_flatpair = params.setdefault('output_flatpair',None)
    with_defaults = params.setdefault('with_defaults',None)

    interval_time = 5

    current_time = time.mktime(time.localtime())
    end_time = current_time + int(check_time)

    if not command_index and command_elements != () :
        raise AssertionError ("index shouldn't be null when element is not null." )

    if command_index == None :
        cmd_string = "get()" 
    else:
        filter_xml = _get_filter_xml(command_index,*command_elements,mgmt_param=NETCONF_OBJ[session_name].mgmt_param,session_name=session_name)
        cmd_string = "get(filter=('subtree','%s'))" % filter_xml 

    if with_defaults:
        cmd_string = cmd_string + 'capabilities:{'+WITH_DEFAULTS_NS+'}with-defaults='+with_defaults   
    while current_time <= end_time :
        search_result = "PASS"          
        ret = NETCONF_OBJ[session_name].netconf_operation(cmd_string,**params)               
        result,message = NETCONF_OBJ[session_name].check_result(ret)

        if result == 'FAIL' :
            if result == expect_result and not (output_regexp and output_flatpair):
                logger.info("gotten expected response: '%s' \n%s" % (expect_result,result))
                return
            else:
                time.sleep(interval_time)
                current_time = time.mktime(time.localtime())
                continue

        info = str(ret)
#        try:
#            xml_str = parseString(info)
#            logger.info("NETCONF REPLY << %s" % (xml_str.toprettyxml(indent=" ")))
#        except:
#            logger.warn("Syntax error in netconf get return, can not parse to xml!")
#            xml_str = info
#            logger.info("NETCONF REPLY << %s" % xml_str)
        if not (output_regexp or output_flatpair):
            if expect_result == 'PASS':
                return (info)
            else:
               raise AssertionError("NETCONF get failed \nexpect: %s, operation return: %s %s" % (expect_result,result,message))

        found1 = found2 = ()
        if output_flatpair:
            list_xml = _change_format(output_flatpair)
            found1 = _check_all_items_return_last (list_xml, info)
            if found1 == False:
                list_xml = _change_format(output_flatpair,style=2)
                found1 = _check_all_items_return_last (list_xml, info)    
        if output_regexp:
            found2 = _check_all_items_return_last (output_regexp.split(','), info)
        if found1 == False or found2 == False :
            search_result = "FAIL"
            logger.debug("not gotten '%s' and '%s' in output" % (str(output_regexp),str(output_flatpair)))
            logger.info("expect_result:%s, search_result:%s" \
                % (expect_result, search_result)) 
            
        if search_result == expect_result :
            if search_result != 'FAIL':
                return found1 + found2
            else:
                return
        else :
            time.sleep(interval_time)
            current_time = time.mktime(time.localtime())           

    raise AssertionError("NETCONF get failed for %s and %s \n\
                          %s " % (output_regexp,output_flatpair,message)) 
    
def netconf_get_config (command_index=None,*command_elements,**params) :
    """           
    get states for specific nodes
            
    usage:
        | netconf_get_config | output_flatpair=name subtending-itf,vlan-id 4 | expect_result=FAIL |
        | netconf_get_config | interfaces{ietf-interfaces}/interface | output_regexp=subtending-itf | timeout=50 | 
                        
    - *command_index:* interface for target nodes.                   
    - *command_elements:* contents for filter
    - *session_name:* string, default value is "first_netconf_session"
    - *source:* target DB
    - *output_regexp:* the match regexp to command output
    - *output_flatpair:* the xml key value pair match
    - *check_time:*  how long time (seconds) before return not found
    - *timeout:* the max-time to get response of this rpc message (second)
    - *expect_result:* PASS or FAIL, default value is "PASS" 
         
    """
    keyword = "netconf_get_config"
    logger.debug ("%s:%s-> get %s %s %s" % (__name__,keyword,command_index,str(command_elements),str(params)))

    check_time = params.setdefault('check_time', 0)
    datastore = params.setdefault('source','running')
    expect_result = params.setdefault('expect_result','PASS')
    expect_result = expect_result.upper()
    session_name = params.setdefault('session_name',"first_netconf_session")
    output_regexp = params.setdefault('output_regexp',None)
    output_flatpair = params.setdefault('output_flatpair',None)
    with_defaults = params.setdefault('with_defaults',None)

    interval_time = 5

    current_time = time.mktime(time.localtime())
    end_time = current_time + int(check_time)
    
    #send netconf cmd
    if command_index == None and command_elements != () :
        raise AssertionError ("index shouldn't be null when element is not null." )

    if command_index == None :
        cmd_string = "get_config(source='%s')" % (datastore)
    else:    
        filter_xml = _get_filter_xml(command_index,*command_elements,mgmt_param=NETCONF_OBJ[session_name].mgmt_param,session_name=session_name)
        cmd_string = "get_config(source='%s',filter=('subtree','%s'))" % (datastore,filter_xml)

    if with_defaults:
        cmd_string = cmd_string + 'capabilities:{'+WITH_DEFAULTS_NS+'}with-defaults='+with_defaults
    while current_time <= end_time :
        search_result = "PASS"
        ret = NETCONF_OBJ[session_name].netconf_operation(cmd_string,**params)             
        # check if the return is correct
        result,message = NETCONF_OBJ[session_name].check_result(ret)
        if result == 'FAIL':
            if result == expect_result and not (output_regexp and output_flatpair):
                logger.info("gotten expected response: '%s'\n %s" \
                % (expect_result,result))
                return
            else:
                raise AssertionError("NETCONF get-config failed\n expect: '%s', \n\
                operation return: %s %s" % (expect_result,result,message))

        info = str(ret)
#        try:
#            xml_str = parseString(info)
#            logger.info("NETCONF REPLY << %s" % (xml_str.toprettyxml(indent=" ")))
#        except:
#            logger.warn("Syntax error in netconf get return, can not parse to xml!")
#            xml_str = info
#            logger.info("NETCONF REPLY << %s" % xml_str)
   
        if not (output_regexp or output_flatpair):
            if expect_result == 'PASS':
                return (info)
            else:
               raise AssertionError("NETCONF get-config failed\n \
               expect: %s, operation return: %s %s" % (expect_result,result,message)) 

        found1 = found2 = ()
        if output_flatpair:
            list_xml = _change_format(output_flatpair)
            found1 = _check_all_items_return_last (list_xml, info)
            if found1 == False:
                list_xml = _change_format(output_flatpair,style=2)
                found1 = _check_all_items_return_last (list_xml, info)    
        if output_regexp:
            found2 = _check_all_items_return_last (output_regexp.split(','), info)
        if found1 == False or found2 == False :
            search_result = "FAIL"
            logger.debug("not gotten '%s' or '%s' in output" % (str(output_regexp),str(output_flatpair)))
            logger.info("expect_result:%s, search_result:%s" \
            % (expect_result, search_result)) 
            
        if search_result == expect_result :
            if search_result != "FAIL":
                return found1 + found2
            else:
                return
        else :
            time.sleep(interval_time)
            current_time = time.mktime(time.localtime())           

    raise AssertionError("NETCONF get-config failed for '%s' and '%s' \n\
                          %s" % (output_regexp,output_flatpair,message)) 


def netconf_database (operation,target='running',source=None,session_name="first_netconf_session",**params) :
    """           
    operations for DB
   
    usage example :
    | netconf_database | copy | <url>tftp://135.251.300.132:/home/auto/IMAGE/confd.db</url> | source=running |           
    | netconf_database | delete | candidate | 
    | netconf_database | lock | running |
    | netconf_database | unlock | running |   
  
    - *operation:* copy,delete,lock,unlock                    
    - *target:* default=running,startup,candidate
    - *session_name:* string, default value is "first_netconf_cli_session"
    - *timeout:* the max-time to get response of this rpc message (second)
    - *source:* source DB
  
    """
    keyword = "netconf_database"
    logger.debug ("%s:%s-> get %s %s %s" % (__name__,keyword,operation,target,source))

    if operation == 'copy' :
        cmd_string = "copy_config(target='%s',source='%s')" % (target,source)

        ret = NETCONF_OBJ[session_name].netconf_operation(cmd_string,**params)
        result,message = NETCONF_OBJ[session_name].check_result(ret)
        if message == 'RPCReply' :
            return ret   
        else :
            raise AssertionError("NETCONF copy-config failed \nOperation return: %s " % ret) 
    if operation == 'delete' :
        cmd_string = "delete_config(target='%s')" % (target)
        ret = NETCONF_OBJ[session_name].netconf_operation(cmd_string,**params)             
        result,message = NETCONF_OBJ[session_name].check_result(ret)
        if message == 'RPCReply' :
            return ret   
        else :
            raise AssertionError("NETCONF delete-config failed \nOperation return: %s" % (ret)) 
    if operation == 'lock' :
        cmd_string = "lock(target='%s')" % (target)
        ret = NETCONF_OBJ[session_name].netconf_operation(cmd_string,**params)
        result,message = NETCONF_OBJ[session_name].check_result(ret)
        if message == 'RPCReply' :
            return ret   
        else :
            raise AssertionError("NETCONF lock failed! \nOperation return: %s" % (ret)) 
    if operation == 'unlock' :
        cmd_string = "unlock(target='%s')" % (target)
        ret = NETCONF_OBJ[session_name].netconf_operation(cmd_string,**params)
        result,message = NETCONF_OBJ[session_name].check_result(ret)
        if message == 'RPCReply' :
            return ret   
        else :
            raise AssertionError("NETCONF unlock failed! \nOperation return: %s" % (ret)) 
    return
    

def netconf_session (operation,session_id=None,session_name="first_netconf_session",**params) :
    """           
    operations for session
    usage example :
    | netconf_session | kill | 21 | session_name=admin_session |         
    | netconf_session | close | session_name=this_session | 
    | netconf_session | close |
            
    - *operation:* kill and close                   
    - *session_id:* the session to be killed 
    - *session_name:* string, default value is "first_netconf_cli_session"
    - *timeout:* the max-time to get response of this rpc message (second)
    """
    keyword = "netconf_session"
    logger.debug ("%s:%s-> get %s %s %s" % (__name__,keyword,operation,session_id,session_name))

    if operation == 'kill' :
        cmd_string = "kill_session(session-id=%s)" % (session_id)
        ret = NETCONF_OBJ[session_name].netconf_operation(cmd_string,**params)
        result,message = NETCONF_OBJ[session_name].check_result(ret)
        if message == 'RPCReply' :
            return ret   
        else :
            raise AssertionError("NETCONF kill failed \nOperation return: %s" % (ret)) 
    if operation == 'close' :
        cmd_string = "close_session()"
        ret = NETCONF_OBJ[session_name].netconf_operation(cmd_string,**params)
        result,message = NETCONF_OBJ[session_name].check_result(ret)
        if message == 'RPCReply' :
            return ret   
        else :
            raise AssertionError("NETCONF close failed \nOperation return: %s " % (ret)) 
    return

def netconf_validate (source="candidate",session_name="first_netconf_session",**params) :
    """           
    operations for validate capability
    
    usage example :
    | netconf_validate | running |
                
    - *source:* default=candidate,or config element containing the configuration subtree to be validated                 
    - *session_name:* string, default value is "first_netconf_cli_session"          
    - *timeout:* the max-time to get response of this rpc message (second)       
    """
    keyword = "netconf_validate"
    logger.debug ("%s:%s-> get %s %s" % (__name__,keyword,source,session_name)) 

    cmd_string = "validate(source='%s')" % (source)
    ret = NETCONF_OBJ[session_name].netconf_operation(cmd_string,**params)
    result,message = NETCONF_OBJ[session_name].check_result(ret)
    if message == 'RPCReply' :
        return ret   
    else :
        raise AssertionError("NETCONF validate failed! \nOperation return: %s" % (ret))

def netconf_discard_changes (session_name="first_netconf_session",**params) :
    """           
    operation for discard changes on candidate
    usage:
    | netconf_discard_changes |    
    - *timeout:* the max-time to get response of this rpc message (second)
    - *session_name:* string, default value is "first_netconf_cli_session"       
    """
    keyword = "netconf_discard_changes"
    logger.debug ("%s:%s-> get %s" % (__name__,keyword,session_name)) 
    cmd_string = "discard_changes()"
    ret = NETCONF_OBJ[session_name].netconf_operation(cmd_string,**params)
    result,message = NETCONF_OBJ[session_name].check_result(ret)
    if message == 'RPCReply' :
        return ret   
    else :
        raise AssertionError("NETCONF discard-changes failed \nOperation return: %s " % (ret))


def netconf_commit (confirmed=False,confirm_timeout=None,persist_id=None,session_name="first_netconf_session",**params) :
    """           
    operation for candidate and confirmed-commit capability
    
    usage example:
        | netconf_commit | source | 
            
    - *confirmed:* enable or disable a confirmed <commit> operation                  
    - *confirm_timeout:* Timeout period for confirmed commit, in seconds,default is 600s
    - *persist_id:* for confirm a commit
    - *session_name:* string, default value is "first_netconf_cli_session"
    - *timeout:* the max-time to get response of this rpc message (second)     
    """
    keyword = "netconf_commit"
    logger.debug ("%s:%s-> get %s %s %s %s" % (__name__,keyword,confirmed,confirm_timeout,persist_id,session_name))
    if not confirmed :
        if persist_id :
            cmd_string = "commit(confirmed='%s',persist-id='%s')" % (confirmed,persist_id)
        else :
            cmd_string = "commit(confirmed='%s')" % (confirmed)
        ret = NETCONF_OBJ[session_name].netconf_operation(cmd_string,**params)
        result,message = NETCONF_OBJ[session_name].check_result(ret)
        if message == 'RPCReply' :
            return ret   
        else :
            raise AssertionError("NETCONF commit operation failed! \nOperation return: %s \n \
            " % (ret))
    else :
        cmd_string = "commit(confirmed='%s'" % (confirmed)
        if confirm_timeout != None :
            cmd_string += ",timeout='%s'" % (confirm_timeout) 
        if persist_id != None :
            cmd_string += ",persist='%s'" % (persist_id)
        cmd_string += ")"              
        ret = NETCONF_OBJ[session_name].netconf_operation(cmd_string,**params)     
        result,message = NETCONF_OBJ[session_name].check_result(ret)
        if message == 'RPCReply' :
            return ret   
        else :
            raise AssertionError("NETCONF commit failed \nOperation return: %s" % (ret))
        
def netconf_cancel_commit (persist_id=None,session_name="first_netconf_session",**params) :
    """           
    operation for cancel commit capability

    usage example:
    | netconf_cancel_commit | persist_id |  
        
    - *persist_id:* for cancel a issued commit
    - *session_name:* string, default value is "first_netconf_cli_session"
    - *timeout:* the max-time to get response of this rpc message (second)
    """
    keyword = "netconf_cancel_commit"
    logger.debug ("%s:%s-> get %s %s" % (__name__,keyword,persist_id,session_name))
    if persist_id :
        cmd_string = "cancel_commit('%s')" % (persist_id)
    else :
        cmd_string = "cancel_commit()"
    ret = NETCONF_OBJ[session_name].netconf_operation(cmd_string,**params)
    result,message = NETCONF_OBJ[session_name].check_result(ret)
    if message == 'RPCReply' :
        return ret   
    else :
        raise AssertionError("NETCONF cancel-commit failed\nOperation return: %s" % (ret))

def netconf_get_schema (identifier,*output_regexp,**params):
    """
    get schema and check operation for netconf 

    Usage Example:
    | netconf get schema | interfaces | namespace +"http://example.com/ns/link" |
    | netconf get schema | interfaces | namespace +"http://example.com/ns/link" | version=1.0 | format=xsd |
    
    - *identifier:* name of the schema to be retrieved
    - *output_regexp:* regular expression to check the content of the schema
    - *version:* version of schema to get, default is None
    - *format:* format of the schema to be retrieved, yang is the default
    - *expect_result:* default value is 'PASS'
    - *session_name:* session name of the connection, default value 'first_netconf_session' 
    - *timeout:* the max-time to get response of this rpc message (second)
    """    
    keyword = "netconf_get_schema"
    logger.debug ("%s:%s-> schema-check %s %s %s" % (__name__,keyword,identifier,str(output_regexp),str(params)))

    # set default value
    version = params.setdefault('version',None)
    format = params.setdefault('format',None)
    expect_result = params.setdefault('expect_result','PASS')
    expect_result = expect_result.upper()
    session_name = params.setdefault('session_name','first_netconf_session')
    
    cmd_string = "get_schema(identifier='%s'" % identifier
    option_dict = {'version':version,\
                       'format':format}
    for option,value in option_dict.items():
        if value:
           cmd_string += ",%s='%s'" % (option,value) 
    cmd_string += ')'
   
    ret = NETCONF_OBJ[session_name].netconf_operation(cmd_string,**params)
        
    # check if the return is correct
    result,message = NETCONF_OBJ[session_name].check_result(ret)

    if result == 'FAIL':
        if result == expect_result and not output_regexp:
            logger.info("NETCONF get schema executed. expect: %s, operation return: %s" % (expect_result,result))
            return ['PASS']
        else:
            raise AssertionError("NETCONF get-schema failed \n%s" % (message))

    if not output_regexp:
        if expect_result == 'PASS':
            return [ret]
   
    found = _check_all_items_return_last(output_regexp,ret)
    logger.debug("search %s in output schema. result: %s" % (str(output_regexp),str(found)))
    if (not found and expect_result == 'PASS') or (found and expect_result == 'FAIL'):
        raise AssertionError("unexpected search result for '%s' \nactual_result: %s, expect_result: %s" \
        % (str(output_regexp),str(found),expect_result))    
    return found

def netconf_create_subscription (command_index=None,*command_element,**params) :
    """
    create subscription and capture notifications from start_time to stop_time.
    check the result if output_regexp or output_flatpaire existing. 
    Need open another new session specially for notification

    Usage Example:
    | netconf create subscription | interfaces | interfaces{ietf-interfaces} interface | type{bbfift:bbf-if-type} bbfift:xdsl |
    |   | start_time = 2007-07-28T15:23:36Z | output_regexp = linkup |    
    
    - *stream:* stream for notification subscription, default value is NETCONF
    - *command_index:* interface for target nodes, default value is None
    - *command_elements:* leafs and values for the most inner container
    - *start_time:* given start_time means replay notification from this point-in-time.default is current time.
    - *stop_time:* given end time means replay notification till this point-in-time. default is forever.
    - *check_time:* timeout for notification check
    - *output_regexp:* exact regular expression to check the notification result, if not provided get output without check and return
    - *output_flatpair:* xml key value style to check the notification result
    - *delay_time:* specific waiting time for alarm comming
    - *expect_result:* default value 'PASS'
    - *session_name:* session name of the connection, default value 'notification_session' 
    - *timeout:* the max-time to get response of this rpc message (second)
    """
    stream = params.setdefault('stream', 'NETCONF')
    start_time = params.setdefault('start_time', None)
    stop_time = params.setdefault('end_time', None)
    session_name = params.setdefault('session_name', 'notification_session')

    logger.debug ( "netconf_create_subscription : stream=%s, command_index=%s, %s, %s" \
    % (stream,command_index,str(command_element),str(params)))

    # create subscription
    root = etree.Element("create-subscription",{'xmlns':NOTIF_NS_1_0})
    if command_index:
        filter = _get_filter_xml(command_index,*command_elements,mgmt_param=NETCONF_OBJ[session_name].mgmt_param,session_name=session_name)
        root.append(filter)
    elem1 = etree.Element("stream")
    elem1.text = stream
    root.append(elem1)
    if start_time:
        elem2 = etree.Element("startTime")
        elem2.text = start_time
        root.append(elem2)
    if stop_time:
        elem3 = etree.Element("stopTime")
        elem3.text = stop_time
        root.append(elem3)
 
    subs_xml = _to_xml(_wrap_rpc(root))
    NETCONF_OBJ[session_name].send_xml(subs_xml)

    ret = netconf_notification_check (**params) 
 
    return ret

def netconf_notification_check (**params):
    """
    check notification from output buffer with regular expression.
    notification must be subscribed before
    
    Usage Example:
    | netconf notification check | output_regexp = linkup.*linkdown | 
        
    - *check_time:* timeout for notification check
    - *output_regexp:* regular expression to check the notification result, if not provided get output without check and return
    - *output_flatpair:* xml key value style to check the notification result
    - *delay_time:* specific waiting time for alarm comming
    - *expect_result:* default value 'PASS'
    - *session_name:* session name of the connection, default value 'notification_session' 
    - *timeout:* the max-time to get response of this rpc message (second)
    """
    
    logger.debug ("netconf_notification_check: " + str(params))
    session_name = params.setdefault('session_name', 'notification_session')
    check_time = params.setdefault('check_time', 2)
    output_regexp = params.setdefault('output_regexp', None)
    output_flatpair = params.setdefault('output_flatpair', None)
    expect_result = params.setdefault('expect_result', 'PASS')
    expect_result = expect_result.upper()
    timeout = params.setdefault('timeout', 0)
    delay_time = params.setdefault('delay_time', 1)
    data = NETCONF_OBJ[session_name].get_output(timeout,int(delay_time))
    result,message = NETCONF_OBJ[session_name].check_result(data)
    if result == 'FAIL':
        if expect_result == 'FAIL' and not (output_regexp and output_flatpair):
            return ['PASS']
        else:    
            raise AssertionError("Check notification fail! Result: %s %s" % (result,message))

    if not (output_regexp or output_flatpair):
        return [data]

    list_xml = found1 = found2 = ()

    if output_flatpair:
        list_xml = _change_format(output_flatpair)

    for i in range(0,int(check_time)/2):  
        if list_xml:       
            found1 = _check_all_items_return_last (list_xml, data)
            if found1 == False:
                list_xml = _change_format(output_flatpair,style=2)
                found1 = _check_all_items_return_last (list_xml, info)    
            logger.debug("search %s in output notification. result: %s" % (output_flatpair,str(found1)))
        if output_regexp:
            found2 = _check_all_items_return_last (output_regexp.split(','), data)       
            logger.debug("search %s in output notification. result: %s" % (output_regexp,str(found2)))        
        if (not (found1 or found2) and expect_result == 'FAIL') :
            return found1 + found2
        elif (not found1) and found2 and expect_result == 'PASS' :
            return found2
        elif (not found2) and found1 and expect_result == 'PASS' :
            return found1
        elif found1 and found2 and expect_result == 'PASS' :
            return found1 + found2
        elif (found1 or found2) and expect_result == 'FAIL':
            raise AssertionError("find %s or %s unexpected" \
            % (output_regexp,output_flatpair))
        else:
            time.sleep(2)
            data += NETCONF_OBJ[session_name].get_output(timeout,int(delay_time))
        
    raise AssertionError("Error in checking %s and %s \n" \
    % (output_regexp,output_flatpair))


def netconf_partial_lock (*select,**params) :
    """
    partial lock of the NETCONF running datastore.

    Usage Example:
    | netconf partial lock | interfaces{ietf-interfaces}/interface/port-type{bbf-l2-forwarding} |
      interfaces{ietf-interfaces}/interface/type xdsl{bbfift:bbf-if-type} |     
    
    - *select:* select node for partial lock 
    - *expect_result:* default value 'PASS'
    - *session_name:* session name of the connection, default value 'first_netconf_session' 
    - *timeout:* the max-time to get response of this rpc message (second)
    """
    keyword = "netconf_partial_lock"
    logger.debug ("%s:%s-> partial-lock %s %s" % (__name__,keyword,str(select),str(params)))
    session_name = params.setdefault('session_name', 'first_netconf_session')
    expect_result = params.setdefault('expect_result', 'PASS')
    expect_result = expect_result.upper()
    timeout = params.setdefault('timeout', 0)
    lock_id = ""
    lock_node = []
    root = etree.Element("partial-lock")
    key_group = []
    index = 0

    for index in range(0,len(select)) :
        key_group.append(select[index].split('/'))
    for index in range(0,len(select)) :
        index1 = 0 
        none = 0
        key_dict = {}
        value = ""
        for index1 in range(0,len(key_group[index])) :
            if key_group[index][index1].find('{') >= 0 and key_group[index][index1].find(' ') < 0 :
                node,key_dict_elem,key_dict_attr,prefix=_parse_attr(key_group[index][index1],session_name)
                key_list = key_dict_elem.keys()
                elem_list = key_dict_elem.values()
                if key_list[0] == None :
                    if none == 0 :
                        value = value + "/%s" % (node)
                        none = none + 1
                    else :
                        key_list[0] = "re" + str(index1)
                        key_dict_elem.clear
                        key_dict_elem = {key_list[0]:elem_list[0]}
                        value = value + "/%s:%s" % (key_list[0],node)
                else :
                    value = value + "/%s:%s" % (key_list[0],node)
                key_dict.update(key_dict_elem)   
            if key_group[index][index1].find('{') < 0 and key_group[index][index1].find(' ') < 0 :
                if value.find(':') >= 0:
                    left = value.rindex('/')
                    right = value.rindex(':')
                    pre = value[left:right]
                    value = value + "%s:%s" % (pre,key_group[index][index1])
                else :
                    value = value +"/%s" % (key_group[index][index1])                   
            if key_group[index][index1].find(' ')  >= 0 and key_group[index][index1].find('{') >= 0 :
                value_list1 = key_group[index][index1].split(' ')
                if value_list1[0].find('{') >= 0 and value_list1[1].find('{') >= 0 :
                    node1,key_dict_elem1,key_dict_attr1,prefix1=_parse_attr(value_list1[0],session_name)
                    node2,key_dict_elem2,key_dict_attr2,prefix2=_parse_attr(value_list1[1],session_name)
                    key_list1 = key_dict_elem1.keys()
                    elem_list1 = key_dict_elem1.values() 
                    key_list2 = key_dict_elem2.keys()
                    elem_list2 = key_dict_elem2.values()    
                    key_list1[0] = "ne" + str(index1)
                    key_dict_elem1.clear
                    key_dict_elem1 = {key_list1[0]:elem_list1[0]}
                    value = value + "[%s:%s='%s:%s']" % (key_list1[0],node1,key_list2[0],node2)                        
                    key_dict.update(key_dict_elem1)
                    key_dict.update(key_dict_elem2)
                if value_list1[0].find('{') >= 0 and value_list1[1].find('{') < 0 :
                    node1,key_dict_elem1,key_dict_attr1,prefix1=_parse_attr(value_list1[0],session_name)
                    key_list1 = key_dict_elem1.keys()
                    elem_list1 = key_dict_elem1.values() 
                    key_list1[0] = "ne" + str(index1)
                    key_dict_elem1.clear
                    key_dict_elem1 = {key_list1[0]:elem_list1[0]}
                    value = value + "[%s:%s='%s']" % (key_list1[0],node1,node2)                        
                    key_dict.update(key_dict_elem1)
                if value_list1[0].find('{') < 0 and value_list1[1].find('{') >= 0 :
                    node2,key_dict_elem2,key_dict_attr2,prefix2=_parse_attr(value_list1[1],session_name)
                    key_list2 = key_dict_elem2.keys()
                    elem_list2 = key_dict_elem2.values()
                    value = value + "[%s='%s:%s']" % (value_list1[0],key_list2[0],node2)                        
                    key_dict.update(key_dict_elem2)              
            if key_group[index][index1].find(' ')  >= 0 and key_group[index][index1].find('{') < 0:
                value_list = key_group[index][index1].split(' ')
                left = value.rindex('/')
                left = left + 1
                right = value.rindex(':')
                pre = value[left:right]
                value = value + "[%s:%s='%s']" % (pre,value_list[0],value_list[1])                   
        select_xml = etree.Element('select',nsmap=key_dict)
        root.append(select_xml)
        select_xml.text=value
    
    etc = _partial_lock_rpc(root)
    print (etree.tostring(etc,pretty_print=True))
    subs_xml = _to_xml(_partial_lock_rpc(root))

    NETCONF_OBJ[session_name].send_xml(subs_xml)
    data = NETCONF_OBJ[session_name].get_output(timeout)

    result = "PASS"
    error_info = ""
    string = ""
    result,error_info = _check_error (data)
    if result == expect_result == 'PASS' : 
        m = re.search('<lock-id.*>(.*)</lock-id>',data)
        count = data.count('</locked-node>')
        if m :
            lock_id = m.group(1)
        lock_node = re.findall('<locked-node[^\>]*>(.*)</locked-node>',data)
        logger.debug("NETCONF partial lock operation executed successfully. \n lock-id is: %s. \n locked node is: \n %s \
        " % (lock_id,lock_node))
        return (lock_id,lock_node)   
    if result == expect_result == 'FAIL' :
        logger.debug("NETCONF partial-lock failed as expected \nOperation return: %s" % (data))
    else :    
        raise AssertionError("NETCONF partial-lock failed \nOperation return: %s" % (data))
   
def netconf_partial_unlock (lock_id=None,**params) :
    """
    partial unlock of the NETCONF running datastore.
    
    Usage Example:
    | netconf partial unlock | lock_id = 45 | 
        
    - *lock_id:* lock id of the locked node  
    - *expect_result:* default value 'PASS'
    - *session_name:* session name of the connection, default value 'first_netconf_session' 
    - *timeout:* the max-time to get response of this rpc message (second)
    """
    keyword = "netconf_partial_unlock"
    logger.debug ("%s:%s-> partial-unlock %s %s" % (__name__,keyword,lock_id,str(params)))
    session_name = params.setdefault('session_name', 'first_netconf_session')
    expect_result = params.setdefault('expect_result', 'PASS')
    expect_result = expect_result.upper()
    timeout = params.setdefault('timeout', 0)    
    root = etree.Element("partial-unlock")
    lock = etree.Element("lock-id")
    root.append(lock)
    lock.text=lock_id
    etc = _partial_lock_rpc(root)
    print (etree.tostring(etc,pretty_print=True))
    subs_xml = _to_xml(_partial_lock_rpc(root))
    NETCONF_OBJ[session_name].send_xml(subs_xml) 
    data = NETCONF_OBJ[session_name].get_output(timeout)
    result = "PASS"
    error_info = ""
    result,error_info = _check_error (data)
    if result == expect_result == 'PASS' : 
        logger.debug("NETCONF partial-unlock operation executed successfully. Operation return: %s." % (data))
        return result                
    elif result == expect_result == 'FAIL' :
        logger.debug("NETCONF partial-unlock failed as expected. Operation return: %s" % (data))
    else :    
        raise AssertionError("NETCONF partial-unlock failed\nOperation return: %s" % (data))

def netconf_rpc (rpc=None,**params):
    """
    send xml command via file which contain xml message
    Need open a new session with session_name as 'rpc_session'

    Usage Example:
    | netconf rpc | /home/sunny/get.xml |     
    
    - *rpc:* the path of the file which contain the xml message 
    - *expect_result:* default value 'PASS'
    - *session_name:* session name of the connection, default value 'first_netconf_session' 
    - *timeout:* the max-time to get response of this rpc message (second)
    - *output_regexp:* 
    - *output_flatpair:*
    """
    expect_result = params.setdefault('expect_result', 'PASS')
    expect_result = expect_result.upper()
    session_name = params.setdefault('session_name', 'first_netconf_session')
    timeout = params.setdefault('timeout', 0)
    output_regexp = params.setdefault('output_regexp',None)
    output_flatpair = params.setdefault('output_flatpair',None)

    xml_str  = '' 
    subs_xml = ''

    if not rpc :
        # get rpc from global variable RPC_XML_INFO[session_name] which is created by netconf_edit_rpc
        if RPC_XML_INFO[session_name].has_key('root') : 
            try:
                subs_xml = _to_xml(_wrap_rpc(RPC_XML_INFO[session_name]['root']))
            except Exception as inst:
                raise AssertionError("Failed to parse xml:%s" % cmd_string) 
           
            # cleanup global variable 
            RPC_XML_INFO[session_name] = {}
        else :
            raise AssertionError("No rpc information can be sent!!")

    else :  
        # get rpc from file
        for single_key in params.keys():
            if single_key in ['expect_result','session_name','timeout','output_regexp','output_flatpair']:
                continue
            if xml_str == '':
                try:
                    fobj = open(rpc)
                    xml_str = fobj.read()
                finally:
                    fobj.close()
            search_pattern = '\$\{'+single_key+'\}'
            xml_str = re.sub(search_pattern,str(params[single_key]),xml_str)
        if xml_str == '':
            try:
                tree = etree.parse(rpc)
                root = etree.fromstring(etree.tostring(tree))
            except Exception as inst:
                raise AssertionError ("parse rpc file fail\n Exception: %s" % inst)
        else:
            try:
                root = etree.fromstring(xml_str)
            except Exception as inst:
                raise AssertionError ("parse rpc string fail\nException: %s" % inst)    
        subs_xml = _to_xml(_wrap_rpc(root))

    i = 0
    while i <= int(timeout) :
        search_result = "PASS"

        NETCONF_OBJ[session_name].send_xml(subs_xml)
        ret = NETCONF_OBJ[session_name].get_output(timeout)
         
        # check if the return is correct
        result,message = NETCONF_OBJ[session_name].check_result(ret)
        if result == 'FAIL':
            if result == expect_result and not (output_regexp and output_flatpair):
                logger.info("gotten expected response: '%s'\n %s" \
                % (expect_result,result))
                return
            else:
                raise AssertionError("netconf rpc failed. expect: '%s', \n\
                operation return: %s %s" % (expect_result,result,message))

        info = str(ret)
#        try:
#            xml_str = parseString(info)
#            logger.info("NETCONF REPLY << %s" % (xml_str.toprettyxml(indent=" ")))
#        except:
#            logger.warn("Syntax error in netconf get return, can not parse to xml!")
#            xml_str = info
#            logger.info("NETCONF REPLY << %s" % xml_str)
   
        if not (output_regexp or output_flatpair):
            if expect_result == 'PASS':
                return (info)
            else:
               raise AssertionError("netconf rpc failed\n expect: %s, operation return: %s %s" % (expect_result,result,message)) 

        found1 = found2 = ()
        if output_flatpair:
            list_xml = _change_format(output_flatpair)
            found1 = _check_all_items_return_last (list_xml, info)
            if found1 == False:
                list_xml = _change_format(output_flatpair,style=2)
                found1 = _check_all_items_return_last (list_xml, info)    
        if output_regexp:
            found2 = _check_all_items_return_last (output_regexp.split(','), info)
        if found1 == False or found2 == False :
            search_result = "FAIL"
            logger.debug("not gotten '%s' or '%s' in output" % (str(output_regexp),str(output_flatpair)))
            logger.info("expect_result:%s, search_result:%s" \
            % (expect_result, search_result)) 
            
        if search_result == expect_result :
            if search_result != "FAIL":
                return found1 + found2
            else:
                return
        
        time.sleep(2)
        i+=2     

    raise AssertionError("netconf rpc failed \nexpect: %s, operation return: %s %s\
                          output_regexp: %s %s" % (expect_result,result,message,output_regexp,output_flatpair))

def netconf_rpc_check_response (**params) :
    """
    Check the response of sending netconf rpc. 
    Need open a new session with session_name as 'rpc_session'

    Usage Example:
    | netconf rpc check response | expect_result=PASS | output_regexp=module.*bbs-fast |     
    
    - *expect_result:* default value 'PASS'
    - *session_name:* session name of the connection, default value 'first_netconf_session' 
    - *timeout:* the max-time to get response of this rpc message (second)
    - *output_regexp:* 
    - *output_flatpair:*
    """
    expect_result = params.setdefault('expect_result', 'PASS')
    expect_result = expect_result.upper()
    session_name = params.setdefault('session_name', 'first_netconf_session')
    timeout = params.setdefault('timeout', 0)
    output_regexp = params.setdefault('output_regexp',None)
    output_flatpair = params.setdefault('output_flatpair',None)

    xml_str  = '' 
    subs_xml = ''

    i = 0
    while i <= int(timeout) :
        search_result = "PASS"

        ret = NETCONF_OBJ[session_name].get_output(timeout)
         
        # check if the return is correct
        result,message = NETCONF_OBJ[session_name].check_result(ret)
        if result == 'FAIL':
            if result == expect_result and not (output_regexp and output_flatpair):
                logger.info("gotten expected response: '%s'\n %s" \
                % (expect_result,result))
                return
            else:
                raise AssertionError("netconf rpc failed. expect: '%s', \n\
                operation return: %s %s" % (expect_result,result,message))

        info = str(ret)
#        try:
#            xml_str = parseString(info)
#            logger.info("NETCONF REPLY << %s" % (xml_str.toprettyxml(indent=" ")))
#        except:
#            logger.warn("Syntax error in netconf get return, can not parse to xml!")
#            xml_str = info
#            logger.info("NETCONF REPLY << %s" % xml_str)
   
        if not (output_regexp or output_flatpair):
            if expect_result == 'PASS':
                return (info)
            else:
               raise AssertionError("netconf rpc failed\n expect: %s, operation return: %s %s" % (expect_result,result,message)) 

        found1 = found2 = ()
        if output_flatpair:
            list_xml = _change_format(output_flatpair)
            found1 = _check_all_items_return_last (list_xml, info)
            if found1 == False:
                list_xml = _change_format(output_flatpair,style=2)
                found1 = _check_all_items_return_last (list_xml, info)    
        if output_regexp:
            found2 = _check_all_items_return_last (output_regexp.split(','), info)
        if found1 == False or found2 == False :
            search_result = "FAIL"
            logger.debug("not gotten '%s' or '%s' in output" % (str(output_regexp),str(output_flatpair)))
            logger.info("expect_result:%s, search_result:%s" \
            % (expect_result, search_result)) 
            
        if search_result == expect_result :
            if search_result != "FAIL":
                return found1 + found2
            else:
                return
        
        time.sleep(2)
        i+=2     

    raise AssertionError("netconf rpc failed \nexpect: %s, operation return: %s %s\
                          output_regexp: %s %s" % 
                        (expect_result,result,message,output_regexp,output_flatpair))


def netconf_rpc_ignore_response (rpc=None,**params):
    """
    send xml command via file which contain xml message and don't wait for RPC response. 
    Need open a new session with session_name as 'rpc_session'

    Usage Example:
    | netconf rpc without response | /home/sunny/get.xml |     
    
    - *rpc:* the path of the file which contain the xml message 
    - *session_name:* session name of the connection, default value 'first_netconf_session' 
    - *timeout:* the max-time to get response of this rpc message (second)
    """
    expect_result = params.setdefault('expect_result', 'PASS')
    expect_result = expect_result.upper()
    session_name = params.setdefault('session_name', 'first_netconf_session')
    timeout = params.setdefault('timeout', 0)

    xml_str  = '' 
    subs_xml = ''

    if not rpc :
        # get rpc from global variable RPC_XML_INFO[session_name] which is created by netconf_edit_rpc
        if RPC_XML_INFO[session_name].has_key('root') : 
            try:
                subs_xml = _to_xml(_wrap_rpc(RPC_XML_INFO[session_name]['root']))
            except Exception as inst:
                raise AssertionError("Failed to parse xml:%s" % cmd_string) 
           
            # cleanup global variable 
            RPC_XML_INFO[session_name] = {}
        else :
            raise AssertionError("No rpc information can be sent!!")

    else :  
        # get rpc from file
        for single_key in params.keys():
            if single_key in ['expect_result','session_name','timeout','output_regexp','output_flatpair']:
                continue
            if xml_str == '':
                try:
                    fobj = open(rpc)
                    xml_str = fobj.read()
                finally:
                    fobj.close()
            search_pattern = '\$\{'+single_key+'\}'
            xml_str = re.sub(search_pattern,params[single_key],xml_str)
        if xml_str == '':
            try:
                tree = etree.parse(rpc)
                root = etree.fromstring(etree.tostring(tree))
            except Exception as inst:
                raise AssertionError ("parse rpc file fail\n Exception: %s" % inst)
        else:
            try:
                root = etree.fromstring(xml_str)
            except Exception as inst:
                raise AssertionError ("parse rpc string fail\nException: %s" % inst)    
        subs_xml = _to_xml(_wrap_rpc(root))

    try:
        NETCONF_OBJ[session_name].send_xml(subs_xml)
    except Exception as inst: 
        raise AssertionError("Fail to send netconf rpc, expect: '%s' " % inst)


def netconf_set_command_timeout (timeout,session_name='first_netconf_session'):
    NETCONF_OBJ[session_name].set_netconf_command_timeout ( int(timeout) )

class tls_netconf (object):

    def __init__(self,**args):
        self.session_name = args.setdefault('session_name','tls_netconf_session')
        self.hello_xml = args.setdefault('hello_msg',HELLO_XML)
        self.timeout = args.setdefault('timeout',600)
        self.mgmt_param = {}
        # these params for tls
        self.ip = args.setdefault('ip','127.0.0.1')
        self.port = args.setdefault('port','6513')
        self.ca_cert = args.setdefault('ca_cert',None)
        self.keyfile = args.setdefault('keyfile',None)
        self.certfile = args.setdefault('certfile',None)
        self.ciphers = args.setdefault('ciphers',None)
        self.sub_process = args.setdefault('connect_only',False)
        self.delimiter = ']]>]]>'
        self.ssl_sock = ""
        self.child_proc = ""
        self.command_timeout = 5

        if args.has_key('ssl_version'):
            self.ssl_version = eval(args['ssl_version'])
        else:
            self.ssl_version = ssl.PROTOCOL_TLSv1_2
    
        logger.debug("%s: init " % __name__)

    def _send_hello (self):
        # send hello message
        self.send_xml(self.hello_xml)
        # get output
        ret = self.get_output (self.command_timeout)
        
        return ret

    def xml_post_operation (self,operation,xml_str):
        return xml_str

    def netconf_connect (self,**args) :
        timeout = args.setdefault('timeout',self.timeout)
        
        try:
            # invoke a subprocess to listen for tcp connection
            s = socket.socket(socket.AF_INET, socket.SOCK_STREAM) 
            s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            s.settimeout(timeout)
            s.bind((self.ip,int(self.port)))
        except socket.error:
            logger.info("the IP address/port has already been bind to socket")
        try:
            s.listen(1)    
            sock,addr = s.accept()         
        except Exception as inst:
            raise AssertionError("fail to establish tcp connection! \
            exception: %s" % (inst))
        try:
            self.ssl_sock = ssl.wrap_socket(sock,ca_certs=self.ca_cert,ssl_version=self.ssl_version,\
                                        keyfile=self.keyfile,certfile=self.certfile,ciphers=self.ciphers,cert_reqs=ssl.CERT_REQUIRED)
            self.ssl_sock.setblocking(0)
        except Exception as inst:
            raise AssertionError("fail to establish tls connection! \
            exception: %s" % (inst))

        # check DUID
        cert = self.ssl_sock.getpeercert()
        logger.debug("server certificate %s" % cert)

        # send hello message
        ret = self._send_hello()
        logger.info("tls to netconf server established.")
     
        # start read from socket for heartbeat
        if self.sub_process:
            self.child_proc = Process(target=self._read_from_socket, args=())
            self.child_proc.start()
   
        return ret
 
    def netconf_disconnect (self) :
        if self.child_proc != "":
            self.child_proc.terminate()  
        try :
            self.ssl_sock.shutdown(socket.SHUT_RDWR)
            self.ssl_sock.close()
        except Exception as inst:
            logger.info("fail to disconnect netconf session! \
            exception: %s" % (inst))
 
        return
        
    def netconf_operation (self,cmd="",**args):
        timeout = args.setdefault('timeout',self.command_timeout)
        hangup_time = args.setdefault('hangup_time',0) 

        # read from socket for heartbeat
        try:
            self._read_from_socket(hangup_time)
        except Exception as inst:
            logger.debug(inst)
        
        root = _from_cmd_to_xml(cmd) 
        try:
            self.send_xml(_to_xml(_wrap_rpc(root)))
            ret = self.get_output()           
            return ret
        except Exception as inst:
            logger.debug(inst)
            self.netconf_disconnect()
            self.netconf_connect(timeout=self.timeout)        
            try:
                self.send_xml(_to_xml(_wrap_rpc(root)))
                ret = self.get_output(timeout)
            except Exception as inst:
                ret = inst

        return ret

    def check_result (self,ret):
        if '<rpc-error>' in ret:
            return ('FAIL','RPCError')
        elif '<rpc-reply' in ret:
            return ('PASS','RPCReply')
        elif '<notification' in ret:
            return ('PASS','NOTIF')
        else:
            return ('FAIL','Other') 
    
    def send_xml (self,xml,timeout=2):
        # will add message delimiter
        wobj = self.ssl_sock
        count = 0
        while True:
            try:
                logger.debug("send xml to netconf server:\n %s" % etree.tostring(etree.fromstring(xml),pretty_print=True))
                wobj.sendall(xml + self.delimiter)
                break
            
            except Exception as inst:
                count += 1
                a,wlist,c = select.select([],[self.ssl_sock],[],1)
                if wlist == []:
                    if count == timeout:
                        raise AssertionError("The ssl socket is not writable!")
                    else:
                        continue
                wobj = wlist[0]
        return

    def get_output (self,timeout=0):
        """
        tls output
        """
        if timeout == 0 :
            timeout == self.command_timeout
        robj = self.ssl_sock
        data = ""
        rlist = []
        cur_time = time.time()
        start_time = cur_time
        res = 'PASS'
        while (cur_time - start_time) <= timeout:
            try:
                data += robj.recv(4096)
            except Exception as inst:
                res,data = self._check_data_complete(data)
                if res == 'PASS':
                    break
                rlist,b,c = select.select([self.ssl_sock],[],[],1)
                if rlist != []:
                    robj = rlist[0]        
            cur_time = time.time()
  
        time_taken = time.time() - start_time
        logger.debug ( "NETCONF response time: %s seconds" % str(time_taken))
        
        if res == 'FAIL':
            logger.debug("(could not tide up) NETCONF REPLY << %s" % data)
        else:
            logger.debug("NETCONF REPLY << %s" % etree.tostring(etree.fromstring(data),pretty_print=True))
        if data == "":
            raise AssertionError("no output gotten from SSL socket!")
        return data

    def _check_data_complete(self,data):
        data = data.replace(self.delimiter,'')
        data = re.sub('>\s+','>',data)
        data = re.sub('\s+<','<',data)
        try:
            etree.fromstring(data)
            return ('PASS',data)
        except:
            return ('FAIL',data)

    def _read_from_socket(self,keep_time=3600):
        count = int(keep_time)
        while count > 0:
            count -= 1
            try:
                self.ssl_sock.recv(4096)
            except Exception as inst:
                time.sleep(1)

class ssh_netconf (object):
    def __init__ (self, **connect_info):
        self.ip = connect_info.setdefault('ip', '127.0.0.1')
        self.port = connect_info.setdefault('port', 830)
        self.password = connect_info.setdefault('password', 'admin')
        self.second_password = connect_info.setdefault('second_password', 'Netconf#150')
        self.username = connect_info.setdefault('username', 'admin')
        self.hello_xml = connect_info.setdefault('hello_xml', HELLO_XML)
        self.timeout = connect_info.setdefault('timeout', 600)
        self.mgmt_param = {}
        self.delimiter = "]]>]]>"
        self.transport = ""
        self.channel = ""
        self.client = ""
        self.command_timeout = 60
    
    def set_netconf_command_timeout (self,timeout):
        self.command_timeout = timeout

    def _send_hello (self):
        # send hello message
        self.send_xml(self.hello_xml)
        # get output
        ret = self.get_output (self.command_timeout)
        return ret

    def netconf_connect (self,**args):
        timeout = args.setdefault('timeout', self.timeout)
        paramiko.util.log_to_file("filename.log")
        self.client=paramiko.SSHClient()	
        self.client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        inst = ""
        connect_flag = False
        for i in range(0,int(timeout)/60) :
            try :
                time1 = time.time()
                logger.info("connect:%s, username:%s,password:%s" % (i,self.username,self.password))
                self.client.connect(self.ip,port=int(self.port),username=self.username,password=self.password,allow_agent=False,look_for_keys=False)
                self.transport=self.client.get_transport()
                self.channel=self.transport.open_session()
                self.channel.invoke_subsystem('netconf')
                ret = self._send_hello()
                connect_flag = True
                logger.info("ssh to netconf server established.")               
                break
            except Exception as inst:
                if re.search("Authentication failed",str(inst)) :
                    break
                time2 = time.time()
                if (int(60+time1-time2)>0):
                    time.sleep(int(60+time1-time2))
                logger.debug("fail to connect to netconf server with default password! Exception: %s" % inst)
       
        if connect_flag :
            try :
                editconfig=etree.Element('edit-config')
                target=etree.SubElement(editconfig,'target')
                running=etree.SubElement(target,'running')
                config=etree.SubElement(editconfig,'config',nsmap={None:'http://tail-f.com/ns/config/1.0'})
                aaa=etree.SubElement(config,'aaa',nsmap={None:'http://tail-f.com/ns/aaa/1.1'})
                authentication=etree.SubElement(aaa,"authentication")
                users=etree.SubElement(authentication,"users")
                user=etree.SubElement(users,"user")
                name=etree.SubElement(user,"name")
                name.text="admin"
                password=etree.SubElement(user,"password")
                password.text=self.second_password                   
                config_xml=etree.tostring(editconfig,pretty_print=True)
                self.send_xml(_to_xml(_wrap_rpc(editconfig)))
                result = self.get_output(60)
                res,message = self.check_result(result)
                if res == "PASS" :
                    logger.info("Change password Netconf#150 successfully !")
            except Exception as inst:
                logger.warn("Fail to change password Netconf#150,Exception: %s" % inst)                      
            return ret
        elif ('Authentication failed' in str(inst)):
            for i in range(0,5) :
                try :
                    logger.info("connect:%s, username:%s,password:%s" % (i,self.username,self.second_password))
                    self.client.connect(self.ip,port= \
                    int(self.port),username=self.username,password=self.second_password,allow_agent=False,look_for_keys=False)
                    self.transport=self.client.get_transport()
                    self.channel=self.transport.open_session()
                    self.channel.invoke_subsystem('netconf')
                    ret = self._send_hello()
                    connect_flag = True
                    logger.info("ssh to netconf server established with second password.")               
                    return ret
                except Exception as inst :
                    logger.debug("fail to connect to netconf server with second password! Exception: %s" % inst)    
                    time.sleep(int(i*5)) 
            raise AssertionError("fail to ssh netconf server! \
            exception: %s" % (inst))

    def send_xml (self,xml,timeout=10):
        # will add message delimiter
        for i in range(0,timeout):
            if self.channel.send_ready():
                try:
                    logger.debug("send xml to netconf server:\n %s" % etree.tostring(etree.fromstring(xml),pretty_print=True))
                    self.channel.sendall(xml + self.delimiter)
                    break
                except Exception as inst:
                    raise AssertionError("fail to send xml message! Exception: %s" % inst)
            time.sleep(0.1)

        return

    def xml_post_operation (self,operation,xml_str):
        return xml_str

    def get_output (self,timeout=0,delay_time=0):
        """
        ssh output
        """
        time.sleep (delay_time)
        if timeout == 0 :
            timeout = self.command_timeout
        logger.debug ("get output with timeout="+ str(timeout))   
        data = ""
        current_time = time.time()
        start_time = current_time
        end_time = current_time + int(timeout)
        
        while current_time <= end_time :
            while self.channel.recv_ready():
                data += self.channel.recv(4096)
            if self.delimiter in data:
                break
            current_time = time.time()
            time.sleep (0.01)
            
        time_taken = time.time() -start_time
        logger.debug ( "NETCONF response time: %s seconds" % str(time_taken))
        
        if self.delimiter not in data:
            logger.debug("no %s in %s" % (self.delimiter,data))

        data = data.replace(self.delimiter,'')
        data = re.sub('>\s+','>',data)
        data = re.sub('\s+<','<',data)
        try:
            logger.debug("NETCONF REPLY << %s" % etree.tostring(etree.fromstring(data),pretty_print=True))
        except:
            logger.debug("(could not tide up) NETCONF REPLY << %s" % data)
        if data == "":
            raise AssertionError("no output got from socket!")
        return data
   
    def netconf_operation (self,cmd="",**args):
    
        cmd_timeout = args.setdefault('timeout',self.command_timeout)
        
        root = _from_cmd_to_xml(cmd)
        try:
            self.send_xml(_to_xml(_wrap_rpc(root)))
            ret = self.get_output(cmd_timeout)
            return ret
        except Exception as inst:
            try:
                self.netconf_disconnect()
            except:
                logger.debug("Fail to disconnect netconf session for reopen")
            self.netconf_connect(timeout=self.timeout)        
        try:
            self.send_xml(_to_xml(_wrap_rpc(root)))
            ret = self.get_output(cmd_timeout)
            return ret
        except Exception as inst:
            return inst

    def check_result (self,ret):
        if '<rpc-error>' in ret:
            return ('FAIL','RPCError')
        elif '<rpc-reply' in ret:
            return ('PASS','RPCReply')
        elif '<notification' in ret:
            return ('PASS','NOTIF')
        else:
            return ('FAIL','Other')
   
    def netconf_disconnect (self):
        root = etree.Element("close-session")
        
        self.channel.send(_to_xml(_wrap_rpc(root)))
        self.transport.close()
        self.channel.close()
        
def _from_cmd_to_xml(cmd):
    capability_cmd = ''
    if 'capabilities' in cmd:
        capability_cmd = cmd.split('capabilities:')[1]
    
    cmd = cmd.split('capabilities:')[0]
    cmd = cmd.strip('\\r\\n').strip('\\n').strip('\\r')
    operation = re.search("(\S+?)\(",cmd).group(1)
    operation = operation.replace('_','-')
    params = re.search("\(([\s\S]*)\)",cmd).group(1)
    params = params.replace('\',\'','\';\'')

    leaf_group = ['running','candidate','startup']
    key_group = ['target','config','default_operation','error_option','test_option',\
                 'source','filter','confirmed','persist','persist-id','timeout','identifier',\
                 'version','format','session-id']
        
    root = etree.Element(operation)   
    param_list = []
    param_grp = []
  
    if params:
        param_grp =  params.split(',')        
    for single_param in param_grp:
        regexp = re.search("(\S+)=([\s\S]*)",single_param)
        if regexp and regexp.group(1) in key_group:
            param_list.append(single_param)
        else:
            param_list[-1] = ','.join([param_list[-1],single_param])
                     
    for single_param in param_list:
        regexp = re.search("(\S+)=([\s\S]*)",single_param)
        if regexp :
            key = regexp.group(1)
            value = regexp.group(2) 
            key = key.replace('_','-')
            value = value.strip('(').strip(')').strip('\'')
            # special for confirm
            key = key.replace('timeout','confirmed-timeout')
            # special for get/get-config
            if key == 'filter':
                value_group = value.split(';')
                filter_type = value_group[0].strip('\'')
                value = value_group[-1].strip('\'')
                child = etree.SubElement(root,key,{'type':filter_type})
            else:       
                child = etree.SubElement(root,key)
            # if value is xml format
            if value == 'True' or value == 'False':
                continue
            elif value in leaf_group:
                child.append(etree.Element(value))
            elif '<' in value and '>' in value:
                child.append(etree.fromstring(value))
            else:
                child.text = value

    for capability in capability_cmd.split(','):
        try:
            key,value = capability.split('=')
            child = etree.SubElement(root,key)
            child.text = value
        except:
            continue     
    return root

def _change_format (search_string,style=1) :
    search_list = search_string.split(",")       
    index = 0
    search_list_xml = []
    list_xml = []   
    """         
    for index in range(0,len(search_list)) :
        search_list_xml.append(search_list[index].split(" "))
        if len(search_list_xml[index]) > 2 :
            search_list_xml[index] = [search_list_xml[index][0],search_list_xml[index][-1]]
        list_xml.append('<'+search_list_xml[index][0]+'>'+ \
        search_list_xml[index][1]+'</'+search_list_xml[index][0]+'>')
    """
    for item in search_list:
        try :
            key,value = item.split(" ")
        except Exception as inst :
            raise AssertionError ("input format error, should be 'key value, key value'")
        else :
            if style == 1:
                list_xml.append('<'+key+'>'+ value+'</'+key+'>')
            elif style == 2:
                list_xml.append('<\w+\:'+key+'>'+ value+'</\w+\:'+key+'>')
    return list_xml

def _check_all_items_return_last (search_list,lines) :
    
    found = True  
    for item in search_list :
        item = re.sub(" +"," +",item).strip ()            
        res = re.search(item,lines,re.DOTALL) 
        if not res :
            logger.debug("not found : %s" % item)
            found = False
            break
        else :
            logger.debug("found : %s" % item)            
    if found : 
        matched = res.groups()       
        if len(matched) == 0 :
            matched = (res.group(0),)
    else :
        matched = False  
    return matched 

def _check_error (data) :
    res = "PASS"
    error_info = ""
    error_list = ["<rpc-error>"]
    for error_item in error_list :
        if error_item in data :
            res = "FAIL"
            error_info = data
            break
    return (res, error_info)

def _to_xml (elem,encoding ="UTF-8",pretty_print=False):
    xml = etree.tostring(elem, encoding=encoding, pretty_print=pretty_print)
    return xml if xml.startswith('<?xml') else '<?xml version="1.0" encoding="%s"?>%s' % (encoding, xml)

def _wrap_rpc (elem):
    rpc = etree.Element('rpc',{'xmlns':BASE_NS_1_0,'message-id':'1'})
    rpc.append(elem)

    return rpc

def _partial_lock_rpc (elem):    
    rpc = etree.Element("{%s}rpc" % BASE_NS_1_0 ,{'message-id':'1'},nsmap={None:PARTIAL_NS_1_0})
    rpc.append(elem)
    return rpc


def _transfer_string_to_dict ( input_string ) :
    d = {}
    input_string = input_string.strip(',')
    str_group = input_string.split(',')
    for single_str in str_group:
        key,value = single_str.split('=')
        d[key] = value

    return d

def _parse_attr(node,session_name):
    xmlns_map = XMLNS_MAP[session_name]
    res = re.search("(\S+?){",node)
    try:
        key = res.group(1)
    except:
        key = node
    key = key.strip()
    try:
        prefix,key = key.split(':')
    except:
        prefix = None
        key = key
    attr_group = re.findall("{.*?}",node)
    attr_dict = {}
    xml_dict = {}
    
    for single_attr in attr_group:
        single_attr = single_attr.strip('{}').strip()
        try:
            attr,value = single_attr.split(':')
            attr = attr.strip()
            value = value.strip()
            if _is_attr(attr):
                attr = '{%s}%s' % (_is_attr(attr),attr)
                attr_dict[attr] = str(value) 
                continue
            if xmlns_map.get(value) == None:
                raise AssertionError ("%s not found in XMLNS_MAP!" % value)      
            xml_dict[attr] = xmlns_map[value]
            
        except:
            if xmlns_map.get(single_attr) == None:
                raise AssertionError ("%s not found in XMLNS_MAP!" % single_attr)
            xml_dict[None] = xmlns_map[single_attr]
 
    return (key,xml_dict,attr_dict,prefix)

def _is_attr(key):    
    if ATTR_DICT.has_key(key):
        return ATTR_DICT[key]
    else:
        return False

def _add_ns_appendix(appendix,*nodes):
    nodes_appendix = []
    for node in nodes:
        node_appendix = re.sub("{(.*?)}","{\g<1>"+appendix+"}",node)
        logger.debug(node_appendix)
        for attr in ATTR_DICT.keys():
            node_appendix = re.sub("{("+attr+":.*?)"+appendix+"}","{\g<1>}",node_appendix)
        nodes_appendix.append(node_appendix)
    return nodes_appendix
    
def _get_filter_xml(command_index,*command_elements,**params):
    """
       to generate XML according to node hirachy
       
       2016-7-5 revision 0 by liminxi
       support space in command_element, Nov 21, chunyagu          
    """
    # preoperation before the logic if1/if2[name s1,mtu 1000]/a/b[name s2,mtu 2000]/c/d/e style
    # command_elements leaf1/leaf2[index 0]/leaf3 1
    # name s1,mtu 1000,a/b/name s2,a/b/mtu 2000
    # if1/if2/a/b/c/d/e
    command_element_append = []
    command_elements_bak = []
    mgmt_param = params.setdefault('mgmt_param',{})
    session_name = params.setdefault('session_name','first_netconf_session')
 
    if mgmt_param != {}:
        try:
            mgmt_prefix = mgmt_param['ns_prefix']
            mgmt_appendix = mgmt_param['ns_appendix']
            command_index = mgmt_prefix + _add_ns_appendix(mgmt_appendix,command_index)[0]
            command_elements = _add_ns_appendix(mgmt_appendix,*command_elements)
        except:
            logger.debug("Can not parse netconf mgmt param, please check the input!")

    index_list = re.findall('\[\w.*?\]',command_index)
    for single_index in index_list:
        str_len = len(single_index)
        str_index = command_index.find(single_index)
        end_index = str_index + str_len
        node2insert = command_index[0:str_index].strip('/')
        for one_index in single_index.split(','):
            one_index = one_index.strip('[').strip(']')
            if node2insert:
                one_index = '/'.join([node2insert,one_index])
            command_element_append.append(one_index)
        command_index = command_index[:str_index] + command_index[end_index:]
 
    node2insert = command_index.strip('/')
    for element in command_elements:
        if node2insert: 
            element = '/'.join([node2insert,element])
        command_elements_bak.append(element)
    command_elements_bak = command_element_append + command_elements_bak
    if command_elements_bak == []:
        command_elements_bak = [node2insert]

    # start to insert the node
    # ['if1/if2/name s1', 'if1/if2/mtu 1000', 'if1/if2/a/b  s2 abc ', 'if1/if2/a/b/mtu 2000', 'if1/if2/a/b/mtu 2000', 
    # 'if1/if2/a/b/c/d/e/leaf1 1',   'if1/if2/a/b/c/d/e/leaf2 2',   'if1/if2/a/b/c/d/e/leaf3 3']
    parent = {}
    xmlns_whole_dict = {}
    instance_parent = []
    instance_list = []
    key_list = []
    for elements in command_elements_bak:
        list_2_elem = elements.split(" ", 1)
        element =list_2_elem[0]
        try :
            value = list_2_elem[1]
        except Exception as inst :
            value = None
            pass
        element_grp = element.split('/')

        for i,single_key in enumerate(element_grp):    
            is_instance = False
            is_new_node = True
            pure_key,xmlns_dict,attr_dict,prefix = _parse_attr(single_key,session_name)  
            pure_key_xmlns = pure_key
            xmlns_whole_dict.update(xmlns_dict)
            if prefix and xmlns_whole_dict.get(prefix):
                pure_key_xmlns = '{%s}%s' % (xmlns_whole_dict[prefix],pure_key_xmlns)        
            # first root node
            if i == 0:
                if not parent:
                    root = etree.Element(pure_key_xmlns,attr_dict,nsmap=xmlns_dict)
                    parent[0] = root
                continue    

            # not first node    
            absolute_path = '/'.join(element_grp[1:i+1])
            absolute_path = re.sub('{.*?}','',absolute_path)
            if i == len(element_grp) - 1:
                if (absolute_path in instance_list) or (element.rstrip(single_key) not in instance_parent):
                    is_instance = True
                    instance_list.append(absolute_path)
                    instance_parent.append(element.rstrip(single_key))

            if root.find('./'+absolute_path) is None:
                parent[i] = etree.SubElement(parent[i-1],pure_key_xmlns,attr_dict,nsmap=xmlns_dict)
                if is_instance:
                    key_list = []
                    key_list.append(absolute_path)
                else:
                    key_list.append(absolute_path)
            else:
                # if a instance
                if is_instance:                    
                    key_list = []
                    key_list.append(absolute_path)
                    # need insert its parent node again
                    if parent[i-1].find(pure_key) is not None:
                        parent_key_xmlns,parent_xmlns_dict,parent_attr_dict,parent_prefix = _parse_attr(element_grp[i-1],session_name)
                        if parent_prefix and xmlns_whole_dict.get(parent_prefix):
                            parent_key_xmlns = '{%s}%s' % (xmlns_whole_dict[parent_prefix],parent_key_xmlns)    
                        parent[i-1] = etree.SubElement(parent[i-2],parent_key_xmlns,parent_attr_dict,nsmap=parent_xmlns_dict)
                    parent[i] = etree.SubElement(parent[i-1],pure_key_xmlns,attr_dict,nsmap=xmlns_dict)
                else:                  
                    # not instance
                    for key in key_list:
                        if re.match(absolute_path,key): 
                            is_new_node = False    
                    if is_new_node or i == len(element_grp)-1:
                        parent[i] = etree.SubElement(parent[i-1],pure_key_xmlns,attr_dict,nsmap=xmlns_dict)
                        key_list.append(absolute_path)

        # check if this node has value
        if value:
            parent[i].text = value   

    xml_string = etree.tostring(root)
    logger.debug("xml message:\n %s" % etree.tostring(root,pretty_print=True))
    return xml_string

def _merge_two_list(list1,list2) : 
    
    list3 = list2

    for a in list1:
        isFound = False
        for b in list2:
            if a == b :
                isFound = True
                break
        if not isFound :
            list3.append(a)
    return list3
 
def _collect_yang_info_from_yang_library (sRpc) :  

    if not re.search ("ietf-yang-library", sRpc) :
        raise AssertionError ("wrong yang-library RPC: %s", sRpc)

    xmlns_map = {}
    d = {}
    l = []
    sModuleEndLable = '</module>'      
    m = re.search('(</[^<]*module>)',sRpc)   
    if m :
        sModuleEndLable = m.group(1)

    lModules = sRpc.split(sModuleEndLable)

    for eachModule in lModules:
        """
        # skip all submodules
        eachModule = re.sub('<[^<]*submodule>.*</[^<]*submodule>','',eachModule)
        """
        
        # get deviation string
        sDeviation = ''
        m = re.search('<[^<]*deviation>(\S+)</[^<]*deviation>',eachModule)
        if m :
            sDeviation = m.group(1)
            eachModule = re.sub('<[^<]*deviation>.*</[^<]*deviation>','',eachModule)

        # get submodule string
        sSubmodule = ''
        m = re.search('<[^<]*submodule>(\S+)</[^<]*submodule>',eachModule)
        if m :
            sSubmodule = m.group(1)
            eachModule = re.sub('<[^<]*submodule>.*</[^<]*submodule>','',eachModule)

        # set conformance-type string
        sConformanceType = ''
        m = re.search('<[^<]*conformance-type>([^<]+)</[^<]*conformance-type>',eachModule)
        if m :
            d['conformance-type'] = m.group(1)

        # set revision string
        sRevision = ''
        m = re.search('<[^<]*revision>([^<]+)</[^<]*revision>',eachModule)
        if m :
            d['revision'] = m.group(1)

        # get features string
        sFeature = ''
        m = re.search('<[^<]*feature>(\S+)</[^<]*feature>',eachModule)
        if m :
            sFeature = m.group(1)
            eachModule = re.sub('<[^<]*feature>.*</[^<]*feature>','',eachModule)

        # set namespace
        m = re.search('<[^<]*namespace>(\S+)</[^<]*namespace>',eachModule)
        if m :
            d['namespace'] = m.group(1)

        # set module name
        # should search like '<name>(\S+)</name>' or '<yanglib:name>(\S+)</yanglib:name>'
        m = re.search('<[^<]*name>(\S+)</[^<]*name>',eachModule)
        if m :
            module_name = m.group(1)
            d['module'] = module_name
            xmlns_map[module_name] = d['namespace']

        # set deviation
        if sDeviation:

            lDeviation = sDeviation.split('</deviation>')
            for eachDeviation in lDeviation:

                dict_deviation = {}
                m = re.search('<[^<]*name>(\S+)</[^<]*name>',eachDeviation)
                if m :
                    dict_deviation['name'] = m.group(1)
                n = re.search('<[^<]*revision>(\S+)</[^<]*revision>',eachDeviation)
                if n :
                    dict_deviation['revision'] = n.group(1)
                
                if m :
                    deviation_name = 'deviations_' + dict_deviation['name']
                    d [deviation_name] = dict_deviation 

        # set submodule
        if sSubmodule:

            lSubmodule = sSubmodule.split('</submodule>')
            for eachModule in lSubmodule:
                dict_submodule = {}
                m = re.search('<[^<]*name>(\S+)</[^<]*name>',eachModule)
                if m :
                    dict_submodule['name'] = m.group(1)
                n = re.search('<[^<]*revision>(\S+)</[^<]*revision>',eachModule)
                if n :
                    dict_submodule['revision'] = n.group(1)
                
                if m : 
                    submodule_name = 'submodule_' + dict_submodule['name']
                    d [submodule_name] = dict_submodule 

        # set features
        if sFeature:
            sFeature = re.sub('</[^<]*feature><[^<]*feature>',',',sFeature)
            sFeature = re.sub('</[^<]*feature>',',',sFeature)
            sFeature = re.sub('<[^<]*feature>',',',sFeature)
            #sFeature.strip('<feature>').strip('</feature>')
            if sFeature :
                d ['features'] = sFeature
             
        l.append(d)
        d = {}
        
    return (l,xmlns_map)
        
def _collect_yang_info_from_hello (hello) :  

    if not re.search ("<capability>.*</capability>", hello) :
        raise AssertionError ("wrong hello: %s", hello)

    xmlns_map = {}
    d = {}
    l = []         
    for capability in re.findall("<capability>(.*?)</capability>",hello) :
        capab_list = capability.split ("?")      
        d['namespace'] = capab_list[0]
        if len(capab_list) > 1 :
            capab_params = capab_list[1].strip ("'").strip('"')
            m=re.search("module=([^;]+)",capab_params)
            if m :
                module_name = re.sub("&amp","",m.group(1))
                d ['module'] = module_name
                xmlns_map[module_name] = d['namespace']

            m=re.search("conformance-type=([^;]+)",capab_params)
            if m :
                d ['conformance-type'] = m.group(1)
            m=re.search("revision=([^;]+)",capab_params)
            if m :
                d ['revision'] = m.group(1)
            m=re.search("deviations=([^;]+)",capab_params)
            if m :
                d ['deviations'] = m.group(1)               
            m=re.search("features=([^;]+)",capab_params)
            if m :
                d ['features'] = m.group(1)
               
        l.append(d)
        d = {}
        
    return (l,xmlns_map)
      
def get_yang_info (module=None,namespace=None,session_name='first_netconf_session') :
    """
      return the capabilities 
    """

    global YANG_INFO
    if module :
        search_key = "module"
        search_value = module
    elif namespace :
        search_key = "namespace"
        search_value = namespace
    else :
        search_key = None
        
    if not search_key :
        return YANG_INFO[session_name]   
    for item in YANG_INFO[session_name] :
        if item[search_key] == search_value :
            return item
    else :
        raise AssertionError ("no found yang with '%s:%s'" % (search_key,search_value))


