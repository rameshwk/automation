import re
import time
from robot.api import logger

try :
    import pexpect
except ImportError :
    pexpect_path = os.environ.get('ROBOTREPO')+'/TOOLS/pexpect-2.3_1'
    sys.path.append(pexpect_path)
    import pexpect

class com_ssh(object):
    def __init__(self,ip="127.0.0.1",port="50023",\
        username="ONTUSER",password="SUGAR2A041",prompt='\[.*\](#|\$|%|~)+',timeout=60):
        self.ip=ip.encode("ascii")
        self.port=port.encode("ascii")
        self.username=username.encode("ascii")
        self.password=password.encode("ascii")
        self.prompt=prompt.encode("ascii")
        self.timeout=timeout
        if self.ip == "127.0.0.1" :
            raise AssertionError("%s -> please give correct ip" % __name__)
        logger.debug ("com_ssh: ip=%s,port=%s,username=%s,password=%s,prompt=%s,timeout=%d" \
        % (self.ip,self.port,self.username,self.password,self.prompt,self.timeout))

    def _init_ssh(self):
        """
        Intialize a SSH session and provide a Session available
        """
        keyword_name = '_init_ssh'

        ssh_command = "ssh -l "+self.username+" "+self.ip +" -p "+self.port
        
        #logger.info("exec command: '%s'" % ssh_command )
        #logger.info("SSH CMD >> '%s'" % ssh_command)
        logger.info("<span style=\"color: #4876FF\">"+"SSH CMD >> '%s'" % ssh_command+"</span>",html=True)



        self.interface = pexpect.spawn(ssh_command,timeout=int(self.timeout))
        time.sleep(3)
        expect_value = self.interface.expect(\
        ['Are you sure you want to continue connecting \(yes\/no\)\?',\
        'assword:',\
        'Host key verification failed',\
        'Connection Refused!',\
        'Unable to connect',\
        'closed by remote host',\
        pexpect.EOF,pexpect.TIMEOUT])
        logger.debug("gotten return: '%s'" % (self.interface.after))

        if expect_value == 0:
            logger.debug("send: yes")
            self.interface.sendline("yes")
            self.interface.expect(['.*assword:', pexpect.EOF,pexpect.TIMEOUT])
            logger.debug("gotten return: '%s'" % (self.interface.after))
            logger.debug("send: %s" % (self.password))
            self.interface.sendline(self.password)
        elif expect_value == 1 : 
            logger.debug("send: %s" % (self.password))
            self.interface.sendline(self.password)
        elif expect_value == 2 :
            if self.port == "22" :
                cmd = "ssh-keygen -R " + self.ip
            else :
                cmd = "ssh-keygen -R [" + self.ip + "]:" + self.port
            logger.debug ( "exec '" + cmd + "' to clean inconsistent public key" )
            res = pexpect.run ( cmd ) 
            logger.debug (str(res))
            self.interface.close()
            raise AssertionError ("inconsistent public key cleaned, please retry")
        elif expect_value >= 3 :
            self.interface.close()
            raise AssertionError("%s -> unexpected error: %s" % (__name__,str(self.interface.after)))
            
        try :      
            expect_value = self.interface.expect([self.prompt,\
            pexpect.EOF, pexpect.TIMEOUT],30)
        except Exception as inst:     
            raise AssertionError ("fail to get prompt after giving password")
        logger.debug("expect prompt: '%s'" % (self.prompt))   
        logger.debug("gotten return: '%s'" % (self.interface.after))
        if expect_value != 0:
            self.interface.close()
            raise AssertionError("%s-> expect '%s' but gotten : %s" % (__name__,self.prompt,self.interface.after))
         

    def open_ssh(self):
        """
        To Open the SSH SESSION , SESSION FAILS IT WILL RETRY TO CONNECT 
        RETURN VALUE :  pass | fail
        Author : 
        BENO K IMMANUEL         Developed """

        keyword_name = 'open_ssh'
        iretry_count = 4

        i = 0
        while (i < iretry_count) :
            i = i+1
            try :
                self._init_ssh()
            except Exception as inst :
                logger.debug("%s:%s-> open ssh failed for the %s th time" % (__name__,keyword_name,str(i)))
                logger.debug(str(inst))
                if i < 4 : 
                    time.sleep(10)
                continue
            else :
                break
            
        if i == iretry_count :    
            raise AssertionError ("%s:%s -> open ssh failed: %s" \
            % (__name__,keyword_name,inst))
  
        try :       
            self.interface.sendline("alias ls='ls --color=never'")
            logger.debug ("send \"alias ls='ls --color=never'\"")  
            expect_value = self.interface.expect([self.prompt,\
            pexpect.EOF, pexpect.TIMEOUT],10)
        except Exception as inst:
            logger.debug ("fail to get prompt\n exception: " + str (self.interface.after))
        else :
            logger.debug("gotten return: '%s'" % self.interface.after)   

        
            
    def send_command(self,command,timeout=0,prompt=None):
        """
        Send the Command to the SSH session 

        """
        
        keyword_name = 'send_command'
        if prompt == None:
            prompt = self.prompt

        # remove all '.*' in prompt
        #prompt = re.sub('\.\*','',prompt)
           
        if timeout == 0 :
            expect_timeout = self.timeout
        else :
            expect_timeout = timeout

        if expect_timeout == 0 :
            expect_timeout = 1
            
        # cleanup the buffer
        expect_value = None
        try :
            expect_value = self.interface.expect\
            ([prompt,pexpect.EOF,pexpect.TIMEOUT],0.05)
        except Exception as inst :
            logger.debug ("unexpected error:\n" +str(self.interface.after))
            logger.debug ("re-connecting....")
            self.interface.close()
            time.sleep(3)
            try :    
                self.open_ssh()
            except Exception as inst :
                raise AssertionError ("%s:%s -> open ssh failed: %s" \
                % (__name__,keyword_name,inst))
            self.interface.sendline("\n")                          
            expect_value = self.interface.expect\
              ([prompt,pexpect.TIMEOUT],0.05)
 
        sCleanBuf = ''             
        if expect_value == 0 :
            sCleanBuf = self.interface.after
            if self.interface.before :
                sCleanBuf = self.interface.before + self.interface.after
            logger.info("dump left info:\n" + sCleanBuf)
        elif expect_value > 0 :
            sCleanBuf = self.interface.before
            if sCleanBuf : 
                logger.info("dump left info (before):\n" + sCleanBuf)    
            else :
                logger.info("dump left info (before):\n" + sCleanBuf)     

        self.interface.sendline(command)

        startTime = time.mktime(time.localtime()) 
        endTime = startTime + int(expect_timeout)
        sRecv = ''
        while time.mktime(time.localtime()) < endTime :
            #logger.warn("enter while!!")
            #logger.warn("sRecv:%s" % sRecv)
            logger.trace("timeout:%s" % str(float(expect_timeout)/100.0))
            try : 
                expect_value = self.interface.expect\
                    (['[\s\S]+',pexpect.EOF,pexpect.TIMEOUT],float(expect_timeout)/100.0)
            except Exception as inst :
                s = sys.exc_info()
                raise AssertionError ("unexpected error when sending cmd, lineno: %s, exception: %s: \nsend: %s\nrecv:%s\n" 
                % (s[2].lineno,inst,command,str(self.interface.before)))

            if expect_value == 0 :
                sRecv = sRecv + self.interface.after
                #logger.warn("sRecv:%s" % sRecv)
                #logger.warn("Prompt:%s" % prompt)
                if self.interface.before :
                    logger.warn("found not empty before:%s" % self.interface.before)

                # don't return when prompt is received. still receive data until encounter recv timeout (that means there is no data output )
                #if re.search(prompt,sRecv) :
                #    return sRecv
               
            elif expect_value == 1 :
                logger.debug ("encountered eof:\n" +str(self.interface.after))
                logger.debug ("re-connecting....")
                self.interface.close()
                time.sleep(3)
                try :    
                    self.open_ssh()
                except Exception as inst :
                    raise AssertionError ("%s:%s -> open ssh failed: %s" \
                    % (__name__,keyword_name,inst))                
                logger.debug ( "re-send command: %s" % command) 
                self.interface.sendline(command)
                expect_value = self.interface.expect\
                    ([prompt,pexpect.EOF,pexpect.TIMEOUT],int(expect_timeout))
                if expect_value == 0 :
                    return str(self.interface.before)+str(self.interface.after)
                else :
                    raise AssertionError ("encountered timeout or eof for se-send command:\n" +str(self.interface.after))
            elif expect_value == 2 :
                logger.debug ("encountered timeout (%s/100 seconds): %s\n" % (expect_timeout,prompt))
                logger.debug ("BEFORE:%s" % str(self.interface.before))
                # if encounter timeout and sRecv has prompt, then return.
                if re.search(prompt,sRecv) :
                    return sRecv
                time.sleep(0.01)

        if not re.search(prompt,sRecv) :
            logger.debug("Can't find prompt '%s' in reply! " % prompt)
    
        # maybe encounter the not closed '}])' in command, to exit command.
        if '\n> ' in sRecv :
            # send ctrl+c to exit 
            etx = '\x03'
            logger.warn("send ctrl+c command to exit the current command!") 
            self.interface.sendline(etx)
        return sRecv

    def get_output_raw(self,command,timeout=0.1):
        """
        Send the Command to the SSH session and get raw output content 

        """
        
        keyword_name = 'get_ssh_output_raw'
              
        # cleanup the buffer
        expect_value = self.interface.expect\
          ([self.prompt,pexpect.TIMEOUT],0.05)

        if expect_value == 0 :
            logger.debug(str(self.interface.after))
        elif expect_value > 0 :
            logger.debug(str(self.interface.before))        

        self.interface.sendline(command)
        expect_value = self.interface.expect\
            ([pexpect.EOF,pexpect.TIMEOUT],int(timeout))

        return self.interface.before

                   
    def close_ssh(self):
        """
        logout the created ssh session
        """
        try : 
            self.interface.sendline('exit')
            self.interface.close()
        except Exception as inst :
            logger.warn("Failed to close ssh session!")

            
            
