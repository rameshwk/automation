import os,sys,time,re
import com_ssh
from robot.api import logger

try :
    import pexpect
except ImportError :
    pexpect_path = os.environ.get('ROBOTREPO')+'/TOOLS/pexpect-2.3_1'
    sys.path.append(pexpect_path)
    import pexpect

      
SSH_SESSION = {}

def connect_ssh (ip='127.0.0.1',port='50023',username='ONTUSER',password='SUGAR2A041', \
    prompt='\[.*\](#|\$|%|~)+',session_name="first_ssh_session",timeout=60) :
    """ 
    Build up the SSH session for sending command to the session
    - *ip:* ip address of ssh server (default 127.0.0.1 will cause error)
    - *port:* port number of ssh server, default value 50023
    - *username:* name of ssh user , default value 'ONTUSER'
    - *password:* password of the ssh user, default value 'SUGAR2A041'
    - *prompt:*   meeting it in ssh session reply indicate command executed correctly
    - *session_name:* session name of the connection, default value 'first_ssh_session'     
    - *timeout:* connect retry timeout, default value is 1 mins  
    - *RETURN VALUE:* SSH_SESSION[session_name] or raise error
    
    Usage Example:
    | connect ssh | ip=135.251.22.12 | port=23 | username=xx | password=xx | prompt=.*# |
    """
    keyword_name = 'connect_ssh' 
    logger.debug ("%s:%s -> ip=%s,port=%s,username=%s,password=%s,prompt=%s,session_name=%s" \
    % (__name__,keyword_name,ip,str(port),username,password,prompt,session_name))
 
    ip = ip.encode("ascii")
    port = port.encode("ascii")
    username = username.encode("ascii")
    password = password.encode("ascii")

    try:
        SSH_SESSION[session_name] = com_ssh.com_ssh\
        (ip,port=port,username=username,password=password,prompt=prompt,timeout=timeout)
    except Exception as inst:
        raise AssertionError("%s:%s -> fail to init ssh object: %s" \
        % (__name__,keyword_name,inst))    
            
    try:
        SSH_SESSION[session_name].open_ssh ()
    except Exception as inst:
        raise AssertionError("%s:%s -> fail to open ssh session: %s" \
        % (__name__,keyword_name,inst))    
            
    logger.info ("success to create ssh session: '%s'" % str(SSH_SESSION))
    return SSH_SESSION[session_name]

def disconnect_ssh (session_name="first_ssh_session") :
    """ 
    Disconnect the SSH session
    
    Usage Example:
    | disconnect ssh |
    | disconnect ssh | second_ssh_session |

    """
    keyword_name = 'disconnect_ssh'
    try:
        SSH_SESSION[session_name].close_ssh()
    except:
        raise AssertionError("%s:%s -> fail to close SSH session: %s" \
        % (__name__,keyword_name,session_name))         
 
    else :
        logger.debug ("SSH session '%s' closed" % session_name)
    
    SSH_SESSION.pop(session_name)


def send_ssh_command(command,timeout=0,session_name="first_ssh_session",prompt=None):
    """
    *Obsoleted* : please use ssh_send_command 
    """
    keyword_name = 'send_ssh_command'
    #logger.debug ("SSH CMD >> %s " % command)
    logger.info("<span style=\"color: #00008B\">"+"SSH CMD >> %s" % command+"</span>",html=True)
    try:
        sshobj = SSH_SESSION[session_name]      
        res = sshobj.send_command(command,timeout=timeout,prompt=prompt)
    except Exception as inst:
        raise AssertionError("%s:%s -> failed SSH command '%s'\n exception: %s" \
        % (__name__,keyword_name,command,inst))        
    else :
	logger.debug ("SSH REPLY << %s" % res)

    return "pass",res


def ssh_send_command(command,timeout=0,session_name="first_ssh_session",prompt=None):
    """
    send single SSH command without checking error in output

    - *command:* ssh command 
    - *session_name:* string, default value is "first_ssh_session"
    - *prompt:* prompt to identify command end
    - *timeout:* waiting time (seconds) before check return prompt, default value is 0
    - *RETURN:* ("pass",cmd_output) 
     
    usage example:
    | ssh send command | ls -la |
    """
    keyword_name = 'send_ssh_command'
    #logger.debug ("SSH CMD >> %s " % command)
    logger.info("<span style=\"color: #00008B\">"+"SSH CMD >> %s" % command+"</span>",html=True)
    try:
        sshobj = SSH_SESSION[session_name] 
        res = sshobj.send_command(command,timeout=timeout,prompt=prompt)
    except Exception as inst:
        raise AssertionError("%s:%s -> failed SSH command '%s'\n exception: %s" \
        % (__name__,keyword_name,command,inst))        
    else :
	logger.info ("SSH REPLY << %s" % res)

    return res  

def ssh_get_output_raw(command,timeout=0,session_name="first_ssh_session"):
    """
    send single SSH command without checking error in output

    - *command:* ssh command 
    - *session_name:* string, default value is "first_ssh_session"
    - *timeout:* waiting time (seconds) before check return prompt, default value is 0
    - *RETURN:* (cmd_output) 
     
    usage example:
    | ssh_get_output_raw | ls -la |
    """
    keyword_name = 'ssh_get_output_raw'
    #logger.debug ("SSH CMD >> %s " % command)
    logger.info("<span style=\"color: #00008B\">"+"SSH CMD >> %s" % command+"</span>",html=True)
    try:
        sshobj = SSH_SESSION[session_name] 
        res = sshobj.get_output_raw(command,timeout=timeout)
    except Exception as inst:
        raise AssertionError("%s:%s -> failed SSH command '%s'\n exception: %s" \
        % (__name__,keyword_name,command,inst))        
    else :
	logger.info ("SSH REPLY << %s" % res)

    return res 
    
def ssh_send_reboot(command='reboot',timeout=120,session_name="first_ssh_session",prompt=None):
    """
    send single SSH reboot command

    - *command:* ssh command 
    - *session_name:* string, default value is "first_ssh_session"
    - *prompt:* prompt to identify command end
    - *timeout:* waiting time (seconds) before check return prompt, default value is 120
    - *RETURN:* ("pass",cmd_output) 
     
    usage example:
    | ssh send command | ls -la |
    """
    keyword_name = 'send_ssh_command'
    logger.debug ("SSH CMD >> %s " % command)
    try:
        sshobj = SSH_SESSION[session_name]      
        res = sshobj.send_reboot(command,timeout=timeout,prompt=prompt)
    except Exception as inst:
        raise AssertionError("%s:%s -> failed SSH reboot '%s'\n exception: %s" \
        % (__name__,keyword_name,command,inst))        
    else :
	logger.info ("SSH REPLY << %s" % res)

    return res  
      
def ssh_check_command (command,*output_regexp,**params):
    """
    to check if specific contents in the command output.
    - *command:* ssh command 
    - *output_regexp:* regular expression list,the match regexp to command output
    params: 
    - *session_name:* string, default value is "first_ssh_session"
    - *prompt:* prompt to identify command end
    - *timeout:* waiting time (seconds) before check return prompt, default value is 0
    - *check_time: * how long time (seconds) before return not found
    - *expect_result:* PASS or FAIL, default value is "PASS" 
     
    usage example:
    | ssh check command | ls -la | temp.txt | \.bashrc |
    | @{date_of_file} | ssh check command | ls -la ~ | \ +(\\S+)\ +(\\d+)\ +(\\d+) +\\.bashrc |
    """

    session_name = params.setdefault('session_name',"first_ssh_session")
    timeout = params.setdefault('timeout', 0)
    check_time = params.setdefault('check_time', 0)
    expect_result = params.setdefault('expect_result',"PASS").upper() 
    prompt = params.setdefault('prompt', None)

    keyword_name = 'ssh_check_command'
    #logger.debug ("SSH CMD >> %s " % command)
    logger.info("<span style=\"color: #00008B\">"+"SSH CMD >> %s" % command+"</span>",html=True)
                
    if check_time > 5 :
        interval_time = int(check_time/5)
    else :
        interval_time = 1   

    current_time = time.mktime(time.localtime())
    end_time = current_time + check_time

    while current_time <= end_time : 
        try:
            info = SSH_SESSION[session_name].send_command(command,timeout=timeout,prompt=prompt)
        except AssertionError as inst:    
            raise AssertionError("failed ssh command '%s' \n exception: %s" \
            % (command, inst))
        logger.info("SSH REPLY << %s" % (info))
        search_result = "PASS"
        if not output_regexp :
            return [info]
        found = _check_all_items_return_last (output_regexp, info)
        if not found :
            search_result = "FAIL"
            logger.debug("not gotten '%s' in output" % str(output_regexp))
            logger.info("expect_result:%s, search_result:%s" \
            % (expect_result, search_result)) 
            
        if search_result == expect_result :
            return found
        else :
            time.sleep(interval_time)
            current_time = time.mktime(time.localtime())           
            continue
    else :        
        raise AssertionError("no '%s' in command '%s' output"\
        % (output_regexp,command))
    
def run_command_remote (command,ip,username,password,expect_output=None,timeout=10):
    """
    execute command on remote pc and return the output
 
    - *command:* ssh command 
    - *ip:* ip address of ssh server (default 127.0.0.1 will cause error)
    - *username:* name of ssh user , default value 'ONTUSER'
    - *password:* password of the ssh user, default value 'SUGAR2A041'
    - *expect_output:* the match regexp to command output
    - *timeout:* waiting time (seconds) before check return prompt, default value is 10
    
    Usage Example:
    | run command remote | pwd | ip=135.251.22.12 | username=xx | password=xx |
    """
    expect_timeout = timeout
    remote_cmd = "ssh "+ username+"@"+ ip+" '"+command+"'"
    try :
#        res = pexpect.run (remote_cmd, events={'(\s|\S)*password:': password+'\n'})
        res = pexpect.run (remote_cmd, events={'yes\/no\)\?':'yes\n','password:': password+'\n'},timeout=20)
    except Exception as inst :
        raise AssertionError("failed to run '%s' \nexception: %s" \
            % (remote_cmd,str(inst))) 
    if expect_output and not re.search(expect_output,res) :
        raise AssertionError("not gotten '%s' in '%s'" % (expect_output,res))
    else :          
        return res

def _check_all_items_return_last (item_list, lines) :

    found = True  
    command,lines = lines.split("\n",1)
    res_list = []

    for item in item_list : 
        res = re.search(item,lines,re.DOTALL) 
        if not res :
            logger.debug("not found : %s" % item)
            found = False
            break
        else :
            res_list.append(res)        
          
    if found :
        matched_list =[]
        for res in res_list :            
            matched = res.groups()   
            if  len(matched) > 0 :
                matched_list.extend ( list(matched) )    
        if len(matched_list) == 0 :            
            matched = res.group(0)
            matched_list = matched 
        if  len(matched_list) == 1 : 
            matched_list = matched_list[0]
    else :
         matched_list = False 
 
    return matched_list 
            
