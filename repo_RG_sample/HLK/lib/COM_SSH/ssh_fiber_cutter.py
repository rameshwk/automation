from robot.api import logger
import os,sys,time,re
import com_ssh

SSH_SESSION = {}
def connect_fiber_cutter (ip='127.0.0.1',port='22',username='osctl',password='osctl', \
    prompt='osctl.*\$',session_name="fiber_cutter_session") :
    """
      connect to fiber cutter by building up a ssh session 
      (defaultly named fiber_cutter_session)
      
      return : the ssh channel object (identify by "fiber_cutter_session" defaultly)
    """   
    keyword_name = 'connect_fiber_cutter' 
    logger.debug ("%s:%s -> ip=%s,port=%s,username=%s,password=%s,prompt=%s,session_name=%s" \
    % (__name__,keyword_name,ip,str(port),username,password,prompt,session_name))
 
    ip = ip.encode("ascii")
    port = port.encode("ascii")
    username = username.encode("ascii")
    password = password.encode("ascii")
    prompt = prompt.encode("ascii")
    
    try:
        SSH_SESSION[session_name] = com_ssh.com_ssh\
        (ip,port=port,username=username,password=password,\
        prompt=prompt)
    except Exception as inst:
        raise AssertionError("%s:%s -> fail to init ssh object: %s" \
        % (__name__,keyword_name,inst))    
            
    try:
        SSH_SESSION[session_name].open_ssh ()
    except Exception as inst:
        raise AssertionError("%s:%s -> fail to open ssh session: %s" \
        % (__name__,keyword_name,inst))    
            
    logger.info ("%s:%s -> success to create ssh session: '%s'" \
    % (__name__,keyword_name,str(SSH_SESSION)))
    return SSH_SESSION[session_name]

def disconnect_fiber_cutter (session_name="fiber_cutter_session") :
    """ 
    Disconnect to fiber cutter by closing ssh session
          
    return: "pass"
    """
    keyword_name = 'disconnect_fiber_cutter'
    try:
        SSH_SESSION[session_name].close_ssh()
    except:
        raise AssertionError("%s:%s -> fail to close SSH session" \
        % (__name__,keyword_name))         
 
    else :
        print __name__,":",keyword_name, " -> SSH session ",session_name, " closed "
    
    SSH_SESSION.pop(session_name)
    return "pass"


def send_fiber_cutter_command(command,timeout=0,\
    session_name="fiber_cutter_session",prompt=None):
    """
    send single fiber_cutter command by ssh session
   
    return : (pass,res)

    """
    keyword_name = 'send_fiber_cutter_command'
    print "SSH CMD >> ",command

    try:
        sshobj = SSH_SESSION[session_name]      
        res = sshobj.send_command(command,timeout=timeout,prompt=None)
    except Exception as inst:
        raise AssertionError("%s:%s -> fail to send fiber cutter command '%s': %s" \
        % (__name__,keyword_name,command,inst))        
    else :
	print "SSH REPLY <<",res

    return res  
    
def block_seconds_on_fiber_cutter_ports (ports,seconds,\
    active_session=None,**fiber_cutter_info) :
    """
    block fiber signal for several seconds on fiber cutter ports
    
    return: "pass"
    """
    keyword_name="block_seconds_on_fiber_cutter_ports"
    logger.debug ("%s:%s -> ports=%s,seconds=%s,active_session=%s,%s" \
    % (__name__,keyword_name,ports,seconds,active_session,str(fiber_cutter_info)))
    if len(fiber_cutter_info) :
        if not fiber_cutter_info.has_key ('ip') :
            raise AssertionError("%s:%s -> \
            'ip' is necessary info for connecting to expected fiber cutter"\
            % (__name__,keyword_name))
        else :
            ip = fiber_cutter_info['ip']
        if fiber_cutter_info.has_key ('port') : 
            port = fiber_cutter_info['port']
        else :
            port = '22'
        if fiber_cutter_info.has_key ('username') : 
            username = fiber_cutter_info['username']
        else :
            username = 'osctl'            
        if fiber_cutter_info.has_key ('password') : 
            password = fiber_cutter_info['password']
        else :
            password = 'osctl'          

        if fiber_cutter_info.has_key ('prompt') : 
            prompt = fiber_cutter_info['prompt']
        else :
            prompt = 'osctl.*\$'
                  
        if fiber_cutter_info.has_key ('session_name') : 
            session_name = fiber_cutter_info['session_name']
        else :
            session_name = "fiber_cutter_session"   
                        
        try :
            connect_fiber_cutter(ip=ip,port=port,\
            username=username,password=password, \
            prompt=prompt,session_name=session_name)        
        except Exception as inst:
            raise AssertionError("%s:%s -> fail to connect fiber cutter: %s" \
            % (__name__,keyword_name,inst))    
    else :
        logger.debug("%s:%s -> fiber cutter should have been connected "\
        % (__name__,keyword_name))
        if not active_session :
            session_name =  "fiber_cutter_session" 
        else :
            session_name = active_session
           
    cmd_off = "osctl -p " + ports + " off"
    send_fiber_cutter_command(cmd_off,session_name=session_name)
    for i in range(15):
        time.sleep(2)
        on_ports = []
        for single_item in ports.split() :
            res,reply = send_fiber_cutter_command('osctl -s '+single_item,\
            session_name=session_name)
            if not re.search("off", reply) and re.search("on", reply) :
                off_ports.append (single_item)
                if i != 14 :
                    send_fiber_cutter_command("osctl -p "+single_item+ " off",\
                    session_name=session_name)
        if len(on_ports) == 0 :
            break  
    if len(on_ports) :
        raise AssertionError (str(off_ports)+ " was not 'off' till now" )           
        
    cmd_on  = "osctl -p " + ports + " on -t " + seconds      
    send_fiber_cutter_command(cmd_on,session_name=session_name)
    for i in range(15):
        time.sleep(2)
        off_ports = []
        for single_item in ports.split() :
            res,reply = send_fiber_cutter_command('osctl -s '+single_item,\
            session_name=session_name)
            if not re.search("on", reply) and re.search("off", reply) :
                off_ports.append (single_item)
                if i != 14 :
                    send_fiber_cutter_command("osctl -p "+single_item+ " on",\
                    session_name=session_name)
        if len(off_ports) == 0 :
            break
            
    if len(off_ports) :
        raise AssertionError (str(off_ports)+ " was not 'on' till now" )       
    
    if len(fiber_cutter_info) :
        disconnect_fiber_cutter(session_name=session_name)
        
        
