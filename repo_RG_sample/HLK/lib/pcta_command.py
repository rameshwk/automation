import re
from robot.api import logger
import com_pcta

PCTA_SESSION = ""
# Protocols Mapping Commands
Protocols = {

    'IP' : { 'start':'ta_traffic_start_ip', 
             'bursty':'ta_bursty_traffic_start_ip',
             'stop':'ta_traffic_stop_ip', 
             'send':'ta_send_ip_packet',
             'start_capture':'ta_start_capt_ip_packet', 
             'rtrv_capture':'ta_rtrv_capt_ip_packet',
             'stop_capture':'ta_stop_capt_ip_packet', 
             'start_measure':'ta_start_measurement_ip',
             'stop_measure':'ta_stop_measurement_ip' },

    'ETH' : { 'start':'ta_traffic_start_eth', 'bursty':'ta_bursty_traffic_start_eth', 'stop':'ta_traffic_stop_eth', 'send':'ta_send_eth_frame', 'start_capture':'ta_start_capt_eth_frame', 'rtrv_capture':'ta_rtrv_capt_eth_frame', 'stop_capture':'ta_stop_capt_eth_frame', 'start_measure':'ta_start_measurement_eth', 'stop_measure':'ta_stop_measurement_eth' },

    'BOOTP' : { 'start':'ta_traffic_start_bootp_dhcp', 'bursty':'x', 'stop':'ta_traffic_stop_bootp_dhcp', 'send':'ta_send_bootp_dhcp_message', 'start_capture':'ta_start_capt_bootp_dhcp_message', 'rtrv_capture':'ta_rtrv_capt_bootp_dhcp_message', 'stop_capture':'ta_stop_capt_bootp_dhcp_message', 'start_measure':'ta_start_measurement_bootp_dhcp', 'stop_measure':'ta_stop_measurement_bootp_dhcp' },

    'DHCP' : { 'start':'ta_traffic_start_bootp_dhcp', 'bursty':'x',
               'stop':'ta_traffic_stop_bootp_dhcp', 'send':'ta_send_bootp_dhcp_message',
               'start_capture':'ta_start_capt_bootp_dhcp_message',
               'rtrv_capture':'ta_rtrv_capt_bootp_dhcp_message',
               'stop_capture':'ta_stop_capt_bootp_dhcp_message',
               'start_measure':'ta_start_measurement_bootp_dhcp',
               'stop_measure':'ta_stop_measurement_bootp_dhcp' 
              },

    'TCP' : { 'start':'ta_traffic_start_tcp', 'bursty':'x',
               'stop':'ta_traffic_stop_tcp', 'send':'ta_send_tcp_packet',
               'start_capture':'ta_start_capt_tcp_packet',
               'rtrv_capture':'x',
               'stop_capture':'ta_stop_capt_tcp_packet',
               'start_measure':'x',
               'stop_measure':'x' 
              },

    'ARP' : { 'start':'ta_traffic_start_arp_rarp', 'bursty':'x', 'stop':'ta_traffic_stop_arp_rarp', 'send':'ta_send_arp_rarp_packet', 'start_capture':'ta_start_capt_arp_rarp_packet', 'rtrv_capture':'ta_rtrv_capt_arp_rarp_packet', 'stop_capture':'ta_stop_capt_arp_rarp_packet', 'start_measure':'ta_start_measurement_arp_rarp', 'stop_measure':'ta_stop_measurement_arp_rarp' },

    'RARP' : { 'start':'ta_traffic_start_arp_rarp', 'bursty':'x', 'stop':'ta_traffic_stop_arp_rarp', 'send':'ta_send_arp_rarp_packet', 'start_capture':'ta_start_capt_arp_rarp_packet', 'rtrv_capture':'ta_rtrv_capt_arp_rarp_packet', 'stop_capture':'ta_stop_capt_arp_rarp_packet', 'start_measure':'ta_start_measurement_arp_rarp', 'stop_measure':'ta_stop_measurement_arp_rarp' },

    'IGMP' : { 'start':'ta_traffic_start_igmp', 'bursty':'x', 'stop':'ta_traffic_stop_igmp', 'send':'ta_send_igmp_message', 'start_capture':'ta_start_capt_igmp_message', 'rtrv_capture':'ta_rtrv_capt_igmp_message', 'stop_capture':'ta_stop_capt_igmp_message', 'start_measure':'ta_start_measurement_igmp', 'stop_measure':'ta_stop_measurement_igmp' },

    'ICMP' : { 'start':'ta_traffic_start_icmp', 'bursty':'x', 'stop':'ta_traffic_stop_icmp', 'send':'ta_send_icmp_message', 'start_capture':'ta_start_capt_icmp_message', 'rtrv_capture':'ta_rtrv_capt_icmp_message', 'stop_capture':'ta_stop_capt_icmp_message', 'start_measure':'ta_start_measurement_icmp', 'stop_measure':'ta_stop_measurement_icmp' },

    'PPPOE' : { 'start':'ta_traffic_start_pppoe_discovery', 'bursty':'x', 'stop':'ta_traffic_stop_pppoe_discovery', 'send':'ta_send_pppoe_discovery_message', 'start_capture':'ta_start_capt_pppoe_discovery_message', 'rtrv_capture':'ta_rtrv_capt_pppoe_discovery_message', 'stop_capture':'ta_stop_capt_pppoe_discovery_message', 'start_measure':'ta_start_measurement_pppoe_discovery', 'stop_measure':'ta_stop_measurement_pppoe_discovery' },

    'PPP' : { 'start':'ta_traffic_start_ppp', 'bursty':'x', 'stop':'ta_traffic_stop_ppp', 'send':'ta_send_ppp_message', 'start_capture':'ta_start_capt_ppp_message', 'rtrv_capture':'ta_rtrv_capt_ppp_message', 'stop_capture':'ta_stop_capt_ppp_message', 'start_measure':'ta_start_measurement_ppp', 'stop_measure':'ta_stop_measurement_ppp' },

    'PPP_LCP' : { 'start':'ta_traffic_start_ppp_lcp', 'bursty':'x', 'stop':'ta_traffic_stop_ppp_lcp_ipcp', 'send':'ta_send_ppp_lcp_packet', 'start_capture':'ta_start_capt_ppp_lcp_ipcp_packet', 'rtrv_capture':'ta_rtrv_capt_ppp_lcp_ipcp_packet', 'stop_capture':'ta_stop_capt_ppp_lcp_ipcp_packet', 'start_measure':'ta_start_measurement_ppp_lcp_ipcp', 'stop_measure':'ta_stop_measurement_ppp_lcp_ipcp' },

    'PPP_IPCP' : { 'start':'ta_traffic_start_ppp_ipcp', 'bursty':'x', 'stop':'ta_traffic_stop_ppp_lcp_ipcp', 'send':'ta_send_ppp_ipcp_packet', 'start_capture':'ta_start_capt_ppp_lcp_ipcp_packet', 'rtrv_capture':'ta_rtrv_capt_ppp_lcp_ipcp_packet', 'stop_capture':'ta_stop_capt_ppp_lcp_ipcp_packet', 'start_measure':'ta_start_measurement_ppp_lcp_ipcp', 'stop_measure':'ta_stop_measurement_ppp_lcp_ipcp' },

    'RADIUS' : { 'start':'x', 'bursty':'x', 'stop':'x', 'send':'ta_send_radius_message', 'start_capture':'ta_start_capt_radius_message', 'rtrv_capture':'ta_rtrv_capt_radius_message', 'stop_capture':'ta_stop_capt_radius_message', 'start_measure':'ta_start_measurement_radius', 'stop_measure':'ta_stop_measurement_radius' },

    '8021x' : { 'start':'x', 'bursty':'x', 'stop':'x', 'send':'ta_send_8021x_frame', 'start_capture':'ta_start_capt_8021x_frame', 'rtrv_capture':'ta_rtrv_capt_8021x_frame', 'stop_capture':'ta_stop_capt_8021x_frame', 'start_measure':'ta_start_measurement_8021x', 'stop_measure':'ta_stop_measurement_8021x' },

    'VBAS' : { 'start':'ta_traffic_start_vbas', 'bursty':'x', 'stop':'ta_traffic_stop_vbas', 'send':'ta_send_vbas_message', 'start_capture':'ta_start_capt_vbas_message', 'rtrv_capture':'ta_rtrv_capt_vbas_message', 'stop_capture':'ta_stop_capt_vbas_message', 'start_measure':'ta_start_measurement_vbas', 'stop_measure':'ta_stop_measurement_vbas' },

    'L2CP_ADJACENCY' : { 'start':'ta_traffic_start_l2cp_adjacency', 'bursty':'x', 'stop':'ta_traffic_stop_l2cp_adjacency', 'send':'ta_send_l2cp_adjacency_message', 'start_capture':'ta_start_capt_l2cp_adjacency_message', 'rtrv_capture':'ta_rtrv_capt_l2cp_adjacency_message', 'stop_capture':'ta_stop_capt_l2cp_adjacency_message', 'start_measure':'ta_start_measurement_l2cp_adjacency', 'stop_measure':'ta_stop_measurement_l2cp_adjacency' },

    'L2CP_EVENT' : { 'start':'ta_traffic_start_l2cp_event', 'bursty':'x', 'stop':'ta_traffic_stop_l2cp_event', 'send':'ta_send_l2cp_event_message', 'start_capture':'ta_start_capt_l2cp_event_message', 'rtrv_capture':'ta_rtrv_capt_l2cp_event_message', 'stop_capture':'ta_stop_capt_l2cp_event_message', 'start_measure':'ta_start_measurement_l2cp_event', 'stop_measure':'ta_stop_measurement_l2cp_event' },

    'L2CP_ADJACENCY_TCP' : { 'start':'ta_traffic_start_l2cp_adjacency_tcp', 'bursty':'x', 'stop':'ta_traffic_stop_l2cp_adjacency_tcp', 'send':'ta_send_l2cp_adjacency_message_tcp', 'start_capture':'x', 'rtrv_capture':'x', 'stop_capture':'x', 'start_measure':'x', 'stop_measure':'x' },

    'L2CP_PORT_MGMT' : { 'start':'ta_traffic_start_l2cp_port_management_tcp', 'bursty':'x', 'stop':'ta_traffic_stop_l2cp_port_management_tcp', 'send':'ta_send_l2cp_port_management_message_tcp', 'start_capture':'ta_start_capt_l2cp_port_management_message', 'rtrv_capture':'ta_rtrv_capt_l2cp_port_management_message', 'stop_capture':'ta_stop_capt_l2cp_port_management_message', 'start_measure':'ta_start_measurement_l2cp_port_management', 'stop_measure':'ta_stop_measurement_l2cp_port_management' },

    'RIP' : { 'start':'ta_traffic_start_rip', 'bursty':'x', 'stop':'ta_traffic_stop_rip', 'send':'ta_send_rip_message', 'start_capture':'ta_start_capt_rip_message', 'rtrv_capture':'ta_rtrv_capt_rip_message', 'stop_capture':'ta_stop_capt_rip_message', 'start_measure':'ta_start_measurement_rip', 'stop_measure':'ta_stop_measurement_rip' },

    'CFM' : { 'start':'x', 'bursty' : 'x','stop' : 'x','send' : 'ta_send_8021ag_frame', 'start_capture' : 'ta_start_capt_8021ag_frame','rtrv_capture' : 'x','stop_capture' : 'ta_stop_capt_8021ag_frame', 'start_measure' : 'x','stop_measure' : 'x'},

    'Y1731' : { 'start':'x', 'bursty' : 'x', 'stop' : 'x', 'send' : 'ta_send_y1731_cfm_frame', 'start_capture' : 'ta_start_capt_y1731_cfm_frame', 'rtrv_capture': 'x', 'stop_capture' : 'ta_stop_capt_y1731_cfm_frame', 'start_measure' : 'x', 'stop_measure' : 'x' },

    'UDP' : { 'start':'ta_traffic_start_udp', 'stop':'ta_traffic_stop_udp', 'send':'ta_send_udp_packet', 'start_capture':'ta_start_capt_udp_packet', 'stop_capture':'ta_stop_capt_udp_packet' },

    'IPV6' : { 'start':'ta_traffic_start_ipv6', 'stop':'ta_traffic_stop_ipv6', 'send':'ta_send_ipv6_packet', 'start_capture':'ta_start_capt_ipv6_packet', 'stop_capture':'ta_stop_capt_ipv6_packet' },

    'DHCPV6' : { 'start':'ta_traffic_start_bootp_dhcpv6', 'stop':'ta_traffic_stop_bootp_dhcpv6_message', 'send':'ta_send_bootp_dhcpv6_message', 'start_capture':'ta_start_capt_bootp_dhcpv6_message', 'stop_capture':'ta_stop_capt_bootp_dhcpv6_message' },

    'ICMPV6' : { 'start':'ta_traffic_start_icmpv6', 'stop':'ta_traffic_stop_icmpv6', 'send':'ta_send_icmpv6_message', 'start_capture':'ta_start_capt_icmpv6_message', 'stop_capture':'ta_stop_capt_icmpv6_message' },

    'MPLS_ATM' : { 'send':'ta_send_mpls_atm_packet', 'start':'ta_traffic_start_mpls_atm', 'stop':'ta_traffic_stop_mpls_atm', 'start_capture':'ta_start_capt_mpls_atm_packet', 'stop_capture':'ta_stop_capt_mpls_atm_packet' },

    'MPLS_IP' : { 'send':'ta_send_mpls_ip_packet', 'start':'ta_traffic_start_mpls_ip', 'stop':'ta_traffic_stop_mpls_ip', 'start_capture':'ta_start_capt_mpls_ip_packet', 'stop_capture':'ta_stop_capt_mpls_ip_packet' },

    'MPLS_ETH' : { 'send':'ta_send_mpls_eth_packet', 'start':'ta_traffic_start_mpls_eth', 'stop':'ta_traffic_stop_mpls_eth', 'start_capture':'ta_start_capt_mpls_eth_packet', 'stop_capture':'ta_stop_capt_mpls_eth_packet' },

    'MPLS_IP_CW' : { 'send':'ta_send_mpls_ip_cw_packet', 'start':'ta_traffic_start_mpls_ip_cw', 'stop':'ta_traffic_stop_mpls_ip_cw', 'start_capture':'ta_start_capt_mpls_ip_cw_packet', 'stop_capture':'ta_stop_capt_mpls_ip_cw_packet' },

    'MPLS_ETH_CW' : { 'send':'ta_send_mpls_eth_cw_packet', 'start':'ta_traffic_start_mpls_eth_cw', 'stop':'ta_traffic_stop_mpls_eth_cw', 'start_capture':'ta_start_capt_mpls_eth_cw_packet', 'stop_capture':'ta_stop_capt_mpls_eth_cw_packet' },

    'GET_IP' : { 'get':'ta_get_ip_addr' }

}
	    
def get_pcta_command (prot,cmdtype) :
    """
    this keyword look up dictionary 'Protocols' 
    to find out the defined pcta traffic command
    
    """
    global Protocols
    logger.debug ("Inputs: protocols : %s, cmdtype : %s" % (prot,cmdtype))

    tmpdict = Protocols[prot]
    cmd = tmpdict[cmdtype]

    logger.debug ("The required pcta command : %s" % cmd)
    return cmd
    
def pcta_command_options (*args):
    """
      this keyword traslate user input options to the format of pcta command required
    """
    out_options = " "
    for arg in args:
        arg = arg.encode("ascii")
        index = arg.index("=") # first "=" is the seperation sign
        key = arg[0:index].strip().strip('\'').strip('"')
        value = arg[1+index:].strip('\'').strip('"')
	if value != 'x' :
            out_options = out_options + "-" + key + " " + value + " "
        
    return out_options
    
def get_pcta_info (sessionObject=None) :
    """
    get active pcta info
    
    return an array
    """
    
    global PCTA_SESSION
    pcta_arr = {}
    pcta_arr['ip'] = PCTA_SESSION.ip
    pcta_arr['port'] = PCTA_SESSION.port 
    pcta_arr['username'] = PCTA_SESSION.username
    pcta_arr['password'] = PCTA_SESSION.password      
    pcta_arr['exec_file'] = PCTA_SESSION.exec_file
    pcta_arr['client_slot'] = PCTA_SESSION.client_slot 
    pcta_arr['network_slot'] = PCTA_SESSION.network_slot
    pcta_arr['client2_slot'] = PCTA_SESSION.client2_slot  
    return pcta_arr
        
def open_pcta_session(ip,username,password,exePath,clientSlot,networkSlot) :
    global PCTA_SESSION
    keyword_name = "open_pcta_session"
    try:
        PCTA_SESSION = com_pcta.com_pcta \
        (ip=ip,username=username,password=password,\
        exec_file=exePath,client_slot=clientSlot,network_slot=networkSlot)
    except Exception as inst:
        raise AssertionError("%s:%s-> fail to init com_pcta, exception: %s" \
        % (__name__,keyword_name,str(inst)))  
        
    try:       
        PCTA_SESSION.open_pcta()
    except Exception as inst:
        raise AssertionError("%s:%s-> fail to open_pcta, exception: %s" \
        % (__name__,keyword_name,str(inst)))           
    else :
        logger.debug("%s:%s-> start pcta session success " \
        % (__name__,keyword_name))
    	
    return "pass"

def close_pcta_session () :
    global PCTA_SESSION
    keyword_name = "close_pcta_session"
    try:
        PCTA_SESSION.close_pcta()
        del PCTA_SESSION
    except Exception as inst:
        raise AssertionError("%s:%s-> fail to close pcta session, exception: %s" \
        % (__name__,keyword_name, str(inst)))      
    else:
        logger.debug("%s:%s -> close pcta session success" \
        % (__name__,keyword_name))

    return "pass"
        
def send_pcta_command (command):
    global PCTA_SESSION
    keyword_name = "send_pcta_command"
    logger.info("PCTA CMD >> %s" % command)
    if PCTA_SESSION.client_slot and PCTA_SESSION.network_slot :
        pass
    try :
        res = PCTA_SESSION.send_command(command)
    except Exception as inst :
        raise AssertionError("%s:%s -> exception: %s" \
        % (__name__,keyword_name,inst))           
    else :
        logger.info("PCTA REPLY << %s" % res)
        
    if re.search('unknown\s*option|extra option|error|\
    invalid token|is not complete', res.lower()):
        return ("fail", res)
    else:
        return ("pass", res)

  

