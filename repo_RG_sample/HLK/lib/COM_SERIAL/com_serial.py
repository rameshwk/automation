import os,re,sys,time
import serial
from robot.api import logger
import datetime
    
class com_serial(object):
	def __init__(self,port='/dev/ttyS0',baudrate=9600,user='ONTUSER',passwd1='SUGAR2A041',passwd2='shell',prompt='#',**args):
		self.ser = ""
		self.serPort = port.encode("ascii")
		self.serBaudrate = baudrate
		prompt = prompt.split()[-1]
		self.serPrompt = prompt.encode("ascii")
		self.serUser = user.encode("ascii")
		self.serPasswd1 = passwd1.encode("ascii")
		self.serPasswd2 = passwd2.encode("ascii")
		if "bytesize" in args:
			self.serBytesize = args['bytesize']
		else:
			self.serBytesize = 8
		if "parity" in args:
			self.serParity = args['parity'].encode("ascii")
		else:
			self.serParity = 'N'
		if "timeout" in args:
			self.serTimeout = args['timeout']
		else:
			self.serTimeout = 0.5
		if "stopbits" in args:
			self.serStopbits = args['stopbits']
		else:
			self.serStopbits = 1

	def open_serial(self):
		keyword_name = "open_serial"
		re_connection = 1 
		try :
			self.ser = serial.Serial(self.serPort,self.serBaudrate,self.serBytesize,self.serParity,self.serStopbits,self.serTimeout)
			self.ser.write("\r\n")
			time.sleep(0.2)
			isOpen = self.ser.inWaiting()
			logger.debug("%s:%s --> Got byte of response: %s\n%s" % (__name__,keyword_name,isOpen,self.ser))
			#Get serial writing status
			#If serial is open and can be writed, 'isOpen' will save how many bytes was read when write '\n' in serial
			if isOpen != 0:
				res = self.ser.read(isOpen)
				return "PASS", self.ser
			else:
				return "FAIL", "Warning: Successful to call serial, but some issues caused there is no response during writing in serial, \
				the serial interface need to be checked, something like that serial is already in used."
		except Exception as e:
			logger.info("%s:%s --> Open serial failed, gotten exception: '%s'" %(__name__,keyword_name,e))

	def send_command(self,cmd):
		cmd = cmd.encode('ASCII')
		logger.info( cmd )
		keyword_name = "send_command"
		reWriteTimes = 0
		while reWriteTimes < 3:
			#check whether session is in prompt or not and read buffer before write cmd
			returnTmp = self._write_to_ser()
			logger.info(returnTmp)
			if self.serPrompt in returnTmp.split():
				try:
					returnTmp = self._write_to_ser(cmd)
				except Exception as e:
					logger.info("%s:%s >> Failed to send cmd >>\nGotten exception: %s" % (__name__,keyword_name,e))
				break
			else:
#				logger.info("%s:%s >> Gotten: %s\nNeed login in serial" % (__name__,keyword_name,returnTmp))
				if "user>" in returnTmp or "user#" in returnTmp:
					logger.info("%s:%s >> Gotten: %s\nNeed login in serial......" % (__name__,keyword_name,returnTmp))
					returnTmp = self._login_aont_shell()
				elif "Password:" in returnTmp or "login:" in returnTmp:
					logger.info("%s:%s >> Gotten: %s\nNeed login in serial......" % (__name__,keyword_name,returnTmp))
					returnTmp = self._login_aont_ontuser()
				reWriteTimes += 1
		return returnTmp

	def read_serial(self):
		keyword_name = "read_serial"
		resByte = 4095
		returnTmp = ""
		res = ""
		reg = re.compile("\n")
		try:
			while resByte != 0:
				time.sleep(0.5)
				resByte = self.ser.inWaiting()
				returnTmp = re.sub(reg,"\n"+str(datetime.datetime.now())+"=> ",self.ser.read(resByte))
				res = res + returnTmp
			logger.debug("Read serial buffer << %s " % (res)) 
		except Exception as e:
			logger.info("Error response, gotten exception: %s" %e)
		return res

	def _login_aont_shell(self):
		keyword_name = "_login_aont_shell"
		loginTimes = 0
		returnTmp = self._write_to_ser()
		logger.info("%s:%s >> Gotten: >>%s<<\nBegin login ..............." % (__name__,keyword_name,returnTmp))
		while self.serPrompt not in returnTmp.split() and loginTimes < 10 :
			if "user>" in returnTmp.split():
				logger.debug("%s:%s >> 'user>' should into %s" % (__name__,keyword_name,returnTmp.split()))
				logger.info("%s:%s >> Login ONT serial >> User: %s" % (__name__,keyword_name,self.serUser))
				returnTmp = self._write_to_ser(self.serUser)
			elif "user#" in returnTmp.split():
				logger.debug("%s:%s >> 'user#' should into %s" % (__name__,keyword_name,returnTmp.split()))
				logger.info("%s:%s >> Login ONT serial >> Passwd: %s" % (__name__,keyword_name,self.serPasswd1))
				returnTmp = self._write_to_ser(self.serPasswd1)
				time.sleep(1)
				returnTmp = self._write_to_ser(self.serPasswd2)
			logger.debug("%s:%s >> 'login:' or 'Password:' should into %s" % (__name__,keyword_name,returnTmp.split()))
			loginTimes += 1
		if loginTimes >= 11:
			returnTmp = "Fail to login serial,\ngot return %s" %returnTmp
		return returnTmp

	def _login_aont_ontuser(self):
		keyword_name = "_login_aont_ontuser"
		loginTimes = 0
		returnTmp = self._write_to_ser()
		logger.info("%s:%s >> Gotten: >>%s<<\nBegin login ..............." % (__name__,keyword_name,returnTmp))
		while self.serPrompt not in returnTmp.split() and loginTimes < 6 :
			if "login:" in returnTmp.split():
				logger.info("%s:%s >> Login ONT serial >> User: %s" % (__name__,keyword_name,self.serUser))
				returnTmp = self._write_to_ser(self.serUser)
			elif "Password:" in returnTmp.split():
				logger.info("%s:%s >> Login ONT serial >> Passwd: %s" % (__name__,keyword_name,self.serPasswd1))
				returnTmp = self._write_to_ser(self.serPasswd1)
				returnTmp = self._write_to_ser(self.serPasswd2)
			logger.debug("%s:%s >> 'login:' or 'Password:' should into %s" % (__name__,keyword_name,returnTmp.split()))
			loginTimes += 1
		if loginTimes >= 6:
			returnTmp = "Fail to login serial,\ngot return %s" %returnTmp
		return returnTmp

	def _login_odm_cig(self):
		returnTmp = ""

		return returnTmp

	def _login_odm_tw(self):
		returnTmp = ""

		return returnTmp

	def _write_to_ser(self,cmd=""):
		resByte = 4095
		returnTmp = ""
		res = ""
		reReadTimes = 5
		try:
			wret = self.ser.write(cmd+'\r\n')
			logger.info("SERAIL WRITTEN >>> '%s \\n'" %cmd)
			while resByte != 0:
#				logger.info("Got %d return buffer" %resByte)
				time.sleep(0.5)
				resByte = self.ser.inWaiting()
				returnTmp = self.ser.read(resByte)
				res = res + returnTmp
				if self.serPrompt in returnTmp.split()[-1]:
					logger.debug(">>>self.serPrompt is %s, returnTmp is %s<<<" %(self.serPrompt,returnTmp))
					logger.info(">>>Got prompt, will exit<<<")
					break
				elif resByte == 0 and reReadTimes > 0:
					logger.info("Got 0 return buffer, %s times left re-read." %reReadTimes)
					time.sleep(2)
					resByte = self.ser.inWaiting()
					returnTmp = self.ser.read(resByte)
					resByte = 1
					reReadTimes -= 1
			logger.info("SERAIL RETURN << %s " % (res)) 
		except Exception as e:
			logger.info("Error response, gotten exception: %s" %e)
		return res
		

	def close_serial(self):
		keyword_name = "close_serial"
		try:
			self.ser.close()
			return self.ser.isOpen()
		except Exception as e :
			logger.info("%s:%s --> fatal to reboot and re-connect cli \n\
			exception:%s" % (__name__,keyword_name,e))	

