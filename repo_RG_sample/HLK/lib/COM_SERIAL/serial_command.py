import os, re
import sys
import time
import threading
from collections import OrderedDict
from robot.api import logger

try :
	import com_serial
except Exception as inst :
	sys.path.append(os.environ['ROBOTREPO'])
        import com_serial

SESSION_SERIAL = {}
SESSION_LISTEN = ""
LISTEN_RES = ""
LISTEN_FLAG = 0
#LISTEN_FLAG: (0:disable);(1:enable);(2:pause)
WRITE_FLAG = 0
#WRITE_FLAG: (0:Don't allow write on serial);(1:Allow write on serial)

class threadListenSer(threading.Thread):
	def __init__(self,session_name):
		threading.Thread.__init__(self)
		self.session_name = session_name

	def run(self):
		self.listen_from_serial()

	def stop(self):
		global LISTEN_FLAG
		LISTEN_FLAG = 0

	def listen_from_serial(self):
		keyword_name = "listen_from_serial"
		global LISTEN_RES
		global WRITE_FLAG
		LISTEN_RES = ""
		try :
			while LISTEN_FLAG != 0:
				if LISTEN_FLAG == 1:
					LISTEN_RES = LISTEN_RES + SESSION_SERIAL[self.session_name].read_serial()
				elif LISTEN_FLAG == 2:
					WRITE_FLAG = 1
					time.sleep(0.5)
		except Exception as inst:
			print "%s:%s --> Fail to open 'SESSION_SERIAL[%s]'\n exception: %s" %(__name__,keyword_name,inst,SESSION_SERIAL[self.session_name])

def connect_serial(port, baudrate, user, passwd1, passwd2, prompt, **args) :
	"""
   	Parameters:
    port:	Device name or None.
    baudrate (int):    Baud rate such as 9600 or 115200 etc.
    bytesize:    Number of data bits. Possible values: FIVEBITS, SIXBITS, SEVENBITS, EIGHTBITS
    parity:    Enable parity checking. Possible values: PARITY_NONE, PARITY_EVEN, PARITY_ODD PARITY_MARK, PARITY_SPACE
    stopbits:    Number of stop bits. Possible values: STOPBITS_ONE, STOPBITS_ONE_POINT_FIVE, STOPBITS_TWO
    timeout (float):    Set a read timeout value.
    xonxoff (bool):    Enable software flow control.
    rtscts (bool):    Enable hardware (RTS/CTS) flow control.
    dsrdtr (bool):    Enable hardware (DSR/DTR) flow control.
    session_name:    the name of target cli session, default 'first_session'
	"""

	bytesize = 8
	parity = 'N'
	timeout = 0.5
	stopbits = 1
	session_name = 'first_session'

	for tmpArgs in args:
		if "bytesize" in tmpArgs:
			bytesize = int(args['bytesize'])
		elif "parity" in tmpArgs:
			parity = args['parity']
		elif "timeout" in tmpArgs:
			timeout = float(args['timeout'])
		elif "stopbits" in tmpArgs:
			stopbits = int(args['stopbits'])
		elif "session_name" in tmpArgs:
			session_name = args['session_name']	

	global SESSION_SERIAL
	keyword_name = "connect_serial"
	logger.debug("%s:%s -> \
    port=%s,baudrate=%s,prompt=%s,bytesize=%s,parity=%s,timeout=%s,stopbits=%s,user=%s,passwd1=%s,passwd2=%s" \
    % (__name__,keyword_name,port,baudrate,prompt,bytesize,parity,timeout,stopbits,user,passwd1,passwd2))

	if SESSION_SERIAL.has_key(session_name):
		logger.info("%s has already been created, will return this session directly" % session_name)
		return SESSION_SERIAL[session_name]

	try:
		SESSION_SERIAL[session_name] = com_serial.com_serial(port,baudrate,user,passwd1,passwd2,prompt,bytesize=bytesize,parity=parity,timeout=timeout,stopbits=stopbits)
	except Exception as inst:
		logger.info("fail to open serial\n exception: %s" % inst) 
    
	try :     
		stu, res = SESSION_SERIAL[session_name].open_serial()
		logger.info("Open serial REPLY <<< \n%s" %res)
		logger.info("Open session : -->> %s" %SESSION_SERIAL)
	except Exception as inst:
		logger.info("fail to login serial session\n exception: %s" % inst)


def disconnect_serial(session_name="first_session"):
	"""
    close serial session
    - *return:*  pass or raise error       
	"""
	keyword_name = "disconnect_serial"
	try:
		stu = SESSION_SERIAL[session_name].close_serial()
		SESSION_SERIAL.pop(session_name)
		logger.info("Current serial status is >>%s<<" %stu)
	except Exception as inst:
		logger.debug("--------------------------------------------")
		logger.info("fail to close serial session\n exception: %s" % inst)

def send_serial_command(cmd,session_name="first_session"):
	keyword_name = "send_serial_command"
	global WRITE_FLAG
	global LISTEN_FLAG
	logger.info("Open session : -->> %s" %SESSION_SERIAL)
	try:
		if LISTEN_FLAG == 0:
			res = SESSION_SERIAL[session_name].send_command(cmd)
			return res
		else:
			logger.info("%s:%s --> Serial Listen is on working, pause listen and write cmd later." %(__name__,keyword_name))
			LISTEN_FLAG = 2
			reTry = 100
			while WRITE_FLAG != 1 and reTry > 1:
				logger.info("Wait for write in serial.")
				time.sleep(0.5)
				reTry -= 1
			res = SESSION_SERIAL[session_name].send_command(cmd)
			LISTEN_FLAG = 1
			WRITE_FLAG = 0
			return res
	except Exception as inst:
		logger.info("fail to send serial command to session\n exception: %s" % inst)

def listen_serial_start(session_name="first_session"):
	global SESSION_LISTEN
	global LISTEN_FLAG
	keyword_name = "listen_serial_start"
	LISTEN_FLAG = 1
	try :
		if not SESSION_LISTEN: 
			SESSION_LISTEN = threadListenSer(session_name)
		if SESSION_LISTEN.isAlive() is not True:
			SESSION_LISTEN.start()
			logger.info("%s:%s --> Start to listen serial output, run keyword 'listen_serial_stop' to get output."%(__name__,keyword_name))
		else:
			logger.info("%s:%s --> Listen session is already start, run keyword 'listen_serial_stop' to get output."%(__name__,keyword_name))
	except Exception as inst:
		logger.info("%s:%s --> Fail to open Listen class with 'SESSION_SERIAL[%s]'\N exception:%s" %(__name__,keyword_name,inst,session_name))

def check_serial_res(**args):
    """ check the specified flags from the serial.\n
        note: if not contain 'timeout', the timeout will be set default as 5*60.

        @param  - *[args]* the dict parameters.Supported arguments: 
                checkx -- the flags to check;
                reset -- clear the pre serial data;
                timeout -- the timeout time to check;
                For example: check1=xxx, check2=xxx.\n
        @return - *return:* \n
            true -- all flags are found;\n
            false -- not all flags are found.\n
    """
    keyword_name = "check_serial_res"
    global LISTEN_RES

    check_res={}
    check_timeout=0
    reset=0
    if not args.has_key('timeout'):
        check_timeout=5*60

    for key,val in args.items():
        if 'timeout' == key:
            check_timeout=eval(val)
        elif 'reset' == key:
            reset=eval(val)
        elif 'check' in key:
            check_res[val]=False

    if reset == 1:
        LISTEN_RES=''

    logger.info("%s:%s --> Start to check the serial data..."%(__name__,keyword_name))
    check_start=time.time()
    check_end=check_start+check_timeout

    check_cnt=0
    check_cur_tm=time.time()
    while check_cur_tm < check_end:
        check_cur_tm=time.time()
        for key,val in check_res.items():
            if not val and (key in LISTEN_RES):
                logger.info("%s:%s --> Found %s"%(__name__,keyword_name, key))
                check_res[key]=True
                check_cnt+=1

        if check_cnt >= len(check_res):
            break

        time.sleep(5)

    if check_cur_tm >= check_end:
        for key,val in check_res.items():
            if not val:
                logger.error("%s:%s --> Can not find %s"%(__name__,keyword_name, key))
        logger.info("%s:%s --> The current serial data is %s"%(__name__,keyword_name,LISTEN_RES))
        return False

    return True

def check_serial_res_seq(**args):
    """ check the specified flags(in sequence) from the serial.\n
        note: if not contain 'timeout', the timeout will be set default as 5*60.

        @param  - *[args]* the dict parameters.Supported arguments: 
                checkx -- the flags to check;
                reset -- clear the pre serial data;
                timeout -- the timeout time to check;
                For example: check1=xxx, check2=xxx.\n
        @return - *return:* \n
            true -- all flags are found;\n
            false -- not all flags are found.\n
    """
    keyword_name = "check_serial_res_seq"
    global LISTEN_RES

    check_res=OrderedDict()
    ls_keys=[]
    check_timeout=0
    reset=0
    if not args.has_key('timeout'):
        check_timeout=5*60

    for key,val in args.items():
        if 'timeout' == key:
            check_timeout=eval(val)
        elif 'reset' == key:
            reset=eval(val)
        elif 'check' in key:
            ls_keys.append(key)
            ls_keys=sorted(ls_keys)

    for item in ls_keys:
        check_res[args[item]]=False

    if reset == 1:
        LISTEN_RES=''

    logger.info("%s:%s --> Start to check the serial data..."%(__name__,keyword_name))
    check_start=time.time()
    check_end=check_start+check_timeout

    check_cnt=0
    check_cur_tm=time.time()
    while check_cur_tm < check_end:
        check_cur_tm=time.time()
        for key,val in check_res.items():
            if not val and (key in LISTEN_RES):
                logger.info("%s:%s --> Found %s"%(__name__,keyword_name, key))
                check_res[key]=True
                check_cnt+=1

        if check_cnt >= len(check_res):
            break

        time.sleep(5)

    if check_cur_tm >= check_end:
        for key,val in check_res.items():
            if not val:
                logger.error("%s:%s --> Can not find %s"%(__name__,keyword_name, key))
        logger.info("%s:%s --> The current serial data is %s"%(__name__,keyword_name,LISTEN_RES))
        return False

    return True

def listen_serial_stop(session_name="first_session"):
	global SESSION_LISTEN
	keyword_name = "listen_serial_stop"
	try :
		SESSION_LISTEN.stop()
		logger.info("stop to listen serial output, GOT LISTEN FLAG is %s" %LISTEN_FLAG)
		#Wait for SESSION_LISTEN.run break.
		while SESSION_LISTEN.isAlive() is True:
			logger.debug("GOT SESSION_LISTEN.isAlive() << \n%s" %SESSION_LISTEN.isAlive())
			time.sleep(1)
		SESSION_LISTEN = ""
		return LISTEN_RES
	except Exception as inst:
		logger.info("%s:%s --> Fail to stop Listen class with 'SESSION_SERIAL[%s]'\N exception:%s" %(__name__,keyword_name,inst,session_name))

def clear_log_messages_ser(logpath='/logs/messages'):
    str_cmd='echo > {0}'.format(logpath)
    return send_serial_command(str_cmd)

def check_log_messages_ser(logpath='/logs/messages',log_type='telnet',**args):
    if not args.has_key('fail_cnt'):
        args.setdefault('fail_cnt',4)

    fail_cnt = int(args['fail_cnt'])
    str_cmd='cat {0}'.format(logpath)
    log_info=send_serial_command(str_cmd)

    patt_timestamp='\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}'
    patt_type='{0}: '.format(log_type)
    patt_ip='((?:(2[0-4]\d)|(25[0-5])|([01]?\d\d?))\.){3}(?:(2[0-4]\d)|(25[0-5])|([01]?\d\d?))'
    patt_fail='<UNKNOWN> login fail.'

    patt_conn='connection from: '
    patt_disconn='disconnection from: '
    patt_conn_info='{0} \[info  \] {1}{2}{3}\r\n'.format(patt_timestamp,patt_type,patt_conn,patt_ip)
    patt_disconn_info='{0} \[info  \] {1}{2}{3}\r\n'.format(patt_timestamp,patt_type,patt_disconn,patt_ip)
    if fail_cnt > 3:
        patt_fail_alert='{0} \[alert \] {1}{2}\r\n'.format(patt_timestamp,patt_type,patt_fail)*3
        patt_try_info='{0}{1}'.format(patt_conn_info,patt_disconn_info)*(fail_cnt-3)
    else:
        patt_fail_alert='{0} \[alert \] {1}{2}\r\n'.format(patt_timestamp,patt_type,patt_fail)*fail_cnt
        patt_try_info=''
    patt='{0}{1}{2}{3}'.format(patt_conn_info,patt_fail_alert,patt_disconn_info,patt_try_info)
    if re.search(patt, log_info):
        return True
    else:
        return False

