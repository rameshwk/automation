import os,sys,time,re
import socket,paramiko
from robot.api import logger

debug = True

try:	
    lib_path =  os.environ['ROBOTREPO'] +'/LIBS/COM/COM_PCTA'
    if lib_path not in sys.path:
        sys.path.append(lib_path)
    
except Exception:
    raise AssertionError("Module: %s -> Fail to set sys.path" % __name__)
    
""" 
    To login the pcta server and launch the pcta 
    
    Its done with the help of paramiko module using ssh protocol.

Author:  
Mahalakshmi Venkatraman 	Developed	 
"""
class com_pcta(object) :
    """ 
    com_pcta is the class will have ip, port, username, password and exec_file arguments.
        
    Author:  
    Mahalakshmi Venkatraman 	Developed	 
    """
    def __init__(self,ip='127.0.0.1',port='22',username='atxuser',\
    password='alcatel01',exec_file='/root/PCTA/pcta.exe',\
    client_slot='1', network_slot='2',client2_slot='3') :
    
        self.ip = ip.encode("ascii")
        self.port = int(port.encode("ascii"))
        self.username = username.encode("ascii")
        self.password = password.encode("ascii")
        self.exec_file = exec_file.encode("ascii")
        self.prompt = "PC_TA_.*>".encode("ascii")
        self.client_slot = client_slot.encode("ascii")
        self.network_slot = network_slot.encode("ascii")
        self.client2_slot = client2_slot.encode("ascii")   
             
        #transport and channel are the object variables which will have the transport & channel objects.
        self.transport = ""
        self.channel = ""
            
    def _exec_command(self,command,expectPrompt,message="execute command",timeout=5):
        keyword_name = "_exec_command"
        returnTmp = ""
        try:
            returnTmp = self._ssh_sendcmd(command+"\n")
            logger.debug("write:'%s', expect:'%s'" % (command,expectPrompt))
            logger.debug("get return:\n%s\n" % returnTmp)
        except Exception as inst:
            msg = "fail to " + message
            raise AssertionError("%s:%s-> %s,exception:%s" % (__name__,keyword_name,msg,inst))
        return returnTmp

                          	
    def open_pcta(self):
        """ 
        open_pcta will establish the ssh connection using paramiko and create a transport & channel objects for the
          ssh session & invoke the shell. Set the pty terminal session to run the sudo commands. Set default timeout
          as '3'. The channel will wait upto 3 seconds to get the data from the socket, else return.
            
        Author:  
        Mahalakshmi Venkatraman 	Developed	 
        """
        keyword_name = "open_pcta"
        logger.debug("%s:%s " % (__name__,keyword_name))
        re_telnetTime = 0
	
        while re_telnetTime < 10:
            try:                                
		self.transport = paramiko.Transport((self.ip, self.port))
		self.transport.connect(username = self.username, password = self.password)
		self.channel = self.transport.open_session()
		self.channel.settimeout(3)
		self.channel.get_pty('vt100')
		self.channel.invoke_shell()
		logger.debug("SSH session established successfully...")
            except Exception as inst:
                if re_telnetTime == 10:
                    raise AssertionError("%s:%s ->Can't open PCTA, exception:%s" \
                    % (__name__,keyword_name,inst))
		try :
		    self.channel.close()
		    self.transport.close()
		except Exception as inst :                  
                    logger.debug("%s:%s : Closing channel and transport failed..." \
                % (__name__,keyword_name))
                re_telnetTime += 1
                logger.debug("%s:%s -> Try to SSH PCTA the %d(th) time" \
                % (__name__,keyword_name,re_telnetTime))
                time.sleep(5)      
                continue         
            else:
                break

        pcta_path = os.path.dirname(self.exec_file)
	dir_prompt = "\$|\#|\%"
	switch_dir =  "cd "+ pcta_path 

        # Switch directory to given 'exec_dir'
        returnInfo = self._exec_command(switch_dir,dir_prompt,"switch directory",5)     
	flag = 0
	if not re.search(dir_prompt,returnInfo):
            raise AssertionError("%s:%s -> fail to switch directory" \
            % (__name__,keyword_name))
                    
        # launch the pcta with the sudo permission   
        run_pcta = "sudo " + pcta_path + "/" + os.path.basename(self.exec_file)
        returnInfo = self._exec_command(run_pcta,self.prompt,"execute pcta process",5)
	if not re.search(self.prompt,returnInfo) :		    
            self.channel.close()
            self.transport.close()
            raise AssertionError("%s:%s -> fail to execute pcta process, ReturnOutput :%s:" \
            %(__name__,keyword_name,msg,returnInfo))
  
	self.channel.settimeout(60)
	self.session_alive = True     
        return "pass"
	
    def reopen_pcta(self):
        iretry_count = 10
        i = 0
        while ( i <= iretry_count ) :
            i += 1
            msg = "try to reopen pcta for the %s(th) time" % str(i)
            logger.debug("%s:%s -> %s" % (__name__,'reopen_pcta',msg))
            try:
                self.open_pcta()
            except Exception as inst:
                pass
            if self.session_alive :
                return "pass"
            else :
                time.sleep(1)
                continue

        raise AssertionError("%s:%s ->Can't open PCTA session, exception:%s" \
                    % (__name__,keyword_name,inst))	
      
    def close_pcta(self):
        """ 
        close_pcta will close the ssh connection which is established using paramiko module
          
        Author:  
        Mahalakshmi Venkatraman 	Developed	 
        """
        keyword_name = "close_pcta"
        resultFlag = "OK"
	self.channel.settimeout(3)
        try:
	    command = "exit\n"
	    expectPrompt = "$|#|%"
	    msg = "exit command execution"
	    returnInfo = self._exec_command(command,expectPrompt,msg,5)
            self.transport.close()
	    self.channel.close()
        except Exception as inst:
            msg = "PCTA can't been closed"
            raise AssertionError("%s:%s -> %s, exception: %s" \
            % (__name__,keyword_name,msg, inst))
             
        return "pass"         
    
    def send_command(self,pctaCmd):
        keyword_name = "send_command"

        i = 1
        while (i < 3) :
            i += 1
            result = self._ssh_sendcmd(pctaCmd)
            if re.search("PC_TA_.*>",result) :
	        return result
	    else :
                self.transport.close()
                self.channel.close()
                self.session_alive = False
                time.sleep(10)
                self.reopen_pcta()

	msg = "PCTA prompt not matched"
	raise AssertionError("%s:%s -> %s, PCTA Output :>>>%s<<<" \
        % (__name__,keyword_name,msg,result))
       

  
    def _ssh_sendcmd(self,cmd):
        keyword_name = "_ssh_sendcmd"
        result = ""
        data = ""		
        self.channel.send(cmd)
        # read the socket buffer until the channel exit_status_ready state
        while not self.channel.exit_status_ready():
            # read the socket buffer if the channel receive in ready state
            if self.channel.recv_ready() :
                # read the socket buffer
		result = ""
                data = self.channel.recv(1024)
                result = data
                while data :
                    try :
			#Huang Fang add below if/else for ta_start_capt_igmp_message waiting 1m.
			if re.search("PC_TA_.*>",result) :
                            return result
			else :
			    #Huang Fang add done
                            data = self.channel.recv(1024)
                            result += data
	                if re.search("PC_TA_.*>",result) :
                            return result
                    except socket.timeout:
                        return result
                    except Exception as inst :
                        logger.debug("Error : %s" % (inst))
                        return result
            if self.channel.recv_stderr_ready():
                error_buf = self.channel.recv_stderr(1024)
                errinfo = ""
                while error_buf :
                    errinfo += error_buf
                    try :
                        error_buf = self.channel.recv_stderr(1024)
                    except Exception as inst :
                        logger.warning("Error : %s" % (inst))
                        return errinfo  
			           
        return result
 

