import re
from robot.api import logger

def parse_pcta_packet(res):
    """
    Author: Alfred Zhang 2017-11-08
    This procedure used to parse a single packet recived from pcta
    """
    i=0
    packet={}

    opts=re.findall(r'-\w+',res)
    args=[o.strip() for o in re.split("|".join(opts)[1:],res)][1:]

    for i in range(len(opts)):
        packet[opts[i]]=args[i]
    return packet

def parse_pcta_result(output):
    """
    Author: Alfred Zhang 2017-11-08
    This procedure used to parse a packet list recived from pcta
    """
    o1=re.sub(r'(^.*received_l +{{)|(}}$)','',output)
    packets= [parse_pcta_packet(r) for r in  re.split(r'} +{',o1)]
    logger.info(packets)
    return packets

def check_value_from_pcta_result(cap_l, target):
    """
    Author: Alfred ZHANG 2017-11-08
    """
    if not isinstance(cap_l,list):
        return False
    TL=target.split(',')
    for e in cap_l:
        if e[TL[0]]==TL[1] :
            return True
     
    return False


