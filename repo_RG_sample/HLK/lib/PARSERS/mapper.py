import os,sys,pprint,itertools,re,logging

""" 
FUNCTION NAME : mapping_parserFunc

DESCRIPTION:
	This function is used to map the particular CLI show command to the respective parser function 

INPUT PARAMETERS : 
	showCommand : it contains the CLI show command which is formed from the LLK.
	sResponse : this contians the response of the ISAM for a particular CLI command

RETURN VALUE: the mapped function name suitable for the respective show commands

"""
def mapping_parserFunc(showCommand, sResponse):
	
	cli_showCommand = showCommand
	sRes = sResponse
	logging.info("Show Command entered into mapper : %s" %showCommand)
	

# this  loop is used to handle the xml format 
	if re.search('^info configure.*',cli_showCommand) and 'xml' in cli_showCommand:
		print "Mapped function is :parse_cli_infocmd_xml_detail()"		
		return('parse_cli_infocmd_xml_detail()')
	elif 'xml' in cli_showCommand:
		if re.search(".*<res-id.*",sRes):
			logging.info(" The mapped parser function --> parse_cli_show_xml_type01()")
			return('parse_cli_show_xml_type01()')
		else:
			logging.info(" The mapped parser function --> parse_cli_show_xml_type01()")
			return('parse_cli_show_xml_type02()')


#the next  loop is used to handle detail command cases only . 
	elif 'detail' in cli_showCommand:

		dMapping1 = { 'show router ospf area' : 'parse_cli_show_ipd_type03()',
					  'show qos-servicerouter service-ingress-network' : 'parse_cli_show_mpls_type05()',
					  'show qos-servicerouter service-ingress' : 'parse_cli_show_mpls_type05()', 
					  'show qos-servicerouter service-egress-network' : 'parse_cli_show_mpls_type07',
					  'show router interface' : 'parse_cli_show_mpls_type08()',
					  'show equipment slot' : 'parse_cli_show_generic_detail()',
					  'show ip shub vrf-media' : 'parse_cli_show_ipd_type03()',
					  'show interface port' : 'parse_cli_show_generic_detail()',
					  'show cfm mep-config-data' : 'parse_cli_show_generic_detail_type02()', 
					  'show cfm ccm-database' : 'parse_cli_show_generic_detail_type02()',
					  'show cfm md-config-data' : 'parse_cli_show_generic_detail_type02()',
					  'show cfm ma-config-data' : 'parse_cli_show_generic_detail_type02()', 
					  'show alarm current' : 'parse_cli_show_ipd_type09()',
                                          'show port' : 'parse_cli_show_mpls_type06()',
					  'show cfm mep-info':'parse_cli_show_generic_detail_type02()'	
					  } 
		for k,v in dMapping1.iteritems():
			if k in cli_showCommand:
				logging.info("The mapped parser function is : %s" %v)				
				return v
	 
#the next  loop is used to handle cases which contain the base commands alone

	else:

		dMapping2 = {   
		                'show equipment slot' : 'parse_cli_show_generic_type01()',				
				'show software-mngt oswp' : 'parse_cli_show_generic_type01()',
				'show software-mngt database' : 'parse_cli_show_generic_type01()',
				'show interface port' : 'parse_cli_show_generic_type01()',
				'show interface stack' : 'parse_cli_show_generic_type01()',
				'show vlan fdb-board' : 'parse_cli_show_generic_type01()',
				'show vlan shub-fdb' : 'parse_cli_show_generic_type01()',
				'show pppox-relay cross-connect monitor-statistics client-port' : 'parse_cli_show_generic_type01()',
				'show pppox-relay cross-connect session engine' : 'parse_cli_show_generic_type01()', 
				'show mcast active-groups' : 'parse_cli_show_generic_type01()',
				'show equipment capab-profile' : 'parse_cli_show_generic_type01()',
				'show equipment applique' : 'parse_cli_show_generic_type01()',
				'show equipment shelf-summary' : 'parse_cli_show_generic_type01()',
				'show equipment protection-element' : 'parse_cli_show_generic_type01()',
				'show equipment protection-group' : 'parse_cli_show_generic_type01()',
				'show equipment diagnostics sfp' : 'parse_cli_show_generic_type01()',
				'show equipment temperature' : 'parse_cli_show_generic_type01()',
				'show equipment transceiver-inventory' : 'parse_cli_show_generic_type01()',
				'show equipment ont operational-data' : 'parse_cli_show_generic_type01()',
				'show equipment ont slot' : 'parse_cli_show_generic_type01()',
				'show equipment ont sw-ctrl' : 'parse_cli_show_generic_type01()',
				'show equipment ont sw-version' : 'parse_cli_show_generic_type01()',
				'show equipment ont data-store' : 'parse_cli_show_generic_type01()',
				'show equipment ont optics' : 'parse_cli_show_generic_type01()',
				'show equipment ont pwr-shed' : 'parse_cli_show_generic_type01()',
				'show equipment ont tc-layer olt-side-on-demand current-interval' : 'parse_cli_show_generic_type01()',
				'show equipment ont tc-layer olt-side-on-demand previous-interval' : 'parse_cli_show_generic_type01()',
				'show equipment ont tc-layer ont-side current-interval' : 'parse_cli_show_generic_type01()',
				'show equipment ont tc-layer ont-side previous-interval' : 'parse_cli_show_generic_type01()',
				'show equipment rack' : 'parse_cli_show_generic_type01()',
				'show equipment shelf' : 'parse_cli_show_generic_type01()',
				'show trap manager' : 'parse_cli_show_generic_type01()',
				'show ip shub vrf-media' : 'parse_cli_show_ipd_type03()',
				'show alarm delta-log indeterminate' : 'parse_cli_show_generic_type01()',
				'show alarm delta-log major' : 'parse_cli_show_generic_type01()',
				'show alarm delta-log minor' : 'parse_cli_show_generic_type01()',
				'show alarm delta-log critical' : 'parse_cli_show_generic_type01()',
				'show alarm log table' : 'parse_cli_show_generic_type01()',
				'show alarm current table' : 'parse_cli_show_generic_type01()',
				'show vlan name' : 'parse_cli_show_generic_type01()',
				'show vlan priority-regen' : 'parse_cli_show_generic_type01()',
				'show vlan fdbid' : 'parse_cli_show_generic_type01()',
				'show qos profile-usage session' : 'parse_cli_show_generic_type01()',
				'show qos profile-usage marker' : 'parse_cli_show_generic_type01()',
				'show qos profile-usage policer' : 'parse_cli_show_generic_type01()',
				'show qos profile-usage queue' : 'parse_cli_show_generic_type01()',
				'show qos profile-usage scheduler-node' : 'parse_cli_show_generic_type01()',
				'show qos profile-usage cac' : 'parse_cli_show_generic_type01()',
				'show qos profile-usage ingress-qos' : 'parse_cli_show_generic_type01()',
				'show qos profile-usage shaper' : 'parse_cli_show_generic_type01()',
				'show qos profile-usage bandwidth' : 'parse_cli_show_generic_type01()',
				'show qos dsl-bandwidth' : 'parse_cli_show_generic_type01()',
				'show qos shdsl-bandwidth' : 'parse_cli_show_generic_type01()',
				'show qos ethernet-bandwidth' : 'parse_cli_show_generic_type01()',
				'show xdsl failure-status near-end' : 'parse_cli_show_generic_type01()',
				'show xdsl failure-status vect-line-near-end' : 'parse_cli_show_generic_type01()',
				'show xdsl board' : 'parse_cli_show_generic_type01()',
				'show dhcp-relay shub' : 'parse_cli_show_ipd_type03()', 
				'show pon ber-stats' : 'parse_cli_show_generic_type01()',
				'show dhcp-relay session' : 'parse_cli_show_generic_type01()',
				'show xdsl operational-data line' : 'parse_cli_show_generic_type03()',
 				'show xdsl-bonding operational-data near-end' : 'parse_cli_show_generic_type01()',
				'show xdsl-bonding operational-data far-end' : 'parse_cli_show_generic_type01()',
				'show xdsl-bonding operational-data group' : 'parse_cli_show_generic_type01()',
				'show xdsl-bonding operational-data link' : 'parse_cli_show_generic_type01()',
				'show xdsl-bonding failure-status group' : 'parse_cli_show_generic_type01()',
				'show xdsl-bonding summary' : 'parse_cli_show_generic_type01()',
				'show pon interface tc-layer current-interval' : 'parse_cli_show_generic_type01()',
				'show pon interface tc-layer previous-interval' : 'parse_cli_show_generic_type01()',
				'show pon interface mcast-tc-layer current-interval' : 'parse_cli_show_generic_type01()',
				'show pon interface mcast-tc-layer previous-interval' : 'parse_cli_show_generic_type01()',
				'show pon optics' : 'parse_cli_show_generic_type01()',
				'show pon protection' : 'parse_cli_show_generic_type01()',
				'show ani ont operational-data' : 'parse_cli_show_generic_type01()',
				'show ani ont video-stats' : 'parse_cli_show_generic_type01()',
				'show interface alarm' : 'parse_cli_show_generic_type01()',
				'show interface phy-itf-status' : 'parse_cli_show_generic_type01()',
				'show interface shub' : 'parse_cli_show_generic_type01()',
				'show interface stack' :  'parse_cli_show_generic_type01()',
				'show interface availability' : 'parse_cli_show_generic_type01()',
				'show system license' : 'parse_cli_show_generic_type01()',
				'show system memory-usage' :  'parse_cli_show_generic_type01()',
				'show system cpu-load' : 'parse_cli_show_generic_type01()',
				'show system cpu-load' : 'parse_cli_show_generic_type01()',
				'show system sync-if-timing-t4' : 'parse_cli_show_generic_type01()',
				'show igmp module counter' : 'parse_cli_show_generic_type01()', 
				'show igmp module time' : 'parse_cli_show_generic_type01()',
				'show igmp module miscellaneous' : 'parse_cli_show_generic_type01()',
				'show shdsl unit-status' : 'parse_cli_show_generic_type01()', 
				'show voice sip server' : 'parse_cli_show_generic_type01()',
				'show voice sip user-agent-ap' : 'parse_cli_show_generic_type01()',
				'show system entry' : 'parse_cli_show_ipd_type03()',
				'show system cde-bitmap' : 'parse_cli_show_ipd_type03()',
				'show igmp system' : 'parse_cli_show_ipd_type03()',
				'show system cde-bitmap' : 'parse_cli_show_ipd_type03()',
				'show transport snmp protocol-stats' : 'parse_cli_show_ipd_type03()',
				'show transport snmp engine' : 'parse_cli_show_ipd_type03()',
				'show transport system-parameters' : 'parse_cli_show_ipd_type03()',
				'show voice sip statistics system' : 'parse_cli_show_ipd_type03()',
				'show router rip table' : 'parse_cli_show_ipd_type01()',
				'show router rip group' : 'parse_cli_show_ipd_type01()',
				'show router rip neighbour' : 'parse_cli_show_ipd_type01()',
				'show router ospf interface' : 'parse_cli_show_ipd_type01()',
				'show router ospf neighbor': 'parse_cli_show_ipd_type01()',
				'show router route-table protocol ospf': 'parse_cli_show_ipd_type01()',
				'show router ospf database' : 'parse_cli_show_ipd_type01()',
				'show router ospf interaface area' : 'parse_cli_show_ipd_type01()',
				'show service fdb-mac' : 'parse_cli_show_ipd_type01()',
				'show service sdp' : 'parse_cli_show_ipd_type01()',
				'show service sap-using sap' : 'parse_cli_show_ipd_type01()',
				'show router rip database' : 'parse_cli_show_ipd_type02()',
				'show router ospf status' : 'parse_cli_show_ipd_type03()',
				'show filter mac' : 'parse_cli_show_ipd_type03()',
				'show router route-table protocol' : 'parse_cli_show_ipd_type04()',
				'show router interface' : 'parse_cli_show_ipd_type04()',
				'show router pim neighbor' : 'parse_cli_show_ipd_type04()',
				'show router bgp routes' : 'parse_cli_show_ipd_type04()', 
				'show router rip statistics' : 'parse_cli_show_ipd_type05()',
				'show router isis adjacency' : 'parse_cli_show_ipd_type06()',
				'show router ldp session' : 'parse_cli_show_mpls_type01()',
				'show router ldp discovery' : 'parse_cli_show_mpls_type02()',
				'show service sdp-using' : 'parse_cli_show_mpls_type02()',
				'show router mpls static-lsp' : 'parse_cli_show_mpls_type03()',
				'show port' : 'parse_cli_show_ipd_type08()',
				'show router igmp ssm-translate Igmp_oif_ISAM_vvpls' : 'parse_cli_show_mpls_type09()',
				'show session' : 'parse_cli_show_generic_type02()',
				'show mcast shub igs-vlan-grp' : 'parse_cli_show_generic_type01()',
				'show service sap-using sap' : 'parse_cli_show_ipd_type01()',
				'show service sap-using sap' : 'parse_cli_show_ipd_type01()',
				'show alarm current': 'parse_cli_show_ipd_type09()'
				 }

		for k,v in dMapping2.iteritems():
			
			if k in cli_showCommand:
				logging.info("The mapped parser function is : %s" %v)				
				return v
				
