import os,re
from robot.api import logger 

global parsed_out 

def check_error(res):
    data = res
    return_val = "pass" 
    val =re.search('.*Error: Invalid parameter.*|.*MINOR:.*|.*A:IPD.*|.*A:IP.*|Error :.*|.*No such user.*|\
		.*Login incorrect.*|.*Command making ambiguity.*|.*Invalid input at Marker.*|.*Login incorrect*|\
		.*Login incorrect.*|login:|.*Connection Refused.*|.*Permission denied, please try again.*|\
		.*No such user.*|.*Connection closed by foreign host.*|.*Unable to connect to remote host.*|\
		.*Internal processing error.*|Internal processing error.*|.*Error : a password must contain at least 1 special characters.*|\
		.*Command making ambiguity.*|.*command is not complete.*|.*invalid token.*|.*More .....To stop press.*|\
		.*Session Closed....*|.*Connection to.*|.*Login incorrect.*|.*Permission denied.*|.*Connection refused.*|\
		.Connection closed by foreign host.*|.*DENY.*|.*Enter Verb.*|.*DENY.*|pppoe start failed.*|\
		 pppoe start failed.*|pppoe start failed.*|dhcpd start failed.*|/tmp clean failed.*|Start failed.*|\
		 Restart failed.*|pcta stop failed.*|failed to stack channel.*|Call to C command failed :.*|\
		 ambiguous command name.*|extra option.*|ERROR:.*|can\'t read.*|Error:.*|File .* cannot be opened, err_code.*' ,data)
    if val :
	return_val = "fail"
                    
    return  return_val


def parse_simple_xml(cmdresponse):
	#fin  = open("data.txt",'r')
	data = cmdresponse.split("\n")
	inst = '<instance>'
	parsed = []
	parsed_data = {} 
	for val in data:
    		val =val.strip()
		val = re.sub( 'type=".*"','',val)
		val = re.sub( 'short-name=".*"','',val)
		val = re.sub( 'is-named=".*"','',val)
		#print " --->",val
    		line_matched = re.search('<.*name=\"(.*)\".*>(.*)<.*',val)
   		if line_matched:
       			if line_matched.group():
            			if line_matched.group(1):
					#print "matched group1 " ,line_matched.group(1)
                			val_sub = line_matched.group(1)
               				if "/" in val_sub:
                    				val_split =val_sub.split("/")
                    				#print "matched key *" ,val_split[0]
                    				#print "matched key *" ,val_split[1]
                    				key1 = val_split[0].strip()
                    				key2 = val_split[1].strip()
                			else:
                    				#print "matched key" ,line_matched.group(1).strip()
                    				key_val= line_matched.group(1).strip()
                		if line_matched.group(2):        
                    			if ":" in line_matched.group(2):
                        			val_split =line_matched.group(2).split(":")
                        			#print "matched val #" ,val_split[0]
                        			#print "matched val #" ,val_split[1]
						try:
                        				if key1:
                            					parsed_data[key1]=val_split[0].strip()
						except Exception:
							pass
						try:
                        				if key2:
                            					parsed_data[key2]=val_split[1].strip()
						except Exception:
							pass		
                    			else:
                        			#print "matched val ------" ,line_matched.group(2)
                        			param_val = line_matched.group(2).strip()
				try:
                			parsed_data[key_val]=param_val
				except Exception:
					pass

	print "parsed dict",parsed_data
	parsed.append(parsed_data)
	return parsed


def retrive_parsed_output(*resp):	
	for val in resp:
		print " RET ",val

def validate_parsed_data(**aArgs):
	global parsed_out	
	input_dict = aArgs
	inp_key =  input_dict.keys()
	keyunavail = 0 
	validate_flag = 0
	status = "pass"
	for key_in in inp_key :
		for val in parsed_out:
			if val.has_key(key_in) :
				if input_dict[key_in] == val[key_in]:
					logger.info("Expected val : %s : %s >>> Retrieved val : %s : %s <<< is matched" % (key_in,input_dict[key_in],key_in,val[key_in]))
				else:
					logger.error("Expected val :>>> %s : %s Retrieved val :>>> %s : %s is not matched" % (key_in,input_dict[key_in],key_in,val[key_in]))
					validate_flag = 1
			else:
				keyunavail =1 
				logger.error("Invalid Key value:%s"%(key_in))
	if keyunavail ==1 :
		status = "fail"
		logger.error("Input argument supplied to validate is incorrect ")
	if validate_flag ==1 :
		status = "fail"
		logger.error("Expected value is not matched with the retrieved val ")
	return status
	
def update_parsed_data(*args):
	global 	parsed_out
	parsed_out = args
	#logger.info("parsed data in UPDATE PARSED DATA: %s " % (str(parsed_out)))
