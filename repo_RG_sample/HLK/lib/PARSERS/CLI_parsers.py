import os,re,sys,itertools,pprint,copy
import mapper,logging
import cli_command


global showcmd

def parse_cli_infocmd_xml_detail():
    """
    FUNCTION NAME : parse_cli_infocmd_xml_detail()

    DESCRIPTION: This parser function supports the info commands.
    
    OUTPUT FORMAT : 'list of dicts'
    Author              Comments    
    Beno K Immanuel     Updated to parse all the fields 
    """
    global lParser_output,sResponse
    lParser_output = []
    node_data = {}
    instance_data = {} 
    sData = sResponse
    lData = sData.split("\n")
    hierarchy_start = '<hierarchy name=(.*)" type='
    hierarchy_end = '</hierarchy>'
    node_start ='<node name="(.*)" type'
    node_end ='</node>'
    parameter_pat = '<parameter name="(.*)" is'
    instance_start = '<instance type="(.*)\"'
    instance_end = '</instance>'
    field_pat = '\>(.*)\<'
    resid_pat = '\<res-id name\=\"(.*?)\" i'
    info_pat = '<info name="(.*)" type'
    INST_FLAG =False
    for lLine in  lData:        
        line= lLine.strip()   
        node_start_mat =re.search(node_start,line)
        if node_start_mat:
            if node_start_mat.group():
                node_name = node_start_mat.group(1)                
        parameter_start = re.search(parameter_pat,line)
        if parameter_start:
            if parameter_start.group():
                if parameter_start.group(1):
                    paramkey = parameter_start.group(1)
                    field_val = re.search(field_pat,line)
                    if field_val:
                        if field_val.group():
                            if field_val.group(1):
                                paramvalue = field_val.group(1)
                                paramvalue =re.sub('&quot;','"',paramvalue)
                                if INST_FLAG:
                                    instance_data[paramkey] = paramvalue
                                else:
                                    node_data[paramkey] = paramvalue         
                                               
        instance_startmat =re.search(instance_start,line)
        if instance_startmat:
            if instance_startmat.group():
                instance_name = instance_startmat.group(1)
                INST_FLAG =True
        resid_start = re.search(resid_pat,line)
        if resid_start:
            if resid_start.group():
                if resid_start.group(1):
                    residkey = resid_start.group(1)
                    field_val = re.search(field_pat,line)
                    if field_val:
                        if field_val.group():
                            if field_val.group(1):
                                residvalue = field_val.group(1)
                                residvalue =re.sub('&quot;','"',residvalue)
                                instance_data[residkey] = residvalue
        info_start = re.search(info_pat,line)
        if info_start:
            if info_start.group():
                if info_start.group(1):
                    infokey = info_start.group(1)
                    field_val = re.search(field_pat,line)
                    if field_val:
                        if field_val.group():
                            if field_val.group(1):
                                infovalue = field_val.group(1)
                                infovalue =re.sub('&quot;','"',infovalue)
                                instance_data[infokey] = infovalue
        instance_endmat =re.search(instance_end,line)
        if instance_endmat:
            if instance_endmat.group():
                lParser_output.append(instance_data)                
                instance_data ={}
                INST_FLAG =False 
        node_end_mat =re.search(node_start,line)
        if node_end_mat:
            if node_end_mat.group():
                if node_data:
                    lParser_output.append(node_data)
                    node_data = {}                    
        hierarchy_end_mat =re.search(hierarchy_end,line)
        if hierarchy_end_mat:
            if hierarchy_end_mat.group():
                if hierarchy_end_mat:
                    if node_data not in lParser_output :
                        lParser_output.append(node_data)
    return lParser_output

def parse_cli_show_xml_type01():
	global lParser_output, sResponse
	
	#sData_new = sResponse.read() 
	sData_new = sResponse
	lData_new1 = sData_new.split("\n")
	flag = 0
	lList1 = []
	lList2 = []
	dDict_temp  = { }
	lParser_output = []

	for lLine in lData_new1:
		if re.search("<res-id",lLine) and flag == 0:
			flag = 1
			y = re.search(".*\>(.*)\<.*",lLine)  #value
			lList2.append(y.group(1))
			x = re.search(".*name=\"(.*)\" .*",lLine) #key
			lList1.append(x.group(1))
		  
		if flag == 1 and re.search("\<info name\=", lLine):
			x1 = re.search("\<info name\=\"(.*)\" short-name.*", lLine)
			lList1.append(x1.group(1)) 
			y1 = re.search("\>(.*)\<\/info\>", lLine)
			lList2.append(y1.group(1))
		if re.search("\<instance\>",lLine) and flag == 1:
			
			dDict_temp = dict(itertools.izip(lList1,lList2))
			lParser_output.append(dDict_temp)
			flag = 0
			dDict_temp  = { }
			lList1 = []
			lList2 = []
	 	elif re.search("</instance>",lLine) and flag == 1:
			dDict_temp = dict(itertools.izip(lList1,lList2))
			lParser_output.append(dDict_temp)
			flag = 0
			dDict_temp  = { }
			lList1 = []
			lList2 = []
	
	#print lParser_output		
	return lParser_output



"""
Name : parse_cli_show_xml_type02()


Description : This procedure can be used to get multiple values in a single CLI call e.g. to get shelf
data from the node If xml data contains multiple values in each instance and multiple instances or node
and multiple nodes or hierarchy and multiple hierarchy ,this procedure can return a list of dict.

Supported formats of show commands : show equipment slot <id> xml detail
									 show software-mngt upload-download xml detail
									 show arp-relay-stats <id> xml details
									 show interface port vlan-port <id> xml details
									 show igmp channel protocol vlan <id> xml details
									 show mcast active-groups <id> xml details
									 show mcast grp-membership xml detail
									 show qos interface-bandwidth <id> xml details
									 show transport snmp engine xml detail
									 show equipment shelf <id> xml detaiils

Output : 'list of dict' containing the matched values.


sample output :
            [{'subscr-vlan': '0',
			  'user-port': '0',
              'arpreply-antispoof': '0',
              'arpreply-same-itf': '0',
              'arpreply-up': '0',
              'arpreq-antispoof': '0',
              'arpreq-dn': '0',
              'arpreq-same-itf': '0',
              'arpreq-up': '1',
              'inv-arpreply-dstip': '0',
              'inv-arpreply-srcip': '0',
              'inv-arpreply-srcmac': '0',
              'inv-arpreq-dstip': '0',
              'inv-arpreq-srcip': '0',
              'inv-arpreq-srcmac': '0',
              'inv-opcode': '0',
              'inv-pktlen': '0',
              'unsupported-addrlen': '0',
              'unsupported-hwtype': '0',
              'unsupported-l3proto': '0',
              'unsupported-probepkt': '0'}]

NOTE : this parser handles the exceptional cases of xml format with respect to type1
history :
Manoj Kumar       22/03/2012
creation
"""    


def parse_cli_show_xml_type02():
    global lParser_output, sResponse
    #sData = sResponse.read()
    sData = sResponse
    lData=sData.split('\n')
    flag = 0
    lList1 = []
    lList2 = []
    dDict_temp  = { }
    lParser_output = []    

    for lLine in lData:
         
        if re.search("<hier-id",lLine):
            flag = 1
            x = re.search(".*name=\"(.*)\" is-named.*",lLine)
            lList1.append(x.group(1))
        elif re.search("<node",lLine):
            flag = 1
            type1 = "\<info name\=\"(.*)\" type.*"
            
        elif re.search("<res-id",lLine):
              flag = 1
              type1 = "\<info name\=\"(.*)\" short-name.*"
              x = re.search(".*name=\"(.*)\" .*",lLine)
              lList1.append(x.group(1))
              y = re.search(".*\>(.*)\<.*",lLine)
              lList2.append(y.group(1))
                 
        elif re.search("<hierarchy",lLine):
            flag = 1
            type1 = "\<info name\=\"(.*)\" short-name.*"
            
        if re.search ("\<info name\=", lLine) and flag ==1:
            x1 = re.search(type1, lLine)
            lList1.append(x1.group(1))
            y1 = re.search("\>(.*)\<\/info\>", lLine)
            lList2.append(y1.group(1))
           
        if re.search("\<node\>",lLine) and flag == 1:
            dDict_temp = dict(itertools.izip(lList1,lList2))
            lParser_output.append(dDict_temp)
            flag = 0
            dDict_temp  = { }
            lList1 = []
            lList2 = []
           
        elif re.search("\<instance\>",lLine) and flag == 1:
            
            dDict_temp = dict(itertools.izip(lList1,lList2))
            lParser_output.append(dDict_temp)
            flag = 0
            
            
        elif re.search("\</hierarchy>",lLine) and flag == 1:
            dDict_temp = dict(itertools.izip(lList1,lList2))
            lParser_output.append(dDict_temp)
            flag = 0
            dDict_temp  = { }
            lList1 = []
            lList2 = []
    return lParser_output
   
"""
Function Name : parse_cli_show_generic_type01()

SAMPLE SHOW COMMAND FORMTS HANDLED:
	show equipment slot; show vlan bridge-port-fdb 
	show software-mngt oswp; show mcast active-groups 
	show software-mngt database; show interface port

OUTPUT FORMAT : 'list of dict' containing the matched values.
"""
def parse_cli_show_generic_type01():
	global lParser_output, sResponse
	
	lParser_output = [ ]
	#file_name = open ("show_cmd_2","r")
	#sData_new = sResponse.read()   
	sData_new = sResponse
	
	sData_new1 = sData_new.split("\n")
	
	flag = 0
	i = 0
	lVal_new = []
	primary_command = ""
	flag_check = 0 ####this is used to check if the primary_command formation as a list is done if so the control is transferred to the elif loop

	#the first part of this  loop is used to form the title line as a single list in the variable primary_command
	for lLine in sData_new1:
		if  "----+-----" in lLine:
			flag_check = 1
			continue
			
		if flag_check == 0:
			if "======" in lLine and flag == 0:
				flag = 1
			if flag == 1 and "|" in lLine:
				i += 1
				lList_name = ' '.join(lLine.split())
				lList_name = lList_name.split('|')
				lVal_new = []
				for temp in lList_name:
					temp = temp.strip()
					lVal_new.append(temp)
				if ( i == 1):
					primary_command = lVal_new
					y = len(primary_command)-1
				elif ( i > 1 and len(primary_command) == len(lVal_new)) :
					while (y >= 0):

						y = y - 1
						if ( primary_command[y] == ""):
							primary_command[y] = lVal_new[y]
						elif (lVal_new[y] == ""):
							primary_command[y] = primary_command[y]
						else:
							primary_command[y] = primary_command[y]+" "+lVal_new[y]
	# this part of the loop is to get the values from the parser and finally converts it into a format of list of dicts
		if (flag_check == 1):
			if ("-------" in lLine):
				flag_check = 0
				continue
			else:
				
				lList_values = ' '.join(lLine.split())
				lList_values = lList_values.split(" ")
				lColumn_line = []
				for temp in lList_values:
					temp = temp.strip()
					lColumn_line.append(temp)
				dDict_temp = { }
				dDict_temp = dict(itertools.izip(primary_command,lColumn_line))      ###this line converts the lists into dict format
				
				lParser_output.append(dDict_temp)
				
	
	return lParser_output







"""
FUNCTION NAME:  parse_cli_show_generic_type02()
DETAILS:
this parser comes under the generic case and is used to handle empty spaces in the value lines.
here these things will be replaced by "EMPTY"

command name : show session

INPUT SAMPLE FORMAT:

 3 Mar18 21:55:55.027 1.1.1.82.11.1: 1001 NMI ars_report_msg: REPLY::COM_CLI:COM_CLI:<REPLY#> show session
================================================================================
session table
================================================================================
id|operator   |from            |type  |stat|login-time         |idle-time   |num
--+-----------+----------------+------+----+-------------------+------------+---
1                               unused res  1970-01-01:00:00:00 000-00:00:00 leg
2                               unused res  1970-01-01:00:00:00 000-00:00:00 leg
3  isadmin     172.21.211.57    telnet act  2012-03-18:21:53:07 000-00:00:00 leg
4                               sftp   res  1970-01-01:00:00:00 000-00:00:00 leg
5                               unused free 1970-01-01:00:00:00 000-00:00:00 leg
6                               unused free 1970-01-01:00:00:00 000-00:00:00 leg
7                               unused free 1970-01-01:00:00:00 000-00:00:00 leg
8                               unused free 1970-01-01:00:00:00 000-00:00:00 leg
9                               unused free 1970-01-01:00:00:00 000-00:00:00 leg
10                              unused free 1970-01-01:00:00:00 000-00:00:00 leg
11                              unused free 1970-01-01:00:00:00 000-00:00:00 leg
12                              unused free 1970-01-01:00:00:00 000-00:00:00 leg
13                              unused free 1970-01-01:00:00:00 000-00:00:00 leg
--------------------------------------------------------------------------------
session count : 13
================================================================================

OUTPUT SAMPLE:

[{'from ': 'EMPTY',
  'id': '1',
  'idle-time ': '000-00:00:0',
  'login-time ': '1970-01-01:00:00:0',
  'num': 'leg',
  'operator ': 'EMPTY',
  'stat': 'res',
  'type ': 'unuse'},
 {'from ': 'EMPTY',
  'id': '1',
  'idle-time ': '000-00:00:0',
  'login-time ': '1970-01-01:00:00:0',
  'num': 'leg',
  'operator ': 'EMPTY',
  'stat': 'res',
  'type ': 'unuse'},
 {'from ': 'EMPTY',
  'id': '1',
  'idle-time ': '000-00:00:0',
  'login-time ': '1970-01-01:00:00:0',
  'num': 'leg',
  'operator ': 'EMPTY',
  'stat': 'res',
  'type ': 'unuse'},
 {'from ': 'EMPTY',
  'id': '1',
  'idle-time ': '000-00:00:0',
  'login-time ': '1970-01-01:00:00:0',
  'num': 'leg',
  'operator ': 'EMPTY',
  'stat': 'res',
  'type ': 'unuse'},
 {'from ': 'EMPTY',
  'id': '1',
  'idle-time ': '000-00:00:0',
  'login-time ': '1970-01-01:00:00:0',
  'num': 'leg',
  'operator ': 'EMPTY',
  'stat': 'res',
  'type ': 'unuse'},
 {'from ': 'EMPTY',
  'id': '1',
  'idle-time ': '000-00:00:0',
  'login-time ': '1970-01-01:00:00:0',
  'num': 'leg',
  'operator ': 'EMPTY',
  'stat': 'res',
  'type ': 'unuse'},
 {'from ': 'EMPTY',
  'id': '1',
  'idle-time ': '000-00:00:0',
  'login-time ': '1970-01-01:00:00:0',
  'num': 'leg',
  'operator ': 'EMPTY',
  'stat': 'res',
  'type ': 'unuse'},
 {'from ': 'EMPTY',
  'id': '1',
  'idle-time ': '000-00:00:0',
  'login-time ': '1970-01-01:00:00:0',
  'num': 'leg',
  'operator ': 'EMPTY',
  'stat': 'res',
  'type ': 'unuse'},
 {'from ': 'EMPTY',
  'id': '1',
  'idle-time ': '000-00:00:0',
  'login-time ': '1970-01-01:00:00:0',
  'num': 'leg',
  'operator ': 'EMPTY',
  'stat': 'res',
  'type ': 'unuse'},
 {'from ': 'EMPTY',
  'id': '1',
  'idle-time ': '000-00:00:0',
  'login-time ': '1970-01-01:00:00:0',
  'num': 'leg',
  'operator ': 'EMPTY',
  'stat': 'res',
  'type ': 'unuse'},
 {'from ': 'EMPTY',
  'id': '1',
  'idle-time ': '000-00:00:0',
  'login-time ': '1970-01-01:00:00:0',
  'num': 'leg',
  'operator ': 'EMPTY',
  'stat': 'res',
  'type ': 'unuse'},
 {'from ': 'EMPTY',
  'id': '1',
  'idle-time ': '000-00:00:0',
  'login-time ': '1970-01-01:00:00:0',
  'num': 'leg',
  'operator ': 'EMPTY',
  'stat': 'res',
  'type ': 'unuse'},
 {'from ': 'EMPTY',
  'id': '1',
  'idle-time ': '000-00:00:0',
  'login-time ': '1970-01-01:00:00:0',
  'num': 'leg',
  'operator ': 'EMPTY',
  'stat': 'res',
  'type ': 'unuse'}]

Output : 'list of dict' containing the matched values. 
"""



def parse_cli_show_generic_type02():
	lParser_output = [ ]
	#sData_new = sResponse.read() 
	sData_new = sResponse
	lData_new1 = sData_new.split("\n")
	check_line = 0
	dash_line = 0 
	lVal_line = []
	for lLine in lData_new1:
		if "================================" in lLine:
			check_line += 1
			continue
		if "----" in lLine:
			dash_line += 1
			if dash_line >1:
				return lParser_output
			continue
		if check_line == 2 and dash_line == 0 and "|" in lLine:
			sTitle_line = lLine
			lTitle_line = ' '.join(lLine.split())
			lTitle_line = lTitle_line.split('|')
		if dash_line == 1 and len(lLine) > 0:
			sVal_tempLine = lLine
			length1 = len(lTitle_line) - 1
			i = 0 # contains the index value of tilte elements
			while length1 >= 0:
				if length1 > 0:
					x1 = sTitle_line.index(lTitle_line[i])
					x2 = sTitle_line.index(lTitle_line[i+1])
					x3 = sVal_tempLine[x1:x2-2] #here -2 because before the title value 
				if length1 == 0:
					x1 = sTitle_line.index(lTitle_line[i])
					x3 = sVal_tempLine[x1:] 
				if re.search('\w',x3):
					x3 = x3.strip()
					lVal_line.append(x3)
				else:
					lVal_line.append("EMPTY") 
	## in value lines if any element is left blank it will be considered as "EMPTY"
									
				length1 -= 1
				i = i+1
			dDict_temp = dict(itertools.izip(lTitle_line,lVal_line))      ###this line converts the lists into dict format
			lParser_output.append(dDict_temp)

		


"""
Name : parse_cli_show_generic_type03

input :  show xdsl operational-data line 1/1/6/1 
======================================================================
line table
==========================================================================
         |         |opr-state     |       tx-rate       |            
if-index |adm-state|tx-rate-ds    |us         ds        |cur-op-mode 
---------+---------+--------------+----------+----------+--------------------
1/1/6/1   up        up :20622      945        20622      g992-5-a    
------------------------------------------------------------------------------
line count : 1
===============================================================================

output_sample:  [{'adm-state': 'up',
  'cur-op-mode': 'g992-5-a',
  'if-index': '1/1/6/1',
  'opr-state tx-rate-ds': 'up :20622',
  'tx-rate ds': '20622',
  'tx-rate us': '945'}]

Comment: This parser specially design to handle "show xdsl operational-data line XXX",As an exception of the generic type01,this parser will handle the cases or instances where single title is split into two different titles.
example:
			|	  tx-rate    	  |
			|us         ds        |

			this will be consider as "tx-rate us" & "tx-rate ds"
history:
		Created by
		Manoj Kumar
"""

def parse_cli_show_generic_type03():
	global sResponse,lParser_output
	#sData = sResponse.read() 
        sData = sResponse
	lData = sData.split('\n')
	flag =0
	dash_flag = 0
	count = 0
	lList =[]
	dDict = { }
	lParser_output =[]
	lList_value =[]
	for lLine in lData:
		if "==================" in lLine:
			flag +=1
			continue
		if "---------------" in lLine:
			dash_flag +=1
			continue
		if flag ==2 and dash_flag is 0:
			count +=1
			lLine = lLine.split('|')
			if count is 1:
			   p = lLine
			   p = [x.rstrip(' ') for x in p] 
			if count is 2:
				q = lLine
				#this case handle exception title lines
				for x in range(len(q)):
				# this will join the tx-rate title
					if x is 3:			
						q1 = q[x].split('   ')
						q1 = filter(None,q1)
						temp = p[x]+' '+q1[0]
						lList.append(temp)
					elif x is 4:
						temp = p[x-1]+' '+q1[1]
						lList.append(temp)
						temp = p[x]+ ' ' +q[x]
						lList.append(temp)
					else:
						temp = p[x]+' '+q[x]
						lList.append(temp)
					lList = [x.strip(' ') for x in lList]               
		if dash_flag is 1:
			lLine = lLine.split('  ')
			lLine = filter(None,lLine)
			lList_value +=lLine
			
		if dash_flag is 2:
			dash_flag =1
			lList_value = [x.strip(' ') for x in lList_value]
			lListKeyTemp = lList[2].split(" ")
			lListValTemp = lList_value[2].split(":")
			print "CHECK"
			print lListValTemp
			lListValTemp1 = []
			for temp in lListValTemp:
				lListValTemp1.append(temp.strip())
			lListValTemp = copy.copy(lListValTemp1)
				
			del lList[2],lList_value[2]
			lList.extend(lListKeyTemp)
			lList_value.extend(lListValTemp)
			for temp in lList_value:
				if re.search("^\r$",temp):
					iIndexTemp = lList_value.index(temp) 
					del lList_value[iIndexTemp]	
			dDict = dict(itertools.izip(lList,lList_value))
			lParser_output.append(dDict)
			lList_value = []
			print lParser_output
	return lParser_output

###################################################################


def parse_cli_show_ipd_type01():
	global sResponse,lParser_output
	lParser_output = [ ]
	#file_name = open ("p1","r")
	sData_new = sResponse
	lData_new1 = sData_new.split("\n")
	start_primaryCommand_line = 0
	end_primaryCommand_line = 0
	primaryCommand_line_count = 0
	flag = 0
	flag_value = 0
	i = 0
	y1_index = []
	y2_index = []
	for lLine in lData_new1:
	######## these two if loops of level 1 indendation are used to generate the primary_command
		if "==========" in lLine and flag == 0:
			
			start_primaryCommand_line += 1
			continue
			
		if (start_primaryCommand_line > 1) and ("------" not in lLine) and (end_primaryCommand_line == 0) :
			lVal_new = []
			primaryCommand_line_count += 1
			
	#the next if loop is used to get the title line 1 as a list
	#in split double spacing is used (ie)"  " inorder to merge the titles with less than double spacing together
			if primaryCommand_line_count == 1:
				lList_name = lLine.split("  ")
				
				for temp in lList_name:
					if temp:
						temp = temp.strip()
						lVal_new.append(temp)
			
				primary_command = lVal_new
				lList1_unsplit = lLine
	##the next if loop is used append the second title line the first if it exists
			if primaryCommand_line_count > 1:
				lList2_unsplit = lLine
				length1 = len(primary_command) - 1
				
				while length1 >= 0:
					
					if length1 > 0:
						x1 = lList1_unsplit.index(primary_command[i])
						x2 = lList1_unsplit.index(primary_command[i+1])
						
	####if value in second title line exists it is attached to repected value in first line 
						x3 = lList2_unsplit[x1:x2-1]
						
	### the next if loop is used to handle the last value in the title row			
					if length1 == 0:
						x1 = lList1_unsplit.index(primary_command[i])
						x3 = lList2_unsplit[x1:] 
						## in x3,  [x1:] is left empty to make it to check till the last index value of the second title line 
					if re.search('\w',x3):
						x3 = x3.strip()
						primary_command[i] = primary_command[i]+" "+x3
						
					i += 1
					length1 -= 1
			#print primary_command
			
	### the program goes into the rows after the title rows
		if "------" in lLine:
			flag += 1
			end_primaryCommand_line += 1
			
		if flag == 1 and "------" not in lLine:
			lList_name = ' '.join(lLine.split())
			lList_name = lList_name.split(" ")
			
			
			if len(lList_name) != len(primary_command) :
				for temp1 in lList_name:
					if re.search("\d+\:\d+\:\d+",temp1):
						
						y1_index = lList_name.index(temp1)
					if re.search("\d+\/\d+\/\d+",temp1):
						
						y2_index = lList_name.index(temp1)
						
				if y1_index and y2_index :
					lList_name[min(y1_index,y2_index)] =lList_name[min(y1_index,y2_index)]+" "+lList_name[max(y1_index,y2_index)] 
					del lList_name[max(y1_index,y2_index)]
						
			lColumn_line = [ ]
			for temp_strip in lList_name:
					temp_strip = temp_strip.strip()
					lColumn_line.append(temp_strip)
			
			dDict_temp = {}
			dDict_temp = dict(itertools.izip(primary_command,lColumn_line))      ###this line converts the lists into dict format
			lParser_output.append(dDict_temp)

	return lParser_output

"""

FUNCTION : parse_cli_show_ipd_type02()

DESCRIPTION:
this ipd parser is used if the title words are separated by a single space 
and there is only one title line

SAMPLE INPUT:

	show router rip database
===============================================================================
RIP Route Database
===============================================================================
Destination        Peer            NextHop         Metric Tag    TTL    Valid   
-------------------------------------------------------------------------------
200.0.0.0/9        10.1.2.2        0.0.0.0         3      0x0000 177    No     
200.0.0.0/9        10.1.3.3        0.0.0.0         2      0x0000 169    Yes    
-------------------------------------------------------------------------------
No. of Routes: 2
===============================================================================

SAMPLE OUTPUT:

[{'Destination': '200.0.0.0/9',
  'Metric': '3',
  'NextHop': '0.0.0.0',
  'Peer': '10.1.2.2',
  'TTL': '177',
  'Tag': '0x0000',
  'Valid': 'No'},
 {'Destination': '200.0.0.0/9',
  'Metric': '2',
  'NextHop': '0.0.0.0',
  'Peer': '10.1.3.3',
  'TTL': '169',
  'Tag': '0x0000',
  'Valid': 'Yes'}]

RETURN VALUE: lParser_output (list of dicts)

"""

def parse_cli_show_ipd_type02():
	global sResponse, lParser_output
	lParser_output = [ ]
	sData_new = sResponse
	lData_new1 = sData_new.split("\n")
	flag = 0
	check_line = 0
	counter = 0
	lTitle_val = []
			 
	for lLine in lData_new1:
		
		if "==========" in lLine and flag == 0:
			check_line += 1
			continue
		if "--------" in lLine:
			flag = 1
			counter += 1
			continue
	###### loop to get the title line
		if check_line == 2 and  flag == 0:
			lList_name = ' '.join(lLine.split())
			lList_name = lList_name.split(" ")
			
			lTitle_line = [ ]
			for temp_strip in lList_name:
					temp_strip = temp_strip.strip()
					lTitle_line.append(temp_strip)
			
		###### loop to get the value line and to form a dict, output
		if flag == 1 and counter < 2:
			lList_val = ' '.join(lLine.split())
			lList_val = lList_val.split(" ")
			lVal_line = []
			for temp_strip in lList_val:
					temp_strip = temp_strip.strip()
					lVal_line.append(temp_strip)
			dDict_temp = { }
			dDict_temp = dict(itertools.izip(lTitle_line,lVal_line))      
			lParser_output.append(dDict_temp)
	return lParser_output




"""

FUNCTION NAME: parse_cli_show_ipd_type03()

DESCRIPTION:
	this parser is used to handle show commands of the cli with format "TYPE : VALUE".
program primarily divided into two cases based on the number of ":" in the line.

SAMPLES OF HANDLED COMMANDS:
 show filter mac <id> ; show router ospf area detail ;show router ospf area detail 
 show router ospf status;

SAMPLE INPUT:

===============================================================================
 Mac Filter
===============================================================================
      Filter Id   : 1                                Applied         : Yes
      Scope       : Template                         Def. Action     : Forward
      Entries     : 1                                Type            : normal
      Description : (Not Specified)
-------------------------------------------------------------------------------
      Filter Match Criteria : Mac
-------------------------------------------------------------------------------
      Entry       : 1                                FrameType       : Unwn
      Description : (Not Specified)	
      Log Id      : n/a                              
      Src Mac     : 00:00:00:00:00:0a ff:ff:ff:ff:ff:ff
      Dest Mac    :                                    
      Dot1p       : Undefined                        Ethertype       : Undefined
      DSAP        : Undefined                        SSAP            : Undefined
      Snap-pid    : Undefined                        ESnap-oui-zero  : Undefined
      Match action: Drop                             
      Ing. Matches: 5 pkts (520 bytes)
      Egr. Matches: 0 pkts
       
===============================================================================

SAMPLE OUTPUT: 

[{'Applied': 'Yes',
  'DSAP': 'Undefined',
  'Def. Action': 'Forward',
  'Description': '(Not Specified)',
  'Dest Mac': '',
  'Dot1p': 'Undefined',
  'ESnap-oui-zero': 'Undefined',
  'Egr. Matches': '0 pkts',
  'Entries': '1',
  'Entry': '1',
  'Ethertype': 'Undefined',
  'Filter Id': '1',
  'Filter Match Criteria': 'Mac',
  'FrameType': 'Unwn',
  'Ing. Matches': '5 pkts (520 bytes)',
  'Log Id': 'n/a',
  'Match action': 'Drop',
  'SSAP': 'Undefined',
  'Scope': 'Template',
  'Snap-pid': 'Undefined',
  'Src Mac': '00:00:00:00:00:0a ff:ff:ff:ff:ff:ff',
  'Type': 'normal'}]

  PARSED OUTPUT FORMAT : list of dicts
"""

def parse_cli_show_ipd_type03():
	global lParser_output,sResponse 
	 
	lParser_output = [ ]
	#file_name = open ("pts","r")
	sData_new = sResponse
		
	lData_new1 = sData_new.split("\n")
	counter = 0
	lKey_list = []
	lValue_list = []
	lVemp_val = []
	colon_count = 0
	i = 0
	for lLine in lData_new1:
		i +=1
		colon_count = 0
		lTemp_val = []
		
		if "==========" in lLine:
			counter +=1
			continue
		if counter == 2 and "--------------" not in lLine:
			sX_line = ' '.join(lLine.split())
			lX_line = sX_line.split(" ")
			
			for temp in lX_line:
				if re.search("^:",temp):
					colon_count += 1
							
	#### the following if loop is for cases with a single ':' or more than two ':' (like "src mac") 
			if colon_count == 1 or colon_count > 2:
		
				x1_temp = lLine.partition(":")
				for temp1 in x1_temp:
					temp1 = temp1.strip()
					lTemp_val.append(temp1)
				
				lKey_list.append(lTemp_val[0])
				lValue_list.append(lTemp_val[len(lTemp_val)-1])
	#### this is for cases with two ':' in the same line (ex: key1 : value1       key2: value2)
			if colon_count == 2:
				
				if len(lX_line) <= 5:
					lX1_temp = lLine.partition(":")
					for temp1 in x1_temp:
						temp1 = temp1.strip()
						lTemp_val.append(temp1)
					
					lKey_list.append(lTemp_val[0])
					lValue_list.append(lTemp_val[len(Temp_val)-1])
				else:
								
					x1_temp = lLine.partition(":")
					x2_temp = lLine.rpartition(":")
					temp_firstkey = x1_temp[0]
					temp_firstkey = temp_firstkey.strip()
					lKey_list.append(temp_firstkey)
					temp_lastvalue = x2_temp[len(x2_temp)-1]
					temp_lastvalue = temp_lastvalue.strip()
					x3_temp = x1_temp[2]
					x3_temp = x3_temp.strip()
					x3_split = x3_temp.split("  ") ### splitting with double space
					
					lastelement_x3_index = len(x3_split)-1
					lastelement_x3_check = x3_split[len(x3_split)-1].split(":")
	###### since 'x3_split' variable is operated on double space the next if & else loop is used to handle exceptions with single spacing 			
					if re.search("\w",lastelement_x3_check[0]):
						del x3_split[lastelement_x3_index]
						x3_split.append(lastelement_x3_check[0])
					else:
						del x3_split[lastelement_x3_index]
						
					for tempval_x3split in x3_split:
						if re.search("\w",tempval_x3split):
							tempval_x3split = tempval_x3split.strip()
							lTemp_val.append(tempval_x3split)
					
					lValue_list.append(lTemp_val[0])
					lValue_list.append(temp_lastvalue)
					
					lKey_list.append(lTemp_val[1])
					
						
	dDict_temp = dict(itertools.izip(lKey_list,lValue_list))
	lParser_output.append(dDict_temp)

	#### uncomment the next line if the output is required as a list of lists and also comment the above two lines

	#parser_output = zip(key_list,value_list)
	print lParser_output
	return lParser_output



"""
Name : Parse_cli_show_ipd_type04 

Description: This parser will hadle multiple lines involving cases where the single title line 
is split into multiple lines

Sample Input : show router XXX static-route 

===============================================================================
     Static Route Table (Service: 4000)  Family: IPv4
===============================================================================
      Prefix                                        Tag         Met    Pref Type Act 
Next Hop                                    Interface
-------------------------------------------------------------------------------
    2.2.2.2/32                                    0           1      5    NH   Y   
        20.20.20.1                          vlan3300
           
-------------------------------------------------------------------------------
     No. of Static Routes: 1
===============================================================================

sample_output: [{'Act': 'Y',
  'Interface': 'vlan3300',
  'Met': '1',
  'Next Hop': '20.20.20.1',
  'Pref': '5',
  'Prefix': '2.2.2.2/32',
  'Tag': '0',
  'Type': 'NH'}]
 Return value format : list of dicts
  created by:
  Manoj Kumar

"""

def parse_cli_show_ipd_type04():
	global sResponse,lParser_output
	#input_file = open('1.txt','r')
	sData = sResponse.read()
	lData = sData.split('\n')
	count = 0
	count1 = 0
	flag = 0
	flag1 = 0
	flag2 = 0
	lParser_output = []
	dDict = { }
	dDict1 = { }
	for line in lData:

		if "====================================" in line:
			flag = 1
			continue
		
		if "----------------------------------" in line:
			flag1 = 1
			count +=1
			continue
			   
		if flag1 is 1 and count == 1:
			count1 +=1
			if count1%2 is 1:
				line = line.split(' ')
				line_val1 = filter(None,line)
				if len(line_val1)== len(line_key1):
					dDict = dict(itertools.izip(line_key1,line_val1))
				if len(line_val1) == len(line2_key1):
					dDict = dict(itertools.izip(line2_key1,line_val1))      
			else:
				line = line.split(' ')
				line_val2 = filter(None,line)
				if len(line_val2) ==len(line_key2):
					dDict1 = dict(itertools.izip(line_key2,line_val2))
					dDict.update(dDict1)
					lParser_output.append(dDict)
				if len(line_val2) == len(line2_key2):
					dDict1 = dict(itertools.izip(line2_key2,line_val2))
					dDict.update(dDict1)
					lParser_output.append(dDict)
					
		if flag is 1 and flag1 is 0:
			flag2 += 1
			if (flag2)%2 is 0:
				line1 = line.split(' ')
				line_key1 = filter(None,line1)
				line2 = line.split('  ')
				line2_key1 = filter(None,line2)  
			else :
				line1 = line.split('   ')
				line_key2= filter(None,line1)
				line2_key2 = line.split(' ')
				line2_key2 = filter(None,line2_key2)
				
				
	return lParser_output



"""
FUNCTION NAME : parse_cli_show_ipd_type05()

DESCRIPTION : This parser only handles this show command alone 
			  show router rip statistics 
HANDLED SHOW COMMAND :	show router rip statistics


SAMPLE INPUT:

*A:IPD3# show router rip statistics 

===============================================================================
RIP Statistics
===============================================================================
Learned Routes     : 0                  Timed Out Routes   : 0                 
Current Memory     : 978,888            Maximum Memory     : 1,048,576         
 
-------------------------------------------------------------------------------
Interface "isam-ipd1"
-------------------------------------------------------------------------------
Primary IP         : 10.1.1.1           Update Timer       : 30                
Timeout Timer      : 180                Flush Timer        : 120               
 
Counter                       Total           Last 5 Min      Last 1 Min
-------------------------------------------------------------------------------
Updates Sent                  0               0               0              
Triggered Updates             0               0               0              
Bad Packets Received          0               0               0              
RIPv1 Updates Received        0               0               0              
RIPv1 Updates Ignored         0               0               0              
RIPv1 Bad Routes              0               0               0              
RIPv1 Requests Received       0               0               0              
RIPv1 Requests Ignored        0               0               0              
RIPv2 Updates Received        0               0               0              
RIPv2 Updates Ignored         0               0               0              
RIPv2 Bad Routes              0               0               0              
RIPv2 Requests Received       0               0               0              
RIPv2 Requests Ignored        0               0               0              
Authentication Errors         0               0               0              
 
-------------------------------------------------------------------------------
Interface "isam-ipd2"
-------------------------------------------------------------------------------
Primary IP         : 10.2.1.1           Update Timer       : 30                
Timeout Timer      : 180                Flush Timer        : 120               
 
Counter                       Total           Last 5 Min      Last 1 Min
-------------------------------------------------------------------------------
Updates Sent                  0               0               0              
Triggered Updates             0               0               0              
Bad Packets Received          0               0               0              
RIPv1 Updates Received        0               0               0              
RIPv1 Updates Ignored         0               0               0              
RIPv1 Bad Routes              0               0               0              
RIPv1 Requests Received       0               0               0              
RIPv1 Requests Ignored        0               0               0              
RIPv2 Updates Received        0               0               0              
RIPv2 Updates Ignored         0               0               0              
RIPv2 Bad Routes              0               0               0              
RIPv2 Requests Received       0               0               0              
RIPv2 Requests Ignored        0               0               0              
Authentication Errors         0               0               0              
 
===============================================================================

 SAMPLE OUTPUT:

	[{'Interface "isam-ipd1"': [{'Flush Timer': '120',
                             'Primary IP': '10.1.1.1',
                             'Timeout Timer': '180',
                             'Update Timer': '30'},
                            {'Counter': 'Updates Sent',
                             'Last 1 Min': '0',
                             'Last 5 Min': '0',
                             'Total': '0'},
                            {'Counter': 'Triggered Updates',
                             'Last 1 Min': '0',
                             'Last 5 Min': '0',
                             'Total': '0'},
                            {'Counter': 'Bad Packets Received',
                             'Last 1 Min': '0',
                             'Last 5 Min': '0',
                             'Total': '0'},
                            {'Counter': 'RIPv1 Updates Received',
                             'Last 1 Min': '0',
                             'Last 5 Min': '0',
                             'Total': '0'},
                            {'Counter': 'RIPv1 Updates Ignored',
                             'Last 1 Min': '0',
                             'Last 5 Min': '0',
                             'Total': '0'},
                            {'Counter': 'RIPv1 Bad Routes',
                             'Last 1 Min': '0',
                             'Last 5 Min': '0',
                             'Total': '0'},
                            {'Counter': 'RIPv1 Requests Received',
                             'Last 1 Min': '0',
                             'Last 5 Min': '0',
                             'Total': '0'},
                            {'Counter': 'RIPv1 Requests Ignored',
                             'Last 1 Min': '0',
                             'Last 5 Min': '0',
                             'Total': '0'},
                            {'Counter': 'RIPv2 Updates Received',
                             'Last 1 Min': '0',
                             'Last 5 Min': '0',
                             'Total': '0'},
                            {'Counter': 'RIPv2 Updates Ignored',
                             'Last 1 Min': '0',
                             'Last 5 Min': '0',
                             'Total': '0'},
                            {'Counter': 'RIPv2 Bad Routes',
                             'Last 1 Min': '0',
                             'Last 5 Min': '0',
                             'Total': '0'},
                            {'Counter': 'RIPv2 Requests Received',
                             'Last 1 Min': '0',
                             'Last 5 Min': '0',
                             'Total': '0'},
                            {'Counter': 'RIPv2 Requests Ignored',
                             'Last 1 Min': '0',
                             'Last 5 Min': '0',
                             'Total': '0'},
                            {'Counter': 'Authentication Errors',
                             'Last 1 Min': '0',
                             'Last 5 Min': '0',
                             'Total': '0'}],
  'Interface "isam-ipd2"': [{'Flush Timer': '120',
                             'Primary IP': '10.2.1.1',
                             'Timeout Timer': '180',
                             'Update Timer': '30'},
                            {'Counter': 'Updates Sent',
                             'Last 1 Min': '0',
                             'Last 5 Min': '0',
                             'Total': '0'},
                            {'Counter': 'Triggered Updates',
                             'Last 1 Min': '0',
                             'Last 5 Min': '0',
                             'Total': '0'},
                            {'Counter': 'Bad Packets Received',
                             'Last 1 Min': '0',
                             'Last 5 Min': '0',
                             'Total': '0'},
                            {'Counter': 'RIPv1 Updates Received',
                             'Last 1 Min': '0',
                             'Last 5 Min': '0',
                             'Total': '0'},
                            {'Counter': 'RIPv1 Updates Ignored',
                             'Last 1 Min': '0',
                             'Last 5 Min': '0',
                             'Total': '0'},
                            {'Counter': 'RIPv1 Bad Routes',
                             'Last 1 Min': '0',
                             'Last 5 Min': '0',
                             'Total': '0'},
                            {'Counter': 'RIPv1 Requests Received',
                             'Last 1 Min': '0',
                             'Last 5 Min': '0',
                             'Total': '0'},
                            {'Counter': 'RIPv1 Requests Ignored',
                             'Last 1 Min': '0',
                             'Last 5 Min': '0',
                             'Total': '0'},
                            {'Counter': 'RIPv2 Updates Received',
                             'Last 1 Min': '0',
                             'Last 5 Min': '0',
                             'Total': '0'},
                            {'Counter': 'RIPv2 Updates Ignored',
                             'Last 1 Min': '0',
                             'Last 5 Min': '0',
                             'Total': '0'},
                            {'Counter': 'RIPv2 Bad Routes',
                             'Last 1 Min': '0',
                             'Last 5 Min': '0',
                             'Total': '0'},
                            {'Counter': 'RIPv2 Requests Received',
                             'Last 1 Min': '0',
                             'Last 5 Min': '0',
                             'Total': '0'},
                            {'Counter': 'RIPv2 Requests Ignored',
                             'Last 1 Min': '0',
                             'Last 5 Min': '0',
                             'Total': '0'},
                            {'Counter': 'Authentication Errors',
                             'Last 1 Min': '0',
                             'Last 5 Min': '0',
                             'Total': '0'}],
  'RIP Statistics': [{'Current Memory': '978,888',
                      'Learned Routes': '0',
                      'Maximum Memory': '1,048,576',
                      'Timed Out Routes': '0'}]}]

RETURNED VALUE FORMAT : list of dict of dicts


"""

def parse_cli_show_ipd_type05():
	global sResponse,lParser_output
	lParser_output = [ ]
	lParser_output_temp = [ ]
	#file_name = open ("ipd3out.txt","r")
	sData_new = sResponse.read()
	lData_new1 = sData_new.split("\n")
	counter = 0
	flag = 0
	colon_count = 0
	lKey_list = []
	lValue_list = []
	temp_val = []
	dash_count = 0
	lSub_title = []
	lTemp_dicList = []
	for lLine in lData_new1:
		
		temp_val = []
		colon_count = 0
		if "==========" in lLine:
			counter +=1
			if counter == 3:
				dash_count = 1
				lParser_output_temp.append(lTemp_dicList)
				dDict_temp = dict(itertools.izip(lSub_title,lParser_output_temp))
				lParser_output.append(dDict_temp)
				return lParser_output
			continue
		
		if counter == 1 and "=========" not in lLine:
			lSub_title.append(lLine)
			continue
		

		if "-----------" in lLine and counter == 2 :
			dash_count += 1
			flag = 1
	#### the next if loop is used notify  the presence of the subtitle line (say INTERFACE "isam-ipd1") if the occurrance is more than once
			if dash_count > 3:
				dash_count = 1
				lParser_output_temp.append(lTemp_dicList)
				lTemp_dicList = []
				
				continue
			if flag == 1 and dash_count == 1:
				dDict_temp = dict(itertools.izip(lKey_list,lValue_list))
				lTemp_dicList.append(dDict_temp) 
				lParser_output_temp.append(lTemp_dicList)
				lTemp_dicList = []
				dDict_temp = { }
				lKey_list = []
				lValue_list = []
				temp_val = []
				

	#### this will append the subtitle line to the list lSub_title
		if dash_count == 1 and "----------" not in lLine:
			lSub_title.append(lLine) 
			
			continue
		if dash_count == 2 and "----------" not in lLine:
			
			if ":" in lLine:
				lX_line = ' '.join(lLine.split())
				lX_line = lX_line.split(" ")
				for temp in lX_line:
					if ":" in temp:
						x1_temp = lLine.partition(":")
						x2_temp = lLine.rpartition(":")
						temp_firstkey = x1_temp[0]
						temp_firstkey = temp_firstkey.strip()
						lKey_list.append(temp_firstkey)
						
						temp_lastvalue = x2_temp[len(x2_temp)-1]
						temp_lastvalue = temp_lastvalue.strip()
						x3_temp = x1_temp[2]
						x3_temp = x3_temp.strip()
						x3_split = x3_temp.split("  ")
						
						lastelement_x3_index = len(x3_split)-1
						lastelement_x3_check = x3_split[len(x3_split)-1].split(":")
						
						if re.search("\w",lastelement_x3_check[0]):
							del x3_split[lastelement_x3_index]
							x3_split.append(lastelement_x3_check[0])
						else:
							del x3_split[lastelement_x3_index]
					
						for tempval_x3split in x3_split:
							if re.search("\w",tempval_x3split):
								tempval_x3split = tempval_x3split.strip()
								temp_val.append(tempval_x3split)
						lValue_list.append(temp_val[0])
						lValue_list.append(temp_lastvalue)
						
						lKey_list.append(temp_val[1])
			elif re.search('\w',lLine) :
					
	###### the next two lines are used to remove white spaces in line and substitute with | and later split it			
					sTitle_line = re.sub("  +","|",lLine)
					lTitle_line = sTitle_line.split('|')
					dDict_temp = dict(itertools.izip(lKey_list,lValue_list))
					
					lTemp_dicList.append(dDict_temp)
					
					lKey_list = []
					lValue_list = []
					temp_val = []
					
		if dash_count == 3 and "----------" not in lLine:	
			
			lLine = lLine.strip()
			sVal_row = re.sub("  +","|",lLine)
			lVal_row = sVal_row.split('|')
			if lVal_row != ['']:
				dDict_temp = dict(itertools.izip(lTitle_line,lVal_row))
				lTemp_dicList.append(dDict_temp)
				
		
	#### this if loop is used to handle the line with ':' in between and must come immediately below the ===== format	
		if counter == 2 and flag == 0:
			lX_line = ' '.join(lLine.split())
			lX_line = lX_line.split(" ")
			for temp in lX_line:
				if ":" in temp:
					x1_temp = lLine.partition(":")
					x2_temp = lLine.rpartition(":")
					temp_firstkey = x1_temp[0]
					temp_firstkey = temp_firstkey.strip()
					lKey_list.append(temp_firstkey)
					temp_lastvalue = x2_temp[len(x2_temp)-1]
					temp_lastvalue = temp_lastvalue.strip()
					x3_temp = x1_temp[2]
					x3_temp = x3_temp.strip()
					x3_split = x3_temp.split("  ")
					
					lastelement_x3_index = len(x3_split)-1
					lastelement_x3_check = x3_split[len(x3_split)-1].split(":")
					
					if re.search("\w",lastelement_x3_check[0]):
						del x3_split[lastelement_x3_index]
						x3_split.append(lastelement_x3_check[0])
					else:
						del x3_split[lastelement_x3_index]
					
					for tempval_x3split in x3_split:
						if re.search("\w",tempval_x3split):
							tempval_x3split = tempval_x3split.strip()
							temp_val.append(tempval_x3split)
					lValue_list.append(temp_val[0])
					lValue_list.append(temp_lastvalue)
					lKey_list.append(temp_val[1])



"""
FUNCTION NAME : parse_cli_show_ipd_type06()

DESCRIPTION : this parser is specifically designed to handle the show command  
				"show router isis adjacency"

HANDLED COMMAND : show router isis adjacency 

SAMPLE INPUT : 

	0....1001 NMI ars_report_msg: CMD::COM_CLI:COM_CLI: <CMD#> show router isis adjacency 
  3 Mar18 22:36:50.222 1.1.1.82.13.1: 1001 NMI ars_report_msg: REPLY::COM_CLI:COM_CLI:<REPLY#> show router isis adjacency 
     
===============================================================================
ISIS Adjacency
===============================================================================
 System ID                Usage State Hold Interface                     MT Enab
-------------------------------------------------------------------------------
IPD1                     L1    Up    7    isam-ipd1                     No     
-------------------------------------------------------------------------------
Adjacencies : 1
===============================================================================
     automation::

SAMPLE OUTPUT : 
	[{'Hold': '7',
  'Interface': 'isam-ipd1',
  'MT Enab': 'No',
  'State': 'Up',
  'System ID': 'IPD1',
  'Usage': 'L1'}]

RETURNED OUTPUT FORMAT : list of dicts
"""

def parse_cli_show_ipd_type06():
	global sResponse,lParser_output
	lParser_output = [ ]
	#file_name = open ("log1.txt","r")
	sData_new = sResponse.read()
	lData_new1 = sData_new.split("\n")
	check_line = 0
	flag = 0

	for lLine in  lData_new1:

		if "=============" in lLine:
			check_line += 1
			continue
		if "----------------" in lLine:
			flag += 1
			continue

		if check_line == 2 and flag == 0:
			lLine = lLine.strip()
			sX_subLine = re.sub("  +","|",lLine)
			lTitle_line = sX_subLine.split("|")
			lX_temp = lTitle_line[1].split(" ")
			lTitle_line.remove(lTitle_line[1])
			for i in range(len(lX_temp)):
				lTitle_line.insert(i+1,lX_temp[i])
					
		if flag == 1 and "--------" not in lLine:

			lList_values = ' '.join(lLine.split())
			lList_values = lList_values.split(" ")
			dDict_temp = { }
			dDict_temp = dict(itertools.izip(lTitle_line,lList_values))      ###this line converts the lists into dict format
			lParser_output.append(dDict_temp)
	return lParser_output



"""
Name :parse_cli_show_ipd_type08

input_sample:show port

leg:isadmin># show port 
===============================================================
Ports on NT
===============================================================
Port        Admin Link Port    Cfg  Oper LAG/ Port Port Port
Id          State      State   MTU  MTU  Bndl Mode Encp Type
---------------------------------------------------------------
nt:vp:1     Up    Yes  Up      1518 1518    - accs dotq vport 

===============================================================
Ports on NT-A
===============================================================
Port        Admin Link Port    Cfg  Oper LAG/ Port Port Port                   
Id          State      State   MTU  MTU  Bndl Mode Encp Type                   
---------------------------------------------------------------
nt-a:sfp:1  Up    Yes  Up      2048 2048    - accs dotq xcme  
nt-a:sfp:2  Up    No   Down    2048 2048    - accs dotq xcme  
nt-a:sfp:3  Up    No   Down    2048 2048    - accs dotq xcme  
nt-a:xfp:1  Down  No   Down    2048 2048    - accs dotq gige  

===============================================================
Ports on NT-B
===============================================================
Port        Admin Link Port    Cfg  Oper LAG/ Port Port Port                   
Id          State      State   MTU  MTU  Bndl Mode Encp Type                   
---------------------------------------------------------------
nt-b:sfp:1  Down  No   Down    2048 2048    - accs dotq xcme  
nt-b:sfp:2  Down  No   Down    2048 2048    - accs dotq xcme  
nt-b:sfp:3  Down  No   Down    2048 2048    - accs dotq xcme  
nt-b:xfp:1  Down  No   Down    2048 2048    - accs dotq gige  

===============================================================
Ports on LT
===============================================================
Port1        Admin Link Port2    Cfg  Oper LAG/ Port3 Port4 Port5                   
Id          State      State   MTU  MTU  Bndl Mode Encp Type
---------------------------------------------------------------
lt:1/1/1    Up    No   Down    2048 2048    - accs dotq lt    
lt:1/1/2    Up    No   Down    2048 2048    - accs dotq lt    
lt:1/1/3    Up    No   Down    2048 2048    - accs dotq lt    
lt:1/1/4    Up    No   Down    2048 2048    - accs dotq lt    
lt:1/1/5    Up    Yes  Up      2048 2048    - accs dotq lt    
lt:1/1/6    Up    No   Down    2048 2048    - accs dotq lt    
lt:1/1/7    Up    No   Down    2048 2048    - accs dotq lt    
lt:1/1/8    Up    No   Down    2048 2048    - accs dotq lt    
lt:1/1/12   Up    No   Down    2048 2048    - accs dotq lt    
lt:1/1/13   Up    No   Down    2048 2048    - accs dotq lt    
lt:1/1/14   Up    No   Down    2048 2048    - accs dotq lt    
lt:1/1/15   Up    No   Down    2048 2048    - accs dotq lt    
lt:1/1/16   Up    No   Down    2048 2048    - accs dotq lt    
lt:1/1/17   Up    No   Down    2048 2048    - accs dotq lt    
lt:1/1/18   Up    No   Down    2048 2048    - accs dotq lt    
lt:1/1/19   Up    No   Down    2048 2048    - accs dotq lt    

===============================================================
Ports on NT
===============================================================
Port        Admin Link Port    Cfg  Oper LAG/ Port Port Port                   
Id          State      State   MTU  MTU  Bndl Mode Encp Type
---------------------------------------------------------------
nt:mc:1     Up    Yes  Up      1518 1518    - accs dotq vport 
===============================================================


output_sample:[{'Ports on NT': [{'AdminAdmin State': 'Up',
                   'CfgCfg MTU': '1518',
                   'LAG/LAG/Bndl': '-',
                   'Link': 'Yes',
                   'OperOper MTU': '1518',
                   'PortPort Encp': 'dotq',
                   'PortPort Id': 'nt:vp:1',
                   'PortPort Mode': 'accs',
                   'PortPort State': 'Up',
                   'PortPort Type': 'vport'}]},
 {'Ports on NT-A': [{'AdminAdmin State': 'Up',
                     'CfgCfg MTU': '2048',
                     'LAG/LAG/Bndl': '-',
                     'Link': 'Yes',
                     'OperOper MTU': '2048',
                     'PortPort Encp': 'dotq',
                     'PortPort Id': 'nt-a:sfp:1',
                     'PortPort Mode': 'accs',
                     'PortPort State': 'Up',
                     'PortPort Type': 'xcme'}]},
 {'Ports on NT-B': [{'AdminAdmin State': 'Down',
                     'CfgCfg MTU': '2048',
                     'LAG/LAG/Bndl': '-',
                     'Link': 'No',
                     'OperOper MTU': '2048',
                     'PortPort Encp': 'dotq',
                     'PortPort Id': 'nt-b:sfp:1',
                     'PortPort Mode': 'accs',
                     'PortPort State': 'Down',
                     'PortPort Type': 'xcme'}]},
 {'Ports on LT': [{'AdminAdmin State': 'Up',
                   'CfgCfg MTU': '2048',
                   'LAG/LAG/Bndl': '-',
                   'Link': 'No',
                   'OperOper MTU': '2048',
                   'PortPort Encp': 'dotq',
                   'PortPort Id': 'lt:1/1/1',
                   'PortPort Mode': 'accs',
                   'PortPort State': 'Down',
                   'PortPort Type': 'lt'}]},
 {'Ports on NT': [{'AdminAdmin State': 'Up',
                   'CfgCfg MTU': '1518',
                   'LAG/LAG/Bndl': '-',
                   'Link': 'Yes',
                   'OperOper MTU': '1518',
                   'PortPort Encp': 'dotq',
                   'PortPort Id': 'nt:mc:1',
                   'PortPort Mode': 'accs',
                   'PortPort State': 'Up',
                   'PortPort Type': 'vport'}]}]

Output format : list of dict of dicts
Created by:
    SCRIPT :    Manoj Kumar
	
"""


def parse_cli_show_ipd_type08():

    #input_file = open('show_port','r')
    sData = sResponse
    lData = sData.split('\n')
    input_file.close()

    #variable declarations
    flag = 0
    dash_flag = 0
    lTitle = []
    count = 0
    i = 0
    x1_tempIndex = -1 
    x2_tempIndex = -1
    flag_repetition = 0
    lParser_temp =[]
    lParser_output =[]
    lParser =[]
    lList_value =[]

    for lLine in lData:
        if "====================" in lLine:
            flag +=1
            if flag is 3 :
                flag =1
                dash_flag =0
                dDict = dict(itertools.izip(lTitle_line1,lList_value))
                lParser_temp.append(dDict)
                lParser.append(lParser_temp)
                dDict1 = dict(itertools.izip(lTitle,lParser))
                dDict1.update(dDict1)
                lParser_output.append(dDict1)
                lParser_temp =[]
                lParser =[]
                lTitle =[]
                lList_value =[]     
            continue
        
        if "---------------" in lLine:
            dash_flag +=1
            continue
        
        if flag is 1:
            lLine = lLine.strip()
            lTitle.append(lLine)
            lTitle += filter(None,lTitle)
              
        if flag is 2 and dash_flag is 0:
           count +=1
           if count == 1:
                sTitle_line1 = lLine
                lLine = lLine.strip()
                sTitle_line1_temp = re.sub(" +","|",lLine)
                lTitle_line1 = sTitle_line1_temp.split("|")     
           if count == 2:
                sTitle_line2 = lLine
                length1 = len(lTitle_line1) - 1
                while length1 >= 0:
                    if length1 > 0:
                        x1 = sTitle_line1.index(lTitle_line1[i])
                        x2 = sTitle_line1.index(lTitle_line1[i+1])
                        if x1 == x2:
                            flag_repetition = 1
                            x2 = sTitle_line1.find(lTitle_line1[i+1],x1+1)			
                        if x1_tempIndex >= x1:
                            x1 = sTitle_line1.find(lTitle_line1[i],x1_tempIndex+1)							
                        if x2_tempIndex >= x2 :
                            x2 = sTitle_line1.find(lTitle_line1[i+1],x1+1)
                        x3 = sTitle_line2[x1:x2-1]
                        x1_tempIndex = x1
                        x2_tempIndex = x2
                    if length1 == 0:
                        x1 = sTitle_line1.find(lTitle_line1[i],x1_tempIndex + 1)
                        x3 = sTitle_line2[x1:]
                    if re.search('\w',x3):
                        x3 = x3.strip()
                        if "/" in lTitle_line1[i]:
                            lTitle_line1[i] = lTitle_line1[i]+ x3
                        else:
                            lTitle_line1[i] = lTitle_line1[i]+" "+x3
                    length1 = length1 - 1
                    i += 1
                    
        if dash_flag is 1:
            val = lLine.split()
            val= [x.strip(' ') for x in val]
            lList_value +=val

    return lParser_output


"""
########################################################################
#
#FUNCTION NAME : parse_cli_show_ipd_type09
#
#DETAILS: This parser is used to handly the CLI show command "show alarm current cfm"
#
#INPUT FORMAT:
==============================================================================================================================================================
cfm table (detailed)
==============================================================================================================================================================

--------------------------------------------------------------------------------------------------------------------------------------------------------------
cfm
--------------------------------------------------------------------------------------------------------------------------------------------------------------
                 index : 1                                      cfm-mep-id : 12                                  cfm-unique-id : 254884800
       additional-info : "dot1agCfmMdIndex : 12 , dot1agCfmMaIndex : 12"
--------------------------------------------------------------------------------------------------------------------------------------------------------------
  current alarm status
--------------------------------------------------------------------------------------------------------------------------------------------------------------
         cfm-defrdiccm : no                               cfm-defmacstatus : no                               cfm-defremoteccm : yes
       cfm-deferrorccm : no                                 cfm-defxconccm : no
--------------------------------------------------------------------------------------------------------------------------------------------------------------
cfm
--------------------------------------------------------------------------------------------------------------------------------------------------------------
                 index : 2                                      cfm-mep-id : 20                                  cfm-unique-id : 254885016
       additional-info : "dot1agCfmMdIndex : 12 , dot1agCfmMaIndex : 12"
--------------------------------------------------------------------------------------------------------------------------------------------------------------
  current alarm status
--------------------------------------------------------------------------------------------------------------------------------------------------------------
         cfm-defrdiccm : no                               cfm-defmacstatus : no                               cfm-defremoteccm : yes
       cfm-deferrorccm : no                                 cfm-defxconccm : no
--------------------------------------------------------------------------------------------------------------------------------------------------------------
cfm
--------------------------------------------------------------------------------------------------------------------------------------------------------------
                 index : 3                                      cfm-mep-id : 19                                  cfm-unique-id : 254884992
       additional-info : "dot1agCfmMdIndex : 12 , dot1agCfmMaIndex : 12"
--------------------------------------------------------------------------------------------------------------------------------------------------------------
  current alarm status
--------------------------------------------------------------------------------------------------------------------------------------------------------------
         cfm-defrdiccm : no                               cfm-defmacstatus : no                               cfm-defremoteccm : yes
       cfm-deferrorccm : no                                 cfm-defxconccm : no
==============================================================================================================================================================

SAMPLE OUTPUT FORMAT:
[{'cfm-deferrorccm': 'no',
  'cfm-defmacstatus': 'no',
  'cfm-defrdiccm': 'no',
  'cfm-defremoteccm': 'yes',
  'cfm-defxconccm': 'no',
  'cfm-mep-id': '12',
  'cfm-unique-id': '254884800',
  'dot1agCfmMaIndex': '12',
  'dot1agCfmMdIndex': '12',
  'index': '1'},
 {'cfm-deferrorccm': 'no',
  'cfm-defmacstatus': 'no',
  'cfm-defrdiccm': 'no',
  'cfm-defremoteccm': 'yes',
  'cfm-defxconccm': 'no',
  'cfm-mep-id': '20',
  'cfm-unique-id': '254885016',
  'dot1agCfmMaIndex': '12',
  'dot1agCfmMdIndex': '12',
  'index': '2'},
 {'cfm-deferrorccm': 'no',
  'cfm-defmacstatus': 'no',
  'cfm-defrdiccm': 'no',
  'cfm-defremoteccm': 'yes',
  'cfm-defxconccm': 'no',
  'cfm-mep-id': '19',
  'cfm-unique-id': '254884992',
  'dot1agCfmMaIndex': '12',
  'dot1agCfmMdIndex': '12',
  'index': '3'}]


"""



def parse_cli_show_ipd_type09():
	global lData_new1,lParser_output,sResponse
	flag_doubleLine = 0
	flag_singleLine = 0
	lParser_output = []
	lKey_list = []
	lVal_list = []
	sData_new = copy.copy(sResponse)
	lData_new1 = sData_new.split("\n")
	index = 0
	skip = False
	#lData_new1 = copy.copy(args)
	for lLine in lData_new1:
		index += 1
		if skip == True :
			skip = False
			continue
		if "========================" in lLine:
			flag_doubleLine += 1
			if flag_doubleLine == 3 :
				dDict_temp = dict(itertools.izip(lKey_list,lVal_list))
				lParser_output.append(dDict_temp)
				
				return lParser_output
			continue
		if "-----------------------------" in lLine:
			flag_singleLine += 1
			if flag_singleLine == 5 :
				dDict_temp = dict(itertools.izip(lKey_list,lVal_list))
				lKey_list = []
				lVal_list = []
				lParser_output.append(dDict_temp)
				flag_singleLine = 1
				
			continue
		if flag_singleLine == 2 or flag_singleLine == 4:
			#lX_line = ' '.join(lLine.split())
			if  re.search(".*additional-info.*",lLine):
				lLinePartition = lLine.strip().partition(":")
				dataline = lLinePartition[len(lLinePartition) -1]
				if not "-----------------------------" in lData_new1[index]:
					dataline += lData_new1[index]
					skip = True
				dataline = re.sub(r'\s+','',dataline)
				dataline = re.sub(r'\"','',dataline)
				lLineSplit = dataline.strip().split(",")
				
			else :
				lLine = re.sub(' : ',':',lLine)
				lLine = re.sub('\s+',' ',lLine)
				lLineSplit = lLine.strip().split()
			for temp in lLineSplit:
				if re.search("\w",temp) and re.search(":",temp):					
					lTempSplit = temp.strip().split(":")					
					lKey_list.append(lTempSplit[0].strip())
					lVal_list.append(lTempSplit[1].strip())






"""  
FUNCTION NAME : parse_cli_mpls_type01()

DESCRIPTION : This mpls parser is specifically designed for cases where the fields in the 
title line are split by more than a single space and the individual fields can have
one, two or three values (ex: 'Peer LDP Id' , 'Adj Type ' ,'State'). 

SAMPLE INPUT : 

show router ldp session
==============================================================================
LDP Sessions
==============================================================================
Peer LDP Id        Adj Type   State         Msg Sent  Msg Recv  Up Time        
------------------------------------------------------------------------------
172.30.1.2:0       Link       Established   47        17        0d 00:00:29  
172.30.1.3:0       Link       Established   63        15        0d 00:00:30  
------------------------------------------------------------------------------
No. of Sessions: 2


SAMPLE OUTPUT :

[{'Adj Type': 'Link',
  'Msg Recv': '17',
  'Msg Sent': '47',
  'Peer LDP Id': '172.30.1.2:0',
  'State': 'Established',
  'Up Time': '0d 00:00:29'},
 {'Adj Type': 'Link',
  'Msg Recv': '15',
  'Msg Sent': '63',
  'Peer LDP Id': '172.30.1.3:0',
  'State': 'Established',
  'Up Time': '0d 00:00:30'}]

 RETURNED OUTPUT FORMAT : list of dicts
"""


def parse_cli_show_mpls_type01():
	global lParser_output, sResponse
	lParser_output = [ ]
	#file_name = open ("mpls01.txt","r")
	sData_new = sResponse.read()
	lData_new1 = sData_new.split("\n")
	check_line = 0
	flag = 0

	for lLine in  lData_new1:

		if "=============" in lLine:
			check_line += 1
			continue
		if "----------------" in lLine:
			flag += 1
			continue
		if check_line == 2 and flag == 0:
			lLine = lLine.strip()
			sX_subLine = re.sub("  +","|",lLine)
			lTitle_line = sX_subLine.split("|")
			
		if flag == 1 and "--------" not in lLine:
			lLine = lLine.strip()
			sX_valLine = re.sub("  +","|",lLine)
			lVal_line = sX_valLine.split("|")
			dDict_temp = { }
			dDict_temp = dict(itertools.izip(lTitle_line,lVal_line))      ###this line converts the lists into dict format
			lParser_output.append(dDict_temp)
	return lParser_output




#####################################################################################
"""
FUNCTION NAME : parse_cli_show_mpls_type02

DESCRIPTION : this mpls parser is used to handle the fields in the title line which
are separated by atleast a single space.Here the fields in the value rows will be 
separated, by atleast a single or more number of spaces

SAMPLE INPUT:

===============================================================================
ISIS Adjacency
===============================================================================
 System ID                Usage State Hold Interface                     MT Enab
-------------------------------------------------------------------------------
IPD1                     L1    Up    7    isam-ipd1                     No     
-------------------------------------------------------------------------------
Adjacencies : 1
===============================================================================
     automation::

SAMPLE OUTPUT :

	[{'Hold': '7',
  'Interface': 'isam-ipd1',
  'MT Enab': 'No',
  'State': 'Up',
  'System ID': 'IPD1',
  'Usage': 'L1'}]

FORMATS OF SHOW COMMANDS SUPPORTED:

1)show router ldp discovery
2)show service sdp-using
3)show service id xx sdp
4) Show router isis adjacency

RETURNED OUTPUT FORMAT : list of dicts

"""
########################################################################################

def parse_cli_show_mpls_type02():
	global lParser_output, sResponse
	lParser_output = [ ]
	#file_name = open ("mpls02.txt","r")
	sData_new = sResponse.read()
	lData_new1 = sData_new.split("\n")
	check_line = 0
	flag = 0
	count_val = 0
	i = 0
	lTitle_line = []
	for lLine in  lData_new1:
		if "=============" in lLine:
			check_line += 1
			continue
		if "----------------" in lLine:
			flag += 1
			continue
		if check_line == 2 and flag == 0:
			sTitle_line = lLine.strip()
		if flag > 1 :
			return lParser_output
		if flag == 1 and "--------" not in lLine:
			count_val += 1
			sVal_line1 = ' '.join(lLine.split())
			lVal_line = sVal_line1.split(" ")
	###### inside this 'if' loop initially based on the index values of the value lines in 
	#### string format, the corresponding title line is formed.
			if count_val == 1:
				sVal_line = lLine.strip()
				length1 = len(lVal_line) - 1
				while length1 >= 0:
					if length1 > 0:
						x1 = sVal_line.index(lVal_line[i])
						x2 = sVal_line.index(lVal_line[i+1])
						x3 = sTitle_line[x1:x2-1]
	### the next if loop is used to handle the last value in the title row			
					if length1 == 0:
						x1 = sVal_line.index(lVal_line[i])
						x3 = sTitle_line[x1:] 
	## the next if loop helps in the formation of the title row
					if re.search('\w',x3):
						x3 = x3.strip()
						lTitle_line.append(x3)
					length1 = length1 - 1
					i += 1
			dDict_temp = { }
			dDict_temp = dict(itertools.izip(lTitle_line,lVal_line))      ###this line converts the lists into dict format
			lParser_output.append(dDict_temp)


##########################################################################################
"""
FUNCTION NAME : parse_cli_show_mpls_type03()

DESCRIPTION : This parser is suitable only for the show command "show router mpls static-lsp"

SUPPORTED SHOW COMMAND FORMAT: show router mpls static-lsp

INPUT FORMAT:

===============================================================================
MPLS Static LSPs (Originating)
==============================================================================
LSP Name     To              Next Hop        Out Label Up/Down Time   Adm  Opr 
ID                                                                            
-------------------------------------------------------------------------------
toipd2       172.30.1.2      172.30.3.3      32        0d 00:00:12    Up   Up  
1           
------------------------------------------------------------------------------
LSPs : 1
===============================================================================
automation::

SAMPLE OUTPUT:

[{'Adm': 'Up',
  'ID': '1',
  'LSP Name': 'toipd2',
  'Next Hop': '172.30.3.3',
  'Opr': 'Up',
  'Out Label': '32',
  'To': '172.30.1.2',
  'Up/Down Time': '0d 00:00:12'}]

OUTPUT FORMAT : list of dicts
"""
########################################################################################


def parse_cli_show_mpls_type03():
	global lParser_output, sResponse
	lParser_output = [ ]
	#file_name = open ("mpls04out.txt","r")
	sData_new = sResponse.read()
	lData_new1 = sData_new.split("\n")
	flag = 0
	check_line = 0
	count_titleLine = 0
	count_valLine = 0
	lval_line = []
	for lLine in  lData_new1:
		if "=============" in lLine:
			check_line += 1
			continue
		if "-------------" in lLine:
			flag += 1
		if check_line == 2 and flag == 0 and "=============" not in lLine:
			count_titleLine += 1
			if count_titleLine == 1:
				lLine = lLine.strip()
				sTemp_title = re.sub("  +","|",lLine)
				lTitle_line = sTemp_title.split("|")
				sX_temp =  lTitle_line[3]
				sX_temp1 = re.sub("l Up","l|Up",sX_temp)
				lX_temp1 = sX_temp1.split("|")
				lTitle_line.remove(lTitle_line[3])
				for i in range(len(lX_temp1)):
					lTitle_line.insert(i+1+2,lX_temp1[i])
				lTitle_line.append("ID")
		if flag == 1 and "-----------" not in lLine:
			count_valLine += 1
			lLine = lLine.strip()
			if count_valLine == 1:
				
				sTemp_val =  re.sub("  +","|",lLine)
				lVal_line= sTemp_val.split("|")

			if count_valLine == 2:
				lVal_line.append(lLine)
				dDict_temp = { }
				dDict_temp = dict(itertools.izip(lTitle_line,lVal_line))      ###this line converts the lists into dict format
				lParser_output.append(dDict_temp)
				lVal_line = []
				count_valLine = 0
	return lParser_output

########################################################################################
"""

FUNCTION NAME : parse_cli_show_mpls_type04()

DESCRIPTION : This mpls parser is designed to handle cases where there are two title 
lines and the fields in the first title line can have the same values. This is an extension
of IPD type o2.


SAMPLE INPUT:
	
===============================================================================
LDP Interfaces
===============================================================================
Interface                        Adm Opr  Hello  Hold  KA     KA      Transport
                                          Factor Time  Factor Timeout Address  
-------------------------------------------------------------------------------
isam-ipd1                        Up  Up   8      80    15     110     Interface
isam-ipd2                        Up  Up   10     90    10     100     System   
-------------------------------------------------------------------------------
No. of Interfaces: 2
===============================================================================
automation::

SAMPLE OUTPUT :

	[{'Adm': 'Up',
  'Hello Factor': '8',
  'Hold Time': '80',
  'Interface': 'isam-ipd1',
  'KA Factor': '15',
  'KA Timeout': '110',
  'Opr': 'Up',
  'Transport Address': 'Interface'},
 {'Adm': 'Up',
  'Hello Factor': '10',
  'Hold Time': '90',
  'Interface': 'isam-ipd2',
  'KA Factor': '10',
  'KA Timeout': '100',
  'Opr': 'Up',
  'Transport Address': 'System'}]

HANDLED SHOW COMMAND FORMATS : 
1)	show router ldp interface
2)	show router ldp peer

RETURNED VALUE FORMAT :  list of dicts

"""
########################################################################################

def parse_cli_show_mpls_type04():
	global lParser_output, sResponse
	lParser_output = [ ]
	#file_name = open ("asdf.txt","r")
	sData_new = sResponse.read()
	lData_new1 = sData_new.split("\n")
	flag = 0
	check_line = 0
	counter = 0
	i = 0
	flag1 = 0
	for lLine in lData_new1:
		if "=============" in lLine and flag == 0:
			check_line += 1
			continue
		if "-------------" in lLine:
			flag += 1
			continue
		if check_line == 2 and flag == 0:
			counter += 1
			if counter == 1:
				sTitle_line1 = lLine
				lLine = lLine.strip()
				sTitle_line1_temp = re.sub(" +","|",lLine)
				lTitle_line1 = sTitle_line1_temp.split("|")
			if counter == 2:
				sTitle_line2 = lLine
				length1 = len(lTitle_line1) - 1
				while length1 >= 0:
						if length1 > 0:
							x1 = sTitle_line1.index(lTitle_line1[i])
							x2 = sTitle_line1.index(lTitle_line1[i+1])
							x3 = sTitle_line2[x1:x2-1]
	######## the next two if loops are used to handle title lines that contain same title word adjacent to one another
							if flag1 == 1:
								flag1 = 0
								x1 = sTitle_line1.rfind(lTitle_line1[i])
								x3 = sTitle_line2[x1:x2-1]
							if x2 <= x1 and flag == 0 :
								temp = lTitle_line1[i+1]
								x2 = sTitle_line1.rfind(temp)
								x3 = sTitle_line2[x1:x2-1]	
								flag1 = 1
		
						if length1 == 0:
							x1 = sTitle_line1.index(lTitle_line1[i])
							x3 = sTitle_line2[x1:] 
					
						if re.search('\w',x3):
							x3 = x3.strip()
							lTitle_line1[i]= lTitle_line1[i]+" "+x3
						length1 = length1 - 1
						i += 1
		if flag == 1 and "------------" not in lLine:
			lVal_line = ' '.join(lLine.split())
			lVal_line = lVal_line.split(" ")
			dict_temp = {}
			dict_temp = dict(itertools.izip(lTitle_line1,lVal_line))      ###this line converts the lists into dict format
			lParser_output.append(dict_temp)
	return lParser_output	


#########################################################################################
"""

FUNCTION NAME : parse_cli_show_mpls_type05()

DESCRIPTION : This mpls parser is designed to handle cases with multiple formats. 
 

SAMPLE INPUT:
	
===============================================================================
QoS Service Ingress
===============================================================================
Service-Ingress Policy (1)
-------------------------------------------------------------------------------
Policy-id        : 1                    Scope            : Template
Default FC       : be                   Default Profile  : In
Description      : Default Policy
Untagged Dot1p   : 0                    
-------------------------------------------------------------------------------
Dot1p               FC                  Profile                                
-------------------------------------------------------------------------------
0                   be                  In                                     
1                   l2                  In                                     
2                   af                  In                                     
3                   l1                  In                                     
4                   h2                  In                                     
5                   ef                  In                                     
6                   h1                  In                                     
7                   nc                  In                                     
-------------------------------------------------------------------------------
DSCP                FC                  Profile                                
-------------------------------------------------------------------------------
No Matching Entries
-------------------------------------------------------------------------------
PrecValue           FC                  Profile                                
-------------------------------------------------------------------------------
0                   be                  In                                     
1                   l2                  In                                     
2                   af                  In                                     
3                   l1                  In                                     
4                   h2                  In                                     
5                   ef                  In                                     
6                   h1                  In                                     
7                   nc                  In                                     
===============================================================================

SAMPLE OUTPUT :

[{'Default FC': 'be',
  'Default Profile': 'In',
  'Description': 'Default Policy',
  'Policy-id': '1',
  'Scope': 'Template',
  'Untagged Dot1p': '0'},
 {'Dot1p': '0', 'FC': 'be', 'Profile': 'In'},
 {'Dot1p': '1', 'FC': 'l2', 'Profile': 'In'},
 {'Dot1p': '2', 'FC': 'af', 'Profile': 'In'},
 {'Dot1p': '3', 'FC': 'l1', 'Profile': 'In'},
 {'Dot1p': '4', 'FC': 'h2', 'Profile': 'In'},
 {'Dot1p': '5', 'FC': 'ef', 'Profile': 'In'},
 {'Dot1p': '6', 'FC': 'h1', 'Profile': 'In'},
 {'Dot1p': '7', 'FC': 'nc', 'Profile': 'In'},
 {'DSCP': ' ', 'FC': ' ', 'Profile': ' '},
 {'FC': 'be', 'PrecValue': '0', 'Profile': 'In'},
 {'FC': 'l2', 'PrecValue': '1', 'Profile': 'In'},
 {'FC': 'af', 'PrecValue': '2', 'Profile': 'In'},
 {'FC': 'l1', 'PrecValue': '3', 'Profile': 'In'},
 {'FC': 'h2', 'PrecValue': '4', 'Profile': 'In'},
 {'FC': 'ef', 'PrecValue': '5', 'Profile': 'In'},
 {'FC': 'h1', 'PrecValue': '6', 'Profile': 'In'},
 {'FC': 'nc', 'PrecValue': '7', 'Profile': 'In'}]

HANDLED SHOW COMMAND FORMATS : 
	This program will only handle the following output formats

1)	show qos-servicerouter service-ingress-network <id> detail
2)	show qos-servicerouter service-ingress <id> detail

RETURNED VALUE FORMAT :  list of dict of dicts

"""
#######################################################################################

def parse_cli_show_mpls_type05():
	global lParser_output,sResponse
	lParser_output = [ ]
	#file_name = open ("mpls05.txt","r")
	sData_new = sResponse.read()
	lData_new = sData_new.split("\n")
	count_line = 0
	flag = 0
	lKey_list = []  #### this list contains the title values
	lValue_list = []  ####### this list contains value elements
	dDict_temp = { }
	counter = 0
	for lLine in lData_new :
						
		if "=============" in lLine :
			count_line += 1
			continue
		if "-----------" in lLine and count_line == 2:
			flag += 1
			continue
		## the next if loop is used to find the title lines due to alternative occurances of the title lines 
		##### between "----------" lines in show command output format. It also helps in geeting title lines
		if flag == 2 :
			flag = 0
			counter += 1
			
			if counter == 1:
				lParser_output.append(dDict_temp)
			lKey_list = []
			lValue_list = []
			lLine = lLine.strip()
			lKey_list = ' '.join(lLine.split())
			lKey_list = lKey_list.split(" ")
			dDict_temp = { }
			continue
		if counter >= 2 :
			if re.search("major alarm|leg:isadmin>#|automation::|minor alarm|critical alarm",lLine):
				count_line += 1
			if  len(lLine) == 0:
				count_line += 1
		if flag == 1 and ":" not in lLine and count_line == 2 :
			if "No Matching Entries" in lLine:
				lValue_list = [" "] * len(lKey_list)
			else:
				lLine = lLine.strip()
				lValue_list = ' '.join(lLine.split())
				lValue_list = lValue_list.split(" ")
			
			dDict_temp1 = dict(itertools.izip(lKey_list,lValue_list))
			lParser_output.append(dDict_temp1)
			continue

		##### the next if loop is used to handle blocks with format -> TYPE : VALUE
		if flag == 1 and "--------" not in lLine and ":" in lLine:
			colon_count = 0
			lTemp_val = []
			sX_line = ' '.join(lLine.split())
			lX_line = sX_line.split(" ")
			for temp in lX_line:
				if re.search("^:",temp):
					colon_count += 1
							
			#### the following if loop is for cases with a single ':' or more than two ':' (like "src mac") 
			if colon_count == 1 or colon_count > 2:
					
				lX1_temp = lLine.partition(":")
				for temp1 in lX1_temp:
					temp1 = temp1.strip()
					lTemp_val.append(temp1)
				
				lKey_list.append(lTemp_val[0])
				lValue_list.append(lTemp_val[len(lTemp_val)-1])
			#### this is for cases with two ':' in the same line (ex: key1 : value1       key2: value2)
			if colon_count == 2:
				
				if len(lX_line) <= 5:
					lX1_temp = lLine.partition(":")
					for temp1 in x1_temp:
						temp1 = temp1.strip()
						lTemp_val.append(temp1)
					
					lKey_list.append(lTemp_val[0])
					lValue_list.append(lTemp_val[len(lTemp_val)-1])
				else:
								
					lX1_temp = lLine.partition(":")
					lX2_temp = lLine.rpartition(":")
					sTemp_firstkey = lX1_temp[0]
					sTemp_firstkey = sTemp_firstkey.strip()
					lKey_list.append(sTemp_firstkey)
					sTemp_lastvalue = lX2_temp[len(lX2_temp)-1]
					sTemp_lastvalue = sTemp_lastvalue.strip()
					sX3_temp = lX1_temp[2]
					lX3_temp = sX3_temp.strip()
					lX3_split = lX3_temp.split("  ") ### splitting with double space
					lastelement_x3_index = len(lX3_split)-1
					lLastelement_x3_check = lX3_split[len(lX3_split)-1].split(":")
					
					###### since 'x3_split' variable is operated on double space the next if & else loop is used to handle exceptions with single spacing 			
					if re.search("\w",lLastelement_x3_check[0]):
						del lX3_split[lastelement_x3_index]
						lX3_split.append(lLastelement_x3_check[0])
					else:
						del lX3_split[lastelement_x3_index]
						
					for tempval_x3split in lX3_split:
						if re.search("\w",tempval_x3split):
							tempval_x3split = tempval_x3split.strip()
							lTemp_val.append(tempval_x3split)
					
					lValue_list.append(lTemp_val[0])
					lValue_list.append(sTemp_lastvalue)
					
					lKey_list.append(lTemp_val[1])
			
			dDict_temp = dict(itertools.izip(lKey_list,lValue_list))
	
	return lParser_output


##################################################################################
"""
FUNCTION NAME : parse_cli_show_mpls_type06()

DESCRIPTION : To handle this format two additional supporting functions namely 
funct1 and funct2 are been added.

SAMPLE INPUT:


===============================================================================
Ethernet Interface
===============================================================================
Description        : 1/2.5 Gig Ethernet
Interface          : lt:1/1/4                   Oper Speed       : N/A
Link-level         : Ethernet                   Config Speed     : N/A
Admin State        : up                         Oper Duplex      : N/A
Oper State         : down                       Config Duplex    : N/A
Physical Link      : No                         MTU              : 2048
IfIndex            : 36077568                   Hold time up     : 0 seconds
Last State Change  : 01/01/1970 00:09:19        Hold time down   : 0 seconds
Last Cleared Time  : N/A                        

Configured Mode    : access                     Encap Type       : 802.1q
Dot1Q Ethertype    : 0x8100                     
Auto-negotiate     : N/A                        MDI/MDX          : unknown
Egress Rate        : Default                    
Egress Burst       : Default                    

Configured Address : 00:07:72:a4:11:d8
Hardware Address   : 00:07:72:a4:11:d8
Cfg Alarm          : 
Alarm Status       : 
Category           : residential                
LoopbackMode       : none                       LoopbackVlan     : 0
Remark             : disbled                    
State Change Count : 0                          

===============================================================================
Traffic Statistics
===============================================================================
                                                   Input                 Output
-------------------------------------------------------------------------------
Octets                                                 0                      0
Packets                                                0                      0
Errors                                                 0                      0

===============================================================================
Ethernet Statistics
===============================================================================

Broadcast Pckts  :                   0  Drop Events      :                   0
Multicast Pckts  :                   0  CRC/Align Errors :                   0
Undersize Pckts  :                   0  Fragments        :                   0
Oversize Pckts   :                   0  Jabbers          :                   0
Collisions       :                   0

Octets                         :                   0
Packets                        :                   0
Packets of 64 Octets           :                   0
Packets of 65 to 127 Octets    :                   0
Packets of 128 to 255 Octets   :                   0
Packets of 256 to 511 Octets   :                   0
Packets of 512 to 1023 Octets  :                   0
Packets of 1024 to 1518 Octets :                   0
Packets of 1519 or more Octets :                   0
===============================================================================

===============================================================================
Port Statistics
===============================================================================
                                                   Input                 Output
-------------------------------------------------------------------------------
Unicast Packets                                        0                      0
Multicast Packets                                      0                      0
Broadcast Packets                                      0                      0
Discards                                               0                      0
Unknown Proto Discards                                 0                       
===============================================================================

SAMPLE OUTPUT:

[{' Ethernet Statistics': [{'Broadcast Pckts': '0',
                            'CRC/Align Errors :': '0',
                            'Collisions': '0',
                            'Drop Events': '0',
                            'Fragments': '0',
                            'Jabbers': '0',
                            'Multicast Pckts': '0',
                            'Octets': '0',
                            'Oversize Pckts': '0',
                            'Packets': '0',
                            'Packets of 1024 to 1518 Octets': '0',
                            'Packets of 128 to 255 Octets': '0',
                            'Packets of 1519 or more Octets': '0',
                            'Packets of 256 to 511 Octets': '0',
                            'Packets of 512 to 1023 Octets': '0',
                            'Packets of 64 Octets': '0',
                            'Packets of 65 to 127 Octets': '0',
                            'Undersize Pckts': '0'}],
  'Ethernet Interface': [{'Admin State': 'up',
                          'Alarm Status': '',
                          'Auto-negotiate': 'N/A',
                          'Category': 'residential',
                          'Cfg Alarm': '',
                          'Config Duplex': 'N/A',
                          'Config Speed': 'N/A',
                          'Configured Address': '00:07:72:a4:11:d8',
                          'Configured Mode': 'access',
                          'Description': '1/2.5 Gig Ethernet',
                          'Dot1Q Ethertype': '0x8100',
                          'Egress Burst': 'Default',
                          'Egress Rate': 'Default',
                          'Encap Type': '802.1q',
                          'Hardware Address': '00:07:72:a4:11:d8',
                          'Hold time down': '0 seconds',
                          'Hold time up': '0 seconds',
                          'IfIndex': '36077568',
                          'Interface': 'lt:1/1/4',
                          'Last Cleared Time': 'N/A',
                          'Last State Change': '01/01/1970 00:09:19',
                          'Link-level': 'Ethernet',
                          'LoopbackMode': 'none',
                          'LoopbackVlan': '0',
                          'MDI/MDX': 'unknown',
                          'MTU': '2048',
                          'Oper Duplex': 'N/A',
                          'Oper Speed': 'N/A',
                          'Oper State': 'down',
                          'Physical Link': 'No',
                          'Remark': 'disbled',
                          'State Change Count': '0'}],
  'Port Statistics': [{'Input': 'Packets',
                       'Output': '0',
                       'Types': 'Unicast'},
                      {'Input': 'Packets',
                       'Output': '0',
                       'Types': 'Multicast'},
                      {'Input': 'Packets',
                       'Output': '0',
                       'Types': 'Broadcast'},
                      {'Input': '0', 'Output': '0', 'Types': 'Discards'},
                      {'Input': 'Proto',
                       'Output': 'Discards',
                       'Types': 'Unknown'}],
  'Traffic Statistics': [{'Input': '0', 'Output': '0', 'Types': 'Octets'},
                         {'Input': '0', 'Output': '0', 'Types': 'Packets'},
                         {'Input': '0', 'Output': '0', 'Types': 'Errors'},
                         {'Types': ''}]}]
					
OUTPUT FORMAT : list of dict of dicts

NOTE : In this case in "Port Statistics" one of the fields in title line is empty.
This field is considered as "Types"
"""
##################################################################################

def funct1(val):
		
	global lData_new1
	key_list = []
	value_list = []
	temp_val = []
	colon_count = 0
	i = 0
	flag = 0
	counter = 0
	data_newer = lData_new1
	parser_tempout = [ ]
	for line in data_newer:
		
		i +=1
		i1 = 0
		colon_count = 0
		temp_val = []
		
		if val in line:
			flag = 1
			
		if  "==========" in line and flag == 1:
			counter +=1
			
		if counter > 1 and "=============" in line :
			
			dict_temp = dict(itertools.izip(key_list,value_list))
			parser_tempout.append(dict_temp)
			return parser_tempout
			
		if counter == 1 and "==========" not in line:
			
			x_line = ' '.join(line.split())
			x_line = x_line.split(" ")
			for temp in x_line:
				if ":" in temp:
					colon_count += 1
#### the following if loop is for cases with a single ':' 
			if colon_count == 1:
				x1_temp = line.partition(":")
				for temp1 in x1_temp:
					temp1 = temp1.strip()
					temp_val.append(temp1)
				
				key_list.append(temp_val[0])
				value_list.append(temp_val[len(temp_val)-1])
#### this is for cases with two ':' in the same line (ex: key1 : value1       key2: value2) or or more than two ':' (like "src mac") 
			if colon_count >= 2:
				if len(x_line) <= 5:
					x1_temp = line.partition(":")
					for temp1 in x1_temp:
						temp1 = temp1.strip()
						temp_val.append(temp1)
					
					key_list.append(temp_val[0])
					value_list.append(temp_val[len(temp_val)-1])
				else:
					x1_temp = line.partition(":")
					x2_temp = line.rpartition(":")
					
					temp_firstkey = x1_temp[0]
					temp_firstkey = temp_firstkey.strip()
					key_list.append(temp_firstkey)
					temp_lastvalue = x2_temp[len(x2_temp)-1]
					temp_lastvalue = temp_lastvalue.strip()
					x3_temp = x1_temp[2]
					x3_temp = x3_temp.strip()
					x3_split = x3_temp.split("  ")
					lastelement_x3 = len(x3_split)-1
					del x3_split[lastelement_x3]
					
					for tempval_x3split in x3_split:
						
						if re.search("\w",tempval_x3split):
							
							i1 +=1
							tempval_x3split = tempval_x3split.strip()
							temp_val.append(tempval_x3split)
					if i1 > 1:			
						value_list.append(temp_val[0])
						value_list.append(temp_lastvalue)
				
						key_list.append(temp_val[1])


def funct2(val):
		
	global lData_new1
	flag = 0
	counter = 0
	temp_count = 0
	parser_output = [ ]
	for line in lData_new1:
		if val in line :
			flag = 1 
		
		if flag == 1 and "===================" in line:
			counter += 1
		if counter == 1 and "==============" not in line:
			if "----------" in line:
				temp_count = 1
			if temp_count == 0:
				line_split = ' '.join(line.split())
				line_split = line_split.split(' ')
				line_split.insert(0,'Types')
				title = line_split
			if temp_count == 1 and "------------" not in line:
				line_temp = ' '.join(line.split())
				line_temp = line_temp.split(' ')
				val_temp = [ ]
				dict_temp = { }
				for temp_value1 in line_temp:
					temp_value1 = temp_value1.strip()
					val_temp.append(temp_value1)
				dict_temp = dict(itertools.izip(title,val_temp))
				parser_output.append(dict_temp)
				
		if counter > 1:
			return parser_output


def parse_cli_show_mpls_type06():
	global lParser_output, sResponse, lData_new1
	lParser_output = [ ]
	parser_tempvalues = [ ]
	#file_name = open ("mpls","r")
	sData_new = sResponse.read()
	lData_new1 = sData_new.split("\n")
	subtitle_count = 0
			
	for lLine in lData_new1:
	#the first line will contain the show command hence use it to classify the subtitiles
		
		if 'detail' in lLine and subtitle_count == 0:
			subtitle_count += 1
			subtitles = ['Ethernet Interface', 'Traffic Statistics', ' Ethernet Statistics', 'Port Statistics'] 
				
		elif 'port' in lLine and 'detail' not in lLine:

			subtitle_count += 1
			subtitles = ['Ethernet Interface','Traffic Statistics','Port Statistics']
		
		
		if "MINOR: CLI" in lLine:
			return lLine   ##### used to handle exceptions   
			
		if 'Ethernet Interface' in lLine:
			listout1 = funct1('Ethernet Interface')
			parser_tempvalues.append(listout1)
		if 'Traffic Statistics' in lLine:
			listout2 = funct2('Traffic Statistics')
			parser_tempvalues.append(listout2)
		if  "Ethernet Statistics" in lLine:
			listout3 = funct1('Ethernet Statistics')
			parser_tempvalues.append(listout3)
			
		if 	"Port Statistics" in lLine:
			listout4 = funct2('Port Statistics')
			parser_tempvalues.append(listout4)
			
	dDict_temps = dict(itertools.izip(subtitles,parser_tempvalues))
	lParser_output.append(dDict_temps)
	#pprint.pprint(lParser_output)
	return lParser_output

#######################################################################################
"""

FUNCTION NAME : parse_cli_show_mpls_type07()

CLI COMMAND HANDLED : show qos-servicerouter service-egress-network <id> detail 


SAMPLE INPUT:

===============================================================================
QoS Service Egress Network
===============================================================================
Service Egress Network Policy (1)
-------------------------------------------------------------------------------
Policy-id        : 1                    Scope            : Template
Description      : Default Policy
-------------------------------------------------------------------------------

FC               : af                   
- LSP EXP Bit Mapping
Out-of-profile   : 2                    In-Profile       : 2


FC               : be                   
- LSP EXP Bit Mapping
Out-of-profile   : 0                    In-Profile       : 0


FC               : ef                   
- LSP EXP Bit Mapping
Out-of-profile   : 5                    In-Profile       : 5


FC               : h1                   
- LSP EXP Bit Mapping
Out-of-profile   : 6                    In-Profile       : 6


FC               : h2                   
- LSP EXP Bit Mapping
Out-of-profile   : 4                    In-Profile       : 4


FC               : l1                   
- LSP EXP Bit Mapping
Out-of-profile   : 3                    In-Profile       : 3


FC               : l2                   
- LSP EXP Bit Mapping
Out-of-profile   : 1                    In-Profile       : 1


FC               : nc                   
- LSP EXP Bit Mapping
Out-of-profile   : 7                    In-Profile       : 7
===============================================================================
 
SAMPLE OUTPUT:

[{'Description': 'Default Policy', 'Policy-id': '1', 'Scope': 'Template'},
 {'FC': 'af', 'In-Profile': '2', 'Out-of-profile': '2'},
 {'FC': 'be', 'In-Profile': '0', 'Out-of-profile': '0'},
 {'FC': 'ef', 'In-Profile': '5', 'Out-of-profile': '5'},
 {'FC': 'h1', 'In-Profile': '6', 'Out-of-profile': '6'},
 {'FC': 'h2', 'In-Profile': '4', 'Out-of-profile': '4'},
 {'FC': 'l1', 'In-Profile': '3', 'Out-of-profile': '3'},
 {'FC': 'l2', 'In-Profile': '1', 'Out-of-profile': '1'},
 {'FC': 'nc', 'In-Profile': '7', 'Out-of-profile': '7'}]

OUTPUT FORMAT : list of dicts
"""
#####################################################################################


def parse_cli_show_mpls_type07():
	global lParser_output, sResponse
	lParser_output = [ ]
	#file_name = open ("mpls07.txt","r")
	sData_new = sResponse.read()
	lData_new1 = sData_new.split("\n")
	counter = 0
	flag = 0
	lKey_list = []  #### this list contains the title values
	lValue_list = []  ####### this list contains value elements

	for lLine in lData_new1:
		if "==================" in lLine:
			counter +=1
			continue
		if counter == 3:
			dDict_temp = dict(itertools.izip(lKey_list,lValue_list))
			lParser_output.append(dDict_temp)
			return lParser_output
			
		if counter == 2:
			
			if len(lLine) == 0 and len(lKey_list) > 0:
				dDict_temp = dict(itertools.izip(lKey_list,lValue_list))
				lParser_output.append(dDict_temp)
				
				lKey_list = []
				lValue_list = []
				continue
			if ":" in lLine:
				
				colon_count = 0
				lTemp_val = []
				sX_line = ' '.join(lLine.split())
				lX_line = sX_line.split(" ")
				for temp in lX_line:
					if re.search("^:",temp):
						colon_count += 1
	### the following if loop is for cases with a single ':' or more than two ':' (like "src mac") 
				if colon_count == 1 or colon_count > 2:
					lX1_temp = lLine.partition(":")
					for temp1 in lX1_temp:
						temp1 = temp1.strip()
						lTemp_val.append(temp1)
					lKey_list.append(lTemp_val[0])
					lValue_list.append(lTemp_val[len(lTemp_val)-1])
					
				#### this is for cases with two ':' in the same line (ex: key1 : value1       key2: value2)
				if colon_count == 2:
					
					if len(lX_line) <= 5:
						lX1_temp = lLine.partition(":")
						for temp1 in x1_temp:
							temp1 = temp1.strip()
							lTemp_val.append(temp1)
						
						lKey_list.append(lTemp_val[0])
						lValue_list.append(lTemp_val[len(lTemp_val)-1])
					else:
									
						lX1_temp = lLine.partition(":")
						lX2_temp = lLine.rpartition(":")
						sTemp_firstkey = lX1_temp[0]
						sTemp_firstkey = sTemp_firstkey.strip()
						lKey_list.append(sTemp_firstkey)
						sTemp_lastvalue = lX2_temp[len(lX2_temp)-1]
						sTemp_lastvalue = sTemp_lastvalue.strip()
						sX3_temp = lX1_temp[2]
						lX3_temp = sX3_temp.strip()
						lX3_split = lX3_temp.split("  ") ### splitting with double space
						lastelement_x3_index = len(lX3_split)-1
						lLastelement_x3_check = lX3_split[len(lX3_split)-1].split(":")
						
						###### since 'x3_split' variable is operated on double space the next if & else loop is used to handle exceptions with single spacing 			
						if re.search("\w",lLastelement_x3_check[0]):
							del lX3_split[lastelement_x3_index]
							lX3_split.append(lLastelement_x3_check[0])
						else:
							del lX3_split[lastelement_x3_index]
							
						for tempval_x3split in lX3_split:
							if re.search("\w",tempval_x3split):
								tempval_x3split = tempval_x3split.strip()
								lTemp_val.append(tempval_x3split)
						
						lValue_list.append(lTemp_val[0])
						lValue_list.append(sTemp_lastvalue)
						
						lKey_list.append(lTemp_val[1])


########################################################################################
"""

FUNCTION NAME : parse_cli_show_mpls_type09()

DESCRIPTION : This mpls parser is used when the a particular field in the value line
is been split by a sigle space. For example in the below mentioned sample input the 
value field which is ("<224.3.3.1 - 224.3.3.5>") actually a single unit is split by 
a single space. In such cases this parser can be used.

CLI COMMAND HANDLED : show router igmp ssm-translate Igmp_oif_ISAM_vvpls_<xxx>


INPUT FORMAT:
" show router igmp ssm-translate Igmp_oif_ISAM_vvpls_1250"
===============================================================================
 IGMP SSM Translate Entries
===============================================================================
Group Range                        Source           Interface                  
-------------------------------------------------------------------------------
<224.3.3.1 - 224.3.3.5>            21.11.41.51      Igmp_oif_ISAM_vvpls_1250   
-------------------------------------------------------------------------------
SSM Translate Entries : 1
===============================================================================

SAMPLE OUTPUT :
[{'Group Range': '<224.3.3.1 - 224.3.3.5>',
  'Interface': 'Igmp_oif_ISAM_vvpls_1250',
  'Source': '21.11.41.51'}]
 
OUTPUT FORMAT : list of dicts

"""
######################################################################################


def parse_cli_show_mpls_type09() :
	global lParser_output, sResponse
	lParser_output = [ ]

#	file_name = open ("detail.txt","r")
	sData_new = sResponse.read()
	lData_new1 = sData_new.split("\n")
	check_line = 0
	dash_line = 0
		
	for lLine in  lData_new1:
		if "================" in lLine:
			check_line += 1
			continue
		if "---------------" in lLine:
			dash_line += 1
			continue
		if dash_line > 1:
			return lParser_output
			
		if check_line == 2 and dash_line == 0:
			sLine = lLine.strip()
			sTitle_line = re.sub("  +","|",sLine)
			lTitle_line = sTitle_line.split("|")
		if dash_line == 1:
			sLine = lLine.strip()
			sVal_line = re.sub("  +","|",sLine)
			lVal_line = sVal_line.split("|")
			dDict_temp = { }
			dDict_temp = dict(itertools.izip(lTitle_line,lVal_line))
			lParser_output.append(dDict_temp)




"""
Name: Parse_cli_show_generic_detail


input:leg:isadmin># show equipment slot detail 
====================================================================================================================================
slot table (detailed)
====================================================================================================================================
 
------------------------------------------------------------------------------------------------------------------------------------
slot
------------------------------------------------------------------------------------------------------------------------------------
                  slot : nt-a                         planned-type : nant-d                        actual-type : nant-d            
           oper-status : enabled                      error-status : no-error                                                      
          availability : available                   alarm-profile : none                                                          
         capab-profile : not_applicable                                                
          manufacturer : ALCL
              mnemonic : NANT-D
              pba-code :                                                                                                               
             fpba-code : 3FE61555ABAA                                                                                              
              fpba-ics : 06
             clei-code : VAI2AFDDAA
             serial-no : AA1010ZA027
           failed-test : 00:00:00:00
       lt-restart-time : 1970-01-01:00:00:00                                                  lt-restart-cause : other             
        lt-restart-num : 0                   mgnt-entity-oamipaddr : 0.0.0.0               mgnt-entity-pairnum : 0                 
          dual-host-ip : 0.0.0.0                     dual-host-loc : none               
------------------------------------------------------------------------------------------------------------------------------------
slot
------------------------------------------------------------------------------------------------------------------------------------
                  slot : nt-b                         planned-type : not-planned                   actual-type : empty             
           oper-status : disabled                     error-status : no-error                                                      
          availability : not-installed               alarm-profile : none                                                          
         capab-profile : not_applicable                                                
          manufacturer :                                                                                                           
              mnemonic :                                                                                                           
              pba-code :                                                                                                           
             fpba-code :                                                                                                           
              fpba-ics :                                                                                                           
             clei-code :                                                                                                           
             serial-no :                                                                                                           
           failed-test : 00:00:00:00                                                                                               
       lt-restart-time : 1970-01-01:00:00:00                                                  lt-restart-cause : other             
        lt-restart-num : 0                   mgnt-entity-oamipaddr : 0.0.0.0               mgnt-entity-pairnum : 0                 
          dual-host-ip : 0.0.0.0                     dual-host-loc : none               
------------------------------------------------------------------------------------------------------------------------------------
slot
------------------------------------------------------------------------------------------------------------------------------------
                  slot : lt:1/1/1                     planned-type : not-planned                   actual-type : empty             
           oper-status : disabled                     error-status : no-error                                                      
          availability : not-installed               alarm-profile : none                                                          
         capab-profile : not_applicable                                                
          manufacturer :                                                                                                           
              mnemonic :                                                                                                           
              pba-code :                                                                                                           
             fpba-code :                                                                                                           
              fpba-ics :                                                                                                           
             clei-code :                                                                                                           
             serial-no :                                                                                                           
           failed-test : 00:00:00:00                                                                                               
       lt-restart-time : 1970-01-01:00:00:00                                                  lt-restart-cause : other             
        lt-restart-num : 0                   mgnt-entity-oamipaddr : 0.0.0.0               mgnt-entity-pairnum : 0                 
          dual-host-ip : 0.0.0.0                     dual-host-loc : none               
------------------------------------------------------------------------------------------------------------------------------------
slot
------------------------------------------------------------------------------------------------------------------------------------
                  slot : lt:1/1/2                     planned-type : not-planned                   actual-type : empty             
           oper-status : disabled                     error-status : no-error                                                      
          availability : not-installed               alarm-profile : none                                                          
         capab-profile : not_applicable                                                
          manufacturer :                                                                                                           
              mnemonic :                                                                                                           
              pba-code :                                                                                                           
             fpba-code :                                                                                                           
              fpba-ics :                                                                                                           
             clei-code :                                                                                                           
             serial-no :                                                                                                           
           failed-test : 00:00:00:00                                                                                               
       lt-restart-time : 1970-01-01:00:00:00                                                  lt-restart-cause : other             
        lt-restart-num : 0                   mgnt-entity-oamipaddr : 0.0.0.0               mgnt-entity-pairnum : 0                 
          dual-host-ip : 0.0.0.0                     dual-host-loc : none               
------------------------------------------------------------------------------------------------------------------------------------
slot
------------------------------------------------------------------------------------------------------------------------------------
                  slot : lt:1/1/3                     planned-type : not-planned                   actual-type : empty             
           oper-status : disabled                     error-status : no-error                                                      
          availability : not-installed               alarm-profile : none                                                          
         capab-profile : not_applicable                                                
          manufacturer :                                                                                                           
              mnemonic :                                                                                                           
              pba-code :                                                                                                           
             fpba-code :                                                                                                           
              fpba-ics :                                                                                                           
             clei-code :                                                                                                           
             serial-no :                                                                                                           
           failed-test : 00:00:00:00                                                                                               
       lt-restart-time : 1970-01-01:00:00:00                                                  lt-restart-cause : other             
        lt-restart-num : 0                   mgnt-entity-oamipaddr : 0.0.0.0               mgnt-entity-pairnum : 0                 
          dual-host-ip : 0.0.0.0                     dual-host-loc : none               
------------------------------------------------------------------------------------------------------------------------------------
slot
------------------------------------------------------------------------------------------------------------------------------------
                  slot : lt:1/1/4                     planned-type : not-planned                   actual-type : empty             
           oper-status : disabled                     error-status : no-error                                                      
          availability : not-installed               alarm-profile : none                                                          
         capab-profile : not_applicable                                                
          manufacturer :                                                                                                           
              mnemonic :                                                                                                           
              pba-code :                                                                                                           
             fpba-code :                                                                                                           
              fpba-ics :                                                                                                           
             clei-code :                                                                                                           
             serial-no :                                                                                                           
           failed-test : 00:00:00:00                                                                                               
       lt-restart-time : 1970-01-01:00:00:00                                                  lt-restart-cause : other             
        lt-restart-num : 0                   mgnt-entity-oamipaddr : 0.0.0.0               mgnt-entity-pairnum : 0                 
          dual-host-ip : 0.0.0.0                     dual-host-loc : none               
------------------------------------------------------------------------------------------------------------------------------------
slot
------------------------------------------------------------------------------------------------------------------------------------
                  slot : lt:1/1/5                     planned-type : nglt-a                        actual-type : nglt-a            
           oper-status : enabled                      error-status : no-error                                                      
          availability : available                   alarm-profile : none                                                          
         capab-profile : fttu_lt                                                       
          manufacturer : ALCL                                                                                                      
              mnemonic : NGLT-A                                                                                                    
              pba-code : 3FE64279AAAA                                                                                              
             fpba-code : 3FE64279AAAA                                                                                              
              fpba-ics : 02                                                                                                        
             clei-code : VAI2AFHDAA                                                                                                
             serial-no : YP105015E1E                                                                                               
           failed-test : 00:00:00:00                                                                                               
       lt-restart-time : 1970-03-31:02:32:49                                                  lt-restart-cause : warm_reset        
        lt-restart-num : 21                  mgnt-entity-oamipaddr : 0.0.0.0               mgnt-entity-pairnum : 0                 
          dual-host-ip : 0.0.0.0                     dual-host-loc : none               
------------------------------------------------------------------------------------------------------------------------------------
slot
------------------------------------------------------------------------------------------------------------------------------------
                  slot : lt:1/1/6                     planned-type : not-planned                   actual-type : empty             
           oper-status : disabled                     error-status : no-error                                                      
          availability : not-installed               alarm-profile : none                                                          
         capab-profile : not_applicable                                                
          manufacturer :                                                                                                           
              mnemonic :                                                                                                           
              pba-code :                                                                                                           
             fpba-code :                                                                                                           
              fpba-ics :                                                                                                           
             clei-code :                                                                                                           
             serial-no :                                                                                                           
           failed-test : 00:00:00:00                                                                                               
       lt-restart-time : 1970-01-01:00:00:00                                                  lt-restart-cause : other             
        lt-restart-num : 0                   mgnt-entity-oamipaddr : 0.0.0.0               mgnt-entity-pairnum : 0                 
          dual-host-ip : 0.0.0.0                     dual-host-loc : none               
------------------------------------------------------------------------------------------------------------------------------------
slot
------------------------------------------------------------------------------------------------------------------------------------
                  slot : lt:1/1/7                     planned-type : not-planned                   actual-type : empty             
           oper-status : disabled                     error-status : no-error                                                      
          availability : not-installed               alarm-profile : none                                                          
         capab-profile : not_applicable                                                
          manufacturer :                                                                                                           
              mnemonic :                                                                                                           
              pba-code :                                                                                                           
             fpba-code :                                                                                                           
              fpba-ics :                                                                                                           
             clei-code :                                                                                                           
             serial-no :                                                                                                           
           failed-test : 00:00:00:00                                                                                               
       lt-restart-time : 1970-01-01:00:00:00                                                  lt-restart-cause : other             
        lt-restart-num : 0                   mgnt-entity-oamipaddr : 0.0.0.0               mgnt-entity-pairnum : 0                 
          dual-host-ip : 0.0.0.0                     dual-host-loc : none               
------------------------------------------------------------------------------------------------------------------------------------
slot
------------------------------------------------------------------------------------------------------------------------------------
                  slot : lt:1/1/8                     planned-type : not-planned                   actual-type : empty             
           oper-status : disabled                     error-status : no-error                                                      
          availability : not-installed               alarm-profile : none                                                          
         capab-profile : not_applicable                                                
          manufacturer :                                                                                                           
              mnemonic :                                                                                                           
              pba-code :                                                                                                           
             fpba-code :                                                                                                           
              fpba-ics :                                                                                                           
             clei-code :                                                                                                           
             serial-no :                                                                                                           
           failed-test : 00:00:00:00                                                                                               
       lt-restart-time : 1970-01-01:00:00:00                                                  lt-restart-cause : other             
        lt-restart-num : 0                   mgnt-entity-oamipaddr : 0.0.0.0               mgnt-entity-pairnum : 0                 
          dual-host-ip : 0.0.0.0                     dual-host-loc : none               
------------------------------------------------------------------------------------------------------------------------------------
slot
------------------------------------------------------------------------------------------------------------------------------------
                  slot : lt:1/1/12                    planned-type : not-planned                   actual-type : empty             
           oper-status : disabled                     error-status : no-error                                                      
          availability : not-installed               alarm-profile : none                                                          
         capab-profile : not_applicable                                                
          manufacturer :                                                                                                           
              mnemonic :                                                                                                           
              pba-code :                                                                                                           
             fpba-code :                                                                                                           
              fpba-ics :                                                                                                           
             clei-code :                                                                                                           
             serial-no :                                                                                                           
           failed-test : 00:00:00:00                                                                                               
       lt-restart-time : 1970-01-01:00:00:00                                                  lt-restart-cause : other             
        lt-restart-num : 0                   mgnt-entity-oamipaddr : 0.0.0.0               mgnt-entity-pairnum : 0                 
          dual-host-ip : 0.0.0.0                     dual-host-loc : none               
------------------------------------------------------------------------------------------------------------------------------------
slot
------------------------------------------------------------------------------------------------------------------------------------
                  slot : lt:1/1/13                    planned-type : not-planned                   actual-type : empty             
           oper-status : disabled                     error-status : no-error                                                      
          availability : not-installed               alarm-profile : none                                                          
         capab-profile : not_applicable                                                
          manufacturer :                                                                                                           
              mnemonic :                                                                                                           
              pba-code :                                                                                                           
             fpba-code :                                                                                                           
              fpba-ics :                                                                                                           
             clei-code :                                                                                                           
             serial-no :                                                                                                           
           failed-test : 00:00:00:00                                                                                               
       lt-restart-time : 1970-01-01:00:00:00                                                  lt-restart-cause : other             
        lt-restart-num : 0                   mgnt-entity-oamipaddr : 0.0.0.0               mgnt-entity-pairnum : 0                 
          dual-host-ip : 0.0.0.0                     dual-host-loc : none               
------------------------------------------------------------------------------------------------------------------------------------
slot
------------------------------------------------------------------------------------------------------------------------------------
                  slot : lt:1/1/14                    planned-type : not-planned                   actual-type : empty             
           oper-status : disabled                     error-status : no-error                                                      
          availability : not-installed               alarm-profile : none                                                          
         capab-profile : not_applicable                                                
          manufacturer :                                                                                                           
              mnemonic :                                                                                                           
              pba-code :                                                                                                           
             fpba-code :                                                                                                           
              fpba-ics :                                                                                                           
             clei-code :                                                                                                           
             serial-no :                                                                                                           
           failed-test : 00:00:00:00                                                                                               
       lt-restart-time : 1970-01-01:00:00:00                                                  lt-restart-cause : other             
        lt-restart-num : 0                   mgnt-entity-oamipaddr : 0.0.0.0               mgnt-entity-pairnum : 0                 
          dual-host-ip : 0.0.0.0                     dual-host-loc : none               
------------------------------------------------------------------------------------------------------------------------------------
slot
------------------------------------------------------------------------------------------------------------------------------------
                  slot : lt:1/1/15                    planned-type : not-planned                   actual-type : empty             
           oper-status : disabled                     error-status : no-error                                                      
          availability : not-installed               alarm-profile : none                                                          
         capab-profile : not_applicable                                                
          manufacturer :                                                                                                           
              mnemonic :                                                                                                           
              pba-code :                                                                                                           
             fpba-code :                                                                                                           
              fpba-ics :                                                                                                           
             clei-code :                                                                                                           
             serial-no :                                                                                                           
           failed-test : 00:00:00:00                                                                                               
       lt-restart-time : 1970-01-01:00:00:00                                                  lt-restart-cause : other             
        lt-restart-num : 0                   mgnt-entity-oamipaddr : 0.0.0.0               mgnt-entity-pairnum : 0                 
          dual-host-ip : 0.0.0.0                     dual-host-loc : none               
------------------------------------------------------------------------------------------------------------------------------------
slot
------------------------------------------------------------------------------------------------------------------------------------
                  slot : lt:1/1/16                    planned-type : not-planned                   actual-type : empty             
           oper-status : disabled                     error-status : no-error                                                      
          availability : not-installed               alarm-profile : none                                                          
         capab-profile : not_applicable                                                
          manufacturer :                                                                                                           
              mnemonic :                                                                                                           
              pba-code :                                                                                                           
             fpba-code :                                                                                                           
              fpba-ics :                                                                                                           
             clei-code :                                                                                                           
             serial-no :                                                                                                           
           failed-test : 00:00:00:00                                                                                               
       lt-restart-time : 1970-01-01:00:00:00                                                  lt-restart-cause : other             
        lt-restart-num : 0                   mgnt-entity-oamipaddr : 0.0.0.0               mgnt-entity-pairnum : 0                 
          dual-host-ip : 0.0.0.0                     dual-host-loc : none               
------------------------------------------------------------------------------------------------------------------------------------
slot
------------------------------------------------------------------------------------------------------------------------------------
                  slot : lt:1/1/17                    planned-type : not-planned                   actual-type : empty             
           oper-status : disabled                     error-status : no-error                                                      
          availability : not-installed               alarm-profile : none                                                          
         capab-profile : not_applicable                                                
          manufacturer :                                                                                                           
              mnemonic :                                                                                                           
              pba-code :                                                                                                           
             fpba-code :                                                                                                           
              fpba-ics :                                                                                                           
             clei-code :                                                                                                           
             serial-no :                                                                                                           
           failed-test : 00:00:00:00                                                                                               
       lt-restart-time : 1970-01-01:00:00:00                                                  lt-restart-cause : other             
        lt-restart-num : 0                   mgnt-entity-oamipaddr : 0.0.0.0               mgnt-entity-pairnum : 0                 
          dual-host-ip : 0.0.0.0                     dual-host-loc : none               
------------------------------------------------------------------------------------------------------------------------------------
slot
------------------------------------------------------------------------------------------------------------------------------------
                  slot : lt:1/1/18                    planned-type : not-planned                   actual-type : empty             
           oper-status : disabled                     error-status : no-error                                                      
          availability : not-installed               alarm-profile : none                                                          
         capab-profile : not_applicable                                                
          manufacturer :                                                                                                           
              mnemonic :                                                                                                           
              pba-code :                                                                                                           
             fpba-code :                                                                                                           
              fpba-ics :                                                                                                           
             clei-code :                                                                                                           
             serial-no :                                                                                                           
           failed-test : 00:00:00:00                                                                                               
       lt-restart-time : 1970-01-01:00:00:00                                                  lt-restart-cause : other             
        lt-restart-num : 0                   mgnt-entity-oamipaddr : 0.0.0.0               mgnt-entity-pairnum : 0                 
          dual-host-ip : 0.0.0.0                     dual-host-loc : none               
------------------------------------------------------------------------------------------------------------------------------------
slot
------------------------------------------------------------------------------------------------------------------------------------
                  slot : lt:1/1/19                    planned-type : not-planned                   actual-type : empty             
           oper-status : disabled                     error-status : no-error                                                      
          availability : not-installed               alarm-profile : none                                                          
         capab-profile : not_applicable                                                
          manufacturer :                                                                                               
              mnemonic :                                                                                                  
              pba-code :                                                                                             
             fpba-code :                                                      
              fpba-ics :                                               
             clei-code :                                          
             serial-no :                                                                                                    
           failed-test : 00:00:00:00                                                                                      
       lt-restart-time : 1970-01-01:00:00:00                                                  lt-restart-cause : other             
        lt-restart-num : 0                   mgnt-entity-oamipaddr : 0.0.0.0               mgnt-entity-pairnum : 0                 
          dual-host-ip : 0.0.0.0                     dual-host-loc : none               
====================================================================================================================================


output_sample :[{'lt:1/1/1': [{'actual-type': 'empty',
                'alarm-profile': 'none',
                'availability': 'not-installed',
                'capab-profile': 'not_applicable',
                'clei-code': 'empty',
                'dual-host-ip': '0.0.0.0',
                'dual-host-loc': 'none',
                'error-status': 'no-error',
                'failed-test': '00:00:00:00',
                'fpba-code': 'empty',
                'fpba-ics': 'empty',
                'lt-restart-cause': 'other',
                'lt-restart-num': '0',
                'lt-restart-time': '1970-01-01:00:00:00',
                'manufacturer': 'empty',
                'mgnt-entity-oamipaddr': '0.0.0.0',
                'mgnt-entity-pairnum': '0',
                'mnemonic': 'empty',
                'oper-status': 'disabled',
                'pba-code': 'empty',
                'planned-type': 'not-planned',
                'serial-no': 'empty',
                'slot': 'lt:1/1/1'}],
  'lt:1/1/12': [{'actual-type': 'empty',
                 'alarm-profile': 'none',
                 'availability': 'not-installed',
                 'capab-profile': 'not_applicable',
                 'clei-code': 'empty',
                 'dual-host-ip': '0.0.0.0',
                 'dual-host-loc': 'none',
                 'error-status': 'no-error',
                 'failed-test': '00:00:00:00',
                 'fpba-code': 'empty',
                 'fpba-ics': 'empty',
                 'lt-restart-cause': 'other',
                 'lt-restart-num': '0',
                 'lt-restart-time': '1970-01-01:00:00:00',
                 'manufacturer': 'empty',
                 'mgnt-entity-oamipaddr': '0.0.0.0',
                 'mgnt-entity-pairnum': '0',
                 'mnemonic': 'empty',
                 'oper-status': 'disabled',
                 'pba-code': 'empty',
                 'planned-type': 'not-planned',
                 'serial-no': 'empty',
                 'slot': 'lt:1/1/12'}],
  'lt:1/1/13': [{'actual-type': 'empty',
                 'alarm-profile': 'none',
                 'availability': 'not-installed',
                 'capab-profile': 'not_applicable',
                 'clei-code': 'empty',
                 'dual-host-ip': '0.0.0.0',
                 'dual-host-loc': 'none',
                 'error-status': 'no-error',
                 'failed-test': '00:00:00:00',
                 'fpba-code': 'empty',
                 'fpba-ics': 'empty',
                 'lt-restart-cause': 'other',
                 'lt-restart-num': '0',
                 'lt-restart-time': '1970-01-01:00:00:00',
                 'manufacturer': 'empty',
                 'mgnt-entity-oamipaddr': '0.0.0.0',
                 'mgnt-entity-pairnum': '0',
                 'mnemonic': 'empty',
                 'oper-status': 'disabled',
                 'pba-code': 'empty',
                 'planned-type': 'not-planned',
                 'serial-no': 'empty',
                 'slot': 'lt:1/1/13'}],
  'lt:1/1/14': [{'actual-type': 'empty',
                 'alarm-profile': 'none',
                 'availability': 'not-installed',
                 'capab-profile': 'not_applicable',
                 'clei-code': 'empty',
                 'dual-host-ip': '0.0.0.0',
                 'dual-host-loc': 'none',
                 'error-status': 'no-error',
                 'failed-test': '00:00:00:00',
                 'fpba-code': 'empty',
                 'fpba-ics': 'empty',
                 'lt-restart-cause': 'other',
                 'lt-restart-num': '0',
                 'lt-restart-time': '1970-01-01:00:00:00',
                 'manufacturer': 'empty',
                 'mgnt-entity-oamipaddr': '0.0.0.0',
                 'mgnt-entity-pairnum': '0',
                 'mnemonic': 'empty',
                 'oper-status': 'disabled',
                 'pba-code': 'empty',
                 'planned-type': 'not-planned',
                 'serial-no': 'empty',
                 'slot': 'lt:1/1/14'}],
  'lt:1/1/15': [{'actual-type': 'empty',
                 'alarm-profile': 'none',
                 'availability': 'not-installed',
                 'capab-profile': 'not_applicable',
                 'clei-code': 'empty',
                 'dual-host-ip': '0.0.0.0',
                 'dual-host-loc': 'none',
                 'error-status': 'no-error',
                 'failed-test': '00:00:00:00',
                 'fpba-code': 'empty',
                 'fpba-ics': 'empty',
                 'lt-restart-cause': 'other',
                 'lt-restart-num': '0',
                 'lt-restart-time': '1970-01-01:00:00:00',
                 'manufacturer': 'empty',
                 'mgnt-entity-oamipaddr': '0.0.0.0',
                 'mgnt-entity-pairnum': '0',
                 'mnemonic': 'empty',
                 'oper-status': 'disabled',
                 'pba-code': 'empty',
                 'planned-type': 'not-planned',
                 'serial-no': 'empty',
                 'slot': 'lt:1/1/15'}],
  'lt:1/1/16': [{'actual-type': 'empty',
                 'alarm-profile': 'none',
                 'availability': 'not-installed',
                 'capab-profile': 'not_applicable',
                 'clei-code': 'empty',
                 'dual-host-ip': '0.0.0.0',
                 'dual-host-loc': 'none',
                 'error-status': 'no-error',
                 'failed-test': '00:00:00:00',
                 'fpba-code': 'empty',
                 'fpba-ics': 'empty',
                 'lt-restart-cause': 'other',
                 'lt-restart-num': '0',
                 'lt-restart-time': '1970-01-01:00:00:00',
                 'manufacturer': 'empty',
                 'mgnt-entity-oamipaddr': '0.0.0.0',
                 'mgnt-entity-pairnum': '0',
                 'mnemonic': 'empty',
                 'oper-status': 'disabled',
                 'pba-code': 'empty',
                 'planned-type': 'not-planned',
                 'serial-no': 'empty',
                 'slot': 'lt:1/1/16'}],
  'lt:1/1/17': [{'actual-type': 'empty',
                 'alarm-profile': 'none',
                 'availability': 'not-installed',
                 'capab-profile': 'not_applicable',
                 'clei-code': 'empty',
                 'dual-host-ip': '0.0.0.0',
                 'dual-host-loc': 'none',
                 'error-status': 'no-error',
                 'failed-test': '00:00:00:00',
                 'fpba-code': 'empty',
                 'fpba-ics': 'empty',
                 'lt-restart-cause': 'other',
                 'lt-restart-num': '0',
                 'lt-restart-time': '1970-01-01:00:00:00',
                 'manufacturer': 'empty',
                 'mgnt-entity-oamipaddr': '0.0.0.0',
                 'mgnt-entity-pairnum': '0',
                 'mnemonic': 'empty',
                 'oper-status': 'disabled',
                 'pba-code': 'empty',
                 'planned-type': 'not-planned',
                 'serial-no': 'empty',
                 'slot': 'lt:1/1/17'}],
  'lt:1/1/18': [{'actual-type': 'empty',
                 'alarm-profile': 'none',
                 'availability': 'not-installed',
                 'capab-profile': 'not_applicable',
                 'clei-code': 'empty',
                 'dual-host-ip': '0.0.0.0',
                 'dual-host-loc': 'none',
                 'error-status': 'no-error',
                 'failed-test': '00:00:00:00',
                 'fpba-code': 'empty',
                 'fpba-ics': 'empty',
                 'lt-restart-cause': 'other',
                 'lt-restart-num': '0',
                 'lt-restart-time': '1970-01-01:00:00:00',
                 'manufacturer': 'empty',
                 'mgnt-entity-oamipaddr': '0.0.0.0',
                 'mgnt-entity-pairnum': '0',
                 'mnemonic': 'empty',
                 'oper-status': 'disabled',
                 'pba-code': 'empty',
                 'planned-type': 'not-planned',
                 'serial-no': 'empty',
                 'slot': 'lt:1/1/18'}],
  'lt:1/1/19': [{'actual-type': 'empty',
                 'alarm-profile': 'none',
                 'availability': 'not-installed',
                 'capab-profile': 'not_applicable',
                 'clei-code': 'empty',
                 'dual-host-ip': '0.0.0.0',
                 'dual-host-loc': 'none',
                 'error-status': 'no-error',
                 'failed-test': '00:00:00:00',
                 'fpba-code': 'empty',
                 'fpba-ics': 'empty',
                 'lt-restart-cause': 'other',
                 'lt-restart-num': '0',
                 'lt-restart-time': '1970-01-01:00:00:00',
                 'manufacturer': 'empty',
                 'mgnt-entity-oamipaddr': '0.0.0.0',
                 'mgnt-entity-pairnum': '0',
                 'mnemonic': 'empty',
                 'oper-status': 'disabled',
                 'pba-code': 'empty',
                 'planned-type': 'not-planned',
                 'serial-no': 'empty',
                 'slot': 'lt:1/1/19'}],
  'lt:1/1/2': [{'actual-type': 'empty',
                'alarm-profile': 'none',
                'availability': 'not-installed',
                'capab-profile': 'not_applicable',
                'clei-code': 'empty',
                'dual-host-ip': '0.0.0.0',
                'dual-host-loc': 'none',
                'error-status': 'no-error',
                'failed-test': '00:00:00:00',
                'fpba-code': 'empty',
                'fpba-ics': 'empty',
                'lt-restart-cause': 'other',
                'lt-restart-num': '0',
                'lt-restart-time': '1970-01-01:00:00:00',
                'manufacturer': 'empty',
                'mgnt-entity-oamipaddr': '0.0.0.0',
                'mgnt-entity-pairnum': '0',
                'mnemonic': 'empty',
                'oper-status': 'disabled',
                'pba-code': 'empty',
                'planned-type': 'not-planned',
                'serial-no': 'empty',
                'slot': 'lt:1/1/2'}],
  'lt:1/1/3': [{'actual-type': 'empty',
                'alarm-profile': 'none',
                'availability': 'not-installed',
                'capab-profile': 'not_applicable',
                'clei-code': 'empty',
                'dual-host-ip': '0.0.0.0',
                'dual-host-loc': 'none',
                'error-status': 'no-error',
                'failed-test': '00:00:00:00',
                'fpba-code': 'empty',
                'fpba-ics': 'empty',
                'lt-restart-cause': 'other',
                'lt-restart-num': '0',
                'lt-restart-time': '1970-01-01:00:00:00',
                'manufacturer': 'empty',
                'mgnt-entity-oamipaddr': '0.0.0.0',
                'mgnt-entity-pairnum': '0',
                'mnemonic': 'empty',
                'oper-status': 'disabled',
                'pba-code': 'empty',
                'planned-type': 'not-planned',
                'serial-no': 'empty',
                'slot': 'lt:1/1/3'}],
  'lt:1/1/4': [{'actual-type': 'empty',
                'alarm-profile': 'none',
                'availability': 'not-installed',
                'capab-profile': 'not_applicable',
                'clei-code': 'empty',
                'dual-host-ip': '0.0.0.0',
                'dual-host-loc': 'none',
                'error-status': 'no-error',
                'failed-test': '00:00:00:00',
                'fpba-code': 'empty',
                'fpba-ics': 'empty',
                'lt-restart-cause': 'other',
                'lt-restart-num': '0',
                'lt-restart-time': '1970-01-01:00:00:00',
                'manufacturer': 'empty',
                'mgnt-entity-oamipaddr': '0.0.0.0',
                'mgnt-entity-pairnum': '0',
                'mnemonic': 'empty',
                'oper-status': 'disabled',
                'pba-code': 'empty',
                'planned-type': 'not-planned',
                'serial-no': 'empty',
                'slot': 'lt:1/1/4'}],
  'lt:1/1/5': [{'actual-type': 'nglt-a',
                'alarm-profile': 'none',
                'availability': 'available',
                'capab-profile': 'fttu_lt',
                'clei-code': 'VAI2AFHDAA',
                'dual-host-ip': '0.0.0.0',
                'dual-host-loc': 'none',
                'error-status': 'no-error',
                'failed-test': '00:00:00:00',
                'fpba-code': '3FE64279AAAA',
                'fpba-ics': '02',
                'lt-restart-cause': 'warm_reset',
                'lt-restart-num': '21',
                'lt-restart-time': '1970-03-31:02:32:49',
                'manufacturer': 'ALCL',
                'mgnt-entity-oamipaddr': '0.0.0.0',
                'mgnt-entity-pairnum': '0',
                'mnemonic': 'NGLT-A',
                'oper-status': 'enabled',
                'pba-code': '3FE64279AAAA',
                'planned-type': 'nglt-a',
                'serial-no': 'YP105015E1E',
                'slot': 'lt:1/1/5'}],
  'lt:1/1/6': [{'actual-type': 'empty',
                'alarm-profile': 'none',
                'availability': 'not-installed',
                'capab-profile': 'not_applicable',
                'clei-code': 'empty',
                'dual-host-ip': '0.0.0.0',
                'dual-host-loc': 'none',
                'error-status': 'no-error',
                'failed-test': '00:00:00:00',
                'fpba-code': 'empty',
                'fpba-ics': 'empty',
                'lt-restart-cause': 'other',
                'lt-restart-num': '0',
                'lt-restart-time': '1970-01-01:00:00:00',
                'manufacturer': 'empty',
                'mgnt-entity-oamipaddr': '0.0.0.0',
                'mgnt-entity-pairnum': '0',
                'mnemonic': 'empty',
                'oper-status': 'disabled',
                'pba-code': 'empty',
                'planned-type': 'not-planned',
                'serial-no': 'empty',
                'slot': 'lt:1/1/6'}],
  'lt:1/1/7': [{'actual-type': 'empty',
                'alarm-profile': 'none',
                'availability': 'not-installed',
                'capab-profile': 'not_applicable',
                'clei-code': 'empty',
                'dual-host-ip': '0.0.0.0',
                'dual-host-loc': 'none',
                'error-status': 'no-error',
                'failed-test': '00:00:00:00',
                'fpba-code': 'empty',
                'fpba-ics': 'empty',
                'lt-restart-cause': 'other',
                'lt-restart-num': '0',
                'lt-restart-time': '1970-01-01:00:00:00',
                'manufacturer': 'empty',
                'mgnt-entity-oamipaddr': '0.0.0.0',
                'mgnt-entity-pairnum': '0',
                'mnemonic': 'empty',
                'oper-status': 'disabled',
                'pba-code': 'empty',
                'planned-type': 'not-planned',
                'serial-no': 'empty',
                'slot': 'lt:1/1/7'}],
  'lt:1/1/8': [{'actual-type': 'empty',
                'alarm-profile': 'none',
                'availability': 'not-installed',
                'capab-profile': 'not_applicable',
                'clei-code': 'empty',
                'dual-host-ip': '0.0.0.0',
                'dual-host-loc': 'none',
                'error-status': 'no-error',
                'failed-test': '00:00:00:00',
                'fpba-code': 'empty',
                'fpba-ics': 'empty',
                'lt-restart-cause': 'other',
                'lt-restart-num': '0',
                'lt-restart-time': '1970-01-01:00:00:00',
                'manufacturer': 'empty',
                'mgnt-entity-oamipaddr': '0.0.0.0',
                'mgnt-entity-pairnum': '0',
                'mnemonic': 'empty',
                'oper-status': 'disabled',
                'pba-code': 'empty',
                'planned-type': 'not-planned',
                'serial-no': 'empty',
                'slot': 'lt:1/1/8'}],
  'nt-a': [{'actual-type': 'nant-d',
            'alarm-profile': 'none',
            'availability': 'available',
            'capab-profile': 'not_applicable',
            'clei-code': 'VAI2AFDDAA',
            'dual-host-ip': '0.0.0.0',
            'dual-host-loc': 'none',
            'error-status': 'no-error',
            'failed-test': '00:00:00:00',
            'fpba-code': '3FE61555ABAA',
            'fpba-ics': '06',
            'lt-restart-cause': 'other',
            'lt-restart-num': '0',
            'lt-restart-time': '1970-01-01:00:00:00',
            'manufacturer': 'ALCL',
            'mgnt-entity-oamipaddr': '0.0.0.0',
            'mgnt-entity-pairnum': '0',
            'mnemonic': 'NANT-D',
            'oper-status': 'enabled',
            'pba-code': 'empty',
            'planned-type': 'nant-d',
            'serial-no': 'AA1010ZA027',
            'slot': 'nt-a'}],
  'nt-b': [{'actual-type': 'empty',
            'alarm-profile': 'none',
            'availability': 'not-installed',
            'capab-profile': 'not_applicable',
            'clei-code': 'empty',
            'dual-host-ip': '0.0.0.0',
            'dual-host-loc': 'none',
            'error-status': 'no-error',
            'failed-test': '00:00:00:00',
            'fpba-code': 'empty',
            'fpba-ics': 'empty',
            'lt-restart-cause': 'other',
            'lt-restart-num': '0',
            'lt-restart-time': '1970-01-01:00:00:00',
            'manufacturer': 'empty',
            'mgnt-entity-oamipaddr': '0.0.0.0',
            'mgnt-entity-pairnum': '0',
            'mnemonic': 'empty',
            'oper-status': 'disabled',
            'pba-code': 'empty',
            'planned-type': 'not-planned',
            'serial-no': 'empty',
            'slot': 'nt-b'}]}]

Comments: This parser is used to parse "detail show" command format.In this case key1 value is usually choosen from the index 1 value for instance in the above example " slot : nt-a" will occupy the list with index 1 and this is considered to be unique for detail command cases. Hence the value say "nt-a " is choosen as key1.		
history:
	created by: manoj Kumar
"""


def parse_cli_show_generic_detail():
	global lParser_output,sResponse
	
	#sData = sResponse.read()  
	sData = sResponse
	lData = sData.split('\n')
	flag =0
	dash_flag = 0
	count =0
	lList2 =[]
	lList_value =[]
	dDict1 = { }
	lList = []
	lTemp_list =[]
	lTemp_list1 =[]
	lParser_output = []
	
	for lLine in lData:

		if "==================" in lLine:
			flag +=1
			continue
		if "-------------" in lLine:
			dash_flag +=1
			continue
		
		if dash_flag ==2:
			lLine = lLine.split('   ')
			lLine = filter(None,lLine)
			while ' ' in lLine:
				lLine.remove(' ')
			while '  'in lLine:
				lLine.remove('  ')
			
			for temp in range(len(lLine)):
				
				if temp%2==0:
					
					if ":" in lLine[temp]:
						line1 = lLine[temp].split(' : ')
						if len(line1)==1:
							l=[x.rstrip(' :') for x in line1]
							lList2 +=l
							#when value will empty
							lList_value.append('empty')
						else:    
							for t in range(len(line1)):
							
								if t%2==0:
								
									lList2.append(line1[t])
								else:
									lList_value.append(line1[t])
				if temp%2==1:
					if ":" in lLine[temp]:
						line1 = lLine[temp].split(':')
						if len(line1)==1:
							l=[x.rstrip(' :') for x in line1]
							lList2 +=l
							lList_value.append('empty')
						else:
						   for x in range(len(line1)):
								if x%2==0:
									lList2.append(line1[x])
								
								else:
									lList_value.append(line1[x])
					else:  
						 lList_value.append(lLine[temp])
						 
						 
		if dash_flag is 3 or flag is 3:
			
			lList.append(lList_value[0])        
			lList2 = [x.rstrip(' :') for x in lList2]
			lList2 = [x.strip(' ') for x in lList2]
			lList_value = [x.strip(' ') for x in lList_value]
			dDict1= dict(itertools.izip(lList2,lList_value))
			print dDict1 
			lTemp_list.append(dDict1)
			lTemp_list1.append(temp_list)
			lTemp_list =[]       
			dash_flag = 1
			lList2 =[]
			lList_value =[]

	
	dDict = dict(itertools.izip(lList,temp_list1))
	lParser_output.append(dDict)
	return (lParser_output)    
       


"""
#######################################################################################################
FUNCTION NAME : parse_cli_show_generic_detail_type02

DESCRIPTION : This parser will handle the detail commands with out the xml cases 
			  and returns the output in the format of list of dict(s).

SAMPLE INPUT FORMAT :

leg:isadmin># show cfm ccm-database domain detail
==============================================================================================================================================================
domain table (detailed)
==============================================================================================================================================================

--------------------------------------------------------------------------------------------------------------------------------------------------------------
domain
--------------------------------------------------------------------------------------------------------------------------------------------------------------
          domain-index : 11                                    association : 11                                            mep : 11
            remote-mep : 12                                          state : rmep-start                         failed-ok-time : 000-23:39:28
              mac-addr : ff:ff:ff:ff:ff:ff                             rdi : rdi-not-set                                status : rmep-not-active
           port-status : ps-no-tlv                        interface-status : is-no-tlv                       chassis-id-format : not-applic
            chassis-id : 00
      mgmt-addr-domain :
             mgmt-addr : 00:00
--------------------------------------------------------------------------------------------------------------------------------------------------------------
domain
--------------------------------------------------------------------------------------------------------------------------------------------------------------
          domain-index : 11                                    association : 11                                            mep : 17
            remote-mep : 12                                          state : rmep-start                         failed-ok-time : 000-02:03:13
              mac-addr : ff:ff:ff:ff:ff:ff                             rdi : rdi-not-set                                status : rmep-not-active
           port-status : ps-no-tlv                        interface-status : is-no-tlv                       chassis-id-format : chassis-component
            chassis-id :
      mgmt-addr-domain :
             mgmt-addr :
--------------------------------------------------------------------------------------------------------------------------------------------------------------
domain
--------------------------------------------------------------------------------------------------------------------------------------------------------------
          domain-index : 12                                    association : 12                                            mep : 12
            remote-mep : 13                                          state : rmep-failed                        failed-ok-time : 005-05:01:00
              mac-addr : ff:ff:ff:ff:ff:ff                             rdi : rdi-not-set                                status : rmep-not-active
           port-status : ps-no-tlv                        interface-status : is-no-tlv                       chassis-id-format : not-applic
            chassis-id :
      mgmt-addr-domain :
             mgmt-addr :
==============================================================================================================================================================

SAMPLE OUTPUT :

[{'association': '11',
  'chassis-id': '00',
  'chassis-id-format': 'not-applic',
  'domain-index': '11',
  'failed-ok-time': '000-23:39:28',
  'interface-status': 'is-no-tlv',
  'mac-addr': 'ff:ff:ff:ff:ff:ff',
  'mep': '11',
  'mgmt-addr': '00:00',
  'mgmt-addr-domain': '',
  'port-status': 'ps-no-tlv',
  'rdi': 'rdi-not-set',
  'remote-mep': '12',
  'state': 'rmep-start',
  'status': 'rmep-not-active'},
 {'association': '11',
  'chassis-id': '',
  'chassis-id-format': 'chassis-component',
  'domain-index': '11',
  'failed-ok-time': '000-02:03:13',
  'interface-status': 'is-no-tlv',
  'mac-addr': 'ff:ff:ff:ff:ff:ff',
  'mep': '17',
  'mgmt-addr': '',
  'mgmt-addr-domain': '',
  'port-status': 'ps-no-tlv',
  'rdi': 'rdi-not-set',
  'remote-mep': '12',
  'state': 'rmep-start',
  'status': 'rmep-not-active'},
 {'association': '12',
  'chassis-id': '',
  'chassis-id-format': 'not-applic',
  'domain-index': '12',
  'failed-ok-time': '005-05:01:00',
  'interface-status': 'is-no-tlv',
  'mac-addr': 'ff:ff:ff:ff:ff:ff',
  'mep': '12',
  'mgmt-addr': '',
  'mgmt-addr-domain': '',
  'port-status': 'ps-no-tlv',
  'rdi': 'rdi-not-set',
  'remote-mep': '13',
  'state': 'rmep-failed',
  'status': 'rmep-not-active'}]

			
				
################################################################################	
"""	
		
def parse_cli_show_generic_detail_type02():
	global lData_new1,lParser_output,sResponse
	flag_doubleLine = 0
	flag_singleLine = 0
	lParser_output = []
	lKey_list = []
	lVal_list = []
	sData_new = copy.copy(sResponse) 
	lData_new1 = sData_new.strip().split("\n")	
	for lLine in lData_new1:
		if "========================" in lLine:
			flag_doubleLine += 1
			if flag_doubleLine == 3 :
				dDict_temp = dict(itertools.izip(lKey_list,lVal_list))
				lParser_output.append(dDict_temp)
				return lParser_output
			continue
		if "-------------------" in lLine:
			flag_singleLine += 1
			
			if flag_singleLine == 3:
				dDict_temp = dict(itertools.izip(lKey_list,lVal_list))
				lKey_list = []
				lVal_list = []
				lParser_output.append(dDict_temp)
				flag_singleLine = 1
			continue
		if flag_singleLine == 2:
			lLineSplit = lLine.strip().split("   ")
			for temp in lLineSplit:
				if re.search("\w",temp):
					lTempSplit = temp.strip().partition(" : ")
					lKey_list.append(lTempSplit[0].strip())
					lVal_list.append(lTempSplit[2].strip())
					
	
	
	
	
	


def main_parserFunct(args_cli,args_response):
	global lParser_output, sResponse, showcmd
	#arg_rem = re.search('(.*)\s\d(.*)',args_cli)
	#arg_rem = 0
	#if arg_rem:
	#	if arg_rem.group():
	#		if arg_rem.group(1):
	#			cli_cmd = arg_rem.group(1)
	#else:
	#	cli_cmd = args_cli 
	cli_cmd = args_cli
	showcmd = cli_cmd
	sResponse = args_response
	#print " DATA RECV IN PARSER",args_response

### the mapping function is been called and the returned value is a function name which is executed to return the parsed output
	parse_function = mapper.mapping_parserFunc(cli_cmd,sResponse)
	print "The mapped function is ",parse_function
	if parse_function is None:
		print "SUITABLE PARSER FUNCTION FOR THE GIVEN CLI COMMAND IN 'mapper.py' IS NOT AVAILABLE "
		#cli_command.show_command_failure_check(cmd=showcmd,reply=sResponse,errtype='MAPPER')
		return "FAIL"
	else:	
		try:
			lparse_out =  eval(parse_function)
			
			return lparse_out
		except Exception:
		        #cli_command.show_command_failure_check(cmd=showcmd,reply=sResponse,errtype='PARSE')
			print "FAILED TO PARSE DATA"
			return "FAIL"
