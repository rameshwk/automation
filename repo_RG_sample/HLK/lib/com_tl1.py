import os,sys,time,re
from robot.api import logger
try :
    import pexpect
except ImportError :
    repo_path = os.environ.get('ROBOTREPO')
    list = repo_path.split('/ATS/HGU/')	    
    pexpect_path = list[0]+'/TOOLS/pexpect-2.3_1'
    print pexpect_path
    sys.path.append(pexpect_path)
    import pexpect

class com_tl1(object):

    def __init__(self,ip="127.0.0.1",port="1022",\
    username="SUPERUSER",password="ANS#150"):
        self.ip=ip.encode("ascii")
        self.port=port.encode("ascii")
        self.username=username.encode("ascii")
        self.password=password.encode("ascii")
        self.interface=""
        self.session_alive = False
        if self.ip == "127.0.0.1" :
          raise AssertionError("%s-> please give correct ip" %  (__name__))

    def _init_tl1_ssh(self):
        """
          intialize a TL1 session through SSH 
        """
        #pexpect.TIMEOUT =180
        ssh_command = "ssh -l "+self.username+" -p "+self.port+" "+self.ip
        
        logger.info("TL1 INIT >> %s" % ssh_command)
        try :
            self.interface = pexpect.spawn(ssh_command)
        except Exception as inst :
            raise AssertionError("%s-> fail to exec 'pexpect.spawn(%s)', exception: %s"\
            % (__name__,ssh_command, inst) )
        try:    	
            # pexpect had a bug: 
            # if buffer is empty and the regular expression is used in re.compile
            # search buffer throw exception "internal error in regular expression engine"
            # the solution :
            # using string in re.compile
            expect_value = self.interface.expect(
                ['.*password:',\
                '.*Are you sure you want to continue connecting.*',\
                '.*Connection Refused!.*',\
                '.*Unable to connect.*',\
                '.*closed by remote host.*',\
                pexpect.EOF,pexpect.TIMEOUT])                         
        except Exception as inst :  
            logger.debug ("%s->pexpect can not deal with '.*' search, exception:%s" \
            % (__name__,inst))
            try:    	           
                expect_value = self.interface.expect(
                    ['password:',\
                    'Are you sure you want to continue connecting',\
                    'Connection Refused!',\
                    'Unable to connect',\
                    'closed by remote host',\
                    pexpect.EOF,pexpect.TIMEOUT])
            except Exception as inst :                     
                raise AssertionError ("%s-> fail to 'expect passowrd': %s" % 
                (__name__,inst) )          
    
        logger.debug("%s-> get return: %s, %s" \
        % (__name__,str(expect_value),self.interface.after))
        
        if int(expect_value) == 0 :
            self.interface.sendline(self.password) 
            logger.debug("%s-> send: '%s'" % (__name__,self.password))             
        elif int(expect_value) > 1 :
            self.session_alive = False
            self.interface.terminate() # to avoid gui pop up when close()
            self.interface.close()
            return "fail" 
        elif int(expect_value) == 1 :
            self.interface.sendline("yes")
            logger.debug("%s-> send: 'yes'" % (__name__))
            try :
                expect_value = self.interface.expect(
                ['.*password:',\
                pexpect.EOF,pexpect.TIMEOUT])
            except Exception as inst:
                raise AssertionError ("%s-> fail to 'expect passowrd': %s"
                %  (__name__,inst) )  
                             
            logger.debug("%s-> get return: %s, %s" \
            % (__name__,str(expect_value),self.interface.after))
            
            if int(expect_value) == 0 :
                self.interface.sendline(self.password) 
                logger.debug("%s-> send: '%s'" % (__name__,self.password))
            else :
                self.session_alive = False
                self.interface.terminate()
                self.interface.close()
                return "fail"                
            
        try :
            expect_value = self.interface.expect(\
            ['.*HELP MENU.*<DEL>.*input.*(\s)+<',\
            '.*;(\s)+>.*',\
            'Connection Refused!.*',\
            '.*DENY.*',\
            '.*Connection closed by foreign host.*',\
            pexpect.EOF, \
            pexpect.TIMEOUT],\
            timeout=30)
        except Exception as inst :
            raise AssertionError ("%s-> fail to exec 'expect', exception: %s" % 
            (__name__,inst) )  
            
        logger.debug("%s-> get return: %s,%s" \
        % (__name__,str(expect_value),self.interface.after))        
        if expect_value == 0 or expect_value == 1 :
            self.session_alive = True	              
            return "pass"
        else :
            self.session_alive = False
            logger.info("%s-> get return: %s" \
            % (__name__,self.interface.after))
            self.interface.close()
            return "fail" 	

    def open_tl1(self):
        keyword_name = 'open_tl1'
        iretry_count = 10        
        try :
            result = self._init_tl1_ssh()
        except Exception as inst :
            raise AssertionError ("%s:%s-> open tl1 failed, exception:%s" \
            % (__name__,keyword_name,inst))
        if self.session_alive :            
            return "pass"
            
        i = 0                   
        while (i <= iretry_count) :
            i += 1
            logger.debug("%s-> open tl1 for the %sth time, try again" \
            % (__name__,str(i)))
            self._init_tl1_ssh()           
            if self.session_alive :                
                return "pass"
            else:
                time.sleep(10)

  
    def send_command(self,command,timeout=0):  
        """ 
            Send the TL1 Command by SSH session 
        """

        keyword_name='send_command'
        if timeout != 0 :
            timeout = int(timeout)
        else:
            timeout = 60

        if not self.session_alive : 
            self.open_tl1()
        i = 0
        while (i < 3) :
            try :
                self.interface.sendline(command+"\n")
                logger.debug("%s:%s-> send command '%s'" \
                % (__name__,keyword_name, command))
            except Exception as inst :
                raise AssertionError ("%s:%s-> fail to send '%s', exception: %s" \
                % (__name__,keyword_name,command,inst))
                
            expect_value = self.interface.expect(['.*M  \d .*;\s+<','^< ',\
            '.*closed by remote host.*',\
            '.*Status, MIB Locked by Different Manager.*',\
            '.*Connection reset by peer.*',\
            pexpect.EOF,pexpect.TIMEOUT],timeout=timeout)
       
            if expect_value == 0 :
                logger.debug("%s-> get return: %s" % (__name__,self.interface.after))
                return self.interface.after
            elif expect_value > 0 :
                self.interface.close()
                time.sleep(30)
                self.session_alive = False
                try :                
                    retval = self.open_tl1()  
                except Exception as inst :
                    raise AssertionError ("%s:%s-> failed to open tl1, exception: %s" \
                    % (__name__,keyword_name,inst))                             
            i += 1

    def close_tl1(self):
        """
            logout the created tl1 session
        """

        if self.session_alive : 
            try :
                self.interface.sendline('LOGOFF;')
                logger.debug("%s-> send: '%s'" % (__name__,'LOGOFF;')) 
            except Exception as inst :
                raise AssertionError("%s-> failed to logoff tl1 , exception:%s" \
                % (__name__,inst))
                
        try :  
            self.session_alive = False
            self.interface.terminate() # to avoid gui pop up
            self.interface.close()
        except Exception as inst :
            raise AssertionError("%s-> failed to close tl1 session, exception:%s" \
            % (__name__,inst))            
         
        return "pass"

