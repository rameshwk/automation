import os,re,sys,time
import telnetlib
from robot.api import logger

try:	
    lib_path = os.environ['ROBOTREPO'] +'/TOOLS/lib/python'
    if lib_path not in sys.path:
        sys.path.append(lib_path)
    lib_com_cli =  os.environ['ROBOTREPO'] +'/LIBS/COM/COM_CLI'
    if lib_com_cli not in sys.path: 
        sys.path.append(lib_com_cli)
except Exception as inst:
    raise AssertionError("%s -> Fail to set sys.path,exception: %s" \
    % (__name__,inst))
    
class com_cli(object) :
    def __init__(self,ip='127.0.0.1',port='23',username='isadmin',\
    password='isamcli!',default_password='i$@mad-',prompt='isadmin>',timeout='3600s') :
        self.hostIp=ip.encode("ascii")
        self.cliPort=port.encode("ascii")
        self.cliUser=username.encode("ascii")
        self.cliPassword=password.encode("ascii")
        self.cliDefPassword=default_password.encode("ascii")
        self.cliPrompt=prompt.encode("ascii")
        self.cliTelnet=""
        self.cliTimeout=timeout.rstrip("s")

        keyword_name = "__init__"

        if self.hostIp == "127.0.0.1" :
            msg = "IP error! please given correct ip"
            raise AssertionError("%s:%s -> %s" \
            % (__name__,keyword_name,msg))        

    def _write_to_session(self,message,timeout=None) :
        logger.debug("write:"+message+"\n")
        self.cliTelnet.write(message)
        if not timeout :
           timeout = 2
        return self.cliTelnet.read_until("*",int(timeout)) 
                                        
    def _any_item_matched(self,item_list,target_string):
        for item in item_list :
            if item in target_string :
                return True
        else :
            return False
              
    def open_cli(self) :
        keyword_name = "open_cli"
        re_telnetTime = 1 
        iPasswordCount = 0
        non_recover_error = ["No such user","error",
        "Connection Refused!","Unable to connect to remote host",
        "Internal processing error","This terminal is locked for "]

        # if cli login failed in 1 hour, raise fatal error to stop robot run
        currentTime = time.mktime(time.localtime())
        endTime = currentTime + int(self.cliTimeout)
        while currentTime <= endTime :
            currentTime = time.mktime(time.localtime())
            try:
                re_telnetTime += 1                
                self.cliTelnet = telnetlib.Telnet(self.hostIp,self.cliPort,1)
            except Exception as inst:  
                logger.debug ("gotten exception for telnetlib.telnet : %s" % inst)
                logger.debug ("cli telnet failed, try it the %d(th) time"  % re_telnetTime)
                time.sleep(30)
                continue   
            logger.debug("telnet object is self.cliTelnet=%s" % self.cliTelnet)
            
            returnTmp = " "
            try :
                returnTmp = self.cliTelnet.read_until("*",3)
            except Exception as inst :
                logger.debug("first read failed for this telnet to %s \n exception:%s" % (self.hostIp,inst))
                if int(self.cliTimeout) < 30:
                    time.sleep(int(self.cliTimeout))
                else:
                    time.sleep(30)
                continue

            promptCount = 0
            while self.cliPrompt not in returnTmp and currentTime <= endTime : 
                currentTime = time.mktime(time.localtime())
                promptCount = promptCount + 1
                if promptCount > 30 :
                    self.cliTelnet.close()
                    break
                logger.debug("get return:%s\n" % returnTmp)                      
                if "<" in returnTmp or returnTmp.strip() == "" :
                    time.sleep(2)
                    returnTmp = self._write_to_session("\r\n",3)
                    continue
                elif "CLI(C) or a TL1 login(T)" in returnTmp: 
                    returnTmp = self._write_to_session("C\r\n",3)
                elif "login: " in returnTmp:
                    returnTmp = self._write_to_session(self.cliUser+"\r",2)           
                elif "enter new password:" in returnTmp :
                    returnTmp = self._write_to_session(self.cliPassword+"\r",2)
                elif "re-enter  password:" in returnTmp :
                    returnTmp = self._write_to_session(self.cliPassword+"\r",2)
                elif "password: " in returnTmp:
                    if iPasswordCount == 0 :
                        returnTmp = self._write_to_session(self.cliPassword+"\r",2) 
                        iPasswordCount += 1
                    elif iPasswordCount == 1 :
                        returnTmp = self._write_to_session(self.cliDefPassword+"\r\n",2) 
                        iPasswordCount += 1
                    else :
                        logger.debug("to write '%s', after got 3+ times \
                        of 'password' prompt" % self.cliPassword)
                        returnTmp = self._write_to_session(self.cliPassword+"\r\n",2)

                elif "Login incorrect" in returnTmp and "Connection closed" in returnTmp :
                    logger.debug("get return:%s\n" % returnTmp)  
                    self.cliTelnet.close()
                    time.sleep(1)
                    break            
                elif self._any_item_matched(non_recover_error,returnTmp) :
                    logger.debug("get return:%s\n" % returnTmp)  
                    self.cliTelnet.close()
                    raise AssertionError ("fatal to telnet CLI\n get error:%s" % returnTmp)
                else:
                    logger.debug("get return:%s\n" % returnTmp) 
                    logger.warn ("unexpected return gotten!!") 
                    self.cliTelnet.close()
                    time.sleep(1)
                    break

            logger.info("CLI REPLY << %s" % returnTmp)
            if self.cliPrompt in returnTmp :
                logger.debug("Telnet CLI success with last prompt: %s"  % returnTmp)
            else :
                self.cliTelnet.close()               
                raise AssertionError ("fail to telnet cli, not gotten expected prompt\n return:"+returnTmp) 
          
            #ALU1923183 - Set the terminal-timeout as '0' to avoid the terminal timeout 
            sessionRet = self._write_to_session("environment terminal-timeout timeout:0 \n",2) 
            logger.debug("gotten return '%s'" % sessionRet)
            return "pass" 

        if currentTime >= endTime :
            raise CliFatalError ("%s:%s-> fatal to telnet CLI in %d seconds" \
            % (__name__,keyword_name,int(self.cliTimeout)))          
                       
                       
    def close_cli(self):
        keyword_name = "close_cli"
        try:
            self.cliTelnet.write("logout\r\n")
            logger.info("CLI CMD >> logout")
            returnTmp = self.cliTelnet.read_until("*",0.1)  
            logger.info("CLI REPLY << "+returnTmp)
            time.sleep(1)
            self.cliTelnet.close()
        except Exception as inst:
            raise AssertionError("%s:%s -> fail to close cli session, exception: %s" \
            % (__name__,keyword_name,inst))
        else:
            return "pass"

    def send_command(self,cliCmd,timeout=0,prompt=None,expect_reboot=False):
        keyword_name = "send_command"
        command =cliCmd.encode("ascii")
        timeout = int(timeout)
        if not prompt :
            prompt = self.cliPrompt
        else :
            prompt = prompt.encode("ascii")

        # if user did not assign timeout, use the global CLITIMEOUT
        if timeout == 0:
            try:
                timeout = int(os.environ['CLITIMEOUT'])
            except:
                timeout = 60
	
        #Cleanup the buffer in try block
        returnTmp = ""
        try: 
            returnTmp = self.cliTelnet.read_until("*",0.0001) 
        except Exception as inst:
            logger.debug("%s:%s-> need re-open cli \n expection: %s" \
            % (__name__,keyword_name,inst))
            self.open_cli()

        logger.debug ("buffer info before send cli:\n %s" \
        % (returnTmp))
	
        # Write the command in the telnet session using try block
        try:
            wret = self.cliTelnet.write(command+"\r")  
            logger.debug("written '%s\\r'" % command)                         
        except Exception as inst:
            logger.debug("to write '%s\\r', gotten exception: %s" % (command,inst))            
            try :
                wret = self.cliTelnet.write(command+"\r")
                logger.debug("re-written '%s\\r'" % command)
            except Exception as inst:     
                raise AssertionError("to re-write '%s\\r', gotten exception:\n %s" \
                % (command,inst))

        if expect_reboot :
            try:
                returnTmp = self.cliTelnet.read_until(prompt,timeout)
            except Exception as inst:
                raise AssertionError("to read buffer, gotten exception: '%s'" % inst)
            try :
                return self._reboot_connect_back(command,prompt)
            except AssertionError as inst :
                raise AssertionError ("failed to reboot and re-connect cli \n\
                exception:%s" % inst)
            except CliFatalError as inst :
                raise CliFatalError ("fatal to reboot and re-connect cli \n\
                exception:%s" % inst)
            else :
                raise AssertionError ("unexpected error happen")                
 
        # Read buffer at least 5 times to capture the command and its output 
        alarmCnt = 0 
        readBuffer = ""
        chkcmd = re.sub('\s+','',command)
        for readBufferCnt in range (5) :
            try:             
                returnTmp = self.cliTelnet.read_until(prompt,timeout)
            except Exception as inst:
                raise AssertionError("to read buffer, gotten exception: '%s'" % inst) 
            readBuffer = readBuffer + returnTmp           
            if chkcmd in re.sub('\s+','',returnTmp) and re.search(prompt,returnTmp):                   
                break
            else :
                self.cliTelnet.write("\r") 
                logger.debug ("written \\r")  
                if "alarm occurred" in returnTmp :
                    alarmCnt= alarmCnt + 1
        else :
             logger.debug ("command did not exist in output: %s" % str(readBuffer))          
                
        if alarmCnt > 4 :        
            raise AssertionError("gotten more than 4 alarm occur" ) 		                
        if readBuffer == "" :
            raise AssertionError("send command '%s', gotten no output" \
            % command)
        if not re.search(prompt,readBuffer) :
            raise AssertionError("no expected prompt '%s' after send '%s' \ngotten '%s'" \
            % (prompt,command,readBuffer))

        return readBuffer


    def send_group_command(self,cliCmd,prompt_number=1,timeout=0):
    
        command =cliCmd.encode("ascii").rstrip('\r')
        timeout = int(timeout)
        prompt = ('.*' + self.cliPrompt) * int(prompt_number)

        timeout = (timeout or  60)
 
        #1, Cleanup the buffer in try block
        try:            
            returnTmp = self.cliTelnet.read_until("*",0.0001)
        except Exception as inst:
            logger.debug("need re-open cli \n exception: %s" % inst)
            try :
                open_cli()
            except CliFatalError as inst :
                raise CliFatalError("failed to re-open cli before send group cli\n \
                exception:%s"  % inst) 
        
        #2, send command      
        for i in range(3) :
            try:          
                self.cliTelnet.write(command+"\r")
            except Exception as inst:
                logger.debug("written %s\\r, gotten exception: %s" % (command,inst))  
                logger.debug("to re-open cli session for re-write")  
                self.open_cli()
                logger.debug("re-opened cli session")  
                continue
            else:
                break
        else :
            raise AssertionError("%s-> failed to send command '%s'" \
            % (__name__,command))
              
        #3, get response
        fullResponse = ""
        for i in range (3) :
            try :
                cmdResponse = self.cliTelnet.read_until(prompt,timeout)
            except Exception as inst:
                raise AssertionError("%s-> gotten exception when read buffer: %s" \
                % (__name__,inst))
            else :
                fullResponse = fullResponse + cmdResponse
                                
            if ("alarm occurred" in cmdResponse) or ("alarm cleared" in cmdResponse):
                self.cliTelnet.write("\r")
            else :
                break
        else :
            logger.warn("gotten more than 3 alarm info during executing '%s'" \
            % command)  
            
        return  fullResponse


    def _reboot_connect_back (self,command,prompt) :
        maxWaitTime = 900              
        returnTmp = ""
        currentTime = time.mktime(time.localtime())
        endTime = currentTime + maxWaitTime
        logger.debug("expect SUT reset by command '%s'" % command)
        while currentTime <= endTime :         
            time.sleep(5)
            currentTime = time.mktime(time.localtime())
            try:
                self.cliTelnet.write("\r\n")
                returnTmp = self.cliTelnet.read_until(prompt,5)
                logger.debug("expect SUT reset: write '\\r\\n', gotten '%s'" \
                % returnTmp)                 
            except Exception as inst:                                                        
                logger.debug ("gotten expected exception for reset\n '%s'" % inst)
                time.sleep(15)
                logger.debug ("to open cli session after reboot")
                self.open_cli()                      
                return returnTmp

        if currentTime >= endTime :
            raise AssertionError("SUT didn't reset in %s seconds by '%s'" \
                % (str(maxWaitTime),command))

	
class CliFatalError (RuntimeError) :
    ROBOT_EXIT_ON_FAILURE = True

   	  
