#!/bin/bash
export DISPLAY=:1
dutType=G-240W-A
echo "Current DUT is :"$dutType
logdir="/CodeCoverageTestLogs/"$dutType-$(date +%Y%m%d%H%M)/
echo $logdir
mkdir ${logdir}
# pybot -v image_path:/repo/yuzz/jenkins_repo/HD_RX_MS_PT/images/brcm/g240wa  \
# -V /root/workspace/args/CI-G240WA-ALCL.py \
# -i coadcoverage \
# -e igmpv2 \
# -e igmpv3 \
# --outputdir ${logdir} 
pybot -v image_path:/repo/yuzz/jenkins_repo/HD_RX_MS_PT/images/brcm/g240wa  \
-V $ROBOTWS/args/CI-G240WA-ALCL.py \
--exclude UNUSE \
--include COMMONORG240WAANDALCL \
-A $ROBOTREPO/ATS/HGU/TestBatch/IndependentTask/CODECOVERAGE.txt \
--outputdir ${logdir} \
$ROBOTREPO/ATS/HGU/CODECOVERAGE/



