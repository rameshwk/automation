*** Settings ***
Suite Setup
Suite Teardown
Force Tags        UNUSE
Library           %{RFPATH}/Python-2.7/lib/python2.7/site-packages/robot/libraries/String.py
Library           %{RFPATH}/Python-2.7/lib/python2.7/site-packages/robot/libraries/Collections.py
Resource          %{ROBOTREPO}/HLK/res/l3-selenium2.txt

*** Test Cases ***
01
    [Documentation]    not done
    ...    AONT should export cfg file to the local.
    ...
    ...    1. login to the ont.
    ...
    ...    2. export cfg file. should export file successfully.
    ...
    ...    check the cfg file size.
    ...
    ...    Author: li lin
    [Tags]
    l3-selenium2.LoginGUI    ${ont['gui_type']}    ${ont['url_protocol']}://${ont['ip']}    ${ont['gui_username']}    ${ont['gui_password']}
    sleep    2s
    l3-selenium2.ExportBackupAndRestore    ${ont['gui_type']}    ${ont['url_protocol']}://${ont['ip']}
    Comment    l3-selenium2.LogoutGUI    ${ont['gui_type']}    ${ont['url_protocol']}://${ont['ip']}
    Comment    ${DeviceInfo}    l3-selenium2.ExportBackupAndRestore    ${ont['gui_type']}    ${ont['url_protocol']}://${ont['ip']}

02
    [Documentation]    Should import config.cfg file successfully,and AONT should reboot.
    ...
    ...
    ...    Author: li lin
    [Tags]
    l3-selenium2.LoginGUI    ${ont['gui_type']}    ${ont['url_protocol']}://${ont['ip']}    ${ont['gui_username']}    ${ont['gui_password']}
    sleep    2s
    ${filepath}=    Set Variable    /root/Downloads/config.cfg
    l3-selenium2.importBackupAndRestore    ${ont['gui_type']}    ${ont['url_protocol']}://${ont['ip']}    ${filepath}
    l3-selenium2.LogoutGUI    ${ont['gui_type']}    ${ont['url_protocol']}://${ont['ip']}
    @{counter}=    evaluate    range(10)
    : FOR    ${a}    IN    @{counter}
    \    Run Process    ping    ${ont['ip']}    -c    60    alias=ping_proc
    \    ${ping_result}=    Get Process Result    ping_proc
    \    ${passed} =    Run Keyword And Return Status    Should Contain    ${ping_result.stdout}    60 received, 0% packet loss
    \    Exit For Loop If    ${passed}
    \    Sleep    10s
    Should Be True    ${passed}
    l3-selenium2.LoginGUI    ${ont['gui_type']}    ${ont['url_protocol']}://${ont['ip']}    ${ont['gui_username']}    ${ont['gui_password']}
    ${DeviceInfo}=    l3-selenium2.GetDeviceInfo    ${ont['gui_type']}    ${ont['url_protocol']}://${ont['ip']}
    l3-selenium2.LogoutGUI    ${ont['gui_type']}    ${ont['url_protocol']}://${ont['ip']}
    [Teardown]    Run Process    rm -f /root/Downloads config.cfg    shell=True

03
    [Documentation]    import the other type file, should be fail.
    ...
    ...    Author: li lin
    [Tags]
    [Timeout]
    ${result}=    Run Process    echo 'test wrong AONT config file' > /root/Downloads/test.cfg    shell=True
    sleep    5s
    Should Be Empty    ${result.stderr}
    l3-selenium2.LoginGUI    ${ont['gui_type']}    ${ont['url_protocol']}://${ont['ip']}    ${ont['gui_username']}    ${ont['gui_password']}
    sleep    2s
    ${filepath}=    Set Variable    /root/Downloads/test.cfg
    l3-selenium2.importBackupAndRestore    ${ont['gui_type']}    ${ont['url_protocol']}://${ont['ip']}    ${filepath}
    l3-selenium2.LogoutGUI    ${ont['gui_type']}    ${ont['url_protocol']}://${ont['ip']}
    [Teardown]    Run Process    rm -f /root/Downloads/config.cfg    shell=True

*** Keywords ***
