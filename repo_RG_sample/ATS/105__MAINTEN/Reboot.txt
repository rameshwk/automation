*** Settings ***
Suite Setup
Suite Teardown
Test Teardown
Default Tags      UNUSE
Library           %{RFPATH}/Python-2.7/lib/python2.7/site-packages/robot/libraries/String.py
Library           HttpLibrary.HTTP
Resource          %{ROBOTREPO}/HLK/res/l3-selenium2.txt

*** Test Cases ***
01
    [Documentation]    restart ONT by webgui
    ...
    ...    Author: li lin
    #reboot ont by web
    l3-selenium2.LoginGUI    ${ont['gui_type']}    ${ont['url_protocol']}://${ont['ip']}    ${ont['gui_username']}    ${ont['gui_password']}
    l3-selenium2.Reboot    ${ont['gui_type']}    ${ont['url_protocol']}://${ont['ip']}
    l3-selenium2.LogoutGUI    ${ont['gui_type']}    ${ont['url_protocol']}://${ont['ip']}
    #re-login web \ after reboot ont
    l3-selenium2.LoginGUI    ${ont['gui_type']}    ${ont['url_protocol']}://${ont['ip']}    ${ont['gui_username']}    ${ont['gui_password']}
    sleep    2s
    Run Keyword If    "${ont['opid']}"!='VFSP'    l1-Selenium2.PageShouldContain    Device Information
    Run Keyword If    "${ont['opid']}"=='VFSP'    l1-Selenium2.PageShouldContain    Status
    l3-selenium2.LogoutGUI    ${ont['gui_type']}    ${ont['url_protocol']}://${ont['ip']}
    [Teardown]

02
    [Documentation]    Adding portforwarding rule in the web gui, and after reboot ont, the rule should exist.
    ...
    ...    Author: li lin
    #change new passwd \ \ for web
    l3-selenium2.LoginGUI    ${ont['gui_type']}    ${ont['url_protocol']}://${ont['ip']}    ${ont['gui_username']}    ${ont['gui_password']}
    l3-selenium2.SetPortForwarding    ${ont['gui_type']}    ${ont['url_protocol']}://${ont['ip']}    Customer setting    20    30    40
    ...    50    TCP    INTERNET
    l3-selenium2.LogoutGUI    ${ont['gui_type']}    ${ont['url_protocol']}://${ont['ip']}
    #get the configuration after ont reboot
    Comment    l3-selenium2.LoginGUI    ${ont['gui_type']}    ${ont['url_protocol']}://${ont['ip']}    ${ont['gui_username']}    ${ont['gui_password']}
    Comment    ${forward_table}    l3-selenium2.GetPortForwarding    ${ont['gui_type']}    ${ont['url_protocol']}://${ont['ip']}
    Comment    l3-selenium2.LogoutGUI    ${ont['gui_type']}    ${ont['url_protocol']}://${ont['ip']}
    Comment    log    ${forward_table}
    Comment    ShouldNot Be Empty    ${forward_table}
    Comment    l3-selenium2.LoginGUI    ${ont['gui_type']}    ${ont['url_protocol']}://${ont['ip']}    ${ont['gui_username']}    ${ont['gui_password']}
    Comment    l3-selenium2.Reboot    ${ont['gui_type']}    ${ont['url_protocol']}://${ont['ip']}
    Comment    l3-selenium2.LogoutGUI    ${ont['gui_type']}    ${ont['url_protocol']}://${ont['ip']}
    Comment    l3-selenium2.LoginGUI    ${ont['gui_type']}    ${ont['url_protocol']}://${ont['ip']}    ${ont['gui_username']}    ${ont['gui_password']}
    Comment    ${forward_table}    l3-selenium2.GetPortForwarding    ${ont['gui_type']}    ${ont['url_protocol']}://${ont['ip']}
    Comment    l3-selenium2.LogoutGUI    ${ont['gui_type']}    ${ont['url_protocol']}://${ont['ip']}
    Comment    log    ${forward_table}
    Comment    ShouldNot Be Empty    ${forward_table}
    [Teardown]    #TestTeardown

03
    [Documentation]    Change dhcp pool, and reboot ont, the dhcp pool should be changed.
    ...
    ...    Author: li lin
    [Tags]    UNUSE
    #change new passwd \ \ for web
    l3-selenium2.LoginGUI    ${ont['gui_type']}    ${ont['url_protocol']}://${ont['ip']}    ${ont['gui_username']}    ${ont['gui_password']}
    l3-selenium2.SetPassword    ${ont['gui_type']}    ${ont['url_protocol']}://${ont['ip']}    ${newpasswd}    ${newpasswd}    change new passwd for the first time
    ${status}    ${msg}=    Run keyword And Ignore Error    Get Alert Message
    l3-selenium2.LogoutGUI    ${ont['gui_type']}    ${ont['url_protocol']}://${ont['ip']}
    #re-login web to verify the new passwd
    l3-selenium2.LoginGUI    ${ont['gui_type']}    ${ont['url_protocol']}://${ont['ip']}    ${ont['gui_username']}    ${newpasswd}
    sleep    1s
    l1-Selenium2.PageShouldContain    Device Information
    l3-selenium2.LogoutGUI    ${ont['gui_type']}    ${ont['url_protocol']}://${ont['ip']}
    [Teardown]    TestTeardown

04
    [Documentation]    Change NTP configuratoin, after reboot, should be changed.
    ...
    ...
    ...    Author: li lin
    [Tags]    UNUSE
    #change new passwd with the less length for web
    l3-selenium2.LoginGUI    ${ont['gui_type']}    ${ont['url_protocol']}://${ont['ip']}    ${ont['gui_username']}    ${ont['gui_password']}
    ${len}    Evaluate    ${min_leng}-1
    ${newpasswd}    Generate Random String    ${len}
    Set Suite Variable    ${newpasswd}
    l3-selenium2.SetPassword    ${ont['gui_type']}    ${ont['url_protocol']}://${ont['ip']}    ${newpasswd}    ${newpasswd}    change new passwd for the \ less min length
    ${status}    ${msg}=    Run keyword And Ignore Error    Get Alert Message
    l3-selenium2.LogoutGUI    ${ont['gui_type']}    ${ont['url_protocol']}://${ont['ip']}
    #should login ont web using the initial password
    l3-selenium2.LoginGUI    ${ont['gui_type']}    ${ont['url_protocol']}://${ont['ip']}    ${ont['gui_username']}    ${ont['gui_password']}
    sleep    1s
    l1-Selenium2.PageShouldContain    Device Information
    l3-selenium2.LogoutGUI    ${ont['gui_type']}    ${ont['url_protocol']}://${ont['ip']}
    [Teardown]

05
    [Documentation]    Change new passwd to exceeds max length.
    ...
    ...    The length of new password should be 5~24!
    ...
    ...    Author: li lin
    [Tags]    UNUSE
    #change new passwd with the less length for web
    l3-selenium2.LoginGUI    ${ont['gui_type']}    ${ont['url_protocol']}://${ont['ip']}    ${ont['gui_username']}    ${ont['gui_password']}
    ${len}    Evaluate    ${max_leng}+1
    ${newpasswd}    Generate Random String    ${len}
    Set Suite Variable    ${newpasswd}
    l3-selenium2.SetPassword    ${ont['gui_type']}    ${ont['url_protocol']}://${ont['ip']}    ${newpasswd}    ${newpasswd}    change new passwd to exceeds max length
    ${status}    ${msg}=    Run keyword And Ignore Error    Get Alert Message
    l3-selenium2.LogoutGUI    ${ont['gui_type']}    ${ont['url_protocol']}://${ont['ip']}
    #should login ont web using the initial password
    l3-selenium2.LoginGUI    ${ont['gui_type']}    ${ont['url_protocol']}://${ont['ip']}    ${ont['gui_username']}    ${ont['gui_password']}
    sleep    1s
    l1-Selenium2.PageShouldContain    Device Information
    l3-selenium2.LogoutGUI    ${ont['gui_type']}    ${ont['url_protocol']}://${ont['ip']}
    [Teardown]

*** Keywords ***
SuiteSetup
    Should Be Equal As Strings    ${startup_status}    PASS
    # Get the range of password's length for web
    ${sta}=    Run Keyword And Return Status    Should Match Regexp    ${ont['gui_type']}AND${ont['opid']}    I240WAANDKAZA|VIET
    l3-selenium2.LoginGUI    ${ont['gui_type']}    ${ont['url_protocol']}://${ont['ip']}    ${ont['gui_username']}    ${ont['gui_password']}
    ${newpasswd}    Generate Random String    1
    l3-selenium2.SetPassword    ${ont['gui_type']}    ${ont['url_protocol']}://${ont['ip']}    ${newpasswd}    ${newpasswd}    change new passwd for the first time
    ${status}    ${msg}=    Run keyword And Ignore Error    Get Alert Message
    l3-selenium2.LogoutGUI    ${ont['gui_type']}    ${ont['url_protocol']}://${ont['ip']}
    Run Keyword and Ignore Error    Pass Execution If    '${msg}'=='Your password has been changed successfully!'    reset password successfully.
    ${digital}    ${min_leng}    ${max_leng}    Should Match Regexp    ${msg}    (\\d+)\\W+(\\d+)
    Set Suite Variable    ${min_leng}
    Set Suite Variable    ${max_leng}

TestTeardown
    [Documentation]    Change password to the initial value.
    #change password back
    l3-selenium2.LoginGUI    ${ont['gui_type']}    ${ont['url_protocol']}://${ont['ip']}    ${ont['gui_username']}    ${newpasswd}
    sleep    2s
    l1-Selenium2.PageShouldContain    Device Information
    l3-selenium2.SetPassword    ${ont['gui_type']}    ${ont['url_protocol']}://${ont['ip']}    ${ont['gui_password']}    ${ont['gui_password']}    change password back    ${newpasswd}
    ${status}    ${msg}=    Run keyword And Ignore Error    Get Alert Message
    l3-selenium2.LogoutGUI    ${ont['gui_type']}    ${ont['url_protocol']}://${ont['ip']}
    # Delete port forward table
    l3-selenium2.LoginGUI    ${ont['gui_type']}    ${ont['url_protocol']}://${ont['ip']}    ${ont['gui_username']}    ${ont['gui_password']}
    l3-selenium2.DeletePortForwarding    ${ont['gui_type']}    ${ont['url_protocol']}://${ont['ip']}    1
    l3-selenium2.LogoutGUI    ${ont['gui_type']}    ${ont['url_protocol']}://${ont['ip']}
    Should Be Empty    ${forward_table}
    [Teardown]
