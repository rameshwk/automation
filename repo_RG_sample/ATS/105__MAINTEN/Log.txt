*** Settings ***
Suite Setup
Suite Teardown
Default Tags      COMMON    UNUSE
Library           %{RFPATH}/Python-2.7/lib/python2.7/site-packages/robot/libraries/String.py
Resource          %{ROBOTREPO}/HLK/res/l3-selenium2.txt

*** Variables ***

*** Test Cases ***
01
    [Documentation]    check manufacture from log.
    ...
    ...    Author: li lin
    [Tags]    UNUSE
    l3-selenium2.LoginGUI    ${ont['gui_type']}    ${ont['url_protocol']}://${ont['ip']}    ${ont['gui_username']}    ${ont['gui_password']}
    ${test}    l3-selenium2.GetLog    ${ont['gui_type']}    ${ont['url_protocol']}://${ont['ip']}    Emergency    Debug
    l3-selenium2.LogoutGUI    ${ont['gui_type']}    ${ont['url_protocol']}://${ont['ip']}
    ${vendor}=    Get From Dictionary    ${test}    logs
    Should Contain    ${vendor}=    Manufacturer:Nokia

02
    [Documentation]    check serial number from log.
    ...
    ...    Author: li lin
    #Get ont serial NO.
    l3-selenium2.LoginGUI    ${ont['gui_type']}    ${ont['url_protocol']}://${ont['ip']}    ${ont['gui_username']}    ${ont['gui_password']}
    ${DeviceInfo}    l3-selenium2.GetDeviceInfo    ${ont['gui_type']}    ${ont['url_protocol']}://${ont['ip']}
    l3-selenium2.LogoutGUI    ${ont['gui_type']}    ${ont['url_protocol']}://${ont['ip']}
    ${SerialName}=    Get From Dictionary    ${DeviceInfo}    serial_number
    #To compare with device information
    l3-selenium2.LoginGUI    ${ont['gui_type']}    ${ont['url_protocol']}://${ont['ip']}    ${ont['gui_username']}    ${ont['gui_password']}
    ${test}    l3-selenium2.GetLog    ${ont['gui_type']}    ${ont['url_protocol']}://${ont['ip']}    Alert    Informational
    l3-selenium2.LogoutGUI    ${ont['gui_type']}    ${ont['url_protocol']}://${ont['ip']}
    ${vendor}=    Get From Dictionary    ${test}    logs
    Should Contain    ${vendor}=    SerialNumber:${SerialName}

03
    [Documentation]    check productClass from log.
    ...
    ...    Author: li lin
    #Get Device name
    l3-selenium2.LoginGUI    ${ont['gui_type']}    ${ont['url_protocol']}://${ont['ip']}    ${ont['gui_username']}    ${ont['gui_password']}
    ${DeviceInfo}    l3-selenium2.GetDeviceInfo    ${ont['gui_type']}    ${ont['url_protocol']}://${ont['ip']}
    LOG    ${DeviceInfo}
    l3-selenium2.LogoutGUI    ${ont['gui_type']}    ${ont['url_protocol']}://${ont['ip']}
    ${DeviceName}=    Get From Dictionary    ${DeviceInfo}    device_name
    l3-selenium2.LoginGUI    ${ont['gui_type']}    ${ont['url_protocol']}://${ont['ip']}    ${ont['gui_username']}    ${ont['gui_password']}
    ${test}    l3-selenium2.GetLog    ${ont['gui_type']}    ${ont['url_protocol']}://${ont['ip']}    Critical    Notice
    l3-selenium2.LogoutGUI    ${ont['gui_type']}    ${ont['url_protocol']}://${ont['ip']}
    ${vendor}=    Get From Dictionary    ${test}    logs
    Should Contain    ${vendor}=    ProductClass:${DeviceName}

04
    [Documentation]    check ont'ip \ from log
    ...
    ...    Author: li lin
    l3-selenium2.LoginGUI    ${ont['gui_type']}    ${ont['url_protocol']}://${ont['ip']}    ${ont['gui_username']}    ${ont['gui_password']}
    ${test}    l3-selenium2.GetLog    ${ont['gui_type']}    ${ont['url_protocol']}://${ont['ip']}    Error    Warning
    l3-selenium2.LogoutGUI    ${ont['gui_type']}    ${ont['url_protocol']}://${ont['ip']}
    ${vendor}=    Get From Dictionary    ${test}    logs
    Should Contain    ${vendor}=    ${ont['ip']}

05
    [Documentation]    logs should still exist after reboot AONT.
    ...
    ...
    ...    Author: li lin
    [Tags]    UNUSE
    l3-selenium2.LoginGUI    ${ont['gui_type']}    ${ont['url_protocol']}://${ont['ip']}    ${ont['gui_username']}    ${ont['gui_password']}
    ${test}    l3-selenium2.GetLog    ${ont['gui_type']}    ${ont['url_protocol']}://${ont['ip']}    Alert    Debug
    log    ${test}
    l3-selenium2.LogoutGUI    ${ont['gui_type']}    ${ont['url_protocol']}://${ont['ip']}
    ${vendor}=    Get From Dictionary    ${test}    logs
    Should Contain    ${vendor}=    LocalDisplayLevel value change: new[Debug]
    Should Contain    ${vendor}=    LocalLogLevel value change: new[Alert]
    #reboot ont, and the log should not lost.
    l3-selenium2.LoginGUI    ${ont['gui_type']}    ${ont['url_protocol']}://${ont['ip']}    ${ont['gui_username']}    ${ont['gui_password']}
    ${test}    l3-selenium2.Reboot    ${ont['gui_type']}    ${ont['url_protocol']}://${ont['ip']}
    l3-selenium2.LogoutGUI    ${ont['gui_type']}    ${ont['url_protocol']}://${ont['ip']}
    sleep    150s
    l3-selenium2.LoginGUI    ${ont['gui_type']}    ${ont['url_protocol']}://${ont['ip']}    ${ont['gui_username']}    ${ont['gui_password']}
    ${test1}    l3-selenium2.GetLog    ${ont['gui_type']}    ${ont['url_protocol']}://${ont['ip']}    Alert    Debug
    log    ${test1}
    l3-selenium2.LogoutGUI    ${ont['gui_type']}    ${ont['url_protocol']}://${ont['ip']}
    ${vendor1}=    Get From Dictionary    ${test1}    logs
    Should Contain    ${vendor1}=    LocalDisplayLevel value change: new[Debug]
    Should Contain    ${vendor1}=    LocalLogLevel value change: new[Alert]

*** Keywords ***
