*** Settings ***
Documentation     Author: Zengle.Xu@nokia-sbell.com
Default Tags      COMMON
Library           %{RFPATH}/Python-2.7/lib/python2.7/site-packages/robot/libraries/String.py
Library           %{RFPATH}/Python-2.7/lib/python2.7/site-packages/robot/libraries/Process.py
Library           %{ROBOTREPO}/HLK/lib/hlk_generate_data_within_range.py
Resource          %{ROBOTREPO}/HLK/res/l3-selenium2.txt

*** Test Cases ***
01
    ${souce_maclist}    Generate Random Standard Mac Lists    30
    log    ${souce_maclist}
    l3-selenium2.LoginGUI    ${ont['gui_type']}    ${ont['url_protocol']}://${ont['ip']}    ${ont['gui_username']}    ${ont['gui_password']}
    sleep    5s
    L1-Selenium2.GoTo    ${ont['url_protocol']}://${ont['ip']}/#/qos
    Wait Until Element Is Visible    xpath=//span[@name="PAGE_QOS_TITLE"]    5s
    sleep    5s
    Wait Until Element Is Visible    xpath=//div[@id='quality-of-service']/..
    ${status}    L1-Selenium2.GetElementAttribute    xpath=//div[@id='quality-of-service']/following-sibling::div[1]/div@class
    log    ${status}
    Comment    Run Keyword If    '${status}'=='toggle btn btn-default off vfsp'    L1-Selenium2.ClickElement    xpath=//div[@id='quality-of-service']/following-sibling::div[1]/div/div/label[2]
    Comment    sleep    2s
    Comment    L1-Selenium2.ClickElement    xpath=//input[@id="applyButton"]
    Comment    sleep    5s
    Comment    ${status}    L1-Selenium2.GetElementAttribute    xpath=//div[@id='quality-of-service']/following-sibling::div[1]/div@class
    Comment    log    ${status}
    Comment    should be equal as strings    ${status}    toggle btn vfsp btn-success
    : FOR    ${source_mac}    IN    @{souce_maclist}
    \    log    ${source_mac}
    \    L1-Selenium2.ClickElement    xpath=//input[@class="button button-add"]
    \    sleep    2s
    \    ${name}    Generate Random String    8
    \    L1-Selenium2.InputText    xpath=//input[@name="QOS_NAME"]    ${name}
    \    L1-Selenium2.InputText    xpath=//div[@class='table l2criteria']/div/div[2]/input    ${source_mac}
    \    sleep    3s
    \    ${dest_mac}    Generate Random Standard Mac Within Range    aabbcc0011dd    cceeaabbccdd
    \    log    ${dest_mac}
    \    L1-Selenium2.InputText    xpath=//div[@class='table l2criteria']/div[4]/div[2]/input    ${dest_mac}
    \    ${dscp}    Evaluate    random.randint(0,63)    random
    \    L1-Selenium2.InputText    xpath=//input[@id="actionDSCP"]    ${dscp}
    \    L1-Selenium2.ClickElement    xpath=//input[@class='button button-apply']
    \    sleep    5s
    \    execute javascript    document.documentElement.scrollTop=10000
    \    sleep    3s
    \    Capture Page Screenshot
    Comment    l3-selenium2.LogoutGUI    ${ont['gui_type']}    ${ont['url_protocol']}://${ont['ip']}
