*** Settings ***
Suite Setup       Create Http Context    localhost:8015
Test Teardown     CloseClient
Default Tags      G240WB    G240WC    G240WZA    MXXT    MXXV    SAIB    VFSP
...               VFNB
Resource          %{ROBOTREPO}/HLK/res/l2-tr069.txt
Library           HttpLibrary.HTTP
Resource          %{ROBOTREPO}/HLK/res/l3-tr069.txt
Library           %{RFPATH}/Python-2.7/lib/python2.7/site-packages/robot/libraries/String.py
Library           %{RFPATH}/Python-2.7/lib/python2.7/site-packages/robot/libraries/Collections.py
Resource          %{ROBOTREPO}/HLK/res/l3-telnet.txt
Resource          %{ROBOTREPO}/HLK/res/l3-selenium2.txt
Resource          %{ROBOTREPO}/HLK/res/l2-pcta.txt

*** Test Cases ***
01
    [Documentation]    R GetParameterValues - Device.DNS.SupportedRecordTypes
    ...    ---
    ...    == Summary ==
    ...
    ...    *unsignedInt-[0:63]*
    ...
    ...    DiffServ codepoint to be used for the test packets. By
    ...    default the CPE SHOULD set this value to zero.
    ...
    ...
    ...    ---
    ...    *RCR:* _*ALU02328747*_
    ...
    ...    *IR:* _*ALU02328747*_
    ...
    ...    *Author:* Zixiao Zhao <zixiao.zhao@alcatel-sbell.com.cn>
    ...
    ...    *Last Reviser:* Zixiao Zhao
    ...
    ...    ---
    ...    == Implementaion Details ==
    ...    Please reference to : Nokia Lib
    ...    [
    ...    http//ct.web.alcatel-lucent.com/scm-lib4/view.cgi?number=3HH-14516-6001-DFZZA
    ...    | 3HH-14516-6001-DFZZA-Feature Readiness Checklists ALU02328747 ]
    ...    - [
    ...    http//ct.web.alcatel-lucent.com/scm-lib4/view.cgi?number=3HH-14314-600B-DFZZA
    ...    | 3HH-14516-6001-DFZZA-Feature Readiness Checklists ALU02328747 FDT1575
    ...    ]
    ...    | *TR-181 spec* | None |
    ...    | *Nokia's implementation* | None |
    ...    ---
    ...    == Customers ==
    ...
    ...    COMMON
    ...    ---
    ...    == History ==
    ...    _*0:*_ 2017.6.21 --- Zixiao Zhao <zixiao.zhao@alcatel-sbell.com.cn> Create this case.
    TriggerInform
    @{parameter_list}=    GetParameterValuesEx    Device.DNS.SupportedRecordTypes

02
    [Documentation]    R GetParameterValues - Device.DNS.Client.Enable
    ...    ---
    ...    == Summary ==
    ...
    ...    *unsignedInt-[0:63]*
    ...
    ...    DiffServ codepoint to be used for the test packets. By
    ...    default the CPE SHOULD set this value to zero.
    ...
    ...
    ...    ---
    ...    *RCR:* _*ALU02328747*_
    ...
    ...    *IR:* _*ALU02328747*_
    ...
    ...    *Author:* Zixiao Zhao <zixiao.zhao@alcatel-sbell.com.cn>
    ...
    ...    *Last Reviser:* Zixiao Zhao
    ...
    ...    ---
    ...    == Implementaion Details ==
    ...    Please reference to : Nokia Lib
    ...    [
    ...    http//ct.web.alcatel-lucent.com/scm-lib4/view.cgi?number=3HH-14516-6001-DFZZA
    ...    | 3HH-14516-6001-DFZZA-Feature Readiness Checklists ALU02328747 ]
    ...    - [
    ...    http//ct.web.alcatel-lucent.com/scm-lib4/view.cgi?number=3HH-14314-600B-DFZZA
    ...    | 3HH-14516-6001-DFZZA-Feature Readiness Checklists ALU02328747 FDT1575
    ...    ]
    ...    | *TR-181 spec* | None |
    ...    | *Nokia's implementation* | None |
    ...    ---
    ...    == Customers ==
    ...
    ...    COMMON
    ...    ---
    ...    == History ==
    ...    _*0:*_ 2017.8.16 --- Zixiao Zhao <@zixiao.zhao@nokia-sbell.com> Create this case.
    TriggerInform
    @{parameter_list}=    GetParameterValuesEx    Device.DNS.Client.Enable

03
    [Documentation]    R GetParameterValues - Device.DNS.Client.Status
    ...    ---
    ...    == Summary ==
    ...
    ...    *unsignedInt-[0:63]*
    ...
    ...    DiffServ codepoint to be used for the test packets. By
    ...    default the CPE SHOULD set this value to zero.
    ...
    ...
    ...    ---
    ...    *RCR:* _*ALU02328747*_
    ...
    ...    *IR:* _*ALU02328747*_
    ...
    ...    *Author:* Zixiao Zhao <zixiao.zhao@alcatel-sbell.com.cn>
    ...
    ...    *Last Reviser:* Zixiao Zhao
    ...
    ...    ---
    ...    == Implementaion Details ==
    ...    Please reference to : Nokia Lib
    ...    [
    ...    http//ct.web.alcatel-lucent.com/scm-lib4/view.cgi?number=3HH-14516-6001-DFZZA
    ...    | 3HH-14516-6001-DFZZA-Feature Readiness Checklists ALU02328747 ]
    ...    - [
    ...    http//ct.web.alcatel-lucent.com/scm-lib4/view.cgi?number=3HH-14314-600B-DFZZA
    ...    | 3HH-14516-6001-DFZZA-Feature Readiness Checklists ALU02328747 FDT1575
    ...    ]
    ...    | *TR-181 spec* | None |
    ...    | *Nokia's implementation* | None |
    ...    ---
    ...    == Customers ==
    ...
    ...    COMMON
    ...    ---
    ...    == History ==
    ...    _*0:*_ 2017.8.16 --- Zixiao Zhao <zixiao.zhao@nokia-sbell.com> Create this case.
    TriggerInform
    @{parameter_list}=    GetParameterValuesEx    Device.DNS.Client.Status

04
    [Documentation]    R GetParameterValues - Device.DNS.Client.ServerNumberOfEntries
    ...    ---
    ...    == Summary ==
    ...
    ...    *unsignedInt-[0:63]*
    ...
    ...    DiffServ codepoint to be used for the test packets. By
    ...    default the CPE SHOULD set this value to zero.
    ...
    ...
    ...    ---
    ...    *RCR:* _*ALU02328747*_
    ...
    ...    *IR:* _*ALU02328747*_
    ...
    ...    *Author:* Zixiao Zhao <zixiao.zhao@alcatel-sbell.com.cn>
    ...
    ...    *Last Reviser:* Zixiao Zhao
    ...
    ...    ---
    ...    == Implementaion Details ==
    ...    Please reference to : Nokia Lib
    ...    [
    ...    http//ct.web.alcatel-lucent.com/scm-lib4/view.cgi?number=3HH-14516-6001-DFZZA
    ...    | 3HH-14516-6001-DFZZA-Feature Readiness Checklists ALU02328747 ]
    ...    - [
    ...    http//ct.web.alcatel-lucent.com/scm-lib4/view.cgi?number=3HH-14314-600B-DFZZA
    ...    | 3HH-14516-6001-DFZZA-Feature Readiness Checklists ALU02328747 FDT1575
    ...    ]
    ...    | *TR-181 spec* | None |
    ...    | *Nokia's implementation* | None |
    ...    ---
    ...    == Customers ==
    ...
    ...    COMMON
    ...    ---
    ...    == History ==
    ...    _*0:*_ 2017.6.21 --- Zixiao Zhao <zixiao.zhao@alcatel-sbell.com.cn> Create this case.
    TriggerInform
    @{parameter_list}=    GetParameterValuesEx    Device.DNS.Client.ServerNumberOfEntries

05
    [Documentation]    RW GetSetParameterValues - Device.DNS.Client.Server.{i}.Enable
    ...    ---
    ...    == Summary ==
    ...
    ...    *boolean*
    ...
    ...    DiffServ codepoint to be used for the test packets. By
    ...    default the CPE SHOULD set this value to zero.
    ...
    ...
    ...    ---
    ...    *RCR:* _*ALU02328747*_
    ...
    ...    *IR:* _*ALU02328747*_
    ...
    ...    *Author:* Zixiao Zhao <zixiao.zhao@alcatel-sbell.com.cn>
    ...
    ...    *Last Reviser:* Zixiao Zhao
    ...
    ...    ---
    ...    == Implementaion Details ==
    ...    Please reference to : Nokia Lib
    ...    [
    ...    http//ct.web.alcatel-lucent.com/scm-lib4/view.cgi?number=3HH-14516-6001-DFZZA
    ...    | 3HH-14516-6001-DFZZA-Feature Readiness Checklists ALU02328747 ]
    ...    - [
    ...    http//ct.web.alcatel-lucent.com/scm-lib4/view.cgi?number=3HH-14314-600B-DFZZA
    ...    | 3HH-14516-6001-DFZZA-Feature Readiness Checklists ALU02328747 FDT1575
    ...    ]
    ...    | *TR-181 spec* | None |
    ...    | *Nokia's implementation* | None |
    ...    ---
    ...    == Customers ==
    ...
    ...    COMMON
    ...    ---
    ...    == History ==
    ...    _*0:*_ 2017.6.21 --- Zixiao Zhao <zixiao.zhao@alcatel-sbell.com.cn> Create this case.
    TriggerInform
    : FOR    ${i}    IN RANGE    1    5
    \    @{parameter_list}=    GetParameterValuesEx    Device.DNS.Client.Server.${i}.Enable
    \    ${r} =    Set Variable    0
    \    @{ret}=    SetParameterAndGetResponse    Device.DNS.Client.Server.${i}.Enable ${r}
    \    Run Keyword And Continue On Failure    Should Not Contain    ${ret[1]}    Fault
    \    ${r} =    Set Variable    1
    \    @{ret}=    SetParameterAndGetResponse    Device.DNS.Client.Server.${i}.Enable ${r}
    \    Run Keyword And Continue On Failure    Should Not Contain    ${ret[1]}    Fault

06
    [Documentation]    R GetParameterValues - Device.DNS.Client.Server.{i}.Status
    ...    ---
    ...    == Summary ==
    ...
    ...    *unsignedInt-[0:63]*
    ...
    ...    DiffServ codepoint to be used for the test packets. By
    ...    default the CPE SHOULD set this value to zero.
    ...
    ...
    ...    ---
    ...    *RCR:* _*ALU02328747*_
    ...
    ...    *IR:* _*ALU02328747*_
    ...
    ...    *Author:* Zixiao Zhao <zixiao.zhao@alcatel-sbell.com.cn>
    ...
    ...    *Last Reviser:* Zixiao Zhao
    ...
    ...    ---
    ...    == Implementaion Details ==
    ...    Please reference to : Nokia Lib
    ...    [
    ...    http//ct.web.alcatel-lucent.com/scm-lib4/view.cgi?number=3HH-14516-6001-DFZZA
    ...    | 3HH-14516-6001-DFZZA-Feature Readiness Checklists ALU02328747 ]
    ...    - [
    ...    http//ct.web.alcatel-lucent.com/scm-lib4/view.cgi?number=3HH-14314-600B-DFZZA
    ...    | 3HH-14516-6001-DFZZA-Feature Readiness Checklists ALU02328747 FDT1575
    ...    ]
    ...    | *TR-181 spec* | None |
    ...    | *Nokia's implementation* | None |
    ...    ---
    ...    == Customers ==
    ...
    ...    COMMON
    ...    ---
    ...    == History ==
    ...    _*0:*_ 2017.6.21 --- Zixiao Zhao <zixiao.zhao@alcatel-sbell.com.cn> Create this case.
    TriggerInform
    : FOR    ${i}    IN RANGE    1    2
    \    @{parameter_list}=    GetParameterValuesEx    Device.DNS.Client.Server.${i}.Status

07
    [Documentation]    R GetParameterValues - Device.DNS.Client.Server.{i}.Interface
    ...    ---
    ...    == Summary ==
    ...
    ...    *unsignedInt-[0:63]*
    ...
    ...    DiffServ codepoint to be used for the test packets. By
    ...    default the CPE SHOULD set this value to zero.
    ...
    ...
    ...    ---
    ...    *RCR:* _*ALU02328747*_
    ...
    ...    *IR:* _*ALU02328747*_
    ...
    ...    *Author:* Zixiao Zhao <zixiao.zhao@alcatel-sbell.com.cn>
    ...
    ...    *Last Reviser:* Zixiao Zhao
    ...
    ...    ---
    ...    == Implementaion Details ==
    ...    Please reference to : Nokia Lib
    ...    [
    ...    http//ct.web.alcatel-lucent.com/scm-lib4/view.cgi?number=3HH-14516-6001-DFZZA
    ...    | 3HH-14516-6001-DFZZA-Feature Readiness Checklists ALU02328747 ]
    ...    - [
    ...    http//ct.web.alcatel-lucent.com/scm-lib4/view.cgi?number=3HH-14314-600B-DFZZA
    ...    | 3HH-14516-6001-DFZZA-Feature Readiness Checklists ALU02328747 FDT1575
    ...    ]
    ...    | *TR-181 spec* | None |
    ...    | *Nokia's implementation* | None |
    ...    ---
    ...    == Customers ==
    ...
    ...    COMMON
    ...    ---
    ...    == History ==
    ...    _*0:*_ 2017.6.21 --- Zixiao Zhao <zixiao.zhao@alcatel-sbell.com.cn> Create this case.
    TriggerInform
    : FOR    ${i}    IN RANGE    1    2
    \    @{parameter_list}=    GetParameterValuesEx    Device.DNS.Client.Server.${i}.Interface

08
    [Documentation]    R GetParameterValues - Device.DNS.Client.Server.{i}.Type
    ...    ---
    ...    == Summary ==
    ...
    ...    *unsignedInt-[0:63]*
    ...
    ...    DiffServ codepoint to be used for the test packets. By
    ...    default the CPE SHOULD set this value to zero.
    ...
    ...
    ...    ---
    ...    *RCR:* _*ALU02328747*_
    ...
    ...    *IR:* _*ALU02328747*_
    ...
    ...    *Author:* Zixiao Zhao <zixiao.zhao@alcatel-sbell.com.cn>
    ...
    ...    *Last Reviser:* Zixiao Zhao
    ...
    ...    ---
    ...    == Implementaion Details ==
    ...    Please reference to : Nokia Lib
    ...    [
    ...    http//ct.web.alcatel-lucent.com/scm-lib4/view.cgi?number=3HH-14516-6001-DFZZA
    ...    | 3HH-14516-6001-DFZZA-Feature Readiness Checklists ALU02328747 ]
    ...    - [
    ...    http//ct.web.alcatel-lucent.com/scm-lib4/view.cgi?number=3HH-14314-600B-DFZZA
    ...    | 3HH-14516-6001-DFZZA-Feature Readiness Checklists ALU02328747 FDT1575
    ...    ]
    ...    | *TR-181 spec* | None |
    ...    | *Nokia's implementation* | None |
    ...    ---
    ...    == Customers ==
    ...
    ...    COMMON
    ...    ---
    ...    == History ==
    ...    _*0:*_ 2017.6.21 --- Zixiao Zhao <zixiao.zhao@alcatel-sbell.com.cn> Create this case.
    TriggerInform
    : FOR    ${i}    IN RANGE    1    2
    \    @{parameter_list}=    GetParameterValuesEx    Device.DNS.Client.Server.${i}.Type

09
    [Documentation]    R GetParameterValues - Device.DNS.Client.Server.{i}.DNSServer
    ...    ---
    ...    == Summary ==
    ...
    ...    *unsignedInt-[0:63]*
    ...
    ...    DiffServ codepoint to be used for the test packets. By
    ...    default the CPE SHOULD set this value to zero.
    ...
    ...
    ...    ---
    ...    *RCR:* _*ALU02328747*_
    ...
    ...    *IR:* _*ALU02328747*_
    ...
    ...    *Author:* Zixiao Zhao <zixiao.zhao@alcatel-sbell.com.cn>
    ...
    ...    *Last Reviser:* Zixiao Zhao
    ...
    ...    ---
    ...    == Implementaion Details ==
    ...    Please reference to : Nokia Lib
    ...    [
    ...    http//ct.web.alcatel-lucent.com/scm-lib4/view.cgi?number=3HH-14516-6001-DFZZA
    ...    | 3HH-14516-6001-DFZZA-Feature Readiness Checklists ALU02328747 ]
    ...    - [
    ...    http//ct.web.alcatel-lucent.com/scm-lib4/view.cgi?number=3HH-14314-600B-DFZZA
    ...    | 3HH-14516-6001-DFZZA-Feature Readiness Checklists ALU02328747 FDT1575
    ...    ]
    ...    | *TR-181 spec* | None |
    ...    | *Nokia's implementation* | None |
    ...    ---
    ...    == Customers ==
    ...
    ...    COMMON
    ...    ---
    ...    == History ==
    ...    _*0:*_ 2017.6.21 --- Zixiao Zhao <zixiao.zhao@alcatel-sbell.com.cn> Create this case.
    TriggerInform
    : FOR    ${i}    IN RANGE    1    2
    \    @{parameter_list}=    GetParameterValuesEx    Device.DNS.Client.Server.${i}.DNSServer

10
    [Documentation]    R GetParameterValues - Device.DNS.Diagnostics.NSLookupDiagnostics.Result.{i}.Status
    ...    ---
    ...    == Summary ==
    ...
    ...    *unsignedInt-[0:63]*
    ...
    ...    Not Showing In 168
    ...    [root@AONT: admin]# tr181 -F Device.DNS.
    ...    [DNS.]
    ...    [Client.]
    ...    [Server.1.]
    ...    [Server.2.]
    ...
    ...
    ...    Since HDR5701
    ...
    ...    ---
    ...    *RCR:* _*ALU02328747*_
    ...
    ...    *IR:* _*ALU02328747*_
    ...
    ...    *FR:* _*ALU02414398*_
    ...
    ...    *Author:* Zixiao Zhao <zixiao.zhao@alcatel-sbell.com.cn>
    ...
    ...    *Last Reviser:* Zixiao Zhao
    ...
    ...    ---
    ...    == Implementaion Details ==
    ...    Please reference to : Nokia Lib
    ...    [
    ...    http//ct.web.alcatel-lucent.com/scm-lib4/view.cgi?number=3HH-14516-6001-DFZZA
    ...    | 3HH-14516-6001-DFZZA-Feature Readiness Checklists ALU02328747 ]
    ...    - [
    ...    http//ct.web.alcatel-lucent.com/scm-lib4/view.cgi?number=3HH-14314-600B-DFZZA
    ...    | 3HH-14516-6001-DFZZA-Feature Readiness Checklists ALU02328747 FDT1575
    ...    ]
    ...    | *TR-181 spec* | None |
    ...    | *Nokia's implementation* | None |
    ...    ---
    ...    == Customers ==
    ...
    ...    COMMON
    ...    ---
    ...    == History ==
    ...    _*0:*_ 2017.6.21 --- Zixiao Zhao <zixiao.zhao@alcatel-sbell.com.cn> Created this case.
    ...
    ...    _*1:*_ 2017.11.215 --- Zixiao Zhao <zixiao.zhao@nokia-sbell.com> Modified this case.
    [Tags]    UNUSE
    TriggerInform
    : FOR    ${i}    IN RANGE    1    2
    \    @{parameter_list}=    GetParameterValuesEx    Device.DNS.Diagnostics.NSLookupDiagnostics.Result.${i}.Status

11
    [Documentation]    W Device.DNS.Client.Server.{i}.Alias
    ...
    ...    This supported from HDR5701
    ...
    ...    RCR ID:ALU02360334
    ...
    ...    *Author:* Yue Hong
    ...
    ...    *Last Reviser:* Yue Hong
    ...    ---
    ...    == Customer ==
    ...
    ...    G240WB,G240WC,G240WZA,MXXT,MXXV,SAIB
    ...    ---
    ...    == History ==
    ...    _*Rev 0:*_ December 7, 2017 --- Yue Hong \ <Hong.Yue@nokia-sbell.com>: Create this case.
    TriggerInform
    : FOR    ${i}    IN RANGE    1    4
    \    ${name_value}=    GetParameterNamesEx    Device.DNS.Client.Server.${i}.Alias
    \    log many    ${name_value}
    \    ${name_value}=    Split String    @{name_value}
    \    log many    @{name_value}[1]
    \    Should Be Equal As Strings    @{name_value}[1]    1
    : FOR    ${i}    IN RANGE    1    4
    \    ${alias}=    GetParameterValuesEx    Device.DNS.Client.Server.${i}.Alias
    \    log many    ${alias}
    \    ${alias_value}=    Split String    @{alias}
    \    log many    @{alias_value}[1]
    \    Comment    Should Contain    @{alias_value}[1]    cpe-
    ${name}=    Generate Random String    5    [LOWER]
    : FOR    ${i}    IN RANGE    1    4
    \    ${status}    ${body}    SetParameterAndGetResponse    Device.DNS.Client.Server.${i}.Alias cpe-${name}    9007
    \    Should be Equal    ${status}    ${True}
    \    log    ${status}
    \    log many    ${body}
    : FOR    ${i}    IN RANGE    1    4
    \    ${name}=    Generate Random String    7    [LOWER]
    \    ${status}    SetParameterValuesEx    Device.DNS.Client.Server.${i}.Alias ${name}
    \    Should be Equal    ${status}    0
    \    ${alias}=    GetParameterValuesEx    Device.DNS.Client.Server.${i}.Alias
    \    log many    ${alias}
    \    ${alias_value}=    Split String    @{alias}
    \    log many    @{alias_value}[1]
    \    Should Be Equal As Strings    @{alias_value}[1]    ${name}
    : FOR    ${i}    IN RANGE    1    4
    \    ${attribute}=    GetParameterAttributesEx    Device.DNS.Client.Server.${i}.Alias
    \    log many    @{attribute}
    \    SetParameterAttributesEx    Device.DNS.Client.Server.${i}.Alias true 1 false x

12
    [Documentation]    R+W Set and get parameter value of Device.DNS.Client.Enable
    ...
    ...    RCR: ALU02399916
    ...
    ...    Author: Cai Jinglu
    ...
    ...    Support from HDR5701
    #get the initial boolean value, it should be in form of 1/0.
    ${node}    SetVariable    Device.DNS.Client.Enable
    TriggerInform
    ${value_got_dict}    GetParameterValuesEx    ${node}    true
    ${value_init}    Get From Dictionary    ${value_got_dict}    ${node}
    Should Match Regexp    ${value_init}    ^[01]$
    #set the opposite boolean value of the initial value
    ${value_new}    Get Another Boolean    ${value_init}
    ${res}    SetParameterAndGetResponse    ${node} ${value_new}    0    ${True}
    ${value_got_dict}    GetParameterValuesEx    ${node}    true
    ${value_got}    Get From Dictionary    ${value_got_dict}    ${node}
    Should Be Equal    ${value_got}    ${value_new}
    #set the value to true, it should be read as 1.
    ${value_new}    SetVariable    true
    ${res}    SetParameterAndGetResponse    ${node} ${value_new}    0    ${True}
    ${value_got_dict}    GetParameterValuesEx    ${node}    true
    ${value_got}    Get From Dictionary    ${value_got_dict}    ${node}
    Should Be Equal    ${value_got}    1
    #set the value to false, it should be read as 0.
    ${value_new}    SetVariable    false
    ${res}    SetParameterAndGetResponse    ${node} ${value_new}    0    ${True}
    ${value_got_dict}    GetParameterValuesEx    ${node}    true
    ${value_got}    Get From Dictionary    ${value_got_dict}    ${node}
    Should Be Equal    ${value_got}    0
    [Teardown]    teardown_t61    ${node}    ${value_init}

13
    [Documentation]    R+W Set and get parameter value of Device.DNS.Client.Server.{i}.Enable
    ...
    ...    RCR: ALU02399916
    ...
    ...    Author: Cai Jinglu
    ...
    ...    Support from HDR5701
    #get the initial boolean value, it should be in form of 1/0.
    ${dns_server}    Set Variable    1.1.1.1
    TriggerInform
    ${index}=    AddObjectEx    Device.DNS.Client.Server.
    ${node}    SetVariable    Device.DNS.Client.Server.${index}.Enable
    ${res}    SetParameterAndGetResponse    Device.DNS.Client.Server.${index}.DNSServer ${dns_server}    0    ${True}
    ${value_got_dict}    GetParameterValuesEx    ${node}    true
    ${value_init}    Get From Dictionary    ${value_got_dict}    ${node}
    Should Match Regexp    ${value_init}    ^[01]$
    #set the opposite boolean value of the initial value
    ${value_new}    Get Another Boolean    ${value_init}
    ${res}    SetParameterAndGetResponse    ${node} ${value_new}    0    ${True}
    ${value_got_dict}    GetParameterValuesEx    ${node}    true
    ${value_got}    Get From Dictionary    ${value_got_dict}    ${node}
    Should Be Equal    ${value_got}    ${value_new}
    #set the value to true, it should be read as 1.
    ${value_new}    SetVariable    true
    ${res}    SetParameterAndGetResponse    ${node} ${value_new}    0    ${True}
    ${value_got_dict}    GetParameterValuesEx    ${node}    true
    ${value_got}    Get From Dictionary    ${value_got_dict}    ${node}
    Should Be Equal    ${value_got}    1
    #set the value to false, it should be read as 0.
    ${value_new}    SetVariable    false
    ${res}    SetParameterAndGetResponse    ${node} ${value_new}    0    ${True}
    ${value_got_dict}    GetParameterValuesEx    ${node}    true
    ${value_got}    Get From Dictionary    ${value_got_dict}    ${node}
    Should Be Equal    ${value_got}    0
    [Teardown]    DeleteDNSServer-181    ${index}

*** Keywords ***
teardown_t61
    [Arguments]    ${node}    ${v}
    # restore the target parameter
    SetParameterValuesEx    ${node} ${v}
    # reset_flag
    # Run Keyword If Test Passed    set suite variable    ${f_test_need_setup}    ${false}
    close client

DeleteDNSServer-181
    [Arguments]    ${index}
    ${delete_status}=    DeleteObjectEx    Device.DNS.Client.Server.${index}.
    Should Be Equal    ${delete_status}    0
