*** Settings ***
Suite Setup       SuiteSetup
Suite Teardown    SuiteTeardown
Test Teardown     CloseClient
Default Tags      G240WB    G240WC    G240WZA    MXXT    MXXV    SAIB    VFSP
...               VFNB
Resource          %{ROBOTREPO}/HLK/res/l2-tr069.txt
Library           HttpLibrary.HTTP
Resource          %{ROBOTREPO}/HLK/res/l3-tr069.txt
Library           %{RFPATH}/Python-2.7/lib/python2.7/site-packages/robot/libraries/String.py
Library           %{RFPATH}/Python-2.7/lib/python2.7/site-packages/robot/libraries/Collections.py
Resource          %{ROBOTREPO}/HLK/res/l3-telnet.txt
Resource          %{ROBOTREPO}/HLK/res/l3-selenium2.txt
Resource          %{ROBOTREPO}/HLK/res/l2-pcta.txt
Library           %{ROBOTREPO}/HLK/lib/hlk_generate_data_within_range.py

*** Test Cases ***
01
    [Documentation]    Get Parameter Name-Device.RootDataModelVersion
    ...
    ...    This is a read-only parameter and can not support modify
    ...
    ...    This support from HDR56
    ...
    ...
    ...    RCR ID:ALU02106045
    ...
    ...
    ...    Author:Zhang Yaping
    TriggerInform
    @{paramter_list}=    GetParameterValuesEx    Device.RootDataModelVersion
    log many    @{paramter_list}
    ${parameter_dict}=    Create Dictionary
    : FOR    ${string}    IN    @{paramter_list}
    \    @{entry}=    Split String    ${string}    ${SPACE}
    \    Set To Dictionary    ${parameter_dict}    @{entry}[0]    @{entry}[1]
    ${RootDataModelVersion}=    Get From Dictionary    ${parameter_dict}    Device.RootDataModelVersion
    Should Be Equal As Numbers    ${RootDataModelVersion}    2.4
    ${status}    ${body}    SetParameterAndGetResponse    Device.RootDataModelVersion 11    9008
    Should be Equal    ${status}    ${True}
    log    ${status}
    log many    ${body}

*** Keywords ***
SuiteSetup
    Should Be Equal As Strings    ${startup_status}    PASS
    Create Http Context    localhost:8015

SuiteTeardown
    log    pass
