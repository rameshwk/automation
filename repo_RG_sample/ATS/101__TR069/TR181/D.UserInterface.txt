*** Settings ***
Suite Setup       Create Http Context    localhost:8015
Suite Teardown    CloseClient
Default Tags      G240WB    G240WC    G240WZA    MXXT    MXXV    SAIB    VFSP
...               VFNB
Resource          %{ROBOTREPO}/HLK/res/l3-selenium2.txt
Resource          %{ROBOTREPO}/HLK/res/l2-pcta.txt
Resource          %{ROBOTREPO}/HLK/res/l2-tr069.txt
Library           HttpLibrary.HTTP
Resource          %{ROBOTREPO}/HLK/res/l3-tr069.txt
Resource          %{ROBOTREPO}/HLK/res/l3-telnet.txt
Library           %{RFPATH}/Python-2.7/lib/python2.7/site-packages/robot/libraries/String.py
Library           %{RFPATH}/Python-2.7/lib/python2.7/site-packages/robot/libraries/Collections.py

*** Test Cases ***
01
    [Documentation]    Get Parameter value of Device.UserInterface.RemoteAccess
    ...
    ...    This supported from HDR5701
    ...
    ...    RCR ID:ALU0236334
    ...
    ...    *Author:* Zhang Yaping
    ...
    ...    *Last Reviser:* Zhang Yaping
    ...    ---
    ...    == Customer ==
    ...
    ...    G240WB,G240WC,G240WZA,MXXT,MXXV,SAIB
    ...    ---
    ...    == History ==
    ...    _*Rev 0:*_ November 27, 2017 --- Zhang Yaping \ <Yaping.Zhang@nokia-sbell.com>: Create this case.
    TriggerInform
    @{parameter_list}=    GetParameterValuesEx    Device.UserInterface.RemoteAccess.
    log many    @{parameter_list}

02
    [Documentation]    Enable http remote access based on \ PPPOE Internet WAN Connection
    ...
    ...    This supported from HDR5701
    ...
    ...    RCR ID:ALU0236334
    ...
    ...    *Author:* Zhang Yaping
    ...
    ...    *Last Reviser:* Zhang Yaping
    ...    ---
    ...    == Customer ==
    ...
    ...    G240WB,G240WC,G240WZA,MXXT,MXXV,SAIB
    ...    ---
    ...    == History ==
    ...    _*Rev 0:*_ November 27, 2017 --- Zhang Yaping \ <Yaping.Zhang@nokia-sbell.com>: Create this case.
    ...
    ...    _*Rev 1:*_ May 31, 2017 --- Zhang Yaping \ <Yaping.Zhang@nokia-sbell.com>: Fix ATC Bug http://135.251.207.227/project/issues/216
    TriggerInform
    ${cpe_internet_interface}=    SearchWANConnection    INTERNET    TR181
    log    ${cpe_internet_interface}
    ${wan}    SetVariable    ${cpe_internet_interface}.IPv4Address.1.IPAddress
    @{parameter_list}=    GetParameterValuesEx    ${wan}
    ${cpe_internet_ip}=    Fetch From Right    @{parameter_list}[0]    ${SPACE}
    @{parameter_list}=    GetParameterValuesEx    Device.X_ALU-COM_AccessControl.AccessControlEntry.
    log many    @{parameter_list}
    ${rule}    Set Variable    Device.X_ALU-COM_AccessControl.AccessControlEntry.([0-9]+).Interface +${cpe_internet_interface}
    ${matches}    Get Matches    ${parameter_list}    regexp=${rule}
    log    ${matches}
    ${res}    ${i}    Should Match Regexp    @{matches}[0]    ${rule}
    log    ${i}
    Set Suite Variable    ${i}
    ${status}=    SetParameterValuesEx    Device.X_ALU-COM_AccessControl.AccessControlEntry.${i}.HttpDisabled 0
    Should Be Equal    ${status}    0
    ${passed}=    Run Keyword And Return Status    Should Match Regexp    ${ont['opid']}    VFSP
    ${status}=    Run Keyword If    '${passed}'=='True'    SetParameterValuesEx    Device.UserInterface.RemoteAccess.Enable 1 Device.UserInterface.RemoteAccess.Protocol HTTP Device.UserInterface.RemoteAccess.Port 80
    ...    ELSE IF    '${passed}'=='False'    SetParameterValuesEx    Device.UserInterface.RemoteAccess.Enable 1 Device.UserInterface.RemoteAccess.Protocol HTTP Device.UserInterface.RemoteAccess.Port 8080
    Should Be Equal    ${status}    0
    @{parameter_list}=    GetParameterValuesEx    Device.UserInterface.RemoteAccess.
    log many    @{parameter_list}
    ${parameter_dict}=    Create Dictionary
    : FOR    ${string}    IN    @{parameter_list}
    \    @{entry}=    Split String    ${string}    ${SPACE}
    \    Set To Dictionary    ${parameter_dict}    @{entry}[0]    @{entry}[1]
    ${Status}=    Get From Dictionary    ${parameter_dict}    Device.UserInterface.RemoteAccess.Enable
    Should Be Equal As Integers    ${Status}    1
    ${protocol}=    Get From Dictionary    ${parameter_dict}    Device.UserInterface.RemoteAccess.Protocol
    Should Be Equal    ${protocol}    HTTP
    ${protocol1}=    Convert to Lowercase    ${protocol}
    ${port}=    Get From Dictionary    ${parameter_dict}    Device.UserInterface.RemoteAccess.Port
    Run Keyword If    '${passed}'=='True'    Should Be Equal    ${port}    80
    ...    ELSE IF    '${passed}'=='False'    Should Be Equal    ${port}    8080
    l3-selenium2.LoginGUI    ${ont['gui_type']}    ${protocol1}://${cpe_internet_ip}:${port}    ${ont['gui_username']}    ${ont['gui_password']}
    ${DeviceInfo}=    l3-selenium2.GetDeviceInfo    ${ont['gui_type']}    ${protocol1}://${cpe_internet_ip}:${port}
    ${SoftwareVersion}=    Get From Dictionary    ${DeviceInfo}    software_version
    Should Not Be Empty    ${SoftwareVersion}
    l3-selenium2.LogoutGUI    ${ont['gui_type']}    ${protocol1}://${cpe_internet_ip}:${port}
    [Teardown]    Teardown

03
    [Documentation]    Enable http remote access based on \ IPOE TR069 WAN Connection
    ...
    ...    This supported from HDR5701
    ...
    ...    RCR ID:ALU0236334
    ...
    ...    *Author:* Zhang Yaping
    ...
    ...    *Last Reviser:* Zhang Yaping
    ...    ---
    ...    == Customer ==
    ...
    ...    G240WB,G240WC,G240WZA,MXXT,MXXV,SAIB
    ...    ---
    ...    == History ==
    ...    _*Rev 0:*_ November 27, 2017 --- Zhang Yaping \ <Yaping.Zhang@nokia-sbell.com>: Create this case.
    ...
    ...    _*Rev 1:*_ May 31, 2017 --- Zhang Yaping \ <Yaping.Zhang@nokia-sbell.com>: Fix ATC Bug http://135.251.207.227/project/issues/216. If for VFSP Failed ,please refer to FR ALU02471063
    TriggerInform
    ${cpe_tr069_interface}=    SearchWANConnection    TR069    TR181
    log    ${cpe_tr069_interface}
    ${wan}    SetVariable    ${cpe_tr069_interface}.IPv4Address.1.IPAddress
    @{parameter_list}=    GetParameterValuesEx    ${wan}
    ${cpe_tr069_ip}=    Fetch From Right    @{parameter_list}[0]    ${SPACE}
    @{parameter_list}=    GetParameterValuesEx    Device.X_ALU-COM_AccessControl.AccessControlEntry.
    log many    @{parameter_list}
    ${rule}    Set Variable    Device.X_ALU-COM_AccessControl.AccessControlEntry.([0-9]+).Interface +${cpe_tr069_interface}
    ${matches}    Get Matches    ${parameter_list}    regexp=${rule}
    log    ${matches}
    ${res}    ${i}    Should Match Regexp    @{matches}[0]    ${rule}
    log    ${i}
    Set Suite Variable    ${i}
    ${status}=    SetParameterValuesEx    Device.X_ALU-COM_AccessControl.AccessControlEntry.${i}.HttpDisabled 0
    Should Be Equal    ${status}    0
    ${passed}=    Run Keyword And Return Status    Should Match Regexp    ${ont['opid']}    VFSP
    ${status}=    Run Keyword If    '${passed}'=='True'    SetParameterValuesEx    Device.UserInterface.RemoteAccess.Enable 1 Device.UserInterface.RemoteAccess.Protocol HTTP Device.UserInterface.RemoteAccess.Port 80
    ...    ELSE IF    '${passed}'=='False'    SetParameterValuesEx    Device.UserInterface.RemoteAccess.Enable 1 Device.UserInterface.RemoteAccess.Protocol HTTP Device.UserInterface.RemoteAccess.Port 8080
    Should Be Equal    ${status}    0
    @{parameter_list}=    GetParameterValuesEx    Device.UserInterface.RemoteAccess.
    log many    @{parameter_list}
    ${parameter_dict}=    Create Dictionary
    : FOR    ${string}    IN    @{parameter_list}
    \    @{entry}=    Split String    ${string}    ${SPACE}
    \    Set To Dictionary    ${parameter_dict}    @{entry}[0]    @{entry}[1]
    ${Status}=    Get From Dictionary    ${parameter_dict}    Device.UserInterface.RemoteAccess.Enable
    Should Be Equal As Integers    ${Status}    1
    ${protocol}=    Get From Dictionary    ${parameter_dict}    Device.UserInterface.RemoteAccess.Protocol
    Should Be Equal    ${protocol}    HTTP
    ${protocol1}=    Convert to Lowercase    ${protocol}
    ${port}=    Get From Dictionary    ${parameter_dict}    Device.UserInterface.RemoteAccess.Port
    Run Keyword If    '${passed}'=='True'    Should Be Equal    ${port}    80
    ...    ELSE IF    '${passed}'=='False'    Should Be Equal    ${port}    8080
    l3-selenium2.LoginGUI    ${ont['gui_type']}    ${protocol1}://${cpe_tr069_ip}:${port}    ${ont['gui_username']}    ${ont['gui_password']}
    ${DeviceInfo}=    l3-selenium2.GetDeviceInfo    ${ont['gui_type']}    ${protocol1}://${cpe_tr069_ip}:${port}
    ${SoftwareVersion}=    Get From Dictionary    ${DeviceInfo}    software_version
    Should Not Be Empty    ${SoftwareVersion}
    l3-selenium2.LogoutGUI    ${ont['gui_type']}    ${protocol1}://${cpe_tr069_ip}:${port}
    [Teardown]    Teardown

04
    [Documentation]    Enable https remote access based on PPPOE Internet WAN Connection
    ...
    ...    This supported from HDR5701
    ...
    ...    RCR ID:ALU0236334
    ...
    ...    *Author:* Zhang Yaping
    ...
    ...    *Last Reviser:* Zhang Yaping
    ...    ---
    ...    == Customer ==
    ...
    ...    G240WB,G240WC,G240WZA,MXXT,MXXV,SAIB
    ...    ---
    ...    == History ==
    ...    _*Rev 0:*_ November 28, 2017 --- Zhang Yaping \ <Yaping.Zhang@nokia-sbell.com>: Create this case.
    TriggerInform
    ${cpe_internet_interface}=    SearchWANConnection    INTERNET    TR181
    log    ${cpe_internet_interface}
    ${wan}    SetVariable    ${cpe_internet_interface}.IPv4Address.1.IPAddress
    @{parameter_list}=    GetParameterValuesEx    ${wan}
    ${cpe_internet_ip}=    Fetch From Right    @{parameter_list}[0]    ${SPACE}
    @{parameter_list}=    GetParameterValuesEx    Device.X_ALU-COM_AccessControl.AccessControlEntry.
    log many    @{parameter_list}
    ${rule}    Set Variable    Device.X_ALU-COM_AccessControl.AccessControlEntry.([0-9]+).Interface +${cpe_internet_interface}
    ${matches}    Get Matches    ${parameter_list}    regexp=${rule}
    log    ${matches}
    ${res}    ${i}    Should Match Regexp    @{matches}[0]    ${rule}
    log    ${i}
    Set Suite Variable    ${i}
    ${status}=    SetParameterValuesEx    Device.X_ALU-COM_AccessControl.AccessControlEntry.${i}.HttpsDisabled 0
    Should Be Equal    ${status}    0
    ${passed}=    Run Keyword And Return Status    Should Match Regexp    ${ont['opid']}    VFSP
    ${status}=    Run Keyword If    '${passed}'=='True'    SetParameterValuesEx    Device.UserInterface.RemoteAccess.Enable 1 Device.UserInterface.RemoteAccess.Protocol HTTPS Device.UserInterface.RemoteAccess.Port 443
    ...    ELSE IF    '${passed}'=='False'    SetParameterValuesEx    Device.UserInterface.RemoteAccess.Enable 1 Device.UserInterface.RemoteAccess.Protocol HTTPS Device.UserInterface.RemoteAccess.Port 8090
    Should Be Equal    ${status}    0
    @{parameter_list}=    GetParameterValuesEx    Device.UserInterface.RemoteAccess.
    log many    @{parameter_list}
    ${parameter_dict}=    Create Dictionary
    : FOR    ${string}    IN    @{parameter_list}
    \    @{entry}=    Split String    ${string}    ${SPACE}
    \    Set To Dictionary    ${parameter_dict}    @{entry}[0]    @{entry}[1]
    ${Status}=    Get From Dictionary    ${parameter_dict}    Device.UserInterface.RemoteAccess.Enable
    Should Be Equal As Integers    ${Status}    1
    ${protocol}=    Get From Dictionary    ${parameter_dict}    Device.UserInterface.RemoteAccess.Protocol
    Should Be Equal    ${protocol}    HTTPS
    ${protocol1}=    Convert to Lowercase    ${protocol}
    ${port}=    Get From Dictionary    ${parameter_dict}    Device.UserInterface.RemoteAccess.Port
    Run Keyword If    '${passed}'=='True'    Should Be Equal    ${port}    443
    ...    ELSE IF    '${passed}'=='False'    Should Be Equal    ${port}    8090
    l3-selenium2.LoginGUI    ${ont['gui_type']}    ${protocol1}://${cpe_internet_ip}:${port}    ${ont['gui_username']}    ${ont['gui_password']}
    ${DeviceInfo}=    l3-selenium2.GetDeviceInfo    ${ont['gui_type']}    ${protocol1}://${cpe_internet_ip}:${port}
    ${SoftwareVersion}=    Get From Dictionary    ${DeviceInfo}    software_version
    Should Not Be Empty    ${SoftwareVersion}
    l3-selenium2.LogoutGUI    ${ont['gui_type']}    ${protocol1}://${cpe_internet_ip}:${port}
    [Teardown]    Teardown

05
    [Documentation]    Enable https remote access based on \ IPOE TR069 WAN Connection
    ...
    ...    This supported from HDR5701
    ...
    ...    RCR ID:ALU0236334
    ...
    ...    *Author:* Zhang Yaping
    ...
    ...    *Last Reviser:* Zhang Yaping
    ...    ---
    ...    == Customer ==
    ...
    ...    G240WB,G240WC,G240WZA,MXXT,MXXV,SAIB
    ...    ---
    ...    == History ==
    ...    _*Rev 0:*_ November 28, 2017 --- Zhang Yaping \ <Yaping.Zhang@nokia-sbell.com>: Create this case.
    ...
    ...    _*Rev 1:*_ May 31, 2017 --- Zhang Yaping \ <Yaping.Zhang@nokia-sbell.com>: Update cases for VFSP, If for VFSP Failed ,please refer to FR ALU02471107
    TriggerInform
    ${cpe_tr069_interface}=    SearchWANConnection    TR069    TR181
    log    ${cpe_tr069_interface}
    ${wan}    SetVariable    ${cpe_tr069_interface}.IPv4Address.1.IPAddress
    @{parameter_list}=    GetParameterValuesEx    ${wan}
    ${cpe_tr069_ip}=    Fetch From Right    @{parameter_list}[0]    ${SPACE}
    @{parameter_list}=    GetParameterValuesEx    Device.X_ALU-COM_AccessControl.AccessControlEntry.
    log many    @{parameter_list}
    ${rule}    Set Variable    Device.X_ALU-COM_AccessControl.AccessControlEntry.([0-9]+).Interface +${cpe_tr069_interface}
    ${matches}    Get Matches    ${parameter_list}    regexp=${rule}
    log    ${matches}
    ${res}    ${i}    Should Match Regexp    @{matches}[0]    ${rule}
    log    ${i}
    Set Suite Variable    ${i}
    ${status}=    SetParameterValuesEx    Device.X_ALU-COM_AccessControl.AccessControlEntry.${i}.HttpsDisabled 0
    Should Be Equal    ${status}    0
    ${passed}=    Run Keyword And Return Status    Should Match Regexp    ${ont['opid']}    VFSP
    ${status}=    Run Keyword If    '${passed}'=='True'    SetParameterValuesEx    Device.UserInterface.RemoteAccess.Enable 1 Device.UserInterface.RemoteAccess.Protocol HTTPS Device.UserInterface.RemoteAccess.Port 443
    ...    ELSE IF    '${passed}'=='False'    SetParameterValuesEx    Device.UserInterface.RemoteAccess.Enable 1 Device.UserInterface.RemoteAccess.Protocol HTTPS Device.UserInterface.RemoteAccess.Port 8090
    Should Be Equal    ${status}    0
    @{parameter_list}=    GetParameterValuesEx    Device.UserInterface.RemoteAccess.
    log many    @{parameter_list}
    ${parameter_dict}=    Create Dictionary
    : FOR    ${string}    IN    @{parameter_list}
    \    @{entry}=    Split String    ${string}    ${SPACE}
    \    Set To Dictionary    ${parameter_dict}    @{entry}[0]    @{entry}[1]
    ${Status}=    Get From Dictionary    ${parameter_dict}    Device.UserInterface.RemoteAccess.Enable
    Should Be Equal As Integers    ${Status}    1
    ${protocol}=    Get From Dictionary    ${parameter_dict}    Device.UserInterface.RemoteAccess.Protocol
    Should Be Equal    ${protocol}    HTTPS
    ${protocol1}=    Convert to Lowercase    ${protocol}
    ${port}=    Get From Dictionary    ${parameter_dict}    Device.UserInterface.RemoteAccess.Port
    Run Keyword If    '${passed}'=='True'    Should Be Equal    ${port}    443
    ...    ELSE IF    '${passed}'=='False'    Should Be Equal    ${port}    8090
    l3-selenium2.LoginGUI    ${ont['gui_type']}    ${protocol1}://${cpe_tr069_ip}:${port}    ${ont['gui_username']}    ${ont['gui_password']}
    ${DeviceInfo}=    l3-selenium2.GetDeviceInfo    ${ont['gui_type']}    ${protocol1}://${cpe_tr069_ip}:${port}
    ${SoftwareVersion}=    Get From Dictionary    ${DeviceInfo}    software_version
    Should Not Be Empty    ${SoftwareVersion}
    l3-selenium2.LogoutGUI    ${ont['gui_type']}    ${protocol1}://${cpe_tr069_ip}:${port}
    [Teardown]    Teardown

06
    [Documentation]    R+W Set and get parameter value of Device.UserInterface.RemoteAccess.Enable
    ...
    ...    RCR: ALU02399916
    ...
    ...    Author: Cai Jinglu
    ...
    ...    Support from HDR5701
    #get the initial boolean value, it should be in form of 1/0.
    ${node}    SetVariable    Device.UserInterface.RemoteAccess.Enable
    TriggerInform
    ${value_got_dict}    GetParameterValuesEx    ${node}    true
    ${value_init}    Get From Dictionary    ${value_got_dict}    ${node}
    Should Match Regexp    ${value_init}    ^[01]$
    #set the opposite boolean value of the initial value
    ${value_new}    Get Another Boolean    ${value_init}
    ${res}    SetParameterAndGetResponse    ${node} ${value_new}    0    ${True}
    ${value_got_dict}    GetParameterValuesEx    ${node}    true
    ${value_got}    Get From Dictionary    ${value_got_dict}    ${node}
    Should Be Equal    ${value_got}    ${value_new}
    #set the value to true, it should be read as 1.
    ${value_new}    SetVariable    true
    ${res}    SetParameterAndGetResponse    ${node} ${value_new}    0    ${True}
    ${value_got_dict}    GetParameterValuesEx    ${node}    true
    ${value_got}    Get From Dictionary    ${value_got_dict}    ${node}
    Should Be Equal    ${value_got}    1
    #set the value to false, it should be read as 0.
    ${value_new}    SetVariable    false
    ${res}    SetParameterAndGetResponse    ${node} ${value_new}    0    ${True}
    ${value_got_dict}    GetParameterValuesEx    ${node}    true
    ${value_got}    Get From Dictionary    ${value_got_dict}    ${node}
    Should Be Equal    ${value_got}    0
    [Teardown]    teardown_t61    ${node}    ${value_init}

07
    [Documentation]    Parameter readOnly - Device.UserInterface.RemoteAccess.SupportedProtocols
    ...    ---
    ...    == Summary ==
    ...
    ...    *list*
    ...
    ...
    ...    {{list}} Indicates the protocols that are supported by the CPE for the purpose of remotely accessing the user interface.
    ...
    ...
    ...
    ...    ---
    ...    *RCR:* _*//your rcr//*_
    ...
    ...    *IR:* _*//your ir//*_
    ...
    ...    *Author:* Yaping Zhang
    ...
    ...    *Last Reviser:* Yaping Zhang
    ...
    ...    ---
    ...    == Implementaion Details ==
    ...    Please reference to : Nokia Lib
    ...    ---
    ...    == Customers ==
    ...
    ...    COMMON
    ...    ---
    ...    == History ==
    ...    _*0:*_ 2018.05.28 --- Yaping ZHANG<yaping.zhang@nokia-sbell.com> Created this case.
    #ONTCONFIG
    TriggerInform
    ${node} =    Set Variable    Device.UserInterface.RemoteAccess.SupportedProtocols
    #tcl soap only supports string, and treats all as string
    ${df} =    Set Variable    HTTP,HTTPS
    ${new} =    Set Variable    tcp
    ${ec9008} =    Set Variable    9008
    #get current value
    @{parameter_list}=    GetParameterValuesEx    ${node}
    ${org} =    Remove String    @{parameter_list}[0]    ${node}${SPACE}
    Run Keyword And Continue On Failure    Should Be Equal    ${org}    ${df}
    #9008
    ${w} =    Set Variable    ${node}${SPACE}${new}
    ${ret}    SetParameterAndGetResponse    ${w}    ${ec9008}    NEED_BREAK=${True}
    #value cant write, so should be still the org
    @{parameter_list}=    GetParameterValuesEx    ${node}
    ${rec} =    Remove String    @{parameter_list}[0]    ${node}${SPACE}
    Should Be Equal    ${rec}    ${org}

*** Keywords ***
Teardown
    TriggerInform
    ${status}=    SetParameterValuesEx    Device.UserInterface.RemoteAccess.Enable 0
    Should Be Equal    ${status}    0
    ${status}=    SetParameterValuesEx    Device.X_ALU-COM_AccessControl.AccessControlEntry.${i}.HttpDisabled 1
    Should Be Equal    ${status}    0

teardown_t61
    [Arguments]    ${node}    ${v}
    # restore the target parameter
    SetParameterValuesEx    ${node} ${v}
    # reset_flag
    # Run Keyword If Test Passed    set suite variable    ${f_test_need_setup}    ${false}
    close client
