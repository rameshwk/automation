*** Settings ***
Suite Setup       Create Http Context    localhost:8015
Test Teardown     CloseClient
Default Tags      G240WB    G240WC    G240WZA    MXXT    MXXV    SAIB    VFSP
...               VFNB
Resource          %{ROBOTREPO}/HLK/res/l2-tr069.txt
Library           HttpLibrary.HTTP
Resource          %{ROBOTREPO}/HLK/res/l3-tr069.txt
Library           %{RFPATH}/Python-2.7/lib/python2.7/site-packages/robot/libraries/String.py
Library           %{RFPATH}/Python-2.7/lib/python2.7/site-packages/robot/libraries/Collections.py
Resource          %{ROBOTREPO}/HLK/res/l3-telnet.txt
Resource          %{ROBOTREPO}/HLK/res/l3-selenium2.txt
Resource          %{ROBOTREPO}/HLK/res/l2-pcta.txt

*** Test Cases ***
01
    [Documentation]    Attribute readOnly - Device.SelfTestDiagnostics.Results
    ...    ---
    ...    == Summary ==
    ...
    ...    *string*
    ...
    ...
    ...    Results of self-test (vendor specific).
    ...
    ...
    ...
    ...    ---
    ...    *RCR:* _*ALU00000000*_
    ...
    ...    *IR:* _*ALU00000000*_
    ...
    ...    *Author:* Zixiao Zhao
    ...
    ...    *Last Reviser:* Zixiao Zhao
    ...
    ...    ---
    ...    == Implementaion Details ==
    ...    Please reference to : Nokia Lib
    ...    ---
    ...    == Customers ==
    ...
    ...    COMMON
    ...    ---
    ...    == History ==
    ...    _*0:*_ 2018.04.20 --- Zixiao Zhao Created this case.
    ...    [Tags]
    #OLTCONFIG
    #your OLT Configuration#
    #ONTCONFIG
    TriggerInform
    ${node} =    Set Variable    Device.SelfTestDiagnostics.Results
    #${df} =    Set Variable
    TriggerInform
    #get current value
    @{attribute_list}=    GetParameterAttributesEx    ${node}
    ${org} =    Remove String    @{attribute_list}[0]    ${node}${SPACE}
    #    Should Be Equal    ${org}    ${df}
    #set to 0,1,2
    : FOR    ${i}    IN RANGE    0    3
    \    ${set_attribut_list} =    Set Variable    ${node} true ${i} false x
    \    SetParameterAttributesEx    ${set_attribut_list}
    \    @{attribute_list}=    GetParameterAttributesEx    ${node}
    \    @{ret} =    Split String    ${attribute_list}[0]    ${SPACE}
    \    ${ii} =    Convert To String    ${i}
    \    Should Be Equal    ${ret[1]}    ${ii}
    #Stream
    #your stream machine#

02
    [Documentation]    Attribute readWrite - Device.SelfTestDiagnostics.DiagnosticsState
    ...    ---
    ...    == Summary ==
    ...
    ...    *string*
    ...
    ...
    ...    Indicates availability of diagnostic data. {{enum}}
    ...    If the ACS sets the value of this parameter to {{enum|Requested}}, the CPE MUST initiate the corresponding diagnostic test. When writing, the only allowed value is {{enum|Requested}}. To ensure the use of the proper test parameters (the writable parameters in this object), the test parameters MUST be set either prior to or at the same time as (in the same SetParameterValues) setting the DiagnosticsState to Requested.
    ...    When requested, the CPE SHOULD wait until after completion of the communication session with the ACS before starting the diagnostic.
    ...    When the test is completed, the value of this parameter MUST be either {{enum|Complete}} (if the test completed successfully), or one of the Error values listed above.
    ...    If the value of this parameter is anything other than {{enum|Complete}}, the values of the results parameters for this test are indeterminate.
    ...    When the diagnostic initiated by the ACS is completed (successfully or not), the CPE MUST establish a new connection to the ACS to allow the ACS to view the results, indicating the Event code "8 DIAGNOSTICS COMPLETE" in the Inform message.
    ...    After the diagnostic is complete, the value of all result parameters (all read-only parameters in this object) MUST be retained by the CPE until either this diagnostic is run again, or the CPE reboots. After a reboot, if the CPE has not retained the result parameters from the most recent test, it MUST set the value of this parameter to {{enum|None}}.
    ...    Modifying any of the writable parameters in this object except for this one MUST result in the value of this parameter being set to {{enum|None}}.
    ...    While the test is in progress, modifying any of the writable parameters in this object except for this one MUST result in the test being terminated and the value of this parameter being set to {{enum|None}}.
    ...    While the test is in progress, setting this parameter to {{enum|Requested}} (and possibly modifying other writable parameters in this object) MUST result in the test being terminated and then restarted using the current values of the test parameters.
    ...
    ...
    ...
    ...    ---
    ...    *RCR:* _*ALU00000000*_
    ...
    ...    *IR:* _*ALU00000000*_
    ...
    ...    *Author:* Zixiao Zhao
    ...
    ...    *Last Reviser:* Zixiao Zhao
    ...
    ...    ---
    ...    == Implementaion Details ==
    ...    Please reference to : Nokia Lib
    ...    ---
    ...    == Customers ==
    ...
    ...    COMMON
    ...    ---
    ...    == History ==
    ...    _*0:*_ 2018.04.20 --- Zixiao Zhao Created this case.
    ...    [Tags]
    #OLTCONFIG
    #your OLT Configuration#
    #ONTCONFIG
    TriggerInform
    ${node} =    Set Variable    Device.SelfTestDiagnostics.DiagnosticsState
    #${df} =    Set Variable
    TriggerInform
    #get current value
    @{attribute_list}=    GetParameterAttributesEx    ${node}
    ${org} =    Remove String    @{attribute_list}[0]    ${node}${SPACE}
    #    Should Be Equal    ${org}    ${df}
    #set to 0,1,2
    : FOR    ${i}    IN RANGE    0    3
    \    ${set_attribut_list} =    Set Variable    ${node} true ${i} false x
    \    SetParameterAttributesEx    ${set_attribut_list}
    \    @{attribute_list}=    GetParameterAttributesEx    ${node}
    \    @{ret} =    Split String    ${attribute_list}[0]    ${SPACE}
    \    ${ii} =    Convert To String    ${i}
    \    Should Be Equal    ${ret[1]}    ${ii}
    #Stream
    #your stream machine#
