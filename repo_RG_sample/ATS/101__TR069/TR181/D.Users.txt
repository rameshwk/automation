*** Settings ***
Test Setup        Create Http Context    localhost:8015
Test Teardown     CloseClient
Default Tags      G240WB    G240WC    G240WZA    MXXT    MXXV    SAIB    VFSP
...               VFNB
Resource          %{ROBOTREPO}/HLK/res/l2-tr069.txt
Library           HttpLibrary.HTTP
Resource          %{ROBOTREPO}/HLK/res/l3-tr069.txt
Library           %{RFPATH}/Python-2.7/lib/python2.7/site-packages/robot/libraries/String.py
Library           %{RFPATH}/Python-2.7/lib/python2.7/site-packages/robot/libraries/Collections.py
Resource          %{ROBOTREPO}/HLK/res/l3-telnet.txt
Resource          %{ROBOTREPO}/HLK/res/l3-selenium2.txt
Resource          %{ROBOTREPO}/HLK/res/l2-pcta.txt

*** Test Cases ***
01
    [Documentation]    RW GetSetParameterValues - Device.Users.User.{i}.Enable
    ...    ---
    ...    == Summary ==
    ...
    ...    *boolean*
    ...
    ...    DiffServ codepoint to be used for the test packets. By
    ...    default the CPE SHOULD set this value to zero.
    ...
    ...
    ...    ---
    ...    *RCR:* _*ALU02328747*_
    ...
    ...    *IR:* _*ALU02328747*_
    ...
    ...    *Author:* Zixiao Zhao <zixiao.zhao@alcatel-sbell.com.cn>
    ...
    ...    *Last Reviser:* Zixiao Zhao
    ...
    ...    ---
    ...    == Implementaion Details ==
    ...    Please reference to : Nokia Lib
    ...    [
    ...    http//ct.web.alcatel-lucent.com/scm-lib4/view.cgi?number=3HH-14516-6001-DFZZA
    ...    | 3HH-14516-6001-DFZZA-Feature Readiness Checklists ALU02328747 ]
    ...    - [
    ...    http//ct.web.alcatel-lucent.com/scm-lib4/view.cgi?number=3HH-14314-600B-DFZZA
    ...    | 3HH-14516-6001-DFZZA-Feature Readiness Checklists ALU02328747 FDT1575
    ...    ]
    ...    | *TR-181 spec* | None |
    ...    | *Nokia's implementation* | None |
    ...    ---
    ...    == Customers ==
    ...
    ...    COMMON
    ...    ---
    ...    == History ==
    ...    _*0:*_ 2017.6.21 --- Zixiao Zhao <zixiao.zhao@alcatel-sbell.com.cn> Created this case.
    ...
    ...    _*1:*_ 2017.8.17 --- Zixiao Zhao <zixiao.zhao@nokia-sbell.com> Modified this case.
    ...
    ...    _*2:*_ 2017.9.2 --- Zixiao Zhao <zixiao.zhao@nokia-sbell.com> Modified this case.
    ...
    ...    _*3:*_ 2017.11.17 --- Zixiao Zhao <zixiao.zhao@nokia-sbell.com> Modified this case.
    [Tags]    UNUSE
    #1 true, 0 false
    #default is 1 now
    ${new_9006} =    Set Variable    ok
    ${new_9007} =    Set Variable    noob
    ${ec9006} =    Set Variable    9007
    ${ec9007} =    Set Variable    9007
    TriggerInform
    # notice that 1 for normal user, 2 for super user
    # 1 [0]; 2 [1]
    ${n} =    GetParameterValuesEx    Device.Users.UserNumberOfEntries
    ${n} =    Fetch From Right    @{n}[0]    ${SPACE}
    ${n} =    ConvertToInteger    ${n}
    ${n2} =    Evaluate    ${n} + 1
    : FOR    ${i}    IN RANGE    1    ${n2}
    \    ${node} =    Set Variable    Device.Users.User.${i}.Enable
    \    #get current value
    \    @{parameter_list}=    GetParameterValuesEx    ${node}
    \    ${org} =    Remove String    @{parameter_list}[0]    ${node}${SPACE}
    \    ${df} =    Set Variable If    ${i} == ${n}    1    0
    \    ${new} =    Set Variable If    ${i} == ${n}    0    1
    \    Run Keyword And Continue On Failure    Should Be Equal    ${org}    ${df}
    \    #9006
    \    ${w} =    Set Variable    ${node}${SPACE}${new_9006}
    \    @{ret}=    SetParameterAndGetResponse    ${w}
    \    Run Keyword And Continue On Failure    Should Contain    ${ret[1]}    ${ec9006}
    \    #9007
    \    ${w} =    Set Variable    ${node}${SPACE}${new_9007}
    \    @{ret}=    SetParameterAndGetResponse    ${w}
    \    Run Keyword And Continue On Failure    Should Contain    ${ret[1]}    ${ec9007}
    \    #set to new value
    \    ${w} =    Set Variable    ${node}${SPACE}${new}
    \    SetParameterValuesEx    ${w}
    \    @{parameter_list}=    GetParameterValuesEx    ${node}
    \    ${new_v} =    Remove String    @{parameter_list}[0]    ${node}${SPACE}
    \    #Log    new is ${new}
    \    Run Keyword And Continue On Failure    Should Match Regexp    ${new_v}    (?i)${new}
    \    #recover back org value
    \    ${w} =    Set Variable    ${node}${SPACE}${org}
    \    SetParameterValuesEx    ${w}
    \    @{parameter_list}=    GetParameterValuesEx    ${node}
    \    ${rec} =    Remove String    @{parameter_list}[0]    ${node}${SPACE}
    \    #Log    recover is ${rec}
    \    Should Be Equal    ${rec}    ${org}

02
    [Documentation]    RW GetSetParameterValues - Device.Users.User.{i}.Username
    ...    ---
    ...    == Summary ==
    ...
    ...    *string*
    ...
    ...    DiffServ codepoint to be used for the test packets. By
    ...    default the CPE SHOULD set this value to zero.
    ...
    ...
    ...    ---
    ...    *RCR:* _*ALU02328747*_
    ...
    ...    *IR:* _*ALU02328747*_
    ...
    ...    *Author:* Zixiao Zhao <zixiao.zhao@alcatel-sbell.com.cn>
    ...
    ...    *Last Reviser:* Zixiao Zhao
    ...
    ...    ---
    ...    == Implementaion Details ==
    ...    Please reference to : Nokia Lib
    ...    [
    ...    http//ct.web.alcatel-lucent.com/scm-lib4/view.cgi?number=3HH-14516-6001-DFZZA
    ...    | 3HH-14516-6001-DFZZA-Feature Readiness Checklists ALU02328747 ]
    ...    - [
    ...    http//ct.web.alcatel-lucent.com/scm-lib4/view.cgi?number=3HH-14314-600B-DFZZA
    ...    | 3HH-14516-6001-DFZZA-Feature Readiness Checklists ALU02328747 FDT1575
    ...    ]
    ...    | *TR-181 spec* | None |
    ...    | *Nokia's implementation* | None |
    ...    ---
    ...    == Customers ==
    ...
    ...    COMMON
    ...    ---
    ...    == History ==
    ...    _*0:*_ 2017.6.21 --- Zixiao Zhao <zixiao.zhao@alcatel-sbell.com.cn> Created this case.
    ...
    ...    _*1:*_ 2017.8.17 --- Zixiao Zhao <zixiao.zhao@nokia-sbell.com> Modified this case.
    ...
    ...    _*2:*_ 2017.9.2 --- Zixiao Zhao <zixiao.zhao@nokia-sbell.com> Modified this case.
    TriggerInform
    ${n} =    GetParameterValuesEx    Device.Users.UserNumberOfEntries
    ${n} =    Fetch From Right    @{n}[0]    ${SPACE}
    ${n} =    ConvertToInteger    ${n}
    ${n} =    Evaluate    ${n} + 1
    : FOR    ${i}    IN RANGE    1    ${n}
    \    @{parameter_list}=    GetParameterValuesEx    Device.Users.User.${i}.Username
    #if add judgement on username super, we may set this
    #    TriggerInform
    #    ${r} =    Set Variable    zixiao zhao
    #    @{ret}=    SetParameterAndGetResponse    Device.Users.User.1.Username ${r}
    #    Should Not Contain    ${ret[1]}    Fault

03
    [Documentation]    RW GetSetParameterValues - Device.Users.User.{i}.Password
    ...    ---
    ...    == Summary ==
    ...
    ...    *string*
    ...
    ...    DiffServ codepoint to be used for the test packets. By
    ...    default the CPE SHOULD set this value to zero.
    ...
    ...
    ...    ---
    ...    *RCR:* _*ALU02328747*_
    ...
    ...    *IR:* _*ALU02328747*_
    ...
    ...    *Author:* Zixiao Zhao <zixiao.zhao@alcatel-sbell.com.cn>
    ...
    ...    *Last Reviser:* Zixiao Zhao
    ...
    ...    ---
    ...    == Implementaion Details ==
    ...    Please reference to : Nokia Lib
    ...    [
    ...    http//ct.web.alcatel-lucent.com/scm-lib4/view.cgi?number=3HH-14516-6001-DFZZA
    ...    | 3HH-14516-6001-DFZZA-Feature Readiness Checklists ALU02328747 ]
    ...    - [
    ...    http//ct.web.alcatel-lucent.com/scm-lib4/view.cgi?number=3HH-14314-600B-DFZZA
    ...    | 3HH-14516-6001-DFZZA-Feature Readiness Checklists ALU02328747 FDT1575
    ...    ]
    ...    | *TR-181 spec* | None |
    ...    | *Nokia's implementation* | None |
    ...    ---
    ...    == Customers ==
    ...
    ...    COMMON
    ...    ---
    ...    == History ==
    ...    _*0:*_ 2017.6.21 --- Zixiao Zhao <zixiao.zhao@alcatel-sbell.com.cn> Created this case.
    ...
    ...    _*1:*_ 2017.8.17 --- Zixiao Zhao <zixiao.zhao@nokia-sbell.com> Modified this case.
    ...
    ...    _*2:*_ 2017.9.2 --- Zixiao Zhao <zixiao.zhao@nokia-sbell.com> Modified this case.
    TriggerInform
    ${n} =    GetParameterValuesEx    Device.Users.UserNumberOfEntries
    ${n} =    Fetch From Right    @{n}[0]    ${SPACE}
    ${n} =    ConvertToInteger    ${n}
    ${n} =    Evaluate    ${n} + 1
    : FOR    ${i}    IN RANGE    1    ${n}
    \    @{parameter_list}=    GetParameterValuesEx    Device.Users.User.${i}.Password
    \    Should Contain    ${parameter_list[0]}    {}
    #    TriggerInform
    #    ${r} =    Set Variable    hong yue
    #    @{ret}=    SetParameterAndGetResponse    Device.Users.User.1.Password ${r}
    #    Should Not Contain    ${ret[1]}    Fault

04
    [Documentation]    R+W Set and get parameter value of Device.Users.User.{i}.Enable
    ...
    ...    RCR: ALU02399916
    ...
    ...    Author: Cai Jinglu
    ...
    ...    Support from HDR5701
    Log    Get number of users, if it's not 2 need to contact author to check.
    TriggerInform
    ${value_got_dict}    GetParameterValuesEx    Device.Users.UserNumberOfEntries    true
    ${user_entries_num}    Get From Dictionary    ${value_got_dict}    Device.Users.UserNumberOfEntries
    Should Be Equal    ${user_entries_num}    2
    #get the initial boolean value, it should be in form of 1/0.
    ${node1}    SetVariable    Device.Users.User.1.Enable
    ${node2}    SetVariable    Device.Users.User.2.Enable
    ${value_got_dict}    GetParameterValuesEx    ${node1} ${node2}    true
    ${value1_init}    Get From Dictionary    ${value_got_dict}    ${node1}
    ${value2_init}    Get From Dictionary    ${value_got_dict}    ${node2}
    Should Match Regexp    ${value1_init}    ^[01]$
    Should Match Regexp    ${value2_init}    ^[01]$
    #set the opposite boolean value of the initial value
    ${value1_new}    Get Another Boolean    ${value1_init}
    ${value2_new}    Get Another Boolean    ${value2_init}
    ${res}    SetParameterAndGetResponse    ${node1} ${value1_new} ${node2} ${value2_new}    0    ${True}
    ${value_got_dict}    GetParameterValuesEx    ${node1} ${node2}    true
    ${value1_got}    Get From Dictionary    ${value_got_dict}    ${node1}
    ${value2_got}    Get From Dictionary    ${value_got_dict}    ${node2}
    Should Be Equal    ${value1_got}    ${value1_new}
    Should Be Equal    ${value2_got}    ${value2_new}
    #set the value to true, it should be read as 1.
    ${value_new}    SetVariable    true
    ${res}    SetParameterAndGetResponse    ${node1} ${value_new} ${node2} ${value_new}    0    ${True}
    ${value_got_dict}    GetParameterValuesEx    ${node1} ${node2}    true
    ${value1_got}    Get From Dictionary    ${value_got_dict}    ${node1}
    ${value2_got}    Get From Dictionary    ${value_got_dict}    ${node2}
    Should Be Equal    ${value1_got}    1
    Should Be Equal    ${value2_got}    1
    #set the value to false, it should be read as 0.
    ${value_new}    SetVariable    false
    ${res}    SetParameterAndGetResponse    ${node1} ${value_new} ${node2} ${value_new}    0    ${True}
    ${value_got_dict}    GetParameterValuesEx    ${node1} ${node2}    true
    ${value1_got}    Get From Dictionary    ${value_got_dict}    ${node1}
    ${value2_got}    Get From Dictionary    ${value_got_dict}    ${node2}
    Should Be Equal    ${value1_got}    0
    Should Be Equal    ${value2_got}    0
    [Teardown]    Run Keywords    teardown_t61    ${node1}    ${value1_init}
    ...    AND    Teardown_t61    ${node2}    ${value2_init}

*** Keywords ***
teardown_t61
    [Arguments]    ${node}    ${v}
    # restore the target parameter
    SetParameterValuesEx    ${node} ${v}
    # reset_flag
    # Run Keyword If Test Passed    set suite variable    ${f_test_need_setup}    ${false}
    ${value_got_dict}    GetParameterValuesEx    ${node}    true
    ${value}    Get From Dictionary    ${value_got_dict}    ${node}
    Should Be Equal    ${value}    ${v}
