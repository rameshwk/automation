*** Settings ***
Suite Setup       Create Http Context    localhost:8015
Test Teardown     CloseClient
Default Tags      G240WB    G240WC    G240WZA    MXXT    MXXV    SAIB    VFSP
...               VFNB
Resource          %{ROBOTREPO}/HLK/res/l2-tr069.txt
Library           HttpLibrary.HTTP
Resource          %{ROBOTREPO}/HLK/res/l3-tr069.txt
Library           %{RFPATH}/Python-2.7/lib/python2.7/site-packages/robot/libraries/String.py
Library           %{RFPATH}/Python-2.7/lib/python2.7/site-packages/robot/libraries/Collections.py
Resource          %{ROBOTREPO}/HLK/res/l3-telnet.txt
Resource          %{ROBOTREPO}/HLK/res/l3-selenium2.txt
Resource          %{ROBOTREPO}/HLK/res/l2-pcta.txt

*** Test Cases ***
01
    [Documentation]    Attribute readOnly - Device.ManagementServer.ManageableDeviceNumberOfEntries
    ...    ---
    ...    == Summary ==
    ...
    ...    *unsignedInt*
    ...
    ...
    ...    {{numentries}}
    ...
    ...
    ...
    ...    ---
    ...    *RCR:* _*ALU00000000*_
    ...
    ...    *IR:* _*ALU00000000*_
    ...
    ...    *Author:* Zixiao Zhao
    ...
    ...    *Last Reviser:* Zixiao Zhao
    ...
    ...    ---
    ...    == Implementaion Details ==
    ...    Please reference to : Nokia Lib
    ...    ---
    ...    == Customers ==
    ...
    ...    COMMON
    ...    ---
    ...    == History ==
    ...    _*0:*_ 2018.04.20 --- Zixiao Zhao Created this case.
    ...    [Tags]
    #OLTCONFIG
    #your OLT Configuration#
    #ONTCONFIG
    TriggerInform
    ${node} =    Set Variable    Device.ManagementServer.ManageableDeviceNumberOfEntries
    #${df} =    Set Variable
    TriggerInform
    #get current value
    @{attribute_list}=    GetParameterAttributesEx    ${node}
    ${org} =    Remove String    @{attribute_list}[0]    ${node}${SPACE}
    #    Should Be Equal    ${org}    ${df}
    #set to 0,1,2
    : FOR    ${i}    IN RANGE    0    3
    \    ${set_attribut_list} =    Set Variable    ${node} true ${i} false x
    \    SetParameterAttributesEx    ${set_attribut_list}
    \    @{attribute_list}=    GetParameterAttributesEx    ${node}
    \    @{ret} =    Split String    ${attribute_list}[0]    ${SPACE}
    \    ${ii} =    Convert To String    ${i}
    \    Should Be Equal    ${ret[1]}    ${ii}
    #Stream
    #your stream machine#

02
    [Documentation]    Attribute readOnly - Device.ManagementServer.ManageableDevice.{i}.ManufacturerOUI
    ...    ---
    ...    == Summary ==
    ...
    ...    *string*
    ...
    ...
    ...    Organizationally unique identifier of the Device manufacturer as provided to the Gateway by the Device. Represented as a six hexadecimal-digit value using all upper-case letters and including any leading zeros. {{pattern}}
    ...    The value MUST be a valid OUI as defined in {{bibref|OUI}}.
    ...
    ...
    ...
    ...    ---
    ...    *RCR:* _*ALU00000000*_
    ...
    ...    *IR:* _*ALU00000000*_
    ...
    ...    *Author:* Zixiao Zhao
    ...
    ...    *Last Reviser:* Zixiao Zhao
    ...
    ...    ---
    ...    == Implementaion Details ==
    ...    Please reference to : Nokia Lib
    ...    ---
    ...    == Customers ==
    ...
    ...    COMMON
    ...    ---
    ...    == History ==
    ...    _*0:*_ 2018.04.20 --- Zixiao Zhao Created this case.
    ...    [Tags]
    #OLTCONFIG
    #your OLT Configuration#
    #ONTCONFIG
    TriggerInform
    ${node} =    Set Variable    Device.ManagementServer.ManageableDevice.1.ManufacturerOUI
    #${df} =    Set Variable
    TriggerInform
    #get current value
    @{attribute_list}=    GetParameterAttributesEx    ${node}
    ${org} =    Remove String    @{attribute_list}[0]    ${node}${SPACE}
    #    Should Be Equal    ${org}    ${df}
    #set to 0,1,2
    : FOR    ${i}    IN RANGE    0    3
    \    ${set_attribut_list} =    Set Variable    ${node} true ${i} false x
    \    SetParameterAttributesEx    ${set_attribut_list}
    \    @{attribute_list}=    GetParameterAttributesEx    ${node}
    \    @{ret} =    Split String    ${attribute_list}[0]    ${SPACE}
    \    ${ii} =    Convert To String    ${i}
    \    Should Be Equal    ${ret[1]}    ${ii}
    #Stream
    #your stream machine#

03
    [Documentation]    Attribute readOnly - Device.ManagementServer.ManageableDevice.{i}.SerialNumber
    ...    ---
    ...    == Summary ==
    ...
    ...    *string*
    ...
    ...
    ...    Serial number of the Device as provided to the Gateway by the Device.
    ...
    ...
    ...
    ...    ---
    ...    *RCR:* _*ALU00000000*_
    ...
    ...    *IR:* _*ALU00000000*_
    ...
    ...    *Author:* Zixiao Zhao
    ...
    ...    *Last Reviser:* Zixiao Zhao
    ...
    ...    ---
    ...    == Implementaion Details ==
    ...    Please reference to : Nokia Lib
    ...    ---
    ...    == Customers ==
    ...
    ...    COMMON
    ...    ---
    ...    == History ==
    ...    _*0:*_ 2018.04.20 --- Zixiao Zhao Created this case.
    ...    [Tags]
    #OLTCONFIG
    #your OLT Configuration#
    #ONTCONFIG
    TriggerInform
    ${node} =    Set Variable    Device.ManagementServer.ManageableDevice.1.SerialNumber
    #${df} =    Set Variable
    TriggerInform
    #get current value
    @{attribute_list}=    GetParameterAttributesEx    ${node}
    ${org} =    Remove String    @{attribute_list}[0]    ${node}${SPACE}
    #    Should Be Equal    ${org}    ${df}
    #set to 0,1,2
    : FOR    ${i}    IN RANGE    0    3
    \    ${set_attribut_list} =    Set Variable    ${node} true ${i} false x
    \    SetParameterAttributesEx    ${set_attribut_list}
    \    @{attribute_list}=    GetParameterAttributesEx    ${node}
    \    @{ret} =    Split String    ${attribute_list}[0]    ${SPACE}
    \    ${ii} =    Convert To String    ${i}
    \    Should Be Equal    ${ret[1]}    ${ii}
    #Stream
    #your stream machine#

04
    [Documentation]    Attribute readOnly - Device.ManagementServer.ManageableDevice.{i}.ProductClass
    ...    ---
    ...    == Summary ==
    ...
    ...    *string*
    ...
    ...
    ...    Identifier of the class of product for which the Device's serial number applies as provided to the Gateway by the Device.
    ...    If the Device does not provide a Product Class, then this parameter MUST be {{empty}}.
    ...
    ...
    ...
    ...    ---
    ...    *RCR:* _*ALU00000000*_
    ...
    ...    *IR:* _*ALU00000000*_
    ...
    ...    *Author:* Zixiao Zhao
    ...
    ...    *Last Reviser:* Zixiao Zhao
    ...
    ...    ---
    ...    == Implementaion Details ==
    ...    Please reference to : Nokia Lib
    ...    ---
    ...    == Customers ==
    ...
    ...    COMMON
    ...    ---
    ...    == History ==
    ...    _*0:*_ 2018.04.20 --- Zixiao Zhao Created this case.
    ...    [Tags]
    #OLTCONFIG
    #your OLT Configuration#
    #ONTCONFIG
    TriggerInform
    ${node} =    Set Variable    Device.ManagementServer.ManageableDevice.1.ProductClass
    #${df} =    Set Variable
    TriggerInform
    #get current value
    @{attribute_list}=    GetParameterAttributesEx    ${node}
    ${org} =    Remove String    @{attribute_list}[0]    ${node}${SPACE}
    #    Should Be Equal    ${org}    ${df}
    #set to 0,1,2
    : FOR    ${i}    IN RANGE    0    3
    \    ${set_attribut_list} =    Set Variable    ${node} true ${i} false x
    \    SetParameterAttributesEx    ${set_attribut_list}
    \    @{attribute_list}=    GetParameterAttributesEx    ${node}
    \    @{ret} =    Split String    ${attribute_list}[0]    ${SPACE}
    \    ${ii} =    Convert To String    ${i}
    \    Should Be Equal    ${ret[1]}    ${ii}
    #Stream
    #your stream machine#
