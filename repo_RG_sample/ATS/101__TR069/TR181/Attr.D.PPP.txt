*** Settings ***
Test Setup        Create Http Context    localhost:8015
Test Teardown     CloseClient
Default Tags      G240WB    G240WC    G240WZA    MXXT    MXXV    SAIB    VFSP
...               VFNB
Resource          %{ROBOTREPO}/HLK/res/l2-tr069.txt
Library           HttpLibrary.HTTP
Resource          %{ROBOTREPO}/HLK/res/l3-tr069.txt
Library           %{RFPATH}/Python-2.7/lib/python2.7/site-packages/robot/libraries/String.py
Library           %{RFPATH}/Python-2.7/lib/python2.7/site-packages/robot/libraries/Collections.py
Resource          %{ROBOTREPO}/HLK/res/l3-telnet.txt
Resource          %{ROBOTREPO}/HLK/res/l3-selenium2.txt
Resource          %{ROBOTREPO}/HLK/res/l2-pcta.txt

*** Test Cases ***
01
    [Documentation]    Attribute readOnly - Device.PPP.SupportedNCPs
    ...    ---
    ...    == Summary ==
    ...
    ...    *list*
    ...
    ...
    ...    The Network Control Protocols (NCPs) that are supported by the device. {{enum}}
    ...    Note that {{enum|IPv6CP}} is an IPv6 capability.
    ...
    ...
    ...
    ...    ---
    ...    *RCR:* _*ALU00000000*_
    ...
    ...    *IR:* _*ALU00000000*_
    ...
    ...    *Author:* Zixiao Zhao
    ...
    ...    *Last Reviser:* Zixiao Zhao
    ...
    ...    ---
    ...    == Implementaion Details ==
    ...    Please reference to : Nokia Lib
    ...    ---
    ...    == Customers ==
    ...
    ...    COMMON
    ...    ---
    ...    == History ==
    ...    _*0:*_ 2018.04.20 --- Zixiao Zhao Created this case.
    ...    [Tags]
    #OLTCONFIG
    #your OLT Configuration#
    #ONTCONFIG
    TriggerInform
    ${node} =    Set Variable    Device.PPP.SupportedNCPs
    #${df} =    Set Variable
    TriggerInform
    #get current value
    @{attribute_list}=    GetParameterAttributesEx    ${node}
    ${org} =    Remove String    @{attribute_list}[0]    ${node}${SPACE}
    #    Should Be Equal    ${org}    ${df}
    #set to 0,1,2
    : FOR    ${i}    IN RANGE    0    3
    \    ${set_attribut_list} =    Set Variable    ${node} true ${i} false x
    \    SetParameterAttributesEx    ${set_attribut_list}
    \    @{attribute_list}=    GetParameterAttributesEx    ${node}
    \    @{ret} =    Split String    ${attribute_list}[0]    ${SPACE}
    \    ${ii} =    Convert To String    ${i}
    \    Should Be Equal    ${ret[1]}    ${ii}
    #Stream
    #your stream machine#

02
    [Documentation]    Attribute readWrite - Device.PPP.Interface.{i}.Reset
    ...    ---
    ...    == Summary ==
    ...
    ...    *boolean*
    ...
    ...
    ...    When set to {{true}}, the device MUST tear down the existing PPP connection represented by this object and establish a new one.
    ...    The device MUST initiate the reset after completion of the current CWMP session. The device MAY delay resetting the connection in order to avoid interruption of a user service such as an ongoing voice call.
    ...    Reset on a disabled interface is a no-op (not an error).
    ...
    ...
    ...
    ...    ---
    ...    *RCR:* _*ALU00000000*_
    ...
    ...    *IR:* _*ALU00000000*_
    ...
    ...    *Author:* Zixiao Zhao
    ...
    ...    *Last Reviser:* Zixiao Zhao
    ...
    ...    ---
    ...    == Implementaion Details ==
    ...    Please reference to : Nokia Lib
    ...    ---
    ...    == Customers ==
    ...
    ...    COMMON
    ...    ---
    ...    == History ==
    ...    _*0:*_ 2018.04.20 --- Zixiao Zhao Created this case.
    ...    [Tags]
    #OLTCONFIG
    #your OLT Configuration#
    #ONTCONFIG
    TriggerInform
    ${node} =    Set Variable    Device.PPP.Interface.1.Reset
    #${df} =    Set Variable
    TriggerInform
    #get current value
    @{attribute_list}=    GetParameterAttributesEx    ${node}
    ${org} =    Remove String    @{attribute_list}[0]    ${node}${SPACE}
    #    Should Be Equal    ${org}    ${df}
    #set to 0,1,2
    : FOR    ${i}    IN RANGE    0    3
    \    ${set_attribut_list} =    Set Variable    ${node} true ${i} false x
    \    SetParameterAttributesEx    ${set_attribut_list}
    \    @{attribute_list}=    GetParameterAttributesEx    ${node}
    \    @{ret} =    Split String    ${attribute_list}[0]    ${SPACE}
    \    ${ii} =    Convert To String    ${i}
    \    Should Be Equal    ${ret[1]}    ${ii}
    #Stream
    #your stream machine#

03
    [Documentation]    Attribute readOnly - Device.PPP.Interface.{i}.EncryptionProtocol
    ...    ---
    ...    == Summary ==
    ...
    ...    *string*
    ...
    ...
    ...    Describes the PPP encryption protocol.
    ...
    ...
    ...
    ...    ---
    ...    *RCR:* _*ALU00000000*_
    ...
    ...    *IR:* _*ALU00000000*_
    ...
    ...    *Author:* Zixiao Zhao
    ...
    ...    *Last Reviser:* Zixiao Zhao
    ...
    ...    ---
    ...    == Implementaion Details ==
    ...    Please reference to : Nokia Lib
    ...    ---
    ...    == Customers ==
    ...
    ...    COMMON
    ...    ---
    ...    == History ==
    ...    _*0:*_ 2018.04.20 --- Zixiao Zhao Created this case.
    ...    [Tags]
    #OLTCONFIG
    #your OLT Configuration#
    #ONTCONFIG
    TriggerInform
    ${node} =    Set Variable    Device.PPP.Interface.1.EncryptionProtocol
    #${df} =    Set Variable
    TriggerInform
    #get current value
    @{attribute_list}=    GetParameterAttributesEx    ${node}
    ${org} =    Remove String    @{attribute_list}[0]    ${node}${SPACE}
    #    Should Be Equal    ${org}    ${df}
    #set to 0,1,2
    : FOR    ${i}    IN RANGE    0    3
    \    ${set_attribut_list} =    Set Variable    ${node} true ${i} false x
    \    SetParameterAttributesEx    ${set_attribut_list}
    \    @{attribute_list}=    GetParameterAttributesEx    ${node}
    \    @{ret} =    Split String    ${attribute_list}[0]    ${SPACE}
    \    ${ii} =    Convert To String    ${i}
    \    Should Be Equal    ${ret[1]}    ${ii}
    #Stream
    #your stream machine#

04
    [Documentation]    Attribute readOnly - Device.PPP.Interface.{i}.CompressionProtocol
    ...    ---
    ...    == Summary ==
    ...
    ...    *string*
    ...
    ...
    ...    Describes the PPP compression protocol.
    ...
    ...
    ...
    ...    ---
    ...    *RCR:* _*ALU00000000*_
    ...
    ...    *IR:* _*ALU00000000*_
    ...
    ...    *Author:* Zixiao Zhao
    ...
    ...    *Last Reviser:* Zixiao Zhao
    ...
    ...    ---
    ...    == Implementaion Details ==
    ...    Please reference to : Nokia Lib
    ...    ---
    ...    == Customers ==
    ...
    ...    COMMON
    ...    ---
    ...    == History ==
    ...    _*0:*_ 2018.04.20 --- Zixiao Zhao Created this case.
    ...    [Tags]
    #OLTCONFIG
    #your OLT Configuration#
    #ONTCONFIG
    TriggerInform
    ${node} =    Set Variable    Device.PPP.Interface.1.CompressionProtocol
    #${df} =    Set Variable
    TriggerInform
    #get current value
    @{attribute_list}=    GetParameterAttributesEx    ${node}
    ${org} =    Remove String    @{attribute_list}[0]    ${node}${SPACE}
    #    Should Be Equal    ${org}    ${df}
    #set to 0,1,2
    : FOR    ${i}    IN RANGE    0    3
    \    ${set_attribut_list} =    Set Variable    ${node} true ${i} false x
    \    SetParameterAttributesEx    ${set_attribut_list}
    \    @{attribute_list}=    GetParameterAttributesEx    ${node}
    \    @{ret} =    Split String    ${attribute_list}[0]    ${SPACE}
    \    ${ii} =    Convert To String    ${i}
    \    Should Be Equal    ${ret[1]}    ${ii}
    #Stream
    #your stream machine#

05
    [Documentation]    Attribute readWrite - Device.PPP.Interface.{i}.Enable
    ...    ---
    ...    == Summary ==
    ...
    ...    *boolean*
    ...
    ...
    ...    Enables or disables the interface.
    ...    This parameter is based on ''ifAdminStatus'' from {{bibref|RFC2863}}.
    ...
    ...
    ...
    ...    ---
    ...    *RCR:* _*ALU00000000*_
    ...
    ...    *IR:* _*ALU00000000*_
    ...
    ...    *Author:* Zixiao Zhao
    ...
    ...    *Last Reviser:* Zixiao Zhao
    ...
    ...    ---
    ...    == Implementaion Details ==
    ...    Please reference to : Nokia Lib
    ...    ---
    ...    == Customers ==
    ...
    ...    COMMON
    ...    ---
    ...    == History ==
    ...    _*0:*_ 2018.04.20 --- Zixiao Zhao Created this case.
    ...    [Tags]
    #OLTCONFIG
    #your OLT Configuration#
    #ONTCONFIG
    TriggerInform
    ${node} =    Set Variable    Device.PPP.Interface.1.Enable
    #${df} =    Set Variable
    TriggerInform
    #get current value
    @{attribute_list}=    GetParameterAttributesEx    ${node}
    ${org} =    Remove String    @{attribute_list}[0]    ${node}${SPACE}
    #    Should Be Equal    ${org}    ${df}
    #set to 0,1,2
    : FOR    ${i}    IN RANGE    0    3
    \    ${set_attribut_list} =    Set Variable    ${node} true ${i} false x
    \    SetParameterAttributesEx    ${set_attribut_list}
    \    @{attribute_list}=    GetParameterAttributesEx    ${node}
    \    @{ret} =    Split String    ${attribute_list}[0]    ${SPACE}
    \    ${ii} =    Convert To String    ${i}
    \    Should Be Equal    ${ret[1]}    ${ii}
    #Stream
    #your stream machine#

06
    [Documentation]    Attribute readWrite - Device.PPP.Interface.{i}.LowerLayers
    ...    ---
    ...    == Summary ==
    ...
    ...    *list*
    ...
    ...
    ...    {{list}} {{reference|an interface object that is stacked immediately below this interface object}} See {{bibref|TR-181i2|Section 4.2.1}}.
    ...
    ...
    ...
    ...    ---
    ...    *RCR:* _*ALU00000000*_
    ...
    ...    *IR:* _*ALU00000000*_
    ...
    ...    *Author:* Zixiao Zhao
    ...
    ...    *Last Reviser:* Zixiao Zhao
    ...
    ...    ---
    ...    == Implementaion Details ==
    ...    Please reference to : Nokia Lib
    ...    ---
    ...    == Customers ==
    ...
    ...    COMMON
    ...    ---
    ...    == History ==
    ...    _*0:*_ 2018.04.20 --- Zixiao Zhao Created this case.
    ...    [Tags]
    #OLTCONFIG
    #your OLT Configuration#
    #ONTCONFIG
    TriggerInform
    ${node} =    Set Variable    Device.PPP.Interface.1.LowerLayers
    #${df} =    Set Variable
    TriggerInform
    #get current value
    @{attribute_list}=    GetParameterAttributesEx    ${node}
    ${org} =    Remove String    @{attribute_list}[0]    ${node}${SPACE}
    #    Should Be Equal    ${org}    ${df}
    #set to 0,1,2
    : FOR    ${i}    IN RANGE    0    3
    \    ${set_attribut_list} =    Set Variable    ${node} true ${i} false x
    \    SetParameterAttributesEx    ${set_attribut_list}
    \    @{attribute_list}=    GetParameterAttributesEx    ${node}
    \    @{ret} =    Split String    ${attribute_list}[0]    ${SPACE}
    \    ${ii} =    Convert To String    ${i}
    \    Should Be Equal    ${ret[1]}    ${ii}
    #Stream
    #your stream machine#

07
    [Documentation]    Attribute readOnly - Device.PPP.Interface.{i}.LastConnectionError
    ...    ---
    ...    == Summary ==
    ...
    ...    *string*
    ...
    ...
    ...    The cause of failure for the last connection setup attempt.
    ...
    ...
    ...
    ...    ---
    ...    *RCR:* _*ALU00000000*_
    ...
    ...    *IR:* _*ALU00000000*_
    ...
    ...    *Author:* Zixiao Zhao
    ...
    ...    *Last Reviser:* Zixiao Zhao
    ...
    ...    ---
    ...    == Implementaion Details ==
    ...    Please reference to : Nokia Lib
    ...    ---
    ...    == Customers ==
    ...
    ...    COMMON
    ...    ---
    ...    == History ==
    ...    _*0:*_ 2018.04.20 --- Zixiao Zhao Created this case.
    ...    [Tags]
    #OLTCONFIG
    #your OLT Configuration#
    #ONTCONFIG
    TriggerInform
    ${node} =    Set Variable    Device.PPP.Interface.1.LastConnectionError
    #${df} =    Set Variable
    TriggerInform
    #get current value
    @{attribute_list}=    GetParameterAttributesEx    ${node}
    ${org} =    Remove String    @{attribute_list}[0]    ${node}${SPACE}
    #    Should Be Equal    ${org}    ${df}
    #set to 0,1,2
    : FOR    ${i}    IN RANGE    0    3
    \    ${set_attribut_list} =    Set Variable    ${node} true ${i} false x
    \    SetParameterAttributesEx    ${set_attribut_list}
    \    @{attribute_list}=    GetParameterAttributesEx    ${node}
    \    @{ret} =    Split String    ${attribute_list}[0]    ${SPACE}
    \    ${ii} =    Convert To String    ${i}
    \    Should Be Equal    ${ret[1]}    ${ii}
    #Stream
    #your stream machine#

08
    [Documentation]    Attribute readWrite - Device.PPP.Interface.{i}.IdleDisconnectTime
    ...    ---
    ...    == Summary ==
    ...
    ...    *unsignedInt*
    ...
    ...
    ...    The time in {{units}} that if the connection remains idle, the CPE automatically terminates the connection. A value of 0 (zero) indicates that the connection is not to be shut down automatically.
    ...
    ...
    ...
    ...    ---
    ...    *RCR:* _*ALU00000000*_
    ...
    ...    *IR:* _*ALU00000000*_
    ...
    ...    *Author:* Zixiao Zhao
    ...
    ...    *Last Reviser:* Zixiao Zhao
    ...
    ...    ---
    ...    == Implementaion Details ==
    ...    Please reference to : Nokia Lib
    ...    ---
    ...    == Customers ==
    ...
    ...    COMMON
    ...    ---
    ...    == History ==
    ...    _*0:*_ 2018.04.20 --- Zixiao Zhao Created this case.
    ...    [Tags]
    #OLTCONFIG
    #your OLT Configuration#
    #ONTCONFIG
    TriggerInform
    ${node} =    Set Variable    Device.PPP.Interface.1.IdleDisconnectTime
    #${df} =    Set Variable
    TriggerInform
    #get current value
    @{attribute_list}=    GetParameterAttributesEx    ${node}
    ${org} =    Remove String    @{attribute_list}[0]    ${node}${SPACE}
    #    Should Be Equal    ${org}    ${df}
    #set to 0,1,2
    : FOR    ${i}    IN RANGE    0    3
    \    ${set_attribut_list} =    Set Variable    ${node} true ${i} false x
    \    SetParameterAttributesEx    ${set_attribut_list}
    \    @{attribute_list}=    GetParameterAttributesEx    ${node}
    \    @{ret} =    Split String    ${attribute_list}[0]    ${SPACE}
    \    ${ii} =    Convert To String    ${i}
    \    Should Be Equal    ${ret[1]}    ${ii}
    #Stream
    #your stream machine#

09
    [Documentation]    Attribute readWrite - Device.PPP.Interface.{i}.Username
    ...    ---
    ...    == Summary ==
    ...
    ...    *string*
    ...
    ...
    ...    Username to be used for authentication.
    ...
    ...
    ...
    ...    ---
    ...    *RCR:* _*ALU00000000*_
    ...
    ...    *IR:* _*ALU00000000*_
    ...
    ...    *Author:* Zixiao Zhao
    ...
    ...    *Last Reviser:* Zixiao Zhao
    ...
    ...    ---
    ...    == Implementaion Details ==
    ...    Please reference to : Nokia Lib
    ...    ---
    ...    == Customers ==
    ...
    ...    COMMON
    ...    ---
    ...    == History ==
    ...    _*0:*_ 2018.04.20 --- Zixiao Zhao Created this case.
    ...    [Tags]
    #OLTCONFIG
    #your OLT Configuration#
    #ONTCONFIG
    TriggerInform
    ${node} =    Set Variable    Device.PPP.Interface.1.Username
    #${df} =    Set Variable
    TriggerInform
    #get current value
    @{attribute_list}=    GetParameterAttributesEx    ${node}
    ${org} =    Remove String    @{attribute_list}[0]    ${node}${SPACE}
    #    Should Be Equal    ${org}    ${df}
    #set to 0,1,2
    : FOR    ${i}    IN RANGE    0    3
    \    ${set_attribut_list} =    Set Variable    ${node} true ${i} false x
    \    SetParameterAttributesEx    ${set_attribut_list}
    \    @{attribute_list}=    GetParameterAttributesEx    ${node}
    \    @{ret} =    Split String    ${attribute_list}[0]    ${SPACE}
    \    ${ii} =    Convert To String    ${i}
    \    Should Be Equal    ${ret[1]}    ${ii}
    #Stream
    #your stream machine#

10
    [Documentation]    Attribute readWrite - Device.PPP.Interface.{i}.Password
    ...    ---
    ...    == Summary ==
    ...
    ...    *string*
    ...
    ...
    ...    Password to be used for authentication.
    ...
    ...
    ...
    ...    ---
    ...    *RCR:* _*ALU00000000*_
    ...
    ...    *IR:* _*ALU00000000*_
    ...
    ...    *Author:* Zixiao Zhao
    ...
    ...    *Last Reviser:* Zixiao Zhao
    ...
    ...    ---
    ...    == Implementaion Details ==
    ...    Please reference to : Nokia Lib
    ...    ---
    ...    == Customers ==
    ...
    ...    COMMON
    ...    ---
    ...    == History ==
    ...    _*0:*_ 2018.04.20 --- Zixiao Zhao Created this case.
    ...    [Tags]
    #OLTCONFIG
    #your OLT Configuration#
    #ONTCONFIG
    TriggerInform
    ${node} =    Set Variable    Device.PPP.Interface.1.Password
    #${df} =    Set Variable
    TriggerInform
    #get current value
    @{attribute_list}=    GetParameterAttributesEx    ${node}
    ${org} =    Remove String    @{attribute_list}[0]    ${node}${SPACE}
    #    Should Be Equal    ${org}    ${df}
    #set to 0,1,2
    : FOR    ${i}    IN RANGE    0    3
    \    ${set_attribut_list} =    Set Variable    ${node} true ${i} false x
    \    SetParameterAttributesEx    ${set_attribut_list}
    \    @{attribute_list}=    GetParameterAttributesEx    ${node}
    \    @{ret} =    Split String    ${attribute_list}[0]    ${SPACE}
    \    ${ii} =    Convert To String    ${i}
    \    Should Be Equal    ${ret[1]}    ${ii}
    #Stream
    #your stream machine#

11
    [Documentation]    Attribute readOnly - Device.PPP.Interface.{i}.AuthenticationProtocol
    ...    ---
    ...    == Summary ==
    ...
    ...    *string*
    ...
    ...
    ...    Describes the PPP authentication protocol.
    ...
    ...
    ...
    ...    ---
    ...    *RCR:* _*ALU00000000*_
    ...
    ...    *IR:* _*ALU00000000*_
    ...
    ...    *Author:* Zixiao Zhao
    ...
    ...    *Last Reviser:* Zixiao Zhao
    ...
    ...    ---
    ...    == Implementaion Details ==
    ...    Please reference to : Nokia Lib
    ...    ---
    ...    == Customers ==
    ...
    ...    COMMON
    ...    ---
    ...    == History ==
    ...    _*0:*_ 2018.04.20 --- Zixiao Zhao Created this case.
    ...    [Tags]
    #OLTCONFIG
    #your OLT Configuration#
    #ONTCONFIG
    TriggerInform
    ${node} =    Set Variable    Device.PPP.Interface.1.AuthenticationProtocol
    #${df} =    Set Variable
    TriggerInform
    #get current value
    @{attribute_list}=    GetParameterAttributesEx    ${node}
    ${org} =    Remove String    @{attribute_list}[0]    ${node}${SPACE}
    #    Should Be Equal    ${org}    ${df}
    #set to 0,1,2
    : FOR    ${i}    IN RANGE    0    3
    \    ${set_attribut_list} =    Set Variable    ${node} true ${i} false x
    \    SetParameterAttributesEx    ${set_attribut_list}
    \    @{attribute_list}=    GetParameterAttributesEx    ${node}
    \    @{ret} =    Split String    ${attribute_list}[0]    ${SPACE}
    \    ${ii} =    Convert To String    ${i}
    \    Should Be Equal    ${ret[1]}    ${ii}
    #Stream
    #your stream machine#

12
    [Documentation]    Attribute readWrite - Device.PPP.Interface.{i}.ConnectionTrigger
    ...    ---
    ...    == Summary ==
    ...
    ...    *string*
    ...
    ...
    ...    Trigger used to establish the PPP connection. {{enum}}
    ...    Note that the reason for a PPP connection becoming disconnected to begin with might be either external to the CPE, such as termination by the BRAS or momentary disconnection of the physical interface, or internal to the CPE, such as use of the {{param|IdleDisconnectTime}} and/or {{param|AutoDisconnectTime}} parameters in this object.
    ...
    ...
    ...
    ...    ---
    ...    *RCR:* _*ALU00000000*_
    ...
    ...    *IR:* _*ALU00000000*_
    ...
    ...    *Author:* Zixiao Zhao
    ...
    ...    *Last Reviser:* Zixiao Zhao
    ...
    ...    ---
    ...    == Implementaion Details ==
    ...    Please reference to : Nokia Lib
    ...    ---
    ...    == Customers ==
    ...
    ...    COMMON
    ...    ---
    ...    == History ==
    ...    _*0:*_ 2018.04.20 --- Zixiao Zhao Created this case.
    ...    [Tags]
    #OLTCONFIG
    #your OLT Configuration#
    #ONTCONFIG
    TriggerInform
    ${node} =    Set Variable    Device.PPP.Interface.1.ConnectionTrigger
    #${df} =    Set Variable
    TriggerInform
    #get current value
    @{attribute_list}=    GetParameterAttributesEx    ${node}
    ${org} =    Remove String    @{attribute_list}[0]    ${node}${SPACE}
    #    Should Be Equal    ${org}    ${df}
    #set to 0,1,2
    : FOR    ${i}    IN RANGE    0    3
    \    ${set_attribut_list} =    Set Variable    ${node} true ${i} false x
    \    SetParameterAttributesEx    ${set_attribut_list}
    \    @{attribute_list}=    GetParameterAttributesEx    ${node}
    \    @{ret} =    Split String    ${attribute_list}[0]    ${SPACE}
    \    ${ii} =    Convert To String    ${i}
    \    Should Be Equal    ${ret[1]}    ${ii}
    #Stream
    #your stream machine#

13
    [Documentation]    Attribute readOnly - Device.PPP.Interface.{i}.ConnectionStatus
    ...    ---
    ...    == Summary ==
    ...
    ...    *string*
    ...
    ...
    ...    Current status of the connection.
    ...
    ...
    ...
    ...    ---
    ...    *RCR:* _*ALU00000000*_
    ...
    ...    *IR:* _*ALU00000000*_
    ...
    ...    *Author:* Zixiao Zhao
    ...
    ...    *Last Reviser:* Zixiao Zhao
    ...
    ...    ---
    ...    == Implementaion Details ==
    ...    Please reference to : Nokia Lib
    ...    ---
    ...    == Customers ==
    ...
    ...    COMMON
    ...    ---
    ...    == History ==
    ...    _*0:*_ 2018.04.20 --- Zixiao Zhao Created this case.
    ...
    ...    _*1:*_ 2018.05.23 --- Zixiao Zhao Modified this case.
    [Tags]    UNUSE
    #OLTCONFIG
    #your OLT Configuration#
    #ONTCONFIG
    TriggerInform
    ${node} =    Set Variable    Device.PPP.Interface.1.ConnectionStatus
    #${df} =    Set Variable
    TriggerInform
    #get current value
    @{attribute_list}=    GetParameterAttributesEx    ${node}
    ${org} =    Remove String    @{attribute_list}[0]    ${node}${SPACE}
    #    Should Be Equal    ${org}    ${df}
    #set to 0,1,2
    : FOR    ${i}    IN RANGE    0    3
    \    ${set_attribut_list} =    Set Variable    ${node} true ${i} false x
    \    SetParameterAttributesEx    ${set_attribut_list}
    \    @{attribute_list}=    GetParameterAttributesEx    ${node}
    \    @{ret} =    Split String    ${attribute_list}[0]    ${SPACE}
    \    ${ii} =    Convert To String    ${i}
    \    Should Be Equal    ${ret[1]}    ${ii}
    #Stream
    #your stream machine#

14
    [Documentation]    Attribute readOnly - Device.PPP.Interface.{i}.Name
    ...    ---
    ...    == Summary ==
    ...
    ...    *string*
    ...
    ...
    ...    The textual name of the interface as assigned by the CPE.
    ...
    ...
    ...
    ...    ---
    ...    *RCR:* _*ALU00000000*_
    ...
    ...    *IR:* _*ALU00000000*_
    ...
    ...    *Author:* Zixiao Zhao
    ...
    ...    *Last Reviser:* Zixiao Zhao
    ...
    ...    ---
    ...    == Implementaion Details ==
    ...    Please reference to : Nokia Lib
    ...    ---
    ...    == Customers ==
    ...
    ...    COMMON
    ...    ---
    ...    == History ==
    ...    _*0:*_ 2018.04.20 --- Zixiao Zhao Created this case.
    ...    [Tags]
    #OLTCONFIG
    #your OLT Configuration#
    #ONTCONFIG
    TriggerInform
    ${node} =    Set Variable    Device.PPP.Interface.1.Name
    #${df} =    Set Variable
    TriggerInform
    #get current value
    @{attribute_list}=    GetParameterAttributesEx    ${node}
    ${org} =    Remove String    @{attribute_list}[0]    ${node}${SPACE}
    #    Should Be Equal    ${org}    ${df}
    #set to 0,1,2
    : FOR    ${i}    IN RANGE    0    3
    \    ${set_attribut_list} =    Set Variable    ${node} true ${i} false x
    \    SetParameterAttributesEx    ${set_attribut_list}
    \    @{attribute_list}=    GetParameterAttributesEx    ${node}
    \    @{ret} =    Split String    ${attribute_list}[0]    ${SPACE}
    \    ${ii} =    Convert To String    ${i}
    \    Should Be Equal    ${ret[1]}    ${ii}
    #Stream
    #your stream machine#

15
    [Documentation]    Attribute readOnly - Device.PPP.Interface.{i}.LastChange
    ...    ---
    ...    == Summary ==
    ...
    ...    *unsignedInt*
    ...
    ...
    ...    The accumulated time in {{units}} since the interface entered its current operational state.
    ...
    ...
    ...
    ...    ---
    ...    *RCR:* _*ALU00000000*_
    ...
    ...    *IR:* _*ALU00000000*_
    ...
    ...    *Author:* Zixiao Zhao
    ...
    ...    *Last Reviser:* Zixiao Zhao
    ...
    ...    ---
    ...    == Implementaion Details ==
    ...    Please reference to : Nokia Lib
    ...    ---
    ...    == Customers ==
    ...
    ...    COMMON
    ...    ---
    ...    == History ==
    ...    _*0:*_ 2018.04.20 --- Zixiao Zhao Created this case.
    ...    [Tags]
    #OLTCONFIG
    #your OLT Configuration#
    #ONTCONFIG
    TriggerInform
    ${node} =    Set Variable    Device.PPP.Interface.1.LastChange
    #${df} =    Set Variable
    TriggerInform
    #get current value
    @{attribute_list}=    GetParameterAttributesEx    ${node}
    ${org} =    Remove String    @{attribute_list}[0]    ${node}${SPACE}
    #    Should Be Equal    ${org}    ${df}
    #set to 0,1,2
    : FOR    ${i}    IN RANGE    0    3
    \    ${set_attribut_list} =    Set Variable    ${node} true ${i} false x
    \    SetParameterAttributesEx    ${set_attribut_list}
    \    @{attribute_list}=    GetParameterAttributesEx    ${node}
    \    @{ret} =    Split String    ${attribute_list}[0]    ${SPACE}
    \    ${ii} =    Convert To String    ${i}
    \    Should Be Equal    ${ret[1]}    ${ii}
    #Stream
    #your stream machine#

16
    [Documentation]    Attribute readWrite - Device.PPP.Interface.{i}.Alias
    ...    ---
    ...    == Summary ==
    ...
    ...    *dataType*
    ...
    ...
    ...    {{datatype|expand}}
    ...
    ...
    ...
    ...    ---
    ...    *RCR:* _*ALU00000000*_
    ...
    ...    *IR:* _*ALU00000000*_
    ...
    ...    *Author:* Zixiao Zhao
    ...
    ...    *Last Reviser:* Zixiao Zhao
    ...
    ...    ---
    ...    == Implementaion Details ==
    ...    Please reference to : Nokia Lib
    ...    ---
    ...    == Customers ==
    ...
    ...    COMMON
    ...    ---
    ...    == History ==
    ...    _*0:*_ 2018.04.20 --- Zixiao Zhao Created this case.
    ...    [Tags]
    #OLTCONFIG
    #your OLT Configuration#
    #ONTCONFIG
    TriggerInform
    ${node} =    Set Variable    Device.PPP.Interface.1.Alias
    #${df} =    Set Variable
    TriggerInform
    #get current value
    @{attribute_list}=    GetParameterAttributesEx    ${node}
    ${org} =    Remove String    @{attribute_list}[0]    ${node}${SPACE}
    #    Should Be Equal    ${org}    ${df}
    #set to 0,1,2
    : FOR    ${i}    IN RANGE    0    3
    \    ${set_attribut_list} =    Set Variable    ${node} true ${i} false x
    \    SetParameterAttributesEx    ${set_attribut_list}
    \    @{attribute_list}=    GetParameterAttributesEx    ${node}
    \    @{ret} =    Split String    ${attribute_list}[0]    ${SPACE}
    \    ${ii} =    Convert To String    ${i}
    \    Should Be Equal    ${ret[1]}    ${ii}
    #Stream
    #your stream machine#

17
    [Documentation]    Attribute readOnly - Device.PPP.Interface.{i}.CurrentMRUSize
    ...    ---
    ...    == Summary ==
    ...
    ...    *unsignedInt*
    ...
    ...
    ...    The current MRU in use over this connection.
    ...
    ...
    ...
    ...    ---
    ...    *RCR:* _*ALU00000000*_
    ...
    ...    *IR:* _*ALU00000000*_
    ...
    ...    *Author:* Zixiao Zhao
    ...
    ...    *Last Reviser:* Zixiao Zhao
    ...
    ...    ---
    ...    == Implementaion Details ==
    ...    Please reference to : Nokia Lib
    ...    ---
    ...    == Customers ==
    ...
    ...    COMMON
    ...    ---
    ...    == History ==
    ...    _*0:*_ 2018.04.20 --- Zixiao Zhao Created this case.
    ...    [Tags]
    #OLTCONFIG
    #your OLT Configuration#
    #ONTCONFIG
    TriggerInform
    ${node} =    Set Variable    Device.PPP.Interface.1.CurrentMRUSize
    #${df} =    Set Variable
    TriggerInform
    #get current value
    @{attribute_list}=    GetParameterAttributesEx    ${node}
    ${org} =    Remove String    @{attribute_list}[0]    ${node}${SPACE}
    #    Should Be Equal    ${org}    ${df}
    #set to 0,1,2
    : FOR    ${i}    IN RANGE    0    3
    \    ${set_attribut_list} =    Set Variable    ${node} true ${i} false x
    \    SetParameterAttributesEx    ${set_attribut_list}
    \    @{attribute_list}=    GetParameterAttributesEx    ${node}
    \    @{ret} =    Split String    ${attribute_list}[0]    ${SPACE}
    \    ${ii} =    Convert To String    ${i}
    \    Should Be Equal    ${ret[1]}    ${ii}
    #Stream
    #your stream machine#

18
    [Documentation]    Attribute readOnly - Device.PPP.Interface.{i}.LCPEcho
    ...    ---
    ...    == Summary ==
    ...
    ...    *unsignedInt*
    ...
    ...
    ...    PPP LCP Echo period in {{units}}.
    ...
    ...
    ...
    ...    ---
    ...    *RCR:* _*ALU00000000*_
    ...
    ...    *IR:* _*ALU00000000*_
    ...
    ...    *Author:* Zixiao Zhao
    ...
    ...    *Last Reviser:* Zixiao Zhao
    ...
    ...    ---
    ...    == Implementaion Details ==
    ...    Please reference to : Nokia Lib
    ...    ---
    ...    == Customers ==
    ...
    ...    COMMON
    ...    ---
    ...    == History ==
    ...    _*0:*_ 2018.04.20 --- Zixiao Zhao Created this case.
    ...    [Tags]
    #OLTCONFIG
    #your OLT Configuration#
    #ONTCONFIG
    TriggerInform
    ${node} =    Set Variable    Device.PPP.Interface.1.LCPEcho
    #${df} =    Set Variable
    TriggerInform
    #get current value
    @{attribute_list}=    GetParameterAttributesEx    ${node}
    ${org} =    Remove String    @{attribute_list}[0]    ${node}${SPACE}
    #    Should Be Equal    ${org}    ${df}
    #set to 0,1,2
    : FOR    ${i}    IN RANGE    0    3
    \    ${set_attribut_list} =    Set Variable    ${node} true ${i} false x
    \    SetParameterAttributesEx    ${set_attribut_list}
    \    @{attribute_list}=    GetParameterAttributesEx    ${node}
    \    @{ret} =    Split String    ${attribute_list}[0]    ${SPACE}
    \    ${ii} =    Convert To String    ${i}
    \    Should Be Equal    ${ret[1]}    ${ii}
    #Stream
    #your stream machine#

19
    [Documentation]    Attribute readOnly - Device.PPP.Interface.{i}.LCPEchoRetry
    ...    ---
    ...    == Summary ==
    ...
    ...    *unsignedInt*
    ...
    ...
    ...    Number of PPP LCP Echo retries within an echo period.
    ...
    ...
    ...
    ...    ---
    ...    *RCR:* _*ALU00000000*_
    ...
    ...    *IR:* _*ALU00000000*_
    ...
    ...    *Author:* Zixiao Zhao
    ...
    ...    *Last Reviser:* Zixiao Zhao
    ...
    ...    ---
    ...    == Implementaion Details ==
    ...    Please reference to : Nokia Lib
    ...    ---
    ...    == Customers ==
    ...
    ...    COMMON
    ...    ---
    ...    == History ==
    ...    _*0:*_ 2018.04.20 --- Zixiao Zhao Created this case.
    ...    [Tags]
    #OLTCONFIG
    #your OLT Configuration#
    #ONTCONFIG
    TriggerInform
    ${node} =    Set Variable    Device.PPP.Interface.1.LCPEchoRetry
    #${df} =    Set Variable
    TriggerInform
    #get current value
    @{attribute_list}=    GetParameterAttributesEx    ${node}
    ${org} =    Remove String    @{attribute_list}[0]    ${node}${SPACE}
    #    Should Be Equal    ${org}    ${df}
    #set to 0,1,2
    : FOR    ${i}    IN RANGE    0    3
    \    ${set_attribut_list} =    Set Variable    ${node} true ${i} false x
    \    SetParameterAttributesEx    ${set_attribut_list}
    \    @{attribute_list}=    GetParameterAttributesEx    ${node}
    \    @{ret} =    Split String    ${attribute_list}[0]    ${SPACE}
    \    ${ii} =    Convert To String    ${i}
    \    Should Be Equal    ${ret[1]}    ${ii}
    #Stream
    #your stream machine#

20
    [Documentation]    Attribute readOnly - Device.PPP.Interface.{i}.Name
    ...    ---
    ...    == Summary ==
    ...
    ...    *string*
    ...
    ...
    ...    The textual name of the interface as assigned by the CPE.
    ...
    ...
    ...
    ...    ---
    ...    *RCR:* _*ALU00000000*_
    ...
    ...    *IR:* _*ALU00000000*_
    ...
    ...    *Author:* Zixiao Zhao
    ...
    ...    *Last Reviser:* Zixiao Zhao
    ...
    ...    ---
    ...    == Implementaion Details ==
    ...    Please reference to : Nokia Lib
    ...    ---
    ...    == Customers ==
    ...
    ...    COMMON
    ...    ---
    ...    == History ==
    ...    _*0:*_ 2018.04.20 --- Zixiao Zhao Created this case.
    ...    [Tags]
    #OLTCONFIG
    #your OLT Configuration#
    #ONTCONFIG
    TriggerInform
    ${node} =    Set Variable    Device.PPP.Interface.1.Name
    #${df} =    Set Variable
    TriggerInform
    #get current value
    @{attribute_list}=    GetParameterAttributesEx    ${node}
    ${org} =    Remove String    @{attribute_list}[0]    ${node}${SPACE}
    #    Should Be Equal    ${org}    ${df}
    #set to 0,1,2
    : FOR    ${i}    IN RANGE    0    3
    \    ${set_attribut_list} =    Set Variable    ${node} true ${i} false x
    \    SetParameterAttributesEx    ${set_attribut_list}
    \    @{attribute_list}=    GetParameterAttributesEx    ${node}
    \    @{ret} =    Split String    ${attribute_list}[0]    ${SPACE}
    \    ${ii} =    Convert To String    ${i}
    \    Should Be Equal    ${ret[1]}    ${ii}
    #Stream
    #your stream machine#

21
    [Documentation]    Attribute readOnly - Device.PPP.Interface.{i}.IPCP.DNSServers
    ...    ---
    ...    == Summary ==
    ...
    ...    *list*
    ...
    ...
    ...    {{list}} Items represent DNS Server IPv4 address(es) received via IPCP {{bibref|RFC1877}}.
    ...
    ...
    ...
    ...    ---
    ...    *RCR:* _*ALU00000000*_
    ...
    ...    *IR:* _*ALU00000000*_
    ...
    ...    *Author:* Zixiao Zhao
    ...
    ...    *Last Reviser:* Zixiao Zhao
    ...
    ...    ---
    ...    == Implementaion Details ==
    ...    Please reference to : Nokia Lib
    ...    ---
    ...    == Customers ==
    ...
    ...    COMMON
    ...    ---
    ...    == History ==
    ...    _*0:*_ 2018.04.20 --- Zixiao Zhao Created this case.
    ...    [Tags]
    #OLTCONFIG
    #your OLT Configuration#
    #ONTCONFIG
    TriggerInform
    ${node} =    Set Variable    Device.PPP.Interface.1.IPCP.DNSServers
    #${df} =    Set Variable
    TriggerInform
    #get current value
    @{attribute_list}=    GetParameterAttributesEx    ${node}
    ${org} =    Remove String    @{attribute_list}[0]    ${node}${SPACE}
    #    Should Be Equal    ${org}    ${df}
    #set to 0,1,2
    : FOR    ${i}    IN RANGE    0    3
    \    ${set_attribut_list} =    Set Variable    ${node} true ${i} false x
    \    SetParameterAttributesEx    ${set_attribut_list}
    \    @{attribute_list}=    GetParameterAttributesEx    ${node}
    \    @{ret} =    Split String    ${attribute_list}[0]    ${SPACE}
    \    ${ii} =    Convert To String    ${i}
    \    Should Be Equal    ${ret[1]}    ${ii}
    #Stream
    #your stream machine#

22
    [Documentation]    Attribute readOnly - Device.PPP.Interface.{i}.IPCP.LocalIPAddress
    ...    ---
    ...    == Summary ==
    ...
    ...    *dataType*
    ...
    ...
    ...    The local IPv4 address for this connection received via IPCP.
    ...
    ...
    ...
    ...    ---
    ...    *RCR:* _*ALU00000000*_
    ...
    ...    *IR:* _*ALU00000000*_
    ...
    ...    *Author:* Zixiao Zhao
    ...
    ...    *Last Reviser:* Zixiao Zhao
    ...
    ...    ---
    ...    == Implementaion Details ==
    ...    Please reference to : Nokia Lib
    ...    ---
    ...    == Customers ==
    ...
    ...    COMMON
    ...    ---
    ...    == History ==
    ...    _*0:*_ 2018.04.20 --- Zixiao Zhao Created this case.
    ...    [Tags]
    #OLTCONFIG
    #your OLT Configuration#
    #ONTCONFIG
    TriggerInform
    ${node} =    Set Variable    Device.PPP.Interface.1.IPCP.LocalIPAddress
    #${df} =    Set Variable
    TriggerInform
    #get current value
    @{attribute_list}=    GetParameterAttributesEx    ${node}
    ${org} =    Remove String    @{attribute_list}[0]    ${node}${SPACE}
    #    Should Be Equal    ${org}    ${df}
    #set to 0,1,2
    : FOR    ${i}    IN RANGE    0    3
    \    ${set_attribut_list} =    Set Variable    ${node} true ${i} false x
    \    SetParameterAttributesEx    ${set_attribut_list}
    \    @{attribute_list}=    GetParameterAttributesEx    ${node}
    \    @{ret} =    Split String    ${attribute_list}[0]    ${SPACE}
    \    ${ii} =    Convert To String    ${i}
    \    Should Be Equal    ${ret[1]}    ${ii}
    #Stream
    #your stream machine#

23
    [Documentation]    Attribute readOnly - Device.PPP.Interface.{i}.IPCP.RemoteIPAddress
    ...    ---
    ...    == Summary ==
    ...
    ...    *dataType*
    ...
    ...
    ...    The remote IPv4 address for this connection received via IPCP.
    ...
    ...
    ...
    ...    ---
    ...    *RCR:* _*ALU00000000*_
    ...
    ...    *IR:* _*ALU00000000*_
    ...
    ...    *Author:* Zixiao Zhao
    ...
    ...    *Last Reviser:* Zixiao Zhao
    ...
    ...    ---
    ...    == Implementaion Details ==
    ...    Please reference to : Nokia Lib
    ...    ---
    ...    == Customers ==
    ...
    ...    COMMON
    ...    ---
    ...    == History ==
    ...    _*0:*_ 2018.04.20 --- Zixiao Zhao Created this case.
    ...    [Tags]
    #OLTCONFIG
    #your OLT Configuration#
    #ONTCONFIG
    TriggerInform
    ${node} =    Set Variable    Device.PPP.Interface.1.IPCP.RemoteIPAddress
    #${df} =    Set Variable
    TriggerInform
    #get current value
    @{attribute_list}=    GetParameterAttributesEx    ${node}
    ${org} =    Remove String    @{attribute_list}[0]    ${node}${SPACE}
    #    Should Be Equal    ${org}    ${df}
    #set to 0,1,2
    : FOR    ${i}    IN RANGE    0    3
    \    ${set_attribut_list} =    Set Variable    ${node} true ${i} false x
    \    SetParameterAttributesEx    ${set_attribut_list}
    \    @{attribute_list}=    GetParameterAttributesEx    ${node}
    \    @{ret} =    Split String    ${attribute_list}[0]    ${SPACE}
    \    ${ii} =    Convert To String    ${i}
    \    Should Be Equal    ${ret[1]}    ${ii}
    #Stream
    #your stream machine#
