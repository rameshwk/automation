*** Settings ***
Documentation     This suite is designed to match VFSP TR098 requirements from HDR58.
Suite Setup       SuiteSetup
Suite Teardown    SuiteTeardown
Test Teardown     CloseClient
Default Tags      G240WC    VFSP    CHENNAI
Resource          %{ROBOTREPO}/HLK/res/l2-tr069.txt
Resource          %{ROBOTREPO}/HLK/res/l3-tr069.txt
Resource          %{ROBOTREPO}/HLK/res/l3-selenium2.txt

*** Test Cases ***
01
    [Documentation]    \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ --------------------------------------------------------------------------------------------
    ...    \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ | \ \ TestCase \ \ Mnemonic \ : \ HGU_ADDITIONAL_TR098_117,133 \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ |
    ...    \ \ \ \ \ \ \ \ \ \ \ \ \ \ | \ \ RCR: \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ ALU02367961 \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ |
    ...    \ \ \ \ \ \ \ \ \ \ \ \ \ --------------------------------------------------------------------------------------------
    ...    \ \ \ \ \ \ \ \ \ \
    log    Starting TestCase HGU_ADDITONAL_TR098_117,133
    log    --------------------------------------------------
    TriggerInform
    log    The default Enable paramter of WAN IP Connection should be true, se it to false and then revert it back to true
    @{parameter_list}=    GetParameterValuesEx    ${cpe_tr069_interface}.Enable
    Should Contain    @{parameter_list}    true
    ${status}    SetParameterValuesEx    ${cpe_tr069_interface}.Enable true
    Should Be Equal    ${status}    0
    @{parameter_list}=    GetParameterValuesEx    ${cpe_tr069_interface}.Enable
    Should Contain    @{parameter_list}    true
    log    DO the same steps for WAN PPP CONNECTION
    @{parameter_list}=    GetParameterValuesEx    ${cpe_internet_interface}.Enable
    Should Contain    @{parameter_list}    true
    ${status}    SetParameterValuesEx    ${cpe_internet_interface}.Enable false
    Should Be Equal    ${status}    0
    @{parameter_list}=    GetParameterValuesEx    ${cpe_internet_interface}.Enable
    Should Contain    @{parameter_list}    false
    ${status}    SetParameterValuesEx    ${cpe_internet_interface}.Enable true
    Should Be Equal    ${status}    0
    @{parameter_list}=    GetParameterValuesEx    ${cpe_internet_interface}.Enable
    Should Contain    @{parameter_list}    true

02
    [Documentation]    \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ --------------------------------------------------------------------------------------------
    ...    \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ | \ \ TestCase \ \ Mnemonic \ : \ HGU_ADDITIONAL_TR098_118,134 \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ |
    ...    \ \ \ \ \ \ \ \ \ \ \ \ \ \ | \ \ RCR: \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ ALU02367961 \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ |
    ...    \ \ \ \ \ \ \ \ \ \ \ \ \ --------------------------------------------------------------------------------------------
    ...    \ \ \ \ \ \ \ \ \ \
    LOG    Starting Testcase HGU_ADDITIONAL_TR098_118,HGU_ADDITIONAL_TR098_134
    log    -------------------------------------------------
    TriggerInform
    @{parameter_list}=    GetParameterValuesEx    ${cpe_tr069_interface}.Reset
    log    @{parameter_list}
    ${status}    SetParameterValuesEx    ${cpe_tr069_interface}.Reset false
    Should Be Equal    ${status}    0
    @{parameter_list}=    GetParameterValuesEx    ${cpe_tr069_interface}.Reset
    Should Contain    @{parameter_list}    false
    @{parameter_list}=    GetParameterValuesEx    ${cpe_tr069_interface}.Reset
    Should Contain    @{parameter_list}    false
    @{parameter_list}=    GetParameterValuesEx    ${cpe_internet_interface}.Reset
    log    @{parameter_list}
    ${status}    SetParameterValuesEx    ${cpe_internet_interface}.Reset false
    Should Be Equal    ${status}    0
    @{parameter_list}=    GetParameterValuesEx    ${cpe_internet_interface}.Reset
    Should Contain    @{parameter_list}    false

03
    [Documentation]    \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ --------------------------------------------------------------------------------------------
    ...    \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ | \ \ TestCase \ \ Mnemonic \ : \ HGU_ADDITIONAL_TR098_119_135_136 \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ |
    ...    \ \ \ \ \ \ \ \ \ \ \ \ \ \ | \ \ RCR: \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ ALU02367961 \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ |
    ...    \ \ \ \ \ \ \ \ \ \ \ \ \ --------------------------------------------------------------------------------------------
    ...    \ \ \ \ \ \ \ \ \ \
    log    Starting Testcase HGU_ADDITIONAL_TR098_119_135_136
    log    -----------------------------------------------
    TriggerInform
    @{parameter_list}=    GetParameterValuesEx    ${cpe_internet_interface}.ConnectionStatus
    log    @{parameter_list}
    Should Contain    @{parameter_list}    Connected
    @{parameter_list}=    GetParameterValuesEx    ${cpe_tr069_interface}.ConnectionStatus
    log    @{parameter_list}
    Should Contain    @{parameter_list}    Connected
    @{parameter_list}=    GetParameterValuesEx    ${cpe_internet_interface}.ConnectionType
    log    @{parameter_list}
    Should Contain    @{parameter_list}    IP_Routed
    @{parameter_list}=    GetParameterValuesEx    ${cpe_tr069_interface}.ConnectionType
    log    @{parameter_list}
    Should Contain    @{parameter_list}    IP_Routed
    ${status}    SetParameterValuesEx    ${cpe_internet_interface}.ConnectionType IP_Routed
    Should Be Equal    ${status}    0
    @{parameter_list}=    GetParameterValuesEx    ${cpe_internet_interface}.ConnectionType
    Should Contain    @{parameter_list}    IP_Routed
    ${status}    SetParameterValuesEx    ${cpe_tr069_interface}.ConnectionType IP_Routed
    Should Be Equal    ${status}    0
    @{parameter_list}=    GetParameterValuesEx    ${cpe_tr069_interface}.ConnectionType
    Should Contain    @{parameter_list}    IP_Routed

04
    [Documentation]    \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ --------------------------------------------------------------------------------------------
    ...    \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ | \ \ TestCase \ \ Mnemonic \ : \ HGU_ADDITIONAL_TR098_120_137 \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ |
    ...    \ \ \ \ \ \ \ \ \ \ \ \ \ \ | \ \ RCR: \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ ALU02367961 \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ |
    ...    \ \ \ \ \ \ \ \ \ \ \ \ \ --------------------------------------------------------------------------------------------
    ...    \ \ \ \ \ \ \ \ \ \
    log    Starting TestCase HGU_TR098_120_137
    log    -------------------------------------------------
    TriggerInform
    @{parameter_list}=    GetParameterValuesEx    ${cpe_internet_interface}.Name
    log    @{parameter_list}
    Should Contain    @{parameter_list}    INTERNET
    @{parameter_list}=    GetParameterValuesEx    ${cpe_tr069_interface}.Name
    log    @{parameter_list}
    Should Contain    @{parameter_list}    TR069

05
    [Documentation]    \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ --------------------------------------------------------------------------------------------
    ...    \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ | \ \ TestCase \ \ Mnemonic \ : \ HGU_ADDITIONAL_TR098_121_125_141 \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ |
    ...    \ \ \ \ \ \ \ \ \ \ \ \ \ \ | \ \ RCR: \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ ALU02367961 \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ |
    ...    \ \ \ \ \ \ \ \ \ \ \ \ \ --------------------------------------------------------------------------------------------
    ...    \ \ \ \ \ \ \ \ \ \
    log    Starting TestCase HGU_ADDITIONAL_TR098_121_125_141
    log    -----------------------------------------------------
    TriggerInform
    @{parameter_list}=    GetParameterValuesEx    ${cpe_internet_interface}.NATEnabled
    log    @{parameter_list}
    Should Contain    @{parameter_list}    true
    @{parameter_list}=    GetParameterValuesEx    ${cpe_tr069_interface}.NATEnabled
    log    @{parameter_list}
    ${status}    SetParameterValuesEx    ${cpe_internet_interface}.NATEnabled true
    Should Be Equal    ${status}    0
    @{parameter_list}=    GetParameterValuesEx    ${cpe_internet_interface}.NATEnabled
    log    @{parameter_list}
    Should Contain    @{parameter_list}    true
    @{parameter_list}=    GetParameterValuesEx    ${cpe_internet_interface}.DNSServers
    log    @{parameter_list}
    @{parameter_list}=    GetParameterValuesEx    ${cpe_tr069_interface}.DNSServers
    log    @{parameter_list}

06
    [Documentation]    \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ --------------------------------------------------------------------------------------------
    ...    \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ | \ \ TestCase \ \ Mnemonic \ : \ HGU_ADDITIONAL_TR098_122_123_124 \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ |
    ...    \ \ \ \ \ \ \ \ \ \ \ \ \ \ | \ \ RCR: \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ ALU02367961 \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ |
    ...    \ \ \ \ \ \ \ \ \ \ \ \ \ --------------------------------------------------------------------------------------------
    ...    \ \ \ \ \ \ \ \ \ \
    log    Starting TestCase HGU_ADDITIONAL_TR098_122_123_124
    log    ------------------------------------------------
    TriggerInform
    @{parameter_list}=    GetParameterValuesEx    ${cpe_tr069_interface}.ExternalIPAddress
    Should Contain    @{parameter_list}    10.18
    @{parameter_list}=    GetParameterValuesEx    ${cpe_tr069_interface}.SubnetMask
    Should Contain    @{parameter_list}    255.255.255.0
    @{parameter_list}=    GetParameterValuesEx    ${cpe_tr069_interface}.DefaultGateway
    Should Contain    @{parameter_list}    10.
    ${status}    SetParameterValuesEx    ${cpe_tr069_interface}.SubnetMask 255.255.255.0
    Should Be Equal    ${status}    0
    @{parameter_list}=    GetParameterValuesEx    ${cpe_tr069_interface}.SubnetMask
    Should Contain    @{parameter_list}    255.255.255.0

07
    [Documentation]    \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ --------------------------------------------------------------------------------------------
    ...    \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ | \ \ TestCase \ \ Mnemonic \ : \ HGU_ADDITIONAL_TR098_126_143 \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ |
    ...    \ \ \ \ \ \ \ \ \ \ \ \ \ \ | \ \ RCR: \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ ALU02367961 \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ |
    ...    \ \ \ \ \ \ \ \ \ \ \ \ \ --------------------------------------------------------------------------------------------
    ...    \ \ \ \ \ \ \ \ \ \
    log    Starting TestCase HGU_ADDITIONAL_TR098_126_143
    log    ---------------------------------------------------
    TriggerInform
    @{parameter_list}=    GetParameterValuesEx    ${cpe_internet_interface}.PortMappingNumberOfEntries
    log    @{parameter_list}
    Should Contain    @{parameter_list}    0
    @{parameter_list}=    GetParameterValuesEx    ${cpe_tr069_interface}.PortMappingNumberOfEntries
    log    @{parameter_list}
    Should Contain    @{parameter_list}    0

08
    [Documentation]    \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ --------------------------------------------------------------------------------------------
    ...    \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ | \ \ TestCase \ \ Mnemonic \ : \ HGU_ADDITIONAL_TR098_138_139 \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ |
    ...    \ \ \ \ \ \ \ \ \ \ \ \ \ \ | \ \ RCR: \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ ALU02367961 \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ |
    ...    \ \ \ \ \ \ \ \ \ \ \ \ \ --------------------------------------------------------------------------------------------
    ...    \ \ \ \ \ \ \ \ \ \
    log    Starting TestCase HGU_ADDITIONAL_TR098_138_139
    log    ---------------------------------------------------
    TriggerInform
    @{parameter_list}=    GetParameterValuesEx    ${cpe_internet_interface}.Username
    Should Contain    @{parameter_list}    atc
    ${status}    SetParameterValuesEx    ${cpe_internet_interface}.Username atc
    Should Be Equal    ${status}    0
    @{parameter_list}=    GetParameterValuesEx    ${cpe_internet_interface}.Username
    Should Contain    @{parameter_list}    atc
    @{parameter_list}=    GetParameterValuesEx    ${cpe_internet_interface}.Password

09
    [Documentation]    \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ --------------------------------------------------------------------------------------------
    ...    \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ | \ \ TestCase \ \ Mnemonic \ : \ HGU_ADDITIONAL_TR098_129_144 \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ |
    ...    \ \ \ \ \ \ \ \ \ \ \ \ \ \ | \ \ RCR: \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ ALU02367961 \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ |
    ...    \ \ \ \ \ \ \ \ \ \ \ \ \ --------------------------------------------------------------------------------------------
    ...    \ \ \ \ \ \ \ \ \ \
    log    Start TestCase HGU_ADDITIONAL_TR098_144_129
    log    --------------------------------------------
    TriggerInform
    @{parameter_list}=    GetParameterValuesEx    InternetGatewayDevice.WANDevice.1.WANConnectionDevice.2.WANIPConnection.1.Stats.EthernetBytesReceived
    log    @{parameter_list}
    TriggerInform
    Set Request Body    <?xml version='1.0'?> <SOAP-ENV:Envelope xmlns:xsi="http://www.w3.org/1999/XMLSchema-instance" xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:SOAP-ENC="http://schemas.xmlsoap.org/soap/encoding/" xmlns:cwmp="urn:dslforum-org:cwmp-1-0" xmlns:xsd="http://www.w3.org/1999/XMLSchema"><SOAP-ENV:Body><SetParameterValuesEx><ParameterList>InternetGatewayDevice.WANDevice.1.WANConnectionDevice.2.WANIPConnection.1.Stats.EthernetBytesReceived 10</ParameterList><ParameterKey>3</ParameterKey></SetParameterValuesEx></SOAP-ENV:Body></SOAP-ENV:Envelope>
    Set Request Header    Content-Type    text/xml
    Set Request Header    SOAPAction    "SetParameterValuesEx"
    POST    /ACS
    Response Status Code Should Equal    200
    ${body}=    Get Response Body
    log    Make sure the Error Code returned is a FaultCode with "non-writable" parameter
    Should Contain    ${body}    FaultCode 9003
    Should Contain    ${body}    non-writable parameter
    TriggerInform
    Set Request Body    <?xml version='1.0'?> <SOAP-ENV:Envelope xmlns:xsi="http://www.w3.org/1999/XMLSchema-instance" xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:SOAP-ENC="http://schemas.xmlsoap.org/soap/encoding/" xmlns:cwmp="urn:dslforum-org:cwmp-1-0" xmlns:xsd="http://www.w3.org/1999/XMLSchema"><SOAP-ENV:Body><SetParameterValuesEx><ParameterList>InternetGatewayDevice.WANDevice.1.WANConnectionDevice.1.WANPPPConnection.1.Stats.EthernetBytesReceived 10</ParameterList><ParameterKey>3</ParameterKey></SetParameterValuesEx></SOAP-ENV:Body></SOAP-ENV:Envelope>
    Set Request Header    Content-Type    text/xml
    Set Request Header    SOAPAction    "SetParameterValuesEx"
    POST    /ACS
    Response Status Code Should Equal    200
    ${body}=    Get Response Body
    log    Make sure the Error Code returned is a FaultCode with "non-writable" parameter
    Should Contain    ${body}    FaultCode 9003
    Should Contain    ${body}    non-writable parameter

10
    [Documentation]    \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ --------------------------------------------------------------------------------------------
    ...    \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ | \ \ TestCase \ \ Mnemonic \ : \ HGU_ADDITIONAL_TR098_130_145 \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ |
    ...    \ \ \ \ \ \ \ \ \ \ \ \ \ \ | \ \ RCR: \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ ALU02367961 \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ |
    ...    \ \ \ \ \ \ \ \ \ \ \ \ \ --------------------------------------------------------------------------------------------
    ...    \ \ \ \ \ \ \ \ \ \
    log    Start TestCase HGU_ADDITIONAL_TR098_130_145
    log    --------------------------------------------
    TriggerInform
    @{parameter_list}=    GetParameterValuesEx    InternetGatewayDevice.WANDevice.1.WANConnectionDevice.2.WANIPConnection.1.Stats.EthernetPacketsReceived
    log    @{parameter_list}
    TriggerInform
    Set Request Body    <?xml version='1.0'?> <SOAP-ENV:Envelope xmlns:xsi="http://www.w3.org/1999/XMLSchema-instance" xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:SOAP-ENC="http://schemas.xmlsoap.org/soap/encoding/" xmlns:cwmp="urn:dslforum-org:cwmp-1-0" xmlns:xsd="http://www.w3.org/1999/XMLSchema"><SOAP-ENV:Body><SetParameterValuesEx><ParameterList>InternetGatewayDevice.WANDevice.1.WANConnectionDevice.2.WANIPConnection.1.Stats.EthernetPacketsReceived 10</ParameterList><ParameterKey>3</ParameterKey></SetParameterValuesEx></SOAP-ENV:Body></SOAP-ENV:Envelope>
    Set Request Header    Content-Type    text/xml
    Set Request Header    SOAPAction    "SetParameterValuesEx"
    POST    /ACS
    Response Status Code Should Equal    200
    ${body}=    Get Response Body
    log    Make sure the Error Code returned is a FaultCode with "non-writable" parameter
    Should Contain    ${body}    FaultCode 9003
    Should Contain    ${body}    non-writable parameter
    TriggerInform
    Set Request Body    <?xml version='1.0'?> <SOAP-ENV:Envelope xmlns:xsi="http://www.w3.org/1999/XMLSchema-instance" xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:SOAP-ENC="http://schemas.xmlsoap.org/soap/encoding/" xmlns:cwmp="urn:dslforum-org:cwmp-1-0" xmlns:xsd="http://www.w3.org/1999/XMLSchema"><SOAP-ENV:Body><SetParameterValuesEx><ParameterList>InternetGatewayDevice.WANDevice.1.WANConnectionDevice.1.WANPPPConnection.1.Stats.EthernetPacketsReceived 10</ParameterList><ParameterKey>3</ParameterKey></SetParameterValuesEx></SOAP-ENV:Body></SOAP-ENV:Envelope>
    Set Request Header    Content-Type    text/xml
    Set Request Header    SOAPAction    "SetParameterValuesEx"
    POST    /ACS
    Response Status Code Should Equal    200
    ${body}=    Get Response Body
    log    Make sure the Error Code returned is a FaultCode with "non-writable" parameter
    Should Contain    ${body}    FaultCode 9003
    Should Contain    ${body}    non-writable parameter

11
    [Documentation]    \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ --------------------------------------------------------------------------------------------
    ...    \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ | \ \ TestCase \ \ Mnemonic \ : \ HGU_ADDITIONAL_TR098_131_146 \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ |
    ...    \ \ \ \ \ \ \ \ \ \ \ \ \ \ | \ \ RCR: \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ ALU02367961 \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ |
    ...    \ \ \ \ \ \ \ \ \ \ \ \ \ --------------------------------------------------------------------------------------------
    ...    \ \ \ \ \ \ \ \ \ \
    log    Start TestCase HGU_ADDITIONAL_TR098_131_146
    log    --------------------------------------------
    TriggerInform
    @{parameter_list}=    GetParameterValuesEx    InternetGatewayDevice.WANDevice.1.WANConnectionDevice.2.WANIPConnection.1.Stats.EthernetBytesSent
    log    @{parameter_list}
    TriggerInform
    Set Request Body    <?xml version='1.0'?> <SOAP-ENV:Envelope xmlns:xsi="http://www.w3.org/1999/XMLSchema-instance" xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:SOAP-ENC="http://schemas.xmlsoap.org/soap/encoding/" xmlns:cwmp="urn:dslforum-org:cwmp-1-0" xmlns:xsd="http://www.w3.org/1999/XMLSchema"><SOAP-ENV:Body><SetParameterValuesEx><ParameterList>InternetGatewayDevice.WANDevice.1.WANConnectionDevice.2.WANIPConnection.1.Stats.EthernetBytesSent 10</ParameterList><ParameterKey>3</ParameterKey></SetParameterValuesEx></SOAP-ENV:Body></SOAP-ENV:Envelope>
    Set Request Header    Content-Type    text/xml
    Set Request Header    SOAPAction    "SetParameterValuesEx"
    POST    /ACS
    Response Status Code Should Equal    200
    ${body}=    Get Response Body
    log    Make sure the Error Code returned is a FaultCode with "non-writable" parameter
    Should Contain    ${body}    FaultCode 9003
    Should Contain    ${body}    non-writable parameter
    TriggerInform
    Set Request Body    <?xml version='1.0'?> <SOAP-ENV:Envelope xmlns:xsi="http://www.w3.org/1999/XMLSchema-instance" xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:SOAP-ENC="http://schemas.xmlsoap.org/soap/encoding/" xmlns:cwmp="urn:dslforum-org:cwmp-1-0" xmlns:xsd="http://www.w3.org/1999/XMLSchema"><SOAP-ENV:Body><SetParameterValuesEx><ParameterList>InternetGatewayDevice.WANDevice.1.WANConnectionDevice.1.WANPPPConnection.1.Stats.EthernetBytesSent 10</ParameterList><ParameterKey>3</ParameterKey></SetParameterValuesEx></SOAP-ENV:Body></SOAP-ENV:Envelope>
    Set Request Header    Content-Type    text/xml
    Set Request Header    SOAPAction    "SetParameterValuesEx"
    POST    /ACS
    Response Status Code Should Equal    200
    ${body}=    Get Response Body
    log    Make sure the Error Code returned is a FaultCode with "non-writable" parameter
    Should Contain    ${body}    FaultCode 9003
    Should Contain    ${body}    non-writable parameter

12
    [Documentation]    \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ --------------------------------------------------------------------------------------------
    ...    \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ | \ \ TestCase \ \ Mnemonic \ : \ HGU_ADDITIONAL_TR098_142 \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ |
    ...    \ \ \ \ \ \ \ \ \ \ \ \ \ \ | \ \ RCR: \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ ALU02367961 \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ |
    ...    \ \ \ \ \ \ \ \ \ \ \ \ \ --------------------------------------------------------------------------------------------
    ...    \ \ \ \ \ \ \ \ \ \
    log    Starting TestCase HGU_ADDITIONAL_TR098_142
    log    ----------------------------------------------
    TriggerInform
    @{parameter_list}=    GetParameterValuesEx    ${cpe_internet_interface}.ConnectionTrigger
    Should Contain    @{parameter_list}    AlwaysOn

13
    [Documentation]    \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ --------------------------------------------------------------------------------------------
    ...    \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ | \ \ TestCase \ \ Mnemonic \ : \ HGU_ADDITIONAL_TR098_132_147 \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ |
    ...    \ \ \ \ \ \ \ \ \ \ \ \ \ \ | \ \ RCR: \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ ALU02367961 \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ |
    ...    \ \ \ \ \ \ \ \ \ \ \ \ \ --------------------------------------------------------------------------------------------
    ...    \ \ \ \ \ \ \ \ \ \
    log    Start TestCase HGU_ADDITIONAL_TR098_132_147
    log    --------------------------------------------
    TriggerInform
    @{parameter_list}=    GetParameterValuesEx    InternetGatewayDevice.WANDevice.1.WANConnectionDevice.2.WANIPConnection.1.Stats.EthernetPacketsSent
    log    @{parameter_list}
    TriggerInform
    Set Request Body    <?xml version='1.0'?> <SOAP-ENV:Envelope xmlns:xsi="http://www.w3.org/1999/XMLSchema-instance" xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:SOAP-ENC="http://schemas.xmlsoap.org/soap/encoding/" xmlns:cwmp="urn:dslforum-org:cwmp-1-0" xmlns:xsd="http://www.w3.org/1999/XMLSchema"><SOAP-ENV:Body><SetParameterValuesEx><ParameterList>InternetGatewayDevice.WANDevice.1.WANConnectionDevice.2.WANIPConnection.1.Stats.EthernetPacketsSent 10</ParameterList><ParameterKey>3</ParameterKey></SetParameterValuesEx></SOAP-ENV:Body></SOAP-ENV:Envelope>
    Set Request Header    Content-Type    text/xml
    Set Request Header    SOAPAction    "SetParameterValuesEx"
    POST    /ACS
    Response Status Code Should Equal    200
    ${body}=    Get Response Body
    log    Make sure the Error Code returned is a FaultCode with "non-writable" parameter
    Should Contain    ${body}    FaultCode 9003
    Should Contain    ${body}    non-writable parameter
    TriggerInform
    Set Request Body    <?xml version='1.0'?> <SOAP-ENV:Envelope xmlns:xsi="http://www.w3.org/1999/XMLSchema-instance" xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:SOAP-ENC="http://schemas.xmlsoap.org/soap/encoding/" xmlns:cwmp="urn:dslforum-org:cwmp-1-0" xmlns:xsd="http://www.w3.org/1999/XMLSchema"><SOAP-ENV:Body><SetParameterValuesEx><ParameterList>InternetGatewayDevice.WANDevice.1.WANConnectionDevice.1.WANPPPConnection.1.Stats.EthernetPacketsSent 10</ParameterList><ParameterKey>3</ParameterKey></SetParameterValuesEx></SOAP-ENV:Body></SOAP-ENV:Envelope>
    Set Request Header    Content-Type    text/xml
    Set Request Header    SOAPAction    "SetParameterValuesEx"
    POST    /ACS
    Response Status Code Should Equal    200
    ${body}=    Get Response Body
    log    Make sure the Error Code returned is a FaultCode with "non-writable" parameter
    Should Contain    ${body}    FaultCode 9003
    Should Contain    ${body}    non-writable parameter

14
    [Documentation]    \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ --------------------------------------------------------------------------------------------
    ...    \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ | \ \ TestCase \ \ Mnemonic \ : \ HGU_ADDITIONAL_TR098_140 \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ |
    ...    \ \ \ \ \ \ \ \ \ \ \ \ \ \ | \ \ RCR: \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ ALU02367961 \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ |
    ...    \ \ \ \ \ \ \ \ \ \ \ \ \ --------------------------------------------------------------------------------------------
    ...    \ \ \ \ \ \ \ \ \ \
    log    Starting TestCase HGU_ADDITIONAL_TR098_140
    log    ---------------------------------------------------
    TriggerInform
    @{parameter_list}=    GetParameterValuesEx    ${cpe_internet_interface}.DNSEnabled
    Should Contain    @{parameter_list}    true

15
    [Documentation]    \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ --------------------------------------------------------------------------------------------
    ...    \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ | \ \ TestCase \ \ Mnemonic \ : \ HGU_ADDITIONAL_TR098_127_128 \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ |
    ...    \ \ \ \ \ \ \ \ \ \ \ \ \ \ | \ \ RCR: \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ ALU02367961 \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ |
    ...    \ \ \ \ \ \ \ \ \ \ \ \ \ --------------------------------------------------------------------------------------------
    ...    \ \ \ \ \ \ \ \ \ \
    log    Starting Testcase HGU_ADDITIONAL_TR098_127_128
    log    -----------------------------------------------------
    TriggerInform
    @{parameter_list}=    GetParameterValuesEx    InternetGatewayDevice.WANDevice.1.WANConnectionDevice.2.WANIPConnection.1.DHCPClient.SentDHCPOption.1.Tag
    log    @{parameter_list}
    @{parameter_list}=    GetParameterValuesEx    InternetGatewayDevice.WANDevice.1.WANConnectionDevice.2.WANIPConnection.1.DHCPClient.SentDHCPOption.1.Value
    log    @{parameter_list}
    Should Contain    @{parameter_list}    {}

*** Keywords ***
SuiteSetup
    Should Be Equal As Strings    ${startup_status}    PASS
    Create Http Context    localhost:8015
    TriggerInform
    ${path}=    Set Variable    InternetGatewayDevice.WANDevice.1.
    Set Suite Variable    ${path}    ${path}
    ${path1}=    Set Variable    ${path}WANCommonInterfaceConfig.
    Set Suite Variable    ${path1}    ${path1}
    ${service_changed}=    Set Variable    False
    Set Suite Variable    ${service_changed}    ${service_changed}
    ${case39_status}=    Set Variable    False
    Set Suite Variable    ${case39_status}
    # 20170526 Alfred: This suite variable is a flag that used for test 33 ~ test 43, when its value is ${True} these tests will do setup , or they will skip the setup.
    Set Suite Variable    ${f_test_need_setup}    ${True}
    Set Suite Variable    ${names_writeable}    ${None}
    Set Suite Variable    ${name_value_pairs}    ${None}
    # This suite variable is used for convinient of using keyword get_node_value
    Set Suite Variable    ${FROM_LIST}    ${name_value_pairs}
    Set Suite Variable    ${cpe_internet_interface}    InternetGatewayDevice.WANDevice.1.WANConnectionDevice.1.WANPPPConnection.1
    Set Suite Variable    ${cpe_tr069_interface}    InternetGatewayDevice.WANDevice.1.WANConnectionDevice.2.WANIPConnection.1

SuiteTeardown
    Pass Execution If    '${service_changed}'=='False'    didn't create new IPoE WAN
    l3-selenium2.LoginGUI    ${ont['gui_type']}    ${ont['url_protocol']}://${ont['ip']}    ${ont['gui_username']}    ${ont['gui_password']}
    Run Keyword and Ignore Error    l3-selenium2.DeleteSpecConnection    ${ont['gui_type']}    ${ont['url_protocol']}://${ont['ip']}    StaticWAN
    Run Keyword and Ignore Error    l3-selenium2.DeleteSpecConnection    ${ont['gui_type']}    ${ont['url_protocol']}://${ont['ip']}    1111
    l3-selenium2.LogoutGUI    ${ont['gui_type']}    ${ont['url_protocol']}://${ont['ip']}
    TriggerInform
    # GET WAN INTERNET Interface
    ${cpe_internet_interface}=    SearchWANConnection    INTERNET
    # GET WAN INTERNET IP
    @{parameter_list}=    GetParameterValuesEx    ${cpe_internet_interface}.ExternalIPAddress
    ${cpe_internet_ip}=    Fetch From Right    @{parameter_list}[0]    ${SPACE}
    # GET WAN TR069 Interface
    ${cpe_tr069_interface}=    SearchWANConnection    TR069
    # GET WAN tr069 IP
    @{parameter_list}=    GetParameterValuesEx    ${cpe_tr069_interface}.ExternalIPAddress
    ${cpe_tr069_ip}=    Fetch From Right    @{parameter_list}[0]    ${SPACE}
    CloseClient
    # SET Global Variables
    Set Global Variable    ${cpe_internet_interface}    ${cpe_internet_interface}
    Set Global Variable    ${cpe_internet_ip}    ${cpe_internet_ip}
    Set Global Variable    ${cpe_tr069_interface}    ${cpe_tr069_interface}
    Set Global Variable    ${cpe_tr069_ip}    ${cpe_tr069_ip}
    [Teardown]    EnableRecovery    1

CreateIPoEWAN
    Pass Execution If    '${service_changed}'=='True'    new IPoE WAN already created
    l3-selenium2.LoginGUI    ${ont['gui_type']}    ${ont['url_protocol']}://${ont['ip']}    ${ont['gui_username']}    ${ont['gui_password']}
    l3-selenium2.CreateDHCPConnection    ${ont['gui_type']}    ${ont['url_protocol']}://${ont['ip']}    1111    IPTV
    l3-selenium2.LogoutGUI    ${ont['gui_type']}    ${ont['url_protocol']}://${ont['ip']}
    TriggerInform
    Comment    #search INTERNET wan interface
    Comment    ${internet_interface}=    SearchWANConnection    INTERNET
    #search OTHER wan interface
    ${iptv_interface}=    SearchWANConnection    OTHER
    Comment    #searcg TR069 wan interface
    Comment    ${tr069_interface}=    SearchWANConnection    TR069
    closeclient
    Comment    Set Suite Variable    ${internet_interface}    ${internet_interface}
    Set Suite Variable    ${iptv_interface}    ${iptv_interface}
    Comment    Set Suite Variable    ${tr069_interface}    ${tr069_interface}
    ${pc_wan_mac}=    Remove String    ${pc['wan_mac']}    :
    Set Suite Variable    ${pc_wan_mac}    ${pc_wan_mac}
    ${service_changed}=    Set Variable    True
    Set Suite Variable    ${service_changed}    ${service_changed}

setup_t61
    RunKeywordIf    ${f_test_need_setup}    init_obj    InternetGatewayDevice.WANDevice.1.

teardown_t61
    [Arguments]    ${node}    ${v}
    # restore the target parameter
    SetParameterValuesEx    ${node} ${v}
    # reset_flag
    # Run Keyword If Test Passed    set suite variable    ${f_test_need_setup}    ${false}
    close client
