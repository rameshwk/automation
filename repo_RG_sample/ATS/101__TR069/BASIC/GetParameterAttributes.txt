*** Settings ***
Suite Setup       SuiteSetup
Test Teardown     CloseClient
Default Tags      COMMON
Resource          %{ROBOTREPO}/HLK/res/l2-tr069.txt
Library           HttpLibrary.HTTP
Resource          %{ROBOTREPO}/HLK/res/l3-tr069.txt
Library           %{RFPATH}/Python-2.7/lib/python2.7/site-packages/robot/libraries/String.py
Library           %{RFPATH}/Python-2.7/lib/python2.7/site-packages/robot/libraries/Collections.py
Resource          %{ROBOTREPO}/HLK/res/l3-selenium2.txt

*** Test Cases ***
01
    [Documentation]    GetParameterAttributes -- Simple Complete Path
    TriggerInform
    @{parameter_list}=    GetParameterAttributesEx    InternetGatewayDevice.ManagementServer.PeriodicInformEnable

02
    [Documentation]    GetParameterAttributes -- Multiple Complete Path
    TriggerInform
    @{parameter_list}=    GetParameterAttributesEx    InternetGatewayDevice.ManagementServer.PeriodicInformEnable InternetGatewayDevice.Layer3Forwarding.ForwardNumberOfEntries InternetGatewayDevice.ManagementServer.ConnectionRequestURL

03
    [Documentation]    GetParameterAttributes -- Partial Path
    TriggerInform
    @{parameter_list}=    GetParameterAttributesEx    InternetGatewayDevice.ManagementServer.

04
    [Documentation]    GetParameterAttributes -- Complete and Partial Path
    TriggerInform
    @{parameter_list}=    GetParameterAttributesEx    InternetGatewayDevice.ManagementServer. InternetGatewayDevice.DeviceInfo.UpTime

05
    [Documentation]    GetParameterAttributes - Invalid Path
    ...
    ...    InternetGatewayDevice.aaa
    TriggerInform
    Set Request Body    <?xml version='1.0'?> <SOAP-ENV:Envelope xmlns:xsi="http://www.w3.org/1999/XMLSchema-instance" xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:SOAP-ENC="http://schemas.xmlsoap.org/soap/encoding/" xmlns:cwmp="urn:dslforum-org:cwmp-1-0" xmlns:xsd="http://www.w3.org/1999/XMLSchema"><SOAP-ENV:Body><GetParameterAttributesEx><ParameterName>InternetGatewayDevice.aaa</ParameterName></GetParameterAttributesEx></SOAP-ENV:Body></SOAP-ENV:Envelope>
    Set Request Header    Content-Type    text/xml
    Set Request Header    SOAPAction    "GetParameterAttributesEx"
    POST    /ACS
    Response Status Code Should Equal    200
    ${body}=    Get Response Body
    Should Contain    ${body}    FaultCode 9005
    Should Contain    ${body}    Invalid parameter name

*** Keywords ***
SuiteSetup
    Should Be Equal As Strings    ${startup_status}    PASS
    Create Http Context    localhost:8015
