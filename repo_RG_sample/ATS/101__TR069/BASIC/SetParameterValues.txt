*** Settings ***
Suite Setup       SuiteSetup
Test Teardown     CloseClient
Default Tags      COMMON
Resource          %{ROBOTREPO}/HLK/res/l2-tr069.txt
Library           HttpLibrary.HTTP
Resource          %{ROBOTREPO}/HLK/res/l3-tr069.txt
Library           %{RFPATH}/Python-2.7/lib/python2.7/site-packages/robot/libraries/String.py
Library           %{RFPATH}/Python-2.7/lib/python2.7/site-packages/robot/libraries/Collections.py
Resource          %{ROBOTREPO}/HLK/res/l3-selenium2.txt

*** Test Cases ***
01
    [Documentation]    SetParameterValues - Simple Parameter
    TriggerInform
    ${status}=    SetParameterValuesEx    InternetGatewayDevice.ManagementServer.PeriodicInformEnable False
    Should Be Equal    ${status}    0

02
    [Documentation]    SetParameterValues - complex Parameter
    TriggerInform
    ${status}=    SetParameterValuesEx    InternetGatewayDevice.ManagementServer.PeriodicInformEnable False InternetGatewayDevice.ManagementServer.PeriodicInformInterval 180 InternetGatewayDevice.LANDevice.1.LANHostConfigManagement.DomainName ASBHome
    Should Be Equal    ${status}    0

03
    [Documentation]    SetParameterValues - Invalid parameter value
    ...
    ...    InternetGatewayDevice.DeviceInfo.Manufacture | badValue
    TriggerInform
    Set Request Body    <?xml version='1.0'?> <SOAP-ENV:Envelope xmlns:xsi="http://www.w3.org/1999/XMLSchema-instance" xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:SOAP-ENC="http://schemas.xmlsoap.org/soap/encoding/" xmlns:cwmp="urn:dslforum-org:cwmp-1-0" xmlns:xsd="http://www.w3.org/1999/XMLSchema"><SOAP-ENV:Body><SetParameterValuesEx><ParameterList>InternetGatewayDevice.DeviceInfo.Manufacture badValue</ParameterList><ParameterKey>3</ParameterKey></SetParameterValuesEx></SOAP-ENV:Body></SOAP-ENV:Envelope>
    Set Request Header    Content-Type    text/xml
    Set Request Header    SOAPAction    "SetParameterValuesEx"
    POST    /ACS
    Response Status Code Should Equal    200
    ${body}=    Get Response Body
    Should Contain    ${body}    FaultCode 9003
    Should Contain    ${body}    Invalid arguments

*** Keywords ***
SuiteSetup
    Should Be Equal As Strings    ${startup_status}    PASS
    Create Http Context    localhost:8015
