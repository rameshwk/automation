*** Settings ***
Suite Setup       SuiteSetup
Test Teardown
Default Tags      A020WA    G010GA    G120GE    G120WF    G140WC    G240GC    G240GE
...               G240WA    G240WB    G240WC    G240WE    G240WF    G240WG    G240WH
...               G240WP    G240WZA    G241WA    G440GA    HA020WA    HA030WB    I240GD
...               I240WA    VIET
Resource          %{ROBOTREPO}/HLK/res/l2-tr069.txt
Library           %{RFPATH}/Python-2.7/lib/python2.7/site-packages/robot/libraries/Process.py

*** Variables ***
${upnpc}          %{ROBOTWS}/miniupnpc/bin/./upnpc

*** Test Cases ***
01
    [Documentation]    === Check if the node X_CT-COM_UPNP.Enable is visible to ACS Server ===
    ...
    ...    == Summary ==
    ...
    ...    Check if X_CT-COM_UPNP.Enable node is visible to ACS Server when retrieved,GetParameterValue of InternetGatewayDevice.,and should be ok,check if ONT report the node InternetGatewayDevice.DeviceInfo.X_CT-COM_UPNP.Enable to ACS Server,and \ the value should be consistent with the webgui setting.
    ...
    ...    *Author:* Li Na
    ...
    ...    ---
    ...    == Customer ==
    ...
    ...    This case is only applicable to VIET
    ...    ---
    ...    == History ==
    ...    _* 07. July, 2017 --- Li Na \ <Na.E.Li@alcatel-sbell.com.cn>: Create this case
    TriggerInform
    @{parameter_list}=    GetParameterValuesEx    InternetGatewayDevice.
    Log Many    ${parameter_list}
    ${parameter_dict}=    Create Dictionary
    : FOR    ${string}    IN    @{parameter_list}
    \    @{entry}=    Split String    ${string}    ${SPACE}
    \    Set To Dictionary    ${parameter_dict}    @{entry}[0]    @{entry}[1]
    ${upnp_enable}=    Get From Dictionary    ${parameter_dict}    InternetGatewayDevice.DeviceInfo.X_CT-COM_UPNP.Enable
    Should Be Equal    ${upnp_default}    ${upnp_enable}
    [Teardown]    CloseClient

02
    [Documentation]    === Check if the node X_CT-COM_UPNP.Enable is visible to ACS Server \ when nextlevel is false===
    ...
    ...    == Summary ==
    ...
    ...    GetParameterName of InternetGatewayDevice. with nextlevel false,and it should be ok,check if ONT report the node InternetGatewayDevice.DeviceInfo.X_CT-COM_UPNP.Enable to ACS Server
    ...
    ...    *Author:* Li Na
    ...
    ...    ---
    ...    == Customer ==
    ...
    ...    This case is only applicable to VIET
    ...    ---
    ...    == History ==
    ...    _* 07. July, 2017 --- Li Na \ <Na.E.Li@alcatel-sbell.com.cn>: Create this case
    TriggerInform
    @{parameter_list}=    GetParameterNamesEx    InternetGatewayDevice.    false
    Log Many    ${parameter_list}
    Should Contain match    ${parameter_list}    regexp=InternetGatewayDevice.DeviceInfo.X_CT-COM_UPNP.Enable
    [Teardown]    CloseClient

03
    [Documentation]    === Check unnecessary nodes should be invisible except the X_CT-COM_UPNP.Enable===
    ...
    ...    == Summary ==
    ...
    ...    GetParameterValue of the node InternetGatewayDevice.DeviceInfo.X_CT-COM_UPNP. and it should be ok,check if ONT only report InternetGatewayDevice.DeviceInfo.X_CT-COM_UPNP. Enable to ACS Server, other nodes should be invisible
    ...
    ...    *Author:* Li Na
    ...
    ...    ---
    ...    == Customer ==
    ...
    ...    This case is only applicable to VIET
    ...    ---
    ...    == History ==
    ...    _* 07. July, 2017 --- Li Na \ <Na.E.Li@alcatel-sbell.com.cn>: Create this case
    TriggerInform
    @{parameter_list}=    GetParameterValuesEx    InternetGatewayDevice.DeviceInfo.X_CT-COM_UPNP.
    Log Many    ${parameter_list}
    ${parameter_list}=    Remove String    @{parameter_list}[0]
    Should Be Equal As Strings    ${parameter_list}    InternetGatewayDevice.DeviceInfo.X_CT-COM_UPNP.Enable ${upnp_default}
    [Teardown]    CloseClient

04
    [Documentation]    === Disable upnp via TR069, then check the result===
    ...
    ...    == Summary ==
    ...
    ...    SetParameterValue of InternetGatewayDevice.DeviceInfo.X_CT-COM_UPNP.Enable false ,it should be ok,then check webgui, UPNP/DLNA should be disabled on webgui
    ...
    ...    *Author:* Li Na
    ...
    ...    ---
    ...    == Customer ==
    ...
    ...    This case is only applicable to VIET
    ...    ---
    ...    == History ==
    ...    _* 07. July, 2017 --- Li Na \ <Na.E.Li@alcatel-sbell.com.cn>: Create this case
    TriggerInform
    ${status}=    SetParameterValuesEx    InternetGatewayDevice.DeviceInfo.X_CT-COM_UPNP.Enable false
    Should Be Equal    ${status}    0
    ${parameter_dict}=    Create Dictionary
    @{parameter_list}=    GetParameterValuesEx    InternetGatewayDevice.DeviceInfo.X_CT-COM_UPNP.Enable
    : FOR    ${string}    IN    @{parameter_list}
    \    @{entry}=    Split String    ${string}    ${SPACE}
    \    Set To Dictionary    ${parameter_dict}    @{entry}[0]    @{entry}[1]
    ${upnp_enable}=    Get From Dictionary    ${parameter_dict}    InternetGatewayDevice.DeviceInfo.X_CT-COM_UPNP.Enable
    CloseClient
    l3-selenium2.LoginGUI    ${ont['gui_type']}    ${ont['url_protocol']}://${ont['ip']}    ${ont['gui_username']}    ${ont['gui_password']}
    sleep    2s
    ${pDict}=    l3-selenium2.GetAppUPNPsetting    ${ont['gui_type']}    ${ont['url_protocol']}://${ont['ip']}
    ${value}=    Get From Dictionary    ${pDict}    Secutity_app_upnp_en
    l3-selenium2.LogoutGUI    ${ont['gui_type']}    ${ont['url_protocol']}://${ont['ip']}
    ${value}=    Set Variable If    '${value}'=='None'    false    true
    Should Be Equal    ${value}    ${upnp_enable}
    ${case04_status}=    set variable    True
    Set Suite Variable    ${case04_status}    ${case04_status}
    [Teardown]

05
    [Documentation]    === Disable upnp via TR069, then check the result===
    ...
    ...    == Summary ==
    ...
    ...    on basis of the case 3,use ps command to check upnp running progress,there is no upnp progress running,and capture SSDP packet ,there is no IGD Device can be found
    ...
    ...    *Author:* Li Na
    ...
    ...    ---
    ...    == Customer ==
    ...
    ...    This case is only applicable to VIET
    ...    ---
    ...    == History ==
    ...    _* 07. July, 2017 --- Li Na \ <Na.E.Li@alcatel-sbell.com.cn>: Create this case
    should be equal    ${case04_status}    True
    L3-Telnet.LoginTelnet    ${ont['ip']}    23    ${ont['telnet_username']}    ${ont['telnet_password']}
    ${upnp}    L3-Telnet.EXECommand    ps
    log many    ${upnp}
    L2-Telnet.CloseAllConnection
    Should Not Match Regexp    ${upnp}    upnpd
    #
    ${pata_handle}=    StartPCTA    ${pcta['ip']}    ${pcta['username']}    ${pcta['password']}    ${pcta['path']}
    ${lan_interface}=    Get Substring    ${pc['lan_interface']}    3
    ${lan_ip}=    Set Variable    ${pc['lan_ip']}
    ${wan_interface}=    Get Substring    ${pc['wan_interface']}    3
    ${wan_ip}=    Set Variable    ${pc['wan_ip']}
    ${ip_src}=    Set Variable    ${pc['lan_ip']}
    ${hgu_ip}=    Remove String    ${ont['ip']}    http://
    #failed capture SSDP packet
    ${cap_dist_id}=    ta_start_capt_udp_packet    ${lan_interface}    -ip_src ${ont['ip']}
    log    ${cap_dist_id}
    #upnpc
    Run Process    ${upnpc} -m eth2 -a ${pc['lan_ip']} 10000 20000 tcp    alias=upnpc_proc    shell=true
    sleep    5s
    ${upnpc_result}=    Get Process Result    upnpc_proc
    log    ${upnpc_result.stdout}
    log    ${upnpc_result.stderr}
    Should Contain    ${upnpc_result.stderr}    No IGD UPnP Device found on the network !
    ${passed}=    Run Keyword And Return Status    Should Contain    ${upnpc_result.stdout}    No IGD UPnP Device found on the network !
    sleep    5s
    ${cap_result_dict}=    ta_stop_capt_udp_packet    ${lan_interface}    -dist_id ${cap_dist_id}
    Should Be Equal As Strings    ${cap_result_dict}    NONE
    ClosePCTA

06
    [Documentation]    === Enable upnp via TR069, then check the result===
    ...
    ...    == Summary ==
    ...
    ...    SetParameterValue of InternetGatewayDevice.DeviceInfo.X_CT-COM_UPNP.Enable true ,it should be ok,then check webgui, UPNP/DLNA should be enabled on webgui
    ...
    ...    *Author:* Li Na
    ...
    ...    ---
    ...    == Customer ==
    ...
    ...    This case is only applicable to VIET
    ...    ---
    ...    == History ==
    ...    _* 07. July, 2017 --- Li Na \ <Na.E.Li@alcatel-sbell.com.cn>: Create this case
    TriggerInform
    ${status}=    SetParameterValuesEx    InternetGatewayDevice.DeviceInfo.X_CT-COM_UPNP.Enable true
    Should Be Equal    ${status}    0
    ${parameter_dict}=    Create Dictionary
    @{parameter_list}=    GetParameterValuesEx    InternetGatewayDevice.DeviceInfo.X_CT-COM_UPNP.Enable
    : FOR    ${string}    IN    @{parameter_list}
    \    @{entry}=    Split String    ${string}    ${SPACE}
    \    Set To Dictionary    ${parameter_dict}    @{entry}[0]    @{entry}[1]
    ${upnp_enable}=    Get From Dictionary    ${parameter_dict}    InternetGatewayDevice.DeviceInfo.X_CT-COM_UPNP.Enable
    CloseClient
    l3-selenium2.LoginGUI    ${ont['gui_type']}    ${ont['url_protocol']}://${ont['ip']}    ${ont['gui_username']}    ${ont['gui_password']}
    sleep    2s
    ${pDict}=    l3-selenium2.GetAppUPNPsetting    ${ont['gui_type']}    ${ont['url_protocol']}://${ont['ip']}
    ${value}=    Get From Dictionary    ${pDict}    Secutity_app_upnp_en
    l3-selenium2.LogoutGUI    ${ont['gui_type']}    ${ont['url_protocol']}://${ont['ip']}
    ${value}=    Set Variable If    '${value}'=='None'    false    true
    Should Be Equal    ${value}    ${upnp_enable}
    ${case06_status}=    set variable    True
    Set Suite Variable    ${case06_status}    ${case06_status}

07
    [Documentation]    === Enable upnp via TR069, then check the result===
    ...
    ...    == Summary ==
    ...
    ...    on basis of the case 5,use ps command to check upnp running progress,there should be upnp progress running
    ...
    ...    *Author:* Li Na
    ...
    ...    ---
    ...    == Customer ==
    ...
    ...    This case is only applicable to VIET
    ...    ---
    ...    == History ==
    ...    _* 07. July, 2017 --- Li Na \ <Na.E.Li@alcatel-sbell.com.cn>: Create this case
    should be equal    ${case06_status}    True
    #check the upnpd whether run with ps command
    L3-Telnet.LoginTelnet    ${ont['ip']}    23    ${ont['telnet_username']}    ${ont['telnet_password']}
    ${result}    L3-Telnet.EXECommand    ip route
    log many    ${result}
    ${upnp}    L3-Telnet.EXECommand    ps
    log many    ${upnp}
    L2-Telnet.CloseAllConnection
    @{result}=    Split String    ${result}    ${SPACE}
    ${passed}=    Run Keyword And Return Status    Should Match Regexp    ${ont['gui_type']}    I240GD|I240WA
    ${interface}    Run_Keyword_If    '${passed}'=='True'    Get From List    ${result}    -2
    ...    ELSE    Get From List    ${result}    4
    log many    ${interface}
    Should Match Regexp    ${upnp}    upnpd ${interface} br0

08
    [Documentation]    === Enable upnp via TR069, then check the result===
    ...
    ...    == Summary ==
    ...
    ...    on basis of the case 5,capture SSDP packet ,there is IGD Device can be found and add port forwarding rule via upnp,it should be successful, send traffic from wan side to verify the rule.
    ...
    ...    *Author:* Li Na
    ...
    ...    ---
    ...    == Customer ==
    ...
    ...    This case is only applicable to VIET
    ...    ---
    ...    == History ==
    ...    _* 07. July, 2017 --- Li Na \ <Na.E.Li@alcatel-sbell.com.cn>: Create this case
    should be equal    ${case06_status}    True
    #
    ${pata_handle}=    StartPCTA    ${pcta['ip']}    ${pcta['username']}    ${pcta['password']}    ${pcta['path']}
    ${lan_interface}=    Get Substring    ${pc['lan_interface']}    3
    ${lan_ip}=    Set Variable    ${pc['lan_ip']}
    ${wan_interface}=    Get Substring    ${pc['wan_interface']}    3
    ${wan_ip}=    Set Variable    ${pc['wan_ip']}
    ${ip_src}=    Set Variable    ${pc['lan_ip']}
    ${hgu_ip}=    Remove String    ${ont['ip']}    http://
    #capture SSDP packet
    ${cap_dist_id}=    ta_start_capt_udp_packet    ${lan_interface}    -ip_src ${ont['ip']}
    log    ${cap_dist_id}
    #upnpc
    Run Process    ${upnpc} -m eth2 -a ${pc['lan_ip']} 8081 8081 udp    alias=upnpc_proc    shell=true
    sleep    5s
    ${upnpc_result}=    Get Process Result    upnpc_proc
    log    ${upnpc_result.stdout}
    log    ${upnpc_result.stderr}
    Should Not Contain    ${upnpc_result.stderr}    upnpc: command not found
    ${passed}=    Run Keyword And Return Status    Should Not Contain    ${upnpc_result.stdout}    No IGD UPnP Device found on the network !
    sleep    20s
    ${cap_result_dict}=    ta_stop_capt_udp_packet    ${lan_interface}    -dist_id ${cap_dist_id}
    ${ip_dest}=    Get From Dictionary    ${cap_result_dict}    -ip_dest
    Should Be Equal    ${ip_dest}    ${pc['lan_ip']}
    ###confirm upnp rule to configure successfully###
    run process    ${upnpc} -m eth2 -l    alias=upnpc_proc_l    shell=true
    ${upnpc_result}=    Get Process Result    upnpc_proc_l
    log    ${upnpc_result.stdout}
    should contain    ${upnpc_result.stdout}    8081->${pc['lan_ip']}:8081
    ######
    ${cap_dist_id}=    ta_start_capt_icmp_message    ${wan_interface}    -icmp_type 08 -ip_src ${cpe_internet_ip}
    ta_send_icmp_message    ${lan_interface}    -mac_src ${pc_lan_mac} -ip_src ${ip_src} -mac_dest ${cpe_lan_mac} -ip_dest ${wan_ip} -message_count 3 -message_delay 1000
    sleep    2s
    ${cap_result_dict}=    ta_stop_capt_icmp_message    ${wan_interface}    -dist_id ${cap_dist_id}
    Should Not Be Equal As Strings    ${cap_result_dict}    NONE
    ####
    ${cap_dist_id}=    ta_start_capt_udp_packet    ${lan_interface}    -dst_port 8081
    ${cpe_wan_mac}=    Get From Dictionary    ${cap_result_dict}    -mac_src
    ta_send_udp_packet    ${wan_interface}    -mac_src ${pc_lan_mac} -ip_src ${wan_ip} -mac_dest ${cpe_wan_mac} -ip_dest ${cpe_internet_ip} -dst_port 8081 -message_count 3
    sleep    2s
    ${cap_result_dict}=    ta_stop_capt_udp_packet    ${lan_interface}    -dist_id ${cap_dist_id}
    ${ip_dest}=    Get From Dictionary    ${cap_result_dict}    -ip_dest
    Should Be Equal    ${ip_dest}    ${pc['lan_ip']}
    ClosePCTA

*** Keywords ***
SuiteSetup
    Should Be Equal As Strings    ${startup_status}    PASS
    Create Http Context    localhost:8015
