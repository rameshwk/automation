*** Settings ***
Default Tags      ALCL    ALCO    LTRW    VNPT    G240WF    G240WB    MXXV
...               SAIB    VIET    BRTI
Library           %{RFPATH}/Python-2.7/lib/python2.7/site-packages/robot/libraries/Process.py
Library           %{RFPATH}/Python-2.7/lib/python2.7/site-packages/robot/libraries/String.py
Resource          %{ROBOTREPO}/HLK/res/l3-selenium2.txt
Resource          %{ROBOTREPO}/HLK/res/l3-telnet.txt
Resource          %{ROBOTREPO}/HLK/res/l3-tr069.txt
Variables         %{ROBOTWS}/args/preconfig.py

*** Test Cases ***
01
    [Documentation]    This case is designed to verify that defualt language = eng/espa
    ...
    ...    *Author:* \ _*Qiu Renjie*_ \ _<Renjie.Qiu@alcatel-sbell.com.cn> _
    ## judge OPID ##
    ${passed}    Run Keyword And Return Status    Should Match Regexp    ${GEN['opid']}    STXX
    ## login ##
    l3-selenium2.LoginGUI    ${ont['gui_type']}    ${ont['url_protocol']}://${ont['ip']}    ${WEB['username']}    ${WEB['password']}
    Comment    ${A}    L1-Selenium2.GetText    //div[@id='div_logout']/ul[1]/li[1]/a[@href="login.cgi?out"]
    Unselect Frame
    Comment    ${A}    L1-Selenium2.GetText    id=xtitle
    ${A}    Run Keyword If    ${passed}    L1-Selenium2.GetText    xpath=/html/body/div/div[1]/div[1]/div[2]/div[2]/table/tbody/tr/td
    ...    ELSE    L1-Selenium2.GetText    //li[@id='xtitle']
    LOG    ${A}
    ### adjust ###
    LOG    ${GEN['language']}
    Run Keyword If    '${GEN['language']}'=='eng'    Should Contain    ${A}    GPON Home Gateway
    ...    ELSE IF    '${GEN['language']}'=='espa'    Should Contain    ${A}    Terminal Óptica
    ## logout ###
    l3-selenium2.LogoutGUI    ${ont['gui_type']}    ${ont['url_protocol']}://${ont['ip']}
    [Teardown]    L1-Selenium2.CloseAllBrowser

02
    [Documentation]    This case is designed to verify that GUI will logout after X time automatially
    ...
    ...    *Author:* \ _*Qiu Renjie*_ \ _<Renjie.Qiu@alcatel-sbell.com.cn> _
    ## judge OPID ##
    ${passed}    Run Keyword And Return Status    Should Match Regexp    ${GEN['opid']}    STXX
    ## login ##
    l3-selenium2.LoginGUI    ${ont['gui_type']}    ${ont['url_protocol']}://${ont['ip']}    ${WEB['username']}    ${WEB['password']}
    Unselect Frame
    ${A}    Run Keyword If    ${passed}    L1-Selenium2.GetText    xpath=/html/body/div/div[1]/div[1]/div[2]/div[2]/table/tbody/tr/td
    ...    ELSE    L1-Selenium2.GetText    //li[@id='xtitle']
    LOG    ${A}
    sleep    610s
    ${B}    Run Keyword and Ignore error    Run Keyword If    ${passed}    L1-Selenium2.GetText    xpath=/html/body/div/div[1]/div[1]/div[2]/div[2]/table/tbody/tr/td
    ...    ELSE    L1-Selenium2.GetText    //li[@id='xtitle']
    Comment    ${B}    Run Keyword and Ignore error    L1-Selenium2.GetText    //li[@id='xtitle']
    ${B}    convert to list    ${B}
    Should Be Equal As Strings    @{B}[0]    FAIL
    l3-selenium2.LogoutGUI    ${ont['gui_type']}    ${ont['url_protocol']}://${ont['ip']}
    ### adjust ###
    Comment    LOG    ${GEN['language']}
    Comment    Run Keyword If    '${GEN['language']}'=='eng'    Should Be Equal As Strings    ${A}    GPON Home Gateway
    ...    ELSE IF    '${GEN['language']}'=='espa'    Should Be Equal As Strings    ${A}    l
    Comment    ## logout ###
    Comment    l3-selenium2.LogoutGUI    ${ont['gui_type']}    ${ont['url_protocol']}://${ont['ip']}
    [Teardown]    L1-Selenium2.CloseAllBrowser

03
    [Documentation]    This case is designed to verify that GUI IP will be same as preconfig
    ...    such as "192.168.1.x"
    ...
    ...    *Author:* \ _*Qiu Renjie*_ \ _<Renjie.Qiu@alcatel-sbell.com.cn> _
    ## login ##
    L1-Selenium2.OpenBrowser    ${ont['url_protocol']}://${ont['ip']}
    sleep    5s
    ${B}    Run Keyword and Ignore error    Run Keyword IF    '${ont['gui_type']}'=='I240WA'    L1-Selenium2.GetText    xpath=/html/body/form/table/tbody/tr[1]/td
    ...    ELSE    L1-Selenium2.GetText    id=loginform
    Comment    ${B}    Run Keyword and Ignore error    L1-Selenium2.GetText    id=loginform
    ${B}    convert to list    ${B}
    Should Be Equal As Strings    @{B}[0]    PASS
    l3-selenium2.LogoutGUI    ${ont['gui_type']}    ${ont['url_protocol']}://${ont['ip']}
    [Teardown]    L1-Selenium2.CloseAllBrowser

04
    [Documentation]    This case is designed to verify that GUI normal user's name and passwd is correct.
    ...
    ...    *Author:* \ _*Qiu Renjie*_ \ _<Renjie.Qiu@alcatel-sbell.com.cn> _
    ## login ##
    l3-selenium2.LoginGUI    ${ont['gui_type']}    ${ont['url_protocol']}://${ont['ip']}    ${WEB['username']}    ${WEB['password']}
    Unselect Frame
    ${B}    Run Keyword and Ignore error    L1-Selenium2.GetText    id=err_times
    ${B}    convert to list    ${B}
    Should Be Equal As Strings    @{B}[0]    FAIL
    l3-selenium2.LogoutGUI    ${ont['gui_type']}    ${ont['url_protocol']}://${ont['ip']}
    [Teardown]    L1-Selenium2.CloseAllBrowser

05
    [Documentation]    This case is designed to verify that ONT type dispalyed on web is correct. (Preconfig file words must be same as gui display. For example , \ G240WA must be save as G-240W-A, just like what dispaly on gui.)
    ...
    ...    *Author:* \ _*Qiu Renjie*_ \ _<Renjie.Qiu@alcatel-sbell.com.cn> _
    ## login ##
    l3-selenium2.LoginGUI    ${ont['gui_type']}    ${ont['url_protocol']}://${ont['ip']}    ${WEB['username']}    ${WEB['password']}
    ## get ont type from web##
    ${ont_type_on_gui}    l3-selenium2.GetOntTypeOnGui    ${ont['gui_type']}    ${ont['url_protocol']}://${ont['ip']}
    LOG    ${ont_type_on_gui}
    ## get ont type from Dic
    ${ont_type}=    Get From Dictionary    ${ont_type_on_gui}    ont_type_on_gui
    #Create a Result List
    @{Result_List}    Create List
    #Check Inform Enable
    ${Status}    Run Keyword And Ignore Error    Should Be Equal As Strings    ${ont_type}    ${ont['hardware']}
    Append To List    ${Result_List}    ${Status}
    #The result should not cain fail
    ${Total_Result}    Convert To String    ${Result_List}
    Should Not Contain    ${Total_Result}    FAIL
    [Teardown]    L1-Selenium2.CloseAllBrowser
