*** Settings ***
Default Tags      MXXT    ALCL    ALCO    LTRW    VNPT    G240WF    G240WB
Library           %{RFPATH}/Python-2.7/lib/python2.7/site-packages/robot/libraries/Process.py
Library           %{RFPATH}/Python-2.7/lib/python2.7/site-packages/robot/libraries/String.py
Resource          %{ROBOTREPO}/HLK/res/l3-selenium2.txt
Resource          %{ROBOTREPO}/HLK/res/l3-telnet.txt
Resource          %{ROBOTREPO}/HLK/res/l3-tr069.txt
Variables         %{ROBOTWS}/args/preconfig.py

*** Test Cases ***
01
    [Documentation]    This case is designed to verify \ *SLID Config* Page is *exsist* when you login by *super User*
    ...
    ...    *Author:* \ _*Qiu Renjie*_ \ _<Renjie.Qiu@alcatel-sbell.com.cn> _
    # If this OPID hiden "SLID Cfg" option, please list them here!
    ${passed}    Run Keyword And Return Status    Should Match Regexp    ${GEN['opid']}    MXXT|ENTB
    Pass Execution if    ${passed}    Should Be Equal    True
    ## login ##
    l3-selenium2.LoginGUI    ${ont['gui_type']}    ${ont['url_protocol']}://${ont['ip']}    ${ont['gui_username']}    ${ont['gui_password']}
    Unselect Frame
    L1-Selenium2.ClickLink    //div[@id="div_menu_left"]/ul[6]/li[1]/a
    sleep    1s
    ${B}    Run Keyword And Ignore Error    L1-Selenium2.ClickLink    //a[@href="soliconfigf?ping"]
    sleep    1s
    Should Contain    ${B}    PASS
    [Teardown]    L1-Selenium2.CloseAllBrowser

02
    [Documentation]    Check option *RG Troubleshoot Counters* is displayed on web gui.
    ...
    ...    *Author:* \ _*Qiu Renjie*_ \ _<Renjie.Qiu@alcatel-sbell.com.cn> _
    # If this OPID hiden "Call Log" option, please list them here!
    ${passed}    Run Keyword And Return Status    Should Match Regexp    ${GEN['opid']}    NA
    Pass Execution if    '${passed}'=='True'    Message: This OPID hiden "RG Troubleshoot Counters" option on GUI, Case will be skip and Pass!
    ## login ##
    L3-Selenium2.LoginGUI    ${ont['gui_type']}    ${ont['url_protocol']}://${ont['ip']}    ${ont['gui_username']}    ${ont['gui_password']}
    Unselect Frame
    L1-Selenium2.ClickLink    //div[@id="div_menu_left"]/ul[6]/li[1]/a
    sleep    1s
    ${A}    Run Keyword And Ignore Error    L1-Selenium2.ClickLink    //a[@href="rgtroubleshooting.cgi?ping"]
    sleep    1s
    ${A}    Convert To String    ${A}
    Should Contain    ${A}    PASS
    [Teardown]    L1-Selenium2.CloseAllBrowser
