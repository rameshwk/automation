*** Settings ***
Suite Setup       SuiteSetup
Suite Teardown    SuiteTeardown
Default Tags      COMMON
Library           %{RFPATH}/Python-2.7/lib/python2.7/site-packages/robot/libraries/String.py
Library           %{RFPATH}/Python-2.7/lib/python2.7/site-packages/robot/libraries/Process.py
Resource          %{ROBOTREPO}/HLK/res/l3-selenium2.txt
Resource          %{ROBOTREPO}/HLK/res/l2-ssh.txt
Resource          %{ROBOTREPO}/HLK/res/l3-telnet.txt
Resource          %{ROBOTREPO}/HLK/res/l2-tr069.txt
Resource          %{ROBOTREPO}/HLK/res/l2-telnet.txt
Resource          %{ROBOTREPO}/HLK/res/l3-tr069.txt

*** Test Cases ***
StartACS
    Pass Execution If    ${FLAG_DEBUG}==${2} or ${FLAG_DEBUG} == ${3}    Skip StartACS, because FLAG_DEBUG=2 or 3.
    ${result}=    Run Process    $ROBOTWS/ACS/bin/tclsh8.4 $ROBOTWS/ACS/lib/tclhttpd3.5.1/bin/httpd.tcl -port 8015 > /dev/null 2>&1 &    shell=True
    sleep    5s
    Should Be Empty    ${result.stderr}

GetParameterValue
    log    pass
    log    ${pc['wan_interface'][3]}
    Log Environment Variables
    Log Variables

UpgradeONT-GUI
    [Documentation]    Upgrade ONT Firmware by WEBGUI
    ...
    ...    --2017-01-05 modify by rock
    ...    add logout GUI after GetDeviceInfo
    Pass Execution If    ${FLAG_DEBUG}==${2}    Skip Upgrade, because FLAG_DEBUG=2.
    ${gui_username}    Set Variable If    "${FLAG_DEBUG}"=="0"    ${GUI_NAME_INIT}    ${ont['gui_username']}
    ${gui_password}    Set Variable If    "${FLAG_DEBUG}"=="0"    ${GUI_PASSWD_INIT}    ${ont['gui_password']}
    ${gui_type}    Set Variable If    "${FLAG_DEBUG}"=="0"    ${GUI_TYPE_INIT}    ${ont['gui_type']}
    ${ont_ip}    Set Variable If    "${FLAG_DEBUG}"=="0"    ${ONT_IP_INIT}    ${ont['ip']}
    ${url_protocol}    Set Variable If    "${FLAG_DEBUG}"=="0"    ${ONT_URL_PROTOCOL_INIT}    ${ont['url_protocol']}
    ${local_path}=    Set Variable    %{ROBOTWS}/firmware
    ${tmp}    ${firmware_tar}    Should Match Regexp    ${image_path}    .+/(.+\.tar)
    ${result}=    Run Process    mkdir -p ${local_path}    shell=True
    ${result}=    Run Process    curl -o ${local_path}/${firmware_tar} http://135.251.206.224/IMAGE/${image_path}    shell=True
    ${result}=    Run Process    tar tf ${local_path}/${firmware_tar} |grep ${ont['product_code']}    shell=True
    ${firmware_name}=    Set Variable    ${result.stdout}
    ${result}=    Run Process    tar xvf ${local_path}/${firmware_tar} -C ${local_path} ${firmware_name}    shell=True
    Should Be Empty    ${result.stderr}
    l3-selenium2.LoginGUI    ${gui_type}    ${url_protocol}://${ont_ip}    ${gui_username}    ${gui_password}
    l3-selenium2.UpgradeFirmware    ${gui_type}    ${url_protocol}://${ont_ip}    ${local_path}/${firmware_name}
    l3-selenium2.LogoutGUI    ${gui_type}    ${url_protocol}://${ont_ip}
    @{counter}=    evaluate    range(10)
    : FOR    ${a}    IN    @{counter}
    \    Run Process    ping    ${ont_ip}    -c    60    alias=ping_proc
    \    ${ping_result}=    Get Process Result    ping_proc
    \    ${passed} =    Run Keyword And Return Status    Should Contain    ${ping_result.stdout}    60 received, 0% packet loss
    \    Exit For Loop If    ${passed}
    \    Sleep    10s
    Should Be True    ${passed}
    l3-selenium2.LoginGUI    ${gui_type}    ${url_protocol}://${ont_ip}    ${gui_username}    ${gui_password}
    ${DeviceInfo}=    l3-selenium2.GetDeviceInfo    ${gui_type}    ${url_protocol}://${ont_ip}
    l3-selenium2.LogoutGUI    ${gui_type}    ${url_protocol}://${ont_ip}
    ${SoftwareVersion}=    Get From Dictionary    ${DeviceInfo}    software_version
    Should Be Equal    ${SoftwareVersion}    ${firmware_name}

FactoryReset
    Pass Execution If    ${FLAG_DEBUG}==${2}    Skip FactoryReset, because FLAG_DEBUG=2.
    # Reset DUT
    ${bak_ip}    Set Variable If    "${FLAG_DEBUG}"=="0"    ${ont['ip']}
    ${bak_user}    Set Variable If    "${FLAG_DEBUG}"=="0"    ${ont['telnet_username']}
    ${bak_passwd}    Set Variable If    "${FLAG_DEBUG}"=="0"    ${ont['telnet_password']}
    ${bak_port}    Set Variable If    "${FLAG_DEBUG}"=="0"    ${ont['telnet_port']}
    ${ont['ip']}    Set Variable If    "${FLAG_DEBUG}"=="0"    ${ONT_IP_INIT}    ${ont['ip']}
    ${ont['telnet_username']}    Set Variable If    "${FLAG_DEBUG}"=="0"    ${TELNET_NAME_INIT}    ${ont['telnet_username']}
    ${ont['telnet_password']}    Set Variable If    "${FLAG_DEBUG}"=="0"    ${TELNET_PASSWD_INIT}    ${ont['telnet_password']}
    ${ont['telnet_port']}    Set Variable If    "${FLAG_DEBUG}"=="0"    ${TELNET_PORT_INIT}    ${ont['telnet_port']}
    log    ${\n}Do reset Now ...
    L3-Telnet.LoginTelnet    ${ont['ip']}    ${ont['telnet_port']}    ${ont['telnet_username']}    ${ont['telnet_password']}
    ${5g_status}=    Run Keyword And Return Status    Should Match Regexp    ${ont['gui_type']}    G240WB|G240WZA|F240WA|G240WC|HA030WB|HA020WA
    Run Keyword if    '${5g_status}'=='True'    L3-Telnet.EXECommand    ritool set CountryID ${ont['country_id']}
    L3-Telnet.EXECommand    ritool set OperatorID ${ont['opid']}
    L3-Telnet.EXECommand    cfgcli -r all
    sleep    5s
    L3-Telnet.EXECommand    reboot
    L2-Telnet.CloseAllConnection
    Sleep    120s
    SetPcIpv4    ${pc['lan_ip']}
    ${ont['ip']}    Set Variable If    "${FLAG_DEBUG}"=="0"    ${bak_ip}    ${ont['ip']}
    ${ont['telnet_username']}    Set Variable If    "${FLAG_DEBUG}"=="0"    ${bak_user}    ${ont['telnet_username']}
    ${ont['telnet_password']}    Set Variable If    "${FLAG_DEBUG}"=="0"    ${bak_passwd}    ${ont['telnet_password']}
    ${ont['telnet_port']}    Set Variable If    "${FLAG_DEBUG}"=="0"    ${bak_port}    ${ont['telnet_port']}
    @{counter}=    evaluate    range(10)
    : FOR    ${a}    IN    @{counter}
    \    Run Process    ping    ${ont['ip']}    -c    60    alias=ping_proc
    \    ${ping_result}=    Get Process Result    ping_proc
    \    ${passed} =    Run Keyword And Return Status    Should Contain    ${ping_result.stdout}    60 received, 0% packet loss
    \    Exit For Loop If    ${passed}
    \    Sleep    10s
    Should Be True    ${passed}

EnableTelnet
    l3-selenium2.LoginGUI    ${ont['gui_type']}    ${ont['url_protocol']}://${ont['ip']}    ${ont['gui_username']}    ${ont['gui_password']}
    l3-selenium2.SetTelnet    enable
    l3-selenium2.LogoutGUI    ${ont['gui_type']}    ${ont['url_protocol']}://${ont['ip']}

ConnectionMode3
    Pass Execution If    ${FLAG_DEBUG}==${2}    Skip ConnectionMode3, because FLAG_DEBUG=2.
    l3-selenium2.LoginGUI    ${ont['gui_type']}    ${ont['url_protocol']}://${ont['ip']}    ${ont['gui_username']}    ${ont['gui_password']}
    l3-selenium2.DeleteAllConnection    ${ont['gui_type']}    ${ont['url_protocol']}://${ont['ip']}
    l3-selenium2.CreatePPPoEConnection    ${ont['gui_type']}    ${ont['url_protocol']}://${ont['ip']}    1001    ${pppoe['username']}    ${pppoe['password']}    INTERNET
    l3-selenium2.CreateDHCPConnection    ${ont['gui_type']}    ${ont['url_protocol']}://${ont['ip']}    1002    TR069    VOIP
    sleep    10s
    l3-selenium2.LogoutGUI    ${ont['gui_type']}    ${ont['url_protocol']}://${ont['ip']}

ConfigTR069
    Pass Execution If    ${FLAG_DEBUG}==${2}    Skip ConfigTR069, because FLAG_DEBUG=2.
    SetEnv    ${ont['serial_num']}
    SetInformFlag
    l3-selenium2.LoginGUI    ${ont['gui_type']}    ${ont['url_protocol']}://${ont['ip']}    ${ont['gui_username']}    ${ont['gui_password']}
    l3-selenium2.ConfigTR069    ${ont['gui_type']}    ${ont['url_protocol']}://${ont['ip']}    http://${acs['ip']}:${acs['port']}/ACS    ${acs['s_username']}    ${acs['s_password']}    ${acs['c_username']}
    ...    ${acs['c_password']}
    @{counter}=    evaluate    range(3)
    : FOR    ${a}    IN    @{counter}
    \    sleep    10s
    \    ${status}=    IsInformReceived
    \    Exit For Loop If    ${status}==1
    Run Keyword If    ${status} == 1    l3-selenium2.LogoutGUI    ${ont['gui_type']}    ${ont['url_protocol']}://${ont['ip']}
    Pass Execution If    ${status} == 1    Got TR069 Inform
    l3-selenium2.Reboot    ${ont['gui_type']}    ${ont['url_protocol']}://${ont['ip']}
    l3-selenium2.LogoutGUI    ${ont['gui_type']}    ${ont['url_protocol']}://${ont['ip']}
    @{counter}=    evaluate    range(10)
    : FOR    ${a}    IN    @{counter}
    \    sleep    10s
    \    ${status}=    IsInformReceived
    \    Exit For Loop If    ${status}==1
    Should Be Equal    ${status}    1
    [Teardown]    CloseClient

GetENV
    UpdateEnv

*** Keywords ***
SuiteSetup
    Log Many    test starting
    Create Http Context    localhost:8015

SuiteTeardown
    L1-Selenium2.CloseAllBrowser
    Run Keyword If    '${SUITE STATUS}' == 'PASS'    Set Global Variable    ${startup_status}    PASS
    ...    ELSE    Fatal Error
