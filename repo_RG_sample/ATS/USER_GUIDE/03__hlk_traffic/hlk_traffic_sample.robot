*** Settings ***
Resource          %{ROBOTREPO}/HLK/res/l2-common.txt
Library           Collections
Variables         variable_hlk_traffic.py

*** Test Cases ***
hlk_traffic_sample
    log    %{SETUPFILENAME}
    create global tables    %{SETUPFILENAME}
    &{stc_info}    get table line    TrafficGenData    type=STC
    &{pcta_info}    get table line    TrafficGenData    type=PCTA
    @{TAToolList}    create list    ${pcta_info}    ${stc_info}
    hlk traffic setup    @{TAToolList}    new_traff_cmd_log=True
    hlk traffic set ta type    STC
    ${dUStream1}    Create_Dictionary    stream_name=U1    ta_protocol=IP    vlan1_user_prior=1    vlan1_cfi=1    vlan1_id=10
    ...    vlan2_id=18    mac_src=00:00:00:00:66:00    frame_len=1050    burst_size=3    burst_rate=2
    Comment    ${dUStream1}    Create_Dictionary    stream_name=U1    ta_protocol=IP    vlan1_id=10    vlan2_id=18
    ...    mac_src=00:00:00:00:66:00    frame_len=1050    packet_count=3
    ${dUStream2}    Create_Dictionary    stream_name=U2    ta_protocol=IP    vlan1_id=11    vlan2_id=19    mac_src=00:00:00:00:77:00
    ...    frame_len=1050    bit_rate=100
    ${dUCapture1}    create dictionary    stream_name=U1    packet_count=>=5    mac_src=00:00:00:00:66:00    vlan1_id=10    vlan2_id=18
    ${dUCapture2}    create dictionary    stream_name=U2    packet_count=>=5    mac_src=00:00:00:00:77:00    vlan1_id=11    vlan2_id=19
    ${dUMeasure1}    create dictionary    stream_name=U1    bit_rate=0    bit_rate_tol=5%    expect_result=pass
    ${dUMeasure2}    create dictionary    stream_name=U2    bit_rate=100    bit_rate_tol=5%
    ${dUPortMeasure}    create dictionary    bit_rate=2000    bit_rate_tol=5%    expect_result=pass
    @{lUStreamList}    create list    ${dUStream1}    ${dUStream2}
    @{lUMeasureList}    create list    ${dUMeasure1}    ${dUMeasure2}
    @{lUCaptureList}    create list    ${dUCapture1}    ${dUCapture2}
    ${dDStream1}    Create_Dictionary    stream_name=D1    ta_protocol=IP    vlan1_id=100    mac_src=00:00:00:00:88:00    frame_len=1050
    ...    bit_rate=1000
    ${dDStream2}    Create_Dictionary    stream_name=D2    ta_protocol=IP    vlan1_id=100    mac_src=00:00:00:00:99:00    frame_len=1050
    ...    bit_rate=1000
    ${dDCapture1}    create dictionary    stream_name=D1    packet_count=>=5    mac_src=00:00:00:00:88:00    vlan1_id=100
    ${dDCapture2}    create dictionary    stream_name=D2    packet_count=(1,)    mac_src=00:00:00:00:99:00    expect_result=pass
    ${dDMeasure1}    create dictionary    stream_name=D1    bit_rate=1000    bit_rate_tol=5%
    ${dDMeasure2}    create dictionary    stream_name=D2    bit_rate=1000    bit_rate_tol=5%
    @{lDStreamList}    create list    ${dDStream1}    ${dDStream2}
    @{lDMeasureList}    create list    ${dDMeasure1}    ${dDMeasure2}
    @{lDCaptureList}    create list    ${dDCapture1}    ${dDCapture2}
    run keyword and continue on failure    hlk_traffic    us_send_format=${dUStream1}    ds_send_format=${lDStreamList}    us_capt_opts=${dUCapture1}    ds_capt_opts=${lDCaptureList}    us_meas_opts=${dUMeasure1}
    ...    us_port_meas_opts=${dUPortMeasure}    ds_meas_opts=${lDMeasureList}    send_delay=3    capture_delay=2    measure_duration=5
    hlk traffic set ta type    STC
    hlk_traffic    us_send_format=${dUStream1}    ds_send_format=${lDStreamList}    us_capt_opts=${dUCapture1}    ds_capt_opts=${lDCaptureList}    us_meas_opts=${dUMeasure1}    ds_meas_opts=${lDMeasureList}
    ...    send_delay=3    capture_delay=2    measure_duration=5
    hlk_traffic_cleanup
    Comment    hlk traffic set ta type    PCTA
    Comment    hlk traffic set ta type    STC

hlk_traffic_ipv6
    log    %{SETUPFILENAME}
    create global tables    %{SETUPFILENAME}
    &{stc_info}    get table line    TrafficGenData    type=STC
    &{pcta_info}    get table line    TrafficGenData    type=PCTA
    @{TAToolList}    create list    ${pcta_info}    ${stc_info}
    hlk traffic setup    @{TAToolList}    new_traff_cmd_log=True
    Comment    hlk traffic set ta type    STC
    ${dUStream1}    Create_Dictionary    stream_name=U1    ta_protocol=IPV6    vlan1_user_prior=1    vlan1_id=10    vlan2_id=18
    ...    mac_src=00:00:00:00:66:00    frame_len=1050    ipv6_dest=2222:0000:0000:0000:0000:0000:0000:2223    ipv6_src=2222:0000:0000:0000:0000:0000:0000:2224    tcp_dst_port=86    tcp_src_port=87
    Comment    ${dUStream1}    Create_Dictionary    stream_name=U1    ta_protocol=IP    vlan1_id=10    vlan2_id=18
    ...    mac_src=00:00:00:00:66:00    frame_len=1050    packet_count=3
    ${dUStream2}    Create_Dictionary    stream_name=U2    ta_protocol=IP    vlan1_id=11    vlan2_id=19    mac_src=00:00:00:00:77:00
    ...    frame_len=1050    bit_rate=100
    ${dUCapture1}    create dictionary    stream_name=U1    packet_count=>=5    mac_src=00:00:00:00:66:00    vlan1_id=10    vlan2_id=18
    ...    ipv6_dest=2222:0000:0000:0000:0000:0000:0000:2223    ipv6_src=2222:0000:0000:0000:0000:0000:0000:2224    tcp_dst_port=86    tcp_src_port=87
    ${dUCapture2}    create dictionary    stream_name=U2    packet_count=>=5    mac_src=00:00:00:00:77:00    vlan1_id=11    vlan2_id=19
    ${dUMeasure1}    create dictionary    stream_name=U1    bit_rate=0    bit_rate_tol=5%    expect_result=pass
    ${dUMeasure2}    create dictionary    stream_name=U2    bit_rate=100    bit_rate_tol=5%
    ${dUPortMeasure}    create dictionary    bit_rate=2000    bit_rate_tol=5%    expect_result=pass
    @{lUStreamList}    create list    ${dUStream1}    ${dUStream2}
    @{lUMeasureList}    create list    ${dUMeasure1}    ${dUMeasure2}
    @{lUCaptureList}    create list    ${dUCapture1}    ${dUCapture2}
    ${dDStream1}    Create_Dictionary    stream_name=D1    ta_protocol=IP    vlan1_id=100    mac_src=00:00:00:00:88:00    frame_len=1050
    ...    bit_rate=1000
    ${dDStream2}    Create_Dictionary    stream_name=D2    ta_protocol=IP    vlan1_id=100    mac_src=00:00:00:00:99:00    frame_len=1050
    ...    bit_rate=1000
    ${dDCapture1}    create dictionary    stream_name=D1    packet_count=>=5    mac_src=00:00:00:00:88:00    vlan1_id=100
    ${dDCapture2}    create dictionary    stream_name=D2    packet_count=(1,)    mac_src=00:00:00:00:99:00    expect_result=pass
    ${dDMeasure1}    create dictionary    stream_name=D1    bit_rate=1000    bit_rate_tol=5%
    ${dDMeasure2}    create dictionary    stream_name=D2    bit_rate=1000    bit_rate_tol=5%
    @{lDStreamList}    create list    ${dDStream1}    ${dDStream2}
    @{lDMeasureList}    create list    ${dDMeasure1}    ${dDMeasure2}
    @{lDCaptureList}    create list    ${dDCapture1}    ${dDCapture2}
    Comment    run keyword and continue on failure    hlk_traffic    us_send_format=${dUStream1}    ds_send_format=${lDStreamList}    us_capt_opts=${dUCapture1}    ds_capt_opts=${lDCaptureList}
    ...    us_meas_opts=${dUMeasure1}    us_port_meas_opts=${dUPortMeasure}    ds_meas_opts=${lDMeasureList}    send_delay=3    capture_delay=2    measure_duration=5
    hlk traffic set ta type    PCTA
    run keyword and continue on failure    hlk traffic    us_send_format=${dUStream1}    ds_send_format=${lDStreamList}    us_capt_opts=${dUCapture1}    ds_capt_opts=${lDCaptureList}    send_delay=3
    ...    capture_delay=2    measure_duration=5
    Comment    hlk traffic set ta type    PCTA
    Comment    hlk traffic set ta type    STC
    Comment    run keyword and continue on failure    hlk_traffic    us_send_format=${dUStream1}    us_capt_opts=${dUCapture1}    send_delay=3    capture_delay=2
    ...    measure_duration=5
    hlk_traffic_cleanup

hlk_traffic_setup_and_cleanup
    log    %{SETUPFILENAME}
    create global tables    %{SETUPFILENAME}
    &{stc_info}    get table line    TrafficGenData    type=STC
    &{pcta_info}    get table line    TrafficGenData    type=PCTA
    @{TAToolList}    create list    ${pcta_info}    ${stc_info}
    # setup
    hlk traffic setup    @{TAToolList}
    # set ta_type to STC,default ta_type is PCTA
    hlk traffic set ta type    STC
    # cleanup
    hlk_traffic_cleanup

only_send_traffic
    create global tables    %{SETUPFILENAME}
    &{stc_info}    get table line    TrafficGenData    type=STC
    &{pcta_info}    get table line    TrafficGenData    type=PCTA
    @{TAToolList}    create list    ${pcta_info}    ${stc_info}
    hlk traffic setup    @{TAToolList}    new_traff_cmd_log=True
    # only set us_send_format or ds_send_format
    hlk_traffic    us_send_format=${dUsSendFormat_1}    ds_send_format=${dUsSendFormat_2}
    hlk_traffic_cleanup

hlk_traffic_ip
    create global tables    %{SETUPFILENAME}
    &{stc_info}    get table line    TrafficGenData    type=STC
    Comment    &{pcta_info}    get table line    TrafficGenData    type=PCTA
    @{TAToolList}    create list    ${stc_info}
    hlk traffic setup    @{TAToolList}    new_traff_cmd_log=True
    # set xx_send_format and xx_capt_opts
    hlk_traffic    us_send_format=${dUsSendFormat_1}    ds_send_format=${dDsSendFormat_1}    us_capt_opts=${dUsCaptOpts_1}    ds_capt_opts=${dDsCaptOpts_1}    save_pcap_file=true
    hlk_traffic_cleanup

send_capture_measure
    create global tables    %{SETUPFILENAME}
    &{stc_info}    get table line    TrafficGenData    type=STC
    &{pcta_info}    get table line    TrafficGenData    type=PCTA
    @{TAToolList}    create list    ${pcta_info}    ${stc_info}
    hlk traffic setup    @{TAToolList}    new_traff_cmd_log=True
    #add bit_rate in send_format and measure_opts
    set to dictionary    ${dUsSendFormat_1}    bit_rate=1000
    set to dictionary    ${dUsMeasOpts_1}    bit_rate=1000    bit_rate_tol=20%
    set to dictionary    ${dDsSendFormat_1}    bit_rate=500
    set to dictionary    ${dDsMeasOpts_1}    bit_rate=500    bit_rate_tol=20%
    # set xx_send_format and xx_capt_opts
    hlk_traffic    us_send_format=${dUsSendFormat_1}    ds_send_format=${dDsSendFormat_1}    us_capt_opts=${dUsCaptOpts_1}    ds_capt_opts=${dDsCaptOpts_1}    us_meas_opts=${dUsMeasOpts_1}    ds_meas_opts=${dDsMeasOpts_1}
    hlk_traffic_cleanup

only_check_upstream
    create global tables    %{SETUPFILENAME}
    &{stc_info}    get table line    TrafficGenData    type=STC
    &{pcta_info}    get table line    TrafficGenData    type=PCTA
    @{TAToolList}    create list    ${pcta_info}    ${stc_info}
    hlk traffic setup    @{TAToolList}    new_traff_cmd_log=True
    set to dictionary    ${dUsSendFormat_1}    bit_rate=1000
    set to dictionary    ${dUsMeasOpts_1}    bit_rate=1000    bit_rate_tol=20%
    # only set the parameter of upstream
    hlk_traffic    us_send_format=${dUsSendFormat_1}    us_capt_opts=${dUsCaptOpts_1}    us_meas_opts=${dUsMeasOpts_1}
    hlk_traffic_cleanup

set_delay_times
    create global tables    %{SETUPFILENAME}
    &{stc_info}    get table line    TrafficGenData    type=STC
    &{pcta_info}    get table line    TrafficGenData    type=PCTA
    @{TAToolList}    create list    ${pcta_info}    ${stc_info}
    hlk traffic setup    @{TAToolList}    new_traff_cmd_log=True
    set to dictionary    ${dUsSendFormat_1}    bit_rate=1000
    set to dictionary    ${dUsMeasOpts_1}    bit_rate=1000    bit_rate_tol=20%
    # set send_delay,capture_delay and    measure_duration
    hlk_traffic    us_send_format=${dUsSendFormat_1}    us_capt_opts=${dUsCaptOpts_1}    us_meas_opts=${dUsMeasOpts_1}    send_delay=3    capture_delay=2    measure_duration=25
    hlk_traffic_cleanup

expect_fail
    create global tables    %{SETUPFILENAME}
    &{stc_info}    get table line    TrafficGenData    type=STC
    &{pcta_info}    get table line    TrafficGenData    type=PCTA
    @{TAToolList}    create list    ${pcta_info}    ${stc_info}
    hlk traffic setup    @{TAToolList}    new_traff_cmd_log=True
    set to dictionary    ${dUsSendFormat_1}    bit_rate=1000
    set to dictionary    ${dUsMeasOpts_1}    bit_rate=1000    bit_rate_tol=20%
    # only set the parameter of upstream
    hlk_traffic    us_send_format=${dUsSendFormat_1}    us_capt_opts=${dUsCaptOpts_1}    us_meas_opts=${dUsMeasOpts_1}    send_delay=3    capture_delay=2    measure_duration=25
    hlk_traffic_cleanup

multiple_streams

hlk_traffic_eth
    log    %{SETUPFILENAME}
    create global tables    %{SETUPFILENAME}
    &{stc_info}    get table line    TrafficGenData    type=STC
    &{pcta_info}    get table line    TrafficGenData    type=PCTA
    @{TAToolList}    create list    ${pcta_info}    ${stc_info}
    hlk traffic setup    @{TAToolList}    new_traff_cmd_log=True
    Comment    hlk traffic set ta type    STC
    ${dUStream1}    Create_Dictionary    stream_name=U1    ta_protocol=eth    vlan1_user_prior=1    vlan1_id=10    vlan2_id=18
    ...    mac_src=00:00:00:00:66:00    frame_len=1050    type=809B
    Comment    ${dUStream1}    Create_Dictionary    stream_name=U1    ta_protocol=IP    vlan1_id=10    vlan2_id=18
    ...    mac_src=00:00:00:00:66:00    frame_len=1050    packet_count=3
    ${dUStream2}    Create_Dictionary    stream_name=U2    ta_protocol=IP    vlan1_id=11    vlan2_id=19    mac_src=00:00:00:00:77:00
    ...    frame_len=1050    bit_rate=100
    ${dUCapture1}    create dictionary    stream_name=U1    packet_count=>=1    mac_src=00:00:00:00:66:00    vlan1_id=10    vlan2_id=18
    ...    vlan1_user_prior=1
    ${dUCapture2}    create dictionary    stream_name=U2    packet_count=>=5    mac_src=00:00:00:00:77:00    vlan1_id=11    vlan2_id=19
    ${dUMeasure1}    create dictionary    stream_name=U1    bit_rate=0    bit_rate_tol=5%    expect_result=pass
    ${dUMeasure2}    create dictionary    stream_name=U2    bit_rate=100    bit_rate_tol=5%
    ${dUPortMeasure}    create dictionary    bit_rate=2000    bit_rate_tol=5%    expect_result=pass
    @{lUStreamList}    create list    ${dUStream1}    ${dUStream2}
    @{lUMeasureList}    create list    ${dUMeasure1}    ${dUMeasure2}
    @{lUCaptureList}    create list    ${dUCapture1}    ${dUCapture2}
    ${dDStream1}    Create_Dictionary    stream_name=D1    ta_protocol=IP    vlan1_id=100    mac_src=00:00:00:00:88:00    frame_len=1050
    ...    bit_rate=1000
    ${dDStream2}    Create_Dictionary    stream_name=D2    ta_protocol=IP    vlan1_id=100    mac_src=00:00:00:00:99:00    frame_len=1050
    ...    bit_rate=1000
    ${dDCapture1}    create dictionary    stream_name=D1    packet_count=>=5    mac_src=00:00:00:00:88:00    vlan1_id=100
    ${dDCapture2}    create dictionary    stream_name=D2    packet_count=(1,)    mac_src=00:00:00:00:99:00    expect_result=pass
    ${dDMeasure1}    create dictionary    stream_name=D1    bit_rate=1000    bit_rate_tol=5%
    ${dDMeasure2}    create dictionary    stream_name=D2    bit_rate=1000    bit_rate_tol=5%
    @{lDStreamList}    create list    ${dDStream1}    ${dDStream2}
    @{lDMeasureList}    create list    ${dDMeasure1}    ${dDMeasure2}
    @{lDCaptureList}    create list    ${dDCapture1}    ${dDCapture2}
    Comment    run keyword and continue on failure    hlk_traffic    us_send_format=${dUStream1}    ds_send_format=${lDStreamList}    us_capt_opts=${dUCapture1}    ds_capt_opts=${lDCaptureList}
    ...    us_meas_opts=${dUMeasure1}    us_port_meas_opts=${dUPortMeasure}    ds_meas_opts=${lDMeasureList}    send_delay=3    capture_delay=2    measure_duration=5
    hlk traffic set ta type    PCTA
    run keyword and continue on failure    hlk_traffic    us_send_format=${dUStream1}    us_capt_opts=${dUCapture1}    send_delay=3    capture_delay=2    measure_duration=5
    ...    save_pcap_file=test
    hlk traffic set ta type    STC
    run keyword and continue on failure    hlk_traffic    us_send_format=${dUStream1}    us_capt_opts=${dUCapture1}    send_delay=3    capture_delay=2    measure_duration=5
    hlk_traffic_cleanup

hlk_traffic_udp
    log    %{SETUPFILENAME}
    create global tables    %{SETUPFILENAME}
    &{stc_info}    get table line    TrafficGenData    type=STC
    &{pcta_info}    get table line    TrafficGenData    type=PCTA
    @{TAToolList}    create list    ${pcta_info}    ${stc_info}
    hlk traffic setup    @{TAToolList}    new_traff_cmd_log=True
    Comment    hlk traffic set ta type    STC
    ${dUStream1}    Create_Dictionary    stream_name=U1    ta_protocol=udp    vlan1_user_prior=1    vlan1_id=10    vlan2_id=18
    ...    mac_src=00:00:00:00:66:00    packet_count=2000
    Comment    ${dUStream1}    Create_Dictionary    stream_name=U1    ta_protocol=IP    vlan1_id=10    vlan2_id=18
    ...    mac_src=00:00:00:00:66:00    frame_len=1050    packet_count=3
    ${dUStream2}    Create_Dictionary    stream_name=U2    ta_protocol=IP    vlan1_id=11    vlan2_id=19    mac_src=00:00:00:00:77:00
    ...    frame_len=1050    bit_rate=100
    ${dUCapture1}    create dictionary    stream_name=U1    packet_count=>=5
    ${dUCapture2}    create dictionary    stream_name=U2    packet_count=>=5    mac_src=00:00:00:00:77:00    vlan1_id=11    vlan2_id=19
    ${dUMeasure1}    create dictionary    stream_name=U1    bit_rate=0    bit_rate_tol=5%    expect_result=pass
    ${dUMeasure2}    create dictionary    stream_name=U2    bit_rate=100    bit_rate_tol=5%
    ${dUPortMeasure}    create dictionary    bit_rate=2000    bit_rate_tol=5%    expect_result=pass
    @{lUStreamList}    create list    ${dUStream1}    ${dUStream2}
    @{lUMeasureList}    create list    ${dUMeasure1}    ${dUMeasure2}
    @{lUCaptureList}    create list    ${dUCapture1}    ${dUCapture2}
    ${dDStream1}    Create_Dictionary    stream_name=D1    ta_protocol=IP    vlan1_id=100    mac_src=00:00:00:00:88:00    frame_len=1050
    ...    bit_rate=1000
    ${dDStream2}    Create_Dictionary    stream_name=D2    ta_protocol=IP    vlan1_id=100    mac_src=00:00:00:00:99:00    frame_len=1050
    ...    bit_rate=1000
    ${dDCapture1}    create dictionary    stream_name=D1    packet_count=>=5    mac_src=00:00:00:00:88:00    vlan1_id=100
    ${dDCapture2}    create dictionary    stream_name=D2    packet_count=(1,)    mac_src=00:00:00:00:99:00    expect_result=pass
    ${dDMeasure1}    create dictionary    stream_name=D1    bit_rate=1000    bit_rate_tol=5%
    ${dDMeasure2}    create dictionary    stream_name=D2    bit_rate=1000    bit_rate_tol=5%
    @{lDStreamList}    create list    ${dDStream1}    ${dDStream2}
    @{lDMeasureList}    create list    ${dDMeasure1}    ${dDMeasure2}
    @{lDCaptureList}    create list    ${dDCapture1}    ${dDCapture2}
    Comment    run keyword and continue on failure    hlk_traffic    us_send_format=${dUStream1}    ds_send_format=${lDStreamList}    us_capt_opts=${dUCapture1}    ds_capt_opts=${lDCaptureList}
    ...    us_meas_opts=${dUMeasure1}    us_port_meas_opts=${dUPortMeasure}    ds_meas_opts=${lDMeasureList}    send_delay=3    capture_delay=2    measure_duration=5
    hlk traffic set ta type    PCTA
    run keyword and continue on failure    hlk_traffic    us_send_format=${dUStream1}    us_capt_opts=${dUCapture1}    send_delay=3    capture_delay=2
    hlk traffic set ta type    STC
    run keyword and continue on failure    hlk_traffic    us_send_format=${dUStream1}    us_capt_opts=${dUCapture1}    send_delay=3    capture_delay=2
    hlk_traffic_cleanup

hlk_traffic_tcp
    log    %{SETUPFILENAME}
    create global tables    %{SETUPFILENAME}
    &{stc_info}    get table line    TrafficGenData    type=STC
    Comment    &{pcta_info}    get table line    TrafficGenData    type=PCTA
    @{TAToolList}    create list    ${stc_info}
    hlk traffic setup    @{TAToolList}    new_traff_cmd_log=True
    Comment    hlk traffic set ta type    STC
    ${dUStream1}    Create_Dictionary    stream_name=U1    ta_protocol=tcp    vlan1_id=10    vlan2_id=18    mac_src=00:00:00:00:66:00
    ...    packet_count=1
    Comment    ${dUStream1}    Create_Dictionary    stream_name=U1    ta_protocol=IP    vlan1_id=10    vlan2_id=18
    ...    mac_src=00:00:00:00:66:00    frame_len=1050    packet_count=3
    ${dUStream2}    Create_Dictionary    stream_name=U2    ta_protocol=IP    vlan1_id=11    vlan2_id=19    mac_src=00:00:00:00:77:00
    ...    frame_len=1050    bit_rate=100
    ${dUCapture1}    create dictionary    stream_name=U1    packet_count=>=1    vlan1_id=10    vlan2_id=18    mac_src=00:00:00:00:66:00
    ${dUCapture2}    create dictionary    stream_name=U2    packet_count=>=5    mac_src=00:00:00:00:77:00    vlan1_id=11    vlan2_id=19
    ${dUMeasure1}    create dictionary    stream_name=U1    bit_rate=0    bit_rate_tol=5%    expect_result=pass
    ${dUMeasure2}    create dictionary    stream_name=U2    bit_rate=100    bit_rate_tol=5%
    ${dUPortMeasure}    create dictionary    bit_rate=2000    bit_rate_tol=5%    expect_result=pass
    @{lUStreamList}    create list    ${dUStream1}    ${dUStream2}
    @{lUMeasureList}    create list    ${dUMeasure1}    ${dUMeasure2}
    @{lUCaptureList}    create list    ${dUCapture1}    ${dUCapture2}
    ${dDStream1}    Create_Dictionary    stream_name=D1    ta_protocol=IP    vlan1_id=100    mac_src=00:00:00:00:88:00    frame_len=1050
    ...    bit_rate=1000
    ${dDStream2}    Create_Dictionary    stream_name=D2    ta_protocol=IP    vlan1_id=100    mac_src=00:00:00:00:99:00    frame_len=1050
    ...    bit_rate=1000
    ${dDCapture1}    create dictionary    stream_name=D1    packet_count=>=5    mac_src=00:00:00:00:88:00    vlan1_id=100
    ${dDCapture2}    create dictionary    stream_name=D2    packet_count=(1,)    mac_src=00:00:00:00:99:00    expect_result=pass
    ${dDMeasure1}    create dictionary    stream_name=D1    bit_rate=1000    bit_rate_tol=5%
    ${dDMeasure2}    create dictionary    stream_name=D2    bit_rate=1000    bit_rate_tol=5%
    @{lDStreamList}    create list    ${dDStream1}    ${dDStream2}
    @{lDMeasureList}    create list    ${dDMeasure1}    ${dDMeasure2}
    @{lDCaptureList}    create list    ${dDCapture1}    ${dDCapture2}
    Comment    run keyword and continue on failure    hlk_traffic    us_send_format=${dUStream1}    ds_send_format=${lDStreamList}    us_capt_opts=${dUCapture1}    ds_capt_opts=${lDCaptureList}
    ...    us_meas_opts=${dUMeasure1}    us_port_meas_opts=${dUPortMeasure}    ds_meas_opts=${lDMeasureList}    send_delay=3    capture_delay=2    measure_duration=5
    Comment    hlk traffic set ta type    PCTA
    Comment    run keyword and continue on failure    hlk_traffic    us_send_format=${dUStream1}    us_capt_opts=${dUCapture1}    send_delay=3    capture_delay=2
    hlk traffic set ta type    STC
    run keyword and continue on failure    hlk_traffic    us_send_format=${dUStream1}    us_capt_opts=${dUCapture1}    send_delay=3    capture_delay=2
    hlk_traffic_cleanup

hlk_traffic_cfm_lbr
    create global tables    %{SETUPFILENAME}
    &{stc_info}    get table line    TrafficGenData    type=STC
    &{pcta_info}    get table line    TrafficGenData    type=PCTA
    @{TAToolList}    create list    ${pcta_info}    ${stc_info}
    hlk traffic setup    @{TAToolList}    new_traff_cmd_log=True
    # set xx_send_format and xx_capt_opts
    hlk traffic set ta type    STC
    set to dictionary    ${dUsSendFormat_1}    mdLevel=5    opcodecfm=2    lbtid=4    ta_protocol=cfm    packet_count=1
    Comment    set to dictionary    ${dUsCaptOpts_1}    ta_protocol=cfm
    hlk_traffic    us_send_format=${dUsSendFormat_1}    us_capt_opts=${dUsCaptOpts_1}
    hlk_traffic_cleanup

hlk_traffic_cfm_lbm
    create global tables    %{SETUPFILENAME}
    &{stc_info}    get table line    TrafficGenData    type=STC
    &{pcta_info}    get table line    TrafficGenData    type=PCTA
    @{TAToolList}    create list    ${pcta_info}    ${stc_info}
    hlk traffic setup    @{TAToolList}    new_traff_cmd_log=True
    # set xx_send_format and xx_capt_opts
    hlk traffic set ta type    STC
    set to dictionary    ${dUsSendFormat_1}    mdLevel=5    opcodecfm=3    lbtid=4    ta_protocol=cfm
    set to dictionary    ${dUsCaptOpts_1}    ta_protocol=cfm    mdLevel=5    opcodecfm=3    lbtid=4
    hlk_traffic    us_send_format=${dUsSendFormat_1}    us_capt_opts=${dUsCaptOpts_1}
    hlk_traffic_cleanup

hlk_traffic_cfm_ccm
    create global tables    %{SETUPFILENAME}
    &{stc_info}    get table line    TrafficGenData    type=STC
    &{pcta_info}    get table line    TrafficGenData    type=PCTA
    @{TAToolList}    create list    ${pcta_info}    ${stc_info}
    hlk traffic setup    @{TAToolList}    new_traff_cmd_log=True
    # set xx_send_format and xx_capt_opts
    hlk traffic set ta type    STC
    set to dictionary    ${dUsSendFormat_1}    mdLevel=5    opcodecfm=1    maepi=1    ta_protocol=cfm    mdnf=4
    ...    smaf=2    sman=6d61323030    mdn=6d6435    sequencenumber=31900    padding=000000000000000000000000000000000000000000000000000000000000000000000000
    set to dictionary    ${dUsCaptOpts_1}    ta_protocol=cfm    mdLevel=5    opcodecfm=1    maepi=1    mdnf=4
    ...    smaf=2    sequencenumber=31900    sman=6d61323030    mdn=6d6435
    hlk_traffic    us_send_format=${dUsSendFormat_1}    us_capt_opts=${dUsCaptOpts_1}
    hlk_traffic_cleanup

hlk_traffic_arp
    log    %{SETUPFILENAME}
    create global tables    %{SETUPFILENAME}
    &{stc_info}    get table line    TrafficGenData    type=STC
    Comment    &{pcta_info}    get table line    TrafficGenData    type=PCTA
    @{TAToolList}    create list    ${stc_info}
    hlk traffic setup    @{TAToolList}    new_traff_cmd_log=True
    Comment    hlk traffic set ta type    STC
    ${dUStream1}    Create_Dictionary    stream_name=U1    ta_protocol=arp    vlan1_user_prior=1    vlan1_id=10    vlan2_id=18
    ...    mac_src=00:00:00:00:66:00    mac_dest=00:88:00:00:00:00    opcode=2    hw_addr_dest=00:88:00:00:00:00    hw_type=2    hw_addr_src=00:99:00:00:00:00
    Comment    ${dUStream1}    Create_Dictionary    stream_name=U1    ta_protocol=IP    vlan1_id=10    vlan2_id=18
    ...    mac_src=00:00:00:00:66:00    frame_len=1050    packet_count=3
    ${dUStream2}    Create_Dictionary    stream_name=U2    ta_protocol=IP    vlan1_id=11    vlan2_id=19    mac_src=00:00:00:00:77:00
    ...    frame_len=1050    bit_rate=100
    ${dUCapture1}    create dictionary    stream_name=U1    packet_count=>=1    mac_src=00:00:00:00:66:00    vlan1_id=10    vlan2_id=18
    ...    mac_src=00:00:00:00:66:00    mac_dest=00:88:00:00:00:00    opcode=2
    ${dUCapture2}    create dictionary    stream_name=U2    packet_count=>=1    mac_src=00:00:00:00:77:00    vlan1_id=11    vlan2_id=19
    ${dUMeasure1}    create dictionary    stream_name=U1    bit_rate=0    bit_rate_tol=5%    expect_result=pass
    ${dUMeasure2}    create dictionary    stream_name=U2    bit_rate=100    bit_rate_tol=5%
    ${dUPortMeasure}    create dictionary    bit_rate=2000    bit_rate_tol=5%    expect_result=pass
    @{lUStreamList}    create list    ${dUStream1}    ${dUStream2}
    @{lUMeasureList}    create list    ${dUMeasure1}    ${dUMeasure2}
    @{lUCaptureList}    create list    ${dUCapture1}    ${dUCapture2}
    ${dDStream1}    Create_Dictionary    stream_name=D1    ta_protocol=IP    vlan1_id=100    mac_src=00:00:00:00:88:00    frame_len=1050
    ...    bit_rate=1000
    ${dDStream2}    Create_Dictionary    stream_name=D2    ta_protocol=IP    vlan1_id=100    mac_src=00:00:00:00:99:00    frame_len=1050
    ...    bit_rate=1000
    ${dDCapture1}    create dictionary    stream_name=D1    packet_count=>=5    mac_src=00:00:00:00:88:00    vlan1_id=100
    ${dDCapture2}    create dictionary    stream_name=D2    packet_count=(1,)    mac_src=00:00:00:00:99:00    expect_result=pass
    ${dDMeasure1}    create dictionary    stream_name=D1    bit_rate=1000    bit_rate_tol=5%
    ${dDMeasure2}    create dictionary    stream_name=D2    bit_rate=1000    bit_rate_tol=5%
    @{lDStreamList}    create list    ${dDStream1}    ${dDStream2}
    @{lDMeasureList}    create list    ${dDMeasure1}    ${dDMeasure2}
    @{lDCaptureList}    create list    ${dDCapture1}    ${dDCapture2}
    Comment    run keyword and continue on failure    hlk_traffic    us_send_format=${dUStream1}    ds_send_format=${lDStreamList}    us_capt_opts=${dUCapture1}    ds_capt_opts=${lDCaptureList}
    ...    us_meas_opts=${dUMeasure1}    us_port_meas_opts=${dUPortMeasure}    ds_meas_opts=${lDMeasureList}    send_delay=3    capture_delay=2    measure_duration=5
    Comment    hlk traffic set ta type    PCTA
    Comment    run keyword and continue on failure    hlk_traffic    us_send_format=${dUStream1}    us_capt_opts=${dUCapture1}    send_delay=3    capture_delay=2
    ...    measure_duration=5
    hlk traffic set ta type    STC
    Comment    run keyword and continue on failure    hlk_traffic    us_send_format=${dUStream1}    us_capt_opts=${dUCapture1}    send_delay=3    capture_delay=2
    ...    measure_duration=5
    hlk_traffic    us_send_format=${dUStream1}    us_capt_opts=${dUCapture1}    send_delay=3    capture_delay=2    measure_duration=5
    hlk_traffic_cleanup

hlk_traffic_rarp
    log    %{SETUPFILENAME}
    create global tables    %{SETUPFILENAME}
    &{stc_info}    get table line    TrafficGenData    type=STC
    &{pcta_info}    get table line    TrafficGenData    type=PCTA
    @{TAToolList}    create list    ${pcta_info}    ${stc_info}
    hlk traffic setup    @{TAToolList}    new_traff_cmd_log=True
    Comment    hlk traffic set ta type    STC
    ${dUStream1}    Create_Dictionary    stream_name=U1    ta_protocol=rarp    vlan1_user_prior=1    vlan1_id=10    vlan2_id=18
    ...    mac_src=00:00:00:00:66:00    mac_dest=00:88:00:00:00:00    prot_addr_src=100.66.88.22    prot_addr_dest=22.44.66.88    opcode=3
    Comment    ${dUStream1}    Create_Dictionary    stream_name=U1    ta_protocol=IP    vlan1_id=10    vlan2_id=18
    ...    mac_src=00:00:00:00:66:00    frame_len=1050
    ${dUStream2}    Create_Dictionary    stream_name=U2    ta_protocol=IP    vlan1_id=11    vlan2_id=19    mac_src=00:00:00:00:77:00
    ...    frame_len=1050    bit_rate=100
    ${dUCapture1}    create dictionary    stream_name=U1    packet_count=>=1    mac_src=00:00:00:00:66:00    vlan1_id=10    vlan2_id=18
    ...    mac_src=00:00:00:00:66:00    mac_dest=00:88:00:00:00:00    opcode=3
    ${dUCapture2}    create dictionary    stream_name=U2    packet_count=>=5    mac_src=00:00:00:00:77:00    vlan1_id=11    vlan2_id=19
    ${dUMeasure1}    create dictionary    stream_name=U1    bit_rate=0    bit_rate_tol=5%    expect_result=pass
    ${dUMeasure2}    create dictionary    stream_name=U2    bit_rate=100    bit_rate_tol=5%
    ${dUPortMeasure}    create dictionary    bit_rate=2000    bit_rate_tol=5%    expect_result=pass
    @{lUStreamList}    create list    ${dUStream1}    ${dUStream2}
    @{lUMeasureList}    create list    ${dUMeasure1}    ${dUMeasure2}
    @{lUCaptureList}    create list    ${dUCapture1}    ${dUCapture2}
    ${dDStream1}    Create_Dictionary    stream_name=D1    ta_protocol=IP    vlan1_id=100    mac_src=00:00:00:00:88:00    frame_len=1050
    ...    bit_rate=1000
    ${dDStream2}    Create_Dictionary    stream_name=D2    ta_protocol=IP    vlan1_id=100    mac_src=00:00:00:00:99:00    frame_len=1050
    ...    bit_rate=1000
    ${dDCapture1}    create dictionary    stream_name=D1    packet_count=>=5    mac_src=00:00:00:00:88:00    vlan1_id=100
    ${dDCapture2}    create dictionary    stream_name=D2    packet_count=(1,)    mac_src=00:00:00:00:99:00    expect_result=pass
    ${dDMeasure1}    create dictionary    stream_name=D1    bit_rate=1000    bit_rate_tol=5%
    ${dDMeasure2}    create dictionary    stream_name=D2    bit_rate=1000    bit_rate_tol=5%
    @{lDStreamList}    create list    ${dDStream1}    ${dDStream2}
    @{lDMeasureList}    create list    ${dDMeasure1}    ${dDMeasure2}
    @{lDCaptureList}    create list    ${dDCapture1}    ${dDCapture2}
    Comment    run keyword and continue on failure    hlk_traffic    us_send_format=${dUStream1}    ds_send_format=${lDStreamList}    us_capt_opts=${dUCapture1}    ds_capt_opts=${lDCaptureList}
    ...    us_meas_opts=${dUMeasure1}    us_port_meas_opts=${dUPortMeasure}    ds_meas_opts=${lDMeasureList}    send_delay=3    capture_delay=2    measure_duration=5
    hlk traffic set ta type    PCTA
    run keyword and continue on failure    hlk_traffic    us_send_format=${dUStream1}    us_capt_opts=${dUCapture1}    send_delay=3    capture_delay=2    measure_duration=5
    hlk traffic set ta type    STC
    run keyword and continue on failure    hlk_traffic    us_send_format=${dUStream1}    us_capt_opts=${dUCapture1}    send_delay=3    capture_delay=2    measure_duration=5
    hlk_traffic_cleanup

hlk_traffic_igmp
    log    %{SETUPFILENAME}
    create global tables    %{SETUPFILENAME}
    &{stc_info}    get table line    TrafficGenData    type=STC
    &{pcta_info}    get table line    TrafficGenData    type=PCTA
    @{TAToolList}    create list    ${pcta_info}    ${stc_info}
    hlk traffic setup    @{TAToolList}    new_traff_cmd_log=True
    Comment    hlk traffic set ta type    STC
    ${dUStream1}    Create_Dictionary    stream_name=U1    ta_protocol=igmp    vlan1_id=10    vlan2_id=18    ip_src=10.10.10.10
    ...    ip_dest=20.20.20.20    igmp_version=V3    packet_count=1    igmp_type=16    nb_of_group_records=1    group_record_list=04000000e0010101
    Comment    ${dUStream1}    Create_Dictionary    stream_name=U1    ta_protocol=IP    vlan1_id=10    vlan2_id=18
    ...    mac_src=00:00:00:00:66:00    frame_len=1050
    ${dUStream2}    Create_Dictionary    stream_name=U2    ta_protocol=IP    vlan1_id=11    vlan2_id=19    mac_src=00:00:00:00:77:00
    ...    frame_len=1050    bit_rate=100
    ${dUCapture1}    create dictionary    stream_name=U1    packet_count=>=1    vlan1_id=10    vlan2_id=18
    ${dUCapture2}    create dictionary    stream_name=U2    packet_count=>=5    mac_src=00:00:00:00:77:00    vlan1_id=11    vlan2_id=19
    ${dUMeasure1}    create dictionary    stream_name=U1    bit_rate=0    bit_rate_tol=5%    expect_result=pass
    ${dUMeasure2}    create dictionary    stream_name=U2    bit_rate=100    bit_rate_tol=5%
    ${dUPortMeasure}    create dictionary    bit_rate=2000    bit_rate_tol=5%    expect_result=pass
    @{lUStreamList}    create list    ${dUStream1}    ${dUStream2}
    @{lUMeasureList}    create list    ${dUMeasure1}    ${dUMeasure2}
    @{lUCaptureList}    create list    ${dUCapture1}    ${dUCapture2}
    ${dDStream1}    Create_Dictionary    stream_name=D1    ta_protocol=IP    vlan1_id=100    mac_src=00:00:00:00:88:00    frame_len=1050
    ...    bit_rate=1000
    ${dDStream2}    Create_Dictionary    stream_name=D2    ta_protocol=IP    vlan1_id=100    mac_src=00:00:00:00:99:00    frame_len=1050
    ...    bit_rate=1000
    ${dDCapture1}    create dictionary    stream_name=D1    packet_count=>=5    mac_src=00:00:00:00:88:00    vlan1_id=100
    ${dDCapture2}    create dictionary    stream_name=D2    packet_count=(1,)    mac_src=00:00:00:00:99:00    expect_result=pass
    ${dDMeasure1}    create dictionary    stream_name=D1    bit_rate=1000    bit_rate_tol=5%
    ${dDMeasure2}    create dictionary    stream_name=D2    bit_rate=1000    bit_rate_tol=5%
    @{lDStreamList}    create list    ${dDStream1}    ${dDStream2}
    @{lDMeasureList}    create list    ${dDMeasure1}    ${dDMeasure2}
    @{lDCaptureList}    create list    ${dDCapture1}    ${dDCapture2}
    Comment    run keyword and continue on failure    hlk_traffic    us_send_format=${dUStream1}    ds_send_format=${lDStreamList}    us_capt_opts=${dUCapture1}    ds_capt_opts=${lDCaptureList}
    ...    us_meas_opts=${dUMeasure1}    us_port_meas_opts=${dUPortMeasure}    ds_meas_opts=${lDMeasureList}    send_delay=3    capture_delay=2    measure_duration=5
    hlk traffic set ta type    PCTA
    run keyword and continue on failure    hlk_traffic    us_send_format=${dUStream1}    us_capt_opts=${dUCapture1}    send_delay=3    capture_delay=2    measure_duration=5
    hlk traffic set ta type    STC
    run keyword and continue on failure    hlk_traffic    us_send_format=${dUStream1}    us_capt_opts=${dUCapture1}    send_delay=3    capture_delay=2    measure_duration=5
    hlk_traffic_cleanup

hlk_traffic_dhcp
    log    %{SETUPFILENAME}
    create global tables    %{SETUPFILENAME}
    &{stc_info}    get table line    TrafficGenData    type=STC
    &{pcta_info}    get table line    TrafficGenData    type=PCTA
    @{TAToolList}    create list    ${pcta_info}    ${stc_info}
    hlk traffic setup    @{TAToolList}    new_traff_cmd_log=True
    Comment    hlk traffic set ta type    STC
    ${dUStream1}    Create_Dictionary    stream_name=U1    ta_protocol=dhcp    vlan1_id=10    vlan2_id=18    ip_src=10.10.10.10
    ...    ip_dest=20.20.20.20    packet_count=1    bootp_type=BOOT_REQUEST    eth_type=ETH    dhcp_message_type=1    chaddr=000000600000
    ...    mac_src=00:00:00:00:60:00
    Comment    ${dUStream1}    Create_Dictionary    stream_name=U1    ta_protocol=IP    vlan1_id=10    vlan2_id=18
    ...    mac_src=00:00:00:00:66:00    frame_len=1050    dhcp_options=370501060f212cff    bootp_dhcp=DHCP    bootp_type=BOOT_REQUEST    eth_type=ETH
    ...    dhcp_message_type=1    opt82_agent_remote_id=DUMMY    opt82_agent_circuit_id=FFFF    opt82_vendor_specific_info=FFFF
    ${dUStream2}    Create_Dictionary    stream_name=U2    ta_protocol=IP    vlan1_id=11    vlan2_id=19    mac_src=00:00:00:00:77:00
    ...    frame_len=1050    bit_rate=100
    ${dUCapture1}    create dictionary    stream_name=U1    packet_count=>=1    bootp_dhcp=DHCP
    ${dUCapture2}    create dictionary    stream_name=U2    packet_count=>=5    mac_src=00:00:00:00:77:00    vlan1_id=11    vlan2_id=19
    ${dUMeasure1}    create dictionary    stream_name=U1    bit_rate=0    bit_rate_tol=5%    expect_result=pass
    ${dUMeasure2}    create dictionary    stream_name=U2    bit_rate=100    bit_rate_tol=5%
    ${dUPortMeasure}    create dictionary    bit_rate=2000    bit_rate_tol=5%    expect_result=pass
    @{lUStreamList}    create list    ${dUStream1}    ${dUStream2}
    @{lUMeasureList}    create list    ${dUMeasure1}    ${dUMeasure2}
    @{lUCaptureList}    create list    ${dUCapture1}    ${dUCapture2}
    ${dDStream1}    Create_Dictionary    stream_name=D1    ta_protocol=IP    vlan1_id=100    mac_src=00:00:00:00:88:00    frame_len=1050
    ...    bit_rate=1000
    ${dDStream2}    Create_Dictionary    stream_name=D2    ta_protocol=IP    vlan1_id=100    mac_src=00:00:00:00:99:00    frame_len=1050
    ...    bit_rate=1000
    ${dDCapture1}    create dictionary    stream_name=D1    packet_count=>=5    mac_src=00:00:00:00:88:00    vlan1_id=100
    ${dDCapture2}    create dictionary    stream_name=D2    packet_count=(1,)    mac_src=00:00:00:00:99:00    expect_result=pass
    ${dDMeasure1}    create dictionary    stream_name=D1    bit_rate=1000    bit_rate_tol=5%
    ${dDMeasure2}    create dictionary    stream_name=D2    bit_rate=1000    bit_rate_tol=5%
    @{lDStreamList}    create list    ${dDStream1}    ${dDStream2}
    @{lDMeasureList}    create list    ${dDMeasure1}    ${dDMeasure2}
    @{lDCaptureList}    create list    ${dDCapture1}    ${dDCapture2}
    Comment    run keyword and continue on failure    hlk_traffic    us_send_format=${dUStream1}    ds_send_format=${lDStreamList}    us_capt_opts=${dUCapture1}    ds_capt_opts=${lDCaptureList}
    ...    us_meas_opts=${dUMeasure1}    us_port_meas_opts=${dUPortMeasure}    ds_meas_opts=${lDMeasureList}    send_delay=3    capture_delay=2    measure_duration=5
    Comment    hlk traffic set ta type    PCTA
    Comment    run keyword and continue on failure    hlk_traffic    us_send_format=${dUStream1}    us_capt_opts=${dUCapture1}    send_delay=3    capture_delay=2
    ...    measure_duration=5
    hlk traffic set ta type    STC
    run keyword and continue on failure    hlk_traffic    us_send_format=${dUStream1}    us_capt_opts=${dUCapture1}    send_delay=3    capture_delay=2    measure_duration=5
    hlk_traffic_cleanup

hlk_traffic_bcmp
    log    %{SETUPFILENAME}
    create global tables    %{SETUPFILENAME}
    &{stc_info}    get table line    TrafficGenData    type=STC
    &{pcta_info}    get table line    TrafficGenData    type=PCTA
    @{TAToolList}    create list    ${pcta_info}    ${stc_info}
    hlk traffic setup    @{TAToolList}    new_traff_cmd_log=True
    Comment    hlk traffic set ta type    STC
    ${dUStream1}    Create_Dictionary    stream_name=U1    ta_protocol=bcmp    vlan1_id=10    vlan2_id=18    ip_src=10.10.10.10
    ...    ip_dest=20.20.20.20    packet_count=1
    Comment    ${dUStream1}    Create_Dictionary    stream_name=U1    ta_protocol=IP    vlan1_id=10    vlan2_id=18
    ...    mac_src=00:00:00:00:66:00    frame_len=1050
    ${dUStream2}    Create_Dictionary    stream_name=U2    ta_protocol=IP    vlan1_id=11    vlan2_id=19    mac_src=00:00:00:00:77:00
    ...    frame_len=1050    bit_rate=100
    ${dUCapture1}    create dictionary    stream_name=U1    packet_count=>=1
    ${dUCapture2}    create dictionary    stream_name=U2    packet_count=>=5    mac_src=00:00:00:00:77:00    vlan1_id=11    vlan2_id=19
    ${dUMeasure1}    create dictionary    stream_name=U1    bit_rate=0    bit_rate_tol=5%    expect_result=pass
    ${dUMeasure2}    create dictionary    stream_name=U2    bit_rate=100    bit_rate_tol=5%
    ${dUPortMeasure}    create dictionary    bit_rate=2000    bit_rate_tol=5%    expect_result=pass
    @{lUStreamList}    create list    ${dUStream1}    ${dUStream2}
    @{lUMeasureList}    create list    ${dUMeasure1}    ${dUMeasure2}
    @{lUCaptureList}    create list    ${dUCapture1}    ${dUCapture2}
    ${dDStream1}    Create_Dictionary    stream_name=D1    ta_protocol=IP    vlan1_id=100    mac_src=00:00:00:00:88:00    frame_len=1050
    ...    bit_rate=1000
    ${dDStream2}    Create_Dictionary    stream_name=D2    ta_protocol=IP    vlan1_id=100    mac_src=00:00:00:00:99:00    frame_len=1050
    ...    bit_rate=1000
    ${dDCapture1}    create dictionary    stream_name=D1    packet_count=>=5    mac_src=00:00:00:00:88:00    vlan1_id=100
    ${dDCapture2}    create dictionary    stream_name=D2    packet_count=(1,)    mac_src=00:00:00:00:99:00    expect_result=pass
    ${dDMeasure1}    create dictionary    stream_name=D1    bit_rate=1000    bit_rate_tol=5%
    ${dDMeasure2}    create dictionary    stream_name=D2    bit_rate=1000    bit_rate_tol=5%
    @{lDStreamList}    create list    ${dDStream1}    ${dDStream2}
    @{lDMeasureList}    create list    ${dDMeasure1}    ${dDMeasure2}
    @{lDCaptureList}    create list    ${dDCapture1}    ${dDCapture2}
    Comment    run keyword and continue on failure    hlk_traffic    us_send_format=${dUStream1}    ds_send_format=${lDStreamList}    us_capt_opts=${dUCapture1}    ds_capt_opts=${lDCaptureList}
    ...    us_meas_opts=${dUMeasure1}    us_port_meas_opts=${dUPortMeasure}    ds_meas_opts=${lDMeasureList}    send_delay=3    capture_delay=2    measure_duration=5
    hlk traffic set ta type    PCTA
    run keyword and continue on failure    hlk_traffic    us_send_format=${dUStream1}    us_capt_opts=${dUCapture1}    send_delay=3    capture_delay=2    measure_duration=5
    Comment    hlk traffic set ta type    STC
    Comment    run keyword and continue on failure    hlk_traffic    us_send_format=${dUStream1}    us_capt_opts=${dUCapture1}    send_delay=3    capture_delay=2
    ...    measure_duration=5
    hlk_traffic_cleanup

hlk_traffic_dhcpv6
    log    %{SETUPFILENAME}
    create global tables    %{SETUPFILENAME}
    &{stc_info}    get table line    TrafficGenData    type=STC
    &{pcta_info}    get table line    TrafficGenData    type=PCTA
    @{TAToolList}    create list    ${pcta_info}    ${stc_info}
    hlk traffic setup    @{TAToolList}    new_traff_cmd_log=True
    Comment    hlk traffic set ta type    STC
    ${dUStream1}    Create_Dictionary    stream_name=U1    ta_protocol=dhcpv6    vlan1_id=10    vlan2_id=18    dhcpv6_ip_dest=8888:0000:0000:0000:0000:0000:0000:8888
    ...    dhcpv6_ip_src=1111:2222:3333:4444:5555:6666:7777:8888    packet_count=1    dhcpv6_udp_src_port=547    dhcp_message_type=1    dhcp_options=0001000e00010001539aba27001094000014000800020000000300280000000000049d4000076200000500180000000000000000000000000000000000093a8000278d00    mac_src=00:00:00:00:60:00
    ...    dhcpv6_udp_dest_port=546
    Comment    ${dUStream1}    Create_Dictionary    stream_name=U1    ta_protocol=IP    vlan1_id=10    vlan2_id=18
    ...    mac_src=00:00:00:00:66:00    frame_len=1050
    ${dUStream2}    Create_Dictionary    stream_name=U2    ta_protocol=IP    vlan1_id=11    vlan2_id=19    mac_src=00:00:00:00:77:00
    ...    frame_len=1050    bit_rate=100
    ${dUCapture1}    create dictionary    stream_name=U1    packet_count=>=1    eth_type=ETH    dhcp_message_type=1    ip_src=1111:2222:3333:4444:5555:6666:7777:8888
    ...    ip_dest=8888::8888    dhcpv6_udp_src_port=547    mac_src=00:00:00:00:60:00    dhcpv6_udp_dest_port=546
    ${dUCapture2}    create dictionary    stream_name=U2    packet_count=>=5    mac_src=00:00:00:00:77:00    vlan1_id=11    vlan2_id=19
    ${dUMeasure1}    create dictionary    stream_name=U1    bit_rate=0    bit_rate_tol=5%    expect_result=pass
    ${dUMeasure2}    create dictionary    stream_name=U2    bit_rate=100    bit_rate_tol=5%
    ${dUPortMeasure}    create dictionary    bit_rate=2000    bit_rate_tol=5%    expect_result=pass
    @{lUStreamList}    create list    ${dUStream1}    ${dUStream2}
    @{lUMeasureList}    create list    ${dUMeasure1}    ${dUMeasure2}
    @{lUCaptureList}    create list    ${dUCapture1}    ${dUCapture2}
    ${dDStream1}    Create_Dictionary    stream_name=D1    ta_protocol=IP    vlan1_id=100    mac_src=00:00:00:00:88:00    frame_len=1050
    ...    bit_rate=1000
    ${dDStream2}    Create_Dictionary    stream_name=D2    ta_protocol=IP    vlan1_id=100    mac_src=00:00:00:00:99:00    frame_len=1050
    ...    bit_rate=1000
    ${dDCapture1}    create dictionary    stream_name=D1    packet_count=>=5    mac_src=00:00:00:00:88:00    vlan1_id=100
    ${dDCapture2}    create dictionary    stream_name=D2    packet_count=(1,)    mac_src=00:00:00:00:99:00    expect_result=pass
    ${dDMeasure1}    create dictionary    stream_name=D1    bit_rate=1000    bit_rate_tol=5%
    ${dDMeasure2}    create dictionary    stream_name=D2    bit_rate=1000    bit_rate_tol=5%
    @{lDStreamList}    create list    ${dDStream1}    ${dDStream2}
    @{lDMeasureList}    create list    ${dDMeasure1}    ${dDMeasure2}
    @{lDCaptureList}    create list    ${dDCapture1}    ${dDCapture2}
    Comment    run keyword and continue on failure    hlk_traffic    us_send_format=${dUStream1}    ds_send_format=${lDStreamList}    us_capt_opts=${dUCapture1}    ds_capt_opts=${lDCaptureList}
    ...    us_meas_opts=${dUMeasure1}    us_port_meas_opts=${dUPortMeasure}    ds_meas_opts=${lDMeasureList}    send_delay=3    capture_delay=2    measure_duration=5
    hlk traffic set ta type    PCTA
    run keyword and continue on failure    hlk_traffic    us_send_format=${dUStream1}    us_capt_opts=${dUCapture1}    send_delay=3    capture_delay=2    measure_duration=5
    hlk traffic set ta type    STC
    run keyword and continue on failure    hlk_traffic    us_send_format=${dUStream1}    us_capt_opts=${dUCapture1}    send_delay=3    capture_delay=2    measure_duration=5
    stc_verify_measure
    hlk_traffic_cleanup

hlk_traffic_icmp
    log    %{SETUPFILENAME}
    create global tables    %{SETUPFILENAME}
    &{stc_info}    get table line    TrafficGenData    type=STC
    @{TAToolList}    create list    ${stc_info}
    hlk traffic setup    @{TAToolList}    new_traff_cmd_log=True
    Comment    hlk traffic set ta type    STC
    ${dUStream1}    Create_Dictionary    stream_name=U1    ta_protocol=icmp    ip_src=10.10.10.10    ip_dest=20.20.20.20    packet_count=10
    ...    icmp_code=01    icmp_type=10
    Comment    ${dUStream1}    Create_Dictionary    stream_name=U1    ta_protocol=IP    vlan1_id=10    vlan2_id=18
    ...    mac_src=00:00:00:00:66:00    frame_len=1050
    ${dUStream2}    Create_Dictionary    stream_name=U2    ta_protocol=IP    vlan1_id=11    vlan2_id=19    mac_src=00:00:00:00:77:00
    ...    frame_len=1050    bit_rate=100
    ${dUCapture1}    create dictionary    stream_name=U1    packet_count=>=1    icmp_code=01    icmp_type=10
    ${dUCapture2}    create dictionary    stream_name=U2    packet_count=>=5    mac_src=00:00:00:00:77:00    vlan1_id=11    vlan2_id=19
    ${dUMeasure1}    create dictionary    stream_name=U1    bit_rate=0    bit_rate_tol=5%    expect_result=pass
    ${dUMeasure2}    create dictionary    stream_name=U2    bit_rate=100    bit_rate_tol=5%
    ${dUPortMeasure}    create dictionary    bit_rate=2000    bit_rate_tol=5%    expect_result=pass
    @{lUStreamList}    create list    ${dUStream1}    ${dUStream2}
    @{lUMeasureList}    create list    ${dUMeasure1}    ${dUMeasure2}
    @{lUCaptureList}    create list    ${dUCapture1}    ${dUCapture2}
    ${dDStream1}    Create_Dictionary    stream_name=D1    ta_protocol=IP    vlan1_id=100    mac_src=00:00:00:00:88:00    frame_len=1050
    ...    bit_rate=1000
    ${dDStream2}    Create_Dictionary    stream_name=D2    ta_protocol=IP    vlan1_id=100    mac_src=00:00:00:00:99:00    frame_len=1050
    ...    bit_rate=1000
    ${dDCapture1}    create dictionary    stream_name=D1    packet_count=>=5    mac_src=00:00:00:00:88:00    vlan1_id=100
    ${dDCapture2}    create dictionary    stream_name=D2    packet_count=(1,)    mac_src=00:00:00:00:99:00    expect_result=pass
    ${dDMeasure1}    create dictionary    stream_name=D1    bit_rate=1000    bit_rate_tol=5%
    ${dDMeasure2}    create dictionary    stream_name=D2    bit_rate=1000    bit_rate_tol=5%
    @{lDStreamList}    create list    ${dDStream1}    ${dDStream2}
    @{lDMeasureList}    create list    ${dDMeasure1}    ${dDMeasure2}
    @{lDCaptureList}    create list    ${dDCapture1}    ${dDCapture2}
    Comment    run keyword and continue on failure    hlk_traffic    us_send_format=${dUStream1}    ds_send_format=${lDStreamList}    us_capt_opts=${dUCapture1}    ds_capt_opts=${lDCaptureList}
    ...    us_meas_opts=${dUMeasure1}    us_port_meas_opts=${dUPortMeasure}    ds_meas_opts=${lDMeasureList}    send_delay=3    capture_delay=2    measure_duration=5
    Comment    hlk traffic set ta type    PCTA
    Comment    run keyword and continue on failure    hlk_traffic    us_send_format=${dUStream1}    us_capt_opts=${dUCapture1}    send_delay=3    capture_delay=2
    ...    measure_duration=5
    hlk traffic set ta type    STC
    run keyword and continue on failure    hlk_traffic    us_send_format=${dUStream1}    us_capt_opts=${dUCapture1}    send_delay=3    capture_delay=2    measure_duration=5
    hlk_traffic_cleanup

hlk_traffic_rip
    log    %{SETUPFILENAME}
    create global tables    %{SETUPFILENAME}
    &{stc_info}    get table line    TrafficGenData    type=STC
    &{pcta_info}    get table line    TrafficGenData    type=PCTA
    @{TAToolList}    create list    ${pcta_info}    ${stc_info}
    hlk traffic setup    @{TAToolList}    new_traff_cmd_log=True
    Comment    hlk traffic set ta type    STC
    ${dUStream1}    Create_Dictionary    stream_name=U1    ta_protocol=rip    ip_src=10.10.10.10    ip_dest=20.20.20.20    packet_count=1
    ...    rip_command=REQ    rip_version=VER1
    Comment    ${dUStream1}    Create_Dictionary    stream_name=U1    ta_protocol=IP    vlan1_id=10    vlan2_id=18
    ...    mac_src=00:00:00:00:66:00    frame_len=1050
    ${dUStream2}    Create_Dictionary    stream_name=U2    ta_protocol=IP    vlan1_id=11    vlan2_id=19    mac_src=00:00:00:00:77:00
    ...    frame_len=1050    bit_rate=100
    ${dUCapture1}    create dictionary    stream_name=U1    packet_count=>=1
    ${dUCapture2}    create dictionary    stream_name=U2    packet_count=>=5    mac_src=00:00:00:00:77:00    vlan1_id=11    vlan2_id=19
    ${dUMeasure1}    create dictionary    stream_name=U1    bit_rate=0    bit_rate_tol=5%    expect_result=pass
    ${dUMeasure2}    create dictionary    stream_name=U2    bit_rate=100    bit_rate_tol=5%
    ${dUPortMeasure}    create dictionary    bit_rate=2000    bit_rate_tol=5%    expect_result=pass
    @{lUStreamList}    create list    ${dUStream1}    ${dUStream2}
    @{lUMeasureList}    create list    ${dUMeasure1}    ${dUMeasure2}
    @{lUCaptureList}    create list    ${dUCapture1}    ${dUCapture2}
    ${dDStream1}    Create_Dictionary    stream_name=D1    ta_protocol=IP    vlan1_id=100    mac_src=00:00:00:00:88:00    frame_len=1050
    ...    bit_rate=1000
    ${dDStream2}    Create_Dictionary    stream_name=D2    ta_protocol=IP    vlan1_id=100    mac_src=00:00:00:00:99:00    frame_len=1050
    ...    bit_rate=1000
    ${dDCapture1}    create dictionary    stream_name=D1    packet_count=>=5    mac_src=00:00:00:00:88:00    vlan1_id=100
    ${dDCapture2}    create dictionary    stream_name=D2    packet_count=(1,)    mac_src=00:00:00:00:99:00    expect_result=pass
    ${dDMeasure1}    create dictionary    stream_name=D1    bit_rate=1000    bit_rate_tol=5%
    ${dDMeasure2}    create dictionary    stream_name=D2    bit_rate=1000    bit_rate_tol=5%
    @{lDStreamList}    create list    ${dDStream1}    ${dDStream2}
    @{lDMeasureList}    create list    ${dDMeasure1}    ${dDMeasure2}
    @{lDCaptureList}    create list    ${dDCapture1}    ${dDCapture2}
    Comment    run keyword and continue on failure    hlk_traffic    us_send_format=${dUStream1}    ds_send_format=${lDStreamList}    us_capt_opts=${dUCapture1}    ds_capt_opts=${lDCaptureList}
    ...    us_meas_opts=${dUMeasure1}    us_port_meas_opts=${dUPortMeasure}    ds_meas_opts=${lDMeasureList}    send_delay=3    capture_delay=2    measure_duration=5
    hlk traffic set ta type    PCTA
    run keyword and continue on failure    hlk_traffic    us_send_format=${dUStream1}    us_capt_opts=${dUCapture1}    send_delay=3    capture_delay=2    measure_duration=5
    hlk traffic set ta type    STC
    run keyword and continue on failure    hlk_traffic    us_send_format=${dUStream1}    us_capt_opts=${dUCapture1}    send_delay=3    capture_delay=2    measure_duration=5
    hlk_traffic_cleanup

hlk_traffic_pppoeDiscovery
    log    %{SETUPFILENAME}
    create global tables    %{SETUPFILENAME}
    &{stc_info}    get table line    TrafficGenData    type=STC
    &{pcta_info}    get table line    TrafficGenData    type=PCTA
    @{TAToolList}    create list    ${pcta_info}    ${stc_info}
    hlk traffic setup    @{TAToolList}    new_traff_cmd_log=True
    Comment    hlk traffic set ta type    STC
    ${dUStream1}    Create_Dictionary    stream_name=U1    ta_protocol=pppoe    code=a7    ver=2    pppoetype=2
    ...    packet_count=1    ac_name_tag=true    ac_name_len=8    ac_name_val=66
    Comment    ${dUStream1}    Create_Dictionary    stream_name=U1    ta_protocol=IP    vlan1_id=10    vlan2_id=18
    ...    mac_src=00:00:00:00:66:00    frame_len=1050
    ${dUStream2}    Create_Dictionary    stream_name=U2    ta_protocol=IP    vlan1_id=11    vlan2_id=19    mac_src=00:00:00:00:77:00
    ...    frame_len=1050    bit_rate=100
    ${dUCapture1}    create dictionary    stream_name=U1    packet_count=>=1    code=a7
    ${dUCapture2}    create dictionary    stream_name=U2    packet_count=>=5    mac_src=00:00:00:00:77:00    vlan1_id=11    vlan2_id=19
    ${dUMeasure1}    create dictionary    stream_name=U1    bit_rate=0    bit_rate_tol=5%    expect_result=pass
    ${dUMeasure2}    create dictionary    stream_name=U2    bit_rate=100    bit_rate_tol=5%
    ${dUPortMeasure}    create dictionary    bit_rate=2000    bit_rate_tol=5%    expect_result=pass
    @{lUStreamList}    create list    ${dUStream1}    ${dUStream2}
    @{lUMeasureList}    create list    ${dUMeasure1}    ${dUMeasure2}
    @{lUCaptureList}    create list    ${dUCapture1}    ${dUCapture2}
    ${dDStream1}    Create_Dictionary    stream_name=D1    ta_protocol=IP    vlan1_id=100    mac_src=00:00:00:00:88:00    frame_len=1050
    ...    bit_rate=1000
    ${dDStream2}    Create_Dictionary    stream_name=D2    ta_protocol=IP    vlan1_id=100    mac_src=00:00:00:00:99:00    frame_len=1050
    ...    bit_rate=1000
    ${dDCapture1}    create dictionary    stream_name=D1    packet_count=>=5    mac_src=00:00:00:00:88:00    vlan1_id=100
    ${dDCapture2}    create dictionary    stream_name=D2    packet_count=(1,)    mac_src=00:00:00:00:99:00    expect_result=pass
    ${dDMeasure1}    create dictionary    stream_name=D1    bit_rate=1000    bit_rate_tol=5%
    ${dDMeasure2}    create dictionary    stream_name=D2    bit_rate=1000    bit_rate_tol=5%
    @{lDStreamList}    create list    ${dDStream1}    ${dDStream2}
    @{lDMeasureList}    create list    ${dDMeasure1}    ${dDMeasure2}
    @{lDCaptureList}    create list    ${dDCapture1}    ${dDCapture2}
    Comment    run keyword and continue on failure    hlk_traffic    us_send_format=${dUStream1}    ds_send_format=${lDStreamList}    us_capt_opts=${dUCapture1}    ds_capt_opts=${lDCaptureList}
    ...    us_meas_opts=${dUMeasure1}    us_port_meas_opts=${dUPortMeasure}    ds_meas_opts=${lDMeasureList}    send_delay=3    capture_delay=2    measure_duration=5
    hlk traffic set ta type    PCTA
    run keyword and continue on failure    hlk_traffic    us_send_format=${dUStream1}    us_capt_opts=${dUCapture1}    send_delay=3    capture_delay=2    measure_duration=5
    hlk traffic set ta type    STC
    run keyword and continue on failure    hlk_traffic    us_send_format=${dUStream1}    us_capt_opts=${dUCapture1}    send_delay=3    capture_delay=2    measure_duration=5
    hlk_traffic_cleanup

hlk_traffic_pppoePPP
    log    %{SETUPFILENAME}
    create global tables    %{SETUPFILENAME}
    &{stc_info}    get table line    TrafficGenData    type=STC
    &{pcta_info}    get table line    TrafficGenData    type=PCTA
    @{TAToolList}    create list    ${pcta_info}    ${stc_info}
    hlk traffic setup    @{TAToolList}    new_traff_cmd_log=True
    Comment    hlk traffic set ta type    STC
    ${dUStream1}    Create_Dictionary    stream_name=U1    ta_protocol=ppp    packet_count=1    session_id=6666    pppoa_encaps_type=LLC
    ...    information=INCREMENT
    Comment    ${dUStream1}    Create_Dictionary    stream_name=U1    ta_protocol=IP    vlan1_id=10    vlan2_id=18
    ...    mac_src=00:00:00:00:66:00    frame_len=1050
    ${dUStream2}    Create_Dictionary    stream_name=U2    ta_protocol=IP    vlan1_id=11    vlan2_id=19    mac_src=00:00:00:00:77:00
    ...    frame_len=1050    bit_rate=100
    ${dUCapture1}    create dictionary    stream_name=U1    packet_count=>=1    session_id=6666    pppoa_encaps_type=LLC
    ${dUCapture2}    create dictionary    stream_name=U2    packet_count=>=5    mac_src=00:00:00:00:77:00    vlan1_id=11    vlan2_id=19
    ${dUMeasure1}    create dictionary    stream_name=U1    bit_rate=0    bit_rate_tol=5%    expect_result=pass
    ${dUMeasure2}    create dictionary    stream_name=U2    bit_rate=100    bit_rate_tol=5%
    ${dUPortMeasure}    create dictionary    bit_rate=2000    bit_rate_tol=5%    expect_result=pass
    @{lUStreamList}    create list    ${dUStream1}    ${dUStream2}
    @{lUMeasureList}    create list    ${dUMeasure1}    ${dUMeasure2}
    @{lUCaptureList}    create list    ${dUCapture1}    ${dUCapture2}
    ${dDStream1}    Create_Dictionary    stream_name=D1    ta_protocol=IP    vlan1_id=100    mac_src=00:00:00:00:88:00    frame_len=1050
    ...    bit_rate=1000
    ${dDStream2}    Create_Dictionary    stream_name=D2    ta_protocol=IP    vlan1_id=100    mac_src=00:00:00:00:99:00    frame_len=1050
    ...    bit_rate=1000
    ${dDCapture1}    create dictionary    stream_name=D1    packet_count=>=5    mac_src=00:00:00:00:88:00    vlan1_id=100
    ${dDCapture2}    create dictionary    stream_name=D2    packet_count=(1,)    mac_src=00:00:00:00:99:00    expect_result=pass
    ${dDMeasure1}    create dictionary    stream_name=D1    bit_rate=1000    bit_rate_tol=5%
    ${dDMeasure2}    create dictionary    stream_name=D2    bit_rate=1000    bit_rate_tol=5%
    @{lDStreamList}    create list    ${dDStream1}    ${dDStream2}
    @{lDMeasureList}    create list    ${dDMeasure1}    ${dDMeasure2}
    @{lDCaptureList}    create list    ${dDCapture1}    ${dDCapture2}
    Comment    run keyword and continue on failure    hlk_traffic    us_send_format=${dUStream1}    ds_send_format=${lDStreamList}    us_capt_opts=${dUCapture1}    ds_capt_opts=${lDCaptureList}
    ...    us_meas_opts=${dUMeasure1}    us_port_meas_opts=${dUPortMeasure}    ds_meas_opts=${lDMeasureList}    send_delay=3    capture_delay=2    measure_duration=5
    hlk traffic set ta type    PCTA
    run keyword and continue on failure    hlk_traffic    us_send_format=${dUStream1}    us_capt_opts=${dUCapture1}    send_delay=3    capture_delay=2    measure_duration=5
    Comment    hlk traffic set ta type    STC
    Comment    run keyword and continue on failure    hlk_traffic    us_send_format=${dUStream1}    us_capt_opts=${dUCapture1}    send_delay=3    capture_delay=2
    ...    measure_duration=5
    hlk_traffic_cleanup

hlk_traffic_pppoe_lcp
    log    %{SETUPFILENAME}
    create global tables    %{SETUPFILENAME}
    &{stc_info}    get table line    TrafficGenData    type=STC
    &{pcta_info}    get table line    TrafficGenData    type=PCTA
    @{TAToolList}    create list    ${pcta_info}    ${stc_info}
    hlk traffic setup    @{TAToolList}    new_traff_cmd_log=True
    Comment    hlk traffic set ta type    STC
    ${dUStream1}    Create_Dictionary    stream_name=U1    ta_protocol=ppp_lcp    session_id=6666    accm_option=true    accm_val=66
    ...    ap_option=true    acfc_option=true    pfc_option=true    qp_option=true    seq_nbr=true    pppoa_encaps_type=LLC
    ...    packet_count=1
    Comment    ${dUStream1}    Create_Dictionary    stream_name=U1    ta_protocol=IP    vlan1_id=10    vlan2_id=18
    ...    mac_src=00:00:00:00:66:00
    ${dUStream2}    Create_Dictionary    stream_name=U2    ta_protocol=IP    vlan1_id=11    vlan2_id=19    mac_src=00:00:00:00:77:00
    ...    frame_len=1050    bit_rate=100
    ${dUCapture1}    create dictionary    stream_name=U1    packet_count=>=1    session_id=6666    pppoa_encaps_type=LLC
    ${dUCapture2}    create dictionary    stream_name=U2    packet_count=>=5    mac_src=00:00:00:00:77:00    vlan1_id=11    vlan2_id=19
    ${dUMeasure1}    create dictionary    stream_name=U1    bit_rate=0    bit_rate_tol=5%    expect_result=pass
    ${dUMeasure2}    create dictionary    stream_name=U2    bit_rate=100    bit_rate_tol=5%
    ${dUPortMeasure}    create dictionary    bit_rate=2000    bit_rate_tol=5%    expect_result=pass
    @{lUStreamList}    create list    ${dUStream1}    ${dUStream2}
    @{lUMeasureList}    create list    ${dUMeasure1}    ${dUMeasure2}
    @{lUCaptureList}    create list    ${dUCapture1}    ${dUCapture2}
    ${dDStream1}    Create_Dictionary    stream_name=D1    ta_protocol=IP    vlan1_id=100    mac_src=00:00:00:00:88:00    frame_len=1050
    ...    bit_rate=1000
    ${dDStream2}    Create_Dictionary    stream_name=D2    ta_protocol=IP    vlan1_id=100    mac_src=00:00:00:00:99:00    frame_len=1050
    ...    bit_rate=1000
    ${dDCapture1}    create dictionary    stream_name=D1    packet_count=>=5    mac_src=00:00:00:00:88:00    vlan1_id=100
    ${dDCapture2}    create dictionary    stream_name=D2    packet_count=(1,)    mac_src=00:00:00:00:99:00    expect_result=pass
    ${dDMeasure1}    create dictionary    stream_name=D1    bit_rate=1000    bit_rate_tol=5%
    ${dDMeasure2}    create dictionary    stream_name=D2    bit_rate=1000    bit_rate_tol=5%
    @{lDStreamList}    create list    ${dDStream1}    ${dDStream2}
    @{lDMeasureList}    create list    ${dDMeasure1}    ${dDMeasure2}
    @{lDCaptureList}    create list    ${dDCapture1}    ${dDCapture2}
    Comment    run keyword and continue on failure    hlk_traffic    us_send_format=${dUStream1}    ds_send_format=${lDStreamList}    us_capt_opts=${dUCapture1}    ds_capt_opts=${lDCaptureList}
    ...    us_meas_opts=${dUMeasure1}    us_port_meas_opts=${dUPortMeasure}    ds_meas_opts=${lDMeasureList}    send_delay=3    capture_delay=2    measure_duration=5
    hlk traffic set ta type    PCTA
    run keyword and continue on failure    hlk_traffic    us_send_format=${dUStream1}    us_capt_opts=${dUCapture1}    send_delay=3    capture_delay=2    measure_duration=5
    Comment    hlk traffic set ta type    STC
    Comment    run keyword and continue on failure    hlk_traffic    us_send_format=${dUStream1}    us_capt_opts=${dUCapture1}    send_delay=3    capture_delay=2
    ...    measure_duration=5
    hlk_traffic_cleanup

hlk_traffic_pppipcp
    log    %{SETUPFILENAME}
    create global tables    %{SETUPFILENAME}
    &{stc_info}    get table line    TrafficGenData    type=STC
    &{pcta_info}    get table line    TrafficGenData    type=PCTA
    @{TAToolList}    create list    ${pcta_info}    ${stc_info}
    hlk traffic setup    @{TAToolList}    new_traff_cmd_log=True
    Comment    hlk traffic set ta type    STC
    ${dUStream1}    Create_Dictionary    stream_name=U1    ta_protocol=ppp_ipcp    code=06    session_id=6666
    Comment    ${dUStream1}    Create_Dictionary    stream_name=U1    ta_protocol=IP    vlan1_id=10    vlan2_id=18
    ...    mac_src=00:00:00:00:66:00
    ${dUStream2}    Create_Dictionary    stream_name=U2    ta_protocol=IP    vlan1_id=11    vlan2_id=19    mac_src=00:00:00:00:77:00
    ...    frame_len=1050    bit_rate=100
    ${dUCapture1}    create dictionary    stream_name=U1    packet_count=>=1    session_id=6666
    ${dUCapture2}    create dictionary    stream_name=U2    packet_count=>=5    mac_src=00:00:00:00:77:00    vlan1_id=11    vlan2_id=19
    ${dUMeasure1}    create dictionary    stream_name=U1    bit_rate=0    bit_rate_tol=5%    expect_result=pass
    ${dUMeasure2}    create dictionary    stream_name=U2    bit_rate=100    bit_rate_tol=5%
    ${dUPortMeasure}    create dictionary    bit_rate=2000    bit_rate_tol=5%    expect_result=pass
    @{lUStreamList}    create list    ${dUStream1}    ${dUStream2}
    @{lUMeasureList}    create list    ${dUMeasure1}    ${dUMeasure2}
    @{lUCaptureList}    create list    ${dUCapture1}    ${dUCapture2}
    ${dDStream1}    Create_Dictionary    stream_name=D1    ta_protocol=IP    vlan1_id=100    mac_src=00:00:00:00:88:00    frame_len=1050
    ...    bit_rate=1000
    ${dDStream2}    Create_Dictionary    stream_name=D2    ta_protocol=IP    vlan1_id=100    mac_src=00:00:00:00:99:00    frame_len=1050
    ...    bit_rate=1000
    ${dDCapture1}    create dictionary    stream_name=D1    packet_count=>=5    mac_src=00:00:00:00:88:00    vlan1_id=100
    ${dDCapture2}    create dictionary    stream_name=D2    packet_count=(1,)    mac_src=00:00:00:00:99:00    expect_result=pass
    ${dDMeasure1}    create dictionary    stream_name=D1    bit_rate=1000    bit_rate_tol=5%
    ${dDMeasure2}    create dictionary    stream_name=D2    bit_rate=1000    bit_rate_tol=5%
    @{lDStreamList}    create list    ${dDStream1}    ${dDStream2}
    @{lDMeasureList}    create list    ${dDMeasure1}    ${dDMeasure2}
    @{lDCaptureList}    create list    ${dDCapture1}    ${dDCapture2}
    Comment    run keyword and continue on failure    hlk_traffic    us_send_format=${dUStream1}    ds_send_format=${lDStreamList}    us_capt_opts=${dUCapture1}    ds_capt_opts=${lDCaptureList}
    ...    us_meas_opts=${dUMeasure1}    us_port_meas_opts=${dUPortMeasure}    ds_meas_opts=${lDMeasureList}    send_delay=3    capture_delay=2    measure_duration=5
    hlk traffic set ta type    PCTA
    run keyword and continue on failure    hlk_traffic    us_send_format=${dUStream1}    us_capt_opts=${dUCapture1}    send_delay=3    capture_delay=2    measure_duration=5
    Comment    hlk traffic set ta type    STC
    Comment    run keyword and continue on failure    hlk_traffic    us_send_format=${dUStream1}    us_capt_opts=${dUCapture1}    send_delay=3    capture_delay=2
    ...    measure_duration=5
    hlk_traffic_cleanup

hlk_traffic_mpls_atm
    log    %{SETUPFILENAME}
    create global tables    %{SETUPFILENAME}
    &{stc_info}    get table line    TrafficGenData    type=STC
    &{pcta_info}    get table line    TrafficGenData    type=PCTA
    @{TAToolList}    create list    ${pcta_info}    ${stc_info}
    hlk traffic setup    @{TAToolList}    new_traff_cmd_log=True
    Comment    hlk traffic set ta type    STC
    ${dUStream1}    Create_Dictionary    stream_name=U1    ta_protocol=mpls_atm    packet_count=1    atm_clp=1    atm_pti=6
    ...    atm_vpi=888
    Comment    ${dUStream1}    Create_Dictionary    stream_name=U1    ta_protocol=IP    vlan1_id=10    vlan2_id=18
    ...    mac_src=00:00:00:00:66:00
    ${dUStream2}    Create_Dictionary    stream_name=U2    ta_protocol=IP    vlan1_id=11    vlan2_id=19    mac_src=00:00:00:00:77:00
    ...    frame_len=1050    bit_rate=100
    ${dUCapture1}    create dictionary    stream_name=U1    packet_count=>=1    atm_clp=1    atm_pti=6    atm_vpi=888
    ${dUCapture2}    create dictionary    stream_name=U2    packet_count=>=5    mac_src=00:00:00:00:77:00    vlan1_id=11    vlan2_id=19
    ${dUMeasure1}    create dictionary    stream_name=U1    bit_rate=0    bit_rate_tol=5%    expect_result=pass
    ${dUMeasure2}    create dictionary    stream_name=U2    bit_rate=100    bit_rate_tol=5%
    ${dUPortMeasure}    create dictionary    bit_rate=2000    bit_rate_tol=5%    expect_result=pass
    @{lUStreamList}    create list    ${dUStream1}    ${dUStream2}
    @{lUMeasureList}    create list    ${dUMeasure1}    ${dUMeasure2}
    @{lUCaptureList}    create list    ${dUCapture1}    ${dUCapture2}
    ${dDStream1}    Create_Dictionary    stream_name=D1    ta_protocol=IP    vlan1_id=100    mac_src=00:00:00:00:88:00    frame_len=1050
    ...    bit_rate=1000
    ${dDStream2}    Create_Dictionary    stream_name=D2    ta_protocol=IP    vlan1_id=100    mac_src=00:00:00:00:99:00    frame_len=1050
    ...    bit_rate=1000
    ${dDCapture1}    create dictionary    stream_name=D1    packet_count=>=5    mac_src=00:00:00:00:88:00    vlan1_id=100
    ${dDCapture2}    create dictionary    stream_name=D2    packet_count=(1,)    mac_src=00:00:00:00:99:00    expect_result=pass
    ${dDMeasure1}    create dictionary    stream_name=D1    bit_rate=1000    bit_rate_tol=5%
    ${dDMeasure2}    create dictionary    stream_name=D2    bit_rate=1000    bit_rate_tol=5%
    @{lDStreamList}    create list    ${dDStream1}    ${dDStream2}
    @{lDMeasureList}    create list    ${dDMeasure1}    ${dDMeasure2}
    @{lDCaptureList}    create list    ${dDCapture1}    ${dDCapture2}
    Comment    run keyword and continue on failure    hlk_traffic    us_send_format=${dUStream1}    ds_send_format=${lDStreamList}    us_capt_opts=${dUCapture1}    ds_capt_opts=${lDCaptureList}
    ...    us_meas_opts=${dUMeasure1}    us_port_meas_opts=${dUPortMeasure}    ds_meas_opts=${lDMeasureList}    send_delay=3    capture_delay=2    measure_duration=5
    hlk traffic set ta type    PCTA
    run keyword and continue on failure    hlk_traffic    us_send_format=${dUStream1}    us_capt_opts=${dUCapture1}    send_delay=3    capture_delay=2    measure_duration=5
    Comment    hlk traffic set ta type    STC
    Comment    run keyword and continue on failure    hlk_traffic    us_send_format=${dUStream1}    us_capt_opts=${dUCapture1}    send_delay=3    capture_delay=2
    ...    measure_duration=5
    hlk_traffic_cleanup

hlk_traffic_mpls_ip
    log    %{SETUPFILENAME}
    create global tables    %{SETUPFILENAME}
    &{stc_info}    get table line    TrafficGenData    type=STC
    &{pcta_info}    get table line    TrafficGenData    type=PCTA
    @{TAToolList}    create list    ${pcta_info}    ${stc_info}
    hlk traffic setup    @{TAToolList}    new_traff_cmd_log=True
    Comment    hlk traffic set ta type    STC
    ${dUStream1}    Create_Dictionary    stream_name=U1    ta_protocol=mpls_ip    packet_count=1    mpls_ttl=66    mpls_bos=1
    ...    mpls_exp=0    mpls_label=666
    Comment    ${dUStream1}    Create_Dictionary    stream_name=U1    ta_protocol=IP    vlan1_id=10    vlan2_id=18
    ...    mac_src=00:00:00:00:66:00
    ${dUStream2}    Create_Dictionary    stream_name=U2    ta_protocol=IP    vlan1_id=11    vlan2_id=19    mac_src=00:00:00:00:77:00
    ...    frame_len=1050    bit_rate=100
    ${dUCapture1}    create dictionary    stream_name=U1    packet_count=>=1    mpls_ttl=66    mpls_bos=1    mpls_exp=0
    ...    mpls_label=666
    ${dUCapture2}    create dictionary    stream_name=U2    packet_count=>=5    mac_src=00:00:00:00:77:00    vlan1_id=11    vlan2_id=19
    ${dUMeasure1}    create dictionary    stream_name=U1    bit_rate=0    bit_rate_tol=5%    expect_result=pass
    ${dUMeasure2}    create dictionary    stream_name=U2    bit_rate=100    bit_rate_tol=5%
    ${dUPortMeasure}    create dictionary    bit_rate=2000    bit_rate_tol=5%    expect_result=pass
    @{lUStreamList}    create list    ${dUStream1}    ${dUStream2}
    @{lUMeasureList}    create list    ${dUMeasure1}    ${dUMeasure2}
    @{lUCaptureList}    create list    ${dUCapture1}    ${dUCapture2}
    ${dDStream1}    Create_Dictionary    stream_name=D1    ta_protocol=IP    vlan1_id=100    mac_src=00:00:00:00:88:00    frame_len=1050
    ...    bit_rate=1000
    ${dDStream2}    Create_Dictionary    stream_name=D2    ta_protocol=IP    vlan1_id=100    mac_src=00:00:00:00:99:00    frame_len=1050
    ...    bit_rate=1000
    ${dDCapture1}    create dictionary    stream_name=D1    packet_count=>=5    mac_src=00:00:00:00:88:00    vlan1_id=100
    ${dDCapture2}    create dictionary    stream_name=D2    packet_count=(1,)    mac_src=00:00:00:00:99:00    expect_result=pass
    ${dDMeasure1}    create dictionary    stream_name=D1    bit_rate=1000    bit_rate_tol=5%
    ${dDMeasure2}    create dictionary    stream_name=D2    bit_rate=1000    bit_rate_tol=5%
    @{lDStreamList}    create list    ${dDStream1}    ${dDStream2}
    @{lDMeasureList}    create list    ${dDMeasure1}    ${dDMeasure2}
    @{lDCaptureList}    create list    ${dDCapture1}    ${dDCapture2}
    Comment    run keyword and continue on failure    hlk_traffic    us_send_format=${dUStream1}    ds_send_format=${lDStreamList}    us_capt_opts=${dUCapture1}    ds_capt_opts=${lDCaptureList}
    ...    us_meas_opts=${dUMeasure1}    us_port_meas_opts=${dUPortMeasure}    ds_meas_opts=${lDMeasureList}    send_delay=3    capture_delay=2    measure_duration=5
    hlk traffic set ta type    PCTA
    run keyword and continue on failure    hlk_traffic    us_send_format=${dUStream1}    us_capt_opts=${dUCapture1}    send_delay=3    capture_delay=2    measure_duration=5
    hlk traffic set ta type    STC
    run keyword and continue on failure    hlk_traffic    us_send_format=${dUStream1}    us_capt_opts=${dUCapture1}    send_delay=3    capture_delay=2    measure_duration=5
    hlk_traffic_cleanup

hlk_traffic_mpls_eth
    log    %{SETUPFILENAME}
    create global tables    %{SETUPFILENAME}
    &{stc_info}    get table line    TrafficGenData    type=STC
    &{pcta_info}    get table line    TrafficGenData    type=PCTA
    @{TAToolList}    create list    ${pcta_info}    ${stc_info}
    hlk traffic setup    @{TAToolList}    new_traff_cmd_log=True
    Comment    hlk traffic set ta type    STC
    ${dUStream1}    Create_Dictionary    stream_name=U1    ta_protocol=mpls_eth    packet_count=1    mpls_ttl=64    mpls_exp=0
    ...    mpls_label=0
    Comment    ${dUStream1}    Create_Dictionary    stream_name=U1    ta_protocol=IP    vlan1_id=10    vlan2_id=18
    ...    mac_src=00:00:00:00:66:00
    ${dUStream2}    Create_Dictionary    stream_name=U2    ta_protocol=IP    vlan1_id=11    vlan2_id=19    mac_src=00:00:00:00:77:00
    ...    frame_len=1050    bit_rate=100
    ${dUCapture1}    create dictionary    stream_name=U1    packet_count=>=1
    ${dUCapture2}    create dictionary    stream_name=U2    packet_count=>=5    mac_src=00:00:00:00:77:00    vlan1_id=11    vlan2_id=19
    ${dUMeasure1}    create dictionary    stream_name=U1    bit_rate=0    bit_rate_tol=5%    expect_result=pass
    ${dUMeasure2}    create dictionary    stream_name=U2    bit_rate=100    bit_rate_tol=5%
    ${dUPortMeasure}    create dictionary    bit_rate=2000    bit_rate_tol=5%    expect_result=pass
    @{lUStreamList}    create list    ${dUStream1}    ${dUStream2}
    @{lUMeasureList}    create list    ${dUMeasure1}    ${dUMeasure2}
    @{lUCaptureList}    create list    ${dUCapture1}    ${dUCapture2}
    ${dDStream1}    Create_Dictionary    stream_name=D1    ta_protocol=IP    vlan1_id=100    mac_src=00:00:00:00:88:00    frame_len=1050
    ...    bit_rate=1000
    ${dDStream2}    Create_Dictionary    stream_name=D2    ta_protocol=IP    vlan1_id=100    mac_src=00:00:00:00:99:00    frame_len=1050
    ...    bit_rate=1000
    ${dDCapture1}    create dictionary    stream_name=D1    packet_count=>=5    mac_src=00:00:00:00:88:00    vlan1_id=100
    ${dDCapture2}    create dictionary    stream_name=D2    packet_count=(1,)    mac_src=00:00:00:00:99:00    expect_result=pass
    ${dDMeasure1}    create dictionary    stream_name=D1    bit_rate=1000    bit_rate_tol=5%
    ${dDMeasure2}    create dictionary    stream_name=D2    bit_rate=1000    bit_rate_tol=5%
    @{lDStreamList}    create list    ${dDStream1}    ${dDStream2}
    @{lDMeasureList}    create list    ${dDMeasure1}    ${dDMeasure2}
    @{lDCaptureList}    create list    ${dDCapture1}    ${dDCapture2}
    Comment    run keyword and continue on failure    hlk_traffic    us_send_format=${dUStream1}    ds_send_format=${lDStreamList}    us_capt_opts=${dUCapture1}    ds_capt_opts=${lDCaptureList}
    ...    us_meas_opts=${dUMeasure1}    us_port_meas_opts=${dUPortMeasure}    ds_meas_opts=${lDMeasureList}    send_delay=3    capture_delay=2    measure_duration=5
    hlk traffic set ta type    PCTA
    run keyword and continue on failure    hlk_traffic    us_send_format=${dUStream1}    us_capt_opts=${dUCapture1}    send_delay=3    capture_delay=2    measure_duration=5
    hlk traffic set ta type    STC
    run keyword and continue on failure    hlk_traffic    us_send_format=${dUStream1}    us_capt_opts=${dUCapture1}    send_delay=3    capture_delay=2    measure_duration=5
    hlk_traffic_cleanup

hlk_traffic_mpls_eth_cw
    log    %{SETUPFILENAME}
    create global tables    %{SETUPFILENAME}
    &{stc_info}    get table line    TrafficGenData    type=STC
    &{pcta_info}    get table line    TrafficGenData    type=PCTA
    @{TAToolList}    create list    ${pcta_info}    ${stc_info}
    hlk traffic setup    @{TAToolList}    new_traff_cmd_log=True
    Comment    hlk traffic set ta type    STC
    ${dUStream1}    Create_Dictionary    stream_name=U1    ta_protocol=mpls_eth_cw    packet_count=1    mpls_ttl=66    mpls_bos=1
    ...    mpls_exp=6    mpls_label=666    pwmcw_frg=2
    Comment    ${dUStream1}    Create_Dictionary    stream_name=U1    ta_protocol=IP    vlan1_id=10    vlan2_id=18
    ...    mac_src=00:00:00:00:66:00
    ${dUStream2}    Create_Dictionary    stream_name=U2    ta_protocol=IP    vlan1_id=11    vlan2_id=19    mac_src=00:00:00:00:77:00
    ...    frame_len=1050    bit_rate=100
    ${dUCapture1}    create dictionary    stream_name=U1    packet_count=>=1    mpls_bos=1    mpls_exp=6    mpls_ttl=66
    ...    mpls_label=666    pwmcw_frg=2
    ${dUCapture2}    create dictionary    stream_name=U2    packet_count=>=5    mac_src=00:00:00:00:77:00    vlan1_id=11    vlan2_id=19
    ${dUMeasure1}    create dictionary    stream_name=U1    bit_rate=0    bit_rate_tol=5%    expect_result=pass
    ${dUMeasure2}    create dictionary    stream_name=U2    bit_rate=100    bit_rate_tol=5%
    ${dUPortMeasure}    create dictionary    bit_rate=2000    bit_rate_tol=5%    expect_result=pass
    @{lUStreamList}    create list    ${dUStream1}    ${dUStream2}
    @{lUMeasureList}    create list    ${dUMeasure1}    ${dUMeasure2}
    @{lUCaptureList}    create list    ${dUCapture1}    ${dUCapture2}
    ${dDStream1}    Create_Dictionary    stream_name=D1    ta_protocol=IP    vlan1_id=100    mac_src=00:00:00:00:88:00    frame_len=1050
    ...    bit_rate=1000
    ${dDStream2}    Create_Dictionary    stream_name=D2    ta_protocol=IP    vlan1_id=100    mac_src=00:00:00:00:99:00    frame_len=1050
    ...    bit_rate=1000
    ${dDCapture1}    create dictionary    stream_name=D1    packet_count=>=5    mac_src=00:00:00:00:88:00    vlan1_id=100
    ${dDCapture2}    create dictionary    stream_name=D2    packet_count=(1,)    mac_src=00:00:00:00:99:00    expect_result=pass
    ${dDMeasure1}    create dictionary    stream_name=D1    bit_rate=1000    bit_rate_tol=5%
    ${dDMeasure2}    create dictionary    stream_name=D2    bit_rate=1000    bit_rate_tol=5%
    @{lDStreamList}    create list    ${dDStream1}    ${dDStream2}
    @{lDMeasureList}    create list    ${dDMeasure1}    ${dDMeasure2}
    @{lDCaptureList}    create list    ${dDCapture1}    ${dDCapture2}
    Comment    run keyword and continue on failure    hlk_traffic    us_send_format=${dUStream1}    ds_send_format=${lDStreamList}    us_capt_opts=${dUCapture1}    ds_capt_opts=${lDCaptureList}
    ...    us_meas_opts=${dUMeasure1}    us_port_meas_opts=${dUPortMeasure}    ds_meas_opts=${lDMeasureList}    send_delay=3    capture_delay=2    measure_duration=5
    hlk traffic set ta type    PCTA
    run keyword and continue on failure    hlk_traffic    us_send_format=${dUStream1}    us_capt_opts=${dUCapture1}    send_delay=3    capture_delay=2    measure_duration=5
    Comment    hlk traffic set ta type    STC
    Comment    run keyword and continue on failure    hlk_traffic    us_send_format=${dUStream1}    us_capt_opts=${dUCapture1}    send_delay=3    capture_delay=2
    ...    measure_duration=5
    hlk_traffic_cleanup

hlk_traffic_mpls_ip_cw
    log    %{SETUPFILENAME}
    create global tables    %{SETUPFILENAME}
    &{stc_info}    get table line    TrafficGenData    type=STC
    &{pcta_info}    get table line    TrafficGenData    type=PCTA
    @{TAToolList}    create list    ${pcta_info}    ${stc_info}
    hlk traffic setup    @{TAToolList}    new_traff_cmd_log=True
    Comment    hlk traffic set ta type    STC
    ${dUStream1}    Create_Dictionary    stream_name=U1    ta_protocol=mpls_ip_cw    packet_count=1    mpls_ttl=66    mpls_bos=1
    ...    mpls_exp=6    mpls_label=666    pwmcw_frg=2
    Comment    ${dUStream1}    Create_Dictionary    stream_name=U1    ta_protocol=IP    vlan1_id=10    vlan2_id=18
    ...    mac_src=00:00:00:00:66:00
    ${dUStream2}    Create_Dictionary    stream_name=U2    ta_protocol=IP    vlan1_id=11    vlan2_id=19    mac_src=00:00:00:00:77:00
    ...    frame_len=1050    bit_rate=100
    ${dUCapture1}    create dictionary    stream_name=U1    packet_count=>=1    mpls_bos=1    mpls_exp=6    mpls_ttl=66
    ...    mpls_label=666    pwmcw_frg=2
    ${dUCapture2}    create dictionary    stream_name=U2    packet_count=>=5    mac_src=00:00:00:00:77:00    vlan1_id=11    vlan2_id=19
    ${dUMeasure1}    create dictionary    stream_name=U1    bit_rate=0    bit_rate_tol=5%    expect_result=pass
    ${dUMeasure2}    create dictionary    stream_name=U2    bit_rate=100    bit_rate_tol=5%
    ${dUPortMeasure}    create dictionary    bit_rate=2000    bit_rate_tol=5%    expect_result=pass
    @{lUStreamList}    create list    ${dUStream1}    ${dUStream2}
    @{lUMeasureList}    create list    ${dUMeasure1}    ${dUMeasure2}
    @{lUCaptureList}    create list    ${dUCapture1}    ${dUCapture2}
    ${dDStream1}    Create_Dictionary    stream_name=D1    ta_protocol=IP    vlan1_id=100    mac_src=00:00:00:00:88:00    frame_len=1050
    ...    bit_rate=1000
    ${dDStream2}    Create_Dictionary    stream_name=D2    ta_protocol=IP    vlan1_id=100    mac_src=00:00:00:00:99:00    frame_len=1050
    ...    bit_rate=1000
    ${dDCapture1}    create dictionary    stream_name=D1    packet_count=>=5    mac_src=00:00:00:00:88:00    vlan1_id=100
    ${dDCapture2}    create dictionary    stream_name=D2    packet_count=(1,)    mac_src=00:00:00:00:99:00    expect_result=pass
    ${dDMeasure1}    create dictionary    stream_name=D1    bit_rate=1000    bit_rate_tol=5%
    ${dDMeasure2}    create dictionary    stream_name=D2    bit_rate=1000    bit_rate_tol=5%
    @{lDStreamList}    create list    ${dDStream1}    ${dDStream2}
    @{lDMeasureList}    create list    ${dDMeasure1}    ${dDMeasure2}
    @{lDCaptureList}    create list    ${dDCapture1}    ${dDCapture2}
    Comment    run keyword and continue on failure    hlk_traffic    us_send_format=${dUStream1}    ds_send_format=${lDStreamList}    us_capt_opts=${dUCapture1}    ds_capt_opts=${lDCaptureList}
    ...    us_meas_opts=${dUMeasure1}    us_port_meas_opts=${dUPortMeasure}    ds_meas_opts=${lDMeasureList}    send_delay=3    capture_delay=2    measure_duration=5
    hlk traffic set ta type    PCTA
    run keyword and continue on failure    hlk_traffic    us_send_format=${dUStream1}    us_capt_opts=${dUCapture1}    send_delay=3    capture_delay=2    measure_duration=5
    Comment    hlk traffic set ta type    STC
    Comment    run keyword and continue on failure    hlk_traffic    us_send_format=${dUStream1}    us_capt_opts=${dUCapture1}    send_delay=3    capture_delay=2
    ...    measure_duration=5
    hlk_traffic_cleanup

hlk_traffic_8021x
    log    %{SETUPFILENAME}
    create global tables    %{SETUPFILENAME}
    &{stc_info}    get table line    TrafficGenData    type=STC
    &{pcta_info}    get table line    TrafficGenData    type=PCTA
    @{TAToolList}    create list    ${pcta_info}    ${stc_info}
    hlk traffic setup    @{TAToolList}    new_traff_cmd_log=True
    Comment    hlk traffic set ta type    STC
    ${dUStream1}    Create_Dictionary    stream_name=U1    ta_protocol=8021x    eap_id=11    file=22    data_source_type=DATA_STR
    ...    key_desc_type=RC4
    Comment    ${dUStream1}    Create_Dictionary    stream_name=U1    ta_protocol=IP    vlan1_id=10    vlan2_id=18
    ...    mac_src=00:00:00:00:66:00
    ${dUStream2}    Create_Dictionary    stream_name=U2    ta_protocol=IP    vlan1_id=11    vlan2_id=19    mac_src=00:00:00:00:77:00
    ...    frame_len=1050    bit_rate=100
    ${dUCapture1}    create dictionary    stream_name=U1    packet_count=>=1    eap_id=11
    ${dUCapture2}    create dictionary    stream_name=U2    packet_count=>=5    mac_src=00:00:00:00:77:00    vlan1_id=11    vlan2_id=19
    ${dUMeasure1}    create dictionary    stream_name=U1    bit_rate=0    bit_rate_tol=5%    expect_result=pass
    ${dUMeasure2}    create dictionary    stream_name=U2    bit_rate=100    bit_rate_tol=5%
    ${dUPortMeasure}    create dictionary    bit_rate=2000    bit_rate_tol=5%    expect_result=pass
    @{lUStreamList}    create list    ${dUStream1}    ${dUStream2}
    @{lUMeasureList}    create list    ${dUMeasure1}    ${dUMeasure2}
    @{lUCaptureList}    create list    ${dUCapture1}    ${dUCapture2}
    ${dDStream1}    Create_Dictionary    stream_name=D1    ta_protocol=IP    vlan1_id=100    mac_src=00:00:00:00:88:00    frame_len=1050
    ...    bit_rate=1000
    ${dDStream2}    Create_Dictionary    stream_name=D2    ta_protocol=IP    vlan1_id=100    mac_src=00:00:00:00:99:00    frame_len=1050
    ...    bit_rate=1000
    ${dDCapture1}    create dictionary    stream_name=D1    packet_count=>=5    mac_src=00:00:00:00:88:00    vlan1_id=100
    ${dDCapture2}    create dictionary    stream_name=D2    packet_count=(1,)    mac_src=00:00:00:00:99:00    expect_result=pass
    ${dDMeasure1}    create dictionary    stream_name=D1    bit_rate=1000    bit_rate_tol=5%
    ${dDMeasure2}    create dictionary    stream_name=D2    bit_rate=1000    bit_rate_tol=5%
    @{lDStreamList}    create list    ${dDStream1}    ${dDStream2}
    @{lDMeasureList}    create list    ${dDMeasure1}    ${dDMeasure2}
    @{lDCaptureList}    create list    ${dDCapture1}    ${dDCapture2}
    Comment    run keyword and continue on failure    hlk_traffic    us_send_format=${dUStream1}    ds_send_format=${lDStreamList}    us_capt_opts=${dUCapture1}    ds_capt_opts=${lDCaptureList}
    ...    us_meas_opts=${dUMeasure1}    us_port_meas_opts=${dUPortMeasure}    ds_meas_opts=${lDMeasureList}    send_delay=3    capture_delay=2    measure_duration=5
    hlk traffic set ta type    PCTA
    run keyword and continue on failure    hlk_traffic    us_send_format=${dUStream1}    us_capt_opts=${dUCapture1}    send_delay=3    capture_delay=2    measure_duration=5
    Comment    hlk traffic set ta type    STC
    Comment    run keyword and continue on failure    hlk_traffic    us_send_format=${dUStream1}    us_capt_opts=${dUCapture1}    send_delay=3    capture_delay=2
    ...    measure_duration=5
    hlk_traffic_cleanup

*** Keywords ***
