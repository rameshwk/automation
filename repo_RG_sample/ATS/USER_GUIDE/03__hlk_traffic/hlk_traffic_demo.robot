*** Settings ***
Library           Collections
Resource          %{ROBOTREPO}/HLK/res/l2-common.txt

*** Test Cases ***
llk_traffic_sample_stc
    log    %{SETUPFILENAME}
    create global tables    %{SETUPFILENAME}
    &{stc_info}    get table line    TrafficGenData    type=STC
    @{TAToolList}    create list    ${stc_info}
    log    ${TAToolList}
    hlk traffic setup    @{TAToolList}
    Comment    ${dUStream1}    Create_Dictionary    stream_name=U1    ta_protocol=IP    vlan.vlan1=11    vlan.pri1=1
    ...    vlan.vlan2=12    vlan.pri2=2    ethii.srcMac=11:11:11:11:11:11    ethii.dstMac=22:22:22:22:22:22    ipv4.destAddr=135.251.1.1    ipv4.sourceAddr=135.251.2.2
    ...    load.load=1000    load.loadunit=BITS_PER_SECOND
    ${dUStream1}    Create_Dictionary    stream_name=U1    ta_protocol=IP    vlan.vlan1=11    vlan.pri1=1    vlan.vlan2=12
    ...    vlan.pri2=2    srcMac=11:11:11:11:11:11    dstMac=22:22:22:22:22:22    ipv4.destAddr=135.251.1.1    ipv4.sourceAddr=135.251.2.2    load.load=1000
    ...    load.loadunit=BITS_PER_SECOND
    ${dUMeasure1}    create dictionary    stream_name=U1    l1bitrate=1100    l1bitrate_tol=20%    bitrate=1000    bitrate_tol=20%
    ${dUCapture1}    create dictionary    stream_name=U1    packet_count=>=5    vlan1.id=11
    Comment    ${dUStream2}    Create_Dictionary    stream_name=U2    ta_protocol=IP    vlan.vlan1=22    vlan.pri1=1
    ...    vlan.vlan2=12    vlan.pri2=2    ethii.srcMac=22:22:22:22:22:22    ethii.dstMac=33:33:33:33:33:33    ipv4.destAddr=192.168.1.1    ipv4.sourceAddr=192.168.2.2
    ...    load.load=1000    load.loadunit=BITS_PER_SECOND
    ${dUStream2}    Create_Dictionary    stream_name=U2    ta_protocol=IP    vlan.vlan1=22    vlan.pri1=1    vlan.vlan2=12
    ...    vlan.pri2=2    srcMac=22:22:22:22:22:22    dstMac=33:33:33:33:33:33    ipv4.destAddr=192.168.1.1    ipv4.sourceAddr=192.168.2.2    load.load=1000
    ...    load.loadunit=BITS_PER_SECOND
    ${dUMeasure2}    create dictionary    stream_name=U2    l1bitrate=1100    l1bitrate_tol=5%    bitrate=1000    bitrate_tol=5%
    ${dUCapture2}    create dictionary    stream_name=U2    packet_count=>=5    vlan1.id=22
    @{lUStreamList}    create list    ${dUStream1}    ${dUStream2}
    @{lUMeasureList}    create list    ${dUMeasure1}    ${dUMeasure2}
    @{lUCaptureList}    create list    ${dUCapture1}    ${dUCapture2}
    Comment    ${dDStream1}    Create_Dictionary    stream_name=D1    ta_protocol=IP    vlan.vlan1=33    vlan.pri1=1
    ...    vlan.vlan2=12    vlan.pri2=2    ethii.srcMac=11:11:11:11:11:11    ethii.dstMac=22:22:22:22:22:22    ipv4.destAddr=135.251.1.1    ipv4.sourceAddr=135.251.2.2
    ...    load.load=1000    load.loadunit=BITS_PER_SECOND
    ${dDStream1}    Create_Dictionary    stream_name=D1    ta_protocol=IP    vlan.vlan1=33    vlan.pri1=1    vlan.vlan2=12
    ...    vlan.pri2=2    srcMac=11:11:11:11:11:11    dstMac=22:22:22:22:22:22    ipv4.destAddr=135.251.1.1    ipv4.sourceAddr=135.251.2.2    load.load=1000
    ...    load.loadunit=BITS_PER_SECOND
    ${dDMeasure1}    create dictionary    stream_name=D1    l1bitrate=1100    l1bitrate_tol=5%    bitrate=1000    bitrate_tol=5%
    ${dDCapture1}    create dictionary    stream_name=D1    packet_count=>=5    vlan1.id=33
    Comment    ${dDStream2}    Create_Dictionary    stream_name=D2    ta_protocol=IP    vlan.vlan1=44    vlan.pri1=1
    ...    vlan.vlan2=12    vlan.pri2=2    ethii.srcMac=22:22:22:22:22:22    ethii.dstMac=33:33:33:33:33:33    ipv4.destAddr=192.168.1.1    ipv4.sourceAddr=192.168.2.2
    ...    load.load=1000    load.loadunit=BITS_PER_SECOND
    ${dDStream2}    Create_Dictionary    stream_name=D2    ta_protocol=IP    vlan.vlan1=44    vlan.pri1=1    vlan.vlan2=12
    ...    vlan.pri2=2    srcMac=22:22:22:22:22:22    dstMac=33:33:33:33:33:33    ipv4.destAddr=192.168.1.1    ipv4.sourceAddr=192.168.2.2    load.load=1000
    ...    load.loadunit=BITS_PER_SECOND
    ${dDMeasure2}    create dictionary    stream_name=D2    l1bitrate=1100    l1bitrate_tol=5%    bitrate=1000    bitrate_tol=5%
    ${dDCapture2}    create dictionary    stream_name=D2    packet_count=>=5    vlan1.id=44
    @{lDStreamList}    create list    ${dDStream1}    ${dDStream2}
    @{lDMeasureList}    create list    ${dDMeasure1}    ${dDMeasure2}
    @{lDCaptureList}    create list    ${dDCapture1}    ${dDCapture2}
    hlk_traffic    us_send_format=${dUStream1}    ds_send_format=${lDStreamList}    us_capt_opts=${dUCapture1}    ds_capt_opts=${lDCaptureList}    us_meas_opts=${dUMeasure1}    ds_meas_opts=${lDMeasureList}
    ...    send_delay=3    capture_delay=2    measure_duration=5
    Comment    hlk_traffic    us_send_format={stream_name=U1,ta_protocol=IP,vlan1.id=5}    ds_send_format=${lDStreamList}    us_capt_opts=${dUCapture1}    ds_capt_opts=[]    us_meas_opts=${dUMeasure1}
    ...    ds_meas_opts=[]    send_delay=10    capture_delay=30
    hlk_traffic_cleanup

llk_traffic_sample_pcta_measure
    "    %{SETUPFILENAME}
    create global tables    %{SETUPFILENAME}
    &{stc_info}    get table line    TrafficGenData    type=STC
    &{pcta_info}    get table line    TrafficGenData    type=PCTA
    Comment    @{TAToolList}    create list    ${pcta_info}    ${stc_info}
    @{TAToolList}    create list    ${pcta_info}
    log    ${TAToolList}
    hlk traffic setup    @{TAToolList}
    Comment    hlk traffic set ta type    PCTA
    ${dUStream1}    Create_Dictionary    stream_name=U1    ta_protocol=IP    vlan1_id=10    vlan2_id=18    mac_src=000000006600
    ...    frame_len=1050    eth_type=ETH
    ${dUMeasure1}    create dictionary    stream_name=U1    vlan1_id=10
    ${dUCapture1}    create dictionary    stream_name=U1    packet_count=>=5    mac_src=000000006600
    ${dUStream2}    Create_Dictionary    stream_name=U2    ta_protocol=IP    vlan1_id=10    vlan2_id=18    mac_src=000000007700
    ...    frame_len=1050    eth_type=ETH
    ${dUMeasure2}    create dictionary    stream_name=U2    bit_rate_good=9    bit_rate_good_tol=5%
    ${dUCapture2}    create dictionary    stream_name=U2    packet_count=>=5    mac_src=000000007700
    @{lUStreamList}    create list    ${dUStream1}    ${dUStream2}
    @{lUMeasureList}    create list    ${dUMeasure1}    ${dUMeasure2}
    @{lUCaptureList}    create list    ${dUCapture1}    ${dUCapture2}
    ${dDStream1}    Create_Dictionary    stream_name=U1    ta_protocol=IP    vlan1_id=100    mac_src=000000008800    frame_len=1050
    ...    eth_type=ETH
    ${dDStream2}    Create_Dictionary    stream_name=U1    ta_protocol=IP    vlan1_id=100    mac_src=000000009900    frame_len=1050
    ...    eth_type=ETH
    ${dDMeasure1}    create dictionary    stream_name=D1    bit_rate_good=9    bit_rate_good_tol=5%
    ${dDCapture1}    create dictionary    stream_name=D1    packet_count=>=5    mac_src=000000008800
    ${dDMeasure2}    create dictionary    stream_name=D2    bit_rate_good=9    bit_rate_good_tol=5%
    ${dDCapture2}    create dictionary    stream_name=D2    packet_count=>=5    mac_src=000000009900
    @{lDStreamList}    create list    ${dDStream1}    ${dDStream2}
    @{lDMeasureList}    create list    ${dDMeasure1}    ${dDMeasure2}
    @{lDCaptureList}    create list    ${dDCapture1}    ${dDCapture2}
    hlk_traffic    us_send_format=${dUStream1}    ds_send_format=${lDStreamList}    us_capt_opts=${dUCapture1}    ds_capt_opts=${lDCaptureList}    us_meas_opts=${dUMeasure1}    ds_meas_opts=${lDMeasureList}
    ...    send_delay=3    capture_delay=2    measure_duration=5
    Comment    hlk_traffic    us_send_format={stream_name=U1,ta_protocol=IP,vlan1.id=5}    ds_send_format=${lDStreamList}    us_capt_opts=${dUCapture1}    ds_capt_opts=[]    us_meas_opts=${dUMeasure1}
    ...    ds_meas_opts=[]    send_delay=10    capture_delay=30
    hlk_traffic_cleanup

llk_traffic_sample_pcta_capture
    log    %{SETUPFILENAME}
    create global tables    %{SETUPFILENAME}
    &{stc_info}    get table line    TrafficGenData    type=STC
    &{pcta_info}    get table line    TrafficGenData    type=PCTA
    Comment    @{TAToolList}    create list    ${pcta_info}    ${stc_info}
    @{TAToolList}    create list    ${pcta_info}
    log    ${TAToolList}
    hlk traffic setup    @{TAToolList}
    Comment    hlk traffic set ta type    PCTA
    ${dUStream1}    Create_Dictionary    stream_name=U1    ta_protocol=IP    vlan1_id=10    vlan2_id=18    mac_src=000000006600
    ...    frame_len=1050    eth_type=ETH
    ${dUMeasure1}    create dictionary    stream_name=U1    vlan1_id=10
    ${dUCapture1}    create dictionary    stream_name=U1    packet_count=>=5    mac_src=000000006600
    ${dUStream2}    Create_Dictionary    stream_name=U2    ta_protocol=IP    vlan1_id=10    vlan2_id=18    mac_src=000000007700
    ...    frame_len=1050    eth_type=ETH
    ${dUMeasure2}    create dictionary    stream_name=U2    mac_src=000000007700
    ${dUCapture2}    create dictionary    stream_name=U2    packet_count=>=5    mac_src=000000007700
    @{lUStreamList}    create list    ${dUStream1}    ${dUStream2}
    @{lUMeasureList}    create list    ${dUMeasure1}    ${dUMeasure2}
    @{lUCaptureList}    create list    ${dUCapture1}    ${dUCapture2}
    ${dDStream1}    Create_Dictionary    stream_name=U1    ta_protocol=IP    vlan1_id=100    mac_src=000000008800    frame_len=1050
    ...    eth_type=ETH
    ${dDStream2}    Create_Dictionary    stream_name=U1    ta_protocol=IP    vlan1_id=100    mac_src=000000009900    frame_len=1050
    ...    eth_type=ETH
    ${dDMeasure1}    create dictionary    stream_name=D1    bit_rate_good=9    bit_rate_good_tol=5%
    ${dDCapture1}    create dictionary    stream_name=D1    packet_count=>=5    mac_src=000000008800
    ${dDMeasure2}    create dictionary    stream_name=D2    bit_rate_good=9    bit_rate_good_tol=5%
    ${dDCapture2}    create dictionary    stream_name=D2    packet_count=>=5    mac_src=000000009900
    @{lDStreamList}    create list    ${dDStream1}    ${dDStream2}
    @{lDMeasureList}    create list    ${dDMeasure1}    ${dDMeasure2}
    @{lDCaptureList}    create list    ${dDCapture1}    ${dDCapture2}
    hlk_traffic    us_send_format=${dUStream1}    ds_send_format=${lDStreamList}    us_capt_opts=${dUCapture1}    ds_capt_opts=${lDCaptureList}    ds_meas_opts=${lDMeasureList}    send_delay=3
    ...    capture_delay=2    measure_duration=5
    Comment    hlk_traffic    us_send_format={stream_name=U1,ta_protocol=IP,vlan1.id=5}    ds_send_format=${lDStreamList}    us_capt_opts=${dUCapture1}    ds_capt_opts=[]    us_meas_opts=${dUMeasure1}
    ...    ds_meas_opts=[]    send_delay=10    capture_delay=30
    hlk_traffic_cleanup

hlk_traffic_sample
    log    %{SETUPFILENAME}
    create global tables    %{SETUPFILENAME}
    &{stc_info}    get table line    TrafficGenData    type=STC
    &{pcta_info}    get table line    TrafficGenData    type=PCTA
    @{TAToolList}    create list    ${pcta_info}    ${stc_info}
    hlk traffic setup    @{TAToolList}    new_traff_cmd_log=True
    hlk traffic set ta type    STC
    ${dUStream1}    Create_Dictionary    stream_name=U1    ta_protocol=IP    vlan1_user_prior=1    vlan1_cfi=1    vlan1_id=10
    ...    vlan2_id=18    mac_src=00:00:00:00:66:00    frame_len=1050    burst_size=3    burst_rate=2
    Comment    ${dUStream1}    Create_Dictionary    stream_name=U1    ta_protocol=IP    vlan1_id=10    vlan2_id=18
    ...    mac_src=00:00:00:00:66:00    frame_len=1050    packet_count=3
    ${dUStream2}    Create_Dictionary    stream_name=U2    ta_protocol=IP    vlan1_id=11    vlan2_id=19    mac_src=00:00:00:00:77:00
    ...    frame_len=1050    bit_rate=100
    ${dUCapture1}    create dictionary    stream_name=U1    packet_count=>=5    mac_src=00:00:00:00:66:00    vlan1_id=10    vlan2_id=18
    ${dUCapture2}    create dictionary    stream_name=U2    packet_count=>=5    mac_src=00:00:00:00:77:00    vlan1_id=11    vlan2_id=19
    ${dUMeasure1}    create dictionary    stream_name=U1    bit_rate=1000    bit_rate_tol=5%    expect_result=fail
    ${dUMeasure2}    create dictionary    stream_name=U2    bit_rate=100    bit_rate_tol=5%
    ${dUPortMeasure}    create dictionary    bit_rate=2000    bit_rate_tol=5%    expect_result=fail
    @{lUStreamList}    create list    ${dUStream1}    ${dUStream2}
    @{lUMeasureList}    create list    ${dUMeasure1}    ${dUMeasure2}
    @{lUCaptureList}    create list    ${dUCapture1}    ${dUCapture2}
    ${dDStream1}    Create_Dictionary    stream_name=D1    ta_protocol=IP    vlan1_id=100    mac_src=00:00:00:00:88:00    frame_len=1050
    ...    bit_rate=1000
    ${dDStream2}    Create_Dictionary    stream_name=D2    ta_protocol=IP    vlan1_id=100    mac_src=00:00:00:00:99:00    frame_len=1050
    ...    bit_rate=1000
    ${dDCapture1}    create dictionary    stream_name=D1    packet_count=>=5    mac_src=00:00:00:00:88:00    vlan1_id=100
    ${dDCapture2}    create dictionary    stream_name=D2    packet_count=(1,)    mac_src=00:00:00:00:99:00    expect_result=fail
    ${dDMeasure1}    create dictionary    stream_name=D1    bit_rate=1000    bit_rate_tol=5%
    ${dDMeasure2}    create dictionary    stream_name=D2    bit_rate=1000    bit_rate_tol=5%
    @{lDStreamList}    create list    ${dDStream1}    ${dDStream2}
    @{lDMeasureList}    create list    ${dDMeasure1}    ${dDMeasure2}
    @{lDCaptureList}    create list    ${dDCapture1}    ${dDCapture2}
    run keyword and continue on failure    hlk_traffic    us_send_format=${dUStream1}    ds_send_format=${lDStreamList}    us_capt_opts=${dUCapture1}    ds_capt_opts=${lDCaptureList}    us_meas_opts=${dUMeasure1}
    ...    us_port_meas_opts=${dUPortMeasure}    ds_meas_opts=${lDMeasureList}    send_delay=3    capture_delay=2    measure_duration=5
    hlk traffic set ta type    PCTA
    run keyword and continue on failure    hlk_traffic    us_send_format=${dUStream1}    ds_send_format=${lDStreamList}    us_capt_opts=${dUCapture1}    ds_capt_opts=${lDCaptureList}    us_meas_opts=${dUMeasure1}
    ...    ds_meas_opts=${lDMeasureList}    send_delay=3    capture_delay=2    measure_duration=5
    hlk_traffic_cleanup
    Comment    hlk traffic set ta type    PCTA
    Comment    hlk traffic set ta type    STC

test1
    &{sb_1}    create dictionary    vlan1=1    pri1=1
    &{sb_2}    create dictionary    vlan1=2    pri1=1    vlan2=2    pri2=2
    @{vlan_list}    create list    ${sb_1}    ${sb_2}
    ${len}    get length    ${vlan_list}
    create multi streamblock    US    000000009900    @{vlan_list}

LLDP

hlk_traffic_packet_count

*** Keywords ***
test2
    run keyword and continue on failure    fail
    run keyword and continue on failure    fail
    log    test

create multi streamblock
    [Arguments]    ${SB_name_prefix}    ${mac_src_prefix}    @{vlan_list}
    : FOR    ${each_vlan}    IN    @{vlan_list}
    \    log    ${each_vlan}
    \    &{vlan_dict}    convert to dictionary    ${each_vlan}
    \    set to dictionary    ${vlan_dict}    test=haha
    \    log    ${vlan_dict}
