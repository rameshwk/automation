# define upstream 1
dUsSendFormat_1 = {\
'stream_name': 'U1', \
'ta_protocol': 'IP',\
'vlan1_id': '10',\
'vlan1_user_prior': '1',\
'vlan2_id': '18',\
'frame_len': '1050',\
'mac_src': '00:00:00:00:66:00'}

dUsCaptOpts_1 = {\
'stream_name': 'U1', \
'vlan1_id': '10',\
'vlan1_user_prior': '1',\
'vlan2_id': '18',\
'mac_src': '00:00:00:00:66:00'}

dUsMeasOpts_1 = {\
'stream_name':'U1',\
'bit_rate':'0',\
'bit_rate_tol':'5%'}

# define upstream_2
dUsSendFormat_2 = {'stream_name': 'U2', 'ta_protocol': 'IP','vlan1_id': '11', 'vlan1_user_prior': '1','vlan2_id': '18','frame_len': '1050','mac_src': '00:00:00:00:77:00'}

dUsCaptOpts_2 = {'stream_name': 'U2', 'vlan1_id': '11','vlan1_user_prior': '1','vlan2_id': '18', 'mac_src': '00:00:00:00:77:00'}

dUsMeasOpts_2 = {'stream_name':'U2','bit_rate':'1000','bit_rate_tol':'5%'}

# define downstream_1
dDsSendFormat_1 = {'stream_name': 'D1', 'ta_protocol': 'IP','vlan1_id': '100', 'vlan1_user_prior': '1','vlan2_id': '18','frame_len': '1050','mac_src': '00:00:00:00:88:00'}

dDsCaptOpts_1 = {'stream_name': 'D1', 'vlan1_id': '100','vlan1_user_prior': '1','vlan2_id': '18','mac_src': '00:00:00:00:88:00'}

dDsMeasOpts_1 = {'stream_name':'D1','bit_rate':'0','bit_rate_tol':'5%'}

# define downstream_2
dDsSendFormat_2 = {'stream_name': 'D2', 'ta_protocol': 'IP','vlan1_id': '101','vlan1_user_prior': '1','vlan2_id': '18','frame_len': '1050','mac_src': '00:00:00:00:99:00'}

dDsCaptOpts_2 = {'stream_name': 'D2', 'vlan1_id': '101','vlan1_user_prior': '1',
'vlan2_id': '18','mac_src': '00:00:00:00:99:00'}

dDsMeasOpts_2 = {'stream_name':'D2','bit_rate':'100','bit_rate_tol':'5%'}
