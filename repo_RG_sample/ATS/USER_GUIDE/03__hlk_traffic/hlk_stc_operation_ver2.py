#!/bin/sh/env python
import os,sys,time,re,hashlib
from robot.libraries.BuiltIn import BuiltIn
from robot.api import logger
import logging
lib_path = os.environ['ROBOTREPO'] +'/LIBS/DATA_TRANSLATION'
if lib_path not in sys.path:
    sys.path.append(lib_path)

buildin_path = os.environ['ROBOTREPO'] +'/PACKAGES/lib/python2.7/site-packages/robot/libraries' 
if buildin_path not in sys.path:
    sys.path.append(buildin_path)
print buildin_path

from data_translation import create_global_tables
from data_translation import get_table_line

try:
    for lib_path in [os.environ['ROBOTREPO'] +'/LIBS/COM_PCTA',\
    os.environ['ROBOTREPO'] +'/LIBS/COM_STC',\
    os.environ['ROBOTREPO'] +'/HLKS'] :
        if lib_path not in sys.path:
            sys.path.append(lib_path)
except Exception as inst:
    raise AssertionError("%s -> Fail to set sys.path, %s" % (__name__,inst))

import llk_com_stc
import hlk_stc
import hlk_com_traffic
import hlk_traffic

TA_TYPE = "STC"
def hlk_stc_operation(**args) :
    global TA_TYPE
    
    # 1. get STC info
    print "STC info obtain."
    TrafficGenData = create_global_tables("/repo/root/robot/SETUPS/HLK_TRAFFIC.csv")
    stc_info=get_table_line('TrafficGenData','type=STC')
    pcta_info = get_table_line('TrafficGenData','type=PCTA')   
    output_dir = {'outputDir':'/tmp/outDir'}
 
    # 2. STC connect
    print "STC connection start."
    stcList=BuiltIn().create_list(stc_info,pcta_info,output_dir)
    hlk_com_traffic.hlk_traffic_setup(*stcList)
    
    # 3. send DHCP packet
    hlk_com_traffic.hlk_traffic_set_ta_type('STC')
    dUStream1={'ta_protocol':'dhcp','stream_name':'U1','packet_count':'2'}
    dUCapture1={}
    dUCapture1['stream_name']='U1'
    dUCapture1['packet_count']='>=1'
    hlk_com_traffic.hlk_traffic(us_send_format=dUStream1,us_capt_opts=dUCapture1,send_delay=3,capture_delay=2,measure_duration=5) 
    
    # 4. Device operation
    dhcpServerArgs={'lif_side':'network','device_name':'device_dhcp_server','ipv4if':'{address=192.85.1.16,gateway=192.85.1.1}','ethiiif':'{sourceMac=00:00:11:22:33:44}','vlanif':'{vlan1=100,pri1=0,vlan2=101,pri2=0}','serverConfig':'[Active=TRUE,hostName=dhcp_server1,leaseTime=600]','serverdefaultpoolconfig':'[StartIpList=192.85.1.5,hostAddrCount=16,addrIncrement=2,Active=TRUE]','option_66':'[msgType=OFFER,HexValue=FALSE,payload=135.251.200.1]','option_22':'[msgType=ACK,HexValue=TRUE]'}
    hlk_com_traffic.hlk_device('dhcpserver','create',**dhcpServerArgs) 
    verifyArgs={'device_name':'device_dhcp_server','expect_state':'UP','check_times':'30'}
    hlk_com_traffic.hlk_device('dhcpserver','verify_state',**verifyArgs)
    operArgs={'operation':'forceRenew','device_name':'device_dhcp_server'}
    hlk_com_traffic.hlk_device('dhcpserver','operate',**operArgs)
    delArgs={'device_name':'device_dhcp_server'}
    hlk_com_traffic.hlk_device('dhcpserver','delete',**delArgs)

    dhcpClientArgs={'lif_side':'client','device_name':'device_dhcp_client','ipv4if':'{address=192.85.1.200,gateway=192.85.1.1}','ethiiif':'{sourceMac=00:00:11:22:33:55}','vlanif':'{vlan1=100,pri1=0,vlan2=101,pri2=0}','circuitId':'circuitId_@p','enableCircuitId':'True','hostName':'client1','optionList':'"1 3 6 15 33 44 59"','remoteId':'remoteId_@p'}
    hlk_com_traffic.hlk_device('dhcpclient','create',**dhcpClientArgs)    
    verifyArgs={'device_name':'device_dhcp_client','expect_state':'BOUND','check_times':'30'}
    hlk_com_traffic.hlk_device('dhcpclient','verify_state',**verifyArgs)
    operArgs={'operation':'release','device_name':'device_dhcp_client'}
    hlk_com_traffic.hlk_device('dhcpclient','operate',**operArgs)
    delArgs={'device_name':'device_dhcp_client'}
    hlk_com_traffic.hlk_device('dhcpclient','delete',**delArgs)


    # 5. clean up to disconnect STC 
    hlk_com_traffic.hlk_traffic_cleanup()


if __name__ == "__main__":
   hlk_stc_operation()

