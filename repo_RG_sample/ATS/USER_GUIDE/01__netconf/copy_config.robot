*** Settings ***
Resource          ../resource_dpu.txt

*** Keywords ***
copy_config
    ${s_xml}    netconf get config
    log    ${s_xml}
    ${s_xml}    replace string    ${s_xml}    <data>    <config>
    ${s_xml}    replace string    ${s_xml}    </data>    </config>
    ${s_xml}    remove string    ${s_xml}    </rpc-reply>
    ${s_xml}    remove string using regexp    ${s_xml}    <rpc.*?>    ${EMPTY}
    ${s_xml}    remove string using regexp    ${s_xml}    <\\?xml .*?>    ${EMPTY}
    log    ${s_xml}
    netconf database    copy    source=${s_xml}    target=running
