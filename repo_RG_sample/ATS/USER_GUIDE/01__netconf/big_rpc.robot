*** Settings ***
Resource          resource_dpu.txt

*** Test Cases ***
big_rpc
    netconf edit rpc    classifiers{bbf-qos-policies}/classifier-entry    name 1    description 1    match-criteria/tag/index 0    match-criteria/tag/in-pbit-list 1    classifier-action-entry-cfg/action-type scheduling-traffic-class
    ...    classifier-action-entry-cfg/scheduling-traffic-class 1    comments=classifier \ match criteria and action    default_operation=default    error_option=error    test_option=test
    netconf edit rpc    interfaces{ietf-interfaces}/interface    name ${UPLINK_INTERFACE1}    type{ianaift:iana-if-type} ianaift:ethernetCsmacd    interface-usage{bbf-l2-forwarding}/interface-usage network-port    mac-learning{bbf-l2-forwarding}/mac-learning-enable true    mac-learning{bbf-l2-forwarding}/max-number-mac-addresses 10
    ...    comments=ethernet uplink interface
    Comment    netconf edit rpc    interfaces{ietf-interfaces}/interface{operation:delete}    name fast
    run keyword and continue on failure    netconf rpc
