*** Settings ***
Suite Setup       setup_hlk
Suite Teardown    cleanup_hlk
Library           Process
Library           XML
Resource          %{ROBOTREPO}/HLK/res/l2-common.txt

*** Test Cases ***
stc_ieee1588v2
    log    ===CREATE A MASTER CLOCK===
    ${oIEEE1588v2_master}    stc create device ieee1588v2    client    device_ieee1588v2_master    session_name=first_stc_session    ipv4if={address=192.168.1.10,gateway=192.168.1.1}    ethiiif={sourceMac=00:00:11:22:33:44}
    ...    vlanif={vlan1=100,pri1=0,vlan2=100,pri2=0}    ieee1588v2clockconfig=[LogSyncInterval=-4,LogMinDelayRequestInterval=-8,StepMode=ONE_STEP,TimeSrc=INTERNAL_OSCILLATOR,ClockClass=6,EnableUnicastNegotiation=TRUE]
    wait until keyword succeeds    10    2    stc retrieve device ieee1588v2    device_ieee1588v2_master    IEEE1588_STATE_MASTER    session_name=first_stc_session
    log    ===CREATE A SLAVE CLOCK===
    ${oIEEE1588v2_slave}    stc create device ieee1588v2    network    device_ieee1588v2_slave    session_name=first_stc_session    ipv4if={address=192.168.1.2,gateway=192.168.1.1}    ethiiif={sourceMac=00:00:11:22:33:55}
    ...    ieee1588v2clockconfig=[slaveOnly=True]
    wait until keyword succeeds    10    2    stc retrieve device ieee1588v2    device_ieee1588v2_slave    IEEE1588_STATE_LISTENING    session_name=first_stc_session
    log    ===MODIFY MASTER CLOCK TO SLAVE
    stc modify device ieee1588v2    device_ieee1588v2_master    oclock=${oIEEE1588v2_master}    ieee1588v2ClockConfig={encap=ETHERNET,slaveOnly=True}    session_name=first_stc_session
    wait until keyword succeeds    20    2    stc retrieve device ieee1588v2    device_ieee1588v2_master    IEEE1588_STATE_LISTENING    session_name=first_stc_session
    stc delete device    device_ieee1588v2_master    session_name=first_stc_session
    stc delete device    device_ieee1588v2_slave    session_name=first_stc_session

stc_dhcp_server
    LOG    ====================================
    LOG    START CAPTURE
    LOG    ====================================
    stc_start_capture    portID=all    session_name=first_stc_session
    log    ===CREATE DHCP SERVER===
    ${oDhcpServer}    stc create dhcp server    network    device_dhcp_server    ipv4if={address=192.85.1.16,gateway=192.85.1.1}    ethiiif={sourceMac=00:00:11:22:33:44}    vlanif={vlan1=100,pri1=0,vlan2=101,pri2=0}
    ...    serverConfig=[Active=TRUE,hostName=dhcp_client1,leaseTime=600]    serverdefaultpoolconfig=[StartIpList=192.85.1.5,hostAddrCount=16,addrIncrement=2,Active=TRUE]    option_66=[msgType=OFFER,HexValue=FALSE,payload=135.251.200.1]    option_22=[msgType=ACK,HexValue=TRUE]    session_name=first_stc_session
    wait until keyword succeeds    20    2    stc verify dhcp server state    device_dhcp_server    UP    session_name=first_stc_session
    log    ===CREATE DHCP CLIENT===
    ${oDhcpClient}    stc create dhcp client    client    device_dhcp_client    ipv4if={address=192.85.1.200,gateway=192.85.1.1}    ethiiif={sourceMac=00:00:11:22:33:55}    vlanif={vlan1=100,pri1=0,vlan2=101,pri2=0}
    ...    circuitId=circuitId_@p    enableCircuitId=True    hostName=client1    optionList="1 3 6 15 33 44 59"    remoteId=remoteId_@p    session_name=first_stc_session
    wait until keyword succeeds    10    2    stc verify dhcp client state    device_dhcp_client    BOUND    session_name=first_stc_session
    LOG    ====================================
    LOG    RENEW DHCPV4 SERVER
    LOG    ====================================
    stc operate dhcp server    operation=forceRenew    device=device_dhcp_server    session_name=first_stc_session
    wait until keyword succeeds    20    2    stc verify dhcp client state    device_dhcp_client    BOUND    session_name=first_stc_session
    LOG    ====================================
    LOG    REBIND DHCPV4 CLIENT
    LOG    ====================================
    stc operate dhcp client    operation=release    device=device_dhcp_client    session_name=first_stc_session
    wait until keyword succeeds    10    2    stc verify dhcp client state    device_dhcp_client    IDLE    session_name=first_stc_session
    log    ===STOP ALL DEVICES===
    stc stop devices    device=all    session_name=first_stc_session
    log    ===DELETE ALL DEVICES===
    stc delete device    device_dhcp_server    session_name=first_stc_session
    stc delete device    device_dhcp_client    session_name=first_stc_session
    LOG    ====================================
    LOG    STOP CAPTURE
    LOG    ====================================
    run keyword and continue on failure    stc_stop_capture    portID=all    session_name=first_stc_session

stc_igmp_host
    LOG    ===Start Capture===
    #stc_start_capture    portID=all    session_name=first_stc_session
    log    ===create igmp host===
    ${oIgmpHost}    stc create igmp host    client    device_igmp_host    ipv4if={address=192.85.1.16,gateway=192.85.1.1}    ethiiif={sourceMac=00:00:11:22:33:44}    hostconfig={Active=TRUE,version=igmp_v3}
    ...    groupmembership={Active=TRUE,FilterMode=INCLUDE,UserDefinedSources=TRUE}    multicastsource={StartIpList=1.0.0.2,PrefixLength=32,NetworkCount=1,AddrIncrement=1,Active=TRUE}    multicastgroups={StartIpList=225.0.0.1,PrefixLength=32,NetworkCount=1,AddrIncrement=1,Active=TRUE}    vlanif={vlan1=100,pri1=0}    session_name=first_stc_session
    wait until keyword succeeds    20    2    stc verify igmp host state    device_igmp_host    member    session_name=first_stc_session
    log    ===Leave Groups===
    stc operate igmp host    operation=leavegroups    device=device_igmp_host    session_name=first_stc_session
    wait until keyword succeeds    20    2    stc verify igmp host state    device_igmp_host    non_member    session_name=first_stc_session
    log    ===Join Groups===
    stc operate igmp host    operation=joingroups    device=device_igmp_host    session_name=first_stc_session
    wait until keyword succeeds    20    2    stc verify igmp host state    device_igmp_host    member    session_name=first_stc_session
    Set Suite Variable    ${sNetworkPortId}    //${stc_info['ip']}/${stc_info['network_slot']}/${stc_info['network_port']}
    Set Suite Variable    ${sClientPortId}    //${stc_info['ip']}/${stc_info['client_slot']}/${stc_info['client_port']}
    log    ===Create and Config Streamblock===
    Stc Config Streamblock    name=DS1    portid=${sNetworkPortId}    session_name=first_stc_session
    Stc Config Streamblock    name=DS1    type=ethii    dstMac=01:00:5e:00:00:01    srcMac=00:00:02:00:00:03    session_name=first_stc_session
    Stc Config Streamblock    name=DS1    type=ipv4    destAddr=225.0.0.1    sourceAddr=30.30.0.2    session_name=first_stc_session
    Stc Config Streamblock    name=DS1    type=load    load=2    loadunit=FRAMES_PER_SECOND    session_name=first_stc_session
    log    ===Start Streamblock===
    Stc Start Traffic    streamblock=DS1    session_name=first_stc_session
    sleep    1
    log    ===Stop Streamblock===
    Stc StopTraffic    streamblock=DS1    session_name=first_stc_session
    log    ===Stop All Devices===
    stc stop devices    device=all    session_name=first_stc_session
    log    ===Delete All Devices===
    stc delete device    device_igmp_host    session_name=first_stc_session
    LOG    ===Delete All Streamblock===
    Stc Delete Streamblock    streamblock=all    session_name=first_stc_session
    LOG    ===Stop Capture===
    #run keyword and continue on failure    stc_stop_capture    portID=all    session_name=first_stc_session
    log    ${stc_info}
    #Set Suite Variable    ${sClientPortId}    //${stc_info['ip']}/${stc_info['client_slot']}/${stc_info['client_port']}
    #${Retrieve2_Capture} =    stc_retrieve_capture    portID=[${sClientPortId}]    session_name=first_stc_session
    #${Retrieve1_Capture} =    stc_retrieve_capture    portID=[${sNetworkPortId}]    session_name=first_stc_session

stc_pppoe_server/client
    LOG    ===Start Capture===
    #stc_start_capture    portID=all    session_name=first_stc_session
    log    ===create pppoe server===
    ${oPppoeserver}    stc create pppoe server    network    device_pppoe_server    ipv4if={address=192.85.1.15,gateway=192.85.1.1}    ethiiif={sourceMac=00:00:11:22:33:55}    vlanif={vlan1=100,pri1=0,vlan2=101,pri2=0}
    ...    pppif={Active=TRUE}    pppoeif={PeerMacAddrList=00:10:94:00:00:05,SessionResolver=PppoeProtocol}    serverconfig={Active=TRUE,Protocol=PPPOE}    emulationmode={EmulationType=server,Active=TRUE}    pppoeserverpool={Ipv4PeerPoolAddr=192.0.1.0,StartIpList=192.0.1.0}    session_name=first_stc_session
    log    ===create pppoe client===
    ${oPppoeClient}    stc create pppoe client    client    device_pppoe_client    ipv4if={address=192.85.1.16,gateway=192.85.1.1}    ethiiif={sourceMac=00:00:11:22:33:44}    vlanif={vlan1=100,pri1=0,vlan2=101,pri2=0}
    ...    pppif={Active=TRUE}    pppoeif={PeerMacAddrList=00:10:94:00:00:06,SessionResolver=PppoeProtocol}    clientconfig={Active=TRUE,Protocol=PPPOE}    emulationmode={EmulationType=client,Active=TRUE}    session_name=first_stc_session
    wait until keyword succeeds    20    2    stc verify pppoe server state    device_pppoe_server    connected    session_name=first_stc_session
    wait until keyword succeeds    20    2    stc verify pppoe client state    device_pppoe_client    connected    session_name=first_stc_session
    log    ===pppoe disconnect===
    stc operate pppoe server    operation=disconnectserver    device=device_pppoe_server    session_name=first_stc_session
    wait until keyword succeeds    20    2    stc verify pppoe server state    device_pppoe_server    idle    session_name=first_stc_session
    wait until keyword succeeds    20    2    stc verify pppoe client state    device_pppoe_client    idle    session_name=first_stc_session
    log    ===pppoe connect===
    stc operate pppoe server    operation=connectserver    device=device_pppoe_server    session_name=first_stc_session
    wait until keyword succeeds    20    2    stc verify pppoe server state    device_pppoe_server    connecting    session_name=first_stc_session
    wait until keyword succeeds    20    2    stc verify pppoe client state    device_pppoe_client    idle    session_name=first_stc_session
    stc operate pppoe client    operation=connectclient    device=device_pppoe_client    session_name=first_stc_session
    wait until keyword succeeds    20    2    stc verify pppoe server state    device_pppoe_server    connected    session_name=first_stc_session
    wait until keyword succeeds    20    2    stc verify pppoe client state    device_pppoe_client    connected    session_name=first_stc_session
    Set Suite Variable    ${sNetworkPortId}    //${stc_info['ip']}/${stc_info['network_slot']}/${stc_info['network_port']}
    Set Suite Variable    ${sClientPortId}    //${stc_info['ip']}/${stc_info['client_slot']}/${stc_info['client_port']}
    #sleep    5
    stc stop devices    device=all    session_name=first_stc_session
    log    ===Delete All Devices===
    stc delete device    device_pppoe_server    session_name=first_stc_session
    stc delete device    device_pppoe_client    session_name=first_stc_session
    LOG    ===Stop Capture===
    #run keyword and continue on failure    stc_stop_capture    portID=all    session_name=first_stc_session
    log    ${stc_info}
    #${Retrieve2_Capture} =    stc_retrieve_capture    portID=[${sClientPortId}]    session_name=first_stc_session
    #${Retrieve1_Capture} =    stc_retrieve_capture    portID=[${sNetworkPortId}]    session_name=first_stc_session

stc_dhcpv6_server/client
    LOG    ===Start Capture===
    #stc_start_capture    portID=all    session_name=first_stc_session
    log    ===create dhcpv6 server===
    ${oDhcpv6server}    stc create dhcpv6 server    network    device_dhcpv6_server    ethiiif={sourceMac=00:00:11:22:33:55}    ipv6if={address=2001::5 ,gateway=2001::1}    vlanif={vlan1=100,pri1=0,vlan2=101,pri2=0}
    ...    ipv6llif={Address=fe80::1}    serverconfig={Active=TRUE,EmulationMode=DHCPV6}    serverdefaultaddrpool={HostAddrStep=::1,StartIpList=2000::8,NetworkCount=1}    serverdefaultprefixpool={ServerPrefixStep=::,StartIpList=2000::,NetworkCount =200}    session_name=first_stc_session
    log    ===create dhcpv6 client===
    ${oDhcpv6client}    stc create dhcpv6 client    client    device_dhcpv6_client    ethiiif={sourceMac=00:00:11:22:33:44}    ipv6if={active=true}    vlanif={vlan1=100,pri1=0,vlan2=101,pri2=0}
    ...    ipv6llif={Address=fe80::2}    clientconfig={Active=TRUE,Dhcpv6ClientMode=DHCPV6ANDPD}    session_name=first_stc_session
    wait until keyword succeeds    20    2    stc verify dhcpv6 server state    device_dhcpv6_server    up    session_name=first_stc_session
    wait until keyword succeeds    20    2    stc verify dhcpv6 client state    device_dhcpv6_client    bound    session_name=first_stc_session
    wait until keyword succeeds    20    2    stc verify dhcpv6 client pdstate    device_dhcpv6_client    bound    session_name=first_stc_session
    log    ===dhcpv6 server stop===
    stc operate dhcpv6 server    operation=stopserver    device=device_dhcpv6_server    session_name=first_stc_session
    wait until keyword succeeds    20    2    stc verify dhcpv6 server state    device_dhcpv6_server    none    session_name=first_stc_session
    wait until keyword succeeds    20    2    stc verify dhcpv6 client state    device_dhcpv6_client    bound    session_name=first_stc_session
    log    ===dhcpv6 server start===
    stc operate dhcpv6 server    operation=startserver    device=device_dhcpv6_server    session_name=first_stc_session
    wait until keyword succeeds    20    2    stc verify dhcpv6 server state    device_dhcpv6_server    up    session_name=first_stc_session
    #wait until keyword succeeds    20    2    stc verify pppoe client state    device_pppoe_client    idle    session_name=first_stc_session
    log    ===dhcpv6 client release===
    stc operate dhcpv6 client    operation=releaseclient    device=device_dhcpv6_client    session_name=first_stc_session
    wait until keyword succeeds    20    2    stc verify dhcpv6 client state    device_dhcpv6_client    idle    session_name=first_stc_session
    wait until keyword succeeds    20    2    stc verify dhcpv6 client pdstate    device_dhcpv6_client    idle    session_name=first_stc_session
    log    ===dhcpv6 client bind===
    stc operate dhcpv6 client    operation=bindclient    device=device_dhcpv6_client    session_name=first_stc_session
    #stc operate dhcpv6 client    operation=rebindclient    device=device_dhcpv6_client    session_name=first_stc_session
    wait until keyword succeeds    20    2    stc verify dhcpv6 client state    device_dhcpv6_client    bound    session_name=first_stc_session
    wait until keyword succeeds    20    2    stc verify dhcpv6 client pdstate    device_dhcpv6_client    bound    session_name=first_stc_session
    Set Suite Variable    ${sNetworkPortId}    //${stc_info['ip']}/${stc_info['network_slot']}/${stc_info['network_port']}
    Set Suite Variable    ${sClientPortId}    //${stc_info['ip']}/${stc_info['client_slot']}/${stc_info['client_port']}
    #sleep    5
    log    ===Stop All Devices===
    stc stop devices    device=all    session_name=first_stc_session
    LOG    ===Stop Capture===
    #run keyword and continue on failure    stc_stop_capture    portID=all    session_name=first_stc_session
    log    ${stc_info}
    #${Retrieve2_Capture} =    stc_retrieve_capture    portID=[${sClientPortId}]    session_name=first_stc_session
    #${Retrieve1_Capture} =    stc_retrieve_capture    portID=[${sNetworkPortId}]    session_name=first_stc_session
    log    ===Delete All Devices===
    stc delete device    device_dhcpv6_server    session_name=first_stc_session
    stc delete device    device_dhcpv6_client    session_name=first_stc_session

stc_802.1x
    LOG    ===Start Capture===
    #stc_start_capture    portID=all    session_name=first_stc_session
    log    ===create 802.1x client===
    ${o8021xClient}    stc create 8021x auth    client    device_8021x_client    ipv4if={address=135.251.200.194,gateway=135.251.200.199}    ethiiif={sourceMac=00:00:11:22:33:44}    8021xconfig={Active=TRUE,EapAuthMethod=MD5}
    ...    md5={UserId=cliuser1,Password=password1!}    session_name=first_stc_session
    wait until keyword succeeds    20    2    stc verify 8021x auth state    device_8021x_client    AUTHENTICATING    session_name=first_stc_session
    #wait until keyword succeeds    20    2    stc verify 8021x auth state    device_8021x_client    auth_success    session_name=first_stc_session
    stc operate 8021x auth    operation=startauth    device=device_8021x_client    session_name=first_stc_session
    #wait until keyword succeeds    20    2    stc verify pppoe server state    device_pppoe_server    connected    session_name=first_stc_session
    #wait until keyword succeeds    20    2    stc verify pppoe client state    device_pppoe_client    connected    session_name=first_stc_session
    Set Suite Variable    ${sNetworkPortId}    //${stc_info['ip']}/${stc_info['network_slot']}/${stc_info['network_port']}
    Set Suite Variable    ${sClientPortId}    //${stc_info['ip']}/${stc_info['client_slot']}/${stc_info['client_port']}
    sleep    5
    stc stop devices    device=all    session_name=first_stc_session
    log    ===Delete All Devices===
    LOG    ===Stop Capture===
    #run keyword and continue on failure    stc_stop_capture    portID=all    session_name=first_stc_session
    log    ${stc_info}
    #${Retrieve2_Capture} =    stc_retrieve_capture    portID=[${sClientPortId}]    session_name=first_stc_session
    #${Retrieve1_Capture} =    stc_retrieve_capture    portID=[${sNetworkPortId}]    session_name=first_stc_session
    stc delete device    device_8021x_client    session_name=first_stc_session

stc_ospfv2_router
    LOG    ===Start Capture===
    #stc_start_capture    portID=all    session_name=first_stc_session
    log    ===create ospfv2 router===
    ${kospfv2router}    stc create ospfv2 router    client    device_ospfv2_router    ipv4if={address=192.85.1.16,gateway=192.85.1.1}    ethiiif={sourceMac=00:00:11:22:33:44}    vlanif={vlan1=100,pri1=0,vlan2=101,pri2=0}
    ...    routerconfig={Active=TRUE,AreaId=0.0.0.0}    extroutecount={RouteCountPerRouter=1}    extrouteaddr={StartIpList=21.0.0.0,NetworkCount=1}    session_name=first_stc_session
    wait until keyword succeeds    20    2    stc verify ospfv2 router state    device_ospfv2_router    down    session_name=first_stc_session
    #wait until keyword succeeds    20    2    stc verify 8021x client state    device_8021x_client    auth_failed    session_name=first_stc_session
    #stc operate 8021x client    operation=startauth    device=device_8021x_client    session_name=first_stc_session
    #wait until keyword succeeds    20    2    stc verify pppoe server state    device_pppoe_server    connected    session_name=first_stc_session
    #wait until keyword succeeds    20    2    stc verify pppoe client state    device_pppoe_client    connected    session_name=first_stc_session
    Set Suite Variable    ${sNetworkPortId}    //${stc_info['ip']}/${stc_info['network_slot']}/${stc_info['network_port']}
    Set Suite Variable    ${sClientPortId}    //${stc_info['ip']}/${stc_info['client_slot']}/${stc_info['client_port']}
    sleep    20
    stc stop devices    device=all    session_name=first_stc_session
    log    ===Delete All Devices===
    LOG    ===Stop Capture===
    #run keyword and continue on failure    stc_stop_capture    portID=all    session_name=first_stc_session
    log    ${stc_info}
    #${Retrieve2_Capture} =    stc_retrieve_capture    portID=[${sClientPortId}]    session_name=first_stc_session
    #${Retrieve1_Capture} =    stc_retrieve_capture    portID=[${sNetworkPortId}]    session_name=first_stc_session
    stc delete device    device_ospfv2_router    session_name=first_stc_session

stc_rip_router
    LOG    ===Start Capture===
    #stc_start_capture    portID=all    session_name=first_stc_session
    log    ===create rip router===
    ${kriprouter}    stc create rip router    client    device_rip_router    ipv4if={address=192.85.1.16,gateway=192.85.1.1}    ethiiif={sourceMac=00:00:11:22:33:44}    vlanif={vlan1=100,pri1=0,vlan2=101,pri2=0}
    ...    routerconfig={Active=TRUE,DutIpv4Addr=224.0.0.9,RipVersion=V2}    riprouteparams={Metric=2}    riprouteaddr={StartIpList=21.0.0.0,NetworkCount=3}    session_name=first_stc_session
    wait until keyword succeeds    20    2    stc verify rip router state    device_rip_router    open    session_name=first_stc_session
    #wait until keyword succeeds    20    2    stc verify 8021x client state    device_8021x_client    auth_failed    session_name=first_stc_session
    stc operate rip router    operation=stoprip    device=device_rip_router    session_name=first_stc_session
    wait until keyword succeeds    20    2    stc verify rip router state    device_rip_router    closed    session_name=first_stc_session
    stc operate rip router    operation=startrip    device=device_rip_router    session_name=first_stc_session
    wait until keyword succeeds    20    2    stc verify rip router state    device_rip_router    open    session_name=first_stc_session
    Set Suite Variable    ${sNetworkPortId}    //${stc_info['ip']}/${stc_info['network_slot']}/${stc_info['network_port']}
    Set Suite Variable    ${sClientPortId}    //${stc_info['ip']}/${stc_info['client_slot']}/${stc_info['client_port']}
    sleep    20
    stc stop devices    device=all    session_name=first_stc_session
    log    ===Delete All Devices===
    LOG    ===Stop Capture===
    #run keyword and continue on failure    stc_stop_capture    portID=all    session_name=first_stc_session
    log    ${stc_info}
    #${Retrieve2_Capture} =    stc_retrieve_capture    portID=[${sClientPortId}]    session_name=first_stc_session
    #${Retrieve1_Capture} =    stc_retrieve_capture    portID=[${sNetworkPortId}]    session_name=first_stc_session
    stc delete device    device_rip_router    session_name=first_stc_session

stc_bgp_router
    LOG    ===Start Capture===
    #stc_start_capture    portID=all    session_name=first_stc_session
    log    ===create bgp router===
    ${kbgprouter}    stc create bgp router    client    device_bgp_router    ipv4if={address=192.85.1.16,gateway=192.85.1.1}    ethiiif={sourceMac=00:00:11:22:33:44}    vlanif={vlan1=100,pri1=0,vlan2=101,pri2=0}
    ...    routerconfig={Active=TRUE,Dutasnum=200,asnum=100}    bgprouteparams={nexthop=192.85.1.16,aspath=100}    bgprouteaddr={StartIpList=21.0.0.0,NetworkCount=3}    session_name=first_stc_session
    wait until keyword succeeds    20    2    stc verify bgp router state    device_bgp_router    CONNECT    session_name=first_stc_session
    #wait until keyword succeeds    20    2    stc verify 8021x client state    device_8021x_client    auth_failed    session_name=first_stc_session
    stc operate bgp router    operation=stopbgp    device=device_bgp_router    session_name=first_stc_session
    wait until keyword succeeds    20    2    stc verify bgp router state    device_bgp_router    idle    session_name=first_stc_session
    stc operate bgp router    operation=startbgp    device=device_bgp_router    session_name=first_stc_session
    wait until keyword succeeds    20    2    stc verify bgp router state    device_bgp_router    connect    session_name=first_stc_session
    Set Suite Variable    ${sNetworkPortId}    //${stc_info['ip']}/${stc_info['network_slot']}/${stc_info['network_port']}
    Set Suite Variable    ${sClientPortId}    //${stc_info['ip']}/${stc_info['client_slot']}/${stc_info['client_port']}
    sleep    20
    stc stop devices    device=all    session_name=first_stc_session
    log    ===Delete All Devices===
    LOG    ===Stop Capture===
    #run keyword and continue on failure    stc_stop_capture    portID=all    session_name=first_stc_session
    log    ${stc_info}
    #${Retrieve2_Capture} =    stc_retrieve_capture    portID=[${sClientPortId}]    session_name=first_stc_session
    #${Retrieve1_Capture} =    stc_retrieve_capture    portID=[${sNetworkPortId}]    session_name=first_stc_session
    stc delete device    device_bgp_router    session_name=first_stc_session

hlk_device_dhcpserver
    stc_start_capture    portID=all
    LOG    ====================================
    log    ===CREATE DHCP SERVER===
    LOG    ====================================
    hlk_device    dhcpserver    create    lif_side=network    device_name=device_dhcp_server    ipv4if={address=192.85.1.16,gateway=192.85.1.1}    ethiiif={sourceMac=00:00:11:22:33:44}
    ...    vlanif={vlan1=100,pri1=0,vlan2=101,pri2=0}    serverConfig=[Active=TRUE,hostName=dhcp_client1,leaseTime=600]    serverdefaultpoolconfig=[StartIpList=192.85.1.5,hostAddrCount=16,addrIncrement=2,Active=TRUE]    option_66=[msgType=OFFER,HexValue=FALSE,payload=135.251.200.1]    option_22=[msgType=ACK,HexValue=TRUE]
    hlk device    dhcpserver    verify_state    device_name=device_dhcp_server    expect_state=UP    check_times=10
    LOG    ====================================
    log    ===CREATE DHCP CLIENT===
    LOG    ====================================
    hlk_device    dhcpclient    create    lif_side=client    device_name=device_dhcp_client    ipv4if={address=192.85.1.200,gateway=192.85.1.1}    ethiiif={sourceMac=00:00:11:22:33:55}
    ...    vlanif={vlan1=100,pri1=0,vlan2=101,pri2=0}    circuitId=circuitId_@p    enableCircuitId=True    hostName=client1    optionList="1 3 6 15 33 44 59"    remoteId=remoteId_@p
    hlk device    dhcpclient    verify_state    device_name=device_dhcp_client    expect_state=BOUND    check_times=10
    LOG    ====================================
    LOG    RENEW DHCPV4 SERVER
    LOG    ====================================
    hlk_device    dhcpserver    operate    operation=forceRenew    device_name=device_dhcp_server
    hlk device    dhcpclient    verify_state    device_name=device_dhcp_client    expect_state=BOUND    check_times=10
    LOG    ====================================
    LOG    REBIND DHCPV4 CLIENT
    LOG    ====================================
    hlk_device    dhcpclient    operate    operation=release    device_name=device_dhcp_client
    hlk device    dhcpclient    verify_state    device_name=device_dhcp_client    expect_state=IDLE    check_times=10
    log    ===STOP ALL DEVICES===
    stc stop devices    device=all
    log    ===DELETE ALL DEVICES===
    hlk_device    dhcpserver    delete    device_name=device_dhcp_server
    hlk_device    dhcpclient    delete    device_name=device_dhcp_client
    LOG    ====================================
    LOG    STOP CAPTURE
    LOG    ====================================
    run keyword and continue on failure    stc_stop_capture    portID=all

*** Keywords ***
save_netconf_config_to_file
    [Arguments]    ${reply_xml}    ${path}    ${file_name}
    log    ${reply_xml}
    log    ===============
    log    GET \ INTERFACES ELEMENT
    log    ===============
    ${xml_root}    parse xml    ${reply_xml}
    ${test}    element to string    ${xml_root}
    ${interface_element}    get element    ${xml_root}    xpath=${path}
    ${config_str}    element to string    ${interface_element}
    log    ===============
    log    ADD CONFIG TAG
    log    ===============
    ${config_str}    set variable    <config>\r\n${config_str}\r\n</config>
    log    ${config_str}
    create file    ${file_name}    ${config_str}

recover_netconf_config
    [Arguments]    ${file_name}
    ${xml_str}    get file    ${file_name}
    log    ${xml_str}

setup_stc
    log    %{SETUPFILENAME}
    create global tables    %{SETUPFILENAME}
    &{stc_info}    get table line    TrafficGenData    type=STC
    stc connect    &{stc_info}    session_name=first_stc_session
    set suite variable    &{stc_info}    &{stc_info}

cleanup_stc
    stc disconnect    session_name=first_stc_session

setup_hlk
    log    %{SETUPFILENAME}
    create global tables    %{SETUPFILENAME}
    &{stc_info}    get table line    TrafficGenData    type=STC
    &{pcta_info}    get table line    TrafficGenData    type=PCTA
    @{TAToolList}    create list    ${stc_info}
    # setup
    hlk traffic setup    @{TAToolList}
    # set ta_type to STC,default ta_type is PCTA
    hlk traffic set ta type    STC

cleanup_hlk
    # cleanup
    hlk_traffic_cleanup
