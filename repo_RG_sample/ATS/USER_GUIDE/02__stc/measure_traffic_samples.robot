*** Settings ***
Library           Process
Library           XML
Resource          %{ROBOTREPO}/HLK/res/l2-common.txt

*** Test Cases ***
CASE1
    &{stc_info}    get table line    TrafficGenData    type=STC
    &{stc_info}    create dictionary    &{stc_info}    durationMode=BURSTS    burstSize=20000
    stc connect    &{stc_info}
    Comment    ${dStream1}    Create_Dictionary    vlan.vlan1=11    vlan.pri1=1    vlan.vlan2=12    vlan.pri2=2
    ...    ethii.srcMac=11:11:11:11:11:11    ethii.dstMac=22:22:22:22:22:22    ipv4.destAddr=135.251.1.1    ipv4.sourceAddr=135.251.2.2    load.load=5000    load.loadunit=BITS_PER_SECOND
    Comment    ${dStream2}    Create_Dictionary    vlan.vlan1=12    vlan.pri1=5    frame.framelengthmode=auto    load.load=1000
    ...    load.loadunit=BITS_PER_SECOND
    Comment    ${dStream3}    Create_Dictionary    vlan.vlan1=13    frame.fixedframelength=512    load.load=5000    load.loadunit=BITS_PER_SECOND
    Comment    @{lStreamList}    create list    ${dStream1}    ${dStream2}    ${dStream3}
    Comment    ${dMeasure1}    create dictionary    bitrate=5000    BitRate_tol=20%    bitcount=2000000    l1bitrate=5000
    ...    l1bitrate_tol=5%    bitcount_tol=50%
    Comment    ${dMeasure2}    create dictionary    bitrate=1000    BitRate_tol=5%    framecount=2000    framecount_tol=0%
    Comment    ${dMeasure3}    create dictionary    bitrate=5000
    Comment    @{lMeasureList}    create list    ${dMeasure1}    ${dMeasure2}    ${dMeasure3}
    @{lFilterItems}    create list    l1bitrate    bitrate    cellrate
    ${dStream1}    Create_Dictionary    vlan.vlan1=11    vlan.pri1=1    vlan.vlan2=12    vlan.pri2=2    ethii.srcMac=11:11:11:11:11:11
    ...    ethii.dstMac=22:22:22:22:22:22    ipv4.destAddr=135.251.1.1    ipv4.sourceAddr=135.251.2.2    load.load=1000000    load.loadunit=BITS_PER_SECOND
    ${dMeasure1}    create dictionary    l1bitrate=1000000    l1bitrate_tol=5%
    ${dStream2}    Create_Dictionary    vlan.vlan1=12    load.load=2000000    vlan.pri1=1    vlan.vlan2=12    vlan.pri2=2
    ...    ethii.srcMac=11:11:11:11:11:11    ethii.dstMac=22:22:22:22:22:22    ipv4.destAddr=135.251.1.1    ipv4.sourceAddr=135.251.2.2    load.loadunit=BITS_PER_SECOND
    ${dMeasure2}    create dictionary    l1bitrate=2000000    l1bitrate_tol=5%
    ${dStream3}    Create_Dictionary    vlan.vlan1=13    load.load=3000000    vlan.pri1=1    vlan.vlan2=12    vlan.pri2=2
    ...    ethii.srcMac=11:11:11:11:11:11    ethii.dstMac=22:22:22:22:22:22    ipv4.destAddr=135.251.1.1    ipv4.sourceAddr=135.251.2.2    load.loadunit=BITS_PER_SECOND
    ${dMeasure3}    create dictionary    l1bitrate=3000000    l1bitrate_tol=5%
    ${dStream4}    Create_Dictionary    vlan.vlan1=14    load.load=4000000    vlan.pri1=1    vlan.vlan2=12    vlan.pri2=2
    ...    ethii.srcMac=11:11:11:11:11:11    ethii.dstMac=22:22:22:22:22:22    ipv4.destAddr=135.251.1.1    ipv4.sourceAddr=135.251.2.2    load.loadunit=BITS_PER_SECOND
    ${dMeasure4}    create dictionary    l1bitrate=4000000    l1bitrate_tol=5%
    ${dStream5}    Create_Dictionary    vlan.vlan1=15    load.load=5000000    vlan.pri1=1    vlan.vlan2=12    vlan.pri2=2
    ...    ethii.srcMac=11:11:11:11:11:11    ethii.dstMac=22:22:22:22:22:22    ipv4.destAddr=135.251.1.1    ipv4.sourceAddr=135.251.2.2    load.loadunit=BITS_PER_SECOND
    ${dMeasure5}    create dictionary    l1bitrate=5000000    l1bitrate_tol=5%
    ${dStream6}    Create_Dictionary    vlan.vlan1=16    load.load=6000000    vlan.pri1=1    vlan.vlan2=12    vlan.pri2=2
    ...    ethii.srcMac=11:11:11:11:11:11    ethii.dstMac=22:22:22:22:22:22    ipv4.destAddr=135.251.1.1    ipv4.sourceAddr=135.251.2.2    load.loadunit=BITS_PER_SECOND
    ${dMeasure6}    create dictionary    l1bitrate=6000000    l1bitrate_tol=5%
    ${dStream7}    Create_Dictionary    vlan.vlan1=17    load.load=7000000    vlan.pri1=1    vlan.vlan2=12    vlan.pri2=2
    ...    ethii.srcMac=11:11:11:11:11:11    ethii.dstMac=22:22:22:22:22:22    ipv4.destAddr=135.251.1.1    ipv4.sourceAddr=135.251.2.2    load.loadunit=BITS_PER_SECOND
    ${dMeasure7}    create dictionary    l1bitrate=7000000    l1bitrate_tol=5%
    ${dStream8}    Create_Dictionary    vlan.vlan1=18    load.load=8000000    vlan.pri1=1    vlan.vlan2=12    vlan.pri2=2
    ...    ethii.srcMac=11:11:11:11:11:11    ethii.dstMac=22:22:22:22:22:22    ipv4.destAddr=135.251.1.1    ipv4.sourceAddr=135.251.2.2    load.loadunit=BITS_PER_SECOND
    ${dMeasure8}    create dictionary    l1bitrate=8000000    l1bitrate_tol=5%
    @{lStreamList}    create list    ${dStream1}    ${dStream2}    ${dStream3}    ${dStream4}    ${dStream5}
    ...    ${dStream6}    ${dStream7}    ${dStream8}
    @{lMeasureList}    create list    ${dMeasure1}    ${dMeasure2}    ${dMeasure3}    ${dMeasure4}    ${dMeasure5}
    ...    ${dMeasure6}    ${dMeasure7}    ${dMeasure8}
    Comment    @{FilterResult}    stc upstream ip traffic measure    ${lStreamList}    measure_list=${lMeasureList}    retrieve_times=200    wait_time=5
    ...    duration=10
    Comment    @{FilterResult}    stc downstream ip traffic measure    ${lStreamList}    measure_list=${lMeasureList}    filter_list=${lFilterItems}    duration=10
    ...    expect_result=FAIL,NA,FAIL
    ${returnFilter}    stc bidirect ip traffic measure    ${lStreamList}    ${lStreamList}    retrieve_times=20    wait_time=10    up_filter_list=${lFilterItems}
    ...    down_measure_list=${lMeasureList}    up_measure_list=${lMeasureList}    duration=20    up_expect_result=PASS,PASS,PASS,PASS,PASS,PASS,PASS,PASS    up_expect_result=PASS,PASS,PASS,PASS,PASS,PASS,PASS,PASS
    stc disconnect

xml
    log    ===============
    log    ${xml_str}
    save netconf config to file    ${xml_str}    data/interfaces    /tftpboot/config.xml
    recover netconf_config    /tftpboot/config.xml

drop_frame_count_0
    log    %{SETUPFILENAME}
    &{stc_info}    get table line    TrafficGenData    type=STC
    stc connect    &{stc_info}
    Comment    ${dStream1}    Create_Dictionary    vlan.vlan1=11    vlan.pri1=1    vlan.vlan2=12    vlan.pri2=2
    ...    ethii.srcMac=11:11:11:11:11:11    ethii.dstMac=22:22:22:22:22:22    ipv4.destAddr=135.251.1.1    ipv4.sourceAddr=135.251.2.2    load.load=5000    load.loadunit=BITS_PER_SECOND
    Comment    ${dStream2}    Create_Dictionary    vlan.vlan1=12    vlan.pri1=5    frame.framelengthmode=auto    load.load=1000
    ...    load.loadunit=BITS_PER_SECOND
    Comment    ${dStream3}    Create_Dictionary    vlan.vlan1=13    frame.fixedframelength=512    load.load=5000    load.loadunit=BITS_PER_SECOND
    Comment    @{lStreamList}    create list    ${dStream1}    ${dStream2}    ${dStream3}
    Comment    ${dMeasure1}    create dictionary    bitrate=5000    BitRate_tol=20%    bitcount=2000000    l1bitrate=5000
    ...    l1bitrate_tol=5%    bitcount_tol=50%
    Comment    ${dMeasure2}    create dictionary    bitrate=1000    BitRate_tol=5%    framecount=2000    framecount_tol=0%
    Comment    ${dMeasure3}    create dictionary    bitrate=5000
    Comment    @{lMeasureList}    create list    ${dMeasure1}    ${dMeasure2}    ${dMeasure3}
    @{lFilterItems}    create list    l1bitrate    bitrate    cellrate
    ${dStream1}    Create_Dictionary    vlan.vlan1=11    vlan.pri1=1    vlan.vlan2=12    vlan.pri2=2    ethii.srcMac=11:11:11:11:11:11
    ...    ethii.dstMac=22:22:22:22:22:22    ipv4.destAddr=135.251.1.1    ipv4.sourceAddr=135.251.2.2    load.load=1000000    load.loadunit=BITS_PER_SECOND
    ${dMeasure1}    create dictionary    droppedFrameCount=0
    @{lStreamList}    create list    ${dStream1}
    @{lMeasureList}    create list    ${dMeasure1}
    Comment    ${returnFilter}    stc bidirect ip traffic measure    ${lStreamList}    ${lStreamList}    up_filter_list=${lFilterItems}    retrieve_times=20
    ...    wait_time=1    down_measure_list=${lMeasureList}    up_measure_list=${lMeasureList}    duration=3    up_expect_result=PASS,PASS,PASS,PASS,PASS,PASS,PASS,PASS    up_expect_result=PASS,PASS,PASS,PASS,PASS,PASS,PASS,PASS
    ${returnFilter}    stc bidirect ip traffic measure    ${lStreamList}    ${lStreamList}    retrieve_times=20    wait_time=1    down_measure_list=${lMeasureList}
    ...    up_measure_list=${lMeasureList}    duration=3    up_expect_result=PASS,PASS,PASS,PASS,PASS,PASS,PASS,PASS    up_expect_result=PASS,PASS,PASS,PASS,PASS,PASS,PASS,PASS
    stc disconnect

ieee1588v2
    log    %SETUPFILENAME
    &{stc_info}    get table line    TrafficGenData    type=STC
    stc connect    &{stc_info}    session_name=first_stc
    ${port_id}    set variable
    ${result}    stc create device ieee1588v2    client    device_ieee1588v2_1    session_name=first_stc    ethiiif={sourceMac=00:00:11:22:33:44}    vlanif={vlan1=100,pri1=0,vlan2=100,pri2=0}
    ...    ieee1588v2ClockConfig={encap=ETHERNET}
    stc disconnect    session_name=first_stc

hlk_dhcp_server
    log    %{SETUPFILENAME}
    Comment    &{stc_info}    get table line    TrafficGenData    type=STC
    Comment    stc connect    &{stc_info}
    log    NEED MODIFIED
    Set Suite Variable    ${sNetworkPortId}    //135.251.247.12/2/1
    Set Suite Variable    ${sClientPortId}    //135.251.247.12/2/2
    hlk_traffic_config_port    portID=${sClientPortId}    burstSize=1    durationMode=CONTINUOUS
    hlk_traffic_config_port    portID=${sNetworkPortId}    burstSize=1    durationMode=CONTINUOUS
    hlk_traffic_connect
    ${dEthernetParams} =    Create_Dictionary    SourceMac=00:00:40:20:77:50
    #${dSBConfigParams} =    Create_Dictionary    frameLengthMode=FIXED    fixedFrameLength=1518
    ${dIPv4Params} =    Create_Dictionary    address=40.20.1.2    gateway=40.20.1.1    prefixLength=30
    hlk_traffic_config_device    name=NetworkDevice    portID=${sNetworkPortId}
    hlk_traffic_config_device    name=NetworkDevice    type=ethii    dConfigParam=${dEthernetParams}
    hlk_traffic_config_device    name=NetworkDevice    type=ipv4    address=40.20.1.1    gateway=40.20.1.2    prefixLength=24
    hlk_traffic_config_dhcp_server    device=NetworkDevice    serverConfig=[Active=TRUE]    serverdefaultpoolconfig=[StartIpList=192.85.1.5,Active=TRUE]
    hlk_traffic_config_device    name=ClientDevice    portID=${sClientPortId}
    hlk_traffic_config_device    name=ClientDevice    type=ipv4    address=20.20.20.20
    hlk_traffic_config_dhcp_client    device=ClientDevice    serverConfig=[Active=TRUE]
    hlk_traffic_start_devices
    #Start devices SB1 - DS, SB2 - US
    hlk_traffic_config_bound_streamblock    name=UPSTREAM    portID=${sClientPortId}    device=ClientDevice
    hlk_traffic_config_bound_streamblock    name=UPSTREAM    type=udp    sourcePort=655    destPort=800    length=15    checksum=22
    hlk_traffic_config_bound_streamblock    name=UPSTREAM    type=frame    FrameLengthMode=RANDOM    MinFrameLength=500    MaxFrameLength=1500    EnableResolveDestMacAddress=FALSE
    #hlk_traffic_config_bound_streamblock    name=UPSTREAM    type=frame    srcbinding=    dstbinding=
    # Input for source and destination should be IPv4 objects
    hlk_traffic_config_bound_streamblock    name=UPSTREAM    type=load    load=1    loadunit=MEGABITS_PER_SECOND
    hlk_traffic_config_bound_streamblock    name=DOWNSTREAM    portID=${sNetworkPortId}    device=NetworkDevice
    hlk_traffic_config_bound_streamblock    name=DOWNSTREAM    type=tcp    sourcePort=80    destPort=102    seqNum=111    ackNum=222
    ...    offset=10    reserved=1111    cwrBit=1    ecnBit=1
    hlk_traffic_config_bound_streamblock    name=DOWNSTREAM    type=frame    FrameLengthMode=RANDOM    MinFrameLength=500    MaxFrameLength=1500    EnableResolveDestMacAddress=FALSE
    #hlk_traffic_config_bound_streamblock    name=DOWNSTREAM    type=frame    srcbinding=    dstbinding=
    hlk_traffic_start_devices    device=all
    Sleep    2s    Allow devices to start
    #Start Capture
    hlk_traffic_start_capture    portID=[${sNetworkPortId},${sClientPortId}]
    hlk_traffic_start_traffic    streamblock=[UPSTREAM,DOWNSTREAM]
    hlk_traffic_stop_traffic    streamblock=UPSTREAM,DOWNSTREAM
    hlk_traffic_stop_capture    portID=all
    hlk_traffic_stop_devices    device=all
    stc disconnect

hlk_ieee1588v2
    llk_stc_send_command    stc::config automationoptions -logTo /tmp/stclog.txt -loglevel DEBUG
    #stc connect
    log    %{SETUPFILENAME}
    Comment    &{stc_info}    get table line    TrafficGenData    type=STC
    Comment    stc connect    &{stc_info}
    log    NEED MODIFIED
    Set Suite Variable    ${sNetworkPortId}    //135.251.247.12/2/2
    Set Suite Variable    ${sClientPortId}    //135.251.247.12/2/1
    hlk_traffic_config_port    portID=${sClientPortId}    burstSize=1    durationMode=CONTINUOUS
    hlk_traffic_config_port    portID=${sNetworkPortId}    burstSize=1    durationMode=CONTINUOUS
    hlk_traffic_connect
    ${dEthernetParams} =    Create_Dictionary    SourceMac=02:03:04:05:06:08
    #${dSBConfigParams} =    Create_Dictionary    frameLengthMode=FIXED    fixedFrameLength=1518
    ${dIPv4Params} =    Create_Dictionary    address=40.20.1.2    gateway=40.20.1.1    prefixLength=30
    log    ================
    log    CREATE A SLAVE CLOCK
    log    ================
    hlk_traffic_config_device    name=todDevice    portID=${sNetworkPortId}
    hlk_traffic_config_device    name=todDevice    type=ipv4    address=192.0.1.10    gateway=192.0.1.1
    hlk_traffic_config_device    name=todDevice    type=ethii    SourceMac=02:03:04:05:06:08
    hlk_traffic_config_device    name=todDevice    type=vlan    vlan1=100    pri1=0    vlan2=200    pri2=3
    ${oIEEE1588}    hlk_traffic_config_ieee1588v2Clock    todDevice    clockConfig=[LogSyncInterval=-4,LogMinDelayRequestInterval=-8,StepMode=ONE_STEP,TimeSrc=INTERNAL_OSCILLATOR,ClockClass=6,EnableUnicastNegotiation=TRUE,slaveOnly=TRUE]
    hlk_traffic_start_capture    portID=[${sNetworkPortId},${sClientPortId}]
    hlk_traffic_start_devices    device=${oIEEE1588}
    wait until keyword succeeds    20    2    hlk_traffic_verify_device_status    todDevice    ieee1588v2clockconfig    clockstate=IEEE1588_STATE_LISTENING
    log    ===MODIFY DEVICE TO CLOCK MASTER===
    ${oIEEE1588}    hlk_traffic_config_ieee1588v2Clock    todDevice    oClock=${oIEEE1588}    clockConfig=[slaveOnly=FALSE]
    wait until keyword succeeds    20    2    hlk_traffic_verify_device_status    todDevice    ieee1588v2clockconfig    clockstate=IEEE1588_STATE_MASTER
    #Start Capture
    Comment    hlk_traffic_start_capture    portID=[${sNetworkPortId},${sClientPortId}]
    Comment    hlk_traffic_start_traffic    streamblock=[UPSTREAM,DOWNSTREAM]
    Comment    hlk_traffic_stop_traffic    streamblock=UPSTREAM,DOWNSTREAM
    hlk_traffic_stop_capture    portID=all
    hlk_traffic_stop_devices    device=all
    #stc disconnect

hlk_ieee1588v2_demo
    llk_stc_send_command    stc::config automationoptions -logTo /tmp/stclog.txt -loglevel DEBUG
    Set Suite Variable    ${sNetworkPortId}    //135.251.247.12/2/2
    Set Suite Variable    ${sClientPortId}    //135.251.247.12/2/1
    hlk_traffic_config_port    portID=${sClientPortId}    burstSize=1    durationMode=CONTINUOUS
    hlk_traffic_config_port    portID=${sNetworkPortId}    burstSize=1    durationMode=CONTINUOUS
    hlk_traffic_connect
    log    ================
    log    CREATE A SLAVE CLOCK
    log    ================
    hlk_traffic_config_device    name=todDevice    portID=${sNetworkPortId}
    hlk_traffic_config_device    name=todDevice    type=ipv4    address=192.0.1.10    gateway=192.0.1.1
    hlk_traffic_config_device    name=todDevice    type=ethii    SourceMac=02:03:04:05:06:08
    hlk_traffic_config_device    name=todDevice    type=vlan    vlan1=100    pri1=0    vlan2=200    pri2=3
    ${oIEEE1588}    hlk_traffic_config_ieee1588v2Clock    todDevice    clockConfig=[LogSyncInterval=-4,LogMinDelayRequestInterval=-8,StepMode=ONE_STEP,TimeSrc=INTERNAL_OSCILLATOR,ClockClass=6,EnableUnicastNegotiation=TRUE,slaveOnly=TRUE]
    log    ================
    log    START DEVICE
    log    ================
    hlk_traffic_start_devices    device=${oIEEE1588}
    log    ================
    log    CHECK STATE
    log    ================
    wait until keyword succeeds    20    2    hlk_traffic_verify_device_status    todDevice    ieee1588v2clockconfig    clockstate=IEEE1588_STATE_LISTENING
    log    ===MODIFY DEVICE TO CLOCK MASTER===
    ${oIEEE1588}    hlk_traffic_config_ieee1588v2Clock    todDevice    oClock=${oIEEE1588}    clockConfig=[slaveOnly=FALSE]
    log    ================
    log    VERIFY STATE
    log    ================
    wait until keyword succeeds    20    2    hlk_traffic_verify_device_status    todDevice    ieee1588v2clockconfig    clockstate=IEEE1588_STATE_MASTER
    log    ================
    log    STOP DEVICES
    log    ================
    hlk_traffic_stop_devices    device=all
    llk_stc_delete    emulateddevice1
    Comment    wait until keyword succeeds    20    2    hlk_traffic_verify_device_status    todDevice    ieee1588v2clockconfig
    ...    clockstate=IEEE1588_STATE_MASTER

stc_ieee1588v2
    log    %{SETUPFILENAME}
    create global tables    %{SETUPFILENAME}
    &{stc_info}    get table line    TrafficGenData    type=STC
    stc connect    &{stc_info}    session_name=first_stc
    log    ===CREATE A MASTER CLOCK===
    ${oIEEE1588v2_master}    stc create device ieee1588v2    client    device_ieee1588v2_master    session_name=first_stc    ipv4if={address=192.168.1.10,gateway=192.168.1.1}    ethiiif={sourceMac=00:00:11:22:33:44}
    ...    vlanif={vlan1=100,pri1=0,vlan2=100,pri2=0}    ieee1588v2clockconfig=[LogSyncInterval=-4,LogMinDelayRequestInterval=-8,StepMode=ONE_STEP,TimeSrc=INTERNAL_OSCILLATOR,ClockClass=6,EnableUnicastNegotiation=TRUE]
    wait until keyword succeeds    10    2    stc retrieve device ieee1588v2    device_ieee1588v2_master    IEEE1588_STATE_MASTER    session_name=first_stc
    log    ===CREATE A SLAVE CLOCK===
    ${oIEEE1588v2_slave}    stc create device ieee1588v2    network    device_ieee1588v2_slave    session_name=first_stc    ipv4if={address=192.168.1.2,gateway=192.168.1.1}    ethiiif={sourceMac=00:00:11:22:33:55}
    ...    ieee1588v2clockconfig=[slaveOnly=True]
    wait until keyword succeeds    10    2    stc retrieve device ieee1588v2    device_ieee1588v2_slave    IEEE1588_STATE_LISTENING    session_name=first_stc
    log    ===MODIFY MASTER CLOCK TO SLAVE
    stc modify device ieee1588v2    device_ieee1588v2_master    oclock=${oIEEE1588v2_master}    ieee1588v2ClockConfig={encap=ETHERNET,slaveOnly=True}    session_name=first_stc
    wait until keyword succeeds    20    2    stc retrieve device ieee1588v2    device_ieee1588v2_master    IEEE1588_STATE_LISTENING    session_name=first_stc
    stc delete device    device_ieee1588v2_master    session_name=first_stc
    stc delete device    device_ieee1588v2_slave    session_name=first_stc
    stc disconnect    session_name=first_stc

stc_dhcp_server
    log    %{SETUPFILENAME}
    create global tables    %{SETUPFILENAME}
    &{stc_info}    get table line    TrafficGenData    type=STC
    stc connect    &{stc_info}    session_name=first_stc
    LOG    ====================================
    LOG    START CAPTURE
    LOG    ====================================
    stc_start_capture    portID=all    session_name=first_stc
    log    ===CREATE DHCP SERVER===
    ${oDhcpServer}    stc create dhcp server    client    device_dhcp_server    ipv4if={address=192.85.1.16,gateway=192.85.1.1}    ethiiif={sourceMac=00:00:11:22:33:44}    vlanif={vlan1=100,pri1=0,vlan2=100,pri2=0}
    ...    serverConfig=[Active=TRUE,hostName=dhcp_client1,leaseTime=600]    serverdefaultpoolconfig=[StartIpList=192.85.1.5,hostAddrCount=16,addrIncrement=2,Active=TRUE]    option_66=[msgType=OFFER,HexValue=FALSE,payload=135.251.200.1]    option_22=[msgType=ACK,HexValue=TRUE]    session_name=first_stc
    wait until keyword succeeds    20    2    stc verify dhcp server state    device_dhcp_server    UP    session_name=first_stc
    log    ===CREATE DHCP CLIENT===
    ${oDhcpClient}    stc create dhcp client    network    device_dhcp_client    ipv4if={address=192.85.1.200,gateway=192.85.1.1}    ethiiif={sourceMac=00:00:11:22:33:55}    vlanif={vlan1=100,pri1=0,vlan2=100,pri2=0}
    ...    circuitId=circuitId_@p    enableCircuitId=True    hostName=client1    optionList="1 3 6 15 33 44 59"    remoteId=remoteId_@p    session_name=first_stc
    wait until keyword succeeds    10    2    stc verify dhcp client state    device_dhcp_client    BOUND    session_name=first_stc
    LOG    ====================================
    LOG    RENEW DHCPV4 SERVER
    LOG    ====================================
    stc operate dhcp server    operation=forceRenew    device=device_dhcp_server    session_name=first_stc
    wait until keyword succeeds    20    2    stc verify dhcp client state    device_dhcp_client    BOUND    session_name=first_stc
    LOG    ====================================
    LOG    REBIND DHCPV4 CLIENT
    LOG    ====================================
    stc operate dhcp client    operation=release    device=device_dhcp_client    session_name=first_stc
    wait until keyword succeeds    10    2    stc verify dhcp client state    device_dhcp_client    IDLE    session_name=first_stc
    log    ===STOP ALL DEVICES===
    stc stop devices    device=all    session_name=first_stc
    log    ===DELETE ALL DEVICES===
    stc delete device    device_dhcp_server    session_name=first_stc
    stc delete device    device_dhcp_client    session_name=first_stc
    LOG    ====================================
    LOG    STOP CAPTURE
    LOG    ====================================
    run keyword and continue on failure    stc_stop_capture    portID=all    session_name=first_stc
    log    ${stc_info}
    Set Suite Variable    ${sNetworkPortId}    //${stc_info['ip']}/${stc_info['network_slot']}/${stc_info['NetworkPort']}
    Set Suite Variable    ${sClientPortId}    //${stc_info['ip']}/${stc_info['client_slot']}/${stc_info['ClientPort']}
    ${Retrieve2_Capture} =    stc_retrieve_capture    portID=[${sClientPortId}]    session_name=first_stc
    ${Retrieve1_Capture} =    stc_retrieve_capture    portID=[${sNetworkPortId}]    session_name=first_stc
    stc disconnect    session_name=first_stc

stc_hlk_arp_streams
    Set Suite Variable    ${sNetworkPort}    //135.251.247.12/2/1
    Set Suite Variable    ${sClientPort}    //135.251.247.12/2/2
    log    %{SETUPFILENAME}
    create global tables    %{SETUPFILENAME}
    &{stc_info}    get table line    TrafficGenData    type=STC
    stc connect    &{stc_info}
    stc_start_capture    portID=all
    LOG    ====================================
    LOG    ENABLE LOG SUPPORT (SEND RAW COMMAND SINCE KEYWORD SUPPORT)
    LOG    ====================================
    Comment    llk_stc_send_command    stc::config automationoptions -logTo /tmp/stclog.txt -loglevel INFO
    LOG    ====================================
    LOG    DETERMINE AND CONFIGURE STC PORTS
    LOG    ====================================
    # Determine STC Ports to be used.    Ports are defined per UNI CPE in TrafficGenMapData table with a network port and client port mapping for each uni.
    # There is no limit to how many ports can be used.
    Comment    ${sNetworkPort} =    hlk_traffic_get_port_id    lif=Network
    Comment    ${sClientPort} =    hlk_traffic_get_port_id    lif=Client
    Comment    # Configure ports for measure
    Comment    #hlk_traffic_config_port    portID=${sNetworkPort}    burstSize=1    duration=1    durationMode=CONTINUOUS    schedulingMode=RATE_BASED
    Comment    hlk_traffic_config_port    portID=${sClientPort}    burstSize=1    durationMode=CONTINUOUS
    Comment    hlk_traffic_config_port    portID=${sNetworkPort}    burstSize=1    durationMode=CONTINUOUS
    LOG    ====================================
    LOG    CREATE AND CONFIGURE STREAMBLOCKS
    LOG    ====================================
    # Configure first upstream streamblock
    Comment    hlk_traffic_config_streamblock    name=US1    ontmac=184a.6f45.92df    ontUni=2    lif=Client
    stc_config_streamblock    name=US1    portid=${sClientPort}
    stc_config_streamblock    name=US1    type=ethii    dstMac=ff:ff:ff:ff:ff:ff    srcMac=00:00:02:00:00:01
    stc_config_streamblock    name=US1    type=vlan    vlan1=11    pri1=1    vlan2=22    pri2=2
    stc_config_streamblock    name=US1    type=arp    senderPAddr=135.251.200.1    targetPAddr=135.251.200.100    senderHwAddr=00:00:02:00:00:01    targetHwAddr=00:00:00:00:00:00
    ...    hardware=0001    protocol=0800    ihAddr=6    ipAddr=4    operation=1
    stc_config_streamblock    name=US1    type=load    load=2    loadunit=FRAMES_PER_SECOND
    stc_config_streamblock    name=US1    type=frame    framelengthmode=auto
    # Configure second upstream streamblock
    stc_config_streamblock    name=US2    portid=${sClientPort}
    stc_config_streamblock    name=US2    type=ethii    dstMac=00:00:01:00:00:01    srcMac=00:00:02:00:00:01
    stc_config_streamblock    name=US2    type=ipv4    destAddr=10.0.0.3    sourceAddr=10.0.0.2
    stc_config_streamblock    name=US2    type=vlan    vlan1=11    pri1=2
    stc_config_streamblock    name=US2    type=load    load=3    loadunit=FRAMES_PER_SECOND
    LOG    ====================================
    LOG    CONNECT TO STC
    LOG    ====================================
    Comment    hlk_traffic_connect
    LOG    ====================================
    LOG    START TRAFFIC
    LOG    ====================================
    stc_start_traffic    streamblock=US1,US2
    LOG    ====================================
    LOG    START MEASURE
    LOG    ====================================
    stc_start_measure    clearResults=FALSE    waitTime=4    resulttype=RxStreamSummaryResults    configtype=StreamBlock
    BuiltIn.Sleep    1s
    LOG    ====================================
    LOG    RETRIEVE MEASURE
    LOG    ====================================
    # Retrieving can be done 3 times and the values averaged if desired.    We are only doing it once here.    Code to check tolerance will need to be added for this method
    # since it is simply retrieving a value
    ${dMeasuredValuesUS1} =    stc_retrieve_measure    streamblock=US1    type=RxStreamSummaryResults    measure=[L1BitRate,FrameCount,DroppedFrameCount,DroppedFramePercent]
    BuiltIn.log    ${dMeasuredValuesUS1}
    ${dMeasuredValuesUS2} =    stc_retrieve_measure    streamblock=US2    type=RxStreamSummaryResults    measure=[L1BitRate,FrameCount,DroppedFrameCount,DroppedFramePercent]
    BuiltIn.log    ${dMeasuredValuesUS2}
    LOG    ====================================
    LOG    VERIFY MEASURE
    LOG    ====================================
    # This type of retrieve verifies the recieved results against expected inputs within specified tolerance
    BuiltIn.RunKeyword_And_Continue_On_Failure    stc_verify_measure    streamblock=US1    type=RxStreamSummaryResults    L1BitRate=845350    L1BitRate_tol=5%
    BuiltIn.RunKeyword_And_Continue_On_Failure    stc_verify_measure    streamblock=US2    type=RxStreamSummaryResults    L1BitRate=845357    L1BitRate_tol=5%
    LOG    ====================================
    LOG    STOP MEASURE
    LOG    ====================================
    stc_stop_measure    resulttype=RxStreamSummaryResults
    LOG    ====================================
    LOG    STOP TRAFFIC
    LOG    ====================================
    stc_stop_traffic    streamblock=US1,US2
    LOG    ====STOP CAPTURE===
    run keyword and continue on failure    stc_stop_capture    portID=all
    ${Retrieve1_Capture} =    stc_retrieve_capture    portID=[${sNetworkPort}]
    ${Retrieve2_Capture} =    stc_retrieve_capture    portID=[${sClientPort}]
    LOG    ====================================
    LOG    DELETE STREAMBLOCKS
    LOG    ====================================
    stc_delete_streamblock    streamblock=all
    LOG    ====================================
    LOG    DISCONNECT FROM STC
    LOG    ====================================
    stc disconnect

stc_arp_streams
    Set Suite Variable    ${sNetworkPort}    //135.251.247.12/2/1
    Set Suite Variable    ${sClientPort}    //135.251.247.12/2/2
    log    %{SETUPFILENAME}
    create global tables    %{SETUPFILENAME}
    &{stc_info}    get table line    TrafficGenData    type=STC
    stc connect    &{stc_info}
    stc_start_capture    portID=all
    @{lFilterItems}    create list    l1bitrate    bitrate    cellrate
    ${dStream1}    Create_Dictionary    vlan.vlan1=11    vlan.pri1=1    vlan.vlan2=12    vlan.pri2=2    ethii.srcMac=11:11:11:11:11:11
    ...    ethii.dstMac=22:22:22:22:22:22    ipv4.destAddr=135.251.1.1    ipv4.sourceAddr=135.251.2.2    load.load=1    loadunit=FRAMES_PER_SECOND    arp.senderPAddr=135.251.200.1
    ...    arp.targetPAddr=135.251.200.100    arp.senderHwAddr=00:00:02:00:00:01    arp.targetHwAddr=00:00:00:00:00:00    arp.hardware=0001    arp.protocol=0800    arp.ihAddr=6
    ...    arp.ipAddr=4    arp.operation=1
    ${dMeasure1}    create dictionary    frameCount=>=2    l1bitrate_tol=5%
    @{lStreamList}    create list    ${dStream1}
    @{lMeasureList}    create list    ${dMeasure1}
    @{FilterResult}    stc upstream protocol traffic measure    arp    ${lStreamList}    measure_list=${lMeasureList}    retrieve_times=200    wait_time=5
    ...    duration=10
    run keyword and continue on failure    stc downstream protocol traffic measure    arp    ${lStreamList}    measure_list=${lMeasureList}    filter_list=${lFilterItems}    duration=10
    ...    expect_result=FAIL,NA,FAIL
    Comment    ${returnFilter}    stc bidirect ip traffic measure    ${lStreamList}    ${lStreamList}    retrieve_times=20    wait_time=10
    ...    up_filter_list=${lFilterItems}    down_measure_list=${lMeasureList}    up_measure_list=${lMeasureList}    duration=20    up_expect_result=PASS,PASS,PASS,PASS,PASS,PASS,PASS,PASS    up_expect_result=PASS,PASS,PASS,PASS,PASS,PASS,PASS,PASS
    run keyword and continue on failure    stc_stop_capture    portID=all
    ${Retrieve1_Capture} =    stc_retrieve_capture    portID=[${sNetworkPort}]
    ${Retrieve2_Capture} =    stc_retrieve_capture    portID=[${sClientPort}]
    stc disconnect

stc_hlk_pppoe_streams
    Set Suite Variable    ${sNetworkPort}    //135.251.247.12/2/1
    Set Suite Variable    ${sClientPort}    //135.251.247.12/2/2
    log    %{SETUPFILENAME}
    create global tables    %{SETUPFILENAME}
    &{stc_info}    get table line    TrafficGenData    type=STC
    stc connect    &{stc_info}
    stc_start_capture    portID=all
    LOG    ====================================
    LOG    ENABLE LOG SUPPORT (SEND RAW COMMAND SINCE KEYWORD SUPPORT)
    LOG    ====================================
    Comment    llk_stc_send_command    stc::config automationoptions -logTo /tmp/stclog.txt -loglevel INFO
    LOG    ====================================
    LOG    DETERMINE AND CONFIGURE STC PORTS
    LOG    ====================================
    # Determine STC Ports to be used.    Ports are defined per UNI CPE in TrafficGenMapData table with a network port and client port mapping for each uni.
    # There is no limit to how many ports can be used.
    Comment    ${sNetworkPort} =    hlk_traffic_get_port_id    lif=Network
    Comment    ${sClientPort} =    hlk_traffic_get_port_id    lif=Client
    Comment    # Configure ports for measure
    Comment    #hlk_traffic_config_port    portID=${sNetworkPort}    burstSize=1    duration=1    durationMode=CONTINUOUS    schedulingMode=RATE_BASED
    Comment    hlk_traffic_config_port    portID=${sClientPort}    burstSize=1    durationMode=CONTINUOUS
    Comment    hlk_traffic_config_port    portID=${sNetworkPort}    burstSize=1    durationMode=CONTINUOUS
    LOG    ====================================
    LOG    CREATE AND CONFIGURE STREAMBLOCKS
    LOG    ====================================
    # Configure first upstream streamblock
    Comment    hlk_traffic_config_streamblock    name=US1    ontmac=184a.6f45.92df    ontUni=2    lif=Client
    stc_config_streamblock    name=US1    portid=${sClientPort}
    stc_config_streamblock    name=US1    type=ethii    dstMac=00:00:02:00:00:03    srcMac=00:00:02:00:00:01
    stc_config_streamblock    name=US1    type=vlan    vlan1=11    pri1=1    vlan2=22    pri2=2
    stc_config_streamblock    name=US1    type=pppoe    code=9    length=996
    stc_config_streamblock    name=US1    type=load    load=2    loadunit=FRAMES_PER_SECOND
    Comment    stc_config_streamblock    name=US1    type=frame    framelengthmode=auto
    # Configure second upstream streamblock
    stc_config_streamblock    name=US2    portid=${sClientPort}
    stc_config_streamblock    name=US2    type=ethii    dstMac=00:00:01:00:00:01    srcMac=00:00:02:00:00:01
    stc_config_streamblock    name=US2    type=ipv4    destAddr=10.0.0.3    sourceAddr=10.0.0.2
    stc_config_streamblock    name=US2    type=vlan    vlan1=11    pri1=2
    stc_config_streamblock    name=US2    type=load    load=3    loadunit=FRAMES_PER_SECOND
    LOG    ====================================
    LOG    CONNECT TO STC
    LOG    ====================================
    Comment    hlk_traffic_connect
    LOG    ====================================
    LOG    START TRAFFIC
    LOG    ====================================
    stc_start_traffic    streamblock=US1,US2
    LOG    ====================================
    LOG    START MEASURE
    LOG    ====================================
    stc_start_measure    clearResults=FALSE    waitTime=4    resulttype=RxStreamSummaryResults    configtype=StreamBlock
    BuiltIn.Sleep    1s
    LOG    ====================================
    LOG    RETRIEVE MEASURE
    LOG    ====================================
    # Retrieving can be done 3 times and the values averaged if desired.    We are only doing it once here.    Code to check tolerance will need to be added for this method
    # since it is simply retrieving a value
    ${dMeasuredValuesUS1} =    stc_retrieve_measure    streamblock=US1    type=RxStreamSummaryResults    measure=[L1BitRate,FrameCount,DroppedFrameCount,DroppedFramePercent]
    BuiltIn.log    ${dMeasuredValuesUS1}
    ${dMeasuredValuesUS2} =    stc_retrieve_measure    streamblock=US2    type=RxStreamSummaryResults    measure=[L1BitRate,FrameCount,DroppedFrameCount,DroppedFramePercent]
    BuiltIn.log    ${dMeasuredValuesUS2}
    LOG    ====================================
    LOG    VERIFY MEASURE
    LOG    ====================================
    # This type of retrieve verifies the recieved results against expected inputs within specified tolerance
    BuiltIn.RunKeyword_And_Continue_On_Failure    stc_verify_measure    streamblock=US1    type=RxStreamSummaryResults    L1BitRate=845350    L1BitRate_tol=5%
    BuiltIn.RunKeyword_And_Continue_On_Failure    stc_verify_measure    streamblock=US2    type=RxStreamSummaryResults    L1BitRate=845357    L1BitRate_tol=5%
    LOG    ====================================
    LOG    STOP MEASURE
    LOG    ====================================
    stc_stop_measure    resulttype=RxStreamSummaryResults
    LOG    ====================================
    LOG    STOP TRAFFIC
    LOG    ====================================
    stc_stop_traffic    streamblock=US1,US2
    LOG    ====STOP CAPTURE===
    run keyword and continue on failure    stc_stop_capture    portID=all
    ${Retrieve1_Capture} =    stc_retrieve_capture    portID=[${sNetworkPort}]
    ${Retrieve2_Capture} =    stc_retrieve_capture    portID=[${sClientPort}]
    LOG    ====================================
    LOG    DELETE STREAMBLOCKS
    LOG    ====================================
    stc_delete_streamblock    streamblock=all
    LOG    ====================================
    LOG    DISCONNECT FROM STC
    LOG    ====================================
    stc disconnect

stc_pppoe_streams
    Set Suite Variable    ${sNetworkPort}    //135.251.247.12/2/1
    Set Suite Variable    ${sClientPort}    //135.251.247.12/2/2
    log    %{SETUPFILENAME}
    create global tables    %{SETUPFILENAME}
    &{stc_info}    get table line    TrafficGenData    type=STC
    stc connect    &{stc_info}
    stc_start_capture    portID=all
    @{lFilterItems}    create list    l1bitrate    bitrate    cellrate
    ${dStream1}    Create_Dictionary    vlan.vlan1=11    vlan.pri1=1    vlan.vlan2=12    vlan.pri2=2    ethii.srcMac=11:11:11:11:11:11
    ...    ethii.dstMac=22:22:22:22:22:22    ipv4.destAddr=135.251.1.1    ipv4.sourceAddr=135.251.2.2    load.load=1    loadunit=FRAMES_PER_SECOND    pppoe.code=9
    ...    pppoe.length=10
    ${dMeasure1}    create dictionary    frameCount=>=2    l1bitrate_tol=5%
    @{lStreamList}    create list    ${dStream1}
    @{lMeasureList}    create list    ${dMeasure1}
    @{FilterResult}    stc upstream protocol traffic measure    pppoe    ${lStreamList}    measure_list=${lMeasureList}    retrieve_times=200    wait_time=5
    ...    duration=10
    run keyword and continue on failure    stc downstream protocol traffic measure    pppoe    ${lStreamList}    measure_list=${lMeasureList}    filter_list=${lFilterItems}    duration=10
    ...    expect_result=FAIL,NA,FAIL
    Comment    ${returnFilter}    stc bidirect ip traffic measure    ${lStreamList}    ${lStreamList}    retrieve_times=20    wait_time=10
    ...    up_filter_list=${lFilterItems}    down_measure_list=${lMeasureList}    up_measure_list=${lMeasureList}    duration=20    up_expect_result=PASS,PASS,PASS,PASS,PASS,PASS,PASS,PASS    up_expect_result=PASS,PASS,PASS,PASS,PASS,PASS,PASS,PASS
    run keyword and continue on failure    stc_stop_capture    portID=all
    ${Retrieve1_Capture} =    stc_retrieve_capture    portID=[${sNetworkPort}]
    ${Retrieve2_Capture} =    stc_retrieve_capture    portID=[${sClientPort}]
    stc disconnect

stc_hlk_dhcp_streams
    Set Suite Variable    ${sNetworkPort}    //135.251.247.12/2/1
    Set Suite Variable    ${sClientPort}    //135.251.247.12/2/2
    log    %{SETUPFILENAME}
    create global tables    %{SETUPFILENAME}
    &{stc_info}    get table line    TrafficGenData    type=STC
    stc connect    &{stc_info}
    stc_start_capture    portID=all
    LOG    ====================================
    LOG    ENABLE LOG SUPPORT (SEND RAW COMMAND SINCE KEYWORD SUPPORT)
    LOG    ====================================
    Comment    llk_stc_send_command    stc::config automationoptions -logTo /tmp/stclog.txt -loglevel INFO
    LOG    ====================================
    LOG    DETERMINE AND CONFIGURE STC PORTS
    LOG    ====================================
    # Determine STC Ports to be used.    Ports are defined per UNI CPE in TrafficGenMapData table with a network port and client port mapping for each uni.
    # There is no limit to how many ports can be used.
    Comment    ${sNetworkPort} =    hlk_traffic_get_port_id    lif=Network
    Comment    ${sClientPort} =    hlk_traffic_get_port_id    lif=Client
    Comment    # Configure ports for measure
    Comment    #hlk_traffic_config_port    portID=${sNetworkPort}    burstSize=1    duration=1    durationMode=CONTINUOUS    schedulingMode=RATE_BASED
    Comment    hlk_traffic_config_port    portID=${sClientPort}    burstSize=1    durationMode=CONTINUOUS
    Comment    hlk_traffic_config_port    portID=${sNetworkPort}    burstSize=1    durationMode=CONTINUOUS
    LOG    ====================================
    LOG    CREATE AND CONFIGURE STREAMBLOCKS
    LOG    ====================================
    # Configure first upstream streamblock
    Comment    hlk_traffic_config_streamblock    name=US1    ontmac=184a.6f45.92df    ontUni=2    lif=Client
    stc_config_streamblock    name=US1    portid=${sClientPort}
    stc_config_streamblock    name=US1    type=ethii    dstMac=00:00:02:00:00:03    srcMac=00:00:02:00:00:01
    stc_config_streamblock    name=US1    type=vlan    vlan1=11    pri1=1    vlan2=22    pri2=2
    stc_config_streamblock    name=US1    type=udp    destPort=67    sourcePort=68
    stc_config_streamblock    name=US1    type=dhcp    messageType=1    clientMac=00:00:02:00:00:01    clientAddr=135.251.200.171
    stc_config_streamblock    name=US1    type=load    load=2    loadunit=FRAMES_PER_SECOND
    Comment    stc_config_streamblock    name=US1    type=frame    framelengthmode=auto
    # Configure second upstream streamblock
    stc_config_streamblock    name=US2    portid=${sClientPort}
    stc_config_streamblock    name=US2    type=ethii    dstMac=00:00:02:00:00:01    srcMac=00:00:02:00:00:03
    stc_config_streamblock    name=US2    type=vlan    vlan1=11    pri1=1    vlan2=22    pri2=2
    stc_config_streamblock    name=US2    type=udp    destPort=68    sourcePort=67
    stc_config_streamblock    name=US2    type=dhcp    messageType=2    clientMac=00:00:02:00:00:01    clientAddr=135.251.200.171
    stc_config_streamblock    name=US2    type=load    load=1    loadunit=FRAMES_PER_SECOND
    LOG    CONNECT TO STC
    LOG    ====================================
    Comment    hlk_traffic_connect
    LOG    ====================================
    LOG    START TRAFFIC
    LOG    ====================================
    stc_start_traffic    streamblock=US1,US2
    LOG    ====================================
    LOG    START MEASURE
    LOG    ====================================
    stc_start_measure    clearResults=FALSE    waitTime=4    resulttype=RxStreamSummaryResults    configtype=StreamBlock
    BuiltIn.Sleep    1s
    LOG    ====================================
    LOG    RETRIEVE MEASURE
    LOG    ====================================
    # Retrieving can be done 3 times and the values averaged if desired.    We are only doing it once here.    Code to check tolerance will need to be added for this method
    # since it is simply retrieving a value
    ${dMeasuredValuesUS1} =    stc_retrieve_measure    streamblock=US1    type=RxStreamSummaryResults    measure=[L1BitRate,FrameCount,DroppedFrameCount,DroppedFramePercent]
    BuiltIn.log    ${dMeasuredValuesUS1}
    ${dMeasuredValuesUS2} =    stc_retrieve_measure    streamblock=US2    type=RxStreamSummaryResults    measure=[L1BitRate,FrameCount,DroppedFrameCount,DroppedFramePercent]
    BuiltIn.log    ${dMeasuredValuesUS2}
    LOG    ====================================
    LOG    VERIFY MEASURE
    LOG    ====================================
    # This type of retrieve verifies the recieved results against expected inputs within specified tolerance
    BuiltIn.RunKeyword_And_Continue_On_Failure    stc_verify_measure    streamblock=US1    type=RxStreamSummaryResults    L1BitRate=845350    L1BitRate_tol=5%
    BuiltIn.RunKeyword_And_Continue_On_Failure    stc_verify_measure    streamblock=US2    type=RxStreamSummaryResults    L1BitRate=845357    L1BitRate_tol=5%
    LOG    ====================================
    LOG    STOP MEASURE
    LOG    ====================================
    stc_stop_measure    resulttype=RxStreamSummaryResults
    LOG    ====================================
    LOG    STOP TRAFFIC
    LOG    ====================================
    stc_stop_traffic    streamblock=US1,US2
    LOG    ====STOP CAPTURE===
    run keyword and continue on failure    stc_stop_capture    portID=all
    ${Retrieve1_Capture} =    stc_retrieve_capture    portID=[${sNetworkPort}]
    ${Retrieve2_Capture} =    stc_retrieve_capture    portID=[${sClientPort}]
    LOG    ====================================
    LOG    DELETE STREAMBLOCKS
    LOG    ====================================
    stc_delete_streamblock    streamblock=all
    LOG    ====================================
    LOG    DISCONNECT FROM STC
    LOG    ====================================
    stc disconnect

stc_dhcp_streams
    Set Suite Variable    ${sNetworkPort}    //135.251.247.12/2/1
    Set Suite Variable    ${sClientPort}    //135.251.247.12/2/2
    log    %{SETUPFILENAME}
    create global tables    %{SETUPFILENAME}
    &{stc_info}    get table line    TrafficGenData    type=STC
    stc connect    &{stc_info}
    stc_start_capture    portID=all
    @{lFilterItems}    create list    l1bitrate    bitrate    cellrate
    ${dStream1}    Create_Dictionary    vlan.vlan1=11    vlan.pri1=1    vlan.vlan2=12    vlan.pri2=2    ethii.srcMac=11:11:11:11:11:11
    ...    ethii.dstMac=22:22:22:22:22:22    ipv4.destAddr=135.251.1.1    ipv4.sourceAddr=135.251.2.2    load.load=1    loadunit=FRAMES_PER_SECOND    udp.destPort=68
    ...    udp.sourcePort=67    dhcp.messageType=1    dhcp.clientMac=00:00:02:00:00:01    dhcp.clientAddr=135.251.200.171
    ${dMeasure1}    create dictionary    frameCount=>=2    l1bitrate_tol=5%
    ${dStream2}    Create_Dictionary    vlan.vlan1=11    vlan.pri1=1    vlan.vlan2=12    vlan.pri2=2    ethii.srcMac=22:22:22:22:22:22
    ...    ethii.dstMac=11:11:11:11:11:11    ipv4.destAddr=135.251.1.1    ipv4.sourceAddr=135.251.2.2    load.load=1    loadunit=FRAMES_PER_SECOND    udp.destPort=67
    ...    udp.sourcePort=68    dhcp.messageType=2    dhcp.clientMac=00:00:02:00:00:03    dhcp.clientAddr=135.251.200.170
    ${dMeasure2}    create dictionary    frameCount=>=2    l1bitrate_tol=5%
    @{lStreamList}    create list    ${dStream1}    ${dStream2}
    @{lMeasureList}    create list    ${dMeasure1}    ${dMeasure2}
    @{FilterResult}    stc upstream protocol traffic measure    dhcp    ${lStreamList}    measure_list=${lMeasureList}    retrieve_times=200    wait_time=5
    ...    duration=10
    run keyword and continue on failure    stc downstream protocol traffic measure    dhcp    ${lStreamList}    measure_list=${lMeasureList}    filter_list=${lFilterItems}    duration=10
    ...    expect_result=FAIL,NA,FAIL
    Comment    ${returnFilter}    stc bidirect ip traffic measure    ${lStreamList}    ${lStreamList}    retrieve_times=20    wait_time=10
    ...    up_filter_list=${lFilterItems}    down_measure_list=${lMeasureList}    up_measure_list=${lMeasureList}    duration=20    up_expect_result=PASS,PASS,PASS,PASS,PASS,PASS,PASS,PASS    up_expect_result=PASS,PASS,PASS,PASS,PASS,PASS,PASS,PASS
    run keyword and continue on failure    stc_stop_capture    portID=all
    ${Retrieve1_Capture} =    stc_retrieve_capture    portID=[${sNetworkPort}]
    ${Retrieve2_Capture} =    stc_retrieve_capture    portID=[${sClientPort}]
    stc disconnect

stc_hlk_ccm_streams
    Set Suite Variable    ${sNetworkPort}    //135.251.247.12/2/1
    Set Suite Variable    ${sClientPort}    //135.251.247.12/2/2
    log    %{SETUPFILENAME}
    create global tables    %{SETUPFILENAME}
    &{stc_info}    get table line    TrafficGenData    type=STC
    stc connect    &{stc_info}
    stc_start_capture    portID=all
    LOG    ====================================
    LOG    ENABLE LOG SUPPORT (SEND RAW COMMAND SINCE KEYWORD SUPPORT)
    LOG    ====================================
    Comment    llk_stc_send_command    stc::config automationoptions -logTo /tmp/stclog.txt -loglevel INFO
    LOG    ====================================
    LOG    CREATE AND CONFIGURE STREAMBLOCKS
    LOG    ====================================
    # Configure first upstream streamblock
    stc_config_streamblock    name=US1    portid=${sClientPort}
    stc_config_streamblock    name=US1    type=ethii    dstMac=00:00:02:00:00:03    srcMac=00:00:02:00:00:01
    stc_config_streamblock    name=US1    type=vlan    vlan1=11    pri1=1    vlan2=22    pri2=2
    stc_config_streamblock    name=US1    type=ccm    sequenceNumber=31983    maepi=1    cfmHeader.mdLevel=5    maid.mdnf=04
    ...    maid.smaf=02    maid.sman=6d61323030    maid.mdn=6d6435    maid.padding=000000000000000000000000000000000000000000000000000000000000000000000000
    stc_config_streamblock    name=US1    type=load    load=2    loadunit=FRAMES_PER_SECOND
    Comment    stc_config_streamblock    name=US1    type=frame    framelengthmode=auto
    # Configure second upstream streamblock
    stc_config_streamblock    name=US2    portid=${sClientPort}
    stc_config_streamblock    name=US2    type=ethii    dstMac=00:00:02:00:00:01    srcMac=00:00:02:00:00:03
    stc_config_streamblock    name=US2    type=vlan    vlan1=11    pri1=1    vlan2=22    pri2=2
    stc_config_streamblock    name=US2    type=udp    destPort=68    sourcePort=67
    stc_config_streamblock    name=US2    type=dhcp    messageType=2    clientMac=00:00:02:00:00:01    clientAddr=135.251.200.171
    stc_config_streamblock    name=US2    type=load    load=1    loadunit=FRAMES_PER_SECOND
    LOG    CONNECT TO STC
    LOG    ====================================
    Comment    hlk_traffic_connect
    LOG    ====================================
    LOG    START TRAFFIC
    LOG    ====================================
    stc_start_traffic    streamblock=US1,US2
    LOG    ====================================
    LOG    START MEASURE
    LOG    ====================================
    stc_start_measure    clearResults=FALSE    waitTime=4    resulttype=RxStreamSummaryResults    configtype=StreamBlock
    BuiltIn.Sleep    1s
    LOG    ====================================
    LOG    RETRIEVE MEASURE
    LOG    ====================================
    # Retrieving can be done 3 times and the values averaged if desired.    We are only doing it once here.    Code to check tolerance will need to be added for this method
    # since it is simply retrieving a value
    ${dMeasuredValuesUS1} =    stc_retrieve_measure    streamblock=US1    type=RxStreamSummaryResults    measure=[L1BitRate,FrameCount,DroppedFrameCount,DroppedFramePercent]
    BuiltIn.log    ${dMeasuredValuesUS1}
    ${dMeasuredValuesUS2} =    stc_retrieve_measure    streamblock=US2    type=RxStreamSummaryResults    measure=[L1BitRate,FrameCount,DroppedFrameCount,DroppedFramePercent]
    BuiltIn.log    ${dMeasuredValuesUS2}
    LOG    ====================================
    LOG    VERIFY MEASURE
    LOG    ====================================
    # This type of retrieve verifies the recieved results against expected inputs within specified tolerance
    BuiltIn.RunKeyword_And_Continue_On_Failure    stc_verify_measure    streamblock=US1    type=RxStreamSummaryResults    L1BitRate=845350    L1BitRate_tol=5%
    BuiltIn.RunKeyword_And_Continue_On_Failure    stc_verify_measure    streamblock=US2    type=RxStreamSummaryResults    L1BitRate=845357    L1BitRate_tol=5%
    LOG    ====================================
    LOG    STOP MEASURE
    LOG    ====================================
    stc_stop_measure    resulttype=RxStreamSummaryResults
    LOG    ====================================
    LOG    STOP TRAFFIC
    LOG    ====================================
    stc_stop_traffic    streamblock=US1,US2
    LOG    ====STOP CAPTURE===
    run keyword and continue on failure    stc_stop_capture    portID=all
    ${Retrieve1_Capture} =    stc_retrieve_capture    portID=[${sNetworkPort}]
    ${Retrieve2_Capture} =    stc_retrieve_capture    portID=[${sClientPort}]
    LOG    ====================================
    LOG    DELETE STREAMBLOCKS
    LOG    ====================================
    stc_delete_streamblock    streamblock=all
    LOG    ====================================
    LOG    DISCONNECT FROM STC
    LOG    ====================================
    stc disconnect

stc_ccm_stream
    Set Suite Variable    ${sNetworkPort}    //135.251.247.12/2/1
    Set Suite Variable    ${sClientPort}    //135.251.247.12/2/2
    log    %{SETUPFILENAME}
    create global tables    %{SETUPFILENAME}
    &{stc_info}    get table line    TrafficGenData    type=STC
    stc connect    &{stc_info}
    stc_start_capture    portID=all
    @{lFilterItems}    create list    l1bitrate    bitrate    cellrate
    ${dStream1}    Create_Dictionary    vlan.vlan1=11    vlan.pri1=1    vlan.vlan2=12    vlan.pri2=2    ethii.srcMac=11:11:11:11:11:11
    ...    ethii.dstMac=22:22:22:22:22:22    ipv4.destAddr=135.251.1.1    ipv4.sourceAddr=135.251.2.2    load.load=1    load.loadunit=FRAMES_PER_SECOND    ccm.maepi=1
    ...    ccm.cfmHeader.mdLevel=5    ccm.maid.mdnf=04    ccm.maid.smaf=02    ccm.maid.sman=6d61323030    ccm.maid.mdn=6d6435    ccm.maid.padding=000000000000000000000000000000000000000000000000000000000000000000000000
    ...    #ccm.sequenceNumber=31900
    ${dMeasure1}    create dictionary    frameCount=>=2    l1bitrate_tol=5%
    ${dStream2}    Create_Dictionary    vlan.vlan1=11    vlan.pri1=1    vlan.vlan2=12    vlan.pri2=2    ethii.srcMac=22:22:22:22:22:22
    ...    ethii.dstMac=11:11:11:11:11:11    ipv4.destAddr=135.251.1.1    ipv4.sourceAddr=135.251.2.2    load.load=1    load.loadunit=FRAMES_PER_SECOND
    ${dMeasure2}    create dictionary    frameCount=>=2    l1bitrate_tol=5%
    @{lStreamList}    create list    ${dStream1}    ${dStream2}
    @{lMeasureList}    create list    ${dMeasure1}    ${dMeasure2}
    @{FilterResult}    stc upstream protocol traffic measure    ccm    ${lStreamList}    measure_list=${lMeasureList}    retrieve_times=200    wait_time=5
    ...    duration=10
    run keyword and continue on failure    stc downstream protocol traffic measure    ccm    ${lStreamList}    measure_list=${lMeasureList}    filter_list=${lFilterItems}    duration=10
    ...    expect_result=FAIL,NA,FAIL
    ${returnFilter}    stc bidirect protocol traffic measure    ccm    ${lStreamList}    ${lStreamList}    retrieve_times=20    up_filter_list=${lFilterItems}
    ...    down_measure_list=${lMeasureList}    up_measure_list=${lMeasureList}    duration=10    up_expect_result=PASS,PASS,PASS,PASS,PASS,PASS,PASS,PASS    up_expect_result=PASS,PASS,PASS,PASS,PASS,PASS,PASS,PASS
    run keyword and continue on failure    stc_stop_capture    portID=all
    Comment    ${Retrieve1_Capture} =    stc_retrieve_capture    portID=[${sNetworkPort}]
    Comment    ${Retrieve2_Capture} =    stc_retrieve_capture    portID=[${sClientPort}]
    stc disconnect

stc_hlk_igmpv2_stream
    Set Suite Variable    ${sNetworkPort}    //135.251.247.12/2/1
    Set Suite Variable    ${sClientPort}    //135.251.247.12/2/2
    log    %{SETUPFILENAME}
    create global tables    %{SETUPFILENAME}
    &{stc_info}    get table line    TrafficGenData    type=STC
    stc connect    &{stc_info}
    stc_start_capture    portID=all
    LOG    ====================================
    LOG    ENABLE LOG SUPPORT (SEND RAW COMMAND SINCE KEYWORD SUPPORT)
    LOG    ====================================
    Comment    llk_stc_send_command    stc::config automationoptions -logTo /tmp/stclog.txt -loglevel INFO
    LOG    ====================================
    LOG    CREATE AND CONFIGURE STREAMBLOCKS
    LOG    ====================================
    # Configure first upstream streamblock
    stc_config_streamblock    name=US1    portid=${sClientPort}
    stc_config_streamblock    name=US1    type=ethii    dstMac=00:00:02:00:00:03    srcMac=00:00:02:00:00:01
    stc_config_streamblock    name=US1    type=vlan    vlan1=11    pri1=1    vlan2=22    pri2=2
    stc_config_streamblock    name=US1    streamtype=igmpv2    groupAddress=225.0.0.2    type=17
    stc_config_streamblock    name=US1    type=load    load=2    loadunit=FRAMES_PER_SECOND
    Comment    stc_config_streamblock    name=US1    type=frame    framelengthmode=auto
    # Configure second upstream streamblock
    stc_config_streamblock    name=US2    portid=${sClientPort}
    stc_config_streamblock    name=US2    type=ethii    dstMac=00:00:02:00:00:01    srcMac=00:00:02:00:00:03
    stc_config_streamblock    name=US2    type=vlan    vlan1=11    pri1=1    vlan2=22    pri2=2
    stc_config_streamblock    name=US2    type=udp    destPort=68    sourcePort=67
    stc_config_streamblock    name=US2    type=dhcp    messageType=2    clientMac=00:00:02:00:00:01    clientAddr=135.251.200.171
    stc_config_streamblock    name=US2    type=load    load=1    loadunit=FRAMES_PER_SECOND
    LOG    CONNECT TO STC
    LOG    ====================================
    Comment    hlk_traffic_connect
    LOG    ====================================
    LOG    START TRAFFIC
    LOG    ====================================
    stc_start_traffic    streamblock=US1,US2
    LOG    ====================================
    LOG    START MEASURE
    LOG    ====================================
    stc_start_measure    clearResults=FALSE    waitTime=4    resulttype=RxStreamSummaryResults    configtype=StreamBlock
    BuiltIn.Sleep    1s
    LOG    ====================================
    LOG    RETRIEVE MEASURE
    LOG    ====================================
    # Retrieving can be done 3 times and the values averaged if desired.    We are only doing it once here.    Code to check tolerance will need to be added for this method
    # since it is simply retrieving a value
    ${dMeasuredValuesUS1} =    stc_retrieve_measure    streamblock=US1    type=RxStreamSummaryResults    measure=[L1BitRate,FrameCount,DroppedFrameCount,DroppedFramePercent]
    BuiltIn.log    ${dMeasuredValuesUS1}
    ${dMeasuredValuesUS2} =    stc_retrieve_measure    streamblock=US2    type=RxStreamSummaryResults    measure=[L1BitRate,FrameCount,DroppedFrameCount,DroppedFramePercent]
    BuiltIn.log    ${dMeasuredValuesUS2}
    LOG    ====================================
    LOG    VERIFY MEASURE
    LOG    ====================================
    # This type of retrieve verifies the recieved results against expected inputs within specified tolerance
    BuiltIn.RunKeyword_And_Continue_On_Failure    stc_verify_measure    streamblock=US1    type=RxStreamSummaryResults    L1BitRate=845350    L1BitRate_tol=5%
    BuiltIn.RunKeyword_And_Continue_On_Failure    stc_verify_measure    streamblock=US2    type=RxStreamSummaryResults    L1BitRate=845357    L1BitRate_tol=5%
    LOG    ====================================
    LOG    STOP MEASURE
    LOG    ====================================
    stc_stop_measure    resulttype=RxStreamSummaryResults
    LOG    ====================================
    LOG    STOP TRAFFIC
    LOG    ====================================
    stc_stop_traffic    streamblock=US1,US2
    LOG    ====STOP CAPTURE===
    run keyword and continue on failure    stc_stop_capture    portID=all
    ${Retrieve1_Capture} =    stc_retrieve_capture    portID=[${sNetworkPort}]
    ${Retrieve2_Capture} =    stc_retrieve_capture    portID=[${sClientPort}]
    LOG    ====================================
    LOG    DELETE STREAMBLOCKS
    LOG    ====================================
    stc_delete_streamblock    streamblock=all
    LOG    ====================================
    LOG    DISCONNECT FROM STC
    LOG    ====================================
    stc disconnect

stc_igmpv2_stream
    Set Suite Variable    ${sNetworkPort}    //135.251.247.12/2/1
    Set Suite Variable    ${sClientPort}    //135.251.247.12/2/2
    log    %{SETUPFILENAME}
    create global tables    %{SETUPFILENAME}
    &{stc_info}    get table line    TrafficGenData    type=STC
    stc connect    &{stc_info}
    stc_start_capture    portID=all
    @{lFilterItems}    create list    l1bitrate    bitrate    cellrate
    ${dStream1}    Create_Dictionary    vlan.vlan1=11    vlan.pri1=1    vlan.vlan2=12    vlan.pri2=2    ethii.srcMac=11:11:11:11:11:11
    ...    ethii.dstMac=22:22:22:22:22:22    ipv4.destAddr=135.251.1.1    ipv4.sourceAddr=135.251.2.2    load.load=1    load.loadunit=FRAMES_PER_SECOND    igmpv2.groupAddress=225.0.0.2
    ...    igmpv2.type=17
    ${dMeasure1}    create dictionary    frameCount=>=2    l1bitrate_tol=5%
    ${dStream2}    Create_Dictionary    vlan.vlan1=11    vlan.pri1=1    vlan.vlan2=12    vlan.pri2=2    ethii.srcMac=22:22:22:22:22:22
    ...    ethii.dstMac=11:11:11:11:11:11    ipv4.destAddr=135.251.1.1    ipv4.sourceAddr=135.251.2.2    load.load=1    load.loadunit=FRAMES_PER_SECOND    igmpv2.groupAddress=225.0.0.3
    ...    igmpv2.type=16
    ${dMeasure2}    create dictionary    frameCount=>=2    l1bitrate_tol=5%
    @{lStreamList}    create list    ${dStream1}    ${dStream2}
    @{lMeasureList}    create list    ${dMeasure1}    ${dMeasure2}
    @{FilterResult}    stc upstream protocol traffic measure    igmpv2    ${lStreamList}    measure_list=${lMeasureList}    retrieve_times=200    wait_time=5
    ...    duration=10
    run keyword and continue on failure    stc downstream protocol traffic measure    igmpv2    ${lStreamList}    measure_list=${lMeasureList}    filter_list=${lFilterItems}    duration=10
    ...    expect_result=FAIL,NA,FAIL
    ${returnFilter}    stc bidirect protocol traffic measure    igmpv2    ${lStreamList}    ${lStreamList}    retrieve_times=20    wait_time=1
    ...    up_filter_list=${lFilterItems}    down_measure_list=${lMeasureList}    up_measure_list=${lMeasureList}    duration=6    up_expect_result=PASS,PASS,PASS,PASS,PASS,PASS,PASS,PASS    up_expect_result=PASS,PASS,PASS,PASS,PASS,PASS,PASS,PASS
    Comment    run keyword and continue on failure    stc_stop_capture    portID=all
    Comment    ${Retrieve1_Capture} =    stc_retrieve_capture    portID=[${sNetworkPort}]
    Comment    ${Retrieve2_Capture} =    stc_retrieve_capture    portID=[${sClientPort}]
    stc disconnect

*** Keywords ***
save_netconf_config_to_file
    [Arguments]    ${reply_xml}    ${path}    ${file_name}
    log    ${reply_xml}
    log    ===============
    log    GET \ INTERFACES ELEMENT
    log    ===============
    ${xml_root}    parse xml    ${reply_xml}
    ${test}    element to string    ${xml_root}
    ${interface_element}    get element    ${xml_root}    xpath=${path}
    ${config_str}    element to string    ${interface_element}
    log    ===============
    log    ADD CONFIG TAG
    log    ===============
    ${config_str}    set variable    <config>\r\n${config_str}\r\n</config>
    log    ${config_str}
    create file    ${file_name}    ${config_str}

recover_netconf_config
    [Arguments]    ${file_name}
    ${xml_str}    get file    ${file_name}
    log    ${xml_str}
