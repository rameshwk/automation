*** Settings ***
Documentation     == Summary ==
...
...               Enable/disable LAN ping reply, telnet, ssh, http and https by TR-069, and check whether it works or not.
...
...               *RCR:* _*ALU02153031*_
...
...               _"Feature request tab security phase 1_
...
...               _8) Security: ACL WAN/LAN for ICMP, SSH, HTTP, TR-069 with trusted network definition in GUI for admin only and with default values."_
...
...
...               *Author:* Cai Jinglu
...
...               *Last Reviser:* Cai Jinglu
...               ---
...               == Customer ==
...
...               Originally requested by Vodafone Spain. And SW is designed generic for *all AONTs*.
...               ---
...               == History ==
...               _*Rev 0:*_ 29. Sep, 2016 --- Cai Jinglu \ <Jinglu.A.Cai@alcatel-sbell.com.cn>: Create this case.
...
...               _*Rev 1:*_ 30. Dec, 2016 --- Cai Jinglu \ <Jinglu.A.Cai@alcatel-sbell.com.cn>: Add determine statements for the suite (I-240W-A/I-240G-D don't support this test) and change some expressions (e.g. ${base_url} -> ${ont['url_protocol']}://${ont['ip']})
...
...               _*Rev 2:*_ 2017 --- Cai Jinglu \ <Jinglu.A.Cai@alcatel-sbell.com.cn>: Originally this function only works on BRCM ONTs in HDR5401, later this suite is also supported on MTK and BRDLT ONTs, change tag to COMMON.
Suite Setup       SuiteSetup
Suite Teardown    SuiteTeardown
Test Teardown     CloseClient
Default Tags      COMMON
Resource          %{ROBOTREPO}/HLK/res/l3-tr069.txt
Resource          %{ROBOTREPO}/HLK/res/l3-selenium2.txt

*** Test Cases ***
01
    [Documentation]    When HttpDisabled = true (means http Denied) for LAN, http open gateway's IP should be failed
    ...
    ...    == Summary ==
    ...
    ...    Enable/disable LAN ping reply, telnet, ssh, http and https by TR-069, and check whether it works or not.
    ...
    ...    *RCR:* _*ALU02153031*_
    ...
    ...    _"Feature request tab security phase 1_
    ...
    ...    _8) Security: ACL WAN/LAN for ICMP, SSH, HTTP, TR-069 with trusted network definition in GUI for admin only and with default values."_
    ...
    ...
    ...    *Author:* Cai Jinglu
    ...
    ...    *Last Reviser:* Cai Jinglu
    ...    ---
    ...    == Customer ==
    ...
    ...    Originally requested by Vodafone Spain. And SW is designed generic for *all AONTs*.
    ...    ---
    ...    == History ==
    ...    _*Rev 0:*_ 29. Sep, 2016 --- Cai Jinglu \ <Jinglu.A.Cai@alcatel-sbell.com.cn>: Create this case.
    ...
    ...    _*Rev 1:*_ 30. Dec, 2016 --- Cai Jinglu \ <Jinglu.A.Cai@alcatel-sbell.com.cn>: Add determine statements for the suite (I-240W-A/I-240G-D don't support this test) and change some expressions (e.g. ${base_url} -> ${ont['url_protocol']}://${ont['ip']})
    ...
    ...    _*Rev 2:*_ 2017 --- Cai Jinglu \ <Jinglu.A.Cai@alcatel-sbell.com.cn>: Originally this function only works on BRCM ONTs in HDR5401, later this suite is also supported on MTK and BRDLT ONTs, change tag to COMMON.
    TriggerInform
    ${status}=    SetParameterValuesEx    ${path}.HttpDisabled true
    Should Be Equal    ${status}    0
    ${status1}=    Run Keyword And Return Status    L1-Selenium2.OpenBrowser    http://${ont['ip']}
    ${status}=    Run Keyword If    ${status1}    Run Keyword And Return Status    L1-Selenium2.GetText    id=username
    ...    ELSE    Set Variable    False
    Should Not Be True    ${status}

02
    [Documentation]    When HttpDisabled = false (means http Allowed) for LAN, http open gateway's IP should be successful
    ...
    ...    == Summary ==
    ...
    ...    Enable/disable LAN ping reply, telnet, ssh, http and https by TR-069, and check whether it works or not.
    ...
    ...    *RCR:* _*ALU02153031*_
    ...
    ...    _"Feature request tab security phase 1_
    ...
    ...    _8) Security: ACL WAN/LAN for ICMP, SSH, HTTP, TR-069 with trusted network definition in GUI for admin only and with default values."_
    ...
    ...
    ...    *Author:* Cai Jinglu
    ...
    ...    *Last Reviser:* Cai Jinglu
    ...    ---
    ...    == Customer ==
    ...
    ...    Originally requested by Vodafone Spain. And SW is designed generic for *all AONTs*.
    ...    ---
    ...    == History ==
    ...    _*Rev 0:*_ 29. Sep, 2016 --- Cai Jinglu \ <Jinglu.A.Cai@alcatel-sbell.com.cn>: Create this case.
    ...
    ...    _*Rev 1:*_ 30. Dec, 2016 --- Cai Jinglu \ <Jinglu.A.Cai@alcatel-sbell.com.cn>: Add determine statements for the suite (I-240W-A/I-240G-D don't support this test) and change some expressions (e.g. ${base_url} -> ${ont['url_protocol']}://${ont['ip']})
    ...
    ...    _*Rev 2:*_ 2017 --- Cai Jinglu \ <Jinglu.A.Cai@alcatel-sbell.com.cn>: Originally this function only works on BRCM ONTs in HDR5401, later this suite is also supported on MTK and BRDLT ONTs, change tag to COMMON.
    TriggerInform
    ${status}=    SetParameterValuesEx    ${path}.HttpDisabled false
    Should Be Equal    ${status}    0
    ${status1}=    Run Keyword And Return Status    L1-Selenium2.OpenBrowser    http://${ont['ip']}
    ${status}=    Run Keyword If    ${status1}    Run Keyword And Return Status    L1-Selenium2.GetText    id=username
    ...    ELSE    Set Variable    False
    Should Be True    ${status}

*** Keywords ***
SuiteSetup
    Create Http Context    localhost:8015
    Set Suite Variable    ${path}    InternetGatewayDevice.X_ALU-COM_LanAccessCfg
    TriggerInform
    LOG    If brdlt ONT fail in suitesetup, please refer to FR ALU02330872
    @{parameter_list}=    GetParameterValuesEx    ${path}.HttpDisabled
    CloseClient
    ${parameter_dict}=    Create Dictionary
    : FOR    ${string}    IN    @{parameter_list}
    \    @{entry}=    Split String    ${string}    ${SPACE}
    \    Set To Dictionary    ${parameter_dict}    @{entry}[0]    @{entry}[1]
    ${http_disabled}=    Get From Dictionary    ${parameter_dict}    ${path}.HttpDisabled
    Set Suite Variable    ${http_disabled}    ${http_disabled}

SuiteTeardown
    CloseAllBrowser
    TriggerInform
    ${status}=    SetParameterValuesEx    ${path}.HttpDisabled ${http_disabled}
    Should Be Equal    ${status}    0
    CloseClient
    [Teardown]    Set Global Variable    ${check_environment}    1
