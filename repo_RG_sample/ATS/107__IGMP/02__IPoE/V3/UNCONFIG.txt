*** Settings ***
Default Tags      COMMON
Library           %{ROBOTREPO}/HLK/lib/cli_command.py
Library           %{RFPATH}/Python-2.7/lib/python2.7/site-packages/robot/libraries/Collections.py
Resource          %{ROBOTREPO}/HLK/res/l2-pcta.txt
Resource          %{ROBOTREPO}/HLK/res/l3-telnet.txt
Resource          %{ROBOTREPO}/HLK/res/l3-igmp.txt
Resource          %{ROBOTREPO}/HLK/res/l2-string.txt
Resource          %{ROBOTREPO}/HLK/res/l3-selenium2.txt

*** Test Cases ***
01
    [Documentation]    IGMPv3 - check CPE LAN side can receive GMQ packets.
    [Tags]    COMMON
    [Setup]    SetupV3-1
    ${lan_interface}=    Get Substring    ${pc['lan_interface']}    3
    @{counter}=    evaluate    range(10)
    : FOR    ${a}    IN    @{counter}
    \    ${cap_dist_id}=    ta_start_capt_igmp_message    ${lan_interface}    -igmp_type 11 -ip_dest 224.0.0.1
    \    sleep    18s
    \    ${cap_result_dict}=    ta_stop_capt_igmp_message    ${lan_interface}    -dist_id ${cap_dist_id}
    \    Log Many    ${cap_result_dict}
    \    ${passed}=    Run Keyword And Return Status    Should Not Be Equal As Strings    ${cap_result_dict}    NONE
    \    Exit For Loop If    ${passed}
    ${igmp_type}=    Get From Dictionary    ${cap_result_dict}    -igmp_type
    ${igmp_version}=    Get From Dictionary    ${cap_result_dict}    -igmp_version
    Should Be Equal As Strings    ${igmp_type}    11
    Should Be Equal As Strings    ${igmp_version}    V3
    [Teardown]    TeardownV3-1

02
    [Documentation]    IGMPv3 ASM - Send IGMPv3 join packets on LAN Client. Check on OLT, status should normally.
    [Tags]    COMMON
    # Send join packets
    JoinGroup    01005E000024    225.0.0.36    version=V3
    Check Cli Command Regexp    show mcast active-groups    ${uni_interface}:${iptv_vlan}.*225.0.0.36    check_time=5

03
    [Documentation]    IGMPv3 ASM- Send IGMPv3 leave packets on LAN Client. Check on OLT, status should normally.
    [Tags]    COMMON
    # send leave packets
    LeaveGroup    01005E000024    225.0.0.36    version=V3
    Check Cli Command Regexp    show mcast active-groups    ${uni_interface}:${iptv_vlan}.*225.0.0.36    expect_result=fail

04
    [Documentation]    IGMPv3 ASM - mcast traffic test
    ...
    ...    Send IGMP join packets on LAN Client. mcast downstream should be PASS
    ...
    ...    Send IGMP leave packets on LAN Client. mcast downstream should be FAIL
    [Tags]    COMMON
    # Send join packets
    JoinGroup    01005E000024    225.0.0.36    version=V3
    Check Cli Command Regexp    show mcast active-groups    ${uni_interface}:${iptv_vlan}.*225.0.0.36    check_time=5
    # check Client received mcast packets
    CheckJoinSuccess    01005E000024    225.0.0.36    vlan=${iptv_vlan}
    # send leave packets
    LeaveGroup    01005E000024    225.0.0.36    version=V3
    # check Client should not received mcast packets
    CheckLeaveSuccess    01005E000024    225.0.0.36    vlan=${iptv_vlan}

05
    [Documentation]    IGMPv3 ASM - multiple mcast group test
    ...
    ...    1. join mcast group 225.0.0.36
    ...
    ...    2. join mcast group 225.0.0.35
    ...
    ...    3. send mcast traffic for 225.0.0.36, LAN client should receive the packets
    ...
    ...    4. send mcast traffic for 225.0.0.35, LAN client should receive the packets
    ...
    ...    5. send leave packets for 225.0.0.36, group 225.0.0.35 should normally.
    ...
    ...    6. send leave packets for 225.0.0.35
    [Tags]    COMMON
    # Send join packets for mcast group 225.0.0.36
    JoinGroup    01005E000024    225.0.0.36    version=V3
    Check Cli Command Regexp    show mcast active-groups    ${uni_interface}:${iptv_vlan}.*225.0.0.36    check_time=5
    # Send join packets for mcast group 225.0.0.35
    JoinGroup    01005E000023    225.0.0.35    version=V3
    Check Cli Command Regexp    show mcast active-groups    ${uni_interface}:${iptv_vlan}.*225.0.0.35    check_time=5
    # check Client received mcast packets for mcast group 225.0.0.36
    CheckJoinSuccess    01005E000024    225.0.0.36    vlan=${iptv_vlan}
    # check Client received mcast packets for mcast group 225.0.0.35
    CheckJoinSuccess    01005E000023    225.0.0.35    vlan=${iptv_vlan}
    # send leave packets for mcast group 225.0.0.36
    LeaveGroup    01005E000024    225.0.0.36    version=V3
    # 225.0.0.36 should be leave
    Check Cli Command Regexp    show mcast active-groups    ${uni_interface}:${iptv_vlan}.*225.0.0.36    expect_result=fail
    CheckLeaveSuccess    01005E000024    225.0.0.36    vlan=${iptv_vlan}
    # group 225.0.0.35 should be exist
    CheckJoinSuccess    01005E000023    225.0.0.35    vlan=${iptv_vlan}
    Check Cli Command Regexp    show mcast active-groups    ${uni_interface}:${iptv_vlan}.*225.0.0.35    check_time=5
    # send leave packets for mcast group 225.0.0.35
    LeaveGroup    01005E000023    225.0.0.35    version=V3
    # group 225.0.0.35 should be leave
    Check Cli Command Regexp    show mcast active-groups    ${uni_interface}:${iptv_vlan}.*225.0.0.35    expect_result=fail
    CheckLeaveSuccess    01005E000023    225.0.0.35    vlan=${iptv_vlan}

06
    [Documentation]    IGMPv3 ASM - multiple mcast group test
    ...
    ...    1. join mcast group 225.0.0.36
    ...
    ...    2. join mcast group 225.0.0.35
    ...
    ...    3. send leave packets for 225.0.0.36
    ...
    ...    4. send mcast down stream for 225.0.0.35, LAN client should receive packets
    ...
    ...    5. send mcast down stream for 225.0.0.36, LAN client should not receive packets
    ...
    ...    6. send leave packets for 225.0.0.35
    [Tags]    COMMON
    # Send join packets for mcast group 225.0.0.36
    JoinGroup    01005E000024    225.0.0.36    version=V3
    Check Cli Command Regexp    show mcast active-groups    ${uni_interface}:${iptv_vlan}.*225.0.0.36    check_time=5
    # Send join packets for mcast group 225.0.0.35
    JoinGroup    01005E000023    225.0.0.35    version=V3
    Check Cli Command Regexp    show mcast active-groups    ${uni_interface}:${iptv_vlan}.*225.0.0.35    check_time=5
    # send leave packets for mcast group 225.0.0.36
    LeaveGroup    01005E000024    225.0.0.36    version=V3
    # group 225.0.0.36 should be leave
    Check Cli Command Regexp    show mcast active-groups    ${uni_interface}:${iptv_vlan}.*225.0.0.36    expect_result=fail
    # group 225.0.0.35 should be exist
    Check Cli Command Regexp    show mcast active-groups    ${uni_interface}:${iptv_vlan}.*225.0.0.35    check_time=5
    CheckJoinSuccess    01005E000023    225.0.0.35    vlan=${iptv_vlan}
    # send leave packets for mcast group 225.0.0.35
    LeaveGroup    01005E000023    225.0.0.35    version=V3
    # group 225.0.0.35 should be leave
    Comment    Check Cli Command Regexp    show mcast active-groups    ${uni_interface}:${iptv_vlan}.*225.0.0.35    expect_result=fail
    CheckLeaveSuccess    01005E000023    225.0.0.35    vlan=${iptv_vlan}

07
    [Documentation]    IGMPv3 ASM - multiple mcast host test
    [Tags]    UNUSE
    # Send join packets, mac is 001122334477
    JoinGroup    01005E000024    225.0.0.36    version=V3    src_mac=001122334477    src_ip=${lan_ip_prefix}.201
    Check Cli Command Regexp    show mcast active-groups    ${uni_interface}:${iptv_vlan}.*225.0.0.36    check_time=5
    # Send join packets, mac is 001122334488
    JoinGroup    01005E000024    225.0.0.36    version=V3    src_mac=001122334488    src_ip=${lan_ip_prefix}.202
    CheckJoinSuccess    01005E000024    225.0.0.36    vlan=${iptv_vlan}
    # Send leave packets, mac is 001122334477
    LeaveGroup    01005E000024    225.0.0.36    src_mac=001122334477    src_ip=${lan_ip_prefix}.201    version=V3
    CheckJoinSuccess    01005E000024    225.0.0.36    vlan=${iptv_vlan}
    # Send leave packets, mac is 001122334488
    LeaveGroup    01005E000024    225.0.0.36    src_mac=001122334488    src_ip=${lan_ip_prefix}.202    version=V3
    CheckLeaveSuccess    01005E000024    225.0.0.36    vlan=${iptv_vlan}

08
    [Documentation]    IGMPv3 ASM - multiple host join to multiple group
    [Tags]    UNUSE
    # Send join packets, mac is 001122334477
    JoinGroup    01005E000024    225.0.0.36    version=V3    src_mac=001122334477    src_ip=${lan_ip_prefix}.201
    CheckJoinSuccess    01005E000024    225.0.0.36    vlan=${iptv_vlan}
    # Send join packets, mac is 001122334488
    JoinGroup    01005E000023    225.0.0.35    version=V3    src_mac=001122334488    src_ip=${lan_ip_prefix}.202
    CheckJoinSuccess    01005E000024    225.0.0.35    vlan=${iptv_vlan}
    # Send leave packets, mac is 00112233447
    LeaveGroup    01005E000024    225.0.0.36    src_mac=001122334477    src_ip=${lan_ip_prefix}.201    version=V3
    CheckLeaveSuccess    01005E000024    225.0.0.36    vlan=${iptv_vlan}
    # Send leave packets, mac is 001122334488
    LeaveGroup    01005E000023    225.0.0.35    src_mac=001122334488    src_ip=${lan_ip_prefix}.202    version=V3
    CheckLeaveSuccess    01005E000023    225.0.0.35    vlan=${iptv_vlan}

09
    [Documentation]    IGMPv3 ASM - check CPE LAN side can receive GSQ packets.
    [Tags]    UNUSE
    # Send join packets
    JoinGroup    01005E000024    225.0.0.36    version=V3
    Check Cli Command Regexp    show mcast active-groups    ${uni_interface}:${iptv_vlan}.*225.0.0.36    check_time=5
    # send leave packets
    ${cap_dist_id}=    ta_start_capt_igmp_message    ${lan_interface}    -igmp_type 11 -ip_dest 225.0.0.36
    LeaveGroup    01005E000024    225.0.0.36    version=V3
    Sleep    ${WAIT_GSQ}
    ${cap_result_dict}=    ta_stop_capt_igmp_message    ${lan_interface}    -dist_id ${cap_dist_id}
    Should Not Be Equal As Strings    ${cap_result_dict}    NONE
    Log Many    ${cap_result_dict}
    Check Cli Command Regexp    show mcast active-groups    ${uni_interface}:${iptv_vlan}.*225.0.0.36    expect_result=fail

*** Keywords ***
SetupV3-1
    Send Cli Command    configure igmp system query-interval 15

TeardownV3-1
    Send Cli Command    configure igmp system no query-interval
