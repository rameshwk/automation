*** Settings ***
Suite Setup
Suite Teardown
Default Tags      COMMON
Library           %{RFPATH}/Python-2.7/lib/python2.7/site-packages/robot/libraries/String.py
Library           %{RFPATH}/Python-2.7/lib/python2.7/site-packages/robot/libraries/Collections.py
Resource          %{ROBOTREPO}/HLK/res/l3-selenium2.txt

*** Test Cases ***
01
    [Documentation]    Objective: Check that ONT will provide vendor information on webgui.
    ...
    ...    Author: li lin
    # Get vendor info
    l3-selenium2.LoginGUI    ${ont['gui_type']}    ${ont['url_protocol']}://${ont['ip']}    ${ont['gui_username']}    ${ont['gui_password']}
    ${DeviceInfo}    l3-selenium2.GetDeviceInfo    ${ont['gui_type']}    ${ont['url_protocol']}://${ont['ip']}
    l3-selenium2.LogoutGUI    ${ont['gui_type']}    ${ont['url_protocol']}://${ont['ip']}
    ${vendor}=    Get From Dictionary    ${DeviceInfo}    vendor
    Should Be Equal    ${vendor}    Nokia

02
    [Documentation]    Objective: Check that ONT will provide device information on webgui.
    ...
    ...    Author: li lin
    #Get Device name
    l3-selenium2.LoginGUI    ${ont['gui_type']}    ${ont['url_protocol']}://${ont['ip']}    ${ont['gui_username']}    ${ont['gui_password']}
    ${DeviceInfo}    l3-selenium2.GetDeviceInfo    ${ont['gui_type']}    ${ont['url_protocol']}://${ont['ip']}
    LOG    ${DeviceInfo}
    l3-selenium2.LogoutGUI    ${ont['gui_type']}    ${ont['url_protocol']}://${ont['ip']}
    ${DeviceName}=    Get From Dictionary    ${DeviceInfo}    device_name
    ${DevName}=    Remove String    ${DeviceName}    -    _
    Should Contain    ${DevName}    ${ont['hardware']}

03
    [Documentation]    Objective: Check that ONT will provide serial NO. information on webgui.
    ...
    ...    Author: li lin
    #Get ont serial NO.
    l3-selenium2.LoginGUI    ${ont['gui_type']}    ${ont['url_protocol']}://${ont['ip']}    ${ont['gui_username']}    ${ont['gui_password']}
    ${DeviceInfo}    l3-selenium2.GetDeviceInfo    ${ont['gui_type']}    ${ont['url_protocol']}://${ont['ip']}
    l3-selenium2.LogoutGUI    ${ont['gui_type']}    ${ont['url_protocol']}://${ont['ip']}
    ${SerialName}=    Get From Dictionary    ${DeviceInfo}    serial_number
    Should Be Equal    ${SerialName}    ${ont['serial_num']}

04
    [Documentation]    Objective: Check that ONT will provide basic chipset information on webgui.
    ...
    ...    Author: li lin
    Run Keyword If    "${ont['gui_type']}"=="G140WC" and "${ont['opid']}"=="BRTI"    Pass Execution    Should not show chipset for G140WC with opid BRTI.
    # Get chipset info
    l3-selenium2.LoginGUI    ${ont['gui_type']}    ${ont['url_protocol']}://${ont['ip']}    ${ont['gui_username']}    ${ont['gui_password']}
    ${DeviceInfo}    l3-selenium2.GetDeviceInfo    ${ont['gui_type']}    ${ont['url_protocol']}://${ont['ip']}
    l3-selenium2.LogoutGUI    ${ont['gui_type']}    ${ont['url_protocol']}://${ont['ip']}
    ${chipset}=    Get From Dictionary    ${DeviceInfo}    chipset
    ${DeviceName}=    Get From Dictionary    ${DeviceInfo}    device_name
    ${DevName}=    Remove String    ${DeviceName}    -
    Run Keyword If    '${DevName}'=='G241WA' or '${DevName}'=='G240WA' or '${DevName}'=='G240WB' or '${DevName}'=='G440GA' or '${DevName}'=='G240WC' or '${DevName}'=='G240GC'    Should Be equal    ${chipset}    BCM6838
    Run Keyword If    '${DevName}'=='I240WA' or '${DevName}'=='I240GD'    Should Be equal    ${chipset}    BL23570
    # add G140WC from HDR57
    Run Keyword If    '${DevName}'=='G240WF' or '${DevName}'=='G240GE' or '${DevName}'=='G120WF' or '${DevName}'=='G120GE'     Should Match Regexp    ${chipset}    MTK7526G|MTK7526FD|MTK7521F
    Run Keyword If    '${DevName}'=='G140WC' and "${ont['opid']}"!="BRTI"    Should Match Regexp    ${chipset}    MTK7526G|MTK7526FD|MTK7521F

*** Keywords ***
